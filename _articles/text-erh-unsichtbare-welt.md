---
title: "Rosenstock-Huessy: Unsichtbare Welt"
category: online-text
landingpage: front
order-lp: 30
org-publ: 1921
---
![Eugen Rosenstock-Huessy]({{ 'assets/images/erh_chess.jpg' | relative_url }}){:.img-right.img-small}

>Weil die Welt zu hell ge&shy;worden\
und mit selbst&shy;ge&shy;speis&shy;tem Lichte\
unsere Augen ewig beizte,\
sank sie plötzlich in das Dunkel,\
heute liegt sie unsichtbar.

>Als ich drum im Bilderladen\
diese Welt im Bilde suchte,\
war ich plötzlich auch wie blind.\
Keins der Bilder gab mehr Farbe,\
ausgeblichen die Gestalten,\
ausgeblichen ganz der Sinn.
<!--more-->

>Ausgegangen sind die Formen,\
die als Gottes Bildersprache\
alle Ordnung unter Menschen\
heiligten und hell verklärten.

>Ausgegangen ist das Licht,\
das auf unsern Tageswegen\
unsere Rechte, unsere Pflichten\
sichtbar wies. Ausgegangen sind\
sogar die klaren Worte,\
die wie goldgeschnitten\
immer alles wohlgereimt verklären.

>Und so stand ich blind im Dunkel;\
in dem Laden,\
in mir selber,\
in der Straße,\
in dem Staate,\
in dem Volk verlosch das Licht.

>Und in dieser unsichtbar und\
ungeformt gewordenen Welt\
soll ich heute Dich beschenken,\
soll das Weihnachtslicht\
in eine Gabe farbig niederglühn?

>Sieh auch sie verschwand im Dunkel.\
Wie wenn erst noch alles wieder\
ungesehen sollte werden,\
eh es wieder kommen darf!

>Unsichtbare kleine Gabe --\
ach sie war schon inhaltsleer!\
War nur eine kleine\
offne, ungefüllte, leere Schale,\
ohne süßen oder bittern,\
ohne irgendwelchen Inhalt.

>Und wie eine Schale\
reicht sich uns die Zeit ja heute\
ohne süßen, ohne bittern,\
ohne Inhalt noch entgegen.

>Nimm die Schale denn des Festes,\
da die Silberschale fehlet,\
da sogar der schöne Umschlag,\
für die Worte eine Schale,\
fehlt mit seinen goldenen Lettern:

>Hier „die unsichtbare Welt”.\
Nimm das Fest als offene Schale,\
und das ganze Licht des Himmels,\
fülle sie im tiefen Dunkel\
einer unsichtbaren Nacht.

>Welt des Krieges, Welt des Neides,\
Welt der Menschen, Welt der Bilder,\
unsichtbar und ohne Formen:\
Weiche vor der Weihnacht Licht!

Eugen Rosenstock-Huessy an Margrit\
Weihnachten 1917
