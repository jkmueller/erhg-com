---
title: Eckart Wilkens' Rede zum Tode Eugen Rosenstock-Huessys
category: einblick
---
![Eckart]({{ 'assets/images/Eckart-Wilkens.jpg' | relative_url }}){:.img-right.img-small}
***DIENST AUF DEM PLANETEN ZUM TODE***\
***EUGEN ROSENSTOCK-HUESSYS (1888-1973)***

*Rede am Freitag, dem 16. März 1973 im Forum (III)*\
*der Volkshochschule Köln*


#### 1. NACHRICHTEN
Meine Damen und Herren!

Vorstellungslos muß ich die Kluft zwischen Vortragendem, der ja leicht in Gefahr steht, zum scharrenden Automaten zu werden, der, um das zu umgehen, dann gern sein Jackett auszieht und sagen möchte wie mancher Lehrer heut zu seinen Schülern: Freunde, Kameraden und Mitarbeiter – zu dem Publikum, das sich aus Ihnen bunt mischt, überbrückenund kann es nur, indem ich exzentrisch anfange.

Seit September 1971 stehen im Arbeitsplan der Volkshochschule Köln unter der Überschrift Lebensnotwendige Logik folgende Sätze:
Logik und erfolgreiches Leben sind untrennbar miteinander verbunden. Die Logik ist das Mittel schlechthin. Deshalb hat sie als Fundamentalwissenschaft zu gelten und sollte Prinzip des gesamten theoretischen und praktischen Unterrichts sein.
Herr Dr. Hermann Segall – mit der Nennung des Namens verlasse ich die Logik, die erfolgreiches Leben verspricht! – hat diese Sätze verfaßt und vertritt sie im pluralistischen Konzert, dessen berauschender Klang beim Durchblättern der Arbeitspläne der Erwachsenenbildung zu vernehmen ist.
Die Sätze beleuchten in eindrücklicher Weise, warum Eugen Rosenstock-Huessy, der im Februar dieses Jahres im 85. Lebensjahr gestorben ist, nicht erfolgreich war.
Im gleichen Arbeitsplan sind insgesamt sieben Foren angekündigt, Vortragsreihen, die zu je einem Thema mehrere Vortragende zu Wort kommen lassen, deren Beiträge vom diskutierenden Publikum aufeinander bezogen und als Vergegenwärtigung eines Spannungsfeldes erfaßt werden wollen.

Sie sind ein Spiegel dessen, was in der Öffentlichkeit zur Sprache steht:
Ost und West – Perspektiven für die Koexistenz Wirtschaftsplanung in Ost und West Anthropologie und Gesellschaftskritik\
Suche nach Gott – Schicksal des Menschen?\
schließlich, damit berühren wir das heute Abend zur Sprache zu bringende,
Erwachsenenbildung, Weiterbildung – für wen, wozu, mit welchen Inhalten und Methoden?
Dieses Forum bietet sieben Beiträge an, von denen die ersten drei im Januar bereits gebracht sind:

Herr Professor Dr. Hans-Hermann Groothoff aus Köln deutet in dem gedruckten Erläuterungstext an, die deutsche Erwachsenenbildung sei, allem Anschein nach, in eine neue Krise geraten; im folgenden Satz nennt er diesen Vorgang eine Entwicklung, einen Vorgang also, der sich aus Gegebenem logisch, folgerichtig ergibt.
Herr Professor Dr. Franz Pöggeler aus Aachen bringt in seinem Beitrag das Wort Curriculum der Erwachsenenbildung – curriculum, im Lexikon sind dafür die Bedeutungen zu finden: der Lauf, Wettlauf, Wettrennen, Umlauf, Kreislauf, Wagen, Rennwagen, Streitwagen, Laufbahn, Rennbahn – natürlich suchen wir gleich die richtige Bedeutung heraus: In der Laufbahn der Erwachsenenbildung wird manches offeriert.
Herr Dr. Hans Tietgens aus Frankfurt am Main bemerkt, um die Frage nach Methoden der Erwachsenenbildung zu beantworten, müsse bekannt sein, wie Erwachsene lernen – also Glaskasten her, Lernende darein gestopft, die richtigen Beobachtungsinstrumente gebraucht und man wird die Frage instrumental beantworten können!
Herr Professor Dr. Oskar Negt aus Hannover fordert die Erziehung zu einer soziologischen Denkweise.

Die letzten drei Vorträge sollen Berichte aus drei Nachbarländern bringen, aus Dänemark, den Niederlanden, Großbritannien.
Zwischen den dritten und vierten Vortrag dieses Forums, zwischen Methoden der Erwachsenenbildung und Neue Wege in der Arbeiterbildung fiel der Tod Eugen Rosenstock-Huessys, der einmal auch Vizepräsident der Weltvereinigung für Erwachsenenbildung war.
In der Wochenzeitung Die Zeit Nr. 10 vom 2. März 1973 ist dieses Ereignis nicht gemeldet.
Dafür feiert sie, wie die kommunistischen Parteien der ganzen Welt in diesen Tagen, mit einem kritischen Artikel Iring Fetschers, den 125. Geburtstag des Kommunistischen Manifests, eines bedeutenden Werks deutscher Prosa, wie es da genannt wird.
Karl Marx, Friedrich Engels, Saint Simon, Louis Althusser, Max Scheler, Malthus, Ricardo, Nicolaus, Maximilian Rubel – der als französischer Marxologe aufgeführt wird – Leo Trotzki, Lenin, Stalin, Suchanow, Mao Tse- tung, Bettelheim sind die Namen, an denen sich die Kritik orientiert, deren inhaltliche Seite mit dem Satz skizziert werden kann:
Das Beispiel der Sowjetunion und Chinas beweist nicht die Richtigkeit des Kommunistischen Manifestes, wohl aber die Möglichkeit der beschleunigten Industrialisierung unter bürokratisch-staatskapitalistischen Formen.

Der Proletarier – so steht es in Rosenstock-Huessys 1931 in Jena zuerst erschienenem Buch Die Europäischen Revolutionen, das in der 2. Ausgabe von 1951 den Untertitel Volkscharakter und Staatenbildung in den Titel aufrücken läßt: Die Europäischen Revolutionen und der Charakter der Nationen – Der Proletarier hat keine Beziehung zu seiner Arbeit. Proletarier ist er nicht als Schmied, sondern als Lohnempfänger. Tue ich alle Schmiede zusammen, so entwickle ich in ihnen ein Berufsgefühl, das die rein negative proletarische Haltung notwendig durchkreuzt. Kein Land mit einer wirklich durchindustrialisierten Gesellschaft bietet daher Chancen für den Marxismus.
In einem anderen Artikel derselben Nummer der Zeit: Eine totale Gegenwelt, zwei Bücher über die Behandlung von Geisteskranken entmystifizieren die Heilanstalt von Hans Krieger werden zwei Bücher besprochen, die den deutschen Leser aus den Vereinigten Staaten mit zehnjähriger Verspätung erreichen.

In dem einen meint Thomas Szasz, ein Psychiater: Krankheiten sind Ereignisse der Objektwelt. Nur der Körper kann daher krank sein in einem nichtmetaphorischen Sinne, die kranke Seele ist ein Wortspiel wie die kranke Währung oder die kranke Ruhrkohle. Der sogenannte Geisteskranke zahlt die Zeche einer sozialen Kommunkationsstörung.
In dem anderen Buch Asyle – Über die soziale Situation psychiatrischer Patienten und anderer Insassen nennt Erving Goffmann die psychiatrischen Anstalten totale Institutionen wie Zuchthäuser, Kadettenanstalten, Konzentrationslager oder Klöster – Einrichtungen, die dadurch charakterisiert sind, daß sie das Leben ihrer Insassen einer lückenlosen Reglementierung und Kontrolle unterwerfen. Er kommt zu dem Schluß, die wahren Klienten seien draußen.
In der Schrift Angewandte Seelenkunde, zu der Rosenstock-Huessy bemerkt: Sie ging 1916 als Sprachbrief an Franz Rosenzweig zur Abwehr aller Sprachphilosophie, und so ist sie die älteste Urkunde eines Sprachdenkens, in dem die Epoche von Parmenides bis Hegel ausgeschieden ist, stehen folgende Sätze, die ein Licht auf Erkrankungen der Seele werfen:
Es wird die erste Folgerung aus dieser grammatischen Grundlegung sein müssen, die Sprache der Arbeitskreise, aller Lebenskreise überhaupt, als diagnostisches Mittel für die soziale Therapie zu verwenden. Die Logik deckt Erkenntnisirrtümer der Vernunft auf. Die Mathematik vermag Sinnestäuschungen aufzuklären. Die Ur-Grammatik hat das Organon zu werden, die Eigenart der seelischen Lücken der bestehenden Gemeinschaften und Einzelnen zu entdecken und zu beheben oder doch in ihren Folgen zu lindern. Die Lücke ist die Erkrankungsform des seelischen Lebens.

4000 Stichwörter zur Geschichte enthält das eben erschienen dtv- Wörterbuch zur Geschichte von Konrad, Fuchs, Professor in Mainz, und Heribert, Raab, Ordinarius in Fribourg; von vornherein haben sie auf den umfangreichsten Teilkomplex verzichtet: auf Personen. Keiner der an diesem Abend bereits Genannten kommt oder käme in diesem Wörterbuch vor, dafür findet man Artikel zu Paradies, Paragium, Paragraph, Paränese, Paraph, Parasange, Paraskene, Paraveredi, Parcham, Parentalien, Parentel, Paria, Pariage!

Immerhin, davon abgesehen – die Tageszeitungen brachten uns die Nachricht:
der Kölner Stadtanzeiger, unter der Sparte Kultur Eugen Rosenstock-Hüssy ist tot, die kalte Notiz des Arztes, der das nur feststellen muß; er war denn auch nur dies und das;
die Frankfurter Allgemeine Zeitung Rosenstock-Huessy gestorben nennt als das Hauptwerk Die europäischen Revolutionen von 1931 – das im selben Atemzug als umfassende Geistesgeschichte Europas vom Mittelalter bis zur Gegenwart bezeichnet wird. Gegenwart? 1931? fragen wir da;
die Süddeutsche Zeitung trägt anteilnehmender all das zusammen, was man alles so wissen muß als Leser des Feuilletons:

**Eugen Rosenstock-Huessy gestorben**

Der deutsch-amerikanische Philosoph, Rechtshistoriker und Soziologe Professor Eugen Rosenstock-Huessy ist im Alter von 84 Jahren in Norwich/Vermont gestorben.
Vor dem Ersten Weltkrieg, an dem der gebürtige Berliner über vier Jahre lang als Soldat teilnahm, promovierte er an der Universität Heidelberg zum zweifachen Doktor und war später der jüngste deutsche Privatdozent.

Von 1921 bis 1923 leitete er in Frankfurt die „Akademie der Arbeit” und ging 1923 als Ordinarius für Rechtsgeschichte an die Universität Breslau.\
In Schlesien organisierte er einen freiwilligen Arbeitsdienst; seine theologischen Interessen brachten ihn außerdem in den zwanziger Jahren in Berührung mit Franz Rosenzweig, Martin Buber, Florens Christian Rang und einigen späteren Mitgliedern des deutschen Widerstandes im „Kreisauer Kreis”.\
1933 kündigte er seinen Breslauer Lehrstuhl auf und erwirkte sich in Berlin die legale Auswanderung in die USA.\
Für einige Zeit lehrte er in Harvard und an anderen amerikanischen Universitäten, wurde aber vor allem bekannt durch seine Arbeiten zur Erwachsenenbildung; später war er Vizepräsident der Weltvereinigung für Erwachsenenbildung.

Von seinen zahlreichen Veröffentlichungen in deutscher und englischer Sprache, darunter eine zweibändige, theologisch orientierte Sprachphilosophie mit dem Titel „Die Sprache des Menschengeschlechts” wurde vor allem das Buch „Die europäischen Revolutionen und der Charakter der Nationen” (1931) bekannt, eine Geistesgeschichte des Abendlandes vom Mittelalter bis zur Gegenwart.\
Rosenstock-Huessy war Ehrendoktor der Universitäten Münster und Köln und übte neben seiner wissenschaftlichen auch viele praktische und sportliche Tätigkeiten aus: Nach dem Ersten Weltkrieg gab er bei Daimler-Benz die erste Fabrikzeitung heraus, züchtete Pferde auf seiner Ranch in Vermont und bestieg 1946 als erster mehrere Berggipfel in British-Columbien. SZ

#### 2. DAS RIESENRAD
Ost und West – wie selbstverständlich gingen uns die Himmelsrichtungen bei den Themen zu dem Politischen Forum und dem Wirtschaftsforum in dieser Reihenfolge ein; noch bei unseren Spielen mit Politik folgen wir den Gestirnsbeobachtungen, die sagen: Orient und Occident.
Und dabei – es könnte sein – sitzen wir längst auf einem anderen Riesenrad, quer zu der Sonne, über beide Pole laufend, von Nord nach Süd, quer zur Natur, zur Schwerkraft, zur Einsinnigkeit – indem wir zum Beispiel nicht der Sonne nach, sondern über den Nordpol nach Japan fliegen; oder wie in dem Riesenrad Wunderwind, das hier nebenan, in seinen sechzehn Gondeln die zu kleinen Besatzungen gruppierten Sphären-Willigen von Nord nach Süd, der Mittagssonne entgegen, schleuderte.

Manchmal mag sich Eugen Rosenstock-Huessy wie der Konstrukteur eines solchermaßen orientierten Riesenrads vorgekommen sein, mit seinen zahlreichen Büchern.
Und doch: Aschermittwoch, auch der Aschermittwoch der Künstler, ist vorbei, und uns interessieren wieder in erster Linie die Breitenkreise, die Orte gleicher Entfernung zum Äquator, und weniger die Meridiane, die Orte gleicher Zeit.

#### 3. ANDERE GONDELFAHRER
Und um Ihnen nicht den Eindruck zu lassen, ich säße mit der Trauer allein in der Gondel solches unglaubwürdigen Riesenrads, möchte ich Stimmen anführen, die schon vorher für Eugen Rosenstock-Huessy laut oder eher leise geworden sind:

1955 schrieb Walter Dirks in dem Vorwort zu der Schrift Der unbezahlbare Mensch:
Es ist verwunderlich genug: man muß Eugen Rosenstock-Huessy in Deutschland vorstellen, und die Angaben des Titelblatts genügen dazu nicht.
Zu dem Werk schrieb er:

Dieser Denker geht nicht systematisierend vor oder das Gelände abtastend und sichernd, sondern kühn und provozierend, im sicheren Sprung; die erschlossene Sprache, die studierte und erschlossene Geschichte und die leibhaftige Erfahrung wirken zusammen.
An die Leserschaft wandte sich Walter Dirks mit der Mahnung:
Er sollte in seinem Vaterland nicht mehr nur von den Menschen gehört werden, deren Aufnahmeorgane auf seine spezifische Wellenlänge ansprechen, sondern endlich von der Nation.
Daß dies nicht geschehen ist, erlaube ich mir hinzuzufügen, ist ein Zeichen dafür, daß es die Nation als hörendes Organ nicht mehr gibt.

1958 erschien in dem Buch Das Geheimnis der Universität, wider den Verfall von Zeitsinn und Sprachkraft, Aufsätze und Reden aus den Jahren 1950 bis 1957, herausgegeben und eingeleitet von Georg Müller, ein Beitrag Kurt Ballerstedts, der zum Beispiel 1930 einen Aufsatz mit dem Titel Neue studentische Erziehung als Aufgabe der Erwachsenenbildung in Studentenwerk, Zeitschrift der studentischen Selbsthilfearbeit veröffentlichte.
In dem Beitrag heißt es:

Wie sehr lähmt so viele von ihnen – nämlich Akademiker – der Gedanke, unsere Zeit sei durch einen Abbau der überlieferten geistigen Ordnung, einen Verfall des Glaubenslebens, eine Schrumpfung des Geschichts- und Kulturbewußtseins, kurz durch die Merkmale eines allgemeinen Niedergangs gekennzeichnet. Einer solchen Mattigkeit hilft kein „studium universale” und keine politische Wissenschaft auf. Hier bedarf es einer Lehre, in der die ungeheuren, die Fassungskraft einer einzelnen Menschenseele weit überfordernden Erfahrungen aus zwei fürchterlichen Kriegen und aus der Bedrohung unseres Geschlechts durch die Sprachlosigkeit geistig bewältigt und an die kommende Generation weitergegeben werden.

Wichtig an Ballerstedts Beitrag ist auch, daß er als Bestandteil der Biographie die Namen der Freunde anführt: Franz Rosenzweig, Joseph Wittig, Carl Dietrich von Trotha, Horst von Einsiedel, Helmuth James Graf von Moltke, Hans Ehrenberg, Ernst Michel, Karl Muth, J.H. Oldham, Ambrose Vernon, Georg Müller, Franz Schürholz.
1970 schrieb der Dichter Wystan Hugh Auden das Vorwort zu dem Buch I am an impure thinker und beschloß es mit dem ermutigenden Satz:
Speaking for myself, I can only say that, by listening to Rosenstock-Huessy, I have been changed.

Das erste Buch in dem für diesen Zweck 1969 gegründeten Verlag, Rosenstock-Huessys Speech and Reality, leitete der Herausgeber Clinton C. Gardner ein. Der letzte Absatz lautet, ins Deutsche übersetzt:

Möge der Leser als Teilnehmer herausgerufen werden, erregt, wie zwei Generationen derer, die Rosenstock-Huessy kennenlernten: wahrnehmend, daß etwas Neues hier geboren wird. Gerade weil es neu ist, mag es entweder zu schwierig oder zu naiv scheinen. Aber selten erhält ein Leser Gelegenheit, an einer noch unerkannten Entdeckung Anteil zu nehmen. In dem Ausmaß, wie Sie das tun, werden Sie Partner bei der Erschließung einer neuen Wissenschaft.
10

#### 4. LEBENSWAGNIS ALS MASSTAB DER GLAUBWÜRDIGKEIT
Von der Generation, die nach 1945 auch die Erwachsenenbildung zunächst in den Besatzungszonen, dann in der Bundesrepublik wiederaufgebaut hat, ist die Arbeit der Erwachsenenbildner während der Weimarer Republik zunächst wohl stillschweigend aufgenommen, dann stillschweigend ignoriert und schließlich stillschweigend, nämlich über den Ernst, wieder gelobt worden.
So ist es schon erstaunlich, daß 1970 in den Frankfurter Beiträgen zur Pädagogik eine Studie von Ulrich Jung: Eugen Rosenstocks Beitrag zur deutschen Erwachsenenbildung der Weimarer Zeit den lesefreudigen Erwachsenenbildnern den Namen, den Mann und sein Wirken in Erinnerung bringt.
Die Position des Autors freilich geht aus folgenden Sätzen des Schlußkapitels hervor:
Eugen Rosenstocks Beobachtungen zur Anthropologie des Erwachsenen und die daraus abgeleiteten Forderungen an die Methoden der Erwachsenenbildung, die in der vorliegenden Arbeit zusammengestellt wurden, dürften seinen bleibenden Beitrag zur Theorie der Erwachsenenbildung ausmachen. Von besonderem Interesse erscheint dabei seine Einsicht, daß die erkannte potentielle Bildsamkeit des berufstätigen Erwachsenen zu ihrer Aktualisierung der Herausforderung bestimmter Situationen bedarf, denn sie wird im Unterschied zu derjenigen des Kindes von sich aus durch kein natürliches Spannungsverhältnis zu einem erst noch zu erreichenden Status begründet …

Zeigt sich hier die Aktualität mancher Gedanken Rosenstocks, so muß seine ablehnende Haltung gegenüber der rationalen Wissenschaftlichkeit ihn in Distanz zu einer Gegenwart rücken, was ist Gegenwart? müssen wir fragen – die wissenschaftliches Denken und rationale Kontrolle in allen Lebensbereichen zur Geltung bringen will. So hat die moderne Erwachsenenbildung ein neues Verhältnis zur Wissenschaft gefunden und orientiert sich in doppelter Hinsicht an der wissenschaftlichen Rationalität: zum einen hat die systematische Wissensvermittlung neue Bedeutung gewonnen, zum anderen kann sich die Erwachsenenbildung zur Erforschung ihrer eigenen Voraussetzungen und ihrer Wirksamkeit mehr und mehr auf empirische wissenschaftliche Untersuchungen stützen.

Meine Damen und Herren!\
Umständlich ist der Weg, den ich mit Ihnen bisher gegangen bin.
Und es sind noch immer nicht Umstände genug; denn was Rosenstock-Huessys Leben als Dienst auf dem Planeten bedeutet, dazu müssen wir erörtern, was Franz Rosenzweig 1916 im Zuge der vorhin schon erwähnten Korrespondenz ausgesprochen hat:

Es gibt im Leben alles Lebendigen Augenblicke oder vielleicht nur einen Augenblick, wo es die Wahrheit spricht. Man braucht also vielleicht überhaupt nichts über das Lebendige sagen, sondern man muß nur den Augenblick abpassen, wo es selber sich ausspricht. Den Dialog aus diesen Monologen halte ich für die ganze Wahrheit.

Um an diesen Augenblick zu gelangen, muß ich noch einmal, etwas weniger exzentrisch ansetzen.
In einer Monographie Martin Buber und das deutsche Judentum, 1963 mit anderen Monographien unter dem Titel Deutsches Judentum; Aufstieg und Krise; Gestalten, Ideen, Werke in der Deutschen Verlagsanstalt erschienen, schildert Ernst Simon, derwie ja auch Martin Buber!Gast der Volkshochschule Köln gewesen ist:

Unterrichten – nicht nur als „Vortrag” – lernte Buber in Rosenzweigs Frankfurter „Freiem Jüdischen Lehrhaus”. Dieser hatte sich vorgenommen, ihn in Frankfurt das Sprechen „richtig” zu „lehren”. Bei dem nächsten Besuch in Heppenheim tat er mehr: er veranlaßte Buber, ihm und seiner Frau eine „Probelektion””in der Interpretation chassidischer Quellenstücke zu geben, nach denen ihn bisher in all den Jahren „nur einmal jemand gefragt habe”. Buber stellte sich damals noch „ziemlich ungeschickt” an, aber es war eben ein Anfang, und ein sehr fruchtbarer. Aus diesem Keim hat sich nicht nur seine zentrale Arbeit im „Lehrhaus” selbst entwickelt, sondern auch seine führende und tragende Rolle in der von ihm 1933 begründeten „Mittelstelle für jüdische Erwachsenenbildung bei der Reichsvertretung der deutschen Juden” und in dem von ihm ausgehenden geistigen Widerstand gegen die national-sozialistische Tyrannei.

Ernst Simon hat zusammen mit Edith Rosenzweig die Briefe Franz Rosenzweigs 1935 herausgegeben; darin ist auch die Korrespondenz zwischen Rosenstock-Huessy und Rosenzweig von 1916 vollständig abgedruckt.

Warum ist in Ernst Simons Monographie der Name Rosenstock (-Huessy) nicht zu finden? Denn sollte Rosenzweigs Vorhaben, ihn das Sprechen richtig zu lehren, nicht Frucht der Begegnung mit Rosenstock sein, wo dieser noch in seinem Hauptwerk Die Sprache des Menschengeschlechts 1963 schreibt:

Denn während Franz Rosenzweig (der auf meine Srpachlehre von 1916 aufbaute, wie er gewissenhaft betont hat), und Ferdinand Ebner, beide alles Sprechen dem Walten des einen Geistes schon 1920 zurückgaben, schien Bubers „Ich und „Du an der akademischen Einteilung der Gegenstände des Nachdenkens nicht zu rütteln.”

\- und wo der Untertitel der genannten Aufsatzsammlung: Gestalten, Ideen, Werke an eine 1925 verfaßte Schrift Eugen Rosenstocks erinnert: Ichthys, Leben – Lehre – Wirken.

Warum verschweigt Ernst Simon Eugen Rosenstock-Huessys Beitrag? Weil er nicht zum deutschen Judentum gehört hat?

Die Sätze Franz Rosenzweigs werden zur Erkenntnismethode, wenn bestimmbar ist, wann dieser Augenblick ist, zu dem das Lebendige sich selber ausspricht; wenn jenes Abpassen nicht nur rein zufällig passiert, von Augenblick zu Augenblick, sondern auf längere Sicht, auf die Sichtweite der ganzen Lebenszeit des beachteten Legendigen.

Wie gefährlich für jemanden, der diese Offenbarung Franz Rosenzweigs in dieser Richtung zu einer Methode präzisiert – weil sie sich an dem eigenen Leben bewähren muß: derjenige, der dieser Methode, dieses Weges innegeworden ist, müßte ja wohl wann an solch offenem Fleck anzutreffen sein: und es im voraus oder späterhin selber auch wissen!
Welch eine Versuchung, die Wahrheit des Lebens vorsätzlich zu verfälschen! Vielleicht sogar in guter Absicht!

Wie gefährlich aber auch, mit dieser Lehre etwa jenen Augenblick bewußt verstreichen zu sehen oder den rechten Zeitpunkt zu verpassen!
Wer spricht, wird abgewandelt – das ist der Hauptsatz der Lehre Rosenstock-Huessys. So naiv, so einfach das klingen mag, es findet statt nur unter ernsten Bedingungen:

1. der Hörer dieser Sprache muß ein bestimmter und ein zu Bestimmender sein;
2. der Sprecher muß sein eigener Hörer werden und vernehmen, was er sagt, und sich daran binden;
3. der Sprecher kann es nur einmal so sagen; dabei kann dieses „einmal” unter Umständen aus verschiedenen Anlässen, Wiederholungen bestehen;
4. der Sprecher muß bereit sein, als Antwort etwas der eigenen Vorstellung Widersprechendes zu hören und es dennoch als Antwort zu akzeptieren.

Prüfen wir, meine Damen und Herren, diese Bedingungen am heutigen Anlaß:
1. Sie muß ich anreden als Interessierte am Geschick unsere Planeten, als Engagierte in der Erwachsenenbildung, als Mitbürger, als Hörer, für die der Zeitpunkt des Todes Eugen Rosenstock-Huessys ein Einschnitt werden soll, an dem Sie mit mir ein Davor und Danach unterscheiden; deshalb – Sie haben es wohl längst gemerkt – halte ich keinen Vortrag über den Dienst auf dem Planeten, oder ein Referat über die 1965 erschienen Schrift Rosenstock- Huessys.
2. Selbstverständlich ist in unserer Öffentlichkeit die Regel, daß ich nicht nachher abstreiten kann, dies und das gesagt zu haben und auch Namen, die zu nennen nicht ungefährlich für mich sein könnte;
3. Anlaß, Ort und Vorfall dieser Rede sind nicht wiederholbar;
4. Ob ich Ihr Interesse für den mich im Augenblick bestimmenden Zeitpunkt gewinne, ist völlig offen; keine logischen Zirkel, Dreiecke und Quadrate können das verstellen, und die unpassendste Antwort eines notorischen Schläfers muß als Antwort angenommen werden, ja sie kann unter Umständen den ganzen Eindruck verderben.

Aber mit etwas gediegenerem als diesem Beweis durch Vergegenwärtigung gegenseitiger Erfahrung kann ich, meine ich, aufwarten.

Aus Anlaß des 80. Geburtstages des Autors – also im Jahre 1968, 52 Jahre, nachdem Rosenstock seinen Sprachbrief an Rosenzweig schickte – gab Georg Müller Autobiographische Fragmente Eugen Rosenstock-Huessys heraus, und darin spricht er selber, teilweise sage ich heute, jenen offenen Fleck, aus, von dem her die zahllosen Streifzüge dieses Mannes Licht erhalten, und nicht nur sie, sondern möglicherweise wir auch.

Auch ein Leben kann ein Beleg sein, und für gewissen Wahrheiten ist es vermutlich der einzige Belegsagt er, als bald Achtzigjähriger.

Und wofür führt er ein Leben als Beleg an?\
Unter welchen Bedingungen trägt eine seelische Erschütterung Frucht? Sie trägt nur Frucht, wenn ihr Träger als Dich, Ich, Wir, Er durchkonjugiert wird.

Ehe ich berichte, wie Rosenstock-Huessy diese Frage und diese Antwort mit seiner
Liebesgeschichte mit der deutschen Sprache
als Muttersprache, als Brautsprache, als Ehe- und Arbeitssprache
belegt, möchte ich beides auf diesen Abend, auf die von Rosenstock-Huessy Betroffenen anwenden:
DICH – in diesem grammatischen, das heißt: sprachlich bestimmten Aggregatzustand, das heißt: Art, geschart oder scharungsfähig zu sein, sind alle, die sich dem Verwirrenden aussetzen, das von diesem Mann widersprüchlich ausgegangen ist, mich selbst betraf diese Lehre vor 15 Jahren, als ich 16 Jahre alt war;

ICH – in diesem grammatischen Aggregatzustand befindet sich bei einem Vortrag nur der Sprechende: ihn drängt es, etwas ihn noch Bedrängendes schon mitzuteilen; seine Anteilnahme, seine Begeisterung tritt befeuernd oder vereisend zwischen den Ruf und den weiteren Hörer;

WIR – falls Sie, meine Damen und Herren, und ich uns entschließen können, die Erwachsenenbildung nach 1945, nach 1918 als gemeinsam durchgemachte oder nachträglich noch wieder durchzumachende Geschichte anzunehmen, dann verkörpern wir in diesem grammatischen Aggregatzustand mit die Institution Volkshochschule Köln;
ER – so redeten die Zeitungsmeldungen von dem Verstorbenen.
Nun aber der Beleg:

Eugen Rosenstocks Beitrag zur deutschen Erwachsenenbildung der Weimarer Zeit wurde geleistet, als er als Träger einer seelischen Erschütterung als WIR konjugiert wurde, also die Stationen DICH und ICH schon durchgemacht hatte.\
Dieser Satz bedarf wohl eingehender Erläuterung.
Folgende Fragen sollen uns an den springenden Punkt bringen:

Was für eine seelische Erschütterung ist gemeint?
Wie sahen die Stationen DICH und ICH aus?
Wie beleuchtet die Situation ES, die darauf folgte, die Station WIR?
Die erste Frage: was für eine seelische Erschütterung ist gemeint? Was für ein
EREIGNIS?
Es war ein Satz, der wenig revolutionär klingt:
Die Sprache ist weiser, als der, der sie spricht.
Die zweite Frage: wie sahen die Stationen DICH und ICH aus? Die erste Station:
Seit 1902 hat Rosenstock-Huessys bewußtes Leben unter dem Kennwort Sprache gestanden. In seiner Habilitationsschrift Ostfalens Rechtsliteratur unter Friedrich II. von 1912 steht der Satz: Der Eigenname wirkt als Imperativ. Das ist eine Entdeckung; sie bedeutet: statt bloßer Worte, die Dinge zu bezeichnen, besteht die Sprache zuerst aus Namen, die uns etwas zu vollziehen heißen, Namen sind gegenseitige Anrufungen zur Ordnung des Gemeinschaftslebens.
Die Erfahrung beim Heer 1910-1912 vertiefte die Überzeugung, daß die Akte Gehorsam und Befehl als Grundverhältnisse der Sprache Sinn geben. Ein Element des Dienstes wollte Rosenstock dem Denken zurückerobern und warf 1911/1912 den Ästheten und Intellektuellen und der ganzen akademischen Welt überall, den Heidelbergern, die sich über russische
Revolutionäre und englische Settlements zuschauerhaft unterhielten, die freiwilligen Arbeitslager als deutsche Lösung entgegen.

Auf den innern Höhepunkt seiner akademischen Laufbahn gelangte Rosenstock-Huessy mit seinem Buch Königshaus und Stämme in Deutschland von 1014 bis 1250 aus dem Jahre 1914, neu aufgelegt 1965. Als Motiv steht vor diesem Werk das Wort des Sokrates an Kriton im Gefängnis vor der Hinrichtung:

Gesetz und Recht und Gerechtigkeit, das sind Worte,
die so laut in meinem Innern dröhnen und widerhallen, daß ich vor ihrem Klang nichts anderes vernehmen kann.

Und dazu Goethes Vers:\
Wie das Wort so wichtig dort war, weil es ein gesprochen wort war.

Die zweite Station oder: wie wurde aus dem Ereignis EIGENSCHAFT?

An diese Stelle gehört die Freundschaft zu Franz Rosenzweig. Rosenstock schreibt:
Es gibt also seit den Jahren 1913 bis 1923, die wir gemeinsam verbracht haben, eine doppelte Lehre von der Sprache; die eine ist die von mir vorgelegte und die andere ist die von Rosenzweig dialektisch dazugesetzte.

Als dritte Lehre nennt Rosenstock die von Martin Buber vertretene, mit der er als Idealist den Existenzialisten wie Heidegger und Sartre, obwohl diese von der Sprache nichts wußten, am nächsten steht. Das Wichtige daran:

für die Lehre von der Sprache und von der Zeit gibt es kein System im alten Sinne, gibt es keine alleinseligmachende Wahrheit. Trotzdem ist es notwendig, von ihnen beiden die Wahrheit zu sagen. Man kann das aber nur in der bestimmten Sprache, die dem Alpha, dem Omega oder der Mitte des Zeitstromes entspringt. Der französische Existenzialist ist bloß Alpha, bloß ewiger Anfang seiner Zukunft … Die entgegengesetzte Sprache tönt vom Ende, aus der Ewigkeit eines Volkes von Priestern, des wahren Israel, welches eifersüchtig die Einheit aller Zeiten bewacht …
Als drittes der Mittelpunkt des Heute, an dem ein Stück ewigen Lebens in die Zeiten einbricht, als das die Zeiten und Räume neu einrichtende Kreuz der sich wandelnden Zeiten und der verwandelten Räume.

1915 – als Frontsoldat – entwarf Rosenstock Reden einer imaginären Ritterschaft St. Georgs, deren Adressaten und Unterredner waren: die Kriegsteilnehmer aller Länder; 1917, am 1. Februar, als genauer Zeitgenosse der russischen Revolution, konzipierte er die Europäischen Revolutionen, die Korrespondenz der europäischen Großmächte in ihren Revolutionen, die Revolutionssymphonie, wie er es nennt, die den Zusammenhang des Jahrtausends von Gregor VII. bis zu Lenin blickfrei räumt.

1918 nahm er ausdrücklich, in dramatischer Zuspitzung, Abschied von Preußen, Bayern, Sachsen, Nation; Kirche, Wissenschaft, nationalem Mythos, der Spaltung in Rom und Wittenberg, und vom Europa der Renaissance: ihm wurde angeboten,
als Unterstaatssekretär die neue Verfassung aufzuzeichnen; beim katholischen Organ „Hochland” mitzuarbeiten;
eine Professur in Leipzig anzunehmen –\
Rosenstock ging zu den Daimlerwerken in Stuttgart, wo gerade 18 000 Arbeiter streikten.
Als menschlicher Mittelpunkt aller sozialen Fragen unserer Zeit stellte sich für ihn, auch am eigenen Leibe, der Arbeitslose heraus, vor dem der Arbeiter Angst hat! – der aber muß angehören und Freunde finden können.

Er schreibt:\
Mit den Fabrikanten und Ingenieuren in der Fabrik, aber auch mit den geistigen Freunden, mit Ernst Michel, mit Richard Koch, mit Werner Picht, mit Joseph Wittig, mit Carl Dietrich von Trotha habe ich öffentlich Duett gesungen.

Unsere dritte Frage lautete: Wie beleuchtet die Station ER, die auf das WIR folgte, Rosenstocks Beitrag zur Erwachsenenbildung der Weimarer Zeit?
In der Bibliographie der Schriften Rosenstock-Huessys wird als letzte in deutscher Sprache gedruckte Veröffentlichung vor 1947 der Artikel Arbeitslager im Handwörterbuch des deutschen Volksbildungswesens, Breslau 1933 genannt; 1948 erschien, wohl in der ersten Nummer der Wochenzeitschrift Christ und Welt ein Artikel über Ernst Michel, den barmherzigen Samariter des Denkens, am 5. Juli 1950 hielt Rosenstock- Huessy auf Einladung von Rektor und Senat der Universität Göttingen die Rede Das Geheimnis der Universität –
in der Zeit von 1933 bis 1947 sind nur zwei umfänglichere Veröffentlichungen in englischer Sprache vermerkt: Out of Revolution, Autobiography of Western Man, das Buch Die Europäischen Revolutionen, konzipiert 1917, in Deutschland 1931 gedruckt, neu geschrieben für amerikanische Leser, veröffentlicht als gerettetes geistiges Gut in dem Jahre 1938,

als Paula Rosenstock, die Mutter Eugen Rosenstock-Huessys, durch Segensspruch die Enteignung des Wortes, die Entwortung, wie sie in den Konzentrationslagern verübt wurde, überwand, ehe sie, aus ihrer Wohnung im 80. Jahre verwiesen, vom Konzentrationslager bedroht, in Ruhe von ihren Angehörigen und dem irdischen Leben schied;

die zweite Veröffentlichung The Christian Future or The Modern Mind Outrun erschien 1946 in New York, 1947 in England, 1955 in München Des Christen Zukunft oder: Wie überholen die Moderne.
Meine drei Jahrzehnte, in denen ich als zukünftiges DICH, gegenwärtiges ICH, wiederkehrendes WIR leben durfte, wurden im vierten Jahrzehnt erledigt. Die Welt entledigte sich ihrer.
Ist es hier ausgesprochen, warum z.B. Ernst Simon und Die Zeit Rosenstock-Huessy verschweigen und warum Ulrich Jung mit seiner Darstellung und Themenstellung das Leben der Weimarer Zeit erreicht, nicht aber mit den distanzierenden Sätzen? Doch wohl.
Wir können dorthin nur gelangen, indem wir unsererseits die Entwortung (ENTEIGNUNG) 1933-1945 als objektiven Tatbestand anerkennen, durchmachen und durch neue Anrede überwinden lassen, indem wir, mit anderen Worten,
den Mißerfolg der Jahre von 1923 bis 1933
als vorläufiges Versagen nehmen, selber das Ungereimte zu reimen versuchen, unter genauso radikalem Verzicht auf garantierten Erfolg.
Damit sind wir, meine Damen und Herren, an den springenden Punkt gelangt:
Der Kreisauer Kreis von 1940 bis 1945 ist die Frucht des Arbeitslagers; es ist seine Leistung, daß er das Intermezzo Hitlers als Intermezzo begriff. Helmuth James von Moltke und Horst von Einsiedel waren dank der von Eugen Rosenstock-Huessy ausgetragenen seelischen Erschütterung, die die Gefahr der Entwortung vorausnahm, der Nazizeit voraus!
Was hat das mit Dienst auf dem Planeten zu tun?
18

#### 5. DIENST AUF DEM PLANETEN
Hier,
im Forum der Volkshochschule Köln, in einem der Vortragssäle
– meine Damen und Herren, wenn wir den Ort genauer bestimmen wollen, wieviele Grenzen ziehen wir, um so mehr, je genauer wir uns ausdrücken wollen. Der kleinste Raum ist unter uns der bestimmteste! Grenzen brauchen wir für das stimmhafte Gespräch.
Aber Grenzen hindern und trennen ja auch. Und bei einiger Besinnung wird es bald einleuchten, daß Grenzverschiebungen nur als Ergebnisse kriegerischer Auseinandersetzung stattgefunden haben, stattfinden: nur ein Krieg würde die Mauer in Berlin beseitigen; in Vietnam geht es nach wie vor um Grenzverschiebungen, in Israel, in Irland.
Da aber der Krieg abscheulich-unmöglich geworden ist – wie ungemütlich, nicht ohne Grenzen auskommen, sie aber nicht mehr verändern zu können!
Was muß geschehen, damit wir sagen können: HIER, AUF DEM PLANETEN?
Jetzt,
am Abend des Freitags dieser Woche nach dem Sonntag Invokavit, dem ersten Fastensonntag (vorbei der Trubel mit Riesenrad und Rosenmontagszug),
am 16. März 1973, im ersten Jahr der zweiten Regierung Brandt, im vierundzwanzigsten Jahr der Bundesrepublik Deutschland,
im 28. Jahr nach der bedingungslosen Kapitulation in Reims und Karlshorst,
40 Jahre nach der armseligen Phrase von der Machtergreifung
– alle näheren Bestimmungen des uns gemeinsamen Zeitpunkts fassen immer längere Strecken zusammen und versetzen uns nach und nach in eine von jenseits unserer Geburt herströmende Zeit.
Aber welch ein Unterschied zu sagen: die Nazizeit war oder die Nazizeit ist oder ist noch!
19

Wie kommt es, daß wir sagen können: die Nazizeit ist nicht mehr,
jetzt,
auf dem Planeten?

Nicht nur eine solche Grenzziehung gibt es in der Zeit; getrennt sind die Generationen, die Nationen, die Völker mit vorchristlichen Ordnungen der Gemeinschaft und des Lebensweges; von allen diesen Parteien wird wenigstens des Exotischen an den anderen Parteien genossen.
Und wer heißt exotisch? Der von draußen Gesehene. Ging man vor hundert Jahren in die Weltausstellung, all das Fremde zu erstaunlichem Reiz zu nutzen, oder in den Zoo, hinter Gitterstäben den Panther zu sehen:

Ihm ist, als ob es tausend Stäbe gäbe\
und hinter tausend Stäben keine Welt – (Rilke)

wir können in Kleinbildformat tagtäglich Weltausstellung vom Sofa aus haben. Die Welt von draußen; gleich hat es Stimmen gegeben, die zum Ausdruck brachten: schamlos ein lebendiges Wesen von draußen anzusehen: esoterische Gruppen haben sich gebildet, die keinen Strahl der neugierigen Öffentlichkeit in den in geheimnisvolles Dunkle getauchten Zirkel fallen lassen.
Wie also wird für uns die Nazizeit einsichtig?dieser gigantische, gegen jeden Einblick von außen abgeriegelte Hexenkessel?

Denn erst kraft solcher Einsicht, die die tausend Stäbe durchdringt und zerbricht, könnten wir sagen:

Die Nazizeit war. Jetzt – darein ist sie nicht mehr zu mischen!?
Es ist die Arbeit Eugen Rosenstock-Huessys in der dritten Phase, in welcher er als Träger der schon namhaft und stimmhaft gemachten seelischen Erschütterung als WIR konjugiert, also zusammen unter ein Joch getan wurde, daß uns solche Einsicht möglich ist.
Was ist von den Arbeitslagern zu erzählen, durch die die seelische Erschütterung zum vererbbaren
EIGENTUM
Rosenstock-Huessys geworden ist?

1927 kam der junge Graf Moltke voller Erregung über die Not im Waldenburger Bergland zu Rosenstock-Huessy und fragte um Rat. Im September trafen sich Mitglieder der schlesischen Jungmannschaft mit ihm und einigen anderen Hochschullehrern bei Moltke in Kreisau: Unternehmer, Landadel, sozialistische und christliche Gewerkschafter, Beamte, Professoren, katholische und evangelische Geistliche, Lehrer und Jugendliche, und bildeten die Löwenberger Arbeitsgemeinschaft, später Arbeitskreis Schlesien genannt, mit acht Arbeitsausschüssen, deren Wirken ein Arbeitslager vorbereiten sollte. 1926 hatte Rosenstock-Huessy mit Hilfe Robert von Erdbergs das Boberhaus in Löwenberg – Löwenberg liegt auf derselben geographischen Breite wie Köln – als Schulheim der schlesischen Jungmannschaft gegründet.

Die Arbeitslager dauerten drei Wochen, mit etwa 100 Teilnehmern, Arbeitern, Bauern, höchstens einem Drittel Studenten im Alter von 18 bis 25 Jahren: Kommunisten, Stahlhelmleute, Christen, Atheisten, Besitzersöhne, Landarbeiter, adlige und kleinbürgerliche Studenten, national jugendbewegt oder aus katholischen Verbindungen, Forstleute, Technische Hochschüler, Juristen, Theologen, Volkswirte, Philologen, Bergarbeiter, Textilarbeiter, ungelernte Arbeiter, Handwerker.

4 Stunden vormittags, auf keinen Fall aber mehr als 6 Stunden, dienten körperlicher Arbeit, Holzhacken, Aufräumungsarbeiten, Erd-, Garten- und Feldarbeit, nicht im Sinne des billigen Phrasenrauschs, es handle sich um Ausgleich von Hand- und Kopfarbeit, sondern weil vor der wirklich nutzbringenden Arbeit Geschwätz nicht Bestand hat. Die übrige Zeit war zu geistiger Tätigkeit als ursprünglicher Energiebildung, das heißt der Kraft, trotzdem man sich nicht rational versteht, geschweige denn einer Meinung ist, miteinander zu leben, und zwar in fünf Geistesstufen:

1. Die Beurteilung der gemeinsamen Arbeitserfahrungen: Der Sinn des Lagers ist seine Selbstverwaltung – alle Angelegenheiten des Lagers sollen vom gesamten Lager geregelt werden. Der Lagerrat behandelte alles, auch die Gegensätze innerhalb der Lagerleitung und die Meinungsverschiedenheiten mit rückhaltloser Offenheit.
2. Die Erzählung der Lebensläufe der Teilnehmer, daß sie einander mit anderen Augen sehen und zu einer menschlich offenen und freien Haltung gelangen, die dann entsteht, wenn die Übersetzung des Lebens in Sprache möglich wird. Für diese Geistesstufe wurden Gruppen zu 20 bis 25 Teilnehmern gebildet.
3. Die Beschäftigung mit unmittelbar drängenden sozialen und politischen Problemen, z.B. sprachen mehrere Redner in Vortragsreihen, Rosenstock- Huessy einmal zu „der Gefährdung der jungen Generation durch die Auflösung der Berufe”.
4. Die Morgenansprache des geistigen Leiters, eine Lesung aus dem Buche des Lebens, mit tagesnormierendem Sinn, hier brachte Rosenstock-Huessy Kapitel aus seiner Lehre.
5. Das Zusammentreffen mit verantwortlichen Persönlichkeiten des öffentlichen Lebens.

Meine Damen und Herren! Diese fünfte Geistesstufe, als solche für den Ernst der Arbeitslager die wichtigste, sie hatte den Namen

FÜHRERBEGEGNUNG.

Spätestens hier ist uns bemerkbar, in welch eine exotische Position unsere Sprache gelangt ist: wir stocken, eine nicht aussprechbare, also außerhalb vernünftiger Gewalt belassene Vergangenheit nimmt uns den Atem, wir lesen erschrocken im Oxford Dictionary, konzis wie es ist, zusammenschneidend auf das Wesentliche:
Führer, Leader, in Klammern groß G, German.

Hier sind wir an eine Grenze gestoßen, die uns hindert zu sagenL
hier, auf dem Planeten,\
wir bestätigen sie, in dem wir aufhören, rückhaltlos miteinander zu sprechen.
Fangen wir an dieser Stelle an, Grenze, die uns hindert zu sagen: Die Nazizeit war, zu beseitigen, indem wir miteinander sprechen, als hätten wir keine Geheimnisse voreinander. Etwas so Zartes und Gebrechliches wie Sprache läßt sich in Grenzen nicht einschließen.
Leader, wie Leader of House of Commons? Das kann ja nicht stimmen! Oder wie ein Dirigent, ein Laienspielleiter? Ein germanisches Wort? Und nun auch noch dieses Wort im Zusammenhang mit einem viel gefeierten: Haus der Begegnung – gibt es in Köln, Jabachstr. 4-8!
Die, die nur den Mißbrauch des Richtigen und Notwendigen sehen, sind nämlich oft selber seine Verhinderer.\
Dieser Satz Eugen Rosenstock-Huessys soll uns ermutigen, die Grenze zu überschreiten, an die wir gestoßen sind.

Was ist Führerbegegnung?\
Die Lagerbelegschaft hatte zwei oder drei Tage lang den Zusammenstoß mit Persönlichkeiten (50 bis 80) aus allen Schichten der Provinz zu ertragen, Grafen, Gewerkschaftssekretären, Professoren, Lehrern, Frauen, Bauern, Verwaltungsbeamten aller Art. Angesichts einer konkreten Notsituation trafen junge Menschen unterschiedlicher Herkunft und in Verantwortung stehende Erwachsene zusammen, um aneinander etwas zu lernen: Ernst und Spiel messen sich aneinander und kommen ins Gespräch.

Die hinzukommenden Älteren aus dem beruflichen und politischen Alltag, sie sind also die in dem Namen Führerbegegnung gemeinten Führer; mehrere, nicht einer.

Das Zusammenstoßen von abgegrenztem Spiel und fortdauerndem Alltag, das ist die in dem Namen Führerbegegnung gemeinte Begegnung.
Welch eine Bedeutung dieser Vorgang hat, geht aus einem 1926 erschienenen Aufsatz Rosenstock-Huessys hervor:

LEHRER ODER FÜHRER\
Als Thesen möchte ich einige Sätze daraus bringen:
Die Natur des Lehrers ist mehraltrig, die des Führers einaltrig; der Lehrer gehört immer mehreren Generationen an, aber keiner ganz. Der Führer gehört seiner Generation ganz an, aber keiner anderen.

Der Geist des Führers ist das Geheimnis einer einmaligen Generation, einer einmaligen, in sich vollendeten Zeugung, Geburt, Schöpfung. Nur der Lehrer, der dem Führerspielen entschlossen entsagt, kann über sich hinaus- oder an sich vorbeiweisen. Die Lehre will die sogenannten führenden Geister eines Zeitalters, genauer sagen wir: die führenden Männer eines Zeitalters mit den Generationen vor und nach ihnen zusammenbinden und verfugen.

Lehrer und Führer sind die zwei Geschlechter auf der Höhe des „Worts”.
Aber wie springt der Lehrer mit dem Führer um? Wo begegnet der Führer der Lehre?
Zwei Verhältnisse dieser Art sind heute beide verbraucht: Mönch und Ritter, Zivil und Militär. Theologe und Akademiker gewinnen im Altertum den zeitentrückten Raum, der alle Lehre vor der Verwechslung mit Politik schützt. Aber die bisherigen Gesetze des Verhältnisses von Lehre und Führung haben sich aufgelöst, indem der Weltkrieg die Führung des Schwert- und Fürstenadels erschöpft hat.

Deshalb muß die Lehre den Führer mit berufen, mit schaffen helfen, weil ihn leibliche Erbfolge nicht mehr hervorbringt. Dazu muß die Lehre sich bequemen, sich für Männer statt für Jünglinge umzustimmen, für Könige, Krieger, Häuptlinge der Politik, statt für Beamte, Richter und Pfarrer. Denn nur die Lehre, die den Träger von Aufgaben im Hörer anspricht, läutert den Traum des Jugendlichen, so daß er Kraft zur Bewährung, Kraft zur Verwirklichung gewinnt. Gültig kann nur die Lehre sein, die für Alte und Junge Sinn hat und Geltung.
23

In der Denkschrift Ein Landfrieden von 1911/12 ist schon gesagt, an welcher Stelle Rosenstock-Huessy auch an die Musen denkt, nämlich am Ende, wenn eine einheitliche unverlierbare Lebensluft besteht, eine Gemeinschaft, in der dann gute Worte und schöne Künste ihre Stelle hätten, ohne mehr die Menschen zu trennen, statt sie zu verbinden.
24

#### 6. DREI GENERATIONEN
Neue Krise in der deutschen Erwachsenenbildung, curriculum der Erwachsenenbildung, Methoden der Erwachsenenbildung, Erziehung zu einer soziologischen Denkweise – das waren Stichworte aus den Vorträgen, die, in der Sprache des Arbeitslagers von Rosenstock-Huessy, der Form nach zu der dritten Geistesstufe gehören, Beschäftigung mit unmittelbar drängenden sozialen und politischen Problemen.

Was würde eine Führerbegegnung in der Volkshochschule Köln zum Beispiel bewirken?
Sollen wir es wagen, uns dieser Frage zu stellen, mit der Perspektive, die die Frucht der Arbeitslager geschaffen hat:
Das Lager wirft über das ganze folgende Jahr des einzelnen Besuchers seinen Schatten. Es läßt ihn durch die Heftigkeit der Erfahrung nicht mehr in ruhiger Zufriedenheit vorliebnehmen mit der ihm vorgesetzten Volkshochschulspeisekarte – (ein Satz Rosenstock-Huessys aus dem Jahrbuch für Erwachsenenbildung von 1930)?

Hier,
im römischen, fränkischen, reichsstädtischen, französischen, preußischen, deutsch-reichischen, britisch besetzten, bundesrepublikanischen Köln,
an der Stelle des Bürgerhospitals,
im Forum der Volkshochschule;
jetzt,
vierzig Jahre nach der totalen Enteignung des Wortes in Deutschland
bewährt sich in meinem Lebensweg eine Lehre, die so über zwei Gräber hinweg in der dritten Generation gültig ist.
Das zweite Grab: in das warfen die Henker 1945 Helmuth James von Moltke, mit dessen Wort die Grenzen zu überschreiten sind, die uns von dem Planeten trennen:
Bringt den Hitler nicht um, daran liegt gar nichts, der muß seinen Krieg verlieren. Aber nach dem Kriege muß es Menschen geben, die sich von diesem Unrecht lossagen können, und wir müssen uns heute schon lossagen, damit uns unsere Lossagerei morgen geglaubt wird. Es wird heute eben wichtiger ein Christ zu sein als ein vom Hakenkreuz repräsentierter Deutscher. Denn in den Reichen dieser Welt hat das Heidentum abgewirtschaftet.
Das erste Grab: vor zwei Wochen in Norwich in Vermont, dem 1791 als Unionsstaat aufgenommenen, gegraben und mit Erde bedeckt.

#### 7. HAMLETS URTÜMLICHE LÄNGE
Verse, Charaktere, Monologe, das Drama im Drama – ist denn die Länge des „Hamlet” nicht noch urtümlicher?

So fragt der Shakespeare-Herausgeber Bernard Grebanier, und so zu fragen ist auch bei dem Leben Eugen Rosenstock-Huessys angebracht.

Der tiefe Sinn des Hamlet ist es ja, daß er die aus den Fugen gegangene Zeit im Einsatz seines Lebens einrenkt, damit nach seinem Sterben Leben weitergehen kann.
Diesen Satz Rosenstock-Huessys möchte ich auf sein Leben anwenden; nur, Hamlet als vierundachtzigjährigen sich vorstellen? Aber Shakespeare's Stück, das das Drama solcher Einrenkung schildert, das hat urtümliche Länge. Können Kulturträger begreifen, daß das Original eines langen Dramas, eines langen Musikwerkes, eines langen Romans ein in aller Fülle gelebtes Menschenleben ist, mit Ecken und Winkeln, Chören, Dialogen, Charakteren, Drama im Drama, daß jedes Menschenleben wieder ist eine Zeile im canticum hominum?
Die Hauptwerke Rosenstock-Huessys sind in seinem siebten und achten Lebensjahrzehnt erschienen, in deutscher Sprache.

In dem zweiten Band der Soziologie Die Vollzahl der Zeiten, der universalen Geschichte, werden Antonius (251?-356), Athanasius (296?-373), Hieronymus (ca. 340-420) und Augustinus (354-430) die vier großen Überleber genannt, weil sie uns gelehrt haben, wie ein langes Leben körperlich, geistig und seelisch außerhalb unserer Zeit gelebt werden muß, damit die Rasse ihre Fruchtbarkeit behalte. Ich kann das nicht mehr ausführen, möchte nur aus jedem der Artikel einen Satz bringen; alle vier Sätze sind durch das Leben Eugen Rosenstock-Huessys glaubwürdig belegt:

Antonius:\
Der Mönch hält die Tore offen, damit der König der Ehren einziehen kann und die Menschen bereitfindet.
Athanasius:\
Das schamhafte Denken tritt also nur zu seiner Stunde in die Erscheinung, und nur weil es nicht anders kann.
Hieronymus:\
Alles Sprechen übersetzt den ewigen Geist in den Anhauch dieser Stunde.
Augustinus:\
Nichts was die Liebe noch nicht erkannt hat, ist erkannt.

#### 8. WESHALB HEISSEN WIR DEUTSCHE?
Meine Damen und Herren!\
Das Riesenrad hat soeben Fortinbras aussteigen lassen,\
Let four captains
Bear Hamlet like a soldier to the stage For he was likely, hat he been put on To have prov'd most royally –

aber es ist noch in Schwung, einige Passagiere müssen noch aussteigen, und mit Erscheinungsdaten bin ich noch immer nicht fertig.
In der Kölnischen Zeitung vom 27. August 1932, einem Tag vor Goethes Geburtstag, erschien als drittletzte Veröffentlichung Rosenstock-Huessys im Deutschen Reich ein Artikel Weshalb heißen wir Deutsche? Und weil mich diese Frage brennend interessiert, möchte ich ihr nachgehen. Ausführlich beantwortet ist sie in einer Arbeit von 1928 Unser Volksname Deutsch und die Aufhebung des Herzogtums Bayern.

Darin heißt es:
Das Heer spricht deutsch! Diutisk ist also die Sprache des Heervolkes, des Heeres im Thing, der zum Gericht versammelten Mannschaft des Frankenheeres aus allen Stämmen.
In den Autobiographischen Fragmenten 1965 geht Rosenstock-Huessy wieder darauf ein:
Ich fand den Ursprung des Namens „Deutsch” in einem Dialog zwischen Französisch und Deutsch! Diese beiden Namen sind auf Gedeih und Verderb aneinandergekettet. Dialogisch nur gibt es Deutsche! Soweit Kontingente des Heeres auf fränkische Kommandos einschwenkten, so weit reichte die deutsche Zunge. Deutsch wurde ein Personenbegriff, französisch aber wurde ein territorialer Name, La Douce France; das Heer der Franken aber, das deut oder diet, ihr Marsfeld, das gab den Deutschen den ihren.

Die Entmilitarisierung Deutschlands erschüttert also die älteste Einteilung der Völker Europas. Die Gens Christiana war in Kirchenvolk und Heervolk gegliedert. Die einzige Wahl oder Namenwahl für die, die deutsch bleiben, wird es vielleicht sein, sich selbst zu einem Heer der Arbeit und damit den Namen Deutsch neu zu wandeln.

#### 9. DIE WIDMUNG
Im Herbst 1970, da hat die Volkshochschule Köln Herrn Professor Dr. Rosenstock-Huessy eingeladen, hier im Forum zu sprechen. Aber es war zu spät; der Leib des Zweiundachtzigjährigen ließ die Reise nicht mehr zu.

Mir, dem Übermittler der Einladung, schickte er nach einem Brief drei Bücher zu, zwei frisch in dem Verlag Argo Books erschienene und den Band CLVI Wege der Forschung mit 19 Beiträgen Der Volksname Deutsch, an fünfter Stelle die Arbeit Rosenstocks von 1928.

In diesen Band schrieb er eine Widmung, die ich zuerst gar nicht verstand, weil sie mich verlegen machte, die ich nun aber Ihnen weitersagen möchte, weil sie mich ergriffen hat; da steht:\
Dem deutschen Richter und\
seiner lieben Frau\
von ihrem getreuen Eugen\
Herbst 1970

Diese Widmung schafft also neu den Thing, wo das Urteil in deutscher Sprache als Rechtssprache ergehen kann und bleibt bei diesem Ernst nicht stehen, sondern geht weiter: nur mit seiner lieben Frau kann die Rechtssprache Zukunft haben, und Hamlet, das ist deren beider getreuer Eugen, Wohlgeborener. Schließlich kommen Noahs Friedensschluß in der Ordnung der Jahreszeiten und der Stifter der crucivert quellenden Zeiten zu Wort.

#### 10. DAS PLAKAT

Ein Zeitgenosse Rosenstock-Huessys, der 1891 geborene, im Verlauf der stalinschen „Säuberungen” der dreißiger Jahre nach Sibirien deportierte und verschollene Ossip Mandelstamm, dichtete 1923 – und auch hier liegen zwischen Quellen und Vernehmlich-werden über dreißig Jahre, Paul Celans Übersetzung ins Deutsche erschien 1959 -:

Dreimal selig, wer einen Namen einführt ins Lied!\
Das namengeschmückte Lied\
lebt länger inmitten der andern –\
Es ist kenntlich gemacht inmitten seiner Gefährten durch eine Stirnbinde, die von Bewußtlosigkeit heilt, von allzu starken, betäubenden Gerüchen, von Männernähe,
vom Geruch, der dem Fell starker Tiere entströmt,
oder einfach vom Duft des zwischen den Handflächen zerriebenen Thymians.
So habe ich keinen Vortrag gehalten, der die Namen aus dem Spiel gelassen hätte.
Das Plakat, nicht ein Gedicht, hat den Namen auf rotem Grunde in schwarzen Lettern an die Öffentlichkeit gebracht.

Ich danke Ihnen, lieber Herr Röhrig, für die Aufforderung, aus diesem Anlaß zu sprechen.\
Ich danke Ihnen, Herr Stragholz, für den Raum.\
Ich danke Ihnen, verehrte Anwesende, wie neulich Richard Griesbach so schön gesagt hat:\
id est – dat is et.

Invokavit, 11.3.1973\
Eckart Wilkens\
5 Köln 30\
Försterstr. 8

[zum Seitenbeginn](#top)
