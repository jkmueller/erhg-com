---
title: "Yunus Emre: Das Kummerrad"
created: 2002
category: veroeff-elem
published: 2024-11-22
---
### Stimmstein 7, 2002

#### Mitteilungsblätter  2002

### Yunus Emre

### Das Kummerrad

Warum seufzt du Wasserrad \
Weil ich leide seufz ich so \
Hab mich in den Herrn verliebt \
Darum seufz ich so \
Ich bin das Kummerrad \
Mein Wasser fließt ganz blank \
Ein Befehl des Schöpfers \
Darum seufz ich so \
In den Bergen fand man mich \
Rupfte mir Arm und Flügel \
Hielt mich für ein Wasserrad \
Darum seufz ich so \
Bin vom Holz des Bergbaums \
Nicht süß und nicht bitter \
Bin ein Fürbitter bei Gott \
Darum seufz ich so \
Sie stutzten mir die Äste \
Niemand erkannte mich wieder \
Bin ein Sänger unermüdlich \
Darum seufz ich so




Yunus Emre. Aus: Das Kummerrad/Dertli Dolap, türkisch-deutsch, übersersetzt von Zafer Senocak, ohne Ort, ohne Jahr. Das Kummerrad Seite 72-75. Der Band ist im Buchhandel nicht erhältlich. Die Lebensdaten  von Yunus Emre sind unsicher, gest. um 1320/21.
