---
title: ARCHIV WORLDWIDE – Ohne Erinnerung keine Zukunft
created: 2011
category: archiv
---

Lieber Gottfried,

selbstverständlich haben wir Jahr für Jahr und Jahrzehnt für Jahrzehnt Deinen Dienst in Anspruch genommen – das Archiv. *„Frag Gottfried, wenn Du etwas suchst.”* Ich erinnere mich an 1980, als Du mir auf meine Frage die alten Mitteilungsblätter zukommen liessest, die mir viele Begegnungen mit Eugen Rosenstock-Huessy und seinen Gesprächspartnern schenkten. Du warst und bist ein Hüter unserer Erinnerung als Gesellschaft und ein Hüter der Erinnerung an Eugen Rosenstock-Huessy. Danke für diesen Deinen Dienst! Ohne Erinnerung keine Zukunft! Das gilt für unsere ERH-Gesellschaft. Das gilt auch für die planetarische Gesellschaft.

Als verantwortlicher Haushalter des Archives, der für die Zukunft Sorge trägt habe ich Dich erlebt seit ich im Vorstand bin: *„Wer und wo und wie wird dieser Archivdienst weitergetragen wenn mein Dienst 2012 zu Ende geht? Wer wird sich darum kümmern bis der Name Eugen Rosenstock-Huessy so bekannt ist wie es ihm gebührt. Der bisherige Standort Hauptarchiv Bethel würde nur eine Verwahrperspektive bieten – ohne Personal.”*

Ich empfinde es wie eine Fügung, dass ich 2009 mit Dr.Jens Murken, dem neuen jungen Leiter des Archives der westfälischen Landeskirche, zusammenarbeiten durfte, der gerade den Auftrag erhalten hatte, das Archiv der Landeskirche in das neue Gebäude in Bethel zu überführen. Dort wird es seit dem Januar 2011 in größeren und technisch optimalen Räumen mit qualifiziertem Personal und Ausstellungs- und Seminarräumen nicht nur aufbewahrt. Dr. Murken gehört zu den Archivaren, die das ‚Bergwerk der Bilder’ der Geschichte mit dem gegenwärtigen Leben ins Gespräch bringen, mit einem Wort Rosenstock-Huessys auf dass ‚die grosse Halle der Geschichte und der Dichtung aufgeschlossen steht, um durch Beispiel und geflügeltes Wort uns zu erfüllen.’ (Totenrede auf Vater Theodor Rosenstock 1928). Dieses gegenwärtige Leben ist zum Beispiel die im selben Gebäude angesiedelte neue Hochschule für Diakonie, aber auch die Webpräsenz des Archives. Begeistert wies Dr. Murken sofort auf die Chancen hin, die sich daraus ergeben, dass der Nachlass des Freundes Hans Ehrenberg sich schon am selben Ort befindet: Ahnen unserer Zukunft versammeln. Wer die grosse Sehnsucht unserer Zeit kennt nach Wurzeln, der wird diese Versammlung hochschätzen, denn *„wir bedürfen des Hervorrufens alter Zeit, damit wir unserer Rufkraft, unserer Berufung, innewerden.”* (ERH Frankreich-Deutschland 1956)

Am Ende einer Epoche, in der „unsichtbaren Welt”, da benötigen wir diese Stimmen notwendend um unsere Berufung zu finden, um unser Wort zu finden, was jetzt gesagt wird (Augustinus). Eugen Rosenstock-Huessy und Hans Ehrenberg gehören zu den Pionieren, die uns herausführen ‚aus dem vertrauten Standpunkt des Zuschauers hin zum entscheidenden Zeitpunkt, an dem wir uns zusammen befinden auf dem Weg zum Planeten’ (Ko Vos) in der Epoche der Gegenseitigkeit aller.

Du warst skeptisch. Das verstehe ich gut! Dann aber leuchtete auf Deinem Gesicht Deine Freude auf, als im Gespräch mit Dr. Murken die Zukunft unseres Archives verbindliche Gestalt annahm. Jetzt liegt der Vertrag vor und treibt mir Tränen der Freude in die Augen und weckt Lust zum Feiern und bietet uns allen Anlass, Dir zu danken für Deinen Dienst.

Dein

Thomas

aus: Brief an die Mitglieder September 2011
