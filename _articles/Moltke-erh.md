---
title: "Freya von Moltke über Rosenstock-Huessy"
category: aussage
published: 2022-01-04
name: Moltke
---

„Wenn ich über Rosenstock länger reden sollte, muß ich zuerst sagen, daß er eigentlich immer quergelegen hat. Das ist die einzige Eigenschaft, die er mit meinem Mann gemeinsam hat.”
>*Freya von Moltke, Die Kreisauerin. Gespräche mit Eva Hoffmann in der Reihe „Zeugen des Jahrhunderts“, hrsg.v. Ingo Hermann, Göttingen: Lamuv Verlag 1992, S.113.*
