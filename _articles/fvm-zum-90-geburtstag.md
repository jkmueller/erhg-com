---
title: "Freya von Moltke zum 90. Geburtstag"
created: 1991
category: veroeff-elem
published: 2024-04-23
---
### Stimmstein 6, 2001

#### Mitteilungsblätter  2001

&nbsp;  

### Freya von Moltke zum 90. Geburtstag

*Am 23. März 2001 wurde  Freya von Moltke 90 Jahre alt. Die Stiftung „Kreisau" für europäische Verständigung gratulierte ihr mit einer Sammlung von Briefen, die ihr Freunde aus Polen, aus den Niederlanden und aus Deutschland schrieben. Wir übernehmen in dieses Heft mit Erlaubnis der Jubilarin, der Verfasser und der Stiftung Kreisau die  Beiträge von Wolfgang Ullmann und Ger van Roon. Van Roon stellte Zitate aus Briefen zusammen, die Freya von Moltke ihm schrieb, als er an dem Buch „Neuordnung im Widerstand −  Der Kreisauer Kreis innerhalb der deutschen Widerstandsbewegung" (München 1967) arbeitete. In den Sechzigerjahren spitzte sich der Streit der vier Besatzungs-mächte um Berlin zu und der Kalte Krieg beherrschte weithin das politische Denken. Wenn man sich daran erinnert, muss man den Weitblick, die Zuversicht und die Beharrlichkeit der Jubilarin bewundern, die in den Briefen und Zitaten zum Ausdruck kommt. Heute ist das Gut Kreisau (Krżowa) zu einer Begegnungsstätte vorbildlich ausgebaut, − ein „planetary post“ des Friedens, für den man von Herzen dankbar sein kann.*

&nbsp; * &nbsp; * &nbsp; *

Ger van Roon &nbsp; &nbsp; [ohne Datum]

Mit diesem Beitrag will ich dankbar erinnern an Ihr großes Vertrauen und Ihre tätige Mithilfe bei der Vorbereitung des Kreisauer Buches während der Jahre 1961-1967. Dazu will ich aus Ihren damaligen Briefen zitieren.

Mit herzlichen Grüßen und Wünschen\
Ihr\
Ger van Roon

&nbsp; * &nbsp; * &nbsp; *

„Es ist also höchste Zeit, Ihnen zu schreiben, dass ich mich freue, Sie mit den Kreisauern beschäftigt zu wissen, dass ich mit Ihren Beweggründen, sich für sie zu interessieren, übereinstimme und dass ich Ihnen gerne, soweit ich vermag, helfen möchte." (27. 11. 61)

„Nachdem Sie weg waren, habe ich mir überlegt, dass Sie mir den Empfang der Aktenstücke quittieren sollen." (7. 1. 62)

„...und ich muss Ihnen auch einmal sagen, wie sehr es mich freut - in die weitere Zukunft gesehen - dass diese Arbeit von einem Holländer gemacht wird." (29. 12. 62)

„Hier kommen jetzt zuerst und endlich einmal die versprochenen Antworten. Ich bin aber darauf eingestellt, mehr und ergänzend zu antworten und mehr für Sie zu arbeiten." (25. 1. 63)

„Wenn Sie erwägen, meinen Mann etwas mehr in den Vordergrund zu stellen, so vergessen Sie bitte nicht, dass das Wesentliche an Kreisau das Zusammen-kommen, die Gemeinschaft, die Gemeinde war...(Die Kreisauer) wollten gemein-sam neue Lösungen finden. Sie sagten, wenn nicht jeder von den Anderen zu lernen bereit ist, bereit ist Gewisses fallen zu lassen und Anderes neu aufzugreifen, dann bleiben wir, wo wir waren... Wir müssen gemeinsam lernen... Bei den Kreisauern gab es daher keine taktischen Erwägungen, sondern nur die Frage „Wo geht die Welt hin?" Und „Wie messen wir uns vorbereiten, um der Zukunft gerecht zu werden?" ...Darum ist Kreisaus Leistung die Gemeinschaft und nicht der Einzelne." (Januar 1963)

„Es ist Ihnen sicherlich klar, wie er sich bemüht, den Sozialisten die Vorurteile gegenüber den Christen auszutreiben und den Katholiken gegenüber den  Sozia-listen." (Januar 1963)

„Grundlage für die Zusammenarbeit in Europa in der Zukunft. Der Widerstand in den einzelnen Ländern würde, so meinte er, die Regierungen im Nachkriegs-europa bilden. Je früher sie bekannt mit dem deutschen Widerstand würden, um so einfacher später die Zusammenarbeit." (Januar 1963)

„Sonntag Morgen bin ich in Schiphol und gegen Mittag werde ich mich gerne mit Ihnen in Verbindung setzen." (2. 8. 65)

„Vielen Dank für die Übersendung des Lebensbildes meines Mannes. Ich habe eine ganze Menge dazu zu sagen, aber grundsätzlich finde ich es schön und entsprechend ...Ich bin dankbar, dass aus dem Ganzen wirklich der anpackende Mann, die ihr Ziel nie aus dem Auge lassende, und andere Menschen dahin ausrichtende und antreibende Kraft meines Mannes endlich einmal zum Vorschein kommt ...Ich bin nie jemandem begegneten in dem ich das so stark gespürt habe. Allerdings war er mir sehr nah." (11.10.65)

„Wissen Sie, er konnte sich in all dem Unglück nur 'am Leben' erhalten, wenn er über die Gegenwart hinaussah. Das hat ihn ja die ganzen Jahre bestimmt er hat nie aufgehört an das "danach" zu glauben. So nur konnte er es verhindern, in der Verzweiflung des

„Jetzt nicht moralisch, seelisch und leiblich unterzugehen." (Januar 1963)

Ich habe in der Tat noch viele, viele Stunden mit Ihrer Arbeit verbracht und werde das auch bis zum Schluss weiter tun. Wir ziehen ja mit dem Ganzen vollkommen an einem Strang." (24. 11. 65) „Ausgerechnet heute kam Ihr Buch, an meines Mannes Todestag. Das hat mich sehr ergriffen. Ich habe es bisher nur in der Hand gehalten und darin geblättert aber es liegt gut in der Hand und wirkt gewichtig und auch inhaltlich gewichtig ...Es ist ein notwendiges Buch." (23.1.67)

„Kaum zu glauben, dass es schon 4 Wochen her sein soll seit der Promotion. Es muss sehr schön und interessant gewesen sein. Konrad hat mir einen ausführlichen Bericht geschickt und ich habe mit Spannung und Freude die vielen Zeitungsausschnitte gelesen.... Denn in der Tat ich habe noch Zukunftspläne mit Kreisau.
- 1) Ich denke immer noch, eines Tages wird noch einmal aus Kreisau ein Haus für polnisch-deutsche Verständigung.
- 2) Ich möchte auf dem Kapellenberg, wo die Moltkes alle seit dem Feldmarschall begraben sind, eine Steinplatte für Helmuth legen. Die gibt es sonst nirgends. Und die soll auch nirgends hin außer dort hin. Aber ich kann mir doch vorstellen, dass es einmal soweit sein wird, dass man Helmuth als einen Europäer ansehen wird, so sehr als einen Europäer, dass auch die Polen bereit sein werden, ihm in Polen einen Stein zu setzen. Ob das mir oder meinen Söhnen gelingen wird? Aber dazu brauchen wir natürlich gerade Ihre Hilfe." (21.3.67)



&nbsp; * &nbsp; * &nbsp; *


Wolfgang Ullmann &nbsp; &nbsp; 10. 10. 2000

Was für ein Geburtstag, wenn Freya von Moltke 90 wird!  Sehen Sie es mir nach, verehrte, liebe Frau von Moltke, daß ich mit diesem Ausruf die Verlegenheit zu beschwören versuche, die mich anwandelt, wenn ich jemandem wie Ihnen, der allen Heroisierungen und Stilisierungen der eigenen Person gründlich abhold ist, zu Gratulieren versuche, indem ich rückhaltlos sage, warum Sie zu denen gehören, die für unsereinen Wegmarkierungen geworden sind.

Ich sehe Sie, die dieses Jahrhundert überlebt hat, das Ossip Mandelstam gezwungen war ein Wolfsjahrhundert zu nennen, als eine Art Siegeszeichen. Ein Beweis dafür, daß dieses Jahrhundert zu überleben war, in dem der Alltag Völkermord, der Ruin Deutschlands und der versuchte Selbstmord Europas dazu geführt haben, daß die Meinung herrschend werden konnte, den braunen Totalitarismus könne nur der rote besiegen.

Sie stehen mir als Zeugin dafür, daß es sich ganz anders verhielt. Sie sind die Empfängerin jenes Briefes, der zu den wichtigsten kirchengeschichtlichen Quellen des 20. Jahrhunderts gehört, der auch viel wichtiger ist als zahlreiche Synodalbeschlüsse. Es ist der Brief, in dem Ihr Mann Ihnen berichtete, wie der Nationalsozialismus durch einen christlichen Märtyrer widerlegt worden ist. Nämlich dadurch, daß der Vertreter des Nationalsozialismus gezwungen wurde - ja, gezwungen! - alle politischen Masken fallen zu lassen und sich offen zum ...Antichristentum zu bekennen. Ein Bekenntnis, dessen Bedeutung weit über die politische Sphäre hinausreicht. Denn mit ihm war klargestellte Christentum und Judentum sind die universalsten Indizien der Humanität. Deren Inhalt ist nicht zuerst der Klassizismus von edler Einfalt und stiller Größe; sondern das un-gerechtfertigte und schuldlose Leiden an der...von der geschändeten Menschen-würde. Edle Einfalt und stille Größe kann es erst dort wieder geben, wo die geschändete Menschenwürde wiederhergestellt und gerettet ist.

Das vor dem Volksgerichtshof und im Disput mit Freisler klargestellt zu haben - das war die Widerlegung des Nationalsozialismus und insofern für die Zukunft der Humanität noch wichtiger als die bedingungslose Kapitulation vom 8. Mai 1945.

Sie sind Kronzeugin in dieser Sache. Aber Sie sind es in einer noch ganz anderen, von der ich fast vermute, daß Sie sich dessen noch gar nicht voll bewußt sind, weil Sie es gar nicht sein können.

Man hat mehr als einmal behauptet, die Friedliche Revolution von 1989 habe keine Vordenker gehabt. Das ist in mehr als einer Hinsicht unzutreffend. In einer aber ganz besonders, nämlich hinsichtlich der Bürgerbewegung „Demokratie jetzt". Es ist nichts weniger als ein Zufall, daß der Tisch, an dem einen Tag nach dem Mauerfall am 10. 11. 1989 der Aufruf zum Runden Tisch und dessen Programm formuliert wurde, der gleiche war, an dem seit 1983 Theologiestudenten, Pfarrer und Vikare Rosenstock-Huessy gelesen und diskutiert hatten. Der gleiche Kreis, der 1988 das Kreisau-Projekt ins Auge faßte, das ich Ihnen im August des gleichen Jahres im Dartmouth College vorgetragen habe, glücklich darüber, daß es Ihre Billigung fand.

Warum konnte Rosenstock-Huessy diese Initiative anregen? Weil er schon lange zuvor in der Dimension der Globalität und im Blick auf das neue Jahrtausend dachte, in das unserer Gesellschaft erst jetzt zögernd einzutreten sich anschickt.

Und es war diese Vorgeschichte, die zu der denkwürdigen Koinzidenz der ersten freien Wahl in Polen mit dem für die Kreisau-Initiative entscheidenden Treffen in Wrocław am ersten Juniwochenende 1989 geführt hat.

Für solche Art Kronzeugenschaft erlauben Sie mir, liebe verehrte Frau von Moltke, Ihnen aus dem Zimmer jenes oben erwähnten Tisches aus einem Berliner Hinterhof meinen Dankesgruß zuzurufen und alles Gute für den Jubiläumsgeburtstag samt allen künftigen Tagen und Jahren zu wünschen.

Ihr\
Wolfgang Ullmann
