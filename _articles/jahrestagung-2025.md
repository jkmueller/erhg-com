---
title: Jahrestagung 2025
created: 2024
category: jahrestagung
thema: "-- Wird noch bekannt gegeben -- "
published: 2024-10-13
landingpage:
order-lp: 1
---

### {{ page.thema }}

![Haus am Turm]({{ 'assets/images/Haus_am_Turm_Essen.jpg' | relative_url }}){:.img-right.img-large}

Datum: Freitag 17.10. - Sonntag 19.10.2025
<!--more-->


Ort: Haus am Turm, Essen
