---
title: "Rosenstock-Huessy: Pierre Abélard (1962)"
category: online-text
published: 2022-08-20
org-publ: 1962
language: english
---

**ABÉLARD, PIERRE,** 1079-1142, the most brilliant and daring philosopher and theologian of the twelfth century was born in the village of Pallet (Palais), near Nantes. His father was of the minor nobility and Pierre, as eldest son, was expected to follow a military career. He chose a life of scholarship instead. One of Abélard's early teachers was Roscelin; another was William of Champeaux, master of the Cathedral School on the Bishop's Island (in the Seine River), at Paris. Abélard soon discovered that the savants were split into two camps, disputing over "universals."

This dispute had to do with the reality quality of general terms in language: is the word "humanity" a mere abstraction - an arbitrary term - or is it fundamentally real in itself? William of Champeaux said that such a word was real and necessary. Roscelin said that the word was an abstraction only - that it had nothing to do with reality. When the discussion concerned words such as "God" and "Trinity"
it became crucial indeed for men of the church, as all those in the schools were.

While Abélard would not accept the extreme nominalism of Roscelin, neither could he accept the ultra-realism of Champeaux. Almost from the day of his arrival in Paris to study on the Bishop's Island, Abélard was in trouble with the authorities. Several times he endeavored to get a teaching post at the Bishop's school, but was always refused. He taught anyway, and was immensely popular with the students. Abélard was ordered to leave "the land of Paris," so he lectured to his students from a tree. The authorities pulled him from the tree. He took a boat out into the Seine and conducted class in the middle of the river. The authorities put a stop to this too.
Finally, he left the Bishop's Island and went to the left bank to the school of Ste Geneviève where, no longer under the authority of the Bishop of Paris, he taught after 1108 with great success. In 1113-18, Abélard was finally allowed to teach at the Bishop' school on the island. See NoMINALISM; REALISM.

As a result of all this commotion an amazing development had taken place. For the first time in history two schools, each teaching different and contradictory doctrines, existed side by side. By bringing this about, Abélard had inadvertently invented the university -something completely different from the purely local schools, as patterned after Plato's academy, which had existed before.

Moreover, the content of what Abélard taught was revolutionary. With his *Sic et Non* (Yes and No) - in which he placed side by side the various mutually contradictory views of the many authorities of the church of the preceding 1,000 years - Abélard literally founded Scholasticism - scientific, systematic, dialectical theology. This movement culminated in the "Summa," the summing up of all church doctrine. In founding the university of Paris, Abélard made Paris the "brain of the occident," and his *Sic et Non* imparted clarity
and brilliance on French literary style. See SCHOLASTICISM

Abélard's solution to the problem of universals was a profound one. He held that words are neither real in the sense that the realists contended, nor merely arbitrary constructions as the extreme nominalists maintained. A *sermo*, or expression, is the way a man must speak for the time being among men in fellowship. Important words *become* universals by their being accepted as universal, and *used* as such to express *necessary* truths. Abélard was also the only major medieval thinker to consider ethical questions; he asserted that the intention is as important, as the act growing out of it.

From the time of his first rebuffs on the Bishop's Island, Abélard was a rebel. As his Historia calamitatum shows, Abélard's castration by thugs hired by Canon Fulbert, uncle of Abélard's wife Héloïse, did little to make him more co-operative, even though it developed that Fulbert had acted on the mistaken idea that Abélard planned to abandon Héloise. Twice Abélard was condemned by church councils.
After the first at Soissons in 1121 his response was defiant: he founded an oratory dedicated to the Holy Ghost - The Paraclete - thereby asserting the right of the mind to freedom. A later condemnation in 1141 by the council at Sens, instigated by Bernard of Clairvaux, "last Father of the Church," was endorsed by the pope. A few months later Abélard died at the priory of St. Marcel, near Châlon-sur-Saône. Héloise, who had succeeded Abélard at Paraclete, died in 1164. See BERNARD OF CLAIRVAUX.

Entry in the American People’s Encyclopedia (Grolier Inc., 1962)

“Abelard, Pierre,”8: 201-204. {Unsigned.  Francis Squibb, co-author.} Reel 11, Item 545, Frame 6733.

[„Abelard, Pierre” as part of the PDF-Scan](http://www.erhfund.org/wp-content/uploads/545.pdf)

[zum Seitenbeginn](#top)
