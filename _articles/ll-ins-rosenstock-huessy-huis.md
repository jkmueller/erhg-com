---
title: "Lien Leenman: Im Rosenstock-Huessy-Huis in Haarlem"
created: 2021
category: veroeff-elem
published: 2021-12-18
---
### Stimmstein 2, 1988
#### Lien Leenman-De Pijper

Hoe hebben de stichters van het Rosenstock-Huessy-Huis het ervaren om in het huis te trekken?

Dit kan alleen een persoonlijk verhaal zijn. Het is moeilijk ruim 16 jaren terug te denken. Ook omdat het achteraf anders is dan het toen voor ons was.

Ik persoonlijk heb er altijd behoefte aan gehad zoveel mogelijk betrokken te zijn bij het werk van mijn man (Wim Leenman). In zijn industriepastoraat vormden de diverse gespreksgroepen het belangrijkste deel van zijn werk. Ik nam zelf deel aan verschillende groepen. Door de vaak intensieve gesprekken daar, groeide, ook bij anderen, het verlangen elkaar vaker en op meer terreinen van het leven te ontmoeten. Dat werden we ons nog meer bewust toen we, op uitnodiging van de »Evangelische Männerarbeit« in de Pfalz, deelnamen aan vakantie / studie-weken op het vormingscentrum Ebernburg.

Toen Wim geen industriepredikant meer was, maar »gewoon werknemer« bij het Hoogovenbedrijf werd, viel er voor mij een gat. Voor mijn gevoel hadden we voordien samen aan iets gewerkt. Nu werkte Wim echt buitenshuis en ik viel terug op alleen ons gezin. Dank zij de vele vrienden die we hadden gekregen en de contacten die we hadden opgebouwd, was het »gat« gelukkig niet zo groot. Ik heb toen nog een poosje les gegeven aan een school, maar ging toch meer en meer de scheiding tussen leven en werk ervaren. Voor Wim en mij was dat één geheel geweest.

De Rosenstock-leeskring was blijven doorgaan. Ook de contacten met Rosenstock zelf werden intensiever via weekends in Heidelberg en door het bijwonen van bijeenkomsten waar Rosenstock sprak.

Toen er stemmen opgingen in de leeskring om met alles wat we tot dan toe gelezen, geleerd en ervaren hadden, iets concreets te gaan doen, was dat een uitdaging om op een andere manier en intensiever te gaan leven. Wim en ik hadden al meermalen plannen gehad om met meer mensen in één huis te gaan wonen en werken. Het idee trok mij erg aan, als was het ook griezelig.

De mensen van de leeskring en de anderen die gingen  meedenken in het opzetten van een leef- en werkgemeenzaam, waren niet primair onze beste vrienden! Onze gemeenschappelijkheid was »Rosenstock-Huessy« èn de wil om op een andere manier in de maatschappij te staan. En ook om de beperking van het gezin te doorbreken.

Hoe ist de ervaring achteraf? — We hadden allemaal onze voorstellingen hoe het zou moeten worden en wat we zouden doen. Natuurlijk bleek na korte tijd, dat ieder zijn persoonlijke vulling daaraan wilde geven. Dat werden dan ook de eerste onderlinge botsingen. Maar de bindende factor (onze doelstelling) bleek sterk genoeg om ons bij elkaar te houden. Ik heb vooral de eerste jaren als zeer leerzaam ervaren.

Rosenstock schrijft ergens over »de universiteit van de toekomst«. Ik heb die eerste jaren ervaren als »een nieuwe universiteit«, een levensschool, dank zij de gesprekken en »gevechten« van de vaste bewoners van het huis onderling, en dank zij de inbreng van onze tijdelijke medebewoners. Van hen heb ik nòg meer geleerd, en ik ben daar erg dankbaar voor. Hun vertrouwen, hun vriendschap en hun geduld doen mij veel. En ik hoop, dat dàt wederkerig is, want het is erg belangrijk voor me.
>*Lien Leenman-De Pijper*
