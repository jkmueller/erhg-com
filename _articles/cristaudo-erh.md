---
title: "Wayne Cristaudo about Rosenstock-Huessy"
category: aussage
published: 2022-01-04
name: Cristaudo
language: english
---

Building upon the works of Augustine, Vico, Hamann, Herder, Goethe, and Saint-Simon, and working in tandem with Franz Rosenzweig, Rosenstock-Huessy has provided an urgent  and timely program for the social or human sciences. It is based on the recognition that to be human is to be torn between the intersection of the inner and outer dimensions of space, and the ‘trajective’ and ‘prejective’ dimensions of time. This is what he calls the cross of reality.

Our different names, traditions, cultures, and institutions are the creative responses of founders and communities to past catastrophic events and processes. Rosenstock-Huessy calls upon us to pool and contribute to the spiritual resources that have been stored in the great treasure houses of languages and traditions, and create institutions for a genuine future peace. Thus his greatest work—the two volume Sociology, In the Cross of Reality—is devoted to achieving what he calls the “Full Number of the Times.”
>*Wayne Cristaudo (2019)*
