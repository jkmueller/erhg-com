---
title: Mitgliederbrief 2015-05
category: rundbrief
created: 2015-05
summary:
zitat: |
  >Unmodern
  >
  >Ich blase nicht der Zukunft Trompetenstöße vor, ich reite immer gerne durchs alte Märchentor.
  >
  >Wohl ist es schön zu leben in hartem stolzem Streit. Ich bleibe still daneben und such die Ewigkeit.
  >*Eugen Rosenstock-Huessy, aus dem Gedichtbuch: Ein Sommer 1906*
---

## Eugen Rosenstock-Huessy Gesellschaft e.V.

{{ page.zitat }}

**Vorstand/board/bestuur:**\
*Dr. Eckart Wilkens (Vorsitzender);*\
*Thomas Dreessen; Andreas Schreck; Dr. Jürgen Müller*\
*Antwortadresse: Dr. Eckart Wilkens: Everhardstraße 77, 50823 Köln, Deutschland*\
*Tel: 0(049) 221 511265*

***Brief an die Mitglieder Mai 2015***

**Inhalt**

{{ page.summary }}


[zum Seitenbeginn](#top)
