---
title: Mitgliederbrief 2008-02
category: rundbrief
created: 2008-02
summary:
zitat: |
  >Wo bleiben die Hellenen in unserer Ära, die griechischen Schulen der Genies? Die Hohen Schulen des letzten Jahrtausends verwurzeln die griechische Muße in dem neuen Reiche des Allmächtigen, der über das All sein Machtwort gesetzt hat. Diese Schießstände und Wachposten der „freien Zeit” bemannt der Schulgeist, um alles zu vergleichen und zu beschreiben; sie heißen bisher Scholastik und Akademik. Sie wurden mit der Gründung der mittelalterlichen Universitäten in Paris und Bologna installiert; sie wurden mit den Akademien in Florenz und Paris und London und Berlin umgestellt, und sie harren in einer von Saint Simon und Goethe bereits angehobenen dritten Epoche ihrer endgültigen Eingemeindung in die christliche Zeitrechnung.
  >*Eugen Rosenstock-Huessy, Soziologie II, S. 683f.*
---

## Eugen Rosenstock-Huessy Gesellschaft e.V.

{{ page.zitat }}

**Vorstand/board/bestuur:**\
*Dr. Eckart Wilkens (Vorsitzender);*\
*Wilmy Verhage; Andreas Schreck; Dr. Jürgen Müller; Lothar Mack*\
*Antwortadresse: Dr. Eckart Wilkens: Everhardstraße 77, 50823 Köln, Deutschland*\
*Tel: 0(049) 221 511265*

***Brief an die Mitglieder Februar 2008***

**Inhalt**

{{ page.summary }}


[zum Seitenbeginn](#top)
