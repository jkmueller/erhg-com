---
title: "Lewis Mumford about Rosenstock-Huessy"
category: aussage
published: 2022-01-04
name: Mumford
language: english
---


„Rosenstock-Huessy's is a powerful and original mind. What is most important in his work is the understanding of the relevance of traditional values to a civilization still undergoing revolutionary transformations; and this contribution will gain rather than lose significance in the future.”
>*Lewis Mumford*
