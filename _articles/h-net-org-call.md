---
title: "H-Net.org: Call for essays and reviewers"
category: rezeption
---
Call for essays and reviewers:\
**Culture Theory and Critique special themed issue on Eugen Rosenstock-Huessy**

Editors: Wayne Cristaudo, Charles Darwin University and Andreas Leutzsch, University of Hong Kong

Inspired by recent reissued correspondence letters between Eugen Rosenstock-Huessy and Franz Rosenzweig, this issue will include the translation of a small group of the letters between Rosenzweig, Rosenstock-Huessy and Gritli Huessy. These translations will be accompanied by essays explaining the background of the letters, the importance of Gritli Huessy to Rosenzweig and how the continuation of the dialogues.

We welcome additional essays that would address other themes related to Rosenstock-Huessy's contributions to social philosophy and theology. Themes can include his study of the meanings of revolution, his role as a historian, his influences on the field of sociology, his intrinsic theopolitics, and his correspondences with Jacob Taubes. The themes are not meant to be proscriptive, however, and we welcome queries about possible essay content.

The [issue](https://www.tandfonline.com/doi/full/10.1080/14735784.2014.979086) appeared in December 2013.
