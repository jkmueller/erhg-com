---
title: "Wilmy Verhage: Ein Brief zu Die Tochter – Das Buch Rut"
category: weitersager
order: 10
---

***„Denn das Leben ist die Liebe
 und der Liebe Leben Geist.”***

(Goethe, West-östlicher Divan, Suleika)
>Nimmer will ich dich verlieren! Liebe gibt der Liebe Kraft.\
Magst du meine Jugend zieren\
Mit gewalt'ger Leidenschaft.\
Ach! Wie schmeichelt's meinem Triebe,\
Wenn man meinen Dichter preist:\
Denn das Leben ist die Liebe,\
Und des Lebens Leben Geist.

Ein Brief zu\
Die Tochter – Das Buch Rut


Amsterdam, April – Oktober 2007

Liebe Mary Louisa,

Du bist schon die zweite Frau, die ich kenne, die das Buch ‘Die Tochter’ geschenkt bekommen hat und es nicht zu lesen wagt.

Ist der Titel schon so beladen?

Hast du Angst, dass das Buch wieder vieles von Dir fordert, was Du gar nicht geben willst oder kannst? Dass es Dir eine neue Art der Bevormundung aufhalst, wo Du das alte Joch noch kaum abgeschüttelt hast?


Das Buch aber handelt eigentlich gar nicht von Frauen. Es geht um Männer. Es geht um den geistlichen Bankrott der Männer, so wie Eugen Rosenstock-Huessy es im ersten Weltkrieg erfahren hat und was er in diesem Aufsatz unter Worte bringt.

Er schreibt sich aus dem seelischen Tief der Verzweiflung:

*„Wir sagen es uns nicht, denn wir können das Wesentliche nicht mehr laut sagen. Alles Laute ist unerträglich geworden. Fast ist uns die gegliederte Sprache schon zu abgegriffen. Wir suchen eine Sprache, die nicht mehr gesprochen zu werden braucht, um uns in sie einzuhüllen. Es ist nicht leeres Schweigen, das wir suchen. Möchten wir doch zueinander; müssen zu einander.”* (Seite 29)
So fängt er an.\
Selber können sie das Heil nicht mehr bringen, die Männer. Das hat der Weltkrieg ausreichend gezeigt, so ist ihm klar geworden. Wo soll es denn ab jetzt herkommen? Am Ende, 15 Seiten weiter, zeigt er erleichtert, aus eigener Erfahrung zapfend, auf die Tochter, obschon sie dazu die am wenigsten auf der Hand liegende Gestalt scheint.


Meine Tante Ko, Deine Heldin Ko Vos, schenkte mir das Buch genau vor zehn Jahren. Zu dieser Zeit zögerte ich auf der Schwelle, Rosenstock-Huessys Worten mein Vertrauen zu schenken und damit den Feminismus zu verlassen.

Was für Gutes hat er einer Frau zu sagen?

Wie wird meine schwer erkämpfte Selbständigkeit gewährleistet?

Mit diesem unausgesprochenen Vorbehalt verwickelte ich meine vielgeplagte Tante in einen heftigen intellektuellen Briefwechsel, was sie schnell satt hatte. Das Zusenden des Buches war ihr ‚Basta’. Ihre vorläufig letzte Antwort.


Ich habe es gewagt. Ich fing an zu lesen. Und ich war freudig überrascht, dass ich meine eigenen Erfahrungen gespiegelt fand in Rosenstock-Huessys Analyse, was mit der Welt los war. Dass eigentlich kein Platz war für mich, kein Raum für die frauliche Perspektiv, die frauliche Art zu reagieren.

Geistlich kam ich endlich, endlich nach Hause.


Der Aufsatz ‚Die Tochter’ wurde 1920 zum ersten Male publiziert in dem Buch „Die Hochzeit des Kriegs und der Revolution”, wie das sich unter seinen Augen und an ihm selber vollzog.

1961 bat er seinen Freund Bas Leenman während einer Zugreise, nach seinem Tod daraus das Kapitel ‚Die Tochter’ herauszugeben, zusammen mit dem Buch Ruth in der Bibelübersetzung seiner Freunde Franz Rosenzweig und Martin Buber. „In einem schönen weißen Einband”, fügte er noch dazu.

Dank einer Spende von Ko Vos wurde das 1988 wahr, zu seinem hundertsten Geburtstag.


Weshalb zusammen mit dem Buch Ruth? Diese Frage hat mich lange beschäftigt. Am Ende habe ich es so verstanden: In der Geschichte Ruth aus der Bibel fand er den Kern der Tochtergestalt, die Stimme die, wie er sagt, im gesellschaftlichen Konzert fehlt.

Erzählt wird darin, wie Ruth mit ihrer Schwiegermutter Naomi mitgehen will und auch nach Israel geht, nachdem sie ihren Mann an den Tod verlor. Naomi war schon 10 Jahre Witwe. Naomi und ihr Mann waren, durch Hungersnot gezwungen, aus Bethlehem zum Stamm der Moabiter gezogen, um den Lebensunterhalt zu verdienen. Sie hatten zwei Söhne, die sich mit zwei Moabiter-Frauen verheirateten, eine von ihnen Ruth. Es sterben auch die beiden Söhne.

Ich bin selber Witwe, ich kann Dir also aus Erfahrung sagen, was einem das antut. Eine der Konsequenzen ist: der Boden unter der Existenz verschwindet in gesellschaftlicher und ökonomischer Hinsicht. In Ruths Zeiten hieß das: wenn man nicht von einem Bruder des verstorbenen Mannes adoptiert wurde, wenn noch kein eigener Sohne da war, mußte man wieder zurück zur Familie, zum Stamm der Herkunft.

Die andere Schwiegertochter kehrte so zurück, obschon sie viel lieber bei Naomi geblieben wäre. Auch Naomi, kehrte zu ihren Verwandten zurück nach Bethlehem. Nur Ruth nicht, sie zieht weiter. Angeblich hatte sie und nahm sich, in diesem Moment des Machtsvakuum gleich nach den Tode ihres Mannes, die Freiheit zu wählen. Das Außergewöhnliche ihrer Situation gab ihr die Chance.

Ruth durchbricht also das Gewohnheitsrecht: Sie ergriff im Gegenteil selber die Initiative: Bitte nicht zurück, weiter ins Unbekannte, zusammen mit der Schwiegermutter, die sie, so heißt es, von ganzem Herzen lieben gelernt hatte – und dann nur hoffen, dass es recht kommt. Naomi, da und in diesem Moment den Geist Israels vergegenwärtigend, gestattet es ihr.

Ruth muss wohl besonders angenehm vom Geist, den sie im Hause Naomis antraf, überrascht gewesen sein. daß sie den Sprung wagte: ich nehme jedenfalls an, es war der Geist des Gottes Israels, und dass er eine erheblich andere Lebensatmosphäre schuf, als der Geist, der in ihrem eigenen Stamm wehte.

Die Geschichte geht dann weiter, wie es ihr auch gelingt, einen eigenen Platz zu bekommen. Indem sie den Zufall regieren ließ und die Kenntnis, Erfahrung und Intuition Naomis folgte. Wie sie sich in aller Bescheidenheit anbietet, weckt sie die Achtung und Liebe eines wichtigen Mannes, Boas. Er schuf daraufhin neue Bedingungen, zusammen mit den Männern, die gerade in Bethlehem die Entscheidungen trafen, Ruth als Tochter Israel zu adoptieren, also nicht als Sklavin, so daß er sie heiraten konnte.

Mit dieser einen Tat aus freier Wahl, diesem Schritt ins Unbekannte, regenerierte Ruth Israel. Denn in Zukunft konnte jede Frau aus fremden Ländern so aufgenommen werden. Ruth wird als eine der Stammesmütter Davids bezeichnet, und also auch von Jesus.

Es war für mich ein ziemlicher Schock, als ich die Tochter zum ersten Male las und den Analysezusammenhang Rosenstock-Huessys kennenlernte. So hatte noch keiner in meiner Umgebung je es gesehen. Es hat also lange gedauert, ehe ich mir diese Art von Schauen aneignen konnte. Eigentlich befinde ich mich noch immer in diesem Prozess. Und für Dich kann ich es nicht tun. Aber vielleicht hilft Dir mein (vorläufiges) Verständnis.

Wenn wir über Menschen reden, sind wir gewöhnt, von Individuen auszugehen. Männer und Frauen gleich. Aber Rosenstock-Huessy sagt, das Individuum sei faktisch eine Zwischenphase, zwischen einer Verbindung und der nächsten. Er ordnet das intermenschliche Geschehen in Väter, Mutter, Töchter und Söhne. Nicht als biologische Phänomene, sondern als vier unterschiedene gesellschaftliche Gestalten. Es ist die erste Ordnungsform, die ein Mensch – als Baby – kennenlernt. Und gleichzeitig ist es die älteste Gesellschaftsordnung. Wie jeder neu entdeckte Stamm aus prähistorischen Zeiten, in Neu Guinea oder im Amazonasgebiet noch beweist.

Die vier Gestalten des Menschen sind in Rosenstock-Huessys Sicht überpersönliche, auf einander bezogene Ämter, die von jeder Generation aufs neue bekleidet und durchlaufen werden, jede auf ihre oder seine eigene Art.

Wenn es gut geht, durchläuft jeder Mensch den Prozess von Tochter zu Mutter zu Frau oder von Sohn zu Mann zu Vater. Erst bist du Tochter und die Mutterschaft formt dich zu einer selbständigen Frau, oder erst bist Du Sohn, und die Selbständigkeit als Mann macht dich zur Vaterschaft geeignet. Die Betonungen liegen bei den Phasen bei Männern und Frauen verschieden, das ist in dier „Soziologie” erläutert. (Dazu brauchst du übrigens nicht tatsächlich verheiratet zu sein oder Kinder zu bekommen, alles, worauf die Sorge geht, dient der Erschaffung des mütterlichen oder väterlichen Verhaltens.)

Die vier Grundhaltungen Vater, Mutter, Sohn und Tochter formen ebenso viele verschiedene Kanäle, wodurch der Logos, die Sprache und die Institutionen, die sich daraus ergeben, sich entfalten, in jedem einzelnen Menschen, in der Gesellschaft als Ganzem.

Jedenfalls so sollte es sein. Aber Rosenstock-Huessy konstatiert in „Die Tochter”, dass die Stimme der Tochter in der Komposition fehlte. Dass die gesellschaftlichen Kräfte, die zu Wachstum erblüht sind, von Vater, Mutter und Sohn herkommen. Geistig übersetzt: Natur, Kultur und Geist. Und gesellschaftlich gesehen: Staat (Vater), Kirche (Mutter) und Wissenschaft (Sohn).

Die Seele, die Gesellschaft selber, das töchterliche Element, fehlt in dem Bewusstsein, in der Sprache seiner, sagen wir ruhig: unserer Zeit.


Jetzt, 90 Jahre nachdem Rosenstock-Huessy ‚Die Tochter’ geschrieben hat, nach einer Krise und noch einem Weltkrieg und dazu dem Kalten Krieg, kannst Du ruhig behaupten, dass „Vater Staat” sein Ansehen verloren hat, dass „Mutter Kirche” leergelaufen ist, und dass „Sohn Wissenschaft” wähnt, sich alles erlauben zu können. Alle drei dürsten nach Regeneration durch Beseelung.

Im Kämpf gegen ihre Sinne, gegen die körperliche Seite ihrer Seele haben Männer ihr Unterscheidungsvermögen verloren, das Gott den Seinen anvertraut hatte: den Sinn für Echt und Unecht, für Tod und Lebendig, sagt Rosenstock-Huessy. Und das ist der Kern des Übels.

Er konstatiert, dass vor zweitausend Jahr in Jesus Christus eine neue Lebensquelle zu fließen anfing, die gebrochenen Herzen zu heilen, aber dass die Männer diese Quelle im Laufe der Jahrhunderte als ihr persönliches Eigentum ansahen: die Katholischen haben ihn als „Heiligen Geist” verhaftet und in ihre Kirchenlehre eingemauert; die Protestanten haben in ihrem Lauf durch die Geschichte ihre Wirkung geleugnet.
In ihrem Kampf gegen die angeborene Natur hat der Katholik gelernt, seinen Körper zu hassen, der Protestant darf Schönheit nicht empfindenso hat er beobachtet und fährt fort: „Diese Abstumpfung des Lebensinstinkts war es, die sie verführte, sich über Gott zu erheben”. (Seite 36/37) Die Wirkung war, dass sie das klare Gefühl verloren, bestimmen zu können, wo Gott steht und wo sie selber stehen. Das hat auch ihre Beziehung zu Frauen bestimmt. Der Katholik hat sie geflohen, der Protestant hat sie ans Haus gebunden, der Liberale – das Genie – tat sie als nicht ernst zu nehmendes Geschöpf ab.
Diese Analyse habe ich neunzig Jahre nach dem Entstehungsdatum in meinem eigenen Leben erkannt. Der Katholik, der seinen Körper hasstder Protestant ohne Geschmack: ich habe mich an ihnen verletzt. Die Frau als Besitz, als in ihrer Aussprache nicht ernst zu nehmendes Objekt…, ach Du kennst das wie keine andere.
Rosenstock-Huessy zieht die Schlussfolgerung, dass die Zeit, die einer rigorosen Scheidung von Körper und Geist den Weg zu einem besseren Leben wies, vorbei ist. Nur aus der Einheit von Körper und Geistes kann Heilung kommen, nur die Seele, verwurzelt in Körper und Geist, findet Gnade in Gottes Augen.
Tochter Gesellschaft, sagt er, hofft auf Frauen in töchterlicher Gestalt.
Er prophezeit:\
*„Nicht bestimmt von der Lehre der Väter treten diese Frauen hervor, nicht als Töchter von Vätern, sondern als Töchter der Schöpfung, die dem Drange der Liebe und der Sehnsucht nachgeben, so wie ihnen das Herz schlägt.”(kursiv von mir) „Aus ihrer Begegnung mit den Söhnen der sich festgelaufenen Geschichte kommt neuer Geist”* (S. 45)sagt er, schöpfend aus der eigenen Erfahrung.

Denn selbstverständlich ist die Tochter als Modell für die Gesellschaft kein abstraktes Ideal. Rosenstock-Huessy hatte ein konkretes Bild vor Augen. Er ist erst selber gerettet worden.

Die gemeinte Frau war seine eigene Frau Margrit Huessy. Sie ist, da er gleich nach ihrer Eheschließung zum Wehrdienst einbezogen wurde, sofort in ein selbständiges Leben geplumpst. Mit ihren liebenden Aktionen, manchmal senkrecht quer zu den herrschenden ehelichen Konventionen, hat sie ihn aus der Verzweiflung retten können.

Er schreibt „Die Tochter” an seinen Freund Franz Rosenzweig, der in diesem Moment Margrit schon eben so feurig liebt wie er selbstund Margrit liebt beide.

Das ist an sich schon keine einfache Gegebenheit. Aber es hat allen drei Welten gutgetan. Ohne Margrits Liebe hätten die Männer sich nicht aus der Situation des Moments herschreiben können, wäre ihr Oeuvre nicht entstanden. Und ohne die Liebe zu Franz und Eugen – für sie, für einander, hätte sich Margrit nicht zu eine erwachsenen Frau entfaltet.

Am Ende des Aufsatzes ist Rostenstock-Huessy der erste Mann geworden, der freiwillig und öffentlich vom Sockel herabsteigt, wie wir Holländer sagen, und sich neben seine Frau plaziert. Er ist bereit anzuerkennen dass Frauen gebraucht werden, dass ihr unmittelbarer Kontakt mit Gott unentbehrlich ist, wenn es besser mit der Welt werden soll, mit den Männern, mit ihm selber

*„Wir können einander nicht Priester sein, aber wir können die Hände so verschlingen, sie werden uns so verschlungen, dass wir nicht mehr ohne einander leben können, dass wir sprechen müssen: ich lasse dich nicht, Du segnest ihn denn! Auch hier gibt es keine Ruhe, kein andres Ertragen als immer mehr lieben. Denn es ist eben wirkliches Leben.”* (S. 44)


Im Vorwort zu „Die Tochter” sagt Bas Leenman, das Buch sei durch und für Männer geschrieben Und es scheint, als genierte er sich darum ein bisschen. Wenn es schon von Frauen gelesen wird, fragt er, dann vielleicht als Mitleserinnen?

Aber nein, Bas, Du da inzwischen im Himmel, auf diese indirekte Art spricht er mich, und damals also auch meine Tante Ko, gerade an!

Soweit wir aus unserem Herzen sprechen lernen, soweit wir ehrlich sind in unserer Liebe – werden wir nicht nur gehört, sondern sind, so heißt es, essentielles Element im Schöpfungsprozess.

Mir hat das Lesen der „Tochter” Selbstvertrauen gegeben, ich bin gespannt, was es Dir bringt.

Liebe Grüße,

Wilmy
