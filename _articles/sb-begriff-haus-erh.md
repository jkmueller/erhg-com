---
title: "Sven Bergmann: Zum Begriff Haus"
category: einblick
published: 2022-01-04
---


„Seit etwa 1200 tritt der Gedanke des Hauses als für den deutschen Staat grundlegend in den Vordergrund. Die Hausverfassung spiegelt sich in der Staatsverfassung. Die deutschen Fürsten sind als Vasallen die persönlichen Diener (Hofleute) des Königs. An ihrer Spitze stehen die Träger der obersten Hofämter, die Kurfürsten. Das Reich wird von den Dienern des Königshauses, das Land von den persönlichen Dienern des Landesherrn regiert. Das sind die Gedanken, die E. Rosenstock in seinem bedeutenden Buch: Königshaus und Stämme in Deutschland zwischen 911 und 1250 (Leipzig 1914) geistvoll entwickelt hat.”
>*Rudolph Sohm, Das altkatholische Kirchenrecht und das Dekret Gratians <1918>, im Anh.: Rezension von Ulrich Stutz, reprogr. Nachdr., Darmstadt: Wissenschaftliche Buchgesellschaft 1967, S.587.*

„Die Plätze und Räume, auf oder in denen gearbeitet wird, stehen in der Gewalt des Arbeitgebers. Er ist der Herr dieses Hauses im rein materiellen Sinne dieses Wortes als Gebäude. Der Unternehmer aber fand, daß er auch im geistigen Sinne dieses Wortes „Haus“ der Hausherr in seiner Fabrik sein müsse. Er übertrug also den alten Grundsatz: „mein Haus ist meine Burg“ auf seine Fabrik. Das ist der sogenannte patriarchalische Herr-im-Hause-Standpunkt. Er übersieht, daß die moderne Fabrik gerade der Zerstörung des patriarchalischen Hausrechts ihre Entstehung verdankt. Gesinderecht, Hofhörigkeit, Hauszugehörigkeit des Handwerksgesellen hatten zurückgedrängt werden müssen, um ihm, dem Unternehmer, freie Kräfte für seine Unternehmung zuströmen zu lassen. Die Unternehmung kann also unmöglich das alte Recht des Eigenen Hauses für sich beanspruchen. Der Unternehmer – und die Arbeiter – verwechseln leicht die patriarchalische Hausgewalt mit der Führerschaft bei der Arbeit.”
>*Eugen Rosenstock, Sozialpolitik und Arbeitsrecht, in: Arbeitskunde. Grundlagen, Bedingungen und Ziele der wirtschaftlichen Arbeit, hrsg.v. Johannes Riedel, Leipzig and Berlin: B.G. Teubner 1925, S.67.*

„Die Verdinglichung ist ein Rechtsfortschritt und kein Verfall. Sie schreitet fort von einem Verhältnis nur zwischen Personen zu der reicheren Ordnung, welche deren Stellung erst durch die Beziehung auf etwas Drittes, eine Institution, erläutert. Zwischen Herrn und Knechte schieben sich die sorgfältig abteilenden und sichernden Wände des Hauses.”
>*Eugen Rosenstock, Königshaus und Stämme in Deutschland zwischen 911 und 1250, Leipzig: Felix Meiner Verlag 1914, S.382.*

Diese Zusammenhänge könnten bei einem Hineintragen der Unterscheidung zwischen privatem und öffentlichem Recht in die frühmittelalterliche Verfassungsordnung nicht verstanden werden. Eine solche Sicht lehnte er als „grundsätzlich verfehlt“ ab.[^1] Bei der Herauslösung des Hauses aus den Geschlechterverbänden und der Person aus der Sippe könne von einem „Individualismus“ keine Rede sein.[^2]
* Und deshalb war und ist es ganz aussichtslos, der germanischen Hausherrschaft mit den „politischen“ Gewächsen des ius publicum und privatem beikommen zu wollen.[^3]

Diesem bloß „politischen“ Denken“ wollte Eugen Rosenstock die Binde von den Augen reißen. Auch der Mann, der nicht mehr an seine Sippe gebunden sei, stehe in einem „Rechtskomplex“, der beide Seiten verpflichte. In einer Fußnote griff er in die Diskussion über die Reichweite des Privatrechts ein: „So rücksichtslos hat es das Privatrecht heute nur mit den einzelnen zu tun, daß in den Ländern des sogenannten Wohnsitzprinzips ja sogar die Staatsangehörigkeit für seine Geltung belanglos wird. Durch den Begriff der „Person“ erkennt also das moderne Privatrecht den Begriff einer alle Menschen umfassenden „Menschheit“ als rechtliche Tatsache an.[^4]

[^1]: Die Interpretation des Mittelalters aus privatrechtlicher oder staatlicher Perspektive lag vielen der zeitgenössischen Kontroversen über die „Kulturgeschichte“ oder den „Staat des Mittelalters“ zugrunde: Exemplarisch ist der niemals enden wollende Streit zwischen Karl Lamprecht und Georg von Below in den 90er Jahren des 19. Jahrhunderts: Chickering, Roger, Karl Lamprecht. Das Leben eines deutschen Historikers (1856-1915), Franz Steiner Verlag 2021, 222ff. Immerhin liegt Eugen Rosenstocks Kappung dieses gordischen Knotens der Mediävistik Otto Brunner zwanzig Jahre voraus.

[^2]: Eugen Rosenstock, Königshaus und Stämme in Deutschland zwischen 911 und 1250, Leipzig: Felix Meiner Verlag 1914, S.391.

[^3]: Eugen Rosenstock, Königshaus und Stämme in Deutschland zwischen 911 und 1250, Leipzig: Felix Meiner Verlag 1914, S.401.

[^4]: Eugen Rosenstock, Königshaus und Stämme in Deutschland zwischen 911 und 1250, Leipzig: Felix Meiner Verlag 1914, S.379f.

Auch im Namen Jerusalem steckt ein Haus!

Zu vergleichen immer die Definition von Souveränität von Bodin.
