---
title: Jahrestagung 2011
created: 2011
category: jahrestagung
thema: Der 9. November 1918ist er so radikal, wie ihn Eugen Rosenstock-Huessy erlebt hat, unser Schicksal?
---
![Haus Salem]({{ 'assets/images/haus_salem.jpg' | relative_url }}){:.img-right.img-large}

Die Jahrestagung 2011 findet vom 15. -17. April in Bielefeld-Bethel unter dem Titel:

### {{ page.thema }}


In der „Hochzeit des Kriegs und der Revolution” 1920 hat Eugen Rosenstock-Huessy ausführlich und existentiell mitgeteilt, wie er die Jahre 1917 bis 1919 im Ersten Weltkrieg bis zum Friedensschluß erlebt hat. Immer wieder hat er den Zusammenbruch am 9. November 1918 als das entscheidende Datum angesprochen: die Gesprächszusammenhänge, die bis dahin das geistige und soziale Leben bestimmt hatten, brachen so zusammen, daß jeweils die Sprecherseite scheinbar bestehenblieb, die Hörerseite aber verlorenging. Der Pastor verlor seine Gemeinde (Religio depopulata), der Professor verlor das Ohr des Rates am Fürstenhof und damit den ernstlichen Grund für alles Forschen, der Offizier verlor das Gegenüber der Zivilen (oder umgekehrt), die Herrschenden verloren ihr Herkommen, mit Art und Gebärde, die Nation verlor den Chor der anderen Nationen, der erst zusammen ein ganzes ausmachte, die Männer, die jahrelang im Krieg waren, verloren das Gehör der Frauen (und umgekehrt), das Universale (die Kirche) verlor das Gehör des Lokalen (der Ortsgemeinde), die Künstler verloren die aufmerksam Hörenden, Schauenden, Lesenden, die in dem Kunstwerk das eigene Erstaunen wiederfinden möchten.

Zum Frieden sollten, in den drei Schlußkapiteln, neue Gesprächslagen geschaffen werden: zwischen Arbeitgebern und Arbeitern in der Arbeitsgemeinschaft als Form andragogischen Wirkens, zwischen Mann und Frau durch die neue Würde der Tochter, die unmittelbar vom Geist angerührt wird, zwischen den Zeiten und den Räumen, indem der Menschheit in der räumlichen Gleichzeitigkeit das Menschengeschlecht durch die Zeiten als Gesprächspartner gegenübertritt.

Wir wollen also während der Tagung fragen, wie ernst der Verlust der Ämter in Pastor, Professor, Offizier, Politiker, Mann, Künstler unsere eigenen Erfahrungen beschreibt, und wollen finden, wie es mit den drei heilenden Strömen bei uns steht.
