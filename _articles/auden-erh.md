---
title: "W.H. Auden about Rosenstock-Huessy"
category: aussage
published: 2022-01-04
name: Auden
language: english
---

„Rosenstock-Huessy continually astonished one by his dazzling and unique insights.”
>*W.H. Auden*

„I am a poet by vocation and, therefore, do not expect to learn much about Language from a writer of Prose. Yet, half of what I now know about the difference between Personal Speech, based upon Proper Names . . .  words of command and obedience, summons and response, and the impersonal “objective” use of words as a communication code between individuals, I owe to Rosenstock-Huessy . . .  I can only say that, by listening to Rosenstock-Huessy, I have been changed.”
>*W.H. Auden (1970)*
