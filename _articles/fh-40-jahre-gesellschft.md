---
title: Fritz Herrenbrück's Tagungseröffnung zum 40jährigen Bestehen der Gesellschaft
category: einblick
---
![Fritz Herrenbrück]({{ 'assets/images/Fritz-Herrenbrueck.jpg' | relative_url }}){:.img-right.img-small}

**Begrüßung in Bethel, Freitagabend, 24.10.2003**

Zu unserer Jubiläumstagung aus Anlaß des 40-jährigen Bestehens unserer Eugen Rosenstock-Huessy Gesellschaft begrüße ich Sie sehr herzlich.

Insbesondere begrüße ich den Executive Director des US-amerikanischen Eugen Rosenstock-Huessy Fund, *Mark Huessy*, mit seiner Frau *Frances* sowie Herrn *Horst-Klaus Hofmann* als Vorsitzenden der Paul Schütz Gesellschaft, sowie deren Schriftführer, Herrn *Michael Nürck*. Seien Sie sehr herzlich willkommen geheißen!

Weiterhin begrüße ich *Herrn Professor Nipkow*. Er wollte mit seiner Gattin kommen; aber sie ist leider erkrankt. So bitten wir Sie, ihr gute Genesungswünsche auszurichten. Herr Professor Nipkow wird morgen den Festvortrag halten unter dem Titel: ***»Weisheit in Kindheit und Alter und zur metaphorischen Tiefengrammatik der Bildung im Licht Eugen Rosenstock-Huessys«***. Ich danke Ihnen sehr, daß Sie anläßlich des 40-jährigen Bestehens unserer Gesellschaft diesen Vortrag halten wollen und daß Sie mir schon etwas Einblick gaben in seinen Aufbau!

Entschuldigen will ich unsere Vorstandsmitglieder Gudrun Elisabeth Lemm und Ute Freisinger-Hahn. Sie mußten leider beide kurzfristig absagen.

Bevor wir unser Abendprogramm beginnen will ich noch etwas der Frage nachgehen: Wer sind *wir*, die Eugen Rosenstock-Huessy Gesellschaft? Gestatten Sie mir, diese Frage[^1] kurz an einem Beispiel zu erläutern:

Im Jahr 1922 schreibt Eugen Rosenstock-Huessy für die Zeitschrift: *Neuwerk* den Artikel ***»Paracelsus in Basel«***. Wer diese Ausführungen liest kann feststellen, daß Paracelsus und Eugen Rosenstock-Huessy hier eine homogene Einheit darstellen. Der eine steht für den anderen ein; oder, anders gesagt: Eugen Rosenstock-Huessy schreibt diese Zeilen mit seinem *Herzblut*. Ein Jahr nach diesem Zeitschriftenaufsatz »Paracelsus in Basel«, also im Jahr 1923, gab er gemeinsam mit Richard Koch, dem Arzt von Franz Rosenzweig, das Paracelsus-Büchlein ***»Krankheit und Glaube«*** heraus. Er forscht also intensiv weiter über Paracelsus. Der Zeitschriftenartikel »Paracelsus in Basel« wird dann neu gedruckt im »Alter der Kirche« 1928. Es gibt Notizen aus dem Jahr 1933, daß Eugen Rosenstock-Huessy diesen Artikel in seinem geplanten Sammelband zum dritten Mal veröffentlichen will. Da kamen ihm aber widrige Zeitumstände dazwischen.[^2]

Schließlich sei noch auf Eugen Rosenstock-Huessys Lehre vom *Zeitenspektrum*, der das »Paramirum« des Paracelsus zu Grunde liegt, hingewiesen.[^3]

In der mir bekannten Literatur zu Paracelsus führen diese für ihn so wichtigen Artikel ein Schattendasein. Wir können diesen Tatbestand auch als tiefes Schweigen benennen, auch im Blick auf Eugen Rosenstock-Huessys Lehre vom Zeitenspektrum.[^4]

Andererseits ist positiv zu bemerken, dass Eugen Rosenstock-Huessys im September 1970 Ehrenmitglied der Internationalen Paracelsus-Gesellschaft wird.[^5] Welche Rolle der damalige Generalsekretär Sepp Domandl dabei spielt, ist mir noch nicht klar. Jedenfalls gibt dieser ebenfalls im Jahr 1970 ein Büchlein heraus: »Erziehung und Menschenbild bei Paracelsus« und erwähnt darin Eugen Rosenstock-Huessys überaus positiv.[^6]

In seinem Buch von 1970 kann Sepp Domandl überzeugend ausführen, dass Paracelsus nicht nur an einer Zeitenwende steht, sondern sie auch vorbereitet,[^7] ebenso wie Johann Wolfgang von Goethe.[^8]

*Heute* können wir auf das Naheliegendste, was sich denken lässt, hinweisen, nämlich auf Eugen Rosenstock-Huessys *nachgoethesche* Soziologie – also auf die Linie (Theophrastus Bombastus von Hohenheim, genannt:) ***Paracelsus – Johann Wolfgang von Goethe – Eugen Rosenstock-Huessy***. Das gemeinsame Stichwort heißt, in der Terminologie von Paracelsus: *Erfahrenheit, experientia*, in der Sprache von Eugen Rosenstock-Huessy: *Im Kreuz der Wirklichkeit*.

Theophrastus Bombastus von Hohenheim wurde als *Caco*phrastus verspottet. Im „Alter der Kirche” steht: „Man kann kein Buch von Theophrastus in die Hand nehmen, ohne sich sofort zu überzeugen, daß der Mensch wahnsinnig war.” Das schreibt ein Arzt, Karl Georg Neumann, im Jahr 1838.[^9]

Goethes Naturforschungen werden im Jahr 1882 von dem angesehenen Physiologen Emil Dubois-Reymond in seiner Berliner Rektoratsrede als eine „totgeborene Spielerei eines autodidaktischen Dilettanten” abgetan.[^10]

Als Eugen Rosenstock-Huessy nach Harvard kommt, weiß man ihn nicht einzuordnen, er sieht sich auf einem wissenschaftlichen Verschiebebahnhof.[^11] Zu seinem 70. Geburtstag betitelt die Frankfurter Allgemeine Zeitung (nicht der Verfasser!) den Geburtstagsgruß wenig souverän mit *„Das Zaubermännchen aus Vermont”*.[^12] Weil sie eine neue Zeit vorbereiten, erfahren alle drei tiefen Spott und scharfe Ablehnung.

Aber so wie sich die Einstellung zu Paracelsus änderte, dass er als Wegbereiter einer neuen Zeit erkannt wurde,[^13] als *Wegzeiger*, ebenfalls (bei dem Naturforscher) Johann Wolfgang von Goethe,[^14] und zwar radikal, so doch hoffentlich auch im Blick auf Eugen Rosenstock-Huessy.

Eugen Rosenstock-Huessy schrieb »Das Dreigestirn der Bildung«.[^15] Wer ***Paracelsus – Johann Wolfgang von Goethe – Eugen Rosenstock-Huessy*** in einem Atemzug nennt, der hat hoffentlich nicht nur das *„Dreigestirn der Mißachtung, der Verlästerung”* vor Augen, sondern auch das *„Dreigestirn zukunftsweisender Wege”*.

Otto von der Gablenz rezensierte die »Soziologie«.[^16] Bei seiner ironisierenden Darstellung hat er ungewollt darin Recht, wenn er Eugen Rosenstock-Huessy einen „neuen Propheten” nennt[^17] – und seine Anhänger, also *uns*, „dilettantisch”. Aber wir hörten auch, dass Johann Wolfgang von Goethe als Dilettant von Emil Dubois-Reymond gelästert wurde. Insofern besteht die Hoffnung, dass das Gablentz'sche Verdikt sich doch zu einem Ehrentitel von uns wandeln könnte.

So eröffne ich den Abend zu unserer Jubiläumstagung aus Anlaß unseres 40-jährigen Bestehens der Eugen Rosenstock-Huessy Gesellschaft mit großer Dankbarkeit im Blick auf alle, die bei den Vorbereitungen halfen, im Blick auf alle, die sich äußern werden, bei Ihnen allen, die die Reise hierher nicht gescheut haben.

[^1]: S. dazu auch Mitteilungen der Eugen Rosenstock-Huessy Gesellschaft [*im folgenden abgekürzt:* »Mitteilungen«], 14. Folge, Januar 1971, S. 1ff., bes. S. 2f. (vgl. auch ebd. 21. Folge, Januar 1975, S. 5 sowie 16. Folge, Februar 1972, S. 11f.).

[^2]: Jetzt auch in »Heilkraft und Wahrheit«, Frankfurt am Main 1952, S. 114ff.

[^3]: S. auch das Typoskript eines Vortrags über Paracelsus (vgl. »Mitteilungen«, 22. Folge, November 1975, S. 14 [Chr. Michel] – ist es die Einleitung zu den »Unsichtbaren Krankheiten«?) sowie sein Artikel: »Paracelsus«, *in:* The American Peoples Encyclopedia, vol. XV, New York 1962, p. 26f. [vol. XIV, New York 1968, p. 301f.].

[^4]: S. »Heilkraft und Wahrheit«, S. 156ff. – Kurt Goldammer, Paracelsus-Bild und Paracelsus-Forschung, *jetzt in:* Ders., Paracelsus in neuen Horizonten. Ges. Aufsätze, Wie 1986, S. 358-375, erwähnt S. 366 flüchtig diese Ausführungen. – Zum Zeitenspektrum vgl. zum Beispiel das blutleere Referat bei K. Goldammer, Der Beitrag des Paracelsus zur neuen wissenschaftlichen Methodologie und zur Erkenntnislehre, *jetzt in:* ebd., S. 229-249, *hier:* S. 236f.

[^5]: Vgl. »Mitteilungen«, 13. Folge, November 1970, S. 2f.

[^6]: Sepp Domandl, Erziehung und Menschenbild bei Paracelsus. Anfänge einer verantwortungsbewußten Pädagogik (Salzburger Beiträge zur Paracelsusforschung, Heft 9), Wien 1970, S. 80f. (Zitat von S. 80f. s. vorige FN, S. 3).

[^7]: Vgl. dazu die ambivalente Stellungnahme von Richard Koch, Ärztliche Studie über zwölf theologische Schriften Hohenheims aus der Philosophia magna, *in:* Archiv für Geschichte der Medizin 19 (1927), S. 169-186, bes. S. 179: „Trotz allem, in der Reihe prometheischer Menschen, auch nur faustischer Naturen steht Hohenheim nicht. Letzten Endes blieb er wie in naturwissenschaftlicher Kritik über Wahr und Falsch auch in Glaubenssachen hinter den Größten seiner Zeit zurück, und wir müssen uns damit abfinden, daß ein so Zeitgebundener auf anderem Gebiet ein zeitloser Mensch sein konnte. Das gilt auch von dem Arzt Hohenheim. Er bedeutet in wichtigen Dingen gegen Galen einen Rückschritt und ist trotzdem in der Geschichte der wichtigen ärztlichen Gedanken und Erkenntnisse ein epochemachender Mensch.”

[^8]: S. Domandl [FN 6], S. X: „Schon Friedrich Gundolf und Ferdinand Weinhandl haben nachgewiesen, daß Goethe und Paracelsus in vielen wesentlichen Zügen ihres Denkens übereinstimmen. Das Wissen um diese innere Verwandtschaft konnte hier durch zusätzliche Hinweise vertieft werden.” Ebd. S. 5: „Die Parallelität in der Erschließung des Paracelsus und Goethes wird ganz deutlich im Werke Friedrich Gundolfs über Goethe; immer wieder zeigt er die enge Verwandtschaft beider auf” [Friedrich Gundolf, Paracelsus, Berlin 1927; Ders., Goethe, Berlin 1916; Ferdinand Weinhandl, Die Metaphysik Goethes, Berlin 1932].

[^9]: Carl Georg Neumann, Von den Krankheiten des Menschen. Specieller Theil oder Specielle Pathologie und Therapie, Bd. IV: Krankheiten der sensiblen Sphäre, Berlin ²1838; § 318, S. 510 [im Abschnitt über „Wahnsinn”]; vgl. »Das Alter der Kirche«, S. 740 [II 173].

[^10]: Emil Dubois-Reymond, Goethe und kein Ende. Rede bei Antritt des Rektorats der koenigl. Friedrich-Wilhelms-Universitaet zu Berlin am 15. October 1882, Berlin 1883, S. 21 (auch in: Reden von Emil du Bois-Reymond in zwei Bänden, Bd. II, Leipzig ²1912, S. 173).

[^11]: Vgl. Eugen Rosenstock-Huessy, Ja und Nein, Heidelberg 1968, S. 131.133-137.

[^12]: Joachim Günther, Das Zaubermännchen von Vermont, *in:* Frankfurter Allgemeine Zeitung Nr. 50, v. Mittwoch, 28. Februar 1973, S. 28 (*s*. »Mitteilungen«, 18. Folge, Mai 1973, S. 13). In einem Leserbrief zu diesem FAZ-Artikel schreibt Waldemar Höpfner u. a. (FAZ v. 13. März 1973): „»Der große Anreger« war der Ehrentitel, den selbst diejenigen ihm zuerkannten, die mit seiner Art nichts anzufangen wußten” (vgl. noch »Mitteilungen«, 9. Folge, November 1968, S. 4 und 26. Folge, Oktober 1977, S. 6f.).

[^13]: Vgl. R. Koch [FN 7], S. 169: „Wie er über den Leib hinaus im Geiste weiterkämpfen wollte, so ist sein Schicksal wirklich geworden” und aus dem Paragranum (1530): „ich wil euchs dermaßen erleutern und fürhalten, das bis in den lezten tag der welt meine gschriften müssen bleiben und warhaftig, und die euer werden voller gallen, gift und schlangen gezücht erkennet werden und von den leuten gehasset wie die kröten. es ist nit mein will, das ir auf ein jar sollet umbfallen oder umbgestoßen werden, sonder ir müsset nach langer zeit euer schand selbs eröfnen und wol durch die reutern fallen. mer wil ich richten nach meinem tot wider euch dan darvor. und ob ir schon mein leib fressent, so habt ir nur drek gefressen: der Theophrastus wird mit euch kriegen on den leib” (Sudhoff VIII, S. 200f.).

[^14]: Vgl. z. B. Hermann Helmholtz, Goethes Vorahnungen kommender naturwissenschaftlicher Ideen, in: Die Naturwissenschaften (Berlin) 20 (Heft 13, 25. März 1932), S. 213-223 [= Rede v. 11.6.1892].

[^15]: Die Arbeitsgemeinschaft 2 (1921), S. 177-199.

[^16]: Otto von der Gablenz, Metapolitik. Zur Soziologie Eugen Rosenstocks, *in:* Zeitschrift für Politik NF 6 (1959), S. 277-284; vgl. »Mitteilungen«, 18. Folge, Mai 1973, S. 8f. und ebd. 26. Folge, Oktober 1977, S. 9 [zu 28.12.59].

[^17]: Vgl. Georg Müller, in: »Mitteilungen«, 18. Folge, Mai 1973, S. 13: „ERH gehört dem Frömmigkeitstyp prophetischer Prägung an und hat mit Mystik nichts zu schaffen …”.
