---
title: "James Eric Lane about Rosenstock-Huessy"
category: aussage
published: 2022-01-04
name: Lane
language: english
---


„Rosenstock-Huessy’s Speech and Reality is to sociology what Galileo’s “Discourse on Two New Sciences” is to modern mathematical physics because both books transform philosophy — natural in the case of Galileo and social in the case of ERH — into a positive science.”
>*James Eric Lane (2006)*
