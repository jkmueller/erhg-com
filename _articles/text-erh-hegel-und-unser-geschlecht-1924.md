---
title: "Rosenstock-Huessy: Hegel und unser Geschlecht (1924)"
category: online-text
org-publ: 1924
---
Paul Alfred Merbach hat im Leipziger Wolkenwanderer-Verlag soeben aus Georg Friedrich Wilhelm Hegels Werken einen Sammelband »DerStaat« herausgegeben. Das junge, durch den Krieg aus der Tradition geschleuderte Geschlecht wird hier noch glücklich in die Vorstellungen eingeführt, die das letzte Vorkriegsjahrzehnt schon als »Neuhegelianismus« durchlebt hat.

Das neunzehnte Jahrhundert ist eine Art Inhaltsverzeichnis zu dem vorhergehenden Jahrtausend gewesen. Daher seine Stillosigkeit, seine Fin-de-siecle-Stimmung, sein Historismus, seine Romantik. Wir haben an diesem Inhaltsverzeichnis nicht mehr mitzuschreiben. Der 9. November 1918 hat uns gezwungen, - wird uns zwingen - ein ganz neues Manuskript der Volksgeschichte anzufangen. Was sollen wir da einstweilen die Kinder lehren? In den Schulen erfahren sie jetzt nur, wie alles nicht gewesen ist. Das perikleische Zeitalter ist nicht das goldene gewesen; Hellas und Rom sind nicht klassisch gewesen. Die Bekehrung der Germanen unter das Kreuz ist eine bedauerliche »judaisierende« Verirrung, die Reformation war der Beginn unseres Untergangs, der Rationalismus des achtzehnten Jahrhunderts hat alles zersetzt. Der Unglaube des Liberalismus, unserer Väter, ist besonders verdammlich. So lehren die Wissenschaften, so lernen die Kinder. Das ist eine große Pleite.

Man hat wohl zu viel aus der Geschichte lernen wollen: Stil, Wahrheit, Schönheit, ja die ewige Seligkeit selbst. Das ging nicht und geht nicht. Jetzt begreift man mit einem Male gar nichts mehr. Die Geschichte scheint immer nur schief zu gehen, hin zum Untergang. Da kommt so eine Sammlung wie diese gerade recht. An ihr läßt sich zeigen, was die Geschichte allein lehren kann: Wie Menschen der ewigen Aufgabe des Menschen genüge tun, wie nicht.

Jenes neunzehnte Jahrhundert des Kulturinhaltsverzeichnisses, dieser Weltmusterkatalog geht uns unmittelbar nichts mehr an. Aber mittelbar um so mehr. Denn immerhin, ihm verdanken wir alles, was wir sind, Dasein, Kenntnisse, Können und Ausbildung. Das Sammelsurium ihrer Inhalte und Weltanschauungen wirkt sich freilich erst heute in seiner ganzen Giftigkeit aus. Wir sehen erst jetzt, daß man von einem Inhaltsverzeichnis, einer noch so reich besetzten »wertfreien« Speisekarte nicht leben kann. Aber welche Leistung gehörte doch dazu, dieses Geisterchaos zusammenzuhalten, so daß die Zeit überhaupt bis an uns gelangt ist? Was für eiserne Klammem und notwendig gewesen, eine so aus den Fugen gegangene Geisterwelt bis 1914 in einem leidlichen Gefüge zu erhalten?

Das ist die Frage, die uns angeht. Wir haben andere Aufgaben. Wir müssen dieser Vielgeisterei zu Leibe rücken und den Mächten und Dämonen, die sie erzeugt hat. Aber wir fragen uns, ob die Menschen vor 1800 wohl ebenso gebangt haben vor ihrer Aufgabe, wohl ebenso gerungen haben um ihren Weg. Es stärkt uns, wenn wir das Ewige im Menschen, das: mit Furcht und Zittern das eine schaffen und das «einfältig wandeln vor seinem Gotte« dem Menschen auch damals zugerufen und von den Rechten und Gerechten befolgt sehen.

Und es klärt, wenn wir sehen, daß sie ihre Mittel voll eingesetzt und erschöpft haben, wir also zu unseren — nicht zu ihren — greifen sollen und müssen. Denn Erfolg haben jene Verklammerer und Zusammenbinder des Chaos von 1800 nur deshalb gehabt, weil sie originelle Mittel, für jene Zeit neue und unerhörte Mittel angewendet haben. Neu und originell, ursprünglich und unmittelbar aus dem Ewigen in uns werden also auch die Mittel entspringen müssen, die uns zum Leben trotz aller Todesgefahren verhelfen.

Das originelle, unerhörte Mittel jener Zeit aber hieß »Restauration«. Nur der Restaurationsgedanke hat die Kraft gehabt, hundert Jahre wildester Umwälzungen aller Lebensverhältnisse in leidlicher Ordnung zu erhalten.

Die »Restauration« widersprach allen »Tendenzen« der Zeit. Sie warf sich ihnen entgegen. Sie bedeutete einen Bruch mit dem Selbstverständlichen. Sie mußte sich aber in diesem selbstverständlichen Zeitgeist irgendwie verankern, um mit ihm ringen, um ihn abfangen zu können.

Es ist eben Hegel, der diese Verankerung der Restauration im Zeitgeist der Aufklärung zuwege bringt. Das hat ihm die ungeheure Wirkung verschafft. Er ist liberal und konservativ, Aufklärer und Traditionalist, Geist-und Machtpolitiker, kurz ein vollkommener Widerspruch, aber eben jener Widerspruch, der allein über das Chaos hinwegzutragen vermochte! Er überspringt und springt um, er macht das Unmögliche möglich, daß ein Hauptteil der alten 1789 erschütterten alten Welt, des ancien regime, daß der Einzelstaat hinübergelangte in die neue Zeit!
Am Anfang seines Wirkens (1802) steht der Aufschrei: »Deutschland ist kein Staat mehr«. Am Ende steht die geistige Ausrüstung Preußens für seine neue Hegemonie und damit für die preußische Spitze in Bismarcks Kaiserreich.

Wie Hegel das zuwege gebracht hat, lese man in dem Bande nach, der übrigens ohne Franz Rosenzweigs Standardwerk »Hegel und der Staat« nicht möglich gewesen wäre. Rosenzweigs zwei imponierende Bände sind eben nicht zufällig die einzige voll ausgereifte Frucht jenes Neuhegelianismus von 1910 bis 1914, von dem wir eingangs sprachen. Sie heben jene Leistung Hegels ans Licht, die ihn unsterblich gemacht hat. Was uns an Hegels Erscheinung - wie an Idealismus und Romantik überhaupt - fasziniert, ist eben dies, daß ihnen für ein Jahrhundert die »Rettung«, die Restauration gelungen ist. Idealismus ist freier Vernunftgebrauch; Romantik ist Beugung unter die vergangene Geschichte. Das Kunststück: aus freiem Vernunftgebrauch Beugung unter das Vergangene zu fordern, hat Hegel fertig gebracht. Damit wurde er zur Großmacht deutschen Geistes- und Staatslebens.

Auch wir Heutigen beginnen mit dem Aufschrei: »Deutschland ist kein Staat mehr«. Aber wir sind weder Idealisten noch Romantiker, können beides nicht sein. Hegel war ja Idealist als Erbe Kants, als Sohn des achtzehnten Jahrhunderts; er ist Romantiker und Restaurator als Vater des historischen Katalogjahrhunderts mit dem nackten Machtstaat als Panzergehäuse der »Kultur«.
Wir sind Kinder einer wirtschaftlich-materiellen »realistischen« Zeit. Von Idealismus wurde zwar reichlich geredet in deutschen Landen zwischen 1870 und 1914. Aber das schwamm nur wie Schneeschaum auf der Biersuppe des Alltags. Der Alltag, aus dem wir kommen, war so »real«, daß er stumm, unverklärt, nackt, formlos, stillos, materiell war: Er hieß PS PS PS, Soldaten Soldaten Soldaten, Schiffe Schiffe Schiffe, Telefon Telefon Telefon, Großstadt, Großmacht, Größenwahn.

Und heut überschlägt sich dieser stumme, unverklärte Realismus in Radio, Kino und Zeppelin.
So ist unsere wahre Abstammung. Aus dieser Kindschaft befreit uns kein verspätetes idealistisches Geschwätz noch ein romantischer Phrasenschwall.
In welche Zeit hinein sollen wir nun als Männer gehören? Welches Gegenmittel ist uns heut zur Hand, welcher Widerspruch ist uns heut geboten?

Hegels Ausweg war die Vaterwerdung des Sohnes, die romantische Beugung der freien Vernunft unter das Gesetz der Vergangenheit. Dieser Saltomortale vom Sohnestypus in den Vatertypus hinüber - Restaurieren heißt den Vater spielen und Patriarchalisieren - hat seine Wirkung getan. Aber diese Wirkung ist ja gerade nunmehr erschöpft. Uns hilft kein Saltomortale. Jetzt heißt es gehen. Auf den Sohn, auf den freien Idealisten folgt biographisch nicht der Vater. Sondern dazwischen liegen zwei andere Stufen: Bräutigam und Ehemann, Braut und junge Frau.
Das aber ist die Stunde der Verwandlung. Was der Jüngling begeistert gewähnt und gedacht, das bewährt der Ruf, der an den Bräutigam ergeht, das begründet er unwiderruflich bei seinem Eheschluß.

 Wir suchen nach Verwandlung einer ganz diesseitigen, ganz technischen, ganz realen Welt. Nur die Haltung des Liebenden und Verwandelnden kann da helfen, nicht die des Restaurators!
Möchten wir ebenso bestehen bei unserer Aufgabe wie die Idealisten und Romatitiker von 1800 bei der ihrigen, wie Georg Friedrich Wilhelm Hegel.

Eugen Rosenstock

= = = = = =

Abgeschlossen am 24. Dezember 1924  
Verantwortlich für die Redaktion: Efraim Frisch, München, Theresienstr. 12


[„Hegel und unser Geschlecht” als PDF-Scan](http://www.erhfund.org/wp-content/uploads/142.pdf)
