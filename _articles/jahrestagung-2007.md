---
title: Jahrestagung 2007 in Würzburg
created: 2007
category: jahrestagung
thema: Des Christen Zukunft oder Wir überholen die Moderne
---
![Frankenwarte]({{ 'assets/images/Frankenwarte_Aussichtsturm_Würzburg.jpg' | relative_url }}){:.img-right.img-large}

### {{ page.thema }}

Freitag, 23. März, bis Sonntag, 25. März 2007\
Akademie Frankenwarte Würzburg\
Leutfresserweg 81-83, D-97082 Würzburg\
[Akademie Frankenwarte](https://www.frankenwarte.de)"

Freitag 23 . März 2007\
17.00 - 18.00 Ankunft\
18.00 Abendessen\
19.30 - 22.00 1. Sitzung: Andreas Schreck stellt aus der soziologischen Analyse in
Des Christen Zukunft die Frage, die dann in drei Gruppen unter
uns sein soll: Was sind unsere Erfahrungen mit Kirchengemeinde oder
Gemeinde (das ist der Zusammenschluß derer, die ein Unglück
gemeinsam hinter sich haben). (Die einleitende Stimme leitet dann das
folgende Gespräch.)

Die Empfänger von e-mails erhalten die Auszüge zu den drei Fragen
(leicht bearbeitete Übersetzung von Eckart Wilkens) schon mit dieser
Einladung, die anderen Empfänger nur auf die Mitteilung hin, daß sie
zur Tagung kommen wollen.

Samstag 24 . März 2007\
8.30 Frühstück\
9.30 -12.00 2. Sitzung: Eckart Wilkens stellt die zweite Frage aus dem Kapitel
darüber, was denn die besondere Zeitqualität der christlichen
Zeitrechnung ist, vor: Haben wir eine Erfahrung von der christlich
geprägten Zeit in unserem eigenen Erleben? Wieder in drei Gruppen,
die aber neu bestimmt werden.

12.30 Mittagessen\
15.00 - 17.30 Sitzung: Wilmy Verhage stellt die dritte Frage aus dem Kapitel:
Buddha, Laotse, Abraham und Jesus: Wie gehen wir mit der
Herausforderung um, die durch Buddha, Laotse und Abraham
geschaffenen Lebensformen als für uns selber unerläßlich zu erproben?
Wiederum in drei Gruppen, neu bestimmt.

18.00 Abendessen\
19.30 - 21.00 Sitzung: Feico Houweling erzählt uns, was er zum 100. Geburtstag Helmuth
James von Moltkes zusammengefügt hat, um es im Goethe-Institut in
Rotterdam gegenwärtig zu machenwir schließen uns dem an.

Sonntag 25. März 2007\
8.00 Andacht\
8.30 Frühstück\
9.30 - 10.00 Abschlußgespräch\
10.15 – 12.30 (Mitgliederversammlung)\
12.30 Mittagessen\
13.30 Abschied

Die Jahrestagung findet statt von Freitag, 23. März 2007, 18 Uhr, bis Sonntag, 25.
März 2007, 10.00 Uhr (anschließend: Mitgliederversammlung)
