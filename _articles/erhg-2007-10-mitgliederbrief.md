---
title: Mitgliederbrief 2007-10
category: rundbrief
created: 2007-10
summary:
zitat: |
  >„Es ist nämlich wirklich nicht einfach, an den lebendigen Gott zu glauben. Von den Menschen, die glauben an Gott zu glauben, glauben nur wenige an den lebendigen Gott. Und viele glauben an den lebenden Gott, obschon sie behaupten, nicht an Gott zu glauben. Die Grenze im Glauben läuft viel seltener zwischen Gottgläubigen und Atheisten als zwischen denen, die ihren Gott hochleben lassen und dadurch zum Spott machen, und denen, die dem
  >lebendigen Gott Leben, Sieg und Herrschaft einräumen.”
  >*Eugen Rosenstock-Huessy, Vivit Deus, in: Das Geheimnis der Universität, S. 278*
---

## Eugen Rosenstock-Huessy Gesellschaft e.V.

{{ page.zitat }}

**Vorstand/board/bestuur:**\
*Dr. Eckart Wilkens (Vorsitzender);*\
*Wilmy Verhage; Andreas Schreck; Dr. Jürgen Müller; Lothar Mack*\
*Antwortadresse: Dr. Eckart Wilkens: Everhardstraße 77, 50823 Köln, Deutschland*\
*Tel: 0(049) 221 511265*

***Brief an die Mitglieder Oktober 2007***

**Inhalt**

{{ page.summary }}


[zum Seitenbeginn](#top)
