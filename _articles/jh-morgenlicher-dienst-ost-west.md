---
title: "Jehuda Halevi: Morgendlicher Dienst - Zwischen Ost und West"
created: 1991
category: veroeff-elem
published: 2024-04-23
---
### Stimmstein 6, 2001

#### Mitteilungsblätter  2001

&nbsp;  

### Jehuda Halevi

&nbsp;  

#### Morgendlicher Dienst

Morgengestirne all  &nbsp; &nbsp; &nbsp; &nbsp;  zu Dir aufsingen,

Denn ihres Strahls Urquelln  &nbsp; &nbsp; &nbsp; &nbsp;  aus Dir urspringen.

Und die Gottes-Sprossen,  &nbsp; &nbsp; &nbsp; &nbsp;  festgebannt auf ihre Wacht,

Nachts, Tags den Überschwang  &nbsp; &nbsp; &nbsp; &nbsp;  des Lobs ausschwingen.

Und die Reichs-Genossen  &nbsp; &nbsp; &nbsp; &nbsp;  ernten es von ihnen, wenn

Früh mit der Frühe sie  &nbsp; &nbsp; &nbsp; &nbsp;  in Dein Haus dringen.

&nbsp;  
&nbsp;  

#### Zwischen Ost und West

Mein Herz im Osten, und ich  &nbsp; &nbsp; &nbsp; &nbsp;  selber am westlichsten Rand.

Wie schmeckte Trank mir und Speis'?  &nbsp; &nbsp; &nbsp; &nbsp;  wie? dran Gefalln je ich fand?

Weh, wie vollend ich Gelübd'?  &nbsp; &nbsp; &nbsp; &nbsp;  wie meine Weihung? da noch

Zion in römischer Haft,  &nbsp; &nbsp; &nbsp; &nbsp;  ich in arabischem Band.

Spreu meinem Aug' alles Gut  &nbsp; &nbsp; &nbsp; &nbsp;  spanischen Bodens, indes

Gold meinem Auge der Staub  &nbsp; &nbsp; &nbsp; &nbsp;  drauf einst das Heiligtum stand!


Aus: Franz Rosenzweig: Sechzig Hymnen und Gedichte des Jehuda Halevi − Deutsch. Konstanz (Oskar Wöhrle Verlag) o. J., S. 66 und 92. Der im Editorial dieses Heftes zitierte Kommentar Rosenzweigs ebenda Seite 151 und 163.
