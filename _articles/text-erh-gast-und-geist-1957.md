---
title: "Rosenstock-Huessy: Gast und Geist (1957)"
category: online-text
published: 2022-01-15
org-publ: 1957
---

**"Gast und Geist" oder "Stammeszunge und Geisteszunge"** [^1]

[^1]: Titel vom Abschreiber Lise van der Molen

Nachdem es sich gezeigt hat, dass schon in der Urzeit Sekten entstanden sind, die um des lieben Friedens willen sich aus der Geschichte geflüchtet haben, sind unsere Augen aufgetan. Schon in der Urzeit gibt es vollständige und unvollständige Verbände. Nicht jede Gruppe ist voll lebensfähig aus sich selber hinaus. Ja in der bunten Landkarte historischer Formen sind, die wenigsten vollständig. Die ungeheure Masse ist angelehnt und lebt auf Borg. Jene friedlichen frommen Urkulturen die den Pater Schmidt begeistern sind angelehnt an jene feurigen Kriegerstämme, die um des Ahnherrns willen Rache und Verfolgung in jedes fernste Dickicht tragen. Denn das Feuer der Begeisterung allein hat in die Menschen die Macht heiliger Namen und Verwandtschaftsgrade gekerbt. Mit milder Freundlichkeit wird kein Sprachstamm und keine Mutterzunge erschaffen, sondern nur in wilder Klage und glühender Hingabe. "Aus Feuer ist der Geist erschaffen", singt das Stundentenlied mit Recht. Ja, sogar darin hat dies Lied Recht, dass es die Waffen und die Begeisterung zusammen in den Reim setzt:

| | Aus Feuer ist der Geist erschaffen,
| |  drum schenkt mir süsses Feuer ein.
| |  Die Lust der Lieder und der Waffen,
| |  die Lust der Liebe schenkt mir ein.

Die Liebe zum Helden, die macht den Kriegspfad für die Getreuen ebenso selbstverständlich wie die Ehrung der Toten, und bestimmt die Tänze ebenso richtig wie das Recht der Sühne.

Die Pfingstfeuer erinnern uns heute noch daran, dass der heilige Geist der Christenheit die Stammesfeuer, zurechtgeglüht hat. Auf Pfingsten fällt die Auseinandersetzung und der Sieg unserer Zeitrechnung mit den Stammeszungen. Die feurigen Zungen, die laut der Apostelgeschichte an Pfingsten den Gläubigen, erschienen, müssen dialektisch verstanden werden als die Erlösung von den Stammeszungen

"Und es wurden, von ihnen gespaltene Zungen gesehen, wie von Feuer. Und je eine setzte sich auf einen jeden und alle wurden des heiligen Geistes voll und begannen in verschiedenen Zungen zu reden, je nachdem ihnen der Geist zu verlautbaren verlieh. Es bewohnten aber Jerusalem Juden, fromme Männer aus jedem Stamm der unter dem Himmel lebt. Als diese Stimme sich ereignete, lief die Menge zusammen und war verwirrt, weil jeder einzelne in seiner eigenen Mundart die Christen reden hörte. Und sie gerieten ausser sich, wunderten sich und sagten: Seht doch, alle diese Männer, die hier reden sind Galiläer. Wie kommt es dass jeder von uns die eigene Mundart, die uns angeboren ist, heraushört, Parther und Meder und Elamiter, Einwohner von Mesopotamien, Judaea und Kapadokien, Pontus und Asia, Phrygien und Pamphylien, Aegypten und die Küstländer Nordafrikas, Römer, Juden, Kreter und Araber sind wir hier und hören sie in unseren Zungen die Grosstaten des Gottes aussprechen."

Die Strenge des Geschichtshaushalts stellt den Pfingstgeist gegen die Stammesgeister und den Weihnachtsstern gegen die Sterne der Gräbern der Helden der Reiche zum Neujahr, das Osterlamm gegen das Passahlamm des Auszugs des Volkes Gottes aus Ägypten.

Das pfingstliche Feuer entspricht also den Stammesfeuern notwendiger Weise, sonst würde noch heut ein jeder von uns nur die Geheimsprache seines Stammes sprechen und würde sogar seinen Namen vor seinen Feinden geheim halten. Der eigentliche Name des antiken Roms war ein Geheimnis und wir kennen ihn bis heut nicht.

Denn nur innerhalb eines Stammes brennt das Feuer der Stammeszunge. Die Stammessprachen sind Geheimsprachen in dem Sinne, dass ihre Kenntnis durch Fremde nicht wünschenswert ist. Sprechen heisst ja Mitglied sein. Unordnung tritt ein, wenn jemand Basuto redet ohne Basuto zu sein.

Das ist das Geheimnis der Gastfreundschaft im Stamm. Hat ein Fremder die Stammessprache erlernt, so ist er damit selber schon Freund. Die Indianer und die Südseeinsulaner ernennen alle ihnen freundlichen Weissen zu Ehrenmitgliedern. Das ist kein Luxus. Seine Kenntnis  ihrer Sprache muss unschädlich gemacht werden. Da niemand die Sprache sprechen soll, der nicht von dem selben Geist erfüllt ist, so ist die Gastfreundschaft die Verwandlung von Feind in Freund.

Das Gastrecht gehört daher in die Urgeschichte der Stämme. Denn es erläutert den Sinn des Stammes selber. Er ist Zungenmacht. Soweit die deutsche Zunge klingt, ist ein Geist wirksam. Dem Hostis, Gast, Xenos, wird dieser Geist geliehen, solange er mit den Stammesgenossen spricht. Das unverletzliche Gastrecht bezeugt die Macht der Sprache: Jeders fremden Sprachkraft würde den Frieden der Freundliche Zunge gefährden.

Gastfreunde geniessen den Stammesfrieden, weil das Dasein des Stammes davon abhängt, dass die Stammeszunge jeden, der sie befällt, befeuern und zum Waffenträger einglühen muss. Sonst wäre sie ja machtlos!

Im Sprechen mit Vollmacht befindet sich der Stamm im Daseinskampf. Im namentlichen Sprechen geht es also immer um Leben und Tod eines Verbandes. Im Sprechen setzt sich die Macht des Stammes durch oder sie unterliegt. Es ist eine der seltsamsten Irrtümer des Idealismus, die beschränkende Macht der Eigennamen zu übersehen.

Jean Jacques Rousseau war überzeugt, ein Ich, ein Mensch zu sein. Aber wer Jean Jacques Rousseau heisst, den heissen die Deutschen einen Welschen, er mag das wollen oder nicht. Namen beschränken. In "Zirkus Mensch" heisst der Held symbolisch "Mensch". Aber sogar Mensch ist noch ein deutscher Name Homme auf Französisch und Adam auf Hebräisch sind nicht das selbe.

Sein angestammter Name beschränkt jeden Menschen. So ist es zu verstehen, dass das Pfingstwunder die Stammeszungen zu entschränken kam, indem es sie um einen neuen Namen erweiterte und auf einen neuen Namen stellte.

Wir haben es aber in diesem Kapitel noch nicht mit der Entschränkung sondern erst einmal mit der Einschränkung der Stämme zu tun. Erst musste eben mit Feuer und Macht gesprochen werden, bevor diese Macht gebrochen werden dürfte. Der Stamm hat die Familie erzeugt. Seine Feuer müssen erst wieder angefacht sein in unserer Phantasie, bevor wir uns unterfangen dürfen, sie zu läutern.

Dies Kapittel muss also den vollständigen Stamm ergründen, damit er von den Anthropologen Prähistoriker Enthnologen, nicht mit unvollständigen parasitären Gruppen verwechselt werde.

*Manuskript Eugen Rosenstock-Huessy's. Four Wells, Norwich VT. 9 S, Vorarbeit von: Die Sekte, Feuer und Geist. Pfingsten. Gastfreundschaft in: Die Vollzahl der Zeiten S. 323 - 333. Vermutlich aus 1957.*

*Maschinenschrift Lise van der Molen, Winsum, Niederlande 14. 9. 1988*

["GAST UND GEIST" oder "STAMMESZUNGE UND GEISTESZUNGE" als PDF-Scan](http://www.erhfund.org/wp-content/uploads/511.pdf)

[zum Seitenbeginn](#top)
