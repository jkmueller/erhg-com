---
title: "Elly Hartman: Im Rosenstock-Huessy-Huis in Haarlem"
created: 2021
category: veroeff-elem
published: 2021-12-18
---
### Stimmstein 2, 1988
#### Elly Hartman

»Hoe wij ertoe kwamen.«

Toen Sam en ik in 1955 trouwden, werden we trouwe leden van de Hervormde Kerk in Velsen-Noord. Sam was al lid van de kerk en ik deed in 1956 belijdenis, werd gedoopt en hoopte een nuttig en gelovig lid van de kerk van Christus te kunnen worden en in die gemeente gestalte te kunnen geven aan wat ik dacht dat een gelovig mens zou moeten doen om samen met anderen gemeente te zijn. We waren vol goede moed en verwachtten dat er na de oorlog veel starre opvattingen en scheidslijnen tussen de diverse kerken en kerkjes opgeruimd zouden blijken te zijn.

Ook de industrie werd bij de kerk betrokken. Sam was al, sinds 1950, toen hij bij de Hoogovens kwam, betrokken bij het werk van »Evangelie en Industrie«, begonnen door Herman Wallenburg en voortgezet eerst door Jaap Steigstra en daarna door Wim Leenman, die in 1956 industriepredikant werd. Er waren gespreksgroepen, die problemen rondom geloof en industrie naar boven probeerden te halen en de band met de kerk, die bij de meeste mensen die in de industrie werkten, verloren was gegaan, te herstellen. Sam was daar natuurlijk meer bij betrokken dan ik, die uit het onderwijs kwam en geen notie had van welke industrie dan ook. Het was toen trouwens nog helemaal een zaak van de mannen alleen.

In die tijd hoorden we over allerlei initiatieven om, naast de kerkdienst op Zondag, ook door de week »gemeente« te kunnen zijn, door met enkele gezinnen uit de buurt een z.g. huisgemeente te vormen. »Geloven op Maandag« was een veelgehoorde uitdrukking en in de zomer van 1957 was er op »Kerk en Wereld« in Driebergen een weekend waarin diverse geloofsgemeenschappen die anders waren dan de gewoon-kerkelijke, besproken werden. Het ging o.a. over Taizé, Nes Ammim, !ona, en de huisgemeente van ds. Klamer in Maastricht. Ook Dippel was daar indruk-wekkend aanwezig. Zijn stuk in »Wending«, met daarin de naam van de ons toen nog onbekende Eugen Rosenstock-Huessy, was ons voorbereidend leesstuk voor dit weekend.

Wij werden door dit weekend erg geïnspireerd, maar zagen toch geen kans er in onze gemeente iets mee te doen. De tijd en wijzelf waren er kennelijk nog niet rijp voor. Maar het idee, als mogelijkheid voor later, liet ons niet los.

In diezelfde tijd viel de ontdekking van het werk van Rosenstock door Bas en Wim, aan de hand van een bladzij over Rosenstock-Huessy en een voetnoot met een paar titels van zijn werken, in een boek van Miskotte. De voetnoot heeft geschiedenis gemaakt. De problemen van het industriepastoraat kwamen er nu heel anders uit te zien.

Intussen deden we braaf mee in het kerkelijk leven. Sam werd ouderling en deed huisbezoek, ik liep met De Open Deur en Open Venster, bezocht oudere gemeenteleden, hielp met een jeugdclub en samen waren we bij het kerkkoor. Onze drie jongens werden gedoopt en bezochten de christelijke school. Echter, toen Christiaan, de jongste, in 1966 werd gedoopt, hadden we al veel twijfels over het reilen en zeilen van de Kerk. Het vasthouden aan »het heil ligt in de Kerk, in de preek, het gezang en het gebed« werd voor ons op den duur toch onbevredigend.

Door Wim werden verschillende Rosenstock-leeskringen gevormd. Sam zat in zo'n groep, ik niet. Voorzover ik weet deden de vrouwen er niet of nauwelijks aan mee. Het was naar mijn idee een mannenzaak, waar ik me buiten voelde staan tot er, veel later pas, plannen begonnen te ontstaan om er in de praktijk iets mee te gaan doen en samen met anderen vorm te geven aan de ideeën die er, in de loop van de jaren van lezen en bestuderen van Rosenstock, waren ontstaan. Een eerste groep werd gevormd van mensen, allen uit de industrie afkomstig, die daar aan mee wilden doen. Het ging erom niet iets te doen wat anderen ook al deden. Waar lagen de lacunes, waar was behoefte aan? Na ongeveer een jaar discussiëren viel deze groep uiteen, het bleef onduidelijk wat we wilden.

Intussen had Wim kennis gemaakt met Piet en Mira Blankevoort, die op een Rodseenstockkring in Bloemendaal zaten en ook interesse in deze richting hadden. In de loop van  1968 werd een tweede groep gevormd. Contacten werden gelegd met o.a. Shalom in Odijk, Huub Oosterhuis en de Studentenecclesia. Ook nu weer toonden velen interesse maar vielen ook velen weer af. Inmiddels waren E en Pie Voet uit Zambia teruggekomen, deden mee en brachten »Afrika« in onze belangstellingssfeer: de Ed. Mondlane Stichting,  het Angola Comité, de opvang van Portugese vluchtelingen, Hanneke Baars, het aktiewerk. Piet en Mira zaten in het werk aan alocohol- en drugsverslaafden.

Zo begon zich langzaam af te tekenen waar onze taak zou komen te liggen. Ook het idee om dit samen te gaan doen als leefgemeenschap  begon sterker te worden. Bij elkaar wonen in verschillende huizen in één straat werd eerst wel overwogen maar was practisch gezien niet te realiseren. We gingen denken aan het kopen van een groot huis, waar in ieder geval verschillende gezinnen konden wonen en waar ook ruimte zou zijn voor gasten. We zagen als voordeel van het samenwonen het gemakkelijker verdelen van de taken, sneller en vaker overleg, het aanvullen van elkaars kwaliteiten, het samengaan van diverse leeftijden (drie generaties!) en het praktische voordeel van samen koken, wassen, gasten ontvangen, ent. (Dat het overleggen met elkaar ook weer veel extra tijd zou vragen ontdekten we pas later!)

Het »Rosenstock-Huessy-Huis« werd zo uiteindelijk voor ons de realisatie van wat Sam en mij voor ogen stond, indertijd, met »geloven op Maandag«, maar dan wel »incognito«, dus niet kerkelijk.
>*Elly Hartman*
