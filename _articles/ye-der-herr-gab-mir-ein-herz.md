---
title: "Yunus Emre: Der Herr gab mir ein Herz"
created: 2002
category: veroeff-elem
published: 2024-11-22
---
### Stimmstein 7, 2002

#### Mitteilungsblätter  2002

### Yunus Emre

### Der Herr gab mir ein Herz

Der Herr gab mir ein Herz \
Sogleich ist es verwundert \
Eine Zeitlang ist es fröhlich \
Eine Zeitlang ist es traurig \
Eine Zeitlang winterlich \
Frostig wie ein Januar \
Eine Zeitlang treibt es Freude \
In die Gärten dass sie blühn \
Eine Zeitlang bleibt es stumm \
Es erklärt mit keinem Wort \
Eine Zeitlang streut es Perlen \
Heilsam für die Kranken \
Eine Zeitlang himmelwärts \
Eine Zeitlang erdenwärts \
Eine Zeitlang schrumpft's zum Tropfen \
Eine Zeitlang wächst's zum Meer \
Eine Zeitlang ahnungslos \
Kennt es wohl die Dinge nicht \
Eine Zeitlang schwimmt's in Weisheit  \
Wie der rühmliche Galen \
Eine Zeitlang wird's zum Riesen \
Wohnt als Fee im Trümmerfeld \
Eine Zeitlang fliegt's wie Belkis \
Beherrscht Menschen und Dämonen \
Eine Zeitlang sucht es Kuppeln \
Wirft sich nieder in Moscheen \
Eine Zeitlang geht's zur Kirche \
Spricht die Bibel wird zum Pfaffen \
Eine Zeitlang wird es Jesus \
Läßt die Toten auferstehn \
Eine Zeitlang thront es wie \
Pharao und sein Gehilfe \
Eine Zeitlang wie Gabriel \
Sät es Segen in den Wind \
Eine Zeitlang geht es irre \
Staunend wie der arme Yunus

Yunus Emre. Aus: Das Kummerrad/Dertli Dolap, türkisch-deutsch, übersersetzt von Zafer Senocak, ohne Ort, ohne Jahr. Der Herr gab mir ein Herz Seite 36-39, Der Band ist im Buchhandel nicht erhältlich. Die Lebensdaten  von Yunus Emre sind unsicher, gest. um 1320/21.
