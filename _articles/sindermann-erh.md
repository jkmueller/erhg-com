---
title: "Joachim Sindermann über Rosenstock-Huessy"
category: aussage
published: 2022-01-23
name: Sindermann
---

Rosenstocks ideelles Interessengebiet und realer Wirkraum sind selten reich und mannigfaltig, ja sie könnten beinahe als disparat erscheinen, kennte man nicht das einheitliche geistige Zentrum, dessen Ausstrahlung sie sind. Vielleicht würde es genügen zu sagen, daß er ein guter Christ – vor allem im geistigen Sinne genommen – ist und wäre damit alles gesagt; doch ist es heutzutage nötig, hinzuzufügen, daß er ein Rechtshistoriker, und zwar ein Germanist und ein Soziologe ist.
>*Joachim Sindermann, Über Eugen Rosenstock, in: Volkswohl. Wissenschaftliche Monatsschrift 17.Jg., H.10 (1926), S.345. (Wien)*

Vielleicht darf überhaupt hier etwas zur Charakteristik der Schriften Rosenstocks angemerkt werden: es kommt dem Autor sehr oft nicht so sehr darauf an, bestimmte Ideen, Wahrheiten, Theorien zu formulieren, behaupten, beweisen, begründen usw., sondern vielmehr den Gegenstand oder das Ereignis zu vergeistigen, es geistig zu durchdringen und zu verklären, es in menschlichen Sprachstrom einzufangen und so zur „Vergegenwärtigung“ zu bringen; so darf der Leser nicht so sehr auf Einzelwort und Einzelsatz sehen; da erschiene vielleicht manches etwa als vorschnelle Verallgemeinerung – was ja auch in der Tat die Gefahr der synthetischen Begabung ist – ; sondern vielmehr: er soll sich von dem Sprachstrom berühren und verwandeln lassen, um so nicht zu einer kritischen Einsicht, einem Entgegensprechen also, sondern zum Mitsprechen zu gelangen.
>*Joachim Sindermann, Über Eugen Rosenstock, in: Volkswohl. Wissenschaftliche Monatsschrift 17.Jg., H.10 (1926), S.350f.*
