---
title: "Rosenstock-Huessy zu Sprache und Philosophy"
category: online-text2
published: 2023-09-15
order-to: 10
---
![Eugen Rosenstock-Huessy]({{ 'assets/images/erh_chess.jpg' | relative_url }}){:.img-right.img-small}

„Im Namen Humboldts also erkläre ich den Krieg gegen den ehrwürdigen Aberglauben, daß Philosophie ohne Philologie, wie auch Philologie ohne Philosophie zum Ziele führen können. Für mich sind Sprache, Logik und Literatur Kristallisationsformen des selben Prozesses der Geistesgegenwart. Freilich scheine ich mit dieser Hypothese das Grunddogma der Philosophie zu verletzen; aber amicus Plato, magis amica veritas (gewiß liebe ich Plato, aber mehr noch die Wahrheit). Ich muß fürchten, daß diese Lösung die Behaviouristen, Pragmatisten oder Anhänger irgendwelcher mehr oder weniger monistischer Schulen des Positivismus nicht befriedigen wird. Wir aber sind weder Materialisten, noch Idealisten. Immerhin gibt es schon viele Vorgänger auf diesem Gebiet wie Thomas Carlyle, der Jünger Johannes als Verfasser seines Evangeliums, Friedrich Schlegel, Hamann. Vor allem in den letzten 40 Jahren begannen Männer wie Majewski, Ebner, Buber, Cuny, Royen Denkformen zu entwickeln, die uns befähigen, die Einheit von Denken, Sprache und Literatur zu beschreiben. Diese neue Richtung ist keineswegs zufällig. Ohne ein solches Bemühen würde die Verwirrung in den Gesellschaftswissenschaften und der Altphilologie ständig anwachsen. Der beklagenswerte Mangel an Methode in den Gesellschaftswissenschaften entsteht aus der sterilen Haltung der Philosophen, die von der Sprache absehen.“  
*Eugen Rosenstock-Huessy, Die Einsinnigkeit von Logik, Linguistik und Literatur, in: ders., Die Sprache des Menschengeschlechts. Eine leibhaftige Grammatik in vier Teilen, 1. Bd., Heidelberg: Verlag Lambert Schneider 1963, S.526.*
