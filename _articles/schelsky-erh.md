---
title: "Helmut Schelsky über Rosenstock-Huessy"
category: aussage
published: 2022-01-05
name: Schelsky
---

„Die geniale Subjektivität dieses Denkers entzieht sich bewußt allen objektiven wissenschaftlichen Maßstäben und will gerade durch die subjektive Lebendigkeit wirken.\
...\
In seiner Vergegenwärtigung der Welt- und Heilsgeschichte, die der zweite Band seiner „Soziologie“ darstellt, zeigt sich Rosenstock-Huessy – wie schon früher in seinen „Europäischen Revolutionen“ (1929) – als ein Meister des aufhellenden geschichtlichen Vergleiches.”
>*Schelsky, Helmut, [Die Maßstäbe der Universität werden in Frage gestellt. Soziologie als Universalwissenschaft – Eugen Rosenstock-Huessy, ein Hamann der Gegenwart](https://www.zeit.de/1959/17/die-massstaebe-der-universitaet-werden-in-frage-gestellt/komplettansicht), DIE ZEIT v. 24. April 1959, S.8*
