---
title: "Texts in the English language"
category: kontakt
published: 2023-02-27
---

{% assign articles = site.articles | where: "language", "english" | sort: "org-publ" %}
<div class="articles">
  {% for article in articles %}
    <div>
      <a href="{{ article.url | relative_url }}">{{ article.title }}</a>
    </div>
  {% endfor %}
</div>

[I am an impure Thinker](https://erhfund.org/wp-content/uploads/I-am-an-Impure-Thinker.pdf)

[The Multiformity of Man](https://erhfund.org/wp-content/uploads/The-Multiformity-of-Man.pdf)

[The Listener's Tract](https://www.erhsociety.org/wp-content/uploads/2018/12/the-listeners-tract.pdf)

[Soul, Body, Spirit](https://www.erhsociety.org/wp-content/uploads/2018/12/erh-soul-body-spirit.pdf)

[zum Seitenbeginn](#top)
