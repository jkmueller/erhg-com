---
title: "Clinton C. Gardner: Rosenstock-Huessy in Russia & Iran"
category: einblick
published: 2024-07-23
language: english
---

### Rosenstock-Huessy in Russia & Iran
#### Responding to the Events of September 11
by Clinton C. Gardner, The Norwich Center, 20002

*“The danger of death is the first cause of any knowledge about society.”*[^1]
>*Eugen Rosenstock-Huessy*

#### I. INTRODUCTION: THE DIALOGICAL METHOD
Many months after September 11 we are still searching for an adequate response. It’s clear that killing or capturing terrorists is not enough. We must get at the roots of the problem or the war on terrorism will never end. We must turn to historians and political scientists, to experts on the Middle East, and especially to persons with wisdom on the dialogue—or lack of dialogue— among nations.

One such person is the Harvard political scientist Samuel Huntington whose 1993 *Foreign Affairs* article “The Clash of Civilizations?” suggested that we’ve reached a time when “the great divisions among humankind and the dominating source of conflict will be cultural.” Among the “fault lines” where clashes might be expected, he listed two that seemed especially dangerous:

1. Western versus Eastern Christendom.
2. Western versus Muslim civilization.

Huntington’s thesis has come up often in the discussions that have taken place since the events of September 11. I recall his views being debated on the Dartmouth College campus in a series of faculty-led seminars. Held in late September and early October of 2001, each of those very well-attended meetings addressed a different question:
1. What are the *historical* roots of Middle Eastern resentment of the U.S.?
2. What distinguishes the literature, religion and overall *culture* of Middle Eastern countries from that of the West?
3. How do the *future aspirations* of Middle Eastern people differ from Western aspirations?
4. What *practical steps, in today’s world* must the U.S. and Europe take now?

As I attended those seminars, I was struck by the fact that their four themes might have been chosen by a Dartmouth professor with whom I studied in the 1940s. Eugen Rosenstock-Huessy (1888-1973) was a historian and social philosopher whose work focused on the dialogue—or lack of dialogue—among persons and cultures. Indeed, he presented a “dialogical” model of the human predicament, a model which he called “The Cross of Reality.” Our orientation to life is brought about, he said, by four conflicting kinds of language:
1. Historical or *narrative* speech which orients us backwards toward *past* time.
2. Prophetic or *imperative* speech which orients us forward toward *future* time.
3. Subjective, “interior” speech (such as that of religion, literature and philosophy) which orients us toward our inner selves.
4. Objective, analytical speech (such as that of science) which orients us toward the outer world.

Our dialogue with other persons and with society as a whole is a constant effort to balance the conflicting demands of those four orientations.[^2]

This four-part dialogical model, Rosenstock-Huessy said, can be turned into a *dialogical method* for dealing with any human problem, from the personal to the international. This method, in turn, he envisioned as the heart of a “higher social science” for which he proposed the name “metanomics.”[^3] Thus, the four seminars at Dartmouth were metanomical in character, quite properly (and quite unconsciously) addressing the larger issues of September 11 in terms of Rosenstock-Huessy’s Cross of Reality. Huessy (as we his students called him) would diagram that cross on the blackboard by drawing a horizontal line for “time” and intersecting it with a vertical line for “space.” He would emphasize that each orientation was related to one of our four grammatical persons: thou, I, we, he or she. It then looked like this:

![Cross of Reality]({{ 'assets/images/cross_of_reality.jpg' | relative_url }}){:.img-center.img-xxlarge}


Now presenting this diagram is not a digression from my theme of what we should do in response to September 11. For Huessy pointed out that our emerging global society will continue to face threats on each of the four “fronts” depicted on the Cross of Reality. To make peace in the Middle East or to avoid a “clash of civilizations” between the Middle East and the West we must recognize that peace is threatened not only by war (a breakdown on the *outer* front), but also by anarchy (a breakdown *inside* society), decadence (loss of faith in the *future*) and revolution (loss of respect for the *past*).[^4] Peace cannot be established by simply rooting out some terrorists (an objective solution); it can be established and maintained only when we address the interrelated problems on all four fronts.

#### II. FROM LOEWENBERG TO TUNBRIDGE
What I want to do in the balance of this paper is reflect on how Huessy’s dialogical method provides both theoretical and practical wisdom as we consider what we must do in response to September 11. We are faced not only with the danger of a never-ending war on terrorism but also with the threat of steadily increasing global *anarchy* if our future unfolds as an ongoing “clash of civilizations.”

Huessy’s life and work address these issues head on. He was by no means an ivory tower academic theorist. He urged his students to join him in practical efforts to establish peace among persons whose background made them what he called “distemporaries,” that is persons who do not feel that they share a common time, a common history, a common future.[^5] His first practical social initiatives were taken in the late 1920s in Germany. Two young men came to him in distress, concerned that German society was fraying. Carl Dietrich von Trotha and his cousin Helmuth von Moltke sensed that there was no dialogue between their privileged class attending universities and the young men working in the Silesian coal mines nearby, nor with the local farmers.

In 1928, with Huessy’s inspiration and encouragement, von Trotha and von Moltke organized their first voluntary service camp at Loewenberg near Breslau. The camp brought together some 100 young men, about equally divided among students, miners and farmers. Mornings the volunteers worked together on building construction; then afternoons were devoted to discussion of current social and economic issues. This dialogue among “distemporaries” was evidently a transforming experience because it launched a voluntary service movement that spread all across Germany.

After Hitler came to power, Huessy came to the US and by 1935 was teaching at Dartmouth. There he continued to attract young men who wanted to be engaged in new social initiatives. In 1940 some of his students joined him in founding Camp William James in Tunbridge, Vermont. I myself was one of those students and served as the camp’s secretary. Of course we chose the name “Camp William James” to honor the American philosopher whose essay “The Moral Equivalent of War” proposed that all young people, as part of their education, devote a significant period to an all-out mobilization of their energies, comparable to the all-out commitment a soldier makes in wartime. As James expressed it, if we do not learn how to mobilize ourselves in peacetime, through selfless service which addresses our planet’s ills, “then war must have its way.”

Formed as an experimental camp within the New Deal’s Civilian Conservation Corps, Camp William James began a dialogue between its over 20 enrollees from the regular CCC and the over 20 privileged youth from Dartmouth and other colleges. It also began dialogue between these generally urban or suburban young people and the Vermont farmers who had 150-year-old roots in their communities. Finally, when the camp sent volunteers to help rebuild an earthquake- devastated town in Mexico, it began the sort of dialogue which now has flowered in the US Peace Corps—and similar efforts around the world.

Ever since my six-month commitment to Camp William James in 1941, I’ve taken every opportunity to advance its cause, from publishing a friend’s book about it to founding an organization that could provide a base for transnational voluntary service projects.[^6] One of the founders of Camp William James, Frank Davidson, helped me form this little organization called The Norwich Center. Other co-founders, in 1978, were Freya von Moltke, Helmuth von Moltke’s widow and Professor Huessy’s son Hans.

Some idea of The Norwich Center’s goals can be gathered from the name of a closely- associated project which shared our office: Argo Books. Actually I founded Argo ten years earlier, in 1968, with Freya von Moltke as my partner. Our purpose was to republish books of Huessy’s that had gone out of print—and put together new ones from unpublished manuscripts. We named our venture “Argo” to underline one of our author’s favorite themes. In his book *Sociology*, Huessy describes the two great intellectual projects of the past millennium: the ***scholastic,*** beginning with Anselm and Abelard in the eleventh century, followed by the ***academic,*** dating from Copernicus and Galileo in the sixteenth century, as well as Descartes in the seventeenth. The scholastics gave us theology as ***queen of the sciences***; the academics have given us natural science, with its triumphant achievements in physics.[^7]

Their needed successor, in our time, is a third great intellectual project, a truly social science which would be neither scholastic nor academic in character. Rather, it would be ***argonautic.*** The name suggests our need to overcome academia=s lack of passion, its cool detached objectivity. 'Argonautic' recalls the first sailors on the open seas, Jason and his crew on the Argo. It's significant that Huessy reached way back to our distant past to find this name for the attitude we'd need in the future. As argonauts, we are adventurers who must contend with winds that are constantly changing. We're no longer in the sheltered quiet of Plato's Academe. We're like venturesome scouts, taking risks. Unlike the academic, we can't equivocate, finding two sides to every question. Unless we steer our ship with a sense of direction, it will crash into the rock of Scylla or be sucked into the whirlpool of Charybdis.

Another idea of The Norwich Center’s goals, bearing quite closely on the work we actually undertook, is the following statement by Huessy in his book Planetary Service: “One thing is certain: we have no hope for abolishing war until we accept the framework of a universal planetary method of crossing borders between all peoples and all countries.”[^8]

#### III. FROM NORWICH, VERMONT TO MOSCOW
After a few years of research on the US Peace Corps and similar projects, The Norwich Center launched an ambitious project of its own. “US-USSR Bridges for Peace” was conceived to establish dialogue between Russians and Americans at a time when such dialogue was almost non-existent. In 1981 we invited various Soviet groups to cooperate with us—and by 1983 we’d begun regular exchanges.
When we started there were only three or four US groups that sponsored occasional exchange visits with the USSR. By creating an open-ended network of cooperating groups, Bridges for Peace exchange projects spread like wildfire, first to all the New England states, then to New Jersey and North Carolina. By 1985 our example had been instrumental in creating what came to be known as a national movement for “citizen diplomacy.” While in 1983 President Reagan was cutting off dialogue by calling the Soviet Union “an evil empire,” by the next year he’d reversed himself and made citizen dialogue an announced policy of his administration.[^9] Between 1983 and 1994 “Bridges” sent over 1,000 Americans to Russia and brought over 800 Russians to the US.

Obviously Huessy’s principles informed every step we took between Norwich and Moscow. But soon there came opportunities to go a step further and share his dialogical thinking directly with Soviet intellectuals we met on our exchange visits. Among these were many who saw similarities between Huessy’s thought and that of such Russian thinkers as Nikolai Berdyaev (1874-1948) and Vladimir Solovyov (1853-1900). In 1983 one Russian friend called my attention to the work of Mikhail Bakhtin (1895-1975) and I soon found there were startling similarities between his and Huessy’s understandings of speech and dialogue.

The way I maintained my own dialogue with these Russian intellectuals was not only by conversation. I handed them a little samizdat book I’d begun years earlier but updated during the 1980s to reflect my discovery of Bakhtin. Entitled *Between East and West: Rediscovering the Gifts of the Russian Spirit*, my samizdat compared Russia’s 19th and 20th century thinkers on the spirit and speech with their Western counterparts: Huessy, Franz Rosenzweig (1886-1929) and Martin Buber (1878-1965).[^10]

Much to my surprise and delight my Russian friends circulated their copies of my samizdat to others—and asked for more. By 1990, after I’d handed out over one hundred copies of my little self-published book, some Leningrad friends told me they’d translated it and hoped to publish it in Moscow.

In 1992 a Moscow friend introduced me to Vitaly Makhlin, who was head of the Bakhtin Center there. Again to my great surprise, after reading my book, Makhlin presented its ideas to readers of the journal *Problems of Philosophy*. At the end of a review of Western responses to Bakhtin, Makhlin had a section entitled “Farewell to Descartes, or the Cross of Reality Between East and West.”[^11] There he expressed appreciation for how I’d linked Bakhtin to his 19th century predecessors and related all of them to the Western tradition of dialogical thought.

In July 1993 I visited the offices of the Moscow journal *Logos*, in hopes they’d write a review of my book, finally published by the Russian Academy of Sciences the preceding January. The Logos editor asked me if I knew Alexander Pigalev, a scholar in Volgograd who’d sent them an article about Huessy. When I said that I didn’t know him, the editor kindly picked up the telephone and reached Professor Pigalev. It turned out that he hadn’t read my samizdat but had recently discovered Huessy on his own, after reading about him in a German periodical.[^12] (I attach as Appendix I to this paper a December 1993 letter to me from Professor Pigalev, since it documents the beginning of his quite successful effort at introducing Huessy’s thought into Russia, via lecturing, translating and publishing.)[^13]

Between 1993 and 1995 my little *Between East-West* book went through two editions and sold 6,000 copies. It was widely-reviewed in Russian journals, including *Problems of Philosophy*.[^14] That journal also printed Huessy’s essay “Farewell to Descartes,” with an introduction by Pigalev.[^15] In the mid-90s, Makhlin and Pigalev translated Huessy’s *Out of Revolution* into Russian and it was finally published in Moscow this past winter.[^16] Earlier, with Makhlin’s help, we’d translated and published Huessy’s *Speech and Reality*.[^17] In recent years, I’ve begun to receive e-mail from Russian groups that are not only reading Huessy but establishing activities based on his work. (I attach, as Appendix II, such an e-mail from The Center of Social Development, Tomsk, Russia.)

At six international conferences organized by The Norwich Center, between 1991 and 2000, there have been papers and discussion of Huessy, Bakhtin, Buber, Berdyaev and Solovyov. I think it was largely by seeing Huessy’s work in relation to that of these other thinkers that we’ve made such significant progress on introducing his thought into Russia.

#### IV. FROM NORWICH, VERMONT TO IRAN
Above I’ve discussed Huessy’s dialogical method and how it came to be applied in the Germany of the 1920s, the Vermont of the 1940s and the Russia of the last 20 years. That brings us full circle to the question with which I began. How can we respond to the events of September 11th in a more profound way than by killing and arresting terrorists? One dialogical response, perhaps a small one, or perhaps one which will grow like Bridges for Peace, was begun a few months after September 11 at the Norwich Vermont Congregational Church. Some of us who’d founded the first “Bridges” decided we’d form a successor organization “Building Bridges: Middle East-US.”

On January 24th we rallied over thirty supporters from five local towns to a founding meeting. Then on May 20th, at a considerably larger gathering, we announced our plans to begin exchanges with Iran in 2003. Professor Gene Garthwaite, an Iranian specialist in Dartmouth’s History Department, has agreed to lead a delegation of us to Tehran, Isfahan and Shiraz—where we’ll seek to identify groups with which to begin the sort of dialogue which Iran’s reforming President Khatami has called for, “a dialogue among civilizations.” If we’re successful, it will certainly be because we live out three principles made evident in the life and work of Eugen Rosenstock-Huessy:
- How academics can turn themselves into “argonauts” when they leave the grove of academe and engage the evils of war, revolution, decadence and anarchy which threaten the collapse of our global society.
- How the time has come for “planetary service” in which we’ll show how to cross borders “like pirates” when conventional diplomacy fails to establish dialogue.
- How people who are “distemporaries,” without a common sense of past, present or future, can turn themselves into contemporaries by working together on common projects that serve our planet’s future.

#### APPENDIX I – A letter from Alexander Pigalev

>*Volgograd, December 18, 1993*

Dear Mr. Clinton C. Gardner,\
Recently I have visited Moscow and made the acquaintance of Mr. Vitaly Makhlin. Formerly I knew him only without seeing. He gave me your address and I decided to write a letter. I hope that you will not condemn my address to you on my own initiative. After our telephone conversation in July, 1993 I was impatiently waiting for a letter or a post-card from you, but unfortunately the post is late. Then I myself decided to write a letter.

After our telephone conversation I learned much that was new to me about professor Eugen Rosenstock-Huessy. My provincial isolation begins to relax and I know now that many people in my country show interest for the speech thinking. In November, 1993 a scientific conference ***Philosophical Tradition, Its Cultural and Existential Dimensions*** took place. It was organized by The Russian Humanitarian University in Moscow. I delivered a lecture ***The Overcoming of Metaphysics and the Temporal Thinking of Eugen Rosenstock-Huessy.*** The lecture has received an enthusiastic welcome. As far as I know it was the first detailed presentation of Rosenstock-Huessy's conception in Russia. It seems to me that the seeds must sprout. At the same time I have read your remarkable book ABetween East and West.@ It impressed me deeply.

I decided to write this letter in order to continue our scientific contacts. Your telephone call was a fortune and a real shock for me. After the decades of the Soviet epoch the foreign countries became for us something unreal (especially for those who live in the provinces). Thank you for this telephone call.

Thanks to your book and to the conversation with Mr. Vitaly Makhlin I know something about you and now I must say some words about myself. I am the professional philosopher and the professor at the Volgograd State University. I am the head of the department of culture and art. This department was organized only four months ago and its teaching staff numbers four instructors (assistant professors and lecturers). Our research work will be devoted to the problem of a Man under the condition of the dialogue of civilizations. I hope that this scientific program will also interest The Transnational Institute. To be sure, we are only at the very beginning, but we hope that a good beginning is half the battle.

My former interests were connected with the history of the Russian philosophy and with the ideas of existentialism. I have even written and published some articles devoted to Husserl, Heidegger and Sartre. Now I became the ardent adherent of Rosenstock-Huessy's ideas and I cannot live and think as if nothing has happened. It is interesting that the significance of Rosenstock-Huessy's ideas came to light thanks to the conceptions of the Russian religious philosophy (I mean, of course, my own way of thinking).

I would like to let you know of the failure of my plan to publish the translation of Rosenstock-Huessy's works in the philosophical magazine ***Logos.*** The purposes of the editorial staff have unexpectedly changed. Now I am going to propose this translation to another philosophical magazine ***Philosophical Sciences.*** Mr. Vitaly Makhlin volunteered to help me.

Will you be so kind as to write whether Mr. Hans R. Huessy approves the purpose to publish the Russian translation of the works of his father. Has he permitted to publish the translation? It will be very kind of you to let me know of Mr. Hans R. Huessy's answer.

Mr. Vitaly Makhlin invited me to take part in the translation of Rosenstock-Huessy's work ***Out of Revolution.*** May I ask you to send me this book in order that the translation will speed up? This book of Rosenstock-Huessy is the only one which I have not. The other works are at my disposal in the form of microfilms. I am afraid of abusing your generosity, but I would be very thankful for every material concerning Rosenstock-Huessy's life and thought. I am also eager to correspond with everybody who devoted himself to the development of Rosenstock- Huessy's philosophical heritage. Would you mind helping me?

*With kindest regards, \
Yours, Alexander Pigalev*

#### APPENDIX II – A Letter from The Center of Social Development, Tomsk, Russia

Dear Mr. Gardner.\
We are writing you from Tomsk, Russia for the first time. In our city the Center of Social Development “Imya” was founded (September, 2001), which was based on E. Rosenstock- Huessy’s idea of metanomics (“Imya” means “Name” from Russian). This Center gets together different specialists of Tomsk State University and other institution of our city: psychologists, psychotherapists, philosophers, physicians etc.

The aim of the center’s activity is to create and improve the conditions of people joint living. The Center implements different projects in spheres of social therapy, education and consulting.
We edit Almanac of metanomical experience “Imya”, which is supposed to include the short (but important) works by ERH, related scholars and texts by contributors of the Center. The first issue of the almanac (from October, 6, 2001) contents “You and Me” and “I Am an Impure Thinker” (translated by I.A. Pigalev); there is also Lev Shestov’s “About Sources of Metaphysical Truths: Fettered Parmenides”

The introduction to the almanac and comments to “You and Me” were written by Oleg Lukyanov (candidate of psychological sciences, reader of TSU, psychoterapist, program director of the CSR “Imya”). There are also two texts by contributors of the Center: philosopher A.V. Murashov and psychologist A. Utkin.

We are ready to collaborate with you in translating and popularization works by ERH, also in devising his ideas in social practice (first of all to devise the method of metanomics in application to psychotherapy, psychology, social work and education).

At the 1st-2nd of November 2001, the conference “Existential experience and education” was given by the Center. The theme of metanomics was nearly the main on it.
We hope to meet with you for dialogue on authentic language.

*Yours sincerely,\
Lukyanov Oleg \
and contributors of the Center of Social Development “Imya”.*

#### NOTES
Several paragraphs in this paper have been taken from the text of a book which I completed this spring, Survival Knowledge: A Memoir of War, Russia and Discovery. (I am currently seeking a publisher. Some chapters of the book are available at: www.valley.net/~transnat/war&russia.html.)

[^1]: Eugen Rosenstock-Huessy, Speech and Reality (Norwich, VT: Argo Books, 1970), p. 21.

[^2]: Ibid. These points and the Cross of Reality described here are presented throughout the book Speech and Reality.
[^3]: ERH, Out of Revolution (Morrow, 1938), pp. 740-758.
[^4]: ERH, Speech and Reality, pp. 11-16.
[^5]: Ibid. p. 44 for use of the term “distemporaries.”
[^6]: Jack J. Preiss, Camp William James (Norwich, VT: Argo Books, 1978).
[^7]: ERH, Soziologie, Vol. II (Stuttgart: Kohlhammer, 1958), p. 683 ff.
[^8]: ERH, Planetary Service (Norwich, VT: Argo Books, 1978), p. 13.
[^9]: Jack F. Matlock, Jr., Autopsy on an Empire (Random House, 1995), p. 87.
[^10]: Clinton C. Gardner, Between East and West: Rediscovering the Gifts of the Russian Spirit, distributed as samizdat 1983-1992, published in Russian translation by the Russian Academy of Sciences publishing house Nauka, Moscow, 1993. Translator A. Kavtaskin, eds. Vitaly Makhlin and Vladimir Malyavin.
[^11]: Vitaly Makhlin, “Bakhtin and the West,” Problems of Philosophy, Moscow, March, 1993, pp. 145-148.
[^12]: I do not have the name of the periodical, but the article was by the German sociologist Dietmar Kamper, a member of the Rosenstock-Huessy Society in Germany.
[^13]: Two books translated and edited by Alexander Pigalev, each his selection of Rosenstock- Huessy essays, are God Makes Us Speak (Moscow: Kanon, 1998) and The Speech of Humankind (Moscow: Soros Foundation, 1999).
[^14]: Leonid Polyakov review in Problems of Philosophy, Moscow, October 1993.
[^15]: ERH, “Farewell to Descartes” in Russian, with an introductory essay by Alexander Pigalev, Problems of Philosophy, Moscow, 1997.
[^16]: ERH, Velikie Revolutzi (The Great Revolutions) (Moscow: St. Andrews Biblical Theological College, 2002). Trans. Vitaly Makhlin, Alexander Pigalev et. al.
[^17]: ERH, Rech I Deistvitelnost (Speech and Reality) (Moscow: Labyrinth, 1994). Trans. Adolph Harash. Introductions by Clinton C. Gardner and Vitaly Makhlin.
