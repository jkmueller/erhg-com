---
title: Mitgliederbrief 2015-11
category: rundbrief
created: 2015-11
summary:
zitat: |
  >… der Ursprung der Sprache ist im Himmel; ihr Weg aber führt nach unten unter die Menschen, die sie aufnehmen müssen in ihre Herzen und Glieder.\
  >Wo aber das Wort nicht mit dem Herzen aufgenommen wird – und diese Verstockung tritt meistens sehr bald ein -, da wird die Sprache bloß als Gehirninstrument mißverstanden und entartet dann zu bloßem Stoff, der dem Gesetz der Schwere unterliegt.
  >…
  >Die Wissenschaft selbst muß darum heute persönlich werden!
  >*Eugen Rosenstock-Huessy, Das Versiegen der Wissenschaften, Sprache d. MG 1 (1925/1958)*
---

## Eugen Rosenstock-Huessy Gesellschaft e.V.

{{ page.zitat }}

**Vorstand/board/bestuur:**\
*Dr. Jürgen Müller (Vorsitzender);*\
*Thomas Dreessen; Andreas Schreck; Dr. Eckart Wilkens; Rudolf Kremers*\
*Antwortadresse: Jürgen Müller: Vermeerstraat 17, 5691 ED Son, Niederlande*\
*Tel: 0(031) 499 32 40 59*

***Brief an die Mitglieder November 2015***

**Inhalt**

{{ page.summary }}


[zum Seitenbeginn](#top)
