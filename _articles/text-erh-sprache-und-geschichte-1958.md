---
title: "Rosenstock-Huessy: Sprache und Geschichte"
category: online-text
published: 2021-10-17
org-publ: 1958
---

Das Thema zeigt schon in seinem Wortlaut, wie krank wir sind, weil wir Sprache und Geschichte trennen, obwohl sie zusammengehören. Es ist doch sehr die Frage, ob sich die Zeitgenossen der Epochen, über die wir in unseren Geschichtsbüchern berichten, in dem wiederhören  werden, was wir von ihnen aussagen und ihnen in den Mund legen. Das liegt daran, daß die „Geschichte” zu einer Wissenschaft wurde, die wie alle Wissenschaft ihren Gegenstand verkaufen will als einen, über den man objektive Aussagen macht, gegen die der Gegenstand selbst sich nicht wehren kann. (Das Atom kann sich nicht wehren gegen die, die darüber schreiben.) Wie wäre  es nun aber, wenn die Geschichte nur aus dem bestünde, was Menschen von sich aussagen, aussagen, weil sie leiden?

Ich will Ihnen sofort meine These darlegen:

Meine These ist: Alle Geschichte ist ausgesprochene, ausgesagte Geschichte; darum kann der Historiker nicht wie ein anderer Wissenschaftler von Objekten ausgehen, die er beherrscht. Er kann nicht von der Geschichte das abstreifen, was die Menschen, an denen sie geschehen ist und geschieht, selber über sich und sie aussagen.

### Geschichte als Aussage

Geschichte ist darum niemals eine Wissenschaft, sondern, wie ja auch die Medizin, eine Art Heilkunde der menschlichen Gesellschaft. Der Historiker muß  darum versuchen, ein Verhältnis zur menschlichen Sprache zu gewinnen. Ganz abwegig ist der Versuch, hinter die Kulissen zu schauen. Dieser Versuch brächte uns im Theater auf den Schnürboden; da erfahren wir überhaupt  nichts mehr von dem Stück , von der Handlung, die vorn auf der Bühne  spielt.
Ich will Ihnen das an einem Beispiel klarmachen: Das einzige, was ein amerikanischer Schüler heute noch auswendig lernt, ist die Unabhängigkeitserklärung. Eine Volksbildungsorganisation rechnete sie sogar zu den 125 bedeutendsten Büchern der Welt, über die in allen Volkshochschulen der USA der Reihe nach gesprochen werden sollte. Und als man sich dann zur Besprechung der Unabhängigkeitserklärung  einen namhaften Historiker holte, stellte der fest, daß an der ganzen Unabhängigkeitserklärung  gar nichts dran sei. Das einzige, was „dahinter” stecke, seien die Grundbesitzerinteressen von Washington und Jefferson, die „in” Landspekulationen „gemacht” hätten. Sehen Sie, als man darüber sprechen wollte und sich dazu einen Historiker holte, der „hinter die Kulissen” auf den Schnürboden führte, stellte sich heraus, daß nichts mehr da war, worüber sich eigentlich zu sprechen lohnte. Die „Unabhängigkeitserklärung” hätte  eigentlich von der Liste der 125 bedeutendsten Bücher  gestrichen werden müssen. Und genau so erging es unter den Händen der Wissenschaft der Ilias und dem Johannesevangelium, den Paulusbriefen und dem Alten Testament.

Die motivsuchende Geschichtsschreibung verfährt bei ihrem Gang „hinter die Kulissen” mit allem so wie dieser akademische Historiker in Chikago mit der „Declaration of Independence”. Ich nenne diese ganze Richtung der Wissenschaft darum die „Philosophie des Ascheneimers”. Sie brachte einen gefährlichen Explosivstoff: das Ergebnis jeder Untersuchung über ein bedeutendes Buch ist, daß es eigentlich nicht mehr verdiente gelesen und geglaubt zu werden. Und dieser Explosivstoff kam nur in die Welt, weil die Historiker es ablehnten, sich an das zu halten, was die Menschen selbst uns ausgesagt haben.

„Wie wird denn Geschichte ausgesprochen?” so müssen  wir uns fragen. Wir schreiben heute den 20. Juli 1957. Diese Zusammenstellung von Zahlen scheint zunächst  völlig willkürlich herausgegriffen aus tausenden von anderen. Die Zahlen in der Geschichte sind nur ein Gerippe, oft ein totes Gerippe. Aber beim Sagen beginnt die Zahl zu leben, sie fordert uns auf, zu erzählen . Und beim Erzählen  wird sie aus einem Tropfen im Meer der Zeit zu einer Musiknote in einer Komposition, die den Akzent erhält , die der Melodie den Rhythmus gibt. Beim Aussagen, beim Hervorheben erhält  dieser 20. Juli 1957 den Akzent - 20. Juli 1944 - 20. Juli 57: ein Geschichtstag und seine Wiederkehr! Das ist der Weg von der Zahl zum Erzählen. Geschichte ist Auslese. Alle Liebe wählt  aus! Jede Liebe liebt in einer Reihenfolge. Die ganze Geschichte ist vielleicht solch eine Folge von auswählenden Liebeserklärungen und Kriegserklärungen. Wir lieben, um uns zu verewigen. Die Liebe ist der Ausweg aus der Todesverfallenheit der Menschheit. Wir brauchen die Liebestat als Weg in die Zukunft. Und so brauchen auch wir die erzählt en Liebeserklärungen  der Menschheit. Denn Erzählung  der Geschichte ist Anerkennung der Geschichte als Anfang einer neuen Geschichte.

Keine Stiftung der Kirche, keine Gründung einer Stadt und auch keine Gründung einer Pädagogischen Akademie kann vorgenommen werden ohne die Hoffnung, daß noch ferne Geschlechter dieses Tages in Dankbarkeit gedenken werden, weil er der Beginn ihrer Geschichte war. Eine Anstalt, wie z. B. das College, an dem ich in den USA tätig  bin, wird von den wesentlichen Personen, bestimmt, die sie gegründet und geleitet haben. Die weniger bedeutenden Leiter leben gleichsam „unter ihren Augen”.

Wenn ein Tag wie ein Leuchtturm herausgehoben wird, rücken alle anderen in den Lichtkegel dieses Tages. So ist es mit dem 20. Juli 1944. Alles, was vorher geschah in stiller Opposition, in zähem Widerstand, im Behaupten der Menschlichkeit, wäre  im Dunkel geblieben, wenn nicht dieser Tag unter einem gewaltigen Knall aufgebrochen und zu einer Fackel geworden wäre, die ihren Schein auf alles wirft, was vorher und nachher geschah. Darum ist es so wichtig, daß wir heute dieses 20. Juli 1944 gedenken. Die 5000 besten Deutschen, die wegen dieses Tages fielen und heute an allen Ecken und Enden in Deutschland so bitter fehlen, sie wären unersetzlich, wenn ihrer nicht gedacht würde. Das Fehlende wird nur durch die Liebe ersetzbar. Darum ist die Totenklage die älteste Sprache der Geschichte. Die Toten sagen uns darin: Wir möchten mit zu
dem gehören, was in Zukunft gesagt wird. Dagegen geschieht heute in Deutschland das große  Verschütten. Man will nichts mehr von dem erzählen, was damals geschah. Aber an Ihrer Stellung zum 20. Juli 1944 entscheidet es sich, ob Sie als Deutsches Volk noch eine Geschichte haben. Was Sie als Deutsche mit der übrigen Welt zu einer gemeinsamen Geschichte zusammenbindet, ist allein die Tatsache, daß es in Deutschland von 1933 bis 1945 die Männer des 20. Juli gab. Bedenken Sie bitte die außenpolitische Bedeutung dieser Ereignisse. Nur wenn Sie sie anerkennen, haben Sie als Deutsches Volk noch eine Geschichte. Als Christen könnten Sie natürlich auch ohne dies, etwa in Amerika, eine Geschichte haben, aber als Deutsche nicht. In Deutschland besteht die Neigung, daß jeder an seinem Standpunkt festhält. Lieben aber heißt, seinen Standpunkt aufgeben.

Ebenso schrecklich ist, daß man sich in Deutschland immer mit den Dingen „auseinandersetzen” will. Das ist ein schreckliches Wort, Ich bitte Sie, gebrauchen Sie es nie. Man kann sich nicht mit dem Jahre 1848, mit dem 20. Juli oder dem Kommunismus „auseinandersetzen” wollen; denn Sprechen heißt: in den anderen eindringen, hören  heißt: ihn in sich eindringen lassen.
Die Geschichte ist Liebestat oder sie ist gar nichts, jedenfalls ist sie keine Wissenschaft. Indem die Geschichte Liebe erklärt  oder Krieg erklärt, indem sie zustimmt oder verdammt, sucht sie dem Vergangenen einen Platz in der Zukunft einzuräumen. Das verpflichtet uns, gibt uns unsere Aufgabe: Wir müssen  als Lehrer auf 1000 Jahre hinaus lehren. Wir müssen  sehr vieles weitergeben, auch wenn wir es nicht gemeistert haben. Wenn die Traditionskette von A über B und C nach D geht, und nur A und D verstehen, müssen  doch B und C weitergeben, um D das Verstehen zu ermöglichen. Der Lehrer muß  also 1.) das Gehirn des Kindes nähren  und füllen ; 2.) das Kind eingliedern in die Kette, die von der Vergangenheit in die Zukunft reicht.

Wer die Geschichte erzählt, bekennt demütig, daß er sie nicht ganz versteht, aber gleichzeitig, daß er sie doch so liebt, daß er sie über sich erhebt. Dadurch gerät er aber auf die Straße, die von diesem Ereignis in die Zukunft führt.

Der Gehorsam gegen die, von denen gesprochen wird, das Ernstnehmen und Überliefern dessen, was sie selbst ausgesagt haben, ist mindestens ebenso wichtig wie das Vielwissen um die Motive und das Geschehen „hinter den Kulissen”.

Wir müssen  doch einfach anerkennen, daß es die Totenklage der Frauen war, die Klage der Kriemhild um Siegfried, die Totenklage um Achill und Hektor, welche die Helden des Nibelungenliedes und der Ilias unsterblich gemacht haben.

Die Liebe ist die Ursache dafür , daß es Geschichte gibt.

### Die Rangordnung in der Geschichte

Ich will Ihnen nun etwas sagen über den Traditionsprozeß, in dem ein gesundes Volk immer steht, und am Schluß  etwas über die Geschichte als Lehrgegenstand.

1. Das Kind muß  gehorchen lernen, es muß  hören, muß  Vater und Mutter beim richtigen Namen nennen, muß  zur Anerkenntnis der heiligen Namen gelangen.\
Dadurch kommt es zum Rückwärtsblicken in das Geschehen der Welt, zur Anerkennung. Es ist schon wichtig, daß Kinder auch die Titel der Menschen anerkennen. Durch das alles tritt die Geschichte als geschehene Geschichte in die Kinder ein. Wir müssen  Erkenntnis durch Anerkenntnis ersetzen. Erkenntnis ist eine grausame Angelegenheit, sie zerstört die Dinge und die Geschichte. Anerkenntnis ist Achtung, sie führt uns weiter und baut auf.

2. Dem Erwachsenen müssen  wir es wieder zutrauen, Leben hervorzurufen. Bedenken Sie, was uns allein schon die sprachliche Form des Wortes sagt: hervorrufen. Wir rufen hervor, was noch nicht da ist. Bei jeder Liebeserklärung  beginnt ein solches Hervorrufen des Neuen. Die Verheißung an Abraham und Sara: „Ich will euren Samen mehren wie den Sand am Meer” gilt für alle Ehepaare. Wir müssen  uns klar darüber werden, daß die Ehe nicht innerhalb der Nation eingezwängt sein darf. Man hat in Rußland versucht, daß nur russische  Staatsbürger russische Staatsbürger  heiraten  durften,  um  russische Staatsbürger hervorzubringen, und ist daran gescheitert. Die Ehe bleibt bestehen, auch wenn die Staatsbürgerschaft wechselt, die Nation untergeht. Mit jeder Ehe beginnt eine neue Geschichte, die sich in die Ewigkeit fortsetzen soll.

Ebenso rufen wir in jedem ernsthaften Gespräch, in jeder von uns ernstgenommenen Begegnung mit einem anderen Menschen Zukunft, zukünftiges geistiges Leben, zukünftige Geschichte hervor.

Anerkennung und Hervorrufung sind die beiden grundlegenden Akte in aller Geschichte.

Beim Anerkennen wenden wir uns der Vergangenheit zu, die unsere Gegenwart bestimmt und richten uns neu an ihr aus, machen sie neu für unsere Gegenwart fruchtbar.

Beim Hervorrufen blicken wir in die Zukunft, der unsere Entscheidung, der unser Wort die Richtung gibt und die unserer Gegenwart Dauer verleiht.

Unausgesetzt müssen  wir rückwärtsblickend anerkennen und vorwärtsblikkend hervorrufen.

Lange bevor wir die Geschichte studieren, sind wir in sie eingebettet. Daraus erwachsen jedem von uns Erfahrungen. Was wir anerkennen, verwandelt uns.

Wenn wir z. B. die Kinder so sehen, wie sie wirklich sind und nicht als die Naturidioten des Herrn Jean Jacques Rousseau, dann sehen wir, wie sie der Vergangenheit zugewandt sind. Die Kinder spielen sich in die Vergangenheit hinein. Im Pfänderspiel spiegelt sich die altgermanische Rechtsordnung.

Das ist das Verhältnis des Kindes zur Geschichte. Ein großer Irrtum ist der Satz: Die Jugend hat die Zukunft. Nicht die Kinder rufen die Zukunft hervor. Kinder sind vorgeschichtlich, sie gehören  höchstens in das Museum für Vor- und Frühgeschichte.

Uns, den Erwachsenen, gehört vielleicht die Zukunft, aber nicht den Kindern; wenn wir gut gelebt haben, gehört uns die Zukunft. Wir rufen die Zukunft hervor.

Die Zukunft hat man nämlich nicht schon deswegen, weil man länger lebt als andere. Wir alle müssen  unsere Bestimmung suchen und auf sie hören  und das heißt: wir müssen  entscheiden, wieviel von der Vergangenheit angefangene Zukunft ist, die wir anzuerkennen haben, und wieweit wir selbst die Zukunft neu hervorzurufen haben.

Dieses Anerkennen und Hervorrufen hilft uns, die Geschichte zu befreien: nach rückwärts von dem bloßen Erkennen, das sie zerstört, und nach vorwärts von dem bloßen „Planen”, das sie vergewaltigt. Wer zu den Menschen gehört, die Vergangenheit „erkennen” wollen, der kann auch dann konsequenterweise nicht darauf verzichten, seine „Erkenntnisse” anzuwenden und die Zukunft zu „planen”. Ja, bewußt oder unbewußt, erkennt er nur die Vergangenheit, um die Zukunft zu planen, und das führt in der Konsequenz zu den Experimenten am lebenden Leichnam.

Das teuflische Prinzip bei allem „Erkennen”, wie es unser wissenschaftlicher Betrieb betreibt und bei allem „Planen”, wie es Bürokraten und Funktionäre tun, ist nun aber, daß jeder „Erkennende” und jeder „Planende” grundsätzlich auswechselbar sind, sie sind nur kleine Rädchen in einer gewaltigen Maschinerie.

Man muß  auf dieses „Erkennen” verzichten, weil das „Planen” uns umbringt. Stattdessen muß  man „hören”, was die Geschichte schon gesagt, gelehrt und angefangen hat, und dann entscheiden, ob man nicht Neues hervorrufen muß. Das aber ist die Gewissenfreiheit.

In jedem Augenblick seines Lebens und der Geschichte steht so der Mensch zwischen Anerkennung und Hervorrufung.

Zuerst müssen  wir anerkennen, und zwar müssen  wir anerkennen in unserer eigenen Zeit, der Zeit, die wir selbst durchlebt haben. Erst wenn wir das tun, haben wir Geschichte.

Was ich jetzt sage, meine ich in großem Ernst: Es ist noch lange nicht abgemacht, ob Sie als Deutsche, als Deutsches Volk noch eine Geschichte, noch eine Zukunft haben. Das ist nämlich ganz unabhängig davon, ob Sie als Christen noch eine Geschichte, noch eine Zukunft haben. Ich versichere Sie, auch in Amerika gibt es Christen. Das ist aber ganz unabhängig von der Frage, ob es dort Amerikaner gibt.

Jedenfalls steht eines fest: die Geschichte des Deutschen Volkes ist dann unwiderruflich zu Ende, wenn sich die Gewohnheit durchsetzen sollte, die Zeit von 1933 bis 1945 einfach auszulassen, zu übergehen, dort ein großes Loch zu lassen und alles, was da geschah, zu verdrängen aus dem Bewußtsein, Gedächtnis und der Diskussion, wie es ja doch weithin geschieht. Es ist eine Lebensfrage für Sie, wohlgemerkt für Sie als Deutsche und als deutsches Volk, ob es
Ihnen gelingt, in der Zeit von 1933 bis 1945 etwas zu finden, was Sie anerkennen können und müssen. Wenn wirklich für Sie in der Zeit von 1933 bis 1945 kein Name zu finden sein sollte, der des Nennens wert wäre, dem Sie Ihre Anerkennung zu zollen hätten und den Sie so Zukunft hervorrufend weiterzugeben wagen, dann hat das deutsche Volk aufgehört, eine Geschichte zu haben.

Und in diesem Zusammenhang muß  ich noch einmal auf den Tag zurückkommen, den der heutige Tag uns ins Gedächtnis ruft, den 20. Juli 1944 und seine Helden. „Ein jeder muß  sich seine Helden wählen” sagt schon Goethe. Es ist Ihre Lebensfrage, ob Sie sie auch in der Zeit von 1933 bis 1945 in Ihrem Volke vorzufinden vermögen.

An Ihrer Stellung zu diesem 20. Juli 1944 entscheidet es sich, ob Sie Ahnen für Ihr Deutschtum haben können.

### Die Geschichte als Lehrgegenstand

Nun ist meine Zeit bald abgelaufen, und ich will noch kurz auf das eingehen, was Ihnen als Lehrern ja am meisten auf den Nägeln brennt: auf die Geschichte als Lehrgegenstand. Hierfür hat die Geschichtswissenschaft eine wichtige Rolle. Vieles wird nämlich vergessen, nicht anerkannt und nicht hervorgerufen aus Feigheit. „Feiglinge sind wir alle”, sagte der alte Blücher, wir unterscheiden uns nur darin, ob wir unsere Feigheit überwinden oder nicht.

Weil nun aus Feigheit so vieles nicht anerkannt und nicht hervorgerufen wird, ist das Wiederanerkennen und Wiederhervorrufen die besondere Aufgabe der Schule, des Lehrers, des Geschichtslehrers und der „Geschichtswissenschaft”.

An das, was vergessen wurde, müssen  wir wieder erinnert werden, auf daß vieles von dem wieder anerkannt wird, was sonst vergessen wird. Das tut der Geschichtsunterricht.

Wie soll er nun verfahren, ohne im Stoff zu ersticken?

1. Der Geschichtsunterricht muß  das stiftende Ereignis wieder anerkennen damit das Brauchtum, welches sich daraus entwickelt hat, weiter anerkannt werden kann, von denen, die mit und in ihm aufgewachsen sind. Denken Sie an die Versuche, die christliche Zeitrechnung abzuschaffen, dann wird Ihnen klar, daß es hier um mehr geht als um tote Gewohnheit, sondern um das Anerkennen der für unsere Geschichte entscheidenden Ereignisse, die auch unsere Zukunft noch mitbestimmen im Guten und Bösen, wie sie unsere Gegenwart mitgeformt haben. So wird bei jedem Tischgebet die Stiftung des Heiligen Abendmahles anerkannt.\
Von den gestaltgebenden großen Einbrüchen in die Geschichte müssen  wir also reden. Wir müssen  davon reden, auch wenn wir nicht alles daran verstehen. Auch eine Mutter kann ihrem Kinde ja nicht sagen: „Wir schaffen Weihnachten ab, morgen gibts keinen Weihnachtsbaum, und keine Bescherung, weil ich nicht verstanden habe und verstehen kann, was dieses Geschehen da im Stall zu Bethlehem eigentlich bedeutet.” Und ebenso wenig kann sie Ostern ausfallen lassen, weil sie nicht alles an der Auferstehung Jesu Christi verstanden hat.

2. Zweitens müssen  wir das, was im Augenblick vergessen zu werden droht, wieder hervorrufen. Denken Sie dabei daran: Mit dem Investiturstreit, der Reformation und der großen französischen Revolution, mit 1122, 1517 und 1789 kann man den Kindern helfen, das zu verstehen, womit sie zu tun haben und zu tun haben werden. Diese Ereignisse sind Einbrüche des Neuen. Darum müssen  sie erforscht werden. Als ein Beispiel für solchen Gegenstand der Forschung aus dem Bereich der Reformation mag etwa der deutsche Buchhandel genannt werden. Wie kommt es, daß Sie nur in Deutschland wirkliche Buchhandlungen haben? Seit Luthers Drucker Hans Lufft in Wittenberg für den Buchhandel das Kommissionsprinzip einrichtete, wurde dieses Prinzip vorbildlich nicht nur für den Buchhandel, sondern für alle Industrieunternehmen in Deutschland. Daß wir in Amerika keinen Buchhandel haben, ist ein schwerer Schade für das amerikanische Bildungswesen. Es erklärt  sich daraus, daß die beispielhafte Industrie in Amerika auf Barzahlung beruht und daß der Buchhandel sich danach richten muß  mit dem Erfolg, den ich genannt habe.

Es gibt aber auch Ereignisse, die nicht mehr anerkannt werden können, weil sie keine Spuren im Brauchtum hinterlassen haben und die wir doch erforschen und erwähnen müssen, um toten Geist wieder zum Leben zu erwecken. Ich will Ihnen das an einer Geschichte klar machen, die mir ein amerikanischer Feldgeistlicher von Saipan erzählte, der nach der Landung am Strand zwei nackte Japaner fand und sie unbestattet liegen ließ, als er festgestellt hatte, daß sie tot waren. Es waren doch Japaner, keine Amerikaner, meinte er. Das hat mich erschüttert und hat in mir ein Entsetzen hervorgerufen über diesen Pfarrer. Der Glaube des Menschen betätigt sich nämlich erst da, wo man das Menschenrecht des Toten anerkennt. Darum ist es auch so verbrecherisch, daß man die Sterbenden in den Krankenhäusern einschließlich der Angehörigen bis zum letzten Augenblick über das nahe Bevorstehen des Todes hinwegzutäuschen versucht, so daß der Sterbende um sein letztes Wort an die Angehörigen gebracht wird, Damit aber wird die Beerdigung um ihren tiefen Sinn gebracht: Sie ist nämlich die Antwort der Hinterbliebenen auf das letzte Wort des Sterbenden.

Die Frage ist nun, welche Urkräfte müssen  wieder hervorgerufen werden, damit das Menschenrecht des Toten wieder anerkannt wird und solche Dinge wie auf Saipan nicht wieder vorkommen. Dazu brauchen wir die Vorgeschichte. Die ganze Vor- und Frühgeschichte wird darum wieder hervorgerufen werden müssen, um die Ehrfurcht vor den Toten wieder wachzurufen.

Warnen möchte ich Sie am Schluß  noch vor einem Darstellen der Epochen gleich einem Naturvorgang. Was ist Renaissance? Wiedergeburt des Altertums? Das ist Unsinn. Als ob etwas wiederkommen könne ohne Ja! und Nein!, d.h. ohne daß es von einzelnen Menschen hervorgerufen wird, die dafür  verantwortlich sind. Ich kann das jetzt am Schluß  nicht mehr im Einzelnen aufreißen, möchte Sie aber doch in allem Ernst darauf hinweisen. Die Renaissance des klassischen Altertums ist ein Hervorrufen gewesen, das klassische Altertum ist wirklich wieder hervorgerufen worden. So wird die Geschichte wieder belebt durch „Wiederhervorrufen” und wir müssen  uns auch jetzt entscheiden, was von der „toten” Geschichte, der Vergangenheit in die Zukunft gehört. Das Hervorrufen ist also nicht nur Sache der Eltern oder der befehlenden Staatsmänner und Führer des geistigen Lebens, sondern geht uns Lehrer an.

In dieser Doppelspurigkeit von dem in die Vergangenheit gerichteten Anerkennen und Wiederanerkennen und dem in die Zukunft gerichteten Hervorrufen und Wiederhervorrufen sind wir Geschichtslehrer.


aus: Das Geheimnis der Universität, Stuttgart 1958, S. 86–93

[„Sprache und Geschichte” ist Teil des PDF-Scan](http://www.erhfund.org/wp-content/uploads/515.pdf)

[zum Seitenbeginn](#top)
