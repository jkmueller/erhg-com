---
title: Jahrestagung 2023
created: 2023
category: jahrestagung
thema: "„Natürlich, künstlich oder unbezahlbar? - Wie wollen wir arbeiten und leben?”"
published: 2023-04-03
landingpage:
order-lp: 1
---

### {{ page.thema }}

![DLA Collegienhaus]({{ 'assets/images/DLA_collegienhaus.jpg' | relative_url }}){:.img-right.img-large}

Datum: Donnerstag 5.10. - Samstag 7.10. 2023

<!--more-->

Liebe Mitglieder und Freunde,

nachdem vor einem Jahr der Depositalvertrag durch das Landeskirchliche Archiv Bielefeld gekündigt worden war, sind wir froh, daß das Deutsche Literaturarchiv in Marbach die Archivalien zu Eugen Rosenstock-Huessy übernimmt. Die Übergabe fand im Februar statt. Dies möchten wir zum Anlass nehmen und unsere nächste Jahrestagung und Mitgliederversammlung in Marbach durchführen.\
Der Termin ist von Donnerstag 5.  bis Samstag 7. Oktober 2023. Der Beginn schon am Donnerstagabend gibt uns die Gelegenheit am Freitag für eine Führung im Literaturarchiv. Der 50ste Todestag Eugen Rosnestock-Huessys und das 60 jährige Bestehen unserer Gesellschaft in diesem Jahr sind gute Gelegenheiten zu einer Bestandsaufnahme und einem Ausblick der Arbeit mit dem Erbe Rosenstock-Huessys. Dazu herzliche Einladung und merken Sie sich bitte den Termin (5.-7.10.2023) vor. Die offizielle Einladung wird Ihnen im nächsten Mitgliederbrief zugehen.
>*Jürgen Müller*
