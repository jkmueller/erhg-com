---
title: Jahrestagung 2022
created: 2022
category: jahrestagung
thema: " „Seit ein Gespräch wir sind” "
published: 2022-07-04
---
![Haus Wiesengrund]({{ 'assets/images/haus-wiesengrund.jpg' | relative_url }}){:.img-right.img-large}

### {{ page.thema }}

Datum: 30.9. - 2.10. 2022

<!--more-->

Textgrundlage: [The Atlantic Revolution](https://www.eckartwilkens.org/text-erh-atlantic-revolution/), 1940

Ort: Haus Wiesengrund, 51588 Nümbrecht-Überdorf\
Tel.: 02262 – 27 33

#### Programm der Jahrestagung


| Freitag, 30.9 | 18:00 | Eintreffen |
|               | 18:30 | Abendessen |
|               | 19:30 | Der Mitgliederbrief im Gespräch |
|               | 21:00 | Geselliges Beisammensein |
| Samstag, 1.10 |  8:30 | Frühstück |
|               |  9:30 | Textlesung: Die Atlantische Revolution |
|               | 10:15 | Pause |
|               | 10:30 | Die Atlantische Revolution im Kontext der Russischen und der Weltkriegsrevolution |
|               | 12:15 | Pause |
|               | 12:30 | Mittagessen |
|               | 15:00 | Kaffeepause |
|               | 15:30 | Mitgliederversammlung |
|               | 17:00 |	Zivilgesellschaft und das Gesetz der Technik |
|               | 18:30 | Abendessen |
|               | 20:00 | Tanz und Takt. Musikalische Impressionen |
|               | 21:30 | Geselliges Beisammensein |
| Sonntag, 2.10 |  8:00 | Morgenandacht |
|               |  8:30 | Frühstück |
|               | 10:00 | Kleine Gemeinschaften / Netzwerke / Arbeitsgemeinschaften |
|               | 11:00 | Abschlußrunde |
|               | 12:30 | Mittagessen |
|               | 13:00 | Abreise |
