---
title: "Freya von Moltke: Über Eugen - Margrit und Franz"
category: einblick
published: 2022-07-11
---


Im Frühjahr 2002 wurden zum ersten Mal mehr als tausend " Gritli" Briefe veröffentlicht,
die Franz Rosenzweig in den Jahren 1917-1922 an Margrit Huessy Rosenstock, die Frau
seines Freundes Eugen Rosenstock, in einer grossen Liebesbeziehung geschrieben hat.
Zuerst erschien eine unvollständige Ausgabe, herausgegeben von Inken Rühle und
Reinhold Mayer im Bilam Verlag. Um die Integrität de Briefkorpus zu gewährleisten
folgte eine ungekürzte Ausgabe auf dem Internet, besorgt vom Eugen Rosenstock-Huessy
Fund. Damit gewann die Öffentlichkeit nach vielen Jahren eine wesentliche neue Quelle
zum Verständnis von Franz Rosenzweigs Denken, Leben und Wirken.

Hinter der Liebe von Franz und Margrit steht die Freundschaft von Eugen und Franz.
Diese Freundschaft hatte für beide Männer grosse existentielle Bedeutung. Es ist bekannt,
dass der Christ Eugen in einem Gespräch, mit Franz und dessen Vetter Rudolf Ehrenberg
in Leipzig im Jahre 1913 ohne zu wollen Franz auf den Weg brachte, aus einem
agnostischen ein an die Offenbarung Gottes in der Welt glaubenden Juden zu werden.
Bekannt ist auch in auf diesem Gespräch aufbauender Briefwechsel, der sich während
des I. Weltkriegs 1916 zwischen Franz, im Kriegseinsatz in Mazedonien und Eugen, nahe
der Front in Frankreich, entwickelte. In ihm erkannten sie sich gegenseitig als Jude und
Christ an. Aber in einer neuen Form des Denkens wurden sie sich auch einig, dass trotz
der Verschiedenheit der Offenbarungen bei Juden und Christen sie das grosse
Gemeinsame im Medium verbindet, im Geheimnis der Sprache, dafür die Offenbarung
das Medium auch immer die Sprache ist. In einem undatierten Brief aus dem Winter
1970 hat Eugen aus dem Haus seines Sohnes Hans mir darüber, geschrieben: "Hier liegt
ein dicker Band mit Theologen Biographien; ich scheine selber ihn Hans geschenkt zu
haben. Franz Rosenzweig ist drin, und ich bin drin. Aber ich bin in seinem Leben nicht
erwähnt und er nicht in meinem. So kann mans also auch machen. Die Welt lebt
angenehmer oder wenigstens bequemer ohne die Mühsal der Wahrheit. Die Sinnlosigkeit
dieses Sammelbandes ist aber doch erstaunlich. Denn Franz und ich haben die Trennung
im Glauben aufgehoben. Das aber darf erst 50 Jahre nach meinem Tode zugestanden
werden."

Von der Zeit dieses Briefwechsels bis zum Tod von Franz Rosenzweig am 10. Dezember
1929 ist die Verbindung zwischen Franz und Eugen nie abgebrochen. Noch am 5.
Oktober 1929 schrieb Franz an Eugen: "Ich lerne ja von niemandem so natürlich oder
eigentlich so zwangsläufig, so ohne Zusatz von eigenem guten Lernwillen, wie von Dir."

Die Liebe von Franz und Margrit muss man im Rahmen dieser Freundschaft betrachten.
Das tut die Rühle/Mayersche Ausgabe der "Gritli Briefe" nicht, wie die Auslassungen
zeigen. Sie macht daraus eine simple Liebschaft. Sie weiss nichts, oder doch zu wenig,
von der komplexen, heute noch Interesse beanspruchenden Bedeutung der Freundschaft
von Franz und Eugen für beider gesamtes Leben und Werk.

Eugen und Margrit hatten in den Tagen des Kriegsausbruchs 1914 geheiratet. Eugen
wurde sofort als Soldat eingezogen; das Paar hatte bis zum Ende des Krieges 1918 kein
eigenes Zuhause. Viele Briefe wurden während de Krieges gewechselt Eugen wünscht,
dass seine Frau seinen Freund Franz kennen lerne. Deren wachsende Beziehung zu Franz
Rosenzweig spielt auch in diesem Briefwechsel eine Rolle, und es befindet sich dort von
Ende Mai bis Mitte Juni 1918 ein Austausch zwischen Eugen und Margrit, der eine Krise
zwischen den beiden bezeugt. Sie liegt in der Zeit höchster gegenseitiger Involvierung
von Margrit und Franz. Eugen hat die Liebe zwischen Franz und Margrit um der Freiheit
und Selbständigkeit seiner Frau und seiner Freundschaft mit Franz willen gelten lassen;
er achtete diese Beziehung. Franz und Margrit haben unbegrenztes Vertrauen in Eugens
"Menschlichkeit". Franz beschreibt das in seinem Brief vom 1. Juni 1918 und stellt selbst
seine Liebe zu Margrit in den Rahmen seiner Freundschaft mit Eugen. "Du weisst, wie
Eugen an meinen Wurzeln gerissen hat, ich meine nicht das Theoretische, nicht
die "Auseinandersetzung", überhaupt nicht das Sagbare, sondern seine Menschlichkeit. Es
ist seine Menschlichkeit ohne den verwirrenden Zusatz des Auseinandersätzigen, die auf
mich überströmt, wenn Deine Liebe sich mir schenkt. Du schaffst an mir, setzest Eugens
Schöpfung in mir fort, ja vollendest sie erst. Bedenk, dass ich ihm erst seit dem "Juni 17 "
[Franzens 1. Begegnung mit Margrit] schrankenlos glaube, und erst seitdem lückenlos
liebe. Nein wirklich, das alles liegt so nah an den Wurzeln des Lebens, dass ich es kaum
mit Worten entblössen kann" Die "Gritli" Briefe zeigen, das Franz und Margrit auch
immer mit Eugen im Gespräch bleiben wollten. Franzens Briefe an Eugen hören nicht
auf; er schreibt auch hie und da sogar an beide, Eugen und Margrit, zusammen. Da
Margrit auch Eugen immer weiter liebt und an ihre Ehe glaubt, ist sie zutiefst davon
überzeugt, dass sie ihm mit ihrer Beziehung zu Franz nichts wegnimmt. Aber so einfach
war es nicht. Beide, Franz und Margrit spüren nicht, den Umfang dessen, was sie Eugen
antun und von ihm verlangen. Später in ihrem langen Eheleben - Margrit starb 1959 - hat
sie das erkannt..

Aber am 26.Mai 1918 schreibt Eugen einen langen Brief an seine Frau, in dem er über
Margrits innere Entfernung klagt, ihr sein Unglück schildert. Margrit fällt aus allen
Wolken. Verzweifelt, unter Tränen schreibt sie zurück, drei Briefe an einem Tag. Nur
ihm sei sie eigen, nur auf ihrer gegenseitigen Liebe bauend könne sie Franz lieben. Das wisse er doch. Eugen breitet in einem folgenden Brief sein Leiden ausführlich und geduldig vor ihr aus. Sie wirft ihm mangelndes Vertrauen in ihre gemeinsame Liebe, in
die Kraft ihrer Ehe vor. Sie fleht um neues Vertrauen. Sie schickt Eugens Brief vom 25.5.
an Franz weiter. Die konsternierende Wirkung auf Franz zeigt sich deutlich, wenn man
dessen Briefe an Margrit vom 1. bis 25. Juni 1918 liest.

Wirklich, es gelingt Eugen und Margrit im Verlauf ihres Mai/Juni Brief-Austauschs
neues Vertrauen zu einander zu finden und neu zu begründen. Die Liebe zwischen Eugen
und Margrit erlaubt es erneut, die Beziehung zwischen Franz und Margrit bestehen zu
lassen. Eugens grosser Seele hilft sein Glaube.

Und es bleibt dabei. Denn es liegt ja im Juni 1918 noch die ganze lange Zeit der
hunderten von Briefen von Franz an Margrit und Margrits verlorenen Antworten vor
diesen drei Menschen, während Franz von August 1918 bis Februar 1919 den "Stern der
Erlösung" schreibt und den Fortgang vor Margrit fast täglich ausbreitet. Inzwischen ist
im November 1918 der Krieg zuende gegangen. Margrit lebt mit Eugen. Aber die
"Gritli' Briefe gehen noch eine ganze Weile weiter.

Hier nur der letzte Satz von Eugen am Ende seines Briefes an Margrit vom 11.6.1918: Er
war hinter der Kriegsfront bei Verdun, verantwortlich für eine Feldbahn. Es war eine
berittene Einheit.

"Ich liess gestern vor meinem Wagen die Rösslein wie wild in die tiefe Nacht hinein
rasen. Wie sie so fegten in die klare kühle Nacht, von meinem Zügel gehalten, war mir
wie dem Gesundenden zu Mute, der seine Kräfte wieder spürt."

Man kann die Bedeutung von Eugen und Margrit in Franzens Leben kaum überschätzen.
Eugen sah in Franzens' "Stern der Erlösung" eine Antwort auf sein eigenes Denken. Wie
Eugen gibt auch Franz dem Medium der Sprache die fundamentale Rolle in allen
Beziehungen des menschlichen Lebens, wozu auch die Offenbarung gehört. Wie könnte
er sonst am 6.10.1929 noch an seine Mutter schreiben: "Sprache ist doch mehr als
'Blut'".

Margrit darf man zugestehen, dass sie in diesen Jahren zwei Männer liebte, was man im
Laufe der Jahrhunderte und in verschiedenen Gesellschaftsordnungen meist nur den
Männern zugestanden hat. So oder so ist es mit vielen Schmerzen verbunden.

Freya von Moltke, Dezember 2003
