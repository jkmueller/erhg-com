---
title: "Quotes about Rosenstock-Huessy from Martin Buber, W.H. Auden, Page Smith, Lewis Mumford, Harvey Cox"
category: aussage
published: 2022-01-04
name: Buber
language: english
---

„The historical nature of man is the aspect of reality about which we have been basically and emphatically instructed in the epoch of thought beginning with Hegel... Rosenstock-Huessy has concretized this teaching in a living way that no other thinker before him has done.”
>*Martin Buber*

„Rosenstock-Huessy continually astonished one by his dazzling and unique insights.”
>*W.H. Auden*

„I am a poet by vocation and, therefore, do not expect to learn much about Language from a writer of Prose. Yet, half of what I now know about the difference between Personal Speech, based upon Proper Names . . .  words of command and obedience, summons and response, and the impersonal “objective” use of words as a communication code between individuals, I owe to Rosenstock-Huessy . . .  I can only say that, by listening to Rosenstock-Huessy, I have been changed.”
>*W.H. Auden (1970)*

„He was a thinker of startling power and originality; in my view an authentic genius of whom no age produces more than a handful.”
>*Page Smith*

„Rosenstock-Huessy's is a powerful and original mind. What is most important in his work is the understanding of the relevance of traditional values to a civilization still undergoing revolutionary transformations; and this contribution will gain rather than lose significance in the future.”
>*Lewis Mumford*

„It is unfortunate that Rosenstock-Huessy's thought has been so overlooked. For years he has been concerned with many of the same things theologians are grappling with today, that is, the meaning of speech, the question of hermeneutics, the problem of secularization, and the disappearance of a sense of the transcendent in modern life.”
>*Harvey G. Cox*

„Rosenstock-Huessy’s Speech and Reality is to sociology what Galileo’s “Discourse on Two New Sciences” is to modern mathematical physics because both books transform philosophy—natural in the case of Galileo and social in the case of ERH—into a positive science.”
>*James Eric Lane (2006)*
