---
title: Jahrestagung 2016
created: 2016
category: jahrestagung
thema: Leben wir nach dem Krieg? Zu Eugen Rosenstock-Huessy's Text „Nietzsche's Masken” 1944
---
![Imshausen]({{ 'assets/images/Imshausen.jpg' | relative_url }}){:.img-right.img-large}

### {{ page.thema }}

Datum: 4.-6.11.2016

Ort: Stiftung Adam von Trott, Bebra-Imshausen

---

Programm folgt.

---

Anmeldung bitte an Andreas Schreck, Tel. +49 (0)551-28047871; schreckschrauber :at: web.de
