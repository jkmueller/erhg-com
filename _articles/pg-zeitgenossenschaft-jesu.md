---
title: "Peter Galli: Die 'Zeitgenossenschaft Jesu' und ihre Bewährung im 'Kreuz der Wirklichkeit'"
category: einblick
published: 2022-01-22
---
Wissenschaftliche Arbeit für die Zulassung zum Staatsexamen

bei\
Herrn Prof. Dr. Joachim Köhler\
Katholisch-Theologisches Seminar \
Fachbereich Kirchengeschichte

**DIE "ZEITGENOSSENSCHAFT JESU"\
UND IHRE BEWÄHRUNG IM "KREUZ DER WIRKLICHKEIT".**\
***Eugen Rosenstock-Huessy (1888-1973) und Joseph Wittig (1879-1949).\
Der Weg zweier Glaubensgenossen in der\
Weimarer Republik.***

vorgelegt von: \
Peter Galli \
Bachgasse 9 \
7400 Tübingen

[Der Text als PDF-Datei]( {{ 'assets/downloads/Galli_Peter_Zeitgenossenschaft_Jesu_ERH_Wittig.pdf' | relative_url }})
