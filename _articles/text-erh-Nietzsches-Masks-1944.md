---
title: "Rosenstock-Huessy: Nietzsche's Masks (1944)"
category: online-text
published: 2024-01-10
org-publ: 1944
language: english
---

Nietzsche said of himself that in himself the Christianity of his forbearers drew to its ultimate conclusion; that the incorruptible honesty drilled into him by Christianity, now took its turn against Christianity.

At the same time, he complained that he was to wear many masks, of the fool, the prophet, the philologist, the prophet, the Devil, the Saint, the child, the camel, the lion.

Is there a connection between the two statements?

I think, indeed, that there is an important relation between the histrionic features in Nietzsche and his inherited Christianity. And since all modern men, more or less, are exposed to this danger, it seems worthwhile to state this relation between the comedian and the Christian.
It will be a detour, to attain a superficial judgement. As a figure in the history of the Church however, Nietzsche cannot be done justice in a different way.

Theatre and tragedy were the two central experiences of his soul. The tragic aspect of life was the Dionysian truth which he tried to impress on a world which ran away from tragedy into enlightenment and progress. And the old Church had compared Christ to the tragic hero.

In the Greek Churches the parts of the Sanctuary are named after the parts of the Athenian stage, even today.

The Easter ceremonies replaced the tragedy of the dying and rising God which had prevailed for more than two thousand years around the whole world. Before Christ, everybody knew
that a God died, that men slew the God. Everybody before Christ had celebrated the rhythm
of ecstasy and mania and fall and depression, which is marking our lives. Only the last two centuries of our era introduced a not rhythmical concept of time, a mathematical time, without highs and lows. \
Our lunatic asylums bear witness to the fact that man cannot live without manias and depressions. When they are denied him in public, he will still try to create them over against the lack of rhythm of the machine age.

Of course, nobody can create the rhythm of society alone. Hence the modern individual frequently breaks down under his manias and depressions. \
The dying and resuscitation of the God is an event of social history. There it has its place as the perpetual rhythm of revolutionary inspiration and evolutionary realization of new creation coming into old nature.

So Nietzsche was right when he stressed that the life of the soul in society has his ecstatic and depressive phases. He unearthed an undisputed feature of the original Church. A God who does not die and rise again, cannot become man. Because we have our ups and downs, we cannot ever live on the peaks of our own experience. Of the revolutionary ecstasy and the patient, indefatigable labour to unfold the germ, once sown into us, our inner life is composed.

The ancients enacted annual tragedies by wearing masks. The masks made allowance for rhythmical ecstasy, for being embraced by the divine in an unembarrassed way, in the high moments. To wear a mask, to play a role, was man’s training for his heroic qualities.

Now,  when the Church replaced the theatre by the Mass and Dionysos by Christ, the only thing that went away was the mythical character of the tragedy. The crucifixion was a real tragedy, no play. The martyrs were real, no actors. The play became brutal fact. In his experience of dying like the first Christian, every man found his role, his identity, his singleness. Whereas Osiris or Linos, or Adonis or Dionysos or Marduk invited the faithful to wear his, the God’s mask, the Christian in as far as he died to the world, made his own face the true face for the divine. In his victory over the world it could shine through him. \  
The masks of wood and paint gave way to the human face itself as the mask of God. In this sense God appeared in Jesus. And it may be said without blasphemy that on the Cross Jesus was God’s mask.

In the Byzantine Church it was forbidden to depict the Father or the Spirit. The Son alone was God’s appearance on this earth. And for this reason art painted him, not the Father. Only in the Renaissance and with the greatest reluctance, the Russian Church broke this rule. She should never have given in.

It was a wise rule. Until the Renaissance Man carried his face as the only mask of God. As long as people believed in incarnation, in the embodiment of the Spirit, they preserved the power to become integrated  into their own definite personality by one ray from God. A real person is God’s mask on earth, one of Him.

That we should count the years of a new era with the coming of Christ, makes sense. Because ever since Jesus men received the power to deify themselves, to throw of the mask of their specific nation, cast, title, family name, religion, property, nobility. They pierced the legal, hereditary, conventional, normalized wooden or iron masks: the brass hats, red tape, lingos, group egotisms, their political masquerades, or however you like to call the hindrances between our face and the light of uniqueness shining upon it.

The time-worn phrase of the image of God becomes transparent when we remember that Christianity replaces the material masks of wood or linen by his power of letting the light shine through the human face itself: Image instead of mask.

And so, in our era disappeared the masks of tragedy, of red-painted warriors, of Bali dancers. Even the cothurn on the stage was abandoned, and in a play finally men no longer were required to act the parts of women.

On the other hand, the Church was not so insipid to deny the deep seated urge in all of us to be educated for this supreme liberty of becoming an image oneself.  
Before we can receive the full light ourselves, we must learn to discriminate between mask and image by looking upon others. The image is found in the faces of all those who under the pressure of danger showed the power to let the light through. They formed a row of windows upon eternity. The saints surrounded every newcomer as an assembly of deified men.
To give a poignant example. When the sturdy Saxons finally became Christians, their princes built monasteries. It was only fifty years after the conversion that the young princess Hathumoda became an abbess. Her brother, an abbot he, ten miles away, comforted the nuns over her death by comparing Hathumoda with eleven saintly women in the Bible, in his distichon: \
“Sara, Rebecca, Rachel, Debora, Noemi, Ruth et Anna,\
Holda, Susanna, Judith, et simul Hester…”[^1]

The tribal gallery of ancestors was replaced by this host of souls.
In Puritan England as well as in New England, this was repeated many centuries later.
Martin Luther thought of himself as St. Paul redivivus. Pope Innocence III spoke to his ecumenic Council as though he were the dying Lord, in the words of Luke 22,15[^2]
Francis of Assisi received the stigmata of Jesus in his body.

[^1]:  a.D. 874 : Agii Obitus Hathumodae    Monumenta Germaniae Historica, Scriptores IV, 181
[^2]:  (“With desire I have desired to eat this Passover with you before I suffer,” ) and applied this great sentence to himself.

In a purely secular manner this habit was in full swing in Nietzsche’s youth. In cafeterias, the literati would salute each other in deadly earnest ways as the new Cromwell, the new Mahomet, the new Alexander of Euripides. Ernst Rohmer, in the year of Nietzsche’s birth posed as the new  born founder of the Church himself. If this was madness, there was method in it. Men started their role in life by trying to play the role of somebody else.

True enough, these plays of imagination degenerated. But even they testify to the bold imagery which allowed the offspring of some sectional, narrow group, town, family, to choose among the infinite multitude of souls through the times for his own growth.

From the time between the Saxon religious discipleship of Biblical Saints and the cynicism of Nietzsche’s own times of adopting any sponsor for one’s own soul I would like to quote an intermediary voice from the dawn of the Renaissance. The heroes of the Renaissance were favorites with Nietzsche. One of them, the Roman Cola di Rienzo, is the hero of a Wagner

opera. In his devotion to the restoration of Roman splendour this Rienzo confessed of himself: \
“I was drunk (ebrius) with the ardour of a burning heart to abolish the errors of partisanship and to lead back the peoples into unity. And so, for this purpose of Charity, I would enact histrionically the jester, the grave man or the simpleton: now play the astute, and again the passionate, or the timid; here the stimulator and there the simulator. And as I handled myself like David who danced before the ark, and acted the madman before the king, or like Judith who stood before Holofernes , cajoling, astute, and gardener. Or like Jacob who received the
blessing by a cheat.[^3]

[^3]:  Konrad Burdach und Paul Piur, Briefwechsel des Cola di Rienzo, Berlin 1912, p. 245

In this hero of the earliest Renaissance, the histrionic element –  in Latin this term is used by Cola himself – is obvious, and it is excused by Biblical examples. The Saints are no longer models but excuses.

Nietzsche, as we said earlier, considered himself the last stepping stone out of Christianity into the future. He sang of himself that he “lest his blessedness oppress you, took upon himself the mask of the devil’s dress, fraud, and spite. But don’t be deceived; inside there shines the face of holiness.”   

Nietzsche wore two masks before his own inner consciousness. Of course these both masks are central, to understand his real role. One mask was made necessary by his soul’s experience, the other by his mind’s fate.

A man’s soul is in his love. And he receives his true love from that which he loves, which, in other words, is his cosmic affinity. “It’s my soul which calls upon my name”, Romeo rightly says, in a verse worthy of the New Testament.

What was the name which came to Nietzsche from his soul’s love?

In 1869 he met Richard Wagner and Cosima Liszt. Cosima had left her husband Buelow and their common children, and had ran away with Wagner. She was Nietzsche’s age. He fell in love with her. Her free situation made this a not impossible impulse. On the other hand it’s obvious that Cosima was now with more than matrimonial chains bound to Wagner: her whole moral existence was at stake; he Wagner, had to be the God. (When Wagner died, she cut her hair and put it in his grave.) Nietzsche did not have a chance, either during Wagner’s life or afterwards since this would have disqualified Cosima’s sacrifice for the God.

But Nietzsche was in love. And Buelow visited him in Basel, and told him that he, Buelow, was the earthly king Theseus whose wife Ariadne was stolen by Dionysos-Wagner. However, Buelow added: “She ruined me; she will do the same to him.” From that day on, it seems, Nietzsche considered himself as the future Dionysos of Ariadne-Cosima. We know he prided himself for keeping the secret of this name, Ariadne. His first outburst from his masked life, his madness, was condensed in a telegram to Ariadne-Cosima, confessing his love. And as Cosima’s husband, he marched into the clinic.[^4]

[^4]:  (Here a footnote is indicated, but not realized. There is only a dot at the bottom of the page.)

So in his own mind his Ariadne secret marked him as Dionysos. Whereas Buelow-Theseus had admitted that the great magician Wagner came as the God Dionysos into Buelow’s terrestrian home, Dionysos was torn to pieces on this earth, had no foothold on this globe for Nietzsche. The tragic Dionysos is the Dionysos whom he had to impersonate. On earth Wagner-Theseus reigned.

The second inner identity and jealousy centred around Jesus. To the extent of his mental rivalry with the founder of the Church his Ecce Homo is but one hint. Often enough he would poke fun at any Messianic ambition, at any Christ-Complex. However, the more he attacked all fruits of Christianity, the more he did compare his own task to the millenarian attainment
of its Lord. Again, his end – as in the case of Cosima – revealed the truth. In his last pronouncements he signed himself as the crucified. It’s not in vain that he had chosen the title: Ecce Homo, and the Antichrist.
Here he was to start another epoch. We have it from his own pen that in September 1888 the new era began which was to put an end to the Christian era. His intellectual ambition came out in the open in this era-statement.

We have called the two masks of Dionysos and the Crucified the inner masks under which he posed to himself. Both had to be kept silent before the world. And Nietzsche did not appear to the world as completely mad as long as he kept enough self-control to know that he had to conceal these two equations from all and everybody. If these masks, in themselves, betray madness, he was mad during the whole period of his highest creativity. If on the other hand madness merely means the loss of such cleverness by which we keep our secrets to ourselves, he went mad only in 1889.

The obvious truth seems to me that Nietzsche represents the admixture of madness and sanity which holds up so many people. Most men are to themselves somebody quite different from that which the world attributes to them. Nietzsche, it is true, to the extreme represents this universal discrepancy between the inner and the external “mask”. But he who wished to bring out the riddle of our human torn-to-pieces-hood, took of course the most extreme position. This does not mean that we all may not be enlightened by his flagrant example.

When you ask yourself in how many social forms you share in internecine wars of mankind, you will see how multifarious you are. A farmer, a catholic, a father of a future doctor and future daughter-secretary, a New-Englander, an American, a Republican… And this is a mild list of conflicting interests. Starting from this basis of a multitude of conflicting interests, we may ask for a road to integration. And here we may learn from Nietzsche.

***Nietzsche: The Annihilation of Time***\
*The story of the two flyers in the New York Night Club: twenty four hours ago we bombed Essen. Space, then, seems to be annihilated. Ubiquity of Man has something divine.
Nietzsche has not to do with the annihilation  of space but of time. If man could be all times, he would become sempiternal which would seem divine. For by his nature , man belongs to one time. And he would redeem his nature if he could belong to all times. But to do that he must become conscious that even he himself is of more than one time. This the decadent knows because he is too little wedded to his own time or passion so that he can wink at the moment and say ‘All is in vain’. The fact of decadency means that a man overlooks the mortality of his own season and thereby  usually  is paralyzed. The decadent does not act any longer except from boredom, in a half indifferent or snobbish fashion. This is the starting point for Nietzsche: How to heal the decadent. How to induce him to keep his refined consciousness of the manifoldness of times, and yet make him live, within the universe of all times and seasons, the duties of each season to the full?* ()[^5]

[^5]:  (There are two pages f  in the manuscript which is numbering the pages from a to m. This is the first.)

In his writings ***the external*** masks of Nietzsche were many: critic, thinker, poet, jester, politico, theologian, historian, etc. etc. But substantially Dionysos wore three permanent masks, masks which he did not simply choose, but which were inherent to his existence. We may call these three his existential masks.

One was the professor emeritus, the physically weak and decadent Nietzsche. He acknowledged this decadence profusely. He added that, whereas the times in which he lived were decadent unknowingly, he recognised the evil, being more heavily visited by it than most of them.

As a decadent who knew that his time and he himself were degenerate times of the end, before a terrible catastrophe, he affirmed his fate which had made him a recluse at 34. He ceased to belong to his time. And indeed, for his most powerful  book he did not find seven readers when it appeared. As he called it, he was  a posthumous man.

It’s a true fact that Nietzsche has come into his own long after his mental and physical death. This makes it necessary that we coin for it a new term. Nietzsche is the prototype of an important possibility in all of us who have been schooled by Christianity. We all are ***distemporaries***. We do not exhaust our existence by being contemporary, by being a current event. Nietzsche abandoning currency and contemporaneity, coined as his medium the eternal re-currence. Without going into this special doctrine here, it’s shedding light on the fact that he was the distemporary of the years 1870 to 1900, the decade in which “God died” from the hands of those who were satisfied to be purely contemporaries and to be found in the “Cavalcade” of the day, to be in the news, in a sense in which Jesus was the contemporary not of Cesar Augustus but, if at all, of perhaps the emperor Constantine or even the emperor Charles the Fifth..

The second mask was the brutal Nazi-Nietzsche. Withdrawing from his own era of progress, he foresaw the end of this era of mere exploitation, of the “last man”, and foreseeing it, to some extent he had to identify himself with the generation of violent destroyers who would smash these decadent decades. He foresaw the terrors of an indescribable explosion. To him it was inevitable by the very fact that his “con-“temporaries had invested their all and everything in their own time, that the tragic element had disappeared from life. Nobody believed in an end of this world. Hence it could not survive.

Again, here he shared the source of all the strength ever found among Christians, that the end of this world was on hand, that this world had to pass away so that the kingdom might begin.
Jesus anticipated the fall of Jerusalem, later the fall of Rome was anticipated. Eschatology realized was the heart and soul of all real faith, in the Church. But from 1800 on the Church, except for Millenarians, or Jehova’s Witnesses, had throttled this belief. Right after Nietzsche broke down, Johannes Weiss rediscovered the eschatological element in Jesus. Today the work of the Englishman Dodd has made this rediscovery of  “eschatology” common property. By  this highfaluting term simply is meant the fact that Jesus and the apostles believed that they had to realize the end of their world. To the Liberal theologian this comes as a shock. One of them, Kirsop Lake, naively wrote that this fact was proof enough that no modern man could be a Christian since no modern man could believe in such bosh. All the worse for the modern man whose world, currency, prosperity, profits, laws, all are vanishing before our eyes. Not because these his orders are especially bad. But because man have given themselves over to their own time and state without any reserve, without any posthumous presence, any reservation for a new and better kingdom to come. Longing for an other state of affairs is the condition for even preserving that state of civilisation already attained.

At a time when no theologian, no official Christianity showed any faith in an end of time, Nietzsche believed in it and acted accordingly. Here, the very fact that Ariadne could never become his in this world, must have helped him immeasurably, as Dante’s Beatrice had to remain unattainable lest the Divina Commedia never be written.

However, back to his identity with the destroyers, the Fascists, the carriers of the hammer, the Nazis. Insight obliged Nietzsche to admit this type of man. The world was ripe to be buried. Could he dissociate himself from the craft of the gravediggers?

Intellectual honesty forced him to share with them the responsibility. He was too clean not to know that responsibility has as much to do with thoughts as with acts. If he thought the end of Europe, how could he deny that somebody had to bring it about, and that relatively they were as right as he in his mere thinking. The false intellectual of today may affirm that his writings are less dynamite, less murderous than the bombs of soldiers or revolutionaries. For such bookkeeping Nietzsche was too robust.

In this sense, then, his second mask was that of a contemporary to the wars of destruction which would shake Europe and lead to the abolition of Protestant Christianity in Germany.

However, he lived in a third tense. Before the catastrophe the decadent, in the catastrophe the preacher of the hammer, after the end – Zarathustra. Of course Zarathustra is Nietzsche’s  mask which is best known. He himself called it a duplication of his personality when he “met” Zarathustra. “Out of one two arose”. Zarathustra is the third mask, the mask which enabled Nietzsche to set foot on the new soil, after the great flood. Zarathustra is the legislator of a new world order in which time will again be linked to eternity, in eternal recurrence.

It was a stroke of genius, this thrust beyond the catastrophe into the future after the World Wars, as of the medicine man of a united mankind, of a denationalized humanity which he foresaw. What could keep such a monster society of world size inside alive if not a medicine man who would represent to the rulers and chieftains of the day the eternal energies of all ages and all times, lest the present again overwhelmed the contemporaries as during the 19th century?

Nietzsche, as Zarathustra, is the first medicine man of a Great Society in which the recipes of all tribes of old are put to new and better use.

It was a masterstroke of Nietzsche to choose Zarathustra. Historically, we now know more of Zarathustra since Johannes Hertel has destroyed many myths current in Nietzsche’s days.    
Zarathustra led nomadic tribes to their first settlements under the Persian protectorate. \
He lived before the division of prose and poetry happened, before speech and song separated. He lived outside the Platonic orb of Greece and Rome. He made modern man free from the prayer to Socrates and Plato to which Erasmus of Rotterdam had intoned and which all American Colleges implicitly prayed until this last war.

Nietzsche went back behind Greece in order to go forward.\
I have shown elsewhere that mankind lives its historical future with a mask of reclaiming a more and more remote past before his eyes.\
Christianity has given us the power to fulfil our lives by reaching out, at the same time, and for this very purpose, into deeper and deeper layers of our evolutionary past. Zarathustra links up with the modern passion for prehistory, anthropology, primitivism.

Zarathustra enabled Nietzsche to claim the next step by opening up a step back of Plato and Socrates and Aristotle, the deities of the academic mind.

However ingenious for his purpose this name was, it nevertheless was a mask, an external mask. Jesus did not have to play upon history or prehistory in the same manner. He simply was the second Adam, the final man, the man as God had had him in mind from the beginning of the world. \
Nietzsche’s rivalry to Christ would have been very childish indeed, if Zarathustra was all for which he stood. A literary figure, a learned rediscovery, a renaissance perhaps, but not a person himself.

It would not have meant more than a Plato redivivus meant in the Remaissance.
But Nietzsche was someone and Zarathustra was someone. And the Professor emeritus was a third reality. The tension between the three tenses of these three… that was Dionysos, that was the Crucified, that was Nietzsche’s Ecce Homo. The decadent who knew that he and his time were the end, the destroyer who knew that his thought was as cruel as plough and hoe and hammer of material steel, the legislator after the destruction, - these three tenses make a man into a person, into a human being.

By the circumstances of a godless time this is the true gospel, lived again by one man alone since the others had abolished the death of civilisation, let alone the belief in resurrection.

Through his masks Nietzsche re-instated the triunity of human life, as the transition between past and future for which we are made. Man is not a thing, man is nothing. He is the change. At least only in this manner does he attain reality, personality, deity.

The three existential masks of Nietzsche make him have a life before, in and after a tremendous catastrophe, the end of Europe. \
To some extent, every human being is required to think of himself as being in the same situation, before the end of the world which he has inherited, in the moment in which this world must be buried, and after the burial, as lawgiver of the future. Any girl who marries, could realize this tri-unity if she took her getting married seriously enough.

In a world crisis of the first order, the pressure proved to be too much for the carrier of masks, and one man alone cannot bear that which fellowship can carry. It is the proof, and not the refutation of Nietzsche that he broke down, lost his self-control and died mentally from his masks, from the gap between inner and outer masks, eleven years before his body passed away. If it had not proved to be superhuman, neither his distemporaneity, nor his tri-unity would have stood the test of being true.

So called failure in his case was success. Without ever integrating them to one human face before God’s eyes, no man can carry his masks as Nietzsche did. But he did prove that man is not one by his nature. That only by good grace we can reduce our infinity of masks which society allows us to don, to the essential and existential tri-unity of our past, our future, our present. Man as long as he lives never carries only one.
On the other hand he cannot stand many. Man can neither be a joiner of all roles and social functions, nor pretend to be a hero. Psychology  which thinks of the human person that unity is normal for man, and schizophrenia a disease, is quite mistaken. Man is not a unity by nature. He is one when he is a corpse. As long as he lives, he must distinguish between his dead and his future elements. Thereby he is made into three functionaries of his own life. He must be in part his own gravedigger, in part his own prophet. Triunity is the most we can achieve. It’s as near to divinity as we ever may hope to come. Our indelible water mark as human beings is this cross of our distemporaneity, of our polychron existence..

This cross by which man is nailed to the tree of time, between the past and the future, with the inner man prophesying and longing for the new legislation, with the external man looked upon by the outside world as though they knew him completely already, this cross of the real man – or as I have named it: the cross of reality, is not one historical event in Palestine, but a scientific fact of general knowledge, since Nietzsche.

After having been a unique revelation for 2000 years, the Cross now is a matter of science, of the new science of man. Only because man is the carrier of space and time, in setting limits to past and future, in his inner and outer masks, the revelation of man’s nature by Jesus is true.

I hear you cry out: but this is a sophistic abuse of the term “Cross”. The execution of a rebel by the Romans, the action of one day, is by this your trick suddenly identified with man’s whole situation between the cradle and the grave. You see any man suspended between past and future, in an inner space of his own terminology and in an external space where he is classified by the looker-on world. What has your “Cross of Reality” – supposed it is true – to do with the historical cross around which the Church is built?\
For the modern mind who has been fed on an alleged “Life of Jesus”, to which the Crucifixion became a regrettable thing, and the resurrection an unbelievable hieroglyph or an apocryphal word about which people shrug their shoulders, this objection is perfectly sound.

The four writers of the news about Jesus, however, are decidedly on my side and against you dear believer in a life of Jesus. They never intended their gospels to be biographies. Biographies are quite well known, in their days. Plutarch was their master. But the evangelists intended to write “thanatographies”.

To them the crucifixion began in the manger, when no place could be found for the new born child in the hostel. And Herod continued immediately this crucifixion when he drove them into Egypt. Life was seen exclusively from the viewpoint of dying, and at the same time of fulfilling. Dying into a better future, created by this process, fulfilling the past of the law and thereby allowing it to come to an end.

And the old Church is on my side, against the modern philosophy of Christianity as a timeless sermon on the mount. They celebrated Christmas, Easter, Pentecost, as the three decisive forms of the Divine in this world. But these three central incisions simply constitute the three existential masks of every deified soul. As a child born into an old world of inheritance, inheriting its promises and defects, man is a Christmas present and an heir of all good things of old. In the same way as Nietzsche experienced himself the heir of the unfathomable honesty of two thousand years of Christianity..

As a man entering into the consequences of old defects, and forced into the destruction of the Temple as it stood for too long already, Easter happened. Jesus knew that his anticipation of the kingdom destroyed the Temple at Jerusalem in the spirit as definitely as the Roman soldiers of Vespasian did in the flesh, one generation later. Being no intellectual, he never doubted that thoughts were as real as arms. His cross was unavoidable because he anticipated the fall of the Temple.

As a creator and founder he began the new Jerusalem, a new order. In its purport after the year 70 that became fully clear,  a whole generation after his crucifixion. Pentecost signifies that this is his creative power. It’s for that time in which the “times” would have caught up with his spirit. In other words, he then would have ceased to be a posthumous man as Nietzsche called it, or to be a distemporary as we preferred to dub man as a victor over current events.

Nietzsche’s decadence is synonymous with his miraculous career as a university teacher. He was made a full professor at 25. In this sense he seemed to fit into the old world to perfection.

His brutal attacks against this same world as doomed  took him out of this same world, made living with him intolerable for every one of his friends, deprived him of participation in the outer world.

His anticipation of decay plus explosion threw him beyond into a kingdom of ends, of new ends, after this period from 1914 to 1944. General Smuts rightly called them a unity, these thirty years in which Europe gave up its leadership over the world.


It would be more than bad taste, it would be wrong to call this situation of Nietzsche between the times, between three times, with the old, venerable names of Christmas, Easter, and Pentecost.

It makes a great difference whether a  man plays with his blood, or with his brain. And Nietzsche remains in the era of mental inheritance, mental rebellion, mental legislation. His adventure was confined to the fight against mental fallacies. In his first steps he did not reveal human nature as completely different  from the nature of the world. But this revelation he proved scientifically by interposing himself as the living guinea-pig.

The equation between his new scientific proof of the Cross as water mark of  Man and Christianity, is something like this.

Outside Christianity man had appeared as a “character”, a coined entity, by birth, property, cast, nation.\
Jesus revealed that no man was one, once he started on the road to deification. The divine in man was his triunity, his mastery of time by living in more than one time, and in more than the external world. He presented us with the insight into life’s fullness: it is between two orders of society, the old and the new, and between two sets of values, the inner and the outer. In one single case Jesus made it clear that the real man was not a coined character but a suffering transformer.

Nietzsche abandoned all reverence to faith or revelation. He jumped out of the circle of theology as far as he could. And he rediscovered that even though he might forego all the fellowship and all the traditions of the Church, he still would find himself suspended between cradle and grave, on the same cross.\
Jesus showed how a man acquired the right of being called the image of God, the master of all times. Nietzsche showed what happened when a man did not acquire this mastery: he went to pieces.

Both, Jesus and Nietzsche stood against the fallacy of all paganism, all natural science, that man is as much “one” as a table or a fish or a rock. Both knew that man had to fight for his integration, not by pretending to be a unit – as most contemporaries do – but by organising the infinite number of impulses, talents, gifts, into the discipline of a sequence. Both discovered that the conflict between the One and the Many could not be healed, on the one hand by calling man either a bundle of nerves, a split personality, a cog on the wheel, a mass-man ruled by stimuli, or on the other hand by idealising him into a personality of unshakeable virtue, a hero of courage, and a genius of infallibility. Both these temptations of which our times are filled we have to reject: the mass-man temptation according to which man is that whatever the day requires and the ideal-temptation according to which man can attain perfection. Then we can regain our true freedom: to rule over times, to become a distemporary, to become a transformer, to become a legislator.

A man who cannot bear this suspense, is lacking personality. In the Christian era we say: he is lacking faith. After Nietzsche we are allowed to add that he is lacking knowledge.

Man, saved by Jesus from his slavery to time, acquires with Nietzsche an objective knowledge of the fact that man cannot become known through any science of space.

Man must be dealt with by a science of time. Man in his unnatural, human quality doesn’t appear at all in the rationalisations of science. Because man creates sciences for his purpose of distinguishing between past and future. His natural sciences allow him to dispose of the encircling gloom, and to see clearly the things on the road. But the faith and courage to be more than a thing himself, is marking the MAN, among scientists as well as among other people.

Nietzsche’s masks are everybody’s masks. Nobody appears to himself exactly as he appears to his neighbour or to the statistician in Washington D.C. We all, however, are in a more advantageous position than he in his loneliness. His masks were the most desperate ones, the gap between his past and future, between his inner evaluation of his role and his outer appearance was wider than in any thinkable case after him.

The extreme case is necessary for any scientific proof. We rightly conclude that “if even X behaves in this manner, how much more will Y and Z behave similarly”. And so we conclude that since even Nietzsche reorganized his innumerable masks around the three tenses of Christmas, Easter, Pentecost, or of finding, fighting, founding, and since he, the weakest of all men, alone survives from all the thinkers before the World-War revolutions, the Cross of Reality must be the scientific truth about man. For it made him strong, it made him important, it made him last.



Eckart Wilkens translated the text into German: [„Nietzsches Masken”](https://www.eckartwilkens.org/text-erh-nietzsches-masken/)

[zum Seitenbeginn](#top)
