---
title: Eckart Wilkens zur Soziologie
category: weitersager
order: 45
---

**BRIEFE AN NIKLAS WILKENS,**\
**DERZEIT IN**\
**47 MAINSTREET CASTLETON**\
**IN VERMONT,**\
**ZUR SOZIOLOGIE**\
**EUGEN ROSENSTOCK-HUESSYS**\
**1956/1958**

**INHALT**

- Erster Brief: Das Erbe der klassischen Musik
- Zweiter Brief: Nach Darwin, Nietzsche, Freud und Marx
- Dritter Brief: Die Schrift als Floß für das ganze Dritte Jahrtausend
- Vierter Brief: Inspiration durch die Liebe (Goethe)
- Fünfter Brief: Das Entfalten
- Sechster Brief: Das Kreuz der Wirklichkeit
- Siebter Brief: Die visionäre Erfahrung des Weltkriegs
- Achter Brief: Systematik
- Neunter Brief: Die Wiederverhüllung als Offenbarungsmoment
- Zehnter Brief: Namen, Wörter, Dinge
- Elfter Brief: Die Gesetze
- Zwölfter Brief: Christentum

*Geschrieben vom 2. Advent (5. Dezember 2004) bis zum 2. Sonntag nach Epiphanis (16. Januar 2005) in Köln*\
*Eckart Wilkens*

 **ERSTER BRIEF: DAS ERBE DER KLASSISCHEN MUSK**

*Köln, 2. Advent 2004, 5. Dezember*

Lieber Niklas,

nun ist der Augenblick gekommen: ich habe die beiden Bände der Soziologie aus dem Bücherschrank geholt, um meine Briefe an Dich zu beginnen.

Seit 1963 begleiten mich diese Bücher, nun schon über vierzig Jahre. Sie sind nicht veraltet. Bei jedem Lesen springt ein neuer Funke hervor. Es ist so wie mit den Werken der klassischen Musik, die sich im Spielen und Gehörtwerden nicht verbrauchen.

Ich kaufte sie mir 1963, als ich für Dora Stössel ein porträtähnliches Bild mit Öl auf Leinwand gemalt und an sie verkauft hatte. Für diese hundert Mark konnte ich mir den schon seit 1958 gewachsenen Wunsch erfüllen und die Soziologie von Eugen Rosenstock-Huessy kaufen, deren zweiten Band: *Die Vollzahl der Zeiten* mein Vater im Jahre seines Erscheinens in der Volkshochschule in Rendsburg – *Rendsburger Ring* hieß sie – im *Haus der Wirtschaft* vorstellte, von dessen Treppenabsatz aus ich einmal Konrad Adenauer über den Paradeplatz hin habe reden hören. Vater war von dem Kapitel über die Zimmer der Wohnung und ihre Zeiten so begeistert, daß er am Eßtisch davon erzählte.

Am 4. April 1963 starb meine Großmutter Mary Schröder geborene Groth – und damit ging von mir die Person, bei der ich mich am besten aufgehoben fühlte. Es war, als ginge die Trauer um ihr Dahinscheiden – ich war es, der als letzter mit ihr sprach, und dabei drückte sie mir auf eine Weise ihre Liebe aus, die mich noch heute durchschauert -, aber auch ihr Segen, den sie in wunderbarem Erinnern an Großvater Friedrich Schröder unzertrennlich an mich weitergab – Du weißt, von ihr habe ich die Familienbibel der Eltern Großvaters vermacht bekommen, aus der ich den Psalm 103 zu Euren Geburtstagen lese – in die Lektüre des Werkes von Eugen Rosenstock-Huessy ein, der sechzehn Jahre jünger als sie war und zehn Jahre nach ihr die Erde verließ.

Die Glut der Begeisterung, die mich aus diesem Werk ankam, war so stark, daß ich gleich wußte: um das weiterzusagen, brauche ich viel Zeit.

Nun klagen viele Menschen darüber, daß die Texte Rosenstock-Huessys nicht so leicht zu lesen seien. Damit habe ich als Schwierigkeit niemals zu tun gehabt. Vielleicht meinen die Leser ja auch die Schwierigkeit des Weitersagens, die ich eben auch erlebte.

Aber das hilft ja nichts, wer beim Lesen Schwierigkeiten hat und nicht gewohnt ist, daß die Schwierigkeiten eigentlich erst beim Wiederlesen anfangen, wenn man sich als Leser nämlich beginnt, Rechenschaft von dem Gelesenen abzugeben, liest eben erst gar nicht zuende und rechtfertigt sein Nicht-zuende-lesen leicht damit, daß es eben zu schwer sei. Es besteht ja keine Pflicht, irgendein Buch zu lesen, das eine neue, noch nicht anerkannte Weltsicht präsentiert.

Warum hatte ich wohl keinerlei Schwierigkeit mit dem Lesen?

Meine Antwort fand ich viel später, als ich genug Schwierigkeiten mit dem Wiederlesen schon gehabt hatte. Sie lautet: Weil ich in der Darstellungsweise so viel von Duktus und Intensität der klassischen Musik wiederfand, daß ich mich sogleich darin zuhause fühlen konnte.

Denn darin war ich ja zuhause. Die Musik war seit meinem achten Lebensjahr, also seit 1949, als ich zur Schule kam und mit dem Klavierunterricht bei Katharina Völkerling begann, erklärtermaßen mein Hauptinteresse. Lange Zeit wollte ich Organist werden – mein erster Roman *Tellingstedt* schildert, wie ich mir nichts schöneres vorstellen konnte, als in einer kleinen Dorfkirche die Werke Johann Sebastian Bachs zu studieren und zu präsentieren, *omnia ad majorem Dei gloriam*, wie mein Kompositionslehrer Bernd Alois Zimmermann noch über sein letztes Werk schrieb. Und ich studierte Komposition bei ihm seit Mai 1962, hatte also das Wichtigste bei ihm bereits gelernt, als ich dem Feuer Rosenstock-Huessys begegnete, pur und geschrieben, auf Zeit hin verläßlich das Leben begleitend.

Das Wichtigste war: Die Musik darf niemals die Kategorien räumlicher Ordnung auf die Organisation der Zeit wendendann erstickt sie an Unverständlichkeit; und: Ich steige aus dem Karren der Modernen Musik aus, der das Komponieren mehr oder weniger als Ingenieursarbeit verstand – so jedenfalls kam es mir vor.

Mit dem Lesen ist es nicht getan. Ich drückte das in der Überschrift zu einer Anleitung zur Lektüre Rosenstock-Huessys mit der Umkehrung des Augustinischen: *Nimm und lies!* aus, nämlich mit: *Lies und nimm!* Darin klang an, was in Paul Celans Engführung steht: *Die Steine, weiß, mit den Schatten der Halme: Lies nicht mehr – schau! Schau nicht mehr – geh!* Das heißt: das Gelesene will angewandt werden, ausprobiert, vervielfältigt, bewährt und bewahrheitet.

Die Soziologie hat Eugen Rosenstock-Huessy im Felde im Ersten Weltkrieg im Jahre 1917 als Vision empfangen, die er verglich mit dem monumentalen Werk Dante Alighieris. Die Vision hatte den Namen: *Das Kreuz der Wirklichkeit*.

IM KREUZ DER WIRKLICHKEIT – steht über dem Epilog zu dem gewaltigen Werk auf den Seiten 758f. Dahin hat also die Scham den ursprünglichen Titel gerückt. Der Verleger wollte sich auf das Kreuz der Wirklichkeit als Titel nicht einlassen. Rosenstock-Huessy hat zugestimmt – wie der Autor es denn zuweilen seufzend oder zähneknirschend muß. Es drückt aber aus, daß das Kreuz der Wirklichkeit nicht eher erscheinen durfte, als bis DER STERN DER ERLÖSUNG Franz Rosenzweigs in die deutsche Sprache aufgenommen wäre.

Inzwischen ist eine Ausgabe des *Sterns der Erlösung* in der Bibliothek Suhrkamp erschienen, wohlfeil – da besteht die Chance, daß die deutsche Öffentlichkeit, die Buchöffentlichkeit, bereit ist, das christliche Erbe anzunehmen.

Daß sie damit Mühe hat, liegt an den finsteren Hitler-Jahren – ob man das nun wahrhaben will oder nicht. Der Suhrkamp-Verlag hat vornehmlich die Autoren gefördert, die jüdischer Herkunft waren, dann aber freudig die Möglichkeit der Emanzipation und Integration in die bürgerliche Gesellschaft wahrgenommen haben und eben nicht ausdrücklich religiös motiviert schreiben, wohl aber messianisch motiviert.

Die klassische Musik steckt in einer ganz ähnlichen Klemme. Zwar gelten die Werke Haydns, Mozarts, Beethovens, Schuberts, und Brahms' unangefochten als unvergänglicher Teil der europäischen Kultur, die man in aller Welt aufsaugen muß, wenn man etwas gelten will – aber sie wird durch die Beschleunigung und Mechanisierung so entstellt, daß die verschiedenen Vermögen, etwas zu erleben, dabei nicht in gleicher Weise beschäftigt und gestillt werden: das Vermögen, einer großen Eingebung folgen zu dürfen, das Vermögen, tief zu empfinden und das Herz schlagen zu lassen, das Vermögen, mit anderen das Innigste und Tiefste zu teilen und zu gemeinsamem Erleben zu steigern und schließlich das Vermögen, Erlebtes zusammenzufassen und mit Methoden des Erkennens zu klären.

Bei der Musik sind dies: Melodie, Harmonien, Form und Klang.

Diese vier Betrachtungsweisen, die natürlich in keinem einzigen Musikstück isoliert auftreten, wenn auch vielleicht extrem in den Vordergrund gerückt, bilden aber bereits das *Kreuz der Wirklichkeit*, das Rosenstock-Huessy allem Erfassen der Wirklichkeit zugrundelegt.

Die Melodie der Zeiten zu vernehmen heißt: auch die Zukunft in die Gestalt mit hineinnehmen.

Die Harmonien zu hören und mit dem Herzen der Schattierung nach lichter und dunkler zu folgen heißt: die ganze Innenwelt, die ohne Zeit und immerdar präsent ist, in sich entfalten.

Die Form als Pflicht und Wunder zu erleben, daß etwas nur sinnvoll anfangen kann, wenn es sein Ende von vornherein akzeptiert, heißt: in die Generationenfolge der Überlieferungen eintreten.

Den Klang zu werten und als Tatsache auch außerhalb des eigenen Wollens und Wirkens gelten zu lassen heißt: Erlebtes schätzen und als Vorrat der Welt zur Verfügung stellen können.

Und die klassische Musik hat in Jahrhunderten die Symphonie herausgebildet, damit diese vier Vermögen in vier Sätzen dargestellt werden können, zusammenhängend und artikuliert als Ereignis, das von allen Menschen, die sich dazu versammeln – *Freude, schöner Götterfunken, Tochter aus Elysium, wir betreten freudetrunken, Himmlische, dein Heiligtum* -, freiwillig und ohne Ansehen, aus welchem Staat oder welcher Religion.

Zuerst ist die *Sinfonia* dreiteilig gewesen, denn die christliche Religion, ob nun katholisch oder evangelisch, kennt das Walten des Dreieinigen Gottes als göttliches Walten – und dieses in Musik auf die Erde zu bringen, das heißt: hörbar zu machen, ist ihr Anliegen gewesen. Man sieht das daraus, daß die Sinfonia als Vorspiel zu Oper, Oratorium und Kantate entstanden ist, Sammartini stellte die dreiteilige Form um 1730 vor. Carl Philipp Emanuel Bach, einer der genialen Söhne Johann Sebastian Bachs, arbeitet damit, Erfahrungen aus dem *Concerto grosso*, dem Zusammenwirken von Orchester und Solist, und der Triosonate als der edelsten kammermusikalischen Form, gingen mit ein.

Aber erst Joseph Haydn machte die Viersätzigkeit der Symphonie zur Regel. Noch bei Mozart begegnet man – besonders in den Sonaten – der Dreisätzigkeit, Beethoven entwickelt, indem er die ganze Form in Frage stellt, die Viersätzigkeit, bei Franz Schubert ist sie ganz sicher da.

Und zwar in der Reihenfolge (so steht es denn im Lexikon): Schnell (in der Sonatenform) – langsam – schnell (Menuett oder Scherzo) – schnell.

Nun heißt das aber nichts anderes als das Zusammenbringen der vier Stile:

*dramatisch* (Sonatenform) --

*lyrisch* (langsamer Satz) --

*episch* (in Form einer Erzählung, einer Liebesgeschichte in gesellschaftlich akzeptierter Form, weswegen zuerst als Menuett, dem höfischem Tanz, dann als Scherzo, welches das Scherzen eines Liebespaars darstellt) --

*berichtend*, nämlich was nun – *sub spezie aeternitatis*, von der Ewigkeit, vom Jüngsten Gericht her – bei dem Erlebten herausgekommen ist.

Rosenstock-Huessy hat also endlich die Erfahrung der klassischen Musik ernstgenommen und sich damit gelöst von der Zweisätzigkeit der Philosophen, die mit den Begriffen von Subject und Object, Innen und Außen auszukommen meinen – wenn sie auch meist, wenn sie gut sind, den Gesetzen der Darstellung folgen, die eben die vier Stile benötigt.

Die zwei Bände der Soziologie: *Die Übermacht der Räume* und: *Die Vollzahl der Zeiten* entfalten denn auch das Dilemma, das durch den Wechsel des grundlegenden Mustes von Zwei auf Vier entstanden ist.

Präludium und Fuge ist die von Johann Sebastian Bach vollendete Form der Zwei: und so stehen die beiden Bände auch zueinander. Der erste, der noch am ehesten Wissen vermittelt, wie ein Leser, ohne empfindlich gestört zu werden, erwartet, ist nur die Grundlage, das Darstellen der Darstellungsmöglichkeiten, wie ein Präludium, in dem alle Möglichkeiten einer Tonart vorgeführt werden. Dann kommt die Fuge, die ein bestimmtes Thema durch alle vier Stimmen (eines Chores – jung und alt, Mann und Frau) durchführt.

Und der zweite Band: *Die Vollzahl der Zeiten* nimmt wirklich die Dreisätzigkeit vor Joseph Haydn und bei Mozart wieder auf, indem er drei Teile hat.

So vereint das Werk auf wunderbarste Weise die edelsten Darstellungsweisen der Zwei, der Drei und der Vier: Bach, Mozart, Beethoven und Schubert.

In der Musik ist es ganz üblich und für den Hörer (nicht für den Leser, den es ja bei Musik ziemlich selten gibt) leicht verständlich und nachzuvollziehen, wenn die verschiedenen Aspekte von Melodie, Harmonie, Form und Klang unvermittelt in den Vordergrund rücken können und immer auf überraschende Weise das Ganze präsent halten. Und so kommt denn heraus: Man tut gut daran, die Soziologie Rosenstock-Huessys als Fortsetzung der Musik von Bach, Mozart und Beethoven zu lesen – das heißt, beim Lesen die Tugenden des Hörens anwenden, wo der Fluß der Zeit nicht darin besteht, daß alles geordnet hintereinander kommt, sondern man sich befindetwie Bernd Alois Zimmermann das ausgedrückt hatin der *Kugelgestalt der Zeit,* ich würde nach Rosenstock-Huessy sagen: in der *Vollzahl der Zeiten*, nämlich wo früh erklingt, was spät erklang und umgekehrt.

In den Niederlanden hat es Leute gegeben, die tatsächlich die ganze Soziologie gehört haben, indem sie vorgelesen wurde! Und daraus sind sie gekommen, die Leute, die das Rosenstock-Huessy Huis in Haarlem gelebt haben mit Wim und Lien Leenman, und das Franziskushaus in Castleton (wie ich es bei meinen Besuchen genannt habe), wo Bas und Teuntje Leenman mit Clazina van Wel gelauscht haben, wie sie das Gehörte weitersagen könnten.

Dein Vater

 **ZWEITER BRIEF: NACH MARX, NIETZSCHE, FREUD UND DARWIN**

*Köln, 6. Dezember 2004, Nikolaustag*

Lieber Niklas,

der Nikolaustag ist zwar gerade beendet, aber trotzdem will ich mit dem Brief noch mit der Erinnerung beginnen, wie meine Mutter zum Nikolaustag uns Kindern einen Brief schrieb, der denn in großen Schreibschriftbuchstaben die Unterschrift hatte: Nikolaus.

Denn die Berechtigung zu dieser Unterschrift wird von den vier Geistesgrößen, die das Werk Rosenstock-Huessys als danach datieren, auf verschiedene Weise in Frage gestellt.

Ich habe ernstlich und lächelnd niemals daran gezweifelt, daß uns da eine Nachricht vom Nikolaus zukam, wenn ich es nur in der Schwebe ließ, was ich da hätte anworten sollen auf die Frage, was denn genau.

*Marx* hätte wohl gesagt, der Nikolausbrauch gehöre zu dem Schwindel, der den Leuten den Sinn für die gerechte Ökonomie vernebele. Da kommen sich die Kinder wundersam beschenkt vor und wissen nicht, wie die Eltern die Kekse, Schokoladen und Apfelsinen bezahlt haben. Und schon gar: wieviel die Apfelsinenbauern in Sizilien dafür bekommen haben. Warum verstecken sich die Eltern hinter der Legendenfigur?

*Nietzsche* hätte bemängelt, daß der Nikolaus nicht gleich zu einer inneren Figur wird, die gleich auf die innere Wahrheit losgeht, daß sich der Mensch nur selbst beschenken könne. Die falsche Bescheidenheit, immer nur das wundergläubige Kind zu sein, das sich gelenkt und geleitet fühlt – warum offenbart sich die Mutter nicht, wenn sie zu loben und zu tadeln hat?

*Freud* hätte die Figur des Über-Ich herbeizitiert, um die in Schenkbräuchen und Opferriten wurzelnde Sitte als Trick des Unbewußten zu entlarven. Was wollte denn Deine Mutter damit erreichen, daß sie sich zur Sprecherin des Nikolaus machte? Welche Maske tat sie vor ihre Stimme und ihr Angesicht? Ist nicht das Ausnutzen der Wundergläubigkeit der Kinder jeder Aufklärung entgegen? Und welche Schwierigkeiten mögen sich für das spätere Leben daraus ergeben, daß die gabeselige Mutter sich so versteckt? Entstehen daraus nicht weiter Facetten des Ödipus-Komplexes?

*Darwin* schließlich hätte die Nikolaus-Sitte als Teil der kirchlichen Lehre betrachtet, die der naturwissenschaftlichen Wahrheit entgegenstehe. Gibt es in der Entwicklung des Menschen im Mutterleibe irgendeine Phase, wo der Nikolaus auftaucht? Und wozu dient im Überlebenskampf und in der Herausbildung der Arten der 6. Dezember, oder, wie bei uns zuhause, jeder der vier Adventssonntage?

 Alle vier aber fußen in ihrer Kritik auf der Namenskraft, ohne es zuzugeben. Denn der Nikolaus ist tatsächlich anwesend mit seinem Namen.

Und der Name, Niklas, ist das Merkwürdigste auf der ganzen Welt! Er umfaßt nämlich sowohl Zukunft wie Vergangenheit, er erreicht das innerste Wesen mit seiner Schwingung, berührt die Seele, das sie sich auffordern läßt, und gleichzeitig stellt er die Person als tatsächlich vorhanden fest.

Mit dem Namen also erreichen wir die Schwebe, die überall im Leben hinkann, ohne sich umzulenken, wie es bei dem Propheten Hesekiel heißt. Der Name ist die gewaltigste soziale Tatsache!

Diese wird beim Nikolausbrauch berührt. Meine Mutter nutzte sie, um ihre Teilnahme an unsrem Leben auszudrücken, was sie sonst nicht so leicht tat. Dabei zweifelte sie gewiß keinen Augenblick daran, daß sie es war, die ihre Schriftzüge – mit etwas Büchereihandschrift dabei – dem Nikolaus lieh.

Die vier Dysangelisten Marx, Nietzsche, Freud und Darwin sind Dir aber wohlbekannt. Sie stellen den ungefähr modernsten Lehrstoff bereit, der auf die Schulen gelangt ist. Marx mit den ökonomischen Fragen, Nietzsche als Philosoph, den man doch – auch jenseits seiner Liebe zu Goethe und Heine – ganz ernstzunehmen habe, mitsamt seiner Syphilis, Freud als der Tabubrecher ersten Ranges, der das Unbewußte einer Einwirkung öffnete, Darwin als Autor der Evolutionstheorie, die endlich eine Vorstellung davon gibt, wie die Entstehung der Arten nun wirklich gewesen sein kann.

Darüber haben wir ja schon öfter gesprochen.

Und Rosenstock-Huessy setzt diese alle als unwiderruflich geschehen voraus!

Dabei ist aber das Wichtige: er sieht sie alle zusammen als eine Gesamterscheinung, als Totengräber einer Epoche. Sie wirken im Sinne des Kreuzes der Wirklichkeit in die vier verschiedenen Richtungen:

INNEN (NIETZSCHE)

Die Stimme der inneren Wahrheit, des Selbstbewußtseins.

AUSSEN (DARWIN)

Die Stimme der äußeren Wahrheit, der Bewährung.

RÜCKWÄRTS (FREUD)

Die Stimme der Schicksalsstunde, die das Gebilde bereits ins Leben rief, bevor wir selber leben.

VORWÄRTS (MARX)

Die Stimme der Verantwortung, die es künftig nicht missen kann.

Da heißt es denn auf Seite 59 des ersten Bandes:

"Wieder werden wir darauf geführt, daß soziologische Erkenntnis nur hörbar und vernehmlich, nicht sichtbar und greifbar gemacht werden kann. Stimmen müssen laut werden, miteinander streiten und ringen; sie bestimmen am Ende unsere Erkenntnis. Die Stimme unserer eigenen Verantwortung ist immer selber eine, wenn auch nur eine Stimme in diesem Zusammenklang. Alle Soziologie ist also nur als mehrstimmige Erkenntnis möglich. Umgekehrt erhellt hieraus auch die Eigenart soziologischer Inhalte. Ein vollständiger soziologischer Tatbestand muß seine Vollständigkeit darin zeigen, daß er mehrer Räume und mehrere Zeiten erfüllt, daß er in einem Innenraum und einer Außenwelt lebendig wirkt, in einer Vergangenheit wurzelt und in eine Zukunft hineinragt. Das Koordinatenkreuz der Wirklichkeit zerschneidet ein mehrräumlich-mehrzeitliches Geschehen. Wirklich ist nur, was in mehr als einem Raum und in mehr als einer Zeit bestimmt wird. Nur diese Wirklichkeit ist das Thema der Soziologie.

Die Mischung aus Anerkennen und Kritisieren, Begeisterung in menschlicher Nähe und Freiheit des Loslassens ist fördernd, weil sie verhindert, daß wir, was sie doch gerne wollten, jeden der vier Genannten, Marx, Nietzsche, Freud und Darwin so total folgen, daß wir über ihr gemeinsames Auftauchen gar keinen Gedanken mehr verschwenden.

Und damit komme ich zu dem, was viele so schwer sehen können: Eugen Rosenstock-Huessy hat selbst dafür gesorgt, daß niemand ihn als Führer nehmen kann, indem er bei ihm lernt, wie man's macht. Er hat sich verstanden als einer unter mehreren, die ausgezogen sind, um nach dem Weltkrieg die Reinheit nachforschenden Denkens wieder zu ermöglichen. Und diese ist nur dann möglich, wenn man sich der Totalität der Erscheinungen stellt.

Mit ihm sind ausgezogen – und ich habe sie in einer Rede in Berlin einmal so hingestellt -: als Stimme

INNEN Hans Ehrenberg,

RÜCKWÄRTS Franz Rosenzweig,

AUSSEN Rudolf Ehrenberg.

Er selber hat die Stimme nach vorwärts dargestellt. Es ist aber seine Ehre, daß er diese drei als Minimum der Begleitung hatte.

So erklingen viele, viele Stimmen in seiner Soziologie, wirkliche Stimmen, die unentbehrlich geworden sind.

Dein Vater

**DRITTER BRIEF: DIE SCHRIFT ALS FLOSS FÜR DAS GANZE DRITTE JAHRTAUSEND**

*Köln, 8. Dezember 2004*

Lieber Niklas,

in jedem anderen Land, in jeder anderen Sprache ist das Urteil: Es ist nur ein Buch! möglich oder naheliegend, nur in der deutschen Sprache, wie sie zwischen 1517 und dem Weltkrieg gelebt und gewaltet hat, nicht – da nämlich hat die Bibel, das Buch der Bücher, das *Alte Gold* jeder neuen Generation gespendet – so nannte mein Großvater Friedrich Schröder eine Sammlung mit Studien zum Alten Testament. Es ist daher nicht gleichgültig, daß Eugen Rosenstock-Huessy das Werk, das er seit 1915-1917 auf der Seele trug, nach vierzig Jahren in deutscher Sprache veröffentlicht hat.

In dem Buch, das die Botschaft aus der Weltkriegserfahrung rechtzeitig zusammentrug, als Rosenstock-Huessy den Eindruck haben mußte: jetzt oder nie, den *Europäischen Revolutionen* von Pfingsten 1931 und dem amerikanischen Buch *Out of Revolution* 1938 – also vor Hitler und vor Ausbruch des Zweiten Weltkriegs -steht folgende Passage über Friedrich Schlegel:

„Der inspirierende Genius der Romantik des neunzehnten Jahrhunderts, Friedrich Schlegel (1772-1829), versuchte den kollektiven Charakter der österreichischen Zivilisation auszudrücken, indem er unter dem Titel *Concordia* in Wien eine Zeitschrift gründete: der Name wies auf die Zeit ungebrochener Harmonie zwischen geistlicher und weltlicher Macht. Die Konversion des hannoverschen Schlegel zum österreichischen Loyalisten mag erklären, was Österreichs Errungenschaft für die Menschheit war. Sein ganzes Leben lang stand Friedrich Schlegel für Totalität. Er wußte, daß das Denken den Anspruch auf Vollständigkeit erhebt, daß ein Mensch im Denken als Repräsentant seiner Gattung handeln soll, nicht für individuelle, subjektive Interessen. Die wahre Vernunft muß daher, kraft eigener Natur, universal sein. Demgemäß Parteirichtlinien auszudenken mag gute Politik, Frömmigkeit, Verfassungstreue oder Ritterlichkeit sein, es kann aber kein Denken in reinster Form sein, was ja bedeutet, daß wer denkt, sich für das bloße Dasein der Wahrheit unter den anderen Mächten dieser Welt verantwortlich hält, denen daran liegt, das Denken gedankenlosen Zwecken anzupassen.

In so verantwortlicher Haltung bewahrte Schlegel in einer Zeit parteilicher und nationaler Tendenzen die Universalität des Gelehrtentums. Er begründete die europäische Schule, die für die wissenschaftlichen Erfolge des neunzehnten Jahrhunderts am ehesten Glaubwürdigkeit verdient. Er zog zwischen Natur- und Gesellschaftswissenschaften eine klare Linie. Er sah diesen Versuch hier, vom fortwährenden Schöpfungsprozeß im Menschengeschlecht zu handeln, voraus. Klar unterschied er zwischen der Beschreibung immerwährender Eigenschaften des Menschen und einer Wissenschaft, die die qualitativen Variationen des Menschen im Laufe der Geschichte als Art und Unterart deutete. So definierte er die menschliche Seele und den menschlichen Charakter als unersetzlichen Gegenstand wissenschaftlicher Historie. Schlegel wußte, daß der Erwerb neuer Qualitäten den Menschen selber verändern konnte, daß Sprünge und Mutationen passiert sind. Als Denker mußte er allumfassend sein; zwar politischer Kämpfer, der parteilich denken muß, war Schlegel, ohne die relativen Ansprüche der Französischen Revolution zu leugnen, mit der Darstellung aller Züge der zivilisierten Menschen befaßt.

Von Geburt war er Protestant aus Norddeutschland, unter dem Einfluß der revolutionären Emanzipation der Juden heiratete er in Berlin eine geschiedene Jüdin. Als Napoleon dem Heiligen Römischen Reich den vernichtenden Schlag versetzte, versuchte Schlegel in einer in Paris veröffentlichten Zeitschrift, eine neue Solidarität aufzubauen, und nannte seine Zeitschrift für Kritiken *Europa*. Er ließ sich in Wien nieder, wurde frommer Katholik, machte sich durch seine Vorlesungen über Weltliteratur berühmt und veröffentlichte in Wien seine *Concordia*. Diese Zeitschrift sollte etwas sein wie die mittelalterliche *Concordantia discordantium*, um den Frieden zwischen Konfessionen, Parteien und Nationen wiederherzustellen. Und Schlegel erfuhr in den Phasen des eigenen Lebens die gegenseitige Durchdringung verschiedener Formen und Stationen der Zivilisation. Er war Europäer kraft langer und schmerzlicher Übung. Daß er sich in Wien niederließ, war die freie Wahl eines gewissenhaften, verantwortlichen Universalisten, der im geteilten Europa nirgendwo anders Asyl für seine umfassenden Aspirationen finden konnte.”

Beim Lesen, Wiederlesen und Übersetzen fiel mir sofort auf, daß Eugen Rosenstock-Huessy hier auch von seiner Position spricht, daß es leicht ist, für die *Concordia* die Zeitschrift *Die Kreatur* zu setzen, für Österreich und Wien die Zuflucht in Norwich, Vermont. Denn Amerika hat auch die Aufgabe wie das Österreich-Ungarn vor dem Zusammenbruch 1918, verschiedene Nationen zu beherbergen und die Universalität in sich zu halten und zu tragen. Und das Bild von Nausikaa, das in dem Österreich-Kapitel später erwähnt wird als Ausdruck der Töchterlichkeit Österreichs, taucht auch in dem Motto zu dem ganzen Werk *Out of Revolution* auf:

TO

THE FAMILY OF

HENRY COPLEY and ROSALIND HUIDEKOPER GREENE

In their homes at

Cambridge, Mass.\
Dublin, N.H.\
Durham, N.C.\
Mount Carmel, Conn.

*In meines Vaters Garten soll die Erde*

*Dich umgetriebnen vielgeplagten Mann*

*Zum freundlichsten empfangen …*

Goethe, Nausikaa-Fragment

In Castleton begegnet Dir wohl auch etwas von dieser Gastlichkeit!

Aber dennoch: das Hauptwerk Rosenstock-Huessys ist in deutscher Sprache erschienen. Das feit gegen die Sprachzerstörung, die der deutschen Sprache in den Hitler-Jahren angetan wurde. Sie bestand in der Aberkennung der Namenswürde und damit der Kraft, die Wirklichkeit, in der wir Mensch werden, zu benennen, ohne in Lüge und Interessenschwindel zu verfallen.

Und es behauptet die Würde, daß es möglich und notwendig ist, den Ertrag eines vollen Menschenlebens in Schrift zu fassen, wenn es für so viel längere Zeit Bedeutung hat, als die Lebenszeit des Schreibers spannt.

Die Soziologie ist nämlich die Urkunde einer neuen Erfahrung mit Sprache, Schrift und Lehre. Sie ist mutig genug, dafür zu halten, daß sie für das Dritte Jahrtausend nach Christus gebraucht wird.

Ich erinnere mich, daß ich zuerst fand, das Dritte Jahrtausend sei doch noch reichlich weit weg. Ich war einundzwanzig Jahre alt, bis zum Anbruch des Dritten Jahrtausends lagen noch 37 Jahre vor mir – eine weite Strecke, herrjeh, da wäre ich dann ja schon 58 Jahre alt! Aber je näher das Jahr 2000 heranrückte, desto mehr bestätigte es sich, daß die Lehre und Lektüre der Werke Rosenstock-Huessys lange, lange Zeit brauchen würde, ehe sie in ihr Eigen käme. Damit meine ich: daß ein Leser ganz selbstverständlich fände, was da verkündet wird. Schon jetzt ist die Differenz zwischen dem, was da steht, und der eigenen Erfahrung nicht mehr so groß. Ich bin also dankbar, daß dieses Werk mir den Zusammenhalt des eigenen Lebens verdeutlicht – und auch die Zuversicht schenkt: das wird auch für Kinder und Enkel wichtig sein.

Für Euch Kinder mag es mir allenfalls gelungen sein, Euch eine Ahnung davon zu geben, wie wichtig das Werk Rosenstock-Huessys für mich ist. Das ist dann Vatersaura, zu der man ja bekanntlich kritisch steht, Mühe haben mag, sie für sich selber zu prüfen. Aber Du wirst es ebenso mühelos finden, daß das von Rosenstock-Huessy Gefundene gilt, auch obwohl ich es wichtig finde.

Zum Beispiel folgende Passage über den Ursprung der Schrift:

„Der Ursprung der Schrift ist etwas so wichtiges, wie der Ursprung der Sprache. Und so muß der Leser auch hier vor die Wahl gestellt werden; hat die Froschperspektive der *generatio äquivoca* recht, die das Schreiben aus dem Alltag der Kaufverträge, Grenzsteine, Spielmarken, an die Tempelwände geschleudert sein läßt? Erklärt die Physik die Biologie? Erklärt die Chemie die Politik? Erklärt der Alltag die Tempelewigkeit? Hat die Schule recht, die das alte ägyptische Kaiserreich aus den heutigen heruntergekommenen Negersitten erklärt? Oder sind wir selber in unseren höchsten Leistungen, in unserer edelsten Begeisterung die Erben des Imhotep, des Erbauers der Stufenpyramide und die Kinder der Göttin Seschat, der Spannerin des ersten Seiles, das den Menschen zum Baumeister einer geordneten Welt machte? Der Leser muß sich entscheiden. Ich aber muß, wie bei der Sprache, meinen Weg weitergehen ohne zu vergessen, daß ich mich damit ent-schieden habe.

Die Hieroglyphen sind die neuen Namen für die Erbauung von Himmel und Erde. Deshalb gelten sie für die neue Ewigkeit des Äons, des im Himmel entdeckten Weltgesetzes der Sternenläufe. Sie drücken die Lebenstaten aus, durch die der Herrscher Sterne und die Sterne Herrscher werden. Die Hieroglyphen sind also die Enzyklopädie der Ewigkeit. Jede Hieroglyphe sanktioniert und besiegelt einen Verlauf. Sterne sind gemalt oder eingemeißelt über jedem Götterbild, denn unterm Sternenhimmel sind wir mit den Göttern. Als Zoser's Stufenpyramide ausgegraben wurde, fanden sich diese Sterne, aber der erste Kritiker wollte es nicht glauben oder zugeben, daß schon damals Sterne an der Innendecke standen. Sie taten es aber! Seit Zoser also erblickte jeder Tote den Himmel auf der Innenseite seines Sargdeckels. Unsere Vorstellung vom „in den Himmel kommen” stammt bis heute aus diesen Hieroglyphen. Hier wurde das Firmament abgebildet, mit Sopdit-Isis als dem Neujahrstagsstern in dem wichtigsten Felde, mit 36 Konstellationen, jede von ihnen zehn Tage bezeichnend; 18 standen vor, 18 hinter Sopdit. Die vier anderen Neujahrssterne, die zusammen mit Sopdit die fünf Tage füllen, an denen das Jahr neu geschaffen wurde und durch die es 365 Tage zählt, waren rechts und links von Sopdit aufgemalt. Unser Bett- und Thronhimmel sind die letzten Nachklänge jener Macht der Hieroglyphen. Der Herrscher mußte unter einem Thronhimmel amten, um Herrscher zu sein! Denn nur der Thronhimmel wies ihm seinen vorschriftsmäßigen Platz im Weltganzen an. Die Hieroglyphen waren mithin Vorschriften für Sternbahnen auf Erden. Wir bauen Eisenbahnen, Straßenbahnen, Autobahnen. Die Ägypter schrieben Sternbahnen vor. Die Hieroglyphen waren also nicht Schriftzeichen, sondern Vorschriftzeichen. Die Silbe „Hiero”, das Sakrale im Namen Hieroglyphe, erinnnert mit Fug an die Zwangsgewalt dieser Vorschriften über die so magisch ins Himmelreich hineingerissenen Sterblichen! Die Hieroglyphen erzwangen ein Verhalten. Deshalb standen sie auf den Toren und Gängen, durch die Pharao und seine Priester kosmisch wandeln mußten.

Die Schrift trat also an die Stelle der Tätowierung. Die Tätowierung machte zum lebenslänglichen Mitglied.” (*Die Vollzahl der Zeiten*, S. 420f.)

Eugen Rosenstock-Huessy entwirft mit seiner Soziologie die *Innendecke* der Erfahrungen des Menschengeschlechts im Dritten Jahrtausend nach Christus! Es lohnt sich, diesen Hieroglyphen zuzuschauen.

Aber es sind keine Hieroglyphen, sondern das aus Fleisch und Blut gewonnene und bezeugte Wort. Deshalb hier noch etwas zu dem Stil des Buches.

Im Vorwort (*Die Übermacht der Räume*, S. 12) heißt es:

„Das wichtigste an diesen Freundschaftsdiensten muß hier ausdrücklich ausgesprochen werden, weil es den Stil dieses Buches erklärt und weil der Leser in der verwissenschaftlichten Welt von heut sich sonst nicht orientieren kann. Die allenthalben zugrunde liegenden Freundschaften sind nämlich nicht in die Theorie des Büchermachens eingegangen. Sondern in ihr heißt es, es gebe nur zwei Sprechweisen für geistige Produktion: nämlich einerseits das System mit seinen Prinzipien, andererseits den Essay à la Emerson oder Nietzsche. Abart des Systems sei die Monographie, die sozusagen einen Paragraphen im System korrigieren soll; eine Abart des Essays sei der einzelne Aphorismus.

Soziologie aber ist einer dritten Schreibart ergeben. Meine Schriften sind ihr ergeben. Sie als unsystematisch zu brandmarken, ist gemeine Praxis. Ich erhebe dagegen Einspruch. Wer sich in guter Gesellschaft bewegt, der soll weder in bloßen Sentenzen noch in seinem eigenen System stecken bleiben. Der Essay ist vorwissenschaftlich. Er sprüht vor Leben; er ist geistvoll. Er verharrt diesseits der Systematik. Umgekehrt die Handbücher vezichten auf das Sprühen um der Ordnung willen. Aber ich, der ich aus Unordnung Ordnung erstehen lassen soll, schreibe weder in dem flüchtigen Augenblick des witzigen Einfalls noch in der langen Zeit der Wissenschaften. Denn ich kehre jedesmal, wenn mir etwas eingefallen ist oder wenn ich mich zum Fachmann meines Systems aufgeschwungen habe, als Freund unter Freunden zurück. Für die Natur- und Geisteswissenschaftler wird diese Tatsache verkleistert, weil man dort den Kollegen mit dem Freund verwechselt. Der Soziologe hat keine Kollegen. Hingegen muß er Freunde haben. Meine Freunde, genau wie meine Frau, dienen mir nämlich nicht wegen sondern trotz meiner Systeme. Nie können sie von meinem System eingewältigt werden. Sie leben ja mit mir.

O wie war glücklich ich,

da ich noch mit Euch

sahe sich röten den Tag,

sinken die Nacht.

Der nach-aphoristische und der nach-systematische Stil ist eine dritte Stilart; ob ihn die Herren von der Stilkritik gelten lassen wollen, weiß ich nicht. Aber die vorliegenden Bände huldigen ihm. Der dritte Stil setzt das strengste, ja pedantischste System voraus. Mein Denken führt mein System zunächst durch. Aber eben nur zunächst. Dann macht es entschlossen vor der Vergötzung des eigenen Gedankenkindes kehrt.

Wenn Goethe seine Werke Bruchstücke einer Konfession genannt hat, so hat er darin seinen höchsten Glauben formuliert. Das höchste Kunstwerk muß unvollkommen bleiben, wenn es sonst die Maske auf dem Antlitz seines Schöpfers versteinert.

„Du umfängst wie Morgenröte

dieser Gipfel dunkle Wand,

und noch einmal fühlet …

Frühlingshauch und Sommerbrand.”

Wie muß denn dieser Vers ergänzt werden? „Denn noch einmal fühlet Hatem”, steht im Druck; aber im Reim steht klärlich „fühlet Goethe”.

Hinter seinen Gedichten muß der Dichter lebendiger Mensch bleiben. Eine Ästhetin sagte mir, Verlaine habe ein Recht gehabt, seinen Schwiegervater zu ermorden, da doch so ein schönes Gedicht daraus entstanden sei. Das ist eine typische Äußerung aus dem Weltalter von Kunst und Wissenschaft als Götzen. Weder Goethe noch die Soziologie können mit solchem Götzendienst etwas anfangen, geschweige denn enden. Dem existentiellen Bekenntnis muß die Perfektion der Kunstform wieder und wieder geopfert werden. Deshalb also ist bei Goethe das Kunstwerk Bruchstück. Denn es bildet nicht die oberste Instanz (darin gipfelt das so oft bezweifelte Christentum Goethes).

Analog würde der Soziologe zum Narren, der alle seine täglich neuen Freundschaften oder Feindschaften unter sein doch nun einmal zu einer bestimmten Stunde vorgefaßtes System einreihte. Soll ich so lächerlich sein, alle neuen Erfahrungen wie der arme Schopenhauer seinem im dreißigsten Jahre konzipierten Systeme unterzuordnen?

Das sei ferne. Ich habe noch Ohren zu hören und Augen zu sehen. Mir ist meine eigenes System um so verheißungsvoller, je mehr es mich ermächtigt, unerwarteter Einsichten zu harren. Also, bitte, lieber Kritiker, schreibe nicht, dies Buch sei essayistisch oder systematisch und etwas drittes könne es logisch nicht geben.”

Das also ist das Gebot des Sternenhimmels der Gesellschaft des Dritten Jahrtausends, daß das Vorübergehen wichtiger wird, als das System!

Dein Vater

**VIERTER BRIEF: INSPIRATION DURCH DIE LIEBE (GOETHE)**

*Köln, 9. Dezember 2004*

Lieber Niklas,

Rosenstock-Huessy ist in viel höherem Maß Lehrer gewesen, als vermutet wird. Als Lehrer hat er die dramatische Veränderung erlebt, daß die Frauen auf die Hochschulen kommen.

Findet man von dem Stilwechsel, der dadurch bedingt ist, etwas in seinen Büchern, die doch erst einmal so aussehen, als gehörten sie zu den typischen Männerbüchern, dick und viel Lesearbeit erfordernd – während heute die Lehrbücher anschaulisch sein wollen, das Schriftbild auflockern, die Beteiligung der Lernenden fast so weit einzuladen, daß die Selbständigkeit des Satzes aufgegeben wird.

Die wichtigste Hörerin seines Werkes ist Margrit Rosenstock-Huessy gewesen, solange sie lebte. Sie starb in dem Jahr, das auf die Veröffentlichen der Soziologie folgte. Sie hat miterlebt, daß das seit vierzig Jahren erschaute Werk ihres Mannes erschien – er betrachtete sie als einen Teil davon.

Dabei hat sie ihm gar nicht die spezielle Anerkennung zuteil werden lassen, die jedem gearbeiteten Buch auch zukommen darf. Sie hat an dem Rang des Erschauten nicht gezweifelt, fand aber immer etwas auszusetzen --- wohl besonders in der Richtung, es müsse verständlicher sein. Diese Mischung aus distanzierter Betrachtung des einzelnen Geschriebenen und Anerkennung des Ranges und der gesamten Lebensschau hat Eugen Rosenstock-Huessy herausgefordert – aber auch davon abgehalten, sein Werk nicht schon eher zu schreiben. Leicht ließ er seine Hauptsachen, wenn andere Menschen seine Hilfe und Anteilnahme brauchten. Und dazu gehörte auch Margrit.

Das Geheimnis aber dieser Ehe ist nicht zu erschließen, wenn dabei – was durchaus möglich ist – die Begegnung mit Franz Rosenzweig nicht auch in Betracht gezogen wird.

*Sprich vom Geheimnis nur geheimnisvoll!* Diese Warnung hat Eugen Rosenstock-Huessy beherzigt. Ich bringe Dir deswegen eine Partie vom Schluß der Soziologie (*Die Vollzahl der Zeiten,* S. 753f.)

„Die Antwort, die Antiope dem Nihilisten, das Weib dem bloßen Manne gibt, damit er lebe, ist die Binität! Antiope sehe ihren Geliebten als Tier und als Gott. Denn sie hörte ihn und seine Stimme, und sie erhört ihn und sein Verlangen. So wird er innerlicher und äußerlicher. Der Nihilist hört auf, wo ihm Gehör und Erhörung beide zuteil werden. Denn da verschmilzt der Janus-Charakter seines Geistes und seines Leibes; wenn beiden ihr Recht wird, hören beide auf! An ihre Stelle tritt das heile Geschöpf.

Aber freilich, Antiope ist noch nicht ins Leben getreten. Noch ist die Vermännerung der Frauen unter der Last der industriellen Arbeitsteilung die schrecklichste Folge unsrer sogenannten Naturbeherrschung. Die Mädchen und Frauen, die aus den Haushalten herausgesaugt werden, frieren in der Weite der zählbaren Welt.

Die Begeisterung ist ihnen fremd geworden; denn so wie sie ihn gewohnt waren, im Gebet des Vaters und in den stürmischen Ideen ihrer Brüder, als Kirche oder als Staat, treffen sie ihn in den Untergrundbahnen und Autobussen nicht an. Dort werden sie angestarrt und nicht besungen. Dort werden sie angesprochen, aber nichts wird ihnen anvertraut. Ein Vater aber vertraute schweigend seiner Tochter die Umwandlung seines tiefsten Wesens. Und über den Vater der Frau ging die geistige Erbfolge vom Großvater zum Enkel am sichersten, weil am unauffälligsten.

Heiratet heut das gejagte Wild, die Sozialarbeiterin, die Schreibmaschinistin, so nimmt sie den Mann und trennt ihn von seinen Wahngenossen. Auf der Hochzeit sieht er seine geistigen Genossen oft zum letztenmal. Diese neuen Töchter der Welt sind eifersüchtig in einer neuen Weise. Keine leibliche Untreue ist ihnen mehr halb so bedrohlich wie eine geistige Beziehung ihres Ehemannes. Denn da sie den Geist nur als männlichen Geist der Beutemacher kennengelernt oder als Schulgeist für Examina, so können sie sich von keinem solchen vorehelichen Geisterbund ihres Mannes etwas heilsames versprechen. Sie haben weder die Absicht noch den Wunsch noch die Erwartung, daß diese Geister ihrer Ehe hilfreich werden könnten. Sie leugnen die Begeisterung. Das ist heute die Gottesleugnung.

Die geistige Eifersucht ist also an die Stelle der leiblichen getreten. Statt antiopisch ist diese Frauenberechtigte nur antithetisch. Die Dialektik der Gesellschaftsunordnung ist eben das Gegenteil des Dialogs in einer Gesellschaftsordnung.

Gerade die wenigen Fälle, wo diese geistige Eifersucht überwunden wird, weil die Geister zum Heiligen Geiste hinweisen, braucht der Erfahrene nur zu analysieren, um zu sehen, wie sie zu Ausnahmen geworden sind.”

Da spricht Rosenstock-Huessy, meine ich, von seiner mit Margrit und Franz Rosenzweig gemachten Erfahrung.

Ich setze hier noch ein Gedicht hin, das vielleicht besser den Schwebezustand ausspricht, aus dem dieses Wunderwerk einer vierzig Jahre Lebensarbeit zusammenfassenden Kündung heraus geboren ist.

Er schrieb für Margrit in das Buch: *Lyrik des Abendlands*, herausgegeben von Georg Britting 1948:

*Dein ganzes Leben ist der Lyrik Feier.*

*Denn immer fand der Herr Dein Herz als Leier*

*Bereit ihm froh zu schlagen, ihm zu singen.*

*Nur des Mosaiks Steinchen kann ich bringen*

*Du liebes Ganzes, lass sie Dir erklingen!*

*New York, 24. Dez. 1948*

 Und wunderbar erklingt das Geheimnis in einem Gedicht für Freya:

*War alles wie im Tanzschritt klar entschieden?*

*War alles bunter, doch eindeutiger Zug?*

*Von Sommers Anfang bis zu Julis Iden*

*Ein unbedingtes Fährnis und ein Flug?*

*Wer fragt mir dies Geheimnis, dies Ereignis,*

*Der weissen Wolke heilgen Festzug ab?*

*Wer flüstert Fragen hier, statt mit Verschweignis*

*Das Widerfahr zu hüten wie das Grab?*

*Bist Du es selber, leicht erkrankte Zunge,*

*geschwächte Kehle trocken zum Gesang,*

*Gealtert wo noch eben Du der Junge,*

*Und wo Du hochgesinnt nun eng und bang?*

*„Gib her die Liebe, wie Du sie empfangen*

*Die wahren Götter sind als Freie nur,*

*Wie sie an Dich im Siegeszug gelangen,*

*Drängt sie voran die göttliche Natur.*

*Sie gaben Deinem Leben neuen Reigen,*

*Sie fügten ihn in Eh und Ordnung ein.*

*Sie fliegen weiter. Deinen Dank zu zeigen,*

*Ist nun Dein eignes Tun. Du bist allein.*

*Nein, nicht allein! So mögen Götter wehen,*

*anwehen ihren Geist, mich dann entleeren.*

*Vor Dir, mein Gott, lass mich nicht einsam stehen,*

*Der Zeitgefährtin lass mich nicht entbehren.*

*Dann mag der Gott des Flugs, des Fests, der Wende*

*Frei uns verlassen, wie er frei uns fügt”.*

*Frei heisst nicht: einzeln, Freya. Deine Hände*

*Und meine lenkt Ein Sinn, und das genügt.*

Dein Vater

**FÜNFTER BRIEF: DAS ENTFALTEN**

*Köln, 14. Dezember 2004-12-14*

Lieber Niklas,

in meinem Computer ist immer noch ein Virus, die Reparatur von Sebastian ist noch nicht erfolgreich gewesen. Das paßt so recht zum Merkur rückläufig im Schützen!

Ich lasse mich nicht abhalten, den nächsten Brief an Dich zu schreiben.

Ich möchte etwas sagen dazu, wie Eugen Rosenstock-Huessy spricht. Dazu nehme ich ein paar Absätze am Schluß des Ersten Teils des ersten Bandes: *Der Eintritt in das Kreuz der Wirklichkeit: Vergegenwärtigung der Soziologie.* Die fünf Schritte eines wirklichen Vorgangs sind durchgeführt:

*1\. Sprachnot: Das Versagen der Namen*

Es gibt einen Vorfall, der etwas Neues hervorruft.

*2\. Das Selbstbewußtsein des Soziologen (Reflexivum)*

Die Darstellung beginnt mit dem Innen des Kreuzes der Wirklichkeit.

*3\. Die Kämpfe um die Soziologie (Aktivum)*

Von da geht sie nach außen, wo die Widerstände ernst sind.

*4\. Der erste Soziologie (Präjektivum)*

Der Ursprung eines wirklichen Vorgangs ist und bleibt der leuchtende Ausgangspunkt, der als Verheißung wirkt, solange es das Neue gibt, das damit angefangen hat.

*5\. Die Stunde der Soziologie (Trajektivum)*

Welche Geschichte gibt es schon, in die der Leser, ob er will oder nicht, eintritt?

Ich gehe nun also in den fünften Abschnitt auf Seite 54. Ich schreibe jeweils einen Absatz ab und sage dann dazu etwas, als wäre es ein Notentext.

 „Die Methode dieser Wissenschaft kann keine theologisch-scholastische, keine rechtswissenschaftlich-philosophische sein: Saint-Simon hat ihre Menschlichkeit schon enthüllt, als er sein eigenes Leben als großen Versuch dahingab. Ihr Beweisverfahren wird also nicht in dicken Büchern und in Zahlenreichen seine Triumphe feiern können, sondern nur in gelebter Lückenausfüllung: Der Soziologe ist der lückenausfüllende Mensch. Erst sein Mitleben und seine Mitwirkung bringen – wieder sei auf Saint-Simon verwiesen – ein soziologisches Problem zur Reife. Beteiligung und Mitleidenschaft des Soziologen, sein leidender Eintritt in die Pathologie des Falles als Teil des Falles, ist der entscheidende Schritt zur Vergegenwärtigung dessen, was fehlt. Erst hinter diesem Mitleben her eröffnen sich auch Erkenntnisse. Leiden werden vom Soziologen gefordert. Leiden sind nichts Theoretisches. Und so ist die Soziologie „theoretisch” nicht als notwendig zu erweisen. Nur wer unter der Krise Europas, der Zerrüttung der Erde, der Auflösung des Volkstums mitleidet, nur für den hat Soziologie eine Zukunft. Nur er wird es verstehen, weshalb Soziologie nicht in Bibliotheken noch in Laboratorien, sondern nur in Gemeinschaften von Menschen bestehen kann. Denn niemand kann sich aus dem menschlichen Geschehen herausgenommen wähnen in abstrakte Begriffswelten und reine Zahlenbereiche. Es gibt keine soziologischen „Definitionen” und Theorien. Denn der Soziologe, der lacht, denkt, spricht oder schreibt, steht dem sozialen Vorgang, den er bedenkt, bespricht oder beschreibt, nie von außen gegenüber, sondern in seinen Gedanken, Besprechungen und Beschreibungen setzen sich die sozialen Vorgänge selber fort, äußern sie ihre Macht über ihn und vollenden sich mit seiner Hilfe oder gegen seinen Widerstand. Simmels unübertreffliche Darstellung europäischer Geselligkeit z.B. ist zugleich ihre letzte feinste Blüte unmittelbar vor ihrem Vergehen; sie ist so selbst ein Letztes im Leben dieser Formen. So gibt es hier kein festes Objekt wie bei jeder „objektiven” Wissenschaft und kein Subjekt wie bei jeder Theorie. Der Soziologe entdeckt den menschlichen Geist als Bestandteil der menschlichen Welt, Teil unter Teilen, Geschöpf unter Geschöpfen, gleich vorübergehend und dennoch gleich wesentlich. Es gibt keine Sicherheiten, keine ein für allemal gültigen Lehrsätze, wenn nicht lebendige, forschende und lehrende Menschen sich mit ihrer ganzen Person auch unter Lebensgefahr für sie einsetzen und verbürgen. Die Soziologie ist mithin keine Geisteswissenschaft im Sinne alter Universitätsüberlieferungen und erst recht keine Naturwissenschaft im modernen Sinne. Dennoch ist Soziologie echte Wissenschaft, genau wie das, was seit achthundert Jahren Wissenschaft heißt. Denn ihr Verlangen geht auf Vergegenwärtigung.”

Von dem Leser wird ganz sanft etwas Gewaltiges gefordert: Leidensfähigkeit, die nicht in Bücher zu fangen und zu fassen ist, auch auf keiner Schreibtafel Platz hat außer auf der wirklichen Zeit. Und die wirkliche Zeit entsteht nur durch Gruß und Gegengruß unter den Menschen, die sich die Zeit sagen. Niemand kann sich aus dieser Zeit fortstehlen, wenn er gerade aus ihr Erkenntnisse gewinnen will. Etwa so, wie kein Musiker etwas über das Musizieren lernen kann, es sei denn er macht sie, die Musik, mit Anfang, zeitlicher Mitte, innerer Mitte und Ende. Als Hörer kann man in diesen Prozeß eintreten. Aber nicht als Betrachter der Noten, es sei denn, der Betrachtende ist wiederum so erfahrungsgesättigt, daß die Praxis sofort mit anklingt. Für das Lernen gilt das gleiche: man kann nichts lernen, als was man nicht zuvor als Unbekanntes erlebt – und das heißt auch: erlitten hat.

Der nächste Absatz:

„Dies aber ist das Bemühen aller großen Wissenschaft immer und zu allen Zeiten. Alle Naturwissenschaft vergegenwärtigt uns die Natur und ihre Kräfte. Soweit ihr das gelingt, können wir dann über diese Natur herrschen und ihre Kräfte gebrauchen. Genau so wie alle Schulweisheit und Philosophie die Weisheit der Alten vergegenwärtigen, damit das lebende Geschlecht über sie verfügen könne. Soziologie will freilich nicht Weisheit und Geistesschätze etwa des klassischen Altertums oder des Mittelalters vergegenwärtigen. Das tun längst andere Wissenschaften. Sie will auch nicht Natur vergegenwärtigen, weil auch das schon glänzend geleistet wird. Sie will den wirklichen Menschen und die menschliche Wirklichkeit vergegenwärtigen, die ihr aus den Namen und Worten der Sprache aufklingt.”

Die zwei Erwartungshaltungen, die der Leser, die Leserin wahrscheinlich gewohnt sind, werden hier nicht verspottet oder für überflüssig erklärt, es wird nur gesagt, worum es hier geht, um etwas anderes. Der Stoff, um den es geht: Namen und Worte der Sprache, die aufklingen. Wer nun genau hinhört, wer liest und beherzigt, was er gelesen hat, kann jetzt entscheiden, ob er oder sie sich auf das angezeigte Abenteuer einlassen will. Nur eines geht nicht mehr an: Weisheit und Geistesschätze oder Daten der Naturwissenschaft zu erwarten. Man kann sie ruhig erst einmal beiseite lassen. Das kann natürlich nur der, für den die Forderungen der Philosophie oder der Naturwissenschaft nicht göttlichen Rang haben, das heißt unbedingten Gehorsam fordern. Für viele Leser hört deshalb die sinnvolle Lektüre bereits hier auf.

Der nächste Absatz:

„Der Unterschied liegt nur im Thema. Die Geisteswissenschaft trachtet irgendeiner als klassisch überlieferten Ordnung des Geistes nach, der Naturforscher vermißt (und ermißt daher) die volle Gegenwart der äußeren Natur. Wir wären am Ende, wenn er einmal ganz über sie verfügte. Und er ist heut am Ende, soweit er zum Teil bereits allzu gut über sie verfügt (Kriegschemie, Kriegsphysik).”

Daß der Rang der Wissenschaften etwas mit ihrer Zeit zu tun hat, mit dem Prozeß, den sie im Laufe der Generationen durchmachen, wird nun hier vorgestellt. Naturwissenschaften schön und gut – aber wenn die Bomben auf Hiroshima und Nagasaki die Totalität der Weltbeherrschung andeuten, ist sie für die Menschen, die leben möchten, ans Ende gelangt. Bis hierher und nicht weiter! Wir müssen uns mit etwas anderem beschäftigen, damit es weiter geht.

Nachdem nun die Vorurteile, denen man begegnen kann, sanft – wie ich meine --, aber um so entschiedener beiseitegezogen sind, wie ein Vorhang, konstatiert ein einzelner Satz, worum es geht:

„Der Soziologe vermißt die wirklichen Kräfte, mit denen wir Menschen geschaffen sind.”

Merkwürdig ist hier, daß der Soziologe, nicht die Soziologie das Subjekt des Satzes ist! Es geht nicht ohne seine Beteiligung! Ein Beispiel: das Farbensehen wird von dem Physiker genau untersucht, nur kommt eines dabei nicht vor: die Farbe. Sie wird in Schwingungen und Schwingungsbreiten verteilt. Warum wir Menschen aber die Farbempfindung haben, kann er nicht erklären. Dazu muß man Mensch sein und sehen lernen! Die Naturwissenschaften sprechen deshalb von vielen Phänomenen wie ein Blinder einem Sehenden. Über die Farben können wir also nur sprechen – sie sind auch zu messen, man kann die Korrelationen feststellen, aber die Qualität als solche muß, wenn die Rede irgendeinen Sinn haben soll, erlebt werden und während des Erlebens geprüft werden. Das hat Goethe mit seiner Farbenlehre unentwegt getan. Deshalb gehört er in die Reihe der Soziologen!

Der Satz hat – als vierter in meiner Reihe – die Qualität, als Lehrsatz merkbar zu sein. Darum geht es: Der Soziologe vermißt die wirklichen Kräfte, mit denen wir Menschen geschaffen sind.

Der nächste Absatz:

„Die Soziologie fragt nicht nach dem aus der Vergangenheit überlieferten Geisteserbe wie die sogenannte Geisteswissenschaft, nicht nach der von außen uns gegebenen Natur wie die Naturwissenschaft, sie erforscht die innere Ordnung der uns angeschaffenen, in uns *hineinerschaffenen* Wirklichkeit. Dieser Unterschied gegen historische Überlieferung und natürliche Gegebenheit bedingt nun auch ihr Verfahren.”

Von dem Lehrsatz fort wird der Blick nun wieder geweitet. Es ist wieder von der Soziologie die Rede. Der Unterschied wird nochmals erinnert. Das Interesse für das Verfahren, das dazu gehört, wird geweckt.

Nun kommen sie:

„Daher sind ihre Verfahren auch nicht die oft als die einzigen wissenschaftlichen Verfahren hingestellten der Deduktion und Induktion. Die Gefahrenquellen unseres Tuns als Soziologen wurzeln ja weder in zu geringer Allgemeinheit, noch in zu geringer Vereinzelung unserer Erfahrung. Jener Mangel muß durch Deduktion, dieser durch Induktion geheilt werden. Den Soziologen gefährdet nur sein Mangel an Kraft zur Vergegenwärtigung. Die Schwäche des Soziologen besteht darin, nicht genügend Teil, Partei, Mit-Glied der Wirklichkeit zu bleiben, also in einer verfrühten Flucht in entwertende Unparteilichkeit. Bei solcher Abstraktion und Herauslösung zieht man nämlich nicht genügend Widerstand aus der Wirklichkeit auf sich, man bindet nicht sein Teil von Trägheit und Schwerkraft, weil man wähnt, die chemisch reine Wahrheit im luftleeren Raum vor lauter Idealisten kampflos zum Siege führen zu können. Aber gerade die letzten Jahrzehnte mit der ungeheuerlichen Lügenpropaganda der Weltkriege haben den letzten Schullehrer in Europa zur Unwahrheit verleitet. Die Lüge erwies sich als der Alltag des Menschengeistes. Anfällig und schwach werden selbst die freiesten Denker; kein Gedanke war geschützt vor Mißbrauch. Alles sozusagen ist gedacht und behauptet worden; das kam mit der Zeitung und ging mit der Zeitung. Die besten, edelsten Herzen haben dem Zeitgeist ihren Tribut entrichtet. Der menschliche „Geist” hat seine ideale Rolle ein für allemal ausgespielt.”

Zwischen Allgemeinheit und Vereinzelung muß es etwas Drittes geben. Und das ist dringend nötig, wenn man nicht wie ein Wimpel dem Zeitgeist und der Propaganda widerstandslos ausgeliefert bleiben soll. Für die Beteiligung, die Rosenstock-Huessy voraussetzt, ist freilich das einzig Kostbare nötig: die Lebenszeit. Nur was dem Widerstand der Lebenszeit standhält, kann Aussicht haben, der Lüge zu wehren. Das Eingestehen der Schwäche ist ganz wichtig!

„Kein Genie und kein Amt, kein Volksgeist und kein Schulgeist in Kunst und Wissenschaft, Kanzel und Politik hat unmittelbar mit Gottes Geist etwas zu schaffen. Der Geist ist nicht Gott. Alle Soziologie fängt mit dieser bitteren Einsicht an. Die Soziologie wies sich als Stück der Welt aus, als Partei, die Partei sein darf und soll, zugleich aber als jemand, der keinen Augenblick sich aus dem Ganzen „herauslösen” (d.h. verabsolutieren) darf. Daher ist das soziale Erkenntnisverfahren, das wir gleich in diesem Ersten Teil zu üben versucht haben, das, welches Innen und Außen, Ursprung nach Rückwärts und Notwendigkeit nach Vorwärts unterscheidet. Diese vier Wege zur Bestimmung der Lebenskraft eines Gedankens, eines Willens, einer Tätigkeit, einer Einrichtung usw. gibt es weder in der Naturwissenschaft noch in der Geisteswissenschaft.”

Genie, Amt, Volksgeist und Schulgeist – das sind bereits die vier, die zur Vergegenwärtigkeit alle nötig wären, sich aber gegenseitig auszuschließen scheinen. Der entscheidende, bittere Satz: Der Geist ist nicht Gott – wird jedem Menschen aufgetragen, der teilhaben will an der göttlichen Wahrheit. Der Geist reißt uns nicht aus den Zusammenhängen der Bewährung heraus!

Rosenstock-Huessy stößt an eine Grenze:

„Der Raum verbietet auszuführen, inwiefern Soziologie einzelne dieser Verfahren selbstverständlich mit älteren Wissenschaften (Rechtswissenschaft, Geschichte, Theologie, Ökonomie usw.) gemein hat.”

Der Leser wird erinnert, daß ein Buch auch eine soziale Grenze hat. Es gibt Stellen auf dem Weg, wo man sich mit Ausblicken vorerst begnügen muß. Der Ausblick ist versöhnlich – die Geschwisterlichkeit der neuen Wissenschaft ist verbürgt.

Jetzt wieder eine Zusammenfassung:

„Entscheidend für die Soziologie ist nur die Mehrzahl ihrer Verfahren. Um z.B. zu sagen, was Soziologie sei, haben wir viermal ansetzen müssen. Diese vier Ansätze lassen sich nie in einen Satz oder in eine Definition zusammenziehen.”

Das kann der Leser nicht beiseiteschieben, wenn er bisher gelesen hat. Also keine Zumutung von etwas von außen her Aufgedrungenem, sondern das Fassen der Erkenntnis aus dem Erleben.

Diese Art, das Gesagte zu bewähren, während man spricht, ist nun recht eigentlich der Gewinn, den das Lesen der Werke Rosenstock-Huessys immer bringt: das Durchleben einer Wahrheit setzt einen instand, den Erkenntnissatz an der eigenen Erfahrung zu prüfen.

Das wird nun ausgeführt:

„Sie liegen auf unvereinbaren Ebenen. Die vier Sprechweisen alle sind unerläßlich, um zu vermitteln, was etwa Soziologie bedeute. Die Wirklichkeit will mit allen Kräften unseres Wesens erfaßt werden, mit Neigung und mit kühlem Verstand, mit Furcht und mit Hoffnung, keine dieser Ebenen ist in eine andere übersetzbar.”

Der Leser muß also stets gewärtig bleiben, von einer Ebene auf die andere wechseln zu müssen. Das ist – meine ich – der Hauptpunkt, der so manchen davon abhält oder in Verwirrung bringt. Oder vielleicht gar nicht in Verwirrung bringt, sondern die Abwehr leicht macht, indem einfach gesagt wird: bringt mich in Verwirrung. Ich will nur Neigung (Lyrik), nur kühlen Verstand (Zahlen und Formeln), nur Furcht (Gebet) oder nur Hoffnung (ausführliches Erzählen von schon gelebtem und erlebtem Leben). Aber Rosenstock-Huessy wechselt von einer Ebene auf die andere.

„Wirklich ist uns nur, was in all diesen vier Bereichen von uns erfaßt worden ist.”

Wieder ein Lehrsatz, der sich mit dem vorigen gut zusammenfügt:

Der Soziologe vermißt die wirklichen Kräfte, mit denen wir Menschen geschaffen sind.

Wirklich ist uns nur, was in all diesen vier Bereichen von uns erfaßt worden ist.

Hier will ich abbrechen, aber doch von der übernächsten Seite 58 die Tafel der Haupterkenntnisse herstellen.

„Merke als Prinzipien, die sich als Aufbau der bisherigen Abschnitte bewährt haben:

INNEN (REFLEXIVUM)

Das Recht des Ganzen steht fest, infolgedessen entfaltet sich die Fülle der Richtungen, Gegensätze, Unterschiede. Beispiel: Die Spaltung in soziologische Schulen. Alle Reflexion bricht uns in Parteien auf.

AUSSEN (AKTIVUM)

Das Wesen des Ganzen ist fragwürdig. Daher treten herrschende, entstellende, gefährliche Züge übermächtig hervor. Beispiel: Die Feinde und Gegner aller Soziologie überhaupt zwingen alle Soziologen zusammen.

VORWÄRTS (PRÄJECTIVUM)

Eine neue Not bittet um Einlaß ins Vernehmen. Sie kann nur eingelassen werden, wenn altes abgetan wird und Not zum Notwenden in neuen Trägern führt. Goethe, Saint-Simon.

RÜCKWÄRTS (TRAJECTIVUM)

Soweit alte Not fortwährt, fahren wir dankbar fort, die alten Notwenden anzuwenden. Wir erkennen also an, daß jene Stunde noch währt. Wir sind Nachfolger, weil das Geschehen feststeht und solange es feststeht.”

Was so oft bezweifelt worden ist, daß Rosenstock-Huessy eine strenge Methode des Ein- und Ausschließens, des ehrfürchtigen und des hoffenden Nennens hat, ist – hoffe ich – genügend angedeutet, um Dir darauf Lust zu machen, den Erkenntnisgewinn im erlebenden Lesen zu mehren.

Dein Vater

**SECHSTER BRIEF: DAS KREUZ DER WIRKLICHKEIT**

*Köln, den 20. Dezember 2004*

Lieber Niklas,

heute hat nun schon die Woche vor Heiligabend begonnen.

Viel später als ich dachte fahre ich mit den Briefen fort. Ich komme nun nämlich zum Ursatz der Soziologie: *Wir müssen einander im gegenseitigen Widersprechen treu bleiben.*

Denn die Kraft zum Bewußtwerden entspringt erst unserem Gegenseitig-Werden. Weil ein Mensch sagt: Du bist mein Vater, muß ein anderer sagen: Du bist meine Tochter, und ein dritter vielleicht: Wir sind Geschwister. Alle Wahrheit ist symphonisch. Jeder Satz, den der sprechende Mensch wahrheitsgemäß setzt, zweigt aus der gemeinschaftlichen Mitte ab. Aus dieser Mitte wird ein jeder zu seiner Seite gezwungen, um „den” Menschen im großen und ganzen zu verkörpern. (Die Übermacht der Räume, S. 294)

Das Gewissen, das Wissen, das Bewußtsein, das Selbstbewußtsein sind die Abwandlungen dieser Treue, je nachdem wir selber uns als Du, Wir, Ich, Es vorfinden. Sie sind einfach dasselbe Licht der Vernunft in vier Brechungen.

So nimmst du immer deine Mitmenschen für dein besonderes Tun in Anspruch und so weißt du in jedem Moment weniger über deine eigene Menschlichkeit, als wahr ist! (S. 295)

Dies ist also der Sinn der Denkweise, die dem Kreuz der Wirklichkeit entquillt. Das Kreuz der Wirklichkeit ist nicht ein Gegenstand, eine mathematische Figur, die ich mir ausgedacht habe – wie oft habe ich diesen Unsinn hören müssen -, sondern im Kreuz der Wirklichkeit tritt jeder von uns kleinen Menschen unter die wirksame Wahrheit. Die wirksame Wahrheit aber wohnt nur dem Menschen im großen und ganzen inne. Erst im Kreuz der Gemeinschaft hat das, was dieses Buch sagt, Sinn. (S. 296)

Noch einmal denn: Der Mittelpunkt des Kreuzes ist kein mathematischer Schnittpunkt von vier Linien. Unser Herz ist die Kreuzweiche. Unser Herz ist kein Punkt. Unser Herz schlägt, damit es uns jeweils auf unsern Kreuzesarm verschlage. Das Herz ist der Abgesandte des Makro-anthropos in den Mikroanthropos, des Einen großen Menschen in uns kleine Menschen hinein. Kraft des Herzens organisiert sich die menschliche Gemeinschaft in uns hinein.

Das Herz, Kreuzesquell aller Lebensarten, ist aber kein Grenzpunkt der Natur des einzelnen Individuums, sondern in ihm tauschen Hörer und Sprecher, so daß der Hörer zum Sprecher, tauschen Braut und Mutter, so daß die Mutter neu zur Braut werden kann! Der Vater wird wieder zum Sohn, das Alter jung. Dem Herzen verdanken wir es also, daß niemand dazu verurteilt bleibt, a=a, b=b, c=c zu spielen. Das Herz erlaubt uns, jeden Tag von vorn anzufangen.

Wir alle entstammen der Gegenseitigkeit menschlichen Herzschlags. Die Gegenseitigkeit führt zur Gemeinschaftsordnung. Die Ordnung verkörpert sich schließlich im All-Einsamen. (S. 297)

Das Kreuz der Wirklichkeit ist nichts anderes als der Vorgang selbst, in dem sich alle Gruppen bilden und erhalten. Sie können sich nur so bilden, daß Menschen, statt jeder seine eigene Mitte zu bilden, aus der Kreuzesmitte der in dieser Gemeinschaft geltenden Doppelzeit und des für die Gemeinschaft eingefangenen Doppelraums getrost gegensätzlich und widerspruchsvoll leben oder wenn es sein muß, zu ihrer Stunde ein nächstes Kreuz stiften. (S.298)

Zwischen den Zeiten und den Räumen besteht eine lebendige Abwandlung, dank derer sie kein Schema der Logik, sondern Erfahrungen, es könnte auch heißen: Durchfahrungen der Wirklichkeit, darstellen. Diese Durchfahrtsstationen nennen wir abwechselnd *Zukunft, Innen, Vergangenheit, Außen*, und begegnen einer jeden Kraft in einer anderen Grundhaltung. Wie verläuft nun dieses Geflecht? Wir haben es schon mehrmals ausgeführt, vor allem im Kapitel über die Sprache. Trotzdem soll es hier noch einmal gesagt werden. Es soll nämlich diesmal so gesagt werden, daß es die Begriffe der Schule, Zeit und Raum, aus ihrer Starrheit löst und in die gegenseitige Bewegung setzt, dank der wir von ihnen überhaupt nur wissen. Der ersten Zeit (Präjekt) folgt der erste Raum (Subjekt), dann folgt die zweite Zeit (Trajekt) und zuletzt der zweite Raum (Objekt). Alle vier sind soziale und sprachliche Schöpfungen. Alle vier gelten nur in der Gemeinschaft und sind benannte Resultate sprachlicher Verbindung zwischen Sprechern und Hörern. (S. 299f.)

Das nicht in die Zukunft namentlich gerufene Menschenkind ist noch nicht Mensch. Der keiner inneren Zeitspanne fähige Mensch, ist noch nicht poetisch. Der keiner Institution Ehrfurcht bezeigende Humanist ist noch nicht kultiviert, und der kultivierte, der keine alten Herkommen analysieren will, ist noch nicht aufgeklärt. Wir sollen aber Menschen, die Befehle vernehmen, Künstler, die begeistert singen, Amtspersonen, die Ehre im Leibe haben, und Kritiker, die auf nichts hereinfallen, alles in einem sein. (S. 301)

Das Nacheinander im Hervortreten von (1) Gebot, (2) Gesang, (3) Gesetz, (4) Gegenstand – bedeutet also durchaus nicht, daß Nr. 1 aufhören könnte, wenn Nr. 3 und Nr. 4 geschehen. „Die objektive Natur” ist nichts Wirklicheres oder Wahreres als die Schöpfung aus Gottes Hand, so wenig Lebenschaffen (Akt 1) primitiver ist als Töten (Akt 4). Aber wir werden ins Leben gerufen, uns enthüllt sich sein Wesen, wir verkörpern seine Formen und wir zerschlagen sie wieder; in unaufhörlicher Durchflechtung zeitlicher und räumlicher Haltungen sind wir gezwungen, bald dem gebieterischen Augenblick uns unbedingt als seine angerufenen Träger, Präjekte, hinzugeben, bald dem Gefühl Raum abzudingen und abzudeichen durch vertrauliche Mitteilung, dann aber dieser inneren Gemeinschaft Dauer zu verleihen durch bleibende Denkmale ewiger Wiederkehr und schließlich diesen ganzen Lebensprozeß auf den Müllhaufen der analysierten Außenwelt zu werfen, um Raum zu schaffen für die nächste Schöpfungswoche. (S. 301)

Biologisch gesprochen sind *Zukunft, Innen, Vergangenheit, Außen* die vier Stadien auf dem Lebensweg jedes lebendigen Gebildes, weil sie Zeugung, Gestaltung, Fortpflanzung, Sterben bedeuten. Diese höchste Verallgemeinerung des Lebensprozesses in seinen vier Stadien geht uns nicht an der Zelle der Amöbe, aber an der höchsten Lebensform der menschlichen Gesellschaft auf. Von dieser höchsten Lebensform gilt es auf die unvollkommeneren Lebewesen hinunterzublicken. Dies hat auch ein Biologe, A. Meyer, bereits in der führenden Sammlung *Bios* im Jahr 1934 mit diesen unseren Lehren geleistet. Er hat sie auf alle Lebewesen als notwendige Kategorien angewendet. Das höchste Leben erklärt alles niedere! Die neue Wirklichkeitslehre mit ihrer Durchbrechung der Scheidewände zwischen Theologie und Philosophie bettet also alle Biologie in die Soziologie ein. Totes wird von unten nach oben, Lebendes aber nur von oben nach unten durchleuchtet. Wie das Sonnensystem das Atom erklärt, so erklärt die gesamte Zeitraumerfahrung der Menschheit die Lebensprozesse aller Vorstufen ihres Lebens. (S. 302)

Der erste Mensch aber war auch der erste Soziologe: Denn wir handeln, weil die Geburten auf den Todesfällen fußen müssen und das Vollbringen auf den Fehlern; der Tod ist uns beigestellt. Zur menschlichen Gesellschaft gehört das Sterben als Vorstufe des nächsten Werdens. (S. 303)

Wer die Zeit als Augenblick erfährt, erfährt sie als Auslese, als Prinzip der Zuchtwahl. Jeder liebenden Seele widerfährt die Zeit als Augenblick, weil sie nur lieben kann, wenn sie auszurufen vermag: „Ihn oder keinen!” (S. 306)

Die subjektive Zeit der Spannung steht unter dem Zeichen: Werd ich's mitteilen können? Wird meine Kraft ausreichen, den auserwählten Auftrag abzutragen?

In dieser dritten Zeitweise wird dem erst dankbar Berufenen und hernach neu angespannten eine wiederkehrende Menschenart verdankt. … Die Ewigkeit wird in Billionen Jahren gesucht und mit „immer”, dem Attribut des Toten, verwechselt. Aber die Worte Ehe, Ewa, Ewigkeit bedeuten nur das Gesetz der Wiederkehr. Das ist ewig, das sich ewig wiederherstellt, nicht das, was immer da ist. (S. 307)

Der vierte Akt der Zeit ist die entspannte Zeit des Raums. Wo die Zeit wederals „jetzt oder nie”, noch als Zeitspanne der Erwartung noch als ewige Wiederkehr auftritt, da ist sie wahrnehmbar als Eigenschaft des Raums. Im bloßen Zeitraum der Physik ist die Zeit gestorben. Der Zeitraum der Physik ist nicht etwa ewig tote Zeit. Er ist bloß gestorbene Zeit. (S. 308)

Und dazu widerfuhr ihm die Zeit in vierfacher Gestalt: als Augenblick, der dich erwählt, als Stunde, die mich spannt, als Ewigkeit, die uns verkörpert, als Zeitraum, der alles Vergangene enthält. (S. 309)

Die entwurzelte, aus ihren Zeiten entwurzelte Menschheit wird zum Menschengeschlecht, sobald sie wieder die Zeiten richtig erleben darf.

Alle Jahrhunderte haben um diese vier Formen der Zeit (Augenblick, Ewigkeit, zeitentrücktes Spiel und den Notstand der Stunde) gewußt. Uns aber ist aufgetragen, den Menschen in seinen Urstand einzusetzen. Er muß Wurzeln zu schlagen wagen, daß ihn die Gezeiten der Zeit ergreifen können, daß er weder als neurasthenischer Gebieter der Zeit noch als Sklave der Stoppuhr lebt, aber im Dienste der Gewalt, die jeder Zeitform das Auf- un Abtreten gebietet. Der Gewalt der Zeiten, das heißt der Verwurzelung des Menschen, wenden wir im zweiten Bande unser Gesicht zu. (S. 310f.)

Jetzt habe ich, Niklas, für uns beide in Anspruch genommen, was Franz Rosenzweig als den ersten Akt der Gemeinschaftsbildung dargestellt hat: das gemeinsame Hören. Nichts habe ich dazwischen gesprochen.

Dabei wollte ich von dem Kreuz der Wirklichkeit reden, den vier Armen, aber der Pointe, daß es Eugen Rosenstock-Huessy vor allem auf die Fünf in der Mitte der Würfelfünf ankommt! Das Herz, das in jedem Augenblick entscheiden darf, kann, soll, muß und mag, zu welcher Form es sich jetzt entschließt.

Der Advent ist in den alten Himmelsreichen immer vorgestellt worden als senkrecht vom Himmel herab: zwischen den vier Punkten der Sonnenaufgangspunkte bei Winter- und Sommersonnenwende und der Sonnenuntergangspunkte bei Winter- und Sommersonnenwende in der Mitte: *O Heiland, reiß die Himmel auf, herab herab vom Himmel lauf, reiß ab vom Himmel Tor und Tür, reiß ab, wo Schloß und Riegel für* – wie Friedrich von Spee, der Jesuit, der auch in Köln gelehrt hat und ein mutiger Streiter gegen Hexenprozesse war, im Jahre 1633 gesungen hat.

Wir haben es noch jedes Jahr gesungen.

Dein Vater

**SIEBTER BRIEF: DIE VISIONÄRE ERFAHRUNG DES WELTKRIEGS**

*Köln, den 24. Dezember 2004*

Lieber Niklas,

daß wir, ein jeder von uns, in einer Epoche stehen, daß die Zeit schon vor der Geburt begonnen hat, auch nach dem Tode hier auf Erden weitergeht, man lernt es mit fünfzehn Jahren, wenn man sich als einzelner entdeckt. Deshalb gelten die fünfzehn Jahre vor der Geburt als Verwurzelung in der kosmischen Zeit, in der alle Zeiten versammelt sind, wo das Leben, das beseelte Leben entworfen wird und nun die Passage durch die bewußt erlebte Zeit erst antritt.

Da ist es denn merkwürdig, wenn jemand behauptet, diese fünfzehn Jahre – bei mir waren es die von 1942 bis 1957, und 1956 und 1958 erschien die Soziologie in zwei Bänden! – seien nicht die wirklich wichtigen Orientierungspunkte für das Leben im Atem des Menschengeschlechts, sondern das im Ersten Weltkrieg schon, von 1914 bis 1918 Erfahrene, und nun hätten die Lebenden Mühe, diese Erfahrung mit vollem Ernst aufzunehmen. Mein Vater ist 1913 geboren – dasselbe gälte also auch schon für ihn.

Daß der Erste Weltkrieg eine Erfahrung der Waffentechnik und dazu gehörig des Kampfes brachte, ist leicht einzusehen, vermitteln vielleicht schwach auch die Fotos von den Kampffeldern in Frankreich. Aber für uns war der Eindruck, den der Zweite Weltkrieg hinterlassen hat, doch sehr viel mächtiger. Denn der fand ja zuhause statt – wir verließen freiwillig im Juli 1943 Hannover und im Oktober, so viel ich weiß, wurde das Haus Lutherstraße 8, wo Ulrich und ich, Lorenz und Albrecht geboren sind, zerbombt – alle nicht nach Imbshausen mitgenommene Habe wurde vernichtet, meine Eltern beklagten den Verlust des Klaviers und dann noch der Bücher – von allem anderen haben sie nie etwas gesagt.

Hebt nun Rosenstock-Huessy diese Eindrücke, die unauslöschlich sind, auf? Will er sie aufheben? Nein – er sieht sie aber als Eindrücke, die nicht nötig gewesen wären, wenn man in Deutschland und anderswo auf den Eindruck gehört hätte, den der Erste Weltkrieg auf ihn und auf viele andre gemacht hat. Er war 26 Jahre alt, als er 1914 in den Krieg zog, als jüdischer Herkunft und Offizier im Ersten Weltkrieg, der doch vorsichtig bleibt und sich lieber dem jüdischen Zahnarzt in Berlin anvertrauen will, als dem Zahnarzt bei der Truppe. Zugleich war er in einem Maße ausgebildet und studiert, daß er schon 1912 sein Professorenbuch *Königshaus und Stämme* geschrieben hatte und der jüngste Privatdozent in Leipzig war. Für den Eindruck im Krieg stand seine Seele also einerseits so offen, wie es bei einem 26jährigen zu erwarten ist (denk Dir, David würde jetzt als Soldat in den Irak gehen müssen!) und andrerseits hatte er Organe zum geschichtlichen Verstehen ausgebildet wie – möchte ich sagen – kein zweiter. Oder der zweite war hier Franz Rosenzweig, zwei Jahre älter, der ihm den Weg verwies, seine Erfahrung in eine Form zu gießen, die esoterisches, das heißt die Allgemeinheit ausschließendes Ansehen gehabt hätte.

Wenn aber die Erfahrung des Weltkriegs bedeutet, daß die geistigen Prozesse erneuert werden, dann ist es etwas ganz Einfaches, was nun geändert werden muß, so schwer meist eben das ganz Einfache auch zu tun und zu meistern ist. Und solange dieses Einfache nicht getan und gemeistert ist, bleibt der Imperativ, es zu tun, stehen und gibt die Epoche an.

Vielfach hat Eugen Rosenstock-Huessy alles drei getan, was gefordert war: dem Imperativ folgen und das dann Erlebte meistern und den Imperativ anderen weitersagen. Wie lautet er?

*Abschied von Descartes* – ist eine Kapitelüberschrift in dem amerikanischen Revolutionenbuch *Out of Revolution*, dem einzigen *opus magnum*, das Rosenstock-Huessy auf Englisch veröffentlicht hat, wenn auch die beiden anderen Werke, *The Multiformity of Man* und *The Christian Future* nicht zu unterschätzen sind.

In diesem Kapitel steht der Imperativ zu einer neuen Form der Hochschule. Er tritt nicht allein auf, sondern zusammen mit der Erinnerung an Anselm von Canterburys *Credo ut intelligam* – der kräftige Glaube an die Erlösung setzt den Verstand frei – und René Descartes' *Cogito ergo sum* – wenn ich im Zweifel bin und etwas erforschen will, ist nur mein Verstand real. Und umgreifend, nämlich so, daß beide Formeln der Scholastischen und der Akademischen Hochschule darin als Variationen enthalten sind, setzt Rosenstock-Huessy nun hinzu: *Respondeo etsi mutabor* – wenn ich eine Wahrheit erforscht habe, bringe ich sie nur um den Preis zum Ausdruck, daß ich sozial nach der Mitteilung nicht mehr als derselbe dastehe wie vorher. Oder: wenn du geschwiegen hättest, wärest du Philosoph geblieben – nämlich in der Meinung deiner Zuhörer. Jede Offenbarung einer Erfahrung verändert den Sprecher – und zwar nicht nur in der Meinung seiner Zuhörer, sondern – weil er sich ja auch selber hört – bei sich selber. Die Flüche, die laut werden, wenn man etwas verbockt hat, kennst Du doch gut genug.

Was ist aber die Veränderung mit einfachen Worten? Die von den Naturwissenschaftler geforderte Haltung ist die, daß man die Wahrheit über die Natur nur erforschen kann, wenn man selber keine Eindrücke hat – oder sie völlig unterdrückt und zum Schweigen bringt. Aber alles, was erforscht werden kann, ist nur an Wirkungen zu erforschen! Wer also Menschliches erforschen will, muß die Eindrücke, die er selbst empfängt als kostbarste Mitteilung nehmen! Wir sind selber das feinste Meßgerät – für die uns treffenden Erfahrungen. Die asiatische Medizin geht so vor. Was ein erfahrener Arzt dort schmecken, riechen, sehen, hören und tasten kann, geht weit über das hinaus, was die Apparate als Meßergebnisse liefern – und vor allem: es wird sogleich im Kontext wahrgenommen und braucht keine weitere Zeit, weil es zu einem unmittelbaren Eindruck wird. Die Ausbildung dieser feinen Meßorgane ist entsprechend langwierig.

So fordert Rosenstock-Huessy, daß an die Seite von Subjekt und Objekt zwei andere Erlebnisweisen anerkannt werden, Präjekt und Trajekt. Präjekt sind wir selber, wenn uns die Not eine neue Erfahrung abverlangt hat, die wir, solange wir auf der Spur des bisher Erlebten, Erforschten und Angewandten bleiben, nicht verstehen können.

Eugen Rosenstock-Huessy ist – vom Ersten Weltkrieg her – also ein Präjekt, vorausgeworfen in ein Verständnis der Wirklichkeit, das es bis dahin so nicht gegeben hat.

Trajekt sind wir entsprechend, solange wir auf der Spur des Bisherigen verweilen und weitergehen können. Ein wie großer Teil unseres Lebens gehört dazu – man macht es so, weil es sich bewährt hat, man weiß vielleicht gar nicht, wieso und warum.

Das geht nun nicht so zu, daß Rosenstock-Huessy etwa Descartes verdammen würde! Ihm öffnet sich eben auch für ihn ein Verständnis, das den Blick verändert. Gerade daran bewährt sich Rosenstock-Huessys präjektive Erfahrung!

Er schreibt von Descartes:

„René Descartes wurde 1596 in der Tourraine geboren; sein Vater amtierte am Parlament von Rennes und der Vater erzog ihn bis zum neunten oder zwölften Lebensjahre (die Daten sind umstritten), d.h. bis er auf die neue Jesuitenschule von La Flèche kam. In diesem Internat hat er neun Jahre zugebracht. Étienne Gilson hat nachgewiesen, wieviel von dem dort Gelernten in Descartes zeitlebens verblieben ist. Aber als 1615 Descartes frei wurde, da fühlte er sich nur in seiner mathematischen Schaffenskraft wohl. Alles sonst Gelernte erschien ihm wirr und unsicher, eine bedauerliche Zeitverschwendung. „Nichts mehr von solchem Zeug, nur noch Gewisses denken", war sein Ruf. Finanziell unabhängig – er ist es zeitlebens gewesen -, reist der Kavalier und kam dabei nach Holland.

Hier traf er den älteren Pylades, der diesem Orestes seine Mutterlosigkeit durch schützende Freundschaft geadelt hat. Es ist der acht Jahre ältere Beeckman gewesen, der 1618 Descartes seine Methode formulieren half. Beide wollten Physik auf Mathematik gründen, und so rohe Empirie und Skepsis beide bekämpfen. Wer bedenkt, daß Beeckman 30 und Descartes 22 Jahre alt war, der weiß, daß Beeckman eine ungeheure Sicherheit für Descartes geschaffen haben muß. Seine Entdeckung durch Beeckman ist seine Empfängnis. Ein Jahr später hat ihn diese Entdeckung so bestärkt, daß er seinen Traum, seine drei Träume am Kachelofen in Ulm träumt. Seine Lebensaufgabe steht deutlich vor ihm. Mit einer Wallfahrt nach Loretto, sechs Jahre später hat er dem Himmel für diese Einsicht gedankt. Er wird mit seiner Deduktion aller physikalischen Tatsachen auf Mathematik eine Kette um sämtliche Wissenschaften winden: es wird am Ende nur einerlei Wissen geben.

Uns geht hier der Mutterschoß an, aus dem der ungeheure Mut des Descartes kam, die geheimnisvlle Begegnung und das eigentlich noch geheimnisvollere Anerkanntwerden durch den acht Jahre älteren und scheinbar nicht im geringsten eifersüchtigen Beeckman.

Wir wissen von dieser ersten Umarmung durch den älteren Freund erst seit kurzem. Aber immer schon ist der zweite Fall hervorgehoben worden, in dem ein acht Jahre Älterer Descartes bestimmend vorwärtsgetrieben hat. 138 Briefe hat Descartes an Mersenne geschrieben, den Herausgeber der – noch nicht existierenden – "Zeitschrift für Physik". Der Mönch Mersenne (1588 bsi 1648) fungierte als Vorläufer unserer heutigen wissenschaftlichen Publizistik. Wie am Ende die Notgemeinschaft deutscher Wissenschaft gekommen ist, so kam zu Beginn die Freiwilligengemeinschaft der Freunde der Forschung. Mersenne ist ein Zentralknoten in dieser Organbildung. Wieder also beugt sich ein acht Jahre Älterer mütterlich über den Gebrechlichen; es war vermutlich vier Jahre nach Ulm, 1623, und der Traumauftrag, die neue Methode auszubauen, kam nicht recht vom Flecke. Da band Mersennes Teilnahme das zarte Spalierobst hoch und durch seine Zuwendung hat er es zur frühen Reife geschickt gemacht.

Die Befruchtung durch Beeckman und die Reifung durch Mersenne sind dann noch fünf Jahre später ergänzt worden durch die Mahnung des Kardinals Berylle: Vergeudet Euer Pfund nicht. Ihr seid es Eurem Traumgesicht schuldig, es nun auch durchzuführen. Erst darauf hat der lässige Kavalier, der Gentleman, von Descartes den Abschied erhalten; nun erst, 1628, ist er in seine Muße in Holland gepilgert und hat sein Bild des zusammenhängenden, nur ausgedehnten "MONDE" begonnen. Wieder fünf Jahre später – die "Monde" ist bald fertig, meint er – kommt die Nachricht von Galileis Verurteilung in Rom. Noch einmal huft er zurück. Die "Welt" bleibt ungedruckt. Man sieht, wie zart diese Seele in Wirklichkeit war. 1618, 1623, 1628, 1632/33 läßt sich der Weltmann zwar gewinnen, befruchten, binden, bestimmen. Aber die Äußerung bleibt auch dann noch aus. Endlich 1637 entsteht aus allen diesen mütterlichen Hilfen – keine Autorität eines Vaters, oder auch nur Lehrers ist am Werke, und Descartes hat nicht gelehrt, sondern ist Kavalier geblieben – endlich entsteht der *Discours de la Methode*. Die Geschichte übersieht zu leicht den Sinn dieses großartigen Zögerns. Im Rückblick gleiten wir so schnell über die Jahre weg. Aber in Wahrheit sind Mersenne wie Beeckman, Berylle wie Galilei mütterlich vor Descartes hingetreten, oder wenn nicht mütterlich, so als ihm überlegene Freunde, um ihn zu dem Schwierigsten vorzubereiten, zu einer neuen Methode. Denn nicht Tatsachen, nur Denkweise verändert die Wirklichkeit." (*Die Übermacht der Räume*, S. 321f.)

Das ist unvergeßlich gesehen und geschrieben – liebevoll! Und zugleich ein Hinweis auf die Verzögerung, die Rosenstock-Huessys verändernde Denkweise selber erfahren hat – als der Freund Franz Rosenzweig seinen *Stern der Erlösung* herausgebracht hatte, gewann Rosenstock-Huessy lange Zeit für sein Erstes Wort, das er Rosenzweig schon eingepflanzt hatte. Rosenzweig schrieb den *Stern der Erlösung* als genauer Zeitgenosse der Revolution von 1918 in Deutschland – Rosenstock-Huessy veröffentlichte sein *Kreuz der Wirklichkeit* (und noch immer schamhaft verhüllt unter dem Titel: *Soziologie*) vierzig Jahre später.

Deine Schreiben aus Castleton folgen ganz unwillkürlich der Methode Eugen Rosenstock-Huessys: Du nimmst Deine Eindrücke ernst und folgst der Spur, die von ihnen gebildet wird. Und die Widersprüche, die sich Dir auftun, bleiben stehen und sind nicht durch Tatsachenbeschreibung à la *Cogito ergo sum* aufzulösen.

Die Geburtsstunden einer neuen Denkweise sind wie Weihnachten. Und deshalb schreibe ich Dir heute, am Heiligen Abend davon.

Dein Vater

**ACHTER BRIEF: SYSTEMATIK**

*Köln, den 26. Dezember 2004, Zweiter Weihnachtstag*

Lieber Niklas,

eben höre ich nach dem Anruf von Herrn Dr. Scherer, daß Johanne in Bangladesh wohlbehalten ist! Das ist doch ein Trost in den schlimmen Nachrichten, die aus Südostasien kommen, die mich an das Erdbeben von Agadir erinnern, welches das sein muß, woran als letztes großes Erdbeben erinnert wird, vor vierzig Jahren.

Beim Weihnachtsgottesdienst hörte ich nun zum drittenmal den Teil der Weihnachtsgeschichte, der sich mit den Hirten befaßt. Und an ihm möchte ich zeigen, wie die Kategorien Präjekts, des Subjekts, des Trajekts und des Objekts aus den vier Evangelien stammen.

Rosenstock-Huessy hat in seinem Revolutionsbuch *Out of Revolution* gefunden, daß die großen Revolutionen im Abendland nacheinander an eine Epoche des christlichen und des biblischen Lebens angeknüpft haben und das das gerade das Merkmal ist, daß sie nicht willkürliche Geschehnisse sind, sondern in einem merkwürdig symphonischen Kontext stehen.

Die Papstrevolution knüpfte an die Zeit die ersten Jahrhunderte der Kirche an, als die Schriften der Bibel kanonisiert wurden. Die Guelphen mit dem Heiligen Franziskus lebten die Passion und das Kreuz Christi und seiner Jünger. Luther, der das Predigtamt des deutschen "Geistes" einsetzte, knüpfte an das Prophetenamt des Elias, des Johannes und Jesu an. Cromwell setzte das Amt der Richter wieder ein und die göttliche Stimme der Volkes aus der Zeit Israels vor dem Königtum Davids. Frankreich ging in das Zeitalter vor den Offenbarungen zurück bis zu Adam vor dem Sündenfall. Und Rußland und – wie er schreibt: *wir Zeitgenossen* vergraben uns in den präadamitischen und prähistorischen Gewalten der Arbeit, des Geschlechts, der Jugend, der primitiven Stämme und Clans, der Hormone und Vitamine. (*Out of Revolution*, S. 799)

Aber er fand ja selber für sich etwas anderes wichtiger!

Wenn die Lehre Rosenstock-Huessys so umstürzend alt und neu, eben altneu wie die Revolutionen ist, dann muß sie sich daran bewähren, daß sie auch an ein Stück der biblischen Geschichte anknüpft. In aller Deutlichkeit ist er damit erst in dem letzten Kapitel des Sprachbuchs von 1963/64 herausgekommen, dem Kapitel über die vier Evangelien mit dem Titel: *Die Frucht der Lippen*. Es war als Büchlein auf Englisch 1954 vollendet – wurde aber, da er die Zeit für noch nicht reif hielt, nicht veröffentlicht. Wie sehr ihm daran lag, ist zu erkennen daran, daß es – zusammen mit einem anderen Kapitel – als Extrabüchlein nochmals erschien unter dem Titel: *Die Umwandlung* 1968 – also zum achtzigsten Geburtstag. (Es war aber eine Gabe des liebsten Jüngers Rosenstock-Huessys, Bas Leenmans!)

Die vier Evangelisten und die vier Evangelien werden da nach langer Zeit wieder ernstgenommen. Sie stellen nämlich die Botschaft in den vier Stilen des Präjekts, des Subjekts, des Trajekts und des Objekts dar!

Das Lukasevangelium, als das dritte, ist ausdrücklich geschrieben als nochmalige Erzählung. Diese richtet sich an das jüdische Volk, möchte alle Sorgen beruhigen, sie gehörten etwa nicht zu den Adressaten, ist daher am weitesten ausschwingend in Erinnerung und Erzählmodus.

Und nun sieh, was ich diesen Weihnachten für Dich gefunden habe!

Wenn Rosenstock-Huessys Botschaft eine Variation der Frohen Botschaft ist – wie es die Scholastik und die Akademik auf ihre Weise auch gewesen sind, vielleicht mit anderem Akzent, aber zunächst mit dem Überraschen und der Befreiungswirkung begrüßt -, dann sind wir, die ersten Hörer der Botschaft (und wirklich müssen wir uns insgesamt immer noch als solche betrachten!) an der Stelle der – Hirten, von denen Lukas erzählt.

Die Weihnachtsgeschichte erzählt also in Wirklichkeit davon, wie das lebendige Wort in uns zum Ereignis wird!

Damit es das kann, muß es erst einmal da sein. Das ist der Vorfall, von dem zuerst erzählt wird. (Ich nehme hier Luthers Übersetzung, weil sie nach wie vor die Wiege unsrer Sprache ist).

VORFALL

*Es begab sich aber zu der Zeit, daß ein Gebot von dem Kaiser Augustus ausging, daß alle Welt geschätzt würde. Und diese Schätzung war die allererste und geschah zur Zeit, da Cyrenius Landpfleger in Syrien war. Und jedermann ging, daß er sich schätzen ließe, ein jeglicher in seine Stadt. Da machte sich auf auch Joseph aus Galiläa, aus der Stadt Nazareth, in das jüdische Land zur Stadt Davids, die da heißt Bethlehem, darum daß er von dem Hause und Geschlechte Davids war, auf daß er sich schätzen ließe mit Maria, seinem vertrauten Weibe, die war schwanger. Und als sie daselbst waren, kam die Zeit, daß sie gebären sollte. Und sie gebar ihren ersten Sohn und wickelte ihn in Windeln und legte ihn in eine Krippe; denn sie hatten sonst keinen Raum in der Herberge.*

Das ist mit dem Bemühen erzählt, ganz deutlich den Vorfall zu sagen, mit genauer Bestimmung der Zeit und des Orts. Der Anlaß, der die Zeit genau bestimmt, ist die Schätzung, das heißt das Registrieren der steuerfähigen Einwohner. Die Welt wird immer mehr eins – in diesem Prozeß ist hier ein genauer Punkt gewählt.

Nun beginnt die Geschichte der Hirten, also unsre Geschichte als Hörer des Worts:

PRÄJEKT

*Und es waren Hirten in derselben Gegend auf dem Felde bei den Hürden, die hüteten des Nachts ihre Herde. Und siehe, des Herrn Engel trat zu ihnen, und die Klarheit des Herrn leuchtete ums sie; und sie fürchteten sich sehr. Und der Engel sprach zu ihnen: Fürchtet euch nicht! Siehe, ich verkündige euch große Freude, die allem Volk widerfahren wird; denn euch ist heute der Heiland geboren, welches ist Christus, der Herr, in der Stadt Davids. Und das habt zum Zeichen: ihr werdet finden das Kind in Windeln gewickelt und in einer Krippe liegen.*

Da ist der Messias, der Gesalbte, der Christus! Das ist die Botschaft, die mit einer Lichterscheinung auftritt und großen Schrecken verbreitet. Der Schrecken wird durch das Wort der Botschaft gelindert. Es bricht eine neue Zeit an. Keine Spekulation, sondern mit Merkzeichen, die wiederzufinden sind auf Erden.

Plötzlich ist das Leben der Hirten verändert: sie sind aus Subjekt, Trajekt und Objekt, das sie waren (Objekt zum Beispiel als Leute, die dem römischen Kaiser Steuern zahlen müssen, Trajekt als fromme Juden, die auf den Messias warten, Subjekt als die Menschen, die ihrer Arbeit nachgehen), zu Präjekten geworden! Alles erscheint nun in einem anderen Licht. Das Licht ist so überwältigend, daß es das ganze Innere erneuert.

Davon ist nun die Rede.

SUBJEKT

*Und alsbald war da bei dem Engel die Menge der himmlischen Heerscharen, die lobten Gott und sprachen: Ehre sei Gott in der Höhe und Friede auf Erden und den Menschen ein Wohlgefallen.*

Die Wünsche aller Menschen, wenn man sie auf den Punkt bringt, die aufrichtigen meine ich, dann sind sie in diesem Engelschor gesagt! Gott, Welt und Mensch in Harmonie und Eintracht! Du kannst alle Dichtung unter diese Verse stellen. Und die uns vielleicht befremdlich anmutende Gestaltung dieses Themas ist doch gar nicht so verwunderlich: in der alten Welt wurde jede Inspiration als Besuch einer göttlichen Kraft verstanden, eines Engels. Die Wünsche der Menschen sind die himmlischen Heerscharen! Die Musik hat sich doch ganz diesem Teil der Weihnachtsgeschichte gewidmet!

Aber danach tritt wieder ein Wechsel ein – die Hirten fangen an, miteinander zu reden und treten als WIR in die nächste Phase:

TRAJEKT

*Und da die Engel von ihnen gen Himmel fuhren, sprachen die Hirten untereinander: Laßt uns nun gehen nach Bethlehem und die Geschichte sehen, die da geschehen ist, die uns der Herr kundgetan hat. Und sie kamen eilend und fanden beide, Maria und Joseph, dazu das Kind in der Krippe liegen. Da sie es aber gesehen hatten, breiteten sie das Wort aus, welches zu ihnen von diesem Kinde gesagt war. Und alle, vor die es kam, wunderten sich der Rede, die ihnen die Hirten gesagt hatten. Maria aber behielt alle diese Worte und bewegte sie in ihrem Herzen.*

Zu dem Vorfall tritt das Wort hinzu! Es sind gleich mehrere, die beteiligt sind. Und die Hirten können nicht in ihrer Brust verschlossen halten, wie Geschichte und Wort dazu für sie Ereignis geworden sind. Das Erstaunen breitet sich aus, Maria behält die Worte als wichtige Mitteilung. Wenn Maria die Kirche ist, dann ist das ihre Aufgabe: alle diese Worte behalten und im Herzen bewegen.

Das Wort aber bewährt sich darin, daß es sich auch gelöst von dem Ereignis als gültig erweist. Deshalb gehört der vierte Schritt unbedingt dazu, so kurz er auch berichtet wird:

OBJEKT

*Und die Hirten kehrten wieder um, priesen und lobten Gott um alles, was sie gehört und gesehen hatten, wie denn zu ihnen gesagt war.*

Es hatte sich erwiesen, es war kein Wahn gewesen, was sich in Präjekt und Subjekt gezeigt hatte und im Trajekt leibhaftig wurde! Die Verläßlichkeit ist der Grund alles weiteren. Du sollst gar nicht einfach etwas glauben, sondern es wirken lassen auf all Deine Wünsche, dann zum Erleben zurückkehren, es finden und den Schritt machen, im Gedenken das festzustellen, daß es ja so geschehen ist.

In vielen Partien des Lukasevangeliums sind diese vier Stationen eines Ereignisses genau erzählt, aber hier doch wohl als Muster für alles weitere, so daß man es lernen kann, wie Gott in der Geschichte wirkt.

Es ist daher kein Wunder, daß sich die vier Kategorien des Präjekts, Subjekts, Trajekts und Objekts auf alles anwenden lassen und dabei offenbarend wirksam sind. Auf diese Offenbarungs-, auf diese Kraft, Orientierung zu geben, kommt es nämlich an, nicht darauf, daß irgendjemand sich freuen kann, weil er rechthat.

Ich meine nun, daß Rosenstock-Huessy neben Gregor VII., Luther, Cromwell, Robespierre als Revolutionär gesetzt werden soll, weil er zu der Geschichte, dem Weltkrieg und dem Zusammenstoß von Krieg und Revolution das Wort gefunden hat, als geduldiger, schamhafter Träger.

Es hat sich in vier Werken während seines Lebens nach der Offenbarung kundgetan:

Als Präjekt in dem Buch: *Die Hochzeit des Kriegs und der Revolution*, Würzburg 1920 (da war er also 32 Jahre alt);

dann als Subjekt in dem Buch: *Die Europäischen Revolutionen* 1931 und *Out of Revolution* 1938, wo der subjektive Charakter viel deutlicher herausgearbeitet ist und auch im Untertitel anklingt: *Autobiography of Western Man*, mit dem Motto: *De Te Fabula Narratur* – es ist Deine Geschichte, von der hier die Rede ist (Eugen Rosenstock-Huessy war in diesem Buch 1938, als Hitler schon wütete, ganz allein Subjekt der deutschen Geschichte!);

 dann als Trajekt, das heißt als schon vielfach bewährte Lehre in der Soziologie mit den zwei Bänden *Die Übermacht der Räume* 1956 und *Die Vollzahl der Zeiten* 1958 (die Freundschaft mit Franz Rosenzweig, der am 10. Dezember 1929 gestorben war, erwies sich in vierzigjähriger Wüstenwanderung als stärker, als die Wut gegen den verlorenen Ersten Weltkrieg, die sich in Hitlers Schergen und Truppen austobte);

schließlich als Objekt in dem ebenfalls zweibändigen Werk *Die Sprache des Menschengeschlechts* 1963/64, denn da ist der Träger des Wortes schon so alt und lebenssatt, daß er die Summa zieht. Es soll einen der Umfang der Bände nicht täuschen, daß dennoch hier die Lehre in Form des Samens hinterlassen wird.

Ich bin gewiß, daß die Lehre von Präjekt, Subjekt, Trajekt und Objekt als die eigentliche Hinterlassenschaft der Weltkriegsrevolution anerkannt werden wird.

Dein Vater

**NEUNTER BRIEF:**

**DIE WIEDERVERHÜLLUNG ALS OFFENBARUNGSMOMENT**

*Köln, den 1. Januar 2005*

Lieber Niklas,

die Soziologie von Eugen Rosenstock-Huessy in zwei Bänden zählt 335 + 774 Seiten, zusammen 1109 Seiten. Die Lesezeit mag – bei drei Minuten für eine Seite – also gerechnet sein als 3327 Minuten, das heißt 55 Stunden und eine halbe.

Wenn man den Einteilungen des Werkes folgt und auf einmal nicht mehr als einen Abschnitt liest, dann ergeben sich folgende Tage:

*Vorwort*

*Einleitung zu beiden Bänden: Die Freiheit*

*Der Eintritt in das Kreuz der Wirklichkeit:*

*Vergegenwärtigung der Soziologie*

*Die Spielräume unsres Reflexivums:*

*A. Die Spielräume jeder für sich*

*B. Die Spielräume im Zusammenhang*

*Die Lebensräume unsres Aktivums:*

*A. Die Lebensräume jeder für sich*

*1. Die Geschlechter der Menschen: Natur*

*2. Die Sprache des Menschengeschlechts: Geist*

*3. Die Lebensalter: Kultur*

*4. Die Todesüberwinder: Seele*

*B. Die Lebensräume im Zusammenhang*

*C. Die Tyrannei der Räume und ihr Zusammenbruch*

- das sind elf Lesevorgänge intensiver Lektüre für den Ersten Band: *Die Übermacht der Räume*. Wenn man sich die Zeit dafür nehmen kann und in einer Woche dreimal liest (und wiederliest), dann braucht man für den Ersten Band also vier Wochen, einen Monat.

Dann für den Zweiten Band:

*Übertritt in den zweiten Band*

*Die Wiederkehr der Erlebnisse: 1. Unser Zeitpunkt*

*2\. Wohnräume- Zeiträume*

     (das war das Kapitel, von dem mein Vater 1958

       am Familientisch erzählte!)

*3\. Festkalender*

*4\. Der Zeitgeist*

*5\. Die Regierungsperiode*

*6\. Das Zeitenspektrum*

*7\. Die Epoche,*

*8\. Wahnzeit und Währzeit: Das Kalenderminimum,*

*9\. Eine Todesanzeige*

*Übergang vom ersten zum zweiten Teil*

Das sind 9 Lesevorgänge für den Ersten Teil des Zweiten Bandes, demnach drei Wochen – mit einer Woche Pause danach: der zweite Monat.

Die Kapitel des Zweiten Teils sind alle so gewichtig, daß man unbedingt nicht mehr als eines auf einmal lesen sollte:

*Stamm: Ein Toter wird ins Auge gefaßt*

*Reich: Ewigkeit schwingt über ihnen Kreise*

*Volk: "Harret, daß ich höre, was der Herr sagt!"*

*Publikum: "Singe mir, Muse, den Mann!"*

*Der Herr der Äonen*

*Das Erste Jahrtausend: Gott ist Mensch geworden*

*Das Zweite Jahrtausend: Die Welt wird revolutioniert*

*Das Dritte Jahrtausend: Der Mensch soll vollzählig werden*

Das sind 8 Lesevorgänge für den Zweiten Teil des Zweiten Bandes, auch hierfür ist ein voller Monat nicht zu wenig, wenn man die Lektüre nicht zu flach lassen will.

Und auch die Kapitel des Dritten Teils sollen jedes für sich gelesen werden:

*Die Vergangenheit der Masken*

*Die Gegenwart der Tempel*

*Die Schule der Genies*

*Die Zukunft der Propheten*

*Die Umkehr des Worts*

*Auf den Wegen des Glaubens: Eine Kirche aus Heiligen*

*Heilige, Revolutionäre, Nihilisten*

*In die Räume der Hoffnung: Eine Welt aus Revolutionen*

*Die Bemannung der Hochschule: Scholastik, Akademik, Argonautik*

*Vor den Zeiten der Liebe: Eine Gesellschaft aus …?*

*Wiederverhüllung*

*Im Kreuz der Wirklichkeit*

12 Lesevorgänge – wieder ein Monat.

Man kann also das ganze Werk bei solchem Rhythmus in vier Monaten lesen, ein Monat für den Ersten, drei Monate für den Zweiten Band.

Die 55 einhalb Lesestunden also verteilt auf vier Monate. Ein Pensum, das plötzlich praktisch und vorstellbar wird!

Wenn ich mir aber einen Volkshochschulkurs vorstelle, wo anderthalb Stunden für ein Treffen gesetzt sind, von denen die Hälfte mit Hören zu füllen ist, dann braucht man für das ganze Werkt etwa 120 Treffen – also zwei Jahre.

In dem Rhythmus von vier Monaten habe ich das Werk 1963 zum erstenmal gelesen. Weitere vollständige Lektüre habe ich mir für 1966, 1969 und, mit genauen Daten, vom 24.12.1978 bis zum 11.3.1979 (also nach Davids Geburt und dem Vortrag über den *Stern der Erlösung* Franz Rosenzweigs am 17. Oktober 1978 im Forum der Volkshochschule) angemerkt. .

Die Lesezeit behandelt nun Rosenstock-Huessy als Lehrzeit, als Lehrstunde, innerhalb derer alles noch verschiebbar ist! Es traf mich wie ein Stoß, als ich auf Seite 565 des Zweiten Bandes las (das ist die Seite 900 des ganzen Werkes):

*Diese Entdeckung krönt unsere Diskussion der Sprache im ersten Band. Sie ist die wichtigste Umkehrung des landläufigen Aberglaubens über "Sprache". Der Leser wird begreifen, daß ich sie an diese innerste Stelle des Werkes verlegt habe, damit sie nur der finden kann, dem an diesem Buch etwas gelegen ist, und der also bis an diese Stelle gelesen hat. Denn dann wird er um das Wort auf dieser Pilgerfahrt unseres Geschlechts selber gezittert haben. Und wenn wir zittern, beginnen wir erst die ungeheure Umkehr durch die Sprache zu wittern. Daß wir auf uns selber zukommen dürfen, ist das Ungeheure des Sprechens. Bekehrung, Zukunft, Advent hat also diesen höchsten Sinn: wir kommen auf uns selber zu. Wenn aber mein eigenes Wort auf mich zukommt und alles, was vorher von mir zu gelten schien, aus den Angeln heben kann, dann schafft mich das Wort um, und ich werde erst unter seinem im Antlitz Gottes auf mich zurückfallenden Klang mein ureigenstes, vor-adamisches, besseres Selbst. Gerade mein "alter" Adam entpuppt sich als der "sekundäre". Der alte Adam ist bloß meine Verpackung in mein Zeitalter. Denn angesichts meines Gegenüber hebt mich mein eigenes Wort aus den Angeln.*

Einen so schamhaften Umgang mit mir als Leser hatte ich noch nicht erlebt!

Über dem 5. Abschnitt des Dritten Teils des Zweiten Bandes steht als Überschrift: *Die Umkehr des Worts* und darin der Abschnitt b) mit der Überschrift: *Das Heiligtum*.

Mit anderen Worten: Eugen Rosenstock-Huessy verlegt das Heiligtum des Menschengeschlechts in den Vorgang: *Sprache schafft Gesichter in Antlitze um (*S. 561). Dieser Vorgang ist weder ortsgebunden wie in Tempeln und Kirchenräumen noch gebunden an bestimmte vorhersehbare Zeiten, wie sie jeder Festkalender in Erinnerung bringt.

Als Beispiel für die Umkehr des Worts bringt Rosenstock-Huessy – in den Absätzen vor dem oben schon zitierten – das Psalmgebet:

*Unterredner hören, was gesagt wird, ganz gleich, von wem von ihnen es gesagt wird. Wenn die Gemeinde und der Geistliche sich im Gesang der Psalmverse abwechseln, dann vernehmen sie aus des anderen Munde das, was sie selber gesagt haben, abgewandelt wieder. Das ist im Brevier und Chorgebet und im Psalm der vielgebrauchte Ausdruck des "Parallelismus membrorum". Nicht das ist der Sinn dieses Parallelgehens von je zwei Vershälften, daß ich zweimal dasselbe sage, sondern daß du mir und ich dir dasselbe sagen dürfen, damit wir beide vernehmen und im Hören das Sagen, im Sagen das Hören so vorherrschen, daß mir der Segen meines eigenen Wortes widerfahren kann. Ein junger Laie sagte zu seinem Priester: "Gelobt sei Jesus Christus." Der stutzte, schwieg, aber dann sagte er mit Anstrengung: "In Ewigkeit Amen." Da gelang die Umkehr des Worts!*

Als Beispiel, das Dir Erlebtes anrühren mag, setze ich die ersten Verse des Psalms 103 hierher:

Der Erste sagt, der Zweite hört:

*Lobe den Herrn meine Seele,*

jetzt geschieht die Umkehr des Worts, indem der Zweite sagt und der Erste hört:

*und was in mir ist, seinen heiligen Namen!*

Der Erste empfängt in dem, was der Zweite sagt, sein eigenes Wort zurück! Und kann aus diesem gewonnenen Atem weiter sagen:

*Lobe den Herrn, meine Seele,*

Der Zweite antwortet nun anders:

*Und vergiß nicht, was er dir Gutes getan hat:*

Der Erste versteht:

*Der dir alle deine Sünde vergibt,*

der Zweite ebenso:

*und heilet alle deine Gebrechen,*

der Erste jubelt:

*der dein Leben vom Verderben erlöst,*

der Zweite kann es noch deutlicher sagen, es ist aber dasselbe:

*der dich krönet mit Gnade und Barmherzigkeit,*

der Erste versteht jetzt, was mit ihm geschieht:

*der deinen Mund fröhlich macht,*

der Zweite lenkt den Blick auf die Schöpfung:

*und du wieder jung wirst wie ein Adler.*

Der Psalm singt die Umkehr des Worts – sie geschieht bei allen Unterredungen, wo Zwei sich aufeinander einlassen können, der eine dem anderen das Gehör zum Wort umwendend.

*Meine leiblichen Werkzeuge, die Fresse, die Visage, die Lefzen, das Gebiß in Mund und Angesicht und Lippen und Wangen verwandelt zu finden, ist die Erfahrung der im Widerhall des Gebets aufeinander hörenden Gemeindeglieder. Der Parallelismus membrorum ist also nichts "Hebräisches" oder Orientalisches, wie die Materialisten des Wortes behaupten. Sondern als Israel aus Ägypten zog, mußte es auf einer Sprechweise bestehen, in der die griechische Ketzerei des "Denkens" und die skythische des tierischen Gebrülls beide unmöglich blieben. Fichte und die Nazisprechchöre sind beide gleich schreckliche Sünden gegen den Geist der Sprache. Herr Fichte, dessen Ich sein Nicht-Ich verschlingt, und der Massenchor, der sich selber nicht vernehmen kann, sind beide außerhalb der Umkehr des Leibes dank dem Wort. Sie sind beide bloß "Er" und "Es".*

Und nun kommen die Sätze, die ich Dir heute besonders ans Herz legen möchte:

*Wo aber gehört wird im Sprechen und gesprochen wird im Hören, da liegt der Mensch mit allen seinen Sinnen auf dem Fittich des zu ihm selber und in ihn hineindringenden obgleich von ihm geäußerten oder angestimmten Worts. Denn er stimmt den Ton an, der nun ihn selber zum Tönen bringt und ihn einstimmig macht und ihn umstimmt und bestimmt in Einmut mit denen, die ihm gleich und doch ihm ungleich das Wort abwandeln. Der Parallelismus Membrorum ist also nicht Gedankenführung des Psalms, sondern Einführung meines eigenen Leibes in das Reich meines Wortes. Dadurch, daß ich die Hälfte der Zeit respondierend, antwortend höre, schwingt mein Leib als Teil des Zwillingsleibes, und alle meine Glieder und Sinne werden mir neu geschenkt, weil sie sich nun umgekehrt verhalten als in meiner sensualen Isolierung vorher. Sie hatten aus mir herausgeführt, nun führen sie in mich hinein.*

Vorher war von der Umkehr so die Rede:

*Das volle Wort, bei dem wir einander ansehen, macht aus kauen begreifen, aus atmen inspiriertsein, aus hören vernehmen und Vernunft, aus riechen spüren, aus schmecken Sinn und sinnen, aus fühlen empfinden. … Nämlich es wird im Hinblick auf den einheitlichen Prozeß der Anrede, des Ansehens, des Antwortens, des Entsprechens, des Korrespondierens, des Responsoriums, des Erwiderns verstanden. Wo ich vorher für mich kaue, da begreife ich nun, was auf mich zukommt und als Wort in mich eindringt. Wo ich vorher sah, da schaue ich nun dank des Anblicks des Auges des mich Anblickenden, was er in mir erblickt. Alle geistigen Akte drehen die Richtung des Leibesaktes, der zugrunde liegt, um. Auch mein Herz schlägt nun erst in mich hinein. Statt vom Akteur in die Welt, gehen sie vom anblickenden und antwortenden und erwartungsvollen und gehorsamen und geduldigen Hörer auf den sich Verlautbarenden hin. Nur so wird statt des "Laut gebenden" Hundes der sprechende Mensch in uns hineingeboren. Der Sexualtrieb, der von mir nach außen rast, wird durch die mich anblickende Geliebte zur Liebe, die ich empfange. Es gibt unter Menschen, die miteinander sprechen, nirgends mehr die bloße "Welt", das bloße Geschlecht. Heut sprechen die Seelen oft am wirksamsten im Briefe.*

Mit diesem schmerzlichen Stichwort, an dem Du die Bedeutung dessen, was Dich gerade heimsucht, ermessen magst, schließe ich diesen neunten Brief.

Dein Vater

**ZEHNTER BRIEF: NAMEN, WÖRTER, DINGE**

*Köln, den 4. Januar 2005*

Lieber Niklas,

was ich bei keinem anderen Autor so deutlich gelesen habe und was grundlegend für das Verständnis der Soziologie ist, ist die Bedeutung der Namen.

"Lange bevor es Gesetze gab, mußte man das Kind beim rechten Namen nennen. Recht und Unrecht reichen daher bereits in die tiefste Schicht gegenseitiger Anrede zurück. Wem wir nämlich einen Namen geben, den können wir damit entweder exkommunizieren oder, umgekehrt, naturalisieren. Namen und der Ton, mit dem sie genannt werden, schließen ein oder aus, billigen oder verwerfen. Die Juristen bis Hitler hatten das vergessen. Das hatte einen guten Grund. Der Grund war die Trennung in öffentliches und privates Recht. Dadurch begann der Mensch privatim als namenloses Naturwesen. Aber Kain schuldete Abel das Leben, weil er ihn angeredet hatte. Gegenseitige Anrede ist die gemeinsame Grundlage allen Rechts, ob privat ob öffentlich. Hitlers Ausrottung der jüdischen Deutschen verwertete übrigens die Unwissenheit der deutschen Gebildeten hinsichtlich dieser Grundlagen. Sie hielten das jüdische "Blut" für wirklicherals den deutschen Namen. Heute müssen die Deutschen bei Fremden um ihr Recht betteln gehen. Da berufen sie sich nun auf alle möglichen Namen: Menschen, Europäer, Christen. Namen, auch "Deutsch" gelten eben nur im gegenseitigen Verkehr. Nur im Gespräch mit allen anderen Völkern bei ihrer Arbeit wird es deshalb wieder ein deutsches Volk geben können, aber nicht in den romantischen Konventikeln der deutschen Märchen – die bekanntlich den Brüdern Grimm aus welschem Hugenottenmunde zukamen – oder in der Beschwörung germanischer Götzen, welche gerade von den Helden des "Deutschtums" gefällt und verbrannt wurden.^1[1]^ "Deutsch" hieß nämlich nur das Heer des allerchristlichsten vom Papst gesalbten Königs."(*Die Übermacht der Räume*, S. 170)

"Namen sind dazu da, bei allen Völkern zu gelten. Es gibt also seit je die Universalsprache der Menschheit als Sprache der Namen. Bismarck und Lincoln bedeuten überall etwas anderes. Erst alle ihre Bedeutungen zusammen "sind" sie als geschichtliches Wirklichkeiten.

Daher ist die Gewalt der Namen nicht innerhalb einer einzelnen Gruppensprache erkennbar. Weil die Schulgrammatik immer nur von einer Sprache handelt: Deutsch oder Latein oder Englisch, hat sie den Namen ihren Platz bei den Hauptworten angewiesen. Die Folge ist, daß die Namen, diese Universale Sprache des ganzen Geschlechts, als bloße Worte innerhalb derselben Sprache gelten." (S.173f.)

"An dieser Stelle ist der Namenszwang, der alle "Muttersprachen" überflutet, als Grundsatz festzuhalten: Der Sinn jedes Namens ist ein Ausgleich zwischen Selbstbewußtsein und Fremdvorstellung! Also verbindet jeder Name zwei Sprachwelten; im Ansatz ist mithin jeder Name ein Friedensschluß zwischen Menschen verschiedener Zunge." (S. 174)

"Darum lautet der Grundsatz der ersten Sprachschicht, der Namen: Wer einen Namen trägt, wird zu einer anerkannten Partei innerhalb der Gesellschaft. Vater, Mutter, Sohn und Tochter sind die ersten Parteinamen der Geschichte. …Jeder weitere Name setzt mich in eine weitere Gruppe: Name, Vorname, Berufsname, Doktorgrad gruppieren mich." (S.175)

"Die Namen sind die Urwege der Sprache. Sie liegen dem Wortwerden, dem Begriffwerden und vor allen Dingen, sie liegen den Fürworten ich, du, er, es, sie vorauf.

So bedürfen sie nicht der Deklination und Konjugation; denn Name ist im Anruf Hauptwort und Zeitwort in einem. Der Vokativ gibt im Tonfall zu erkennen, ob befohlen, verboten, gefragt oder gebeten wird. Der Name ist das klangvollste und klangreichste Element jeder Sprache, weil alle Sätze in ihm ohne weitere grammatische Formen gesprochen werden können. Während es bei den Hauptworten und Beiworten auf die Fälle ankommt, der, des, derer, dem usw., kommt bei den Namen alles auf den Tonfall an. In den Namen leben daher die Tonfälle der Völker. Und diese fallen nirgends mit den Sprachgrenzen der Nationen zusammen! Tatsächlich sind die Namen plus Tonfall bereits eine richtige Sprache. Die erste Sprache ist mithin rein thematisch ohne Morphologie, aber mit reichsten Kadenzen und Klangfarben." (S. 176f.)

Wir alle, lieber Niklas, sind auf diesen Urwegen der Sprache lange gegangen, als wir nämlich nur mit Namen angeredet wurden, als wir die Namen lernten, aktiv und passiv.

"Der sogannte "Mensch", von dem so oft die Rede ist, kommt also erst hinterher zustande, nachdem ihm schon kraft seines Namens sein Platz in der Gesellschaft eingeräumt wird." (S. 175)

Ich habe, seit ich das gelesen habe, mich darin geübt, diese erste vollständige Sprache ganz und gar gelten zu lassen und jedes Gespräch mit der Anrede zu beginnen, damit der andere, wer auch immer es ist, erfährt, daß das Platzeinräumen mit Name und Tonfall immer wieder neu geschehen muß.

Bedenk, wie folgenreich für jeglichen Umgang diese Wahrheit ist!

Die Ordnung der Soziologie Eugen Rosenstock-Huessys ist aber am ehesten zu erschließen, wenn die Glutpunkte der Namen in dem Werk selber als solche wahrgenommen werden. Das ausführliche Namensverzeichnis besonders am Ende des zweiten Bandes gibt darüber Aufschluß. Um Dir einen Begriff von der Dichte der Namen zu geben, schreibe ich hier die ersten sechzehn:

*Aaron, Abailard, Abraham, Achilleus,*

*Adams, Henry, Adams, John Quincy, Aischylos, Akbar, Kaiser in Indien,*

*Alberoni, Kardinal, Alexander d. Gr., Amenophis III., Amon Ra,*

*Ampelius, Rhetor, Ananias aus dem Buch Daniel, Angilbert, Anselm von Canterbury*

Dein Vater

 **ELFTER BRIEF: DIE GESETZE**

*Köln, den 9. Januar 2005*

Lieber Niklas,

jetzt ist der Dreikönigstag vorüber, eben haben wir den Tannenbaum abgebaut – draußen ist schon fast frühlingshafte Luft: das Neue Jahr hat wirklich angefangen.

Und damit wächst auch die Erwartung Deiner Rückkehr!

In dem elften Brief möchte ich die Gesetze aufführen, die Rosenstock-Huessy gefunden und formuliert hat. Damit sind nicht die Gesetze gemeint, die politischer Natur sind und daher geändert werden können. Gemeint sind gesetzmäßige Zusammenhänge, deren Nichtbeachtung Folgen nach sich zieht.

Mir schweben vor:

*1.das Gesetz der Technik (I, S. 80),*

*2\. das Erste Sozialgesetz (I, S.111),*

*3\. die Lebensalter (I, S. 207)*

*4\. der Betrieb und die Ehe (I, S.230, 242, 250)*

*5\. Wer spricht, wird abgewandelt (I, S.312)*

*6\. der Zwölfton des Geistes (II, S.57),*

*7\. die Regierungsperiode(II, S.91),*

*8\. das Zeitenspektrum (II; S.136),*

*9\. die übernatürliche Auslese deiner Feiertage (II, S.143),*

*10\. die vier Antiken und die vier Evangelien*

*(II, S.155, 173, 202, 212, 276, 283)*

*11\. die europäischen Revolutionen (II, S.638),*

*12\. der Irrtum des Lohnbüros (II, S. 712).*

1\. DAS GESETZ DER TECHNIK

Über das Gesetz der Technik haben wir schon öfter gesprochen, wenn ich erklären wollte, warum die Einführung einer technischen Neuerung wie Computer, Fernsehen, Digitaltechnik usw. jedesmal auch in das Leben zuhause einwirkt. Es lautet: *Jeder Fortschritt der Technik erweitert den Raum, verkürzt die Zeit und zerschlägt die Gruppe, mit der wir bisher gearbeitet haben.*

"Daß mir also meine Arbeitsgruppe zerschlagen werde, gerade das habe ich zu erwarten. Wenn es jedem gewiß viel bedeutet, mit wem er arbeitet, so ist es unter der Herrschaft der Technik doch noch bedeutsamer, daß von vorneherein die Zerschlagung dieser Gruppe feststeht. Denn daraus folgt, daß wir Energie aufspeichern müssen, um trotz der Gruppe, in der wir gerade funktionieren, auch noch künftiger Gruppen fähig bleiben! Diese Speicherenergie aber ist eben unsere Bildsamkeit, unsere Knetbarkeit. Derselbe Zug also, der den Individualisten an uns hippokratisch dünkt, daß wir als Arbeiter und Verdiener nie "niemals" und nie "immer" sagen können, rettet uns das Leben und erhält die Gesellschaft in technischer Leistungskraft. Die irdische Seite unseres Daseins muß also unausgesetzt umknetbar bleiben." (S. 81f.)

"Den Handgriffen der Handwerker, den tradierten Gewerben schuldete man lebenslänglich die Treue. Der Industrie schuldet man gerade umgekehrt die Willigkeit, jede neue Produktionsweise unverzüglich ohne Widerstreben anzunehmen. …

Die Neidlosigkeit meines Abschieds von meiner Gruppe ist meine industrielle Leistung. Die Umknetbarkeit meiner technischen Einspannung ist meine produktive Brauchbarkeit" (S.82f.)

"Wer zugibt, daß er dem Massenmenschen sein tägliches Brot verdankt, der wird wohl nicht mehr über die Masse philosophieren, sondern wird sie – selbst Teil der Masse – zu dosieren streben, in sich selber und in seinen Mitarbeitern. Und er wird sich mit diesen ewig wechselnden Arbeitskräften der technischen Welt in seiner eigenen Welterfassung solidarisch fühlen." (S. 84)

Wer etwa Klavier spielt, befindet sich in einer vorindustriellen Epoche – da muß man den Handgriffen lebenslänglich die Treue halten. Und doch haben gerade einige Komponisten des 20. Jahrhunderts das Klavier vorgeführt als Instrument, mit dem man Neues ausprobieren soll.

Es ist nicht ganz leicht, die Tragweite dieses Gesetzes zu ermessen, vielleicht weil es sich so einfach anhört.

Ich kann Dir sagen, daß zum Beispiel die Einführung der Datenverarbeitung bei der Volkshochschule ohne das Wissen um dieses Gesetz passiert ist und daher das Zerschlagen der Gruppen von Mal zu Mal wieder schmerzt, ohne daß die zurückgehaltene Energie für den immer bevorstehenden Wechsel genährt oder gelobt würde.

2\. DAS ERSTE SOZIALGESETZ

Es lautet: *Jeder tue alles; alles muß wenigstens von einem getan werden; beides zusammen ist das gesellschaftliche Dogma.*

"Der Grundsatz der Völker durch alle Zeiten aber lautet: Jedermann muß alles zu tun aufgefordert werden; Einer muß für jede bestimmte Leistung bestimmt da sein. Wir alle sollen alles tun; aber es ist unbestimmt, ob wir es tun. Deshalb muß Einer es bestimmt tun, auch wenn er es vielleicht schlechter tut als die Freiwilligen und gelegentlichen Täter.

 Die Liebe pflegt besser, der Weise denkt besser, der Glaube ficht besser als die Berufspfleger, die Berufsphilosophen, die Berufssoldaten. Aber das hilft uns nichts. Doppelt wird für jede gesellschaftliche Leistung vorgesorgt. Alle gesellschaftlichen Ordnungen werden an diesem Gesetz gemessen: Mindestens einer immer und alle manchmal. Die Ordnung, die dem nicht entspricht, muß fort." (S. 112)

"So gelingt es uns nun, drei Stufen wertvollen Handelns – vom Stümper sehe ich ab – zu unterscheiden:

*Wer spielt, entdeckt eine neue Situation, aber unbekümmert um die Folgen.*

*Wer beruflich handelt, wird zum Meister über alle Konsequenzen in einer bekannten Lage.*

*Wer lasterhaft handelt, wird zum Knecht seines Handelns*." (S. 114)

"Es ist also das erste Sozialgesetz, daß Volk und Amt einander bewundern müssen, sonst sterben beide. Wir wollen diese Beziehung nicht mit dem Wort Liebe belasten. "Bewunderung" ist angemessen. Der spielende Mensch muß die Abwandlungen bewundern, die seine Spielart ergänzt; der amtierende muß die Spielart bewundern, die, weil noch nicht festgelegt, mit seiner beruflichen Tätigkeit spielen darf. Klerus und Laie, Gelehrter und Forscher sind nur solange wertvoll, als sie ohne Selbstbewußtsein bleiben; und das sind sie, solange sie für ihren Gegenpol eingenommen bleiben und es auf ihn abgesehen haben. Der Berufsmensch trachte sich überflüssig zu machen wie jener berühmte Sparkommissar, der als letztes sein eigenes Amt abschaffte. Wer es so auf sein Gegenstück absieht, wird seltsamerweise unentbehrlich. Das sieht man gut am Verhältnis von Berufssoldat und Freiwilligen, denn gerade das kann krank oder gesund sein.

Umgekehrt bleibt jeder beliebige Volksgenosse menschlich nur dadurch, daß er beachtet, wie seine gelegentliche unverantwortliche Spielerei unablässig durch dauernde Verantwortung d.h. durch Ämter, ergänzt werden muß."(S. 115f.)

Hier hast Du in Kürze, warum ich an die Volkshochschule gegangen bin, um als Musiker leben zu können! Und warum ich erleben muß, daß gerade das nicht anerkannt wird – ich fürchte, daß das strenge Wort: *daß die Ordnung, die solches gegenseitige Bewundern nicht mehr achtet, fort muß*, auch für die Volkshochschulen gilt, die in der Bundesrepublik Deutschland zwischen 1946 und 1990 das Interim des Waffenstillstands nach dem Zweiten Weltkrieg artikuliert und benutzt haben.

3\. DIE LEBENSALTER

"Die Kunst, alt zu werden, besteht in dem labilen Schwingen zwischen den Eigenschaften der verschiedenen Altersstufen." (S. 205)

"Die Lebensalter treiben also in jedem Menschenkind Anlagen hervor, die sich gegenseitig ausschließen, die somit vom Menschen einen unaufhaltsamen Wandel verlangen. Kein Mensch wiederholt eintönig durch sein Leben ein Stück der Kulturordnung. Was er vielmehr wiederholt, ist das Menschenleben. Die Gestalt dieses Menschenlebens ist die einzige wichtige Kulturform, neben der alles andere wie Verpackung, Schutz, Hülle, Verschnürung oder ähnlich wirkt. "Träger" der Kultur ist also einzig der Mensch; er ist es, indem er zugleich in ihrer Bejahung die Wirklichkeit bestätigt. Er bestätigt sie: Denn die Formen dieses Menschenalters erfindet er nicht, sie überkommen ihn. Er erlebt sie. Seine Kulturaufgabe ist einzig, diese Altersstufen mit dem für sie schicklichen Leben zu erfüllen." (S.206)

*Die Lebensalter*

|                             | Das Kind      |                      |
| Der Knabe 7-15              |               | Das Mädchen 7-15     |
| Der Lehrling 15-21          |               | Die Braut 15-21      |
| Der Wanderer 21-28          |               | Die junge Frau 22-28 |
| Der Mann 28-35              |               | Die Mutter 28-35     |
| Der Vater 35-49             |               | Die Frau 35-49       |
| Der Meister 49-63           |               | Die Hausfrau 49-60   |
| Der Großvater, Lehrer 63-70 |               | Die Großmutter 60-70 |
|                             | Greis und Ahn |                      |

(S. 207)

"Die Stufen des Geschlechts, Freier, Mann, Braut, Mutter, die wir schon kennen als Schicksalsstufen, werden mithin überkreuzt durch die Kulturstufen. Hinter dem Manne her entfaltet sich auf diese Weise der Vater. Nach der Mutter ersteht die Gestalt der Frau und Hausfrau. Es ist bezeichnend, daß die Weise des Mannes: Spannkraft, Eroberungslust, Erwerbssinn, Ellenbogen, sich ausbildet vor der des Vaters, Lehrers, Beraters, Vormundes, Nachbarn, kurz der Art dessen, der nach den Jahren kämpferischer Durchsetzung wieder so weit zu Atem gekommen ist, daß er mitteilen kann. Wenn die Kinder zwölf bis vierzehn sind, brauchen sie den Vater oder er sie und ihresgleichen. Und ebenso wichtig ist, zu sehen, daß die Hausfrau der Typus ist, der sich gemeiniglich erst ausbildet, nachdem die der Mutterschaft geweihten Jahre vorüber sind.

Deshalb ist unsere mechanische "Gleichberechtigung" von Mann und Frau nicht durchdacht. Eine Frau von 45 Jahren braucht laut unserer Tabelle das "Studium", nicht aber die Braut mit 18. Natürlich ist hier in tausend Einzelfällen – Kinderlosikgkeit! – das Leben anders. Aber auch dann wird es sich in seiner Weise der Wirklichkeit anzuähneln suchen.

Die Unverheirateten erleben die Aufgaben der Altersstufen in ihrer Art genau so in ihrem Verhältnis zum Beruf. So konnte das Wort Jüngling in den letzten Jahrzehnten einfach aussterben und durch die Berufsworte Arbeiter und Student verdrängt werden. Die beiden in unserer Tabelle aufgeführten Typen des "Ehemannes" und der "jungen Frau" sind mehr als geschlechtliche Stufen, da sie ebenso im Berufsleben dieser Jahre wie in der Ehe nach Verwirklichung drängen.

Natürlich bleiben viele Menschen auf gewissen Lebensstufen hängen und gehen daran zugrunde. Die folgende Zahlentabelle (s.o.) gibt an, welche Stufe innerhalb des angegebenen Jahrsiebents erreicht zu werden pflegt. Die frauliche Entwicklung des Weibes ist um ein volles Jahrsiebt gegen die männliche voraus.

Die Siebenperiode ist bei den einzelnen Menschen oft abgewandelt in sechs-, acht- oder neunjährigen Zyklen. Bei schwachlebenden Menschen ist sie entsprechend schwach wahrnehmbar.

Die Tabelle bedarf mancher Ausfüllung durch die Lebenskunde des Lesers." (S. 206f.)

Diese Sätze stehen heute so deutlich und glühend in mir wie als ich sie zum erstenmal las! Ich hoffe, daß eines Tages die Abwandlungen des Bestätigungstriebes (*Erhaltung des kultivierten Selbst*) in meinen Gedichtbänden, Kompositionen und Bildern ablesbar wird.

"Das enspricht Goethes Grabinschrift:

Als Knabe verschlossen und trutzig,

Als Jüngling anmaßlich und stutzig,

Als Mann zu Taten willig,

Als Greis leichtsinnig und grillig! --

Auf deinem Grabstein wird man lesen:

Das ist fürwahr ein Mensch gewesen!" (S. 209)

4\. DER BETRIEB UND DIE EHE

"Die Psychologie hat die Dreiheit der Kategorien von Denken, Wollen, Fühlen aus einer künstlichen Einengung der menschlichen Seele auf die "rein geistigen" Werte des Wahren, Guten, Schönen gewonnen. Sie hat damit statt des Gemeinwillens, der Kulturzusammenhänge und der Liebe einzig die "objektiven" Ideale des Kopfmenschen, des philosophischen "Individuums" zur Erkenntnis des "Menschen" herangezogen. Die Seele aber hat

ein Vermögen zur Begeisterung für Werte und zur Vergeistigung,

ein Vermögen zum Kampf und zum zweckmäßigen Handeln,

ein Vermögen zum Leiden und Erleben von Gewordenem,

ein Vermögen zum Lieben und zum Schaffen von Werdendem." (S. 230)

"Industrie ist begrifflich eine auf unausgesetzte Produktionsänderung aufgebaute Produktion. Das Ganze der Industrie ist daher das einzige Bleibende in der Erscheinungen Flucht." (S. 240)

"Arbeit ist das nur Wiederholte. Wo immer etwas noch nicht durchschaut und wiederholt wird, ist es mehr als Arbeit. Nun leidet alles Wiederholte in der Welt an einem Erzgebrechen: Es kann sich gegen eine Veränderung der Umwelt nicht schützen. Dem Arbeiter, soweit er nur arbeitet in dem von uns hier gegen des Ingenieurs Tun festgelegten Sinne, bringt jede Änderung der Betriebslage eine Gefahr, gegen die er machtlos ist. Jede technische Verbesserung bedroht ihn, denn sie kann ihn überflüssig machen. So wird nur in der Antithese Ingenieur-Arbeiter die Spannung sichtbar, die über die Arbeit sich legt: Sie kann jeden Augenblick überflüssig werden.

Daher widerstreitet es dem Interesse des Arbeiters, seine Leistung zu beschleunigen. Dieser eine Leistungsauftrag stellt die einzige Zeitspanne dar, für die er seines Arbeitsplatzes leidlich sicher ist.

Dies aber ist nur die halbe Wahrheit über die Selbstbehauptung des Arbeiters im Betriebe. Die andere Hälfte ergibt sich, wenn neben den Ingenieur der Betriebsleiter, der Manager tritt, und der Verkäufer, der die Produkte an den Markt leitet.

Der Betriebsleiter vermittelt zwischen dem technischen Fortschritt durch die Ingenieure, den bestehenden Arbeitsgruppen im Betriebe und den Geschäftsleuten auf dem Markt. Und diese industriellen Koordinaten gliedern

|                | Manager   |
| Arbeitsgruppen | Ingenieur |
|                | Verkäufer |

zu einem Kreuze, in dem die Zukunft technisch, die Außenwelt kaufmännisch, die Werkgruppe wiederholbar von innen durch Betriebsleitung verkittet und verfugt wird." S. 241f.)

Für die vier Teile des Kreuzes hat Rosenstock-Huessy dann Formeln gefunden, die plastisch merklich machen, was ihre Verschiedenheit ist.

"Die Gewerkschaft sagt: Alle Arbeiter vertrete ich wie ein Mann, wie ihre Mutter, wie ein einziger Kontrahent. Man könnte also die Schau des Tarifvertrages formelhaft wiedergeben in der Gleichung 1= ¥. Denn der Unterschied von dem einzelnen Arbeiter und der gesamten Arbeiterschaft soll überwunden werden." (S. 242)

"Wo Schichtarbeit besteht, ist der durchgängige industrielle Produktionsprozeß allerdings leichter durchschaubar. So wollen auch wir es uns erst einmal leicht machen und den Betrieb, der 24 Stunden lang in drei Schichten arbeitet, voraussetzen.

In diesem Betrieb treten an einem Tage drei Mann füreinander ein, an den gleichen Platz für die gleiche Arbeit. Für das Lohnbüro sind die drei wie ein eiserner Arbeiter. Erst die drei gleichen der Maschine, die ja 24 Stunden laufen kann. Mithin ist die Tatsache, daß die Menschen nicht 24 Stunden "laufen", ein Mangel für das Kostenbüro. Um richtig zu kalkulieren, muß es also 1=3 schreiben, und damit wird der Arbeiter in seiner leiblichen Bedürftigkeit ein Dritteil der vom Kalkulationsbüro dem Maschinen-Inventar gleichgestellen Arbeitskraft. Hier liegt eine Wurzel bloß, weshalb allerdings die "unkräftige", d.h. die zarteste und charakteristischste und persönlichste Gestalt des Arbeiters nicht in den Betrieb eingeht. Eingestellt wird der kräftige Teil, jene Kraft an ihm, die dann auch ein anderer und noch ein anderer liefern wird. Das Zarte und Gebrechliche ist freilich unser bestes Teil, denn es ist der wachstumsfähige Herztrieb. Aber eingestellt werde ich als der, der ich bin, nicht als der Werdende.

Die Industrie verlegt alle Wachstums- und Werdeprozesse heraus aus den Betrieben – in Schulen, Lehrwerkstätten usw. Die Rationalisierung eines Betriebes besteht gerade darin, daß nur mit festen Größen gerechnet wird, d.h. beim Arbeiter nur mit seiner Kraft, während den Wert des Menschen auch seine Schwäche ausmacht. Wir müssen viel rücksichtsloser dieser Gleichung 3=1 uns annehmen als jeder Liberale seines naiven 1=1 oder der Marxist seiner 1=¥. Denn wir erobern uns erst mit dieser Gleichung den Zutritt zum Betriebsinnern; ohne politischen Idealismus der Bourgeoisie und ohne ökonomischen Materialismus des Proletariats gilt es, den Betrieb von den Werkstätten des Handwerks abzusetzen. In der Werkstatt wuchsen Meister, Gesellen, Lehrbuben. Im Betriebe hingegen funktionieren feste Größen, die eben deshalb fungibel werden.

Was geschieht der Arbeitskraft, die in Schichtarbeit Eine aus Dreien wird? Nun, sie darf den beiden anderen Dritteilen nicht die Preise verderben. Wer in Schichtarbeit steht, muß den in einer Schicht zu leistenden Durchschnitt ungefähr innehalten. Sonst verletzt er die Betriebssolidarität der Arbeitskräfte. Diese brüderliche Solidarität der Schicht ist nicht die klassenbewußte Solidarität des Marxisten und auch nicht die Brüderlichkeit aus Schillers "Lied an die Freude". Sie ist eine entsagende Selbstbescheidung wie der Gleichschritt mit einem Vordermann auf dem Marsche in Reih und Glied. Sie bedarf kaum des Bewußtseins. Denn sie entspringt gerade einem Verlöschen dieses Bewußtseins.

Dies Gefüge von Arbeitskräften ist prähistorisch in dem Sinne, daß weder mein Wille, noch mein Bewußtsein es herbeiführen, sondern es ist ja umgekehrt: Aus dem Vollzug der Arbeit in dem mir gestellten Rahmen ergibt sich überhaupt erst das Muß, in dem ich da aufgeweckt und zum eigenen Willen aufgefordert werde. Wer in Reih und Glied marschiert, der summt besser halblaut vor sich hin. Zu viel Bewußtheit und Absicht beim Einzelnen würden den guten Marsch bloß stören. So viel und nicht mehr: dann wird am mühelosesten und am längsten marschiert werden können." (S.243f.)

"Der Unternehmer, die Gewerkschaft, der Betrieb sind ja Strukturen, so unverwechselbar wie Kristalle, und sie treten mächtig allenthalben hervor, wo industrialisiert wird. Wir haben den Namen

*Unternehmer 1=1*

*Gewerkschaft 1=*¥

*Betriebsgruppe 1=3*

nun Schlüsselzahlen beigegeben, um sie voneinander abzuheben. Es bleibt uns aber noch eine Schwelle zu überschreiten.

Wir sagten vom Frieden doch, daß in ihm das Wirtschaften zweiten Ranges bleibe gegenüber unserer Erfüllung als Geschlechterwesen. Während im Krieg die Männergesellschaft umfassender als die Geschlechterordnung ist, sollte der Friede die Proportion

*Arbeit geringfügiger als Geschlechterordnung*

aufweisen." (S.245)

"Die Hochzeit ist heute verengt zum Heiratstag zweier Liebenden. Aber Hochzeiten sind öffentliche Angelegenheiten. Auf Hochzeiten der Habsburger ist ihr Reich erheiratet worden. Daß ein Liebespaar die Gemeinde mitreißen kann, so daß für alle die Zeit erhöht wird, das allein unterscheidet das Konkubinat und die "freie Liebe" von dem Liebesfrühling hochzeitlicher Machtvollkommenheit. …

Auf Hochzeiten bemächtigt sich die Liebeswelt der Arbeitswelt und überwältigt sie." (S. 246)

"In der modernen technischen Trennung von Liebesleben und Arbeitsleben hat zuerst das Arbeitsleben sich zum Souverän ausgerufen. Produktion geht vor Konsumtion. Fabriken kommen zuerst, Arbeitersiedlungen zu zweit. Die Welt ist in Produzenten und Konsumenten zerspalten, und da plant der eine Architekt eine Riesenfabrik für die Produktion, und der andere plant eine Vorstadtsiedlung aus lauter Zwergwohnungen…

Wir sollten uns nicht davon blenden lassen. Die Konsumentenwohnung und der Großbetrieb sind beide tot. Denn sie werden beide für Organisationen ausgegeben. Organisation kann aber nur der von außen geordnete Prozeß werden. …

Wohin sind die hochzeitlichen und ehelichen Energien abgewandert, fragen wir. Dazu müssen wir den Schlüssel für alles fruchtbare Leben finden und zu den Schlüsselzahlen für Unternehmer, Gewerkschaft, Betrieb, hinzufügen. Diese Sätze lauteten:

*1=1*

¥*=1*

*3=1*

Die Zeugungskraft aber ruht auf dem Satz

*1x1=1*.

Denn wo nicht zwei ein Leib werden können, wo nicht Einer und Eine selbstvergessen ineinander stürzen, da bleibt das Leben zeugungsunfähig." (S.250)

"Wie die verschiedenen Wellenlängen am Radio uns offenstehen, so müssen wir auf diese verschiedenen Wellen der 1=¥, 1=3, 1x1=1 Zeitlängen uns einlassen. Der ganze Mensch tritt also erst aus *Todesgemeinschaft, Liebesgemeinschaft, Geistesgemeinschaft, Arbeitsgemeinschaft* hervor. Sie wetteifern um seine Seele: Heer, Betrieb, Ehe, Politik, alle wollen sie ihn ganz. Aber er ist doch noch mehr als jedes dieser Aggregate." (S. 252)

Das Wissen der Gleichung 3=1 hat mich mein ganzes Arbeitsleben an der Volkshochschule Köln begleitet.

Andererseits bin ich bei den Gedanken zur Andragogik immer dem Bestreben gefolgt, den ganzen Menschen auch in den vorübergehend versammelten Menschen anzusprechen und anzuhören. Und dann hinzuweisen auf die Mächte, die Götzen werden, wenn sie einzeln anders als vorübergehend und im Wechsel Dich und mich haben wollen.

5\. WER SPRICHT; WIRD ABGEWANDELT

"Jeder Ausspruch in der Gesellschaft spiegelt eine vierfache Bewegung. M(ensch) 1 wird dadurch, daß er spricht und zu M 2 an seinem Ausspruch wird, erst endgültig als M1 erkennbar. Denn nun erst ist M1 vorüber.

Mit den "Leiden Werthers" ist der junge Goethe berühmt; aber eben deshalb hat der Junge Goethe aufgehört zu existieren. M1, d.h. der nur *Junge Goethe*, hat dank dem Ruhm seines Werther aufgehört zu existieren. So aber auch ist es mit Lotte Buff-Kestner. Sie ist nicht mehr dieselbe, seit sie als Heldin des Romans erkannt und sich selber bewußt geworden ist. Darum wird "die Unbefangene Lotte", die F(rau)1, mit dem Erscheinen des Werther abgeschlossen und tritt ins Gewesene, in das Präteritum. Ein neues Leben fing für sie und für den Verfasser des Werther an. Das Buch also legte beiden ihre grammatischen Tempora des Präteritums und des Futurum fest. Und Gegenwart, Präsens wurden beiden nun das aus den Resten des Präteritums und den Ansätzen des Futurums gebildete Kraftfeld, in dem sich Vergangenheit und Zukunft gegenseitig aufarbeiten.

Gegenwart wird daher als Kraftfeld erkannt, in das Zukunft und Vergangenheit hineinragen, weil etwas ausgesprochen worden ist. …

Diese grundsätzliche Vervielfältigung meiner Existenz durch die zwei Tatsachen, daß ich spreche und daß von mir gesprochen wird, setzt mich aus der Mathematik in die Grammatik. …

Sprechen macht uns verwandt, weil wir einander die Tempora des Präteritums, des Futurums und des Präsens mitteilen." (S. 313f.)

Lieber Niklas, Deine Briefe aus Castleton sind ein gutes Beispiel für dieses Gesetz – und wer weiß, auch der Brief, dessentwegen Du mich um ein Uhr hiesiger Zeit angerufen hast!

*Das Gesetz der Technik* – daß der technische Fortschritt die Gruppe zerschlägt -,

*das Erste Sozialgesetz* – warum es Andragogik geben muß -,

*die Lebensalter* – daß Mann und Frau sozusagen einander verpassen, wie Vater und Mutter auch -,

*der Betrieb und die Ehe* – die vier Gleichungen 1=1, 1x1=1 (2=1), 3=1 und ¥=1 -,

und: *Wer spricht, wird abgewandelt* – der Unterschied zwischen Mathematik und Tieferer Grammatik --

stehen im ersten Band "Die Übermacht der Räume". Im zweiten Band "Die Vollzahl der Zeiten" stehen die Gesetze, mit denen wir die erlebte Zeit verstehen können.

6\. DER ZWÖLFTON DES GEISTES

In großartiger Weise hat Rosenstock-Huessy die Zahl 12 erlöst.

Sie ist in die Astrologie gekommen, um die 8er-Teilung zum Beispiel der *I Ging* Grundzeichen (*das Schöpferische, das Empfangende, das Heitere, das Sanfteindringende, das Festhalten, die Erschütterung, das Wahrhaftige und das Begeisterte*) zur Zwölf zu erweitern – man erkennt an dem Vergleich der chinesischen Reihe mit der geläufigen, daß Stier- und Skorpion- und Steinbock- und Krebs-Zeichen später interpoliert worden sind.

Rosenstock-Huessy hat das Gesetz unsrer Zeitigung durch die Sprache gefunden. Der Zwölfton des Geistes lautet:

*Heiße, Lerne, Diene Singe --*

*Zweifle, Forsche, Protestiere, Harre --*

*Amtiere, Lehre, Verheiße, Stifte.*

Das ist alles schön ausgeführt und entwickelt – aber dann kommt die Umkehr des Worts in einer überraschenden Weise:

"Ich hatte die Gebote der Zeitigung oder der Vergeistigung bereits entdeckt, die ich hier vor den Leser noch einmal setze:

*Heiße        1 L*

*Lerne        2 K*

*Diene        3 J*

*Singe        4 I*

*Zweifle      5 H*

*Forsche      6 G*

*Protestiere  7 F*

*Harre        8 E*

*Amtiere      9 D*

*Lehre        10 C*

*Verheiße     11 B*

*Stifte       12 A*

Als es mir wie Schuppen von den Augen fiel. Nach der Uhr und dem Augenschein zähle ich hier diese Gebote von des Kindes Geburt bis zu des Greises Tod auf, als ob sich 1, 2, 3, 4, 5, 6, 7, 8, 9. 10 folgten, aber gegen den Augenschein ergibt sich 9 aus 10, 8 aus 9, 7 aus 8, 6 aus 7 und so weiter bis zum Anfang zurück.

Kein Kind also bräuchte zu gehorchen, zu lesen, zu dienen oder zu spielen, kein Mann zu zweifeln oder zu protestieren, wäre es nicht deshalb, weil der sterbende Mensch zum Stifter bestimmt ist. Weil wir sterben müssen, sollen uns *Name, Autorität, letzter Wille, Einsetzung, Vermächtnis* überleben. Aller nobler Geist beginnt als namentliche Überlebens-Weisheit. Wer recht gelebt hat, stirbt nicht umsonst, sondern hat die Welt so verändert, daß sie nicht mehr hinter seinen Namen zurückfallen kann.

Wer daher auf einen letzten Willen hört, wer ihn "erbt", erwirbt damit eine von dem Stifter für die Nachlebenden erworbene Eigenschaft. Daher muß das Enträtseln der Zeitigung mit dem anscheinend an letzter Stelle stehenden Gebote anfangen und diese Reihenfolge muß so auch heute unter uns adoptiert werden.

Wir Menschen haben Geist, soweit wir den Tod von vornherein überleben. "Stifte" ist keineswegs das zehnte *(?)*, sondern das erste Gebot der Zeitigung oder des Geistes. Hingegen ist "Heiße", diese an den Säugling ergehende Mahnung nur die letzte bis in die Geburtsstunde des Erben zurückreichende Vorbereitungsstufe jedes Stifters.

So also lautet die wahre Reihenfolge:

*I.*

*a. Hinterlasse, stifte, testiere; deshalb*

*b. Verheiße, prophezeie, beschwöre; deshalb*

*c. Lehre, erzähle, unterrichte; deshalb*

*d. Herrsche, regiere, gib Gesetze; deshalb*

*II.*

*e. Harre, bewähre, leide; deshalb*

*f. Protestiere, erkläre, lege dich fest; deshalb*

*g. Urteile, nimm wahr, forsche; deshalb*

*h. Zweifle, verwirf, entzaubere; deshalb*

*III.*

*i. Singe, dichte, wirb, freie; deshalb*

*j. Diene, gehorche, folge, erfahre; deshalb*

*k. Lerne, lies, sieh; deshalb*

*l. Heiße, vernimm, sprich nach.*

Damit werden wir der Zeiten in der unerwartetsten Weise Herr. Wir müssen sie nämlich umkehren. Weil wir sterben müssen, verlangt es uns über Geburt wie Tod hinaus. Im "Letzten Willen" reift der Ertrag dieses Verlangens. Er führt zur Stiftung. Er wird ja nur dadurch erfolgreich, daß sein Tod Folgen hat, die ihn, den Stifter, nicht ausradieren, sondern die ihn bestätigen und bewähren. Mit dem Erb-lasser fängt die Geschichte des Geistes an." (S. 73f.)

"Die Geschichtszeit der Menschheit rechnet also nach rückwärts, nicht nach vorwärts!

Seit Adam ist dem Menschen sein eigener Tod bekannt. Und seit Christi Geburt ist der Jüngste Tag bekannt und daher wird uns von Anfang an und seit der Mitte noch einmal geboten, die "natürliche" nachgeborene Menschheit auf diesen Endtag seiner Wiederkehr hin auszurichten. Alle Zucht ist mithin Einspielen und Einkämpfen in die Ämter *der Stifter, Propheten, Lehrer und Könige*. Mag die Demokratie proklamieren: "Jedermann ein König". Mag das Evangelium verkünden: "Jedermann ein Priester", so heißen doch eben beide Sätze klar und deutlich, daß erst Priester und dann Könige die vollständigen Menschen wären." (S. 75)

"Ob sich die wissenschaftliche Geschichte der Einsicht öffnen läßt, daß wir nicht für den Kampf ums Dasein geboren werden, sondern um den Kampf um unser Wieder-Dasein? Daß wir dazu vom Tode vorher zu wissen haben, und zwar in der Form, daß wir mindestens eine seiner Stimmen in uns laut werden lassen? Die Stimmenteilung muß uns durchtönen, dank derer *Stifter, Propheten, Lehrer, Herren, Dulder, Protestanten, Forscher, Zweifler, Sänger, Diener, Leser, Heißer* so einträchtig die Zeiten verkörpern wie C CIS D Dis E F Fis G Gis A B H die Töne." (S. 85)

Der Zwölfton des Geistes ist mir ganz durch und durch in Erleben und Wahrnehmen gegangen – ich entdeckte, daß die zwölf Bilder neben der Kreuzigung auf einem westfälischen Altar im Wallraf-Richartz-Museum den Zwölfton des Geistes darstellen, und zwar in folgender merkwürdiger Zuordnung:

*1 Heiße:*

die Verkündigung an Maria,

*2\. Lies:*

die Geburt im Stall von Bethlehem (die Weihnachtsgeschichte),

*3\. Diene:*

die heiligen Drei Könige,

*4\. Singe:*

die Darstellung im Tempel, mit dem Lobgesang des Simeon,

*5\. Zweifle:*

der Einzug in Jerusalem (Hosianna dem Sohn Davids – und gleichzeitig wird dies im Advent und am Sonntag Palmarum gelesen)

*6\. Kritisiere:*

die Kreuzabnahme mit dem genauen, die Nägel herausziehenden Prüfen, was Leben, was Tod ist,

*7\. Protestiere:*

Jesu Kreuztragung – das war sein öffentliches Bekenntnis und Wort,

*8\. Harre:*

die Grablegung – bis zur Auferstehung,

*9\. Regiere:*

die Himmelfahrt, Jesus als der König,

*10\. Lehre:*

Pfingsten, die Ausgießung des Heiligen Geistes,

*11\. Prophezeie:*

Jesus in der Vorhölle, Adam und Eva erlösend,

*12\. Stifte:*

die Auferstehung.

Du siehst: die Reihenfolge ist nicht chronologisch! Die Kreuzabnahme (oben rechts neben dem Kreuzigungsbild) kommt vor der Kreuztragung, und Pfingsten (oben rechts am äußersten Rand) kommt vor der Höllenfahrt und der Auferstehung!

Wunderbar ist die Farbwelt dieses Altarwerks als unterstützender Wink zum Zwölfton des Geistes, dessen Ursprung das Einlassen des Todes (die Kreuzigung) ist!

Wenn *ein* Kapitel ist, das ich zum sofortigen Gründlichlesen empfehle, dann dieses!

7\. DIE REGIERUNGSPERIODE

Ich möchte nicht versäumen, eine der wenigen biographischen Mitteilungen Rosenstock-Huessys in der Soziologie an den Anfang zu stellen:

"Es wird in scheinbarer Kopie der Griechen jedem Staat eine Form zugeschrieben, die entweder Monarchie, Demokratie, Aristokratie oder Diktatur heißt. Statt Demokratie heißt es manchmal auch Republik. Das Blickfeld solcher Staatstheorie erfaßt ein angeblich sich selber gleichbleibendes Raumgebiet und erörtert die in diesem Gebiet an und für sich tätige Waltung nach dem Maße der Teilnahme der Bewohner.

Diese Staatslehre rückt also den Untergang einer Regierung an die äußerste Peripherie, den sich gleichbleibenden Raum aber stellt sie in die Mitte.

Meine Erfahrung wird damit auf den Kopf gestellt. Jedes Ostflüchtlings Erfahrung wird damit auf den Kopf gestellt. Alle Amerikaner werden so in Frage gestellt.

Ich habe am 1. Februar 1933 beschlossen, in ein gerechteres Staatswesen als das hitlersche auszuwanden. Ich bat meine juristische Fakultät, deren Mitglied ich war, wenigstens wegen Revolution zu schließen. Als ich damit ausgelacht wurde, ging ich, dem Staate dorthin nach, wo er sich noch dem Recht untertan wisse.

Der Staat war damals vor meinen Augen auf deutschem Boden untergegangen. Das tun alle Regierungen. Deshalb ist die Fremdherrschaft zum Beispiel eine ewig wiederkehrende Erfahrung. "Der Raum", "das Staatsgebiet", die "Nation" sind durchaus nicht immer in der Lage, eine Regierung zu bilden. Trotzdem kommt in den Lehrbüchern der Staatslehre die Fremdherrschaft nicht vor.

Deutschland ist zur Zeit unter Fremdherrschaft. …

1944/45 haben die edelsten und tapfesten Deutschen die Hitlerpest nicht ausrotten können. 5000 von ihnen, die heut allenthalben fehlen, hat er für diesen Versuch umgebracht, zu einer Zeit, als nur noch Wahnsinnige nicht genau wußten, daß diese Männer recht hatten.

Für die Außenwelt sind sie die einzigen gültigen Deutschen von 1933-1945. Wenn man aber heut mit wohlhabenden Deutschen spricht, so werden sie meist sehr heftig, wenn man von den 5000 spricht; ihr Tun wird für durchaus unpraktisch erklärt, mit der niederträchtigen Begründung, daß sie doch umgekommen seien. Daß sie aber die einzigen Deutschen sind, um deren willen es heut noch die alliierte Provinz Deutschland statt eines Morgenthau-Deutschland gibt, wird verschwiegen.

Das heißt, es wird verschwiegen, daß naturgemäß diese wohlhabenden Leute sich nicht wundern dürfen, wenn sie nun Fremdherrschaft haben müssen, weil ihr Schicksal von den Flugplätzen der Amerikaner und Russen in Berlin gesteuert wird.

Gerechtigkeit ist eben wichtiger als alles andere. Und in jedem Lande kommt der Tag, wo nur der Fremdherrscher das Gesetz durchzusetzen vermag. Auch die Fremdherrschaft ist nur eine Regierungsperiode." (S. 92f.)

 Nun zu der Regierungsperiode:

"Der Regierungswechsel ist der Ausweg, der die Erfüllung der dringendsten Aufgabe einer bestimmten Regierung und das Abtreten eben dieser Regierung gern aneinander knüpfen will. Der Regierungswechsel ist das zeitliche Problem, das der in den Mittelpunkt seiner Gedanken rücken muß, der von dem Wunder ausgeht, daß irgendeine Regierung jederzeit gegen das Unrecht gesucht wird, daß aber keine Regierung gerecht bleiben kann, sobald sie das getan hat, wofür sie organisiert worden ist. …

Als die Sowjets Ehen von Russen mit Fremden verboten, war das genau so ein Abfall vom Recht wie jede der oben aufgezählten Teilentartungen. Denn das Recht scheint in einen Staat hinein, und die Idee, sich etwa in Ehesachen zu isolieren, ist staatlicher Größenwahn.

In jedem solchen Fall wird ein Regierungswechsel notwendig. Tritt er nicht rechtzeitig ein, so sammelt sich soviel Unrecht, daß später der ganze Staat verschwinden kann.

Kurzfristige Staatsordnungen sind nun entgegen der deutschen Fürstenstaatslegende die Regel."(S.94f.)

"Sobald die Fremdherrschaft mitgedacht wird, fallen die künstlichen Gedächtnislücken fort. Und erst mit Hilfe eines freien Gedächtnisses lassen sich alle Zustände nach der Länge der Zeit gruppieren, während derer sie "gerecht" waren." (S. 97)

Nun zu der Länge der Regierungsperiode:

"In der sogenannten Jugend sind wir von Natur Revolutionär, und in dem sogenannten Alter von Natur vielleicht reaktionär; aber doch ist das Menschenalter "Jugend" nicht einfach dreißig Jahre lang. Die ersten 18 Jahre kann sich dieser Junge ja noch gar nicht ausdrücken. Was einer also zwischen 18 und 30 tut, das ist der Ausdruck seiner vollen Jugend von im ganzen dreißig Jahren! Die Jahre 18 bis 30 zählen mithin doppelt im öffentlichen Leben, weil die Jahre 0 bis 17 gar nicht zählten.

Beachtet man andererseits den Wechsel im Lebensrhythmus zwischen Jugend und Alter allein, so würde ein Bruch alle 25 bis 30 Jahre zu erwarten sein.

Aber es kann jedermann an seiner Erfahrung mit Schulen, Vereinen, Parteien erfahren, daß ausdrückliche artikulierte, proklamierte programmatische Lebensakte doppelt so schnell sich verbrauchen wie die rein organischen Prozesse. Eine Universität braucht alle 15-20 Jahre eine Erneuerung, nicht alle 30." (S. 98)

Nach wichtigen Beispielen, wie der Vorgang des Ans-Recht-kommens durchaus oft ohne jede Brücke aus dem Recht der vorhergehenden Regierung geschehen ist:

"In den seltenen Fällen, in denen eine Regierung die Frist von 15 bis 20 Jahren überschreitet, ohne ungerecht zu werden, da vollbringt sie das, indem sie sich gründlich ändert. Eine Regierung, die sich nicht jede Halbgeneration wandelt, wird eben dadurch weniger gerecht.

Über Demokratie, Monarchie, Aristokratie läßt sich also erst dann diskutieren, wenn sie auf den gerechten Zeitraum bezogen werden. Dann enthüllen sie sich alle als bloße exzentrisch an die Regierungsfrist sich herantastende Spielregeln.

Bei häufigen Wahlen zum Beispiel scheint die Regierung viel öfter gewechselt werden zu können, als sie wirklich weggewählt wird. Die einzelne Wahl ist dann eine Art Übung am Phantom wie die letzte Wahl in Westdeutschland. Diese zahlreichen Wahlen wirken allerdings dann verwirrend, wenn sie einen Zickzackkurs begünstigen oder den sicher zu kurzen Zeitraum von 2, 3, 4 Jahren der Masse der Wähler als normale Regierungsepoche vorgaukeln wie in den südamerikanischen Republiken. Die Wahlen werden dort daher unerträglich und sind heut nur Begleiterscheinungen der Diktaturen.

Zu viele, zu häufige Wahlen greifen unter die angemessene Periode." (S.99)

"Es ist eben diese eine Tatsache: daß eine Regierung ein zeitlich befristetes Mandat hat, das mit ihr emporgetragene Rechtspostulat in die Tat umzusetzen." (S. 101)

Für die Betrachtung der Regierung in Deutschland nach 1945 ist die 15-Jahre-Frist erhellend:

1945-1961 bis zum Mauerbau in Berlin,

1961-1989 bis zum Fall der Mauer in Berlin,

1989-2004 das Verschmelzen von Ost und West --

die Katastrophe um den Indischen Ozean fordert die Aufmerksamkeit der Deutschen auf den Planeten Erde.

Ob nun eine neue Regierungsperiode beginnt, die mit den von CDU, SPD, FDP, PDS und Grünen aufgestellten Kategorien nicht mehr zu meistern ist? Das heißt: alle müssen sich wandeln?

Eine solche Betrachtung jedenfalls würde von etwas anderem ausgehen, als dem Gezänk, das derzeit üblich ist.

8\. DAS ZEITENSPEKTRUM

"Wir haben hier eine volle Stufenreihe der Grade der Lebendigkeit:

ins Leben rufend, stiftend,

liebevoll und leidenschaftlich,

lebhaft und absichtlich,

lebendig und organisch,

tot und mechanisch.

Jede dieser Haltungen verhält sich zur Zeit auf besondere Art. Die Zeitigungskraft der Epochalen wohnt im weitesten Zeitbogen: *Es wird die Spur von ihren Erdentagen nicht in Äonen untergehn.* So sagen die Helden von sich selber, und so wird es den Heiligen nachgesagt.

Die Liebenden umfangen Eltern und Kinder mit gleicher Liebe. Jedes Hochzeitsfest soll ja gerade dies Wunder bezeugen, daß wir nach vorwärts und nach rückwärts versöhnend, anhaltend lieben. Darin liegt der Unterschied zwischen dem klugen Verführer und dem Bräutigam, daß dieser wahrhaft liebt und deshalb bei den Eltern der "Braut" anhält.

Wieder kürzer ist der Wachtraum des Verstandes. Er opponiert, so sahen wir, gegen das nur Vernommene. Er lebt also auf zwei Lebensalter. Die nur Klugen zeigen das, wenn sie im Alter das Gegenteil werden von dem, was sie in der Jugend waren. Der grimme Satz*: Junge Huren alte Betschwestern* ist nur die Doublette zu "Junge Revolutionäre, alte Tyrannen". An beiden Schlagworten wird der dialektische Rhythmus des nur wachen Lebens klar.

Das Rhythmische ist nicht epochal, aber periodisch. Der Leser findet solche Perioden im ersten Bande bei den Lebensaltern analysiert. Der Zeithorizont deckt sich hier nicht mit dem Raumhorizont des Geschöpfs. Er wird kürzer.

Beim Mechanismus ist das Auseinanderfallen von Zeiten und Räumen auf die Spitze getrieben. Ein Gegenstand der Physik gibt daher Anlaß zu jener Abstraktion "Zeit" und "Raum", die den Schwindel des Idealismus erregt. Ein totes Ding hat selber keine Zeit und kriegt die Zeit von seinem Beobachter, also von außen, zugemessen, während die Dinge dem beobachtenden Verstand seinen Weltraum so zumessen, wie er das im Laboratorium oder bei der Vermessung der Erde – die ihn doch mit einschließt – bestätigt.

Raum und Zeit sind da also auf Objekt und Subjekt in vollster Spaltung verteilt. Wer keine Zeit hat, der liegt im Sterben. Und wenn Menschen keine Zeit zu haben glauben, so sind sie tödlich krank. Auch dies also ist eine Kairose, eine Erkrankung unseres Zeitsinns.

Die ganze Skala dieser Zeitmasse verdient den Namen eines Spektrums nach dem Vorbild des Farbenspektrums. Das ist nicht nur ein treffender Ausdruck, weil hier alle "Lebewesen" der Zeitigung aufgefangen werden, das leibhaftige, das lebendige, das lebhafte, das liebenswerte und das lebenspendende, sondern weil sie aufeinander innerlich bezogen bleiben." (S. 136f.)

Triffst du also einen Kranken, so kann er *mechanisch, organisch, absichtlich, leidenschaftlich und bestimmungsgemäß* krank sein. Aus unserem Versagen auf fünf verschiedenen Ebenen,

*aus Ungehorsam gegen unsre Berufung,*

*aus Lieblosigkeit,*

*aus Trotz gegen die Logik,*

*aus Taubheit gegen den Rhythmus des Leibes,*

*aus Unvorsichtigkeit gegen einen fallenden Stein*

wird das Zeitenspektrum sozusagen negativ bestätigt. Blind, taub, trotzig, kalt, Ungehorsam sind die Abwehrvorrichtungen gegen die Ansprüche, die an uns in unserer Zeit gestellt werden. In der Abwehr büßen wir also unsere Vollständigkeit ein. Jede solche Abwehr verstümmelt uns.

Aber – und damit wird das Zeitenspektrum die Hauptwaffe der Soziologen – niemand unter uns kann allein allen den Ansprüchen des Zeitenspektrums genügen. "Niemand ist gut denn Gott allein" hat den guten Sinn, daß niemand unter uns die Vollzahl der Zeiten leben kann, weil kein einzelner Mensch in diese Vollzähligkeit allein hineinreicht. Auch Jesus brauchte Mutter und Jünger dazu. Wir sind in die Unvollständigkeit hinein geschaffen." (S. 138)

In den fünf Tiefen bei *Jin Shin Jyutsu* findet man die genaue Entsprechung zu den fünf Stufen der Lebendigkeit:

Tot und mechanisch – Erste Tiefe: Magen und Milz,

lebendig und organisch – Zweite Tiefe: Lunge und Dickdarm,

lebhaft und absichtlich – Dritte Tiefe: Leber und Galle,

liebevoll und leidenschaftlich – Vierte Tiefe: Niere und Blase,

ins Leben rufend, stiftend – Fünfte Tiefe: Herz und Dünndarm

Der Magen verleibt das Tote als Nahrung dem Leben ein,

Atem und Verdauung sind unwillkürlich gesteuerte Vorgänge,

die Erregbarkeit und das Gedächtnis sind

                zu vernünftigem Handeln notwendig,

das Karma und das Empfinden für Zeit und Lebenslauf

            sind in der Vierten Tiefe (mit Niere und Blase) gesetzt,

das Herz und die geistige Verarbeitung der Erlebnisse

                          rufen ins Leben.

9\. DIE ÜBERNATÜRLICHE AUSLESE DEINER FEIERTAGE

"Gottes Zeit entquillt als Notwende aus dem Notfall, wenn immer ein Geschöpf in sich den Schöpfer Herr werden läßt über sein geschöpfliches Selbst. Daher gibt es so viele Epochen der Weltgeschichte, wie es Gottes Söhne und Gottes Töchter gibt." (S. 142)

"Wer der Kreisauer und des 20. Juli nicht gedenkt in Deutschland, der zelebriert noch den Tag der "Machtergreifung" oder den Tag des Bürgerbräuputsches oder das Sedanfest. Die übernatürliche Auslese deiner Feiertage ist ganz dein Werk. Dafür kann dir niemand und nichts die Verantwortung abnehmen. Durch dich also fängt eine Epoche an oder hört eine Epoche auf oder wird eine Epoche durchgesetzt, je nachdem. …

Den Zeitpunkt unserer Abstammung empfangen wir als die eine Koordinate unserer Bestimmung. Die natürliche Auslese unserer Zu-stammung entscheiden wir durch unsere Witterung für den Notfall." (S. 143)

Hier, lieber Niklas, bringe ich Dir Eure Namen in Erinnerung:

*Julia,*

*Caroline,*

*David Chaim Marius,*

*Niklas Sebastian Nahum,*

*Hanna Freya,*

*Simon Eugen Vladimir.*

10\. DIE VIER ANTIKEN UND DIE VIER EVANGELIEN

Hier muß ich nun innehalten – es geht über die Kraft, an diesem Tage weiter zu schreiben.

*Köln, 16. Januar 2005*

Lieber Niklas,

in den letzten Tagen bin ich nicht dazu gekommen, an dem elften Brief weiterzuschreiben. Das hat verschiedene Gründe – der wichtigste: ich bin tatsächlich an die Grenze gelangt, Dir zu präsentieren, was in der Soziologie alles steht.

Die Lehre von den vier Antiken und den vier Evangelien, die jene zueinander wenden, ist so aufregend und gewaltig, folgenreich und jauchzen-machend, daß ich unmöglich in Auszügen oder erklärenden Worten Dir das vorwegnehmen kann und will.

So setze ich an diese Stelle den Fund, den ich im vorigen Jahr gemacht haben: die ersten vier Kapitel des Matthäusevangeliums repräsentieren die vier Antiken, wie Eugen Rosenstock-Huessy sie versteht!

Die vier Antiken – das sind Stamm, Reich, Israel und Griechenland.

*Der Stamm*: "Die Toten blicken auf die Lebenden." (S. 156) --

*das Reich*: "Jedes Reich bringt den Himmel auf die Erde. Ein Reich ist der Himmel auf Erden." --

*Israel*: "Israel ging über Pharaos Zyklen hinaus, indem es den einzigen und einmaligen Schöpfer Himmels und der Erde anrief, dem alle Kreise und Zyklen und Jahreszeiten und Äonen untertan sind. Jahve ist der Gott, der alles ein für allemal tut; Elohim ist derselbe Gott, insofern wir ihn als den Inbegriff aller "El"s, aller Gottheiten, als eine Summe also, erkennen…Der Name *Elohim* bedeutet: alle himmlischen Mächte, der Name *Jahve* aber heißt: *Ich bin jetzt*. Der dritte Name, *Schaddaj*, unsicherer Bedeutung, besagt nach dem weisesten Interpreten Benno Jakob: *der überall im Raum Gegenwärtige*." (S. 206) --

*Griechenland*: "Zwischen die wirklichen Sterblichen, den wirklichen Gott und die wirkliche Welt,

*jene erschrocken, hungrig, anhänglich,*

*der eine langmütig, unerkannt, kommend,*

*die Welt unerbittlich fortrollend im Sternenlauf,*

haben die Griechen sich angebaut und aus der "Eintagsfliege Mensch" (Pindar), aus Gott, wie er sich in der Geschichte eröffnet, und aus der Welt, wie sie aller Geschichte spottet, etwas Viertes zusammengesetzt: ein "göttliches Sein", das nie altert, nie stirbt, nie häßlich wird, nie aus dem Rahmen fällt. Die Welt der Ideale steht in einem Rahmen, in dem sie "Ewig klar und spiegelrein und eben fließt". Das Leben im Olymp "fließt den Himmlischen wandellos dahin". "Das Leben" ist weder männlich noch weiblich, weder Volk noch Stamm, weder ich noch du. Es ist sächlich. So ist das "Sein". " (S. 214)

Diese vier Stile standen vor Christi Geburt im Widerstreit zueinander und unverbunden da. Das Neue Testament beginnt im Matthäus-Evangelium damit, sie einander zuzuwenden.

Das erste Kapitel beginnt (in der Übersetzung Fridolin Stiers): *Buch des Ursprungs Jesu: des Messias, des Sohnes Davids, des Sohnes Abrahams.* Und dann folgt der Stammbaum von Abraham an bis David, von David bis zur Zeit der Verschleppung nach Babylon, von der Zeit der Verschleppung nach Babylon bis Josef, den Mann Marias. "Aus ihr ward gezeugt Jesus, der Christus genannt wird – das heißt: "Messias"." Und dann folgt die Summe der Geschlechter, dreimal vierzehn Geschlechter von Abraham bis zum Messias.

Im ersten Kapitel dann noch von der Empfängnis Jesu, und wie Josef geheißen ward, Maria zu sich zu nehmen, und welchen Namen er Jesus geben soll. Er tat so.

Das Merkwüridge: der Stammbaum ist die Urkunde des Stammeslebens auf der ganzen Erde. *Die Toten blicken die Lebenden an.* Nur wird hier die Reihenfolge unterbrochen und an die Stelle des leiblichen Ahnen tritt das Geheiß des Engels des Herrn. So wird einerseits das Stammesleben in all seinen Teilen anerkannt und für weiter gültig erklärt – andrerseits verliert es seine absolute Macht.

Im zweiten Kapitel – das also zu den Reichen gehört und etwas Ähnliches für sie vollbringen soll – wird erzählt von den heiligen drei Königen: Sternkundige fanden sich aus Ländern des Aufgangs in Jerusalem ein. Herodes weist sie mithilfe der Schriftgelehrten nach Bethlehem in Judäa. Die Geburt ist durch das Gestirn bestimmt – das ist das Gesetz der Reiche! Jesus ist auch als Pharao geboren!

Josef, Maria und Jesus fliehen nach Ägyptenland: *Aus Ägypten habe ich meinen Sohn gerufen.* Damit ist auch die Liturgie der Reiche eingelassen in die Heilsgeschichte.

Herodes – als Stammesfürst, der das Recht nimmt, alles Erstgeborene zu tötenwill auf leiblichem Wege verhindern, daß der neue König ihm den Thron streitig macht, und läßt alle Kinder hinmorden, vom Zweijährigen an und darunter, entsprechend der Zeit, die er von den Sternkundigen genau erkundet hatte. Nach seinem Tod ziehen Josef, Maria (Mirjam) und Jesus aus Ägypten nach Israel zurück, gerade so wie die Kinder Israel beim Exodus (2. Buch Mose).

So bleibt auch die Liturgie des Reiches mit *Gold, Weihrauch und Myrrhe* bei Jesus, Jesus hebt sie aber als absolut ebenso auf wie Mose, als er das Volk Israel aus Ägyptenland führte.

Das dritte Kapitel schildert den edelsten Mann Israels, Johannes den Täufer, der Jesus voranging und ihn taufte. "Ich taufe euch zwar im Wasser auf Umkehr hin. Der nach mir Kommende aber ist stärker als ich; und ich bin nicht genug, ihm die Sandalen abzunehmen. Er wird euch taufen in heiligem Geist und Feuer. In seiner Hand ist die Worfel: Säubern wird er den Drusch seiner Tenne und sammeln sein Korn in die Scheune. Die Spreu aber wird er verbrennen in unlöschbaren Feuer."

So redet Johannes der Täufer wie die Propheten. Die Taufe Jesu bewahrheitet dann sein eigenes Wort: "Als Jesus getauft war, stieg er sogleich aus dem Wasser herauf. Und da! die Himmel öffneten sich ihm, und er sah den Geist Gottes – herniedersteigend wie eine Taube – über ihn kommend. Und da! eine Stimme aus den Himmeln, die sagt: "Dieser ist mein Sohn, der Geliebte, an dem ich Gefallen habe."

So wird auch hier die Essenz gerettet, aber aus der Gewalt der Menschen herausgenommen. Die Prophezeiung wölbt bei Johannes dem Täufer einen so kleinen Bogen, weil sie sogleich in Erfüllung geht – und doch genügt diese kleine Zeitdifferenz, um Gott als den Waltenden vom Ende der Zeiten her auch für Jesus zu bestätigen.

Das vierte Kapitel erzählt von der Versuchung Jesu, dem Beginn seiner Lehre, dem Ruf an die Jünger Petrus, Andreas, Jakobus, Johannes – von denen man also annehmen kann, daß sie innerhalb derJünger die vier Stile der vorchristlichen Zeit verkörpern: Petrus den Stammeshäuptling, Andreas den Pharao, Jakobus den Propheten in Jerusalem, Johannes den Philosophen des Worts.

Die Versuchung des Vergleichs ist tatsächlich das Hauptmerkmal des griechischen Geistes. Die Griechen fuhren ja mit den Schiffen im Mittelmeer hin und her und gelangten so in die verschiedensten Gegenden mit verschiedenen Sitten und Gebräuchen – wer käme da nicht auf die Idee, sie miteinander zu vergleichen? Der Teufel bietet Jesus nun aber an, Macht zu erhalten und auszuüben, über das Brot, das Heiligtum, und alle Königtumer der Welt und ihre Herrlichkeit.

Der Teufel legt nahe wie jeder, der Vergleiche anstellt, man könne den Ernst doch in Frage stellen: zu den Steinen sagen, sie sollten Brot werden, sich vom Tempel stürzen und von den Engeln getragen werden, den Teufel anbeten und alle Macht auf Erden ausüben.

Die Stämme sorgten für das Brot, daß jeder zu essen kriegte, die Reiche schufen die Ordnung der Bebauung der Erde, der Heilkunst, aller Kunstfertigkeit (es gibt keine Technik nach Christus, die die Kunstfertigkeit der Reiche überboten hätte!), Israel verzichtete auf alle Macht über die Königtümer der Welt und ihre Herrlichkeit, um IHM allein zu dienen.

Der Teufel versucht Jesus, als Herr der Antiken Macht auszuüben. Und Jesus weist ihn mit Worten der Heiligen Schrift ab, dreimal.

Sein Ruf ist ganz einfach: *Kehrt um! Denn genaht ist das Königtum der Himmel.* Das ist nichts anderes, als was jeder der Propheten Israels auch gesagt hatte. Nur daß diesmal das Wörtchen *nah* in seiner eigenen Person wahrwird.

Und die Jüngerschar – es ist doch nicht schwer zu erkennen, daß Jesus da der griechischen Versammlungsform eines Lehrers mit seinen Jünger folgt, wie Platon und Aristoteles und die verschiedenen philosophischen Schulen.

Aber auch hier wird eingelassen, was weiter wirken soll: die Schriftkunde, das Verkünden der neuen Lehre, das Um-sich-scharen einer Schar von Lehre-Hörenden (wie ich das Wort *Jünger* einmal übersetzt habe).

Was ich sagen will: Eugen Rosenstock-Huessy folgt mit seiner Lehre von den vier Antiken und den vier Evangelien, die sie erlösen zueinander hin, genau dem Evangelisten Matthäus! Er hat ihn neu gelesen!

*Stämme, Reiche, Israel und Griechenland* – es gibt sie noch immer. Aber es ist bemerkenswert, daß in der Zeit des Ersten Weltkriegs fünf Kaiserreiche zerbrochen sind: Österreich-Ungarn, Deutschland, Rußland, Äthiopien und China!

(Aus China hat der evangelische Missionar Richard Wilhelm die Essenz der kulturellen Überlieferung eben vor dem Zusammenbruch des chinesischen Reiches gerettet – auch mit einer Traumerscheinung, wie sie von Josef berichtet wird! Von ihm habe ich das *I Ging, das Buch der Wandlungen* in deutscher Sprache – und Du weißt, was es mir bedeutet.)

11\. DIE EUROPÄISCHEN REVOLUTIONEN

Über die europäischen Revolutionen hat Eugen Rosenstock-Huessy ein eigenes Buch geschrieben, von dem er selber sagt: jeder Leser möge es fortschreiben und durch seine eigene Erfahrung ergänzen. Die Erkenntnis des Zusammenhangs des Zweiten Jahrtausends nach Christus im Abendland und in den europäischen Nationen als Dialog ist auch in der Soziologie unentbehrlich. Die Tafel der Zirkumvolution der Revolutionen auf Seite 642f. macht den Zusammenhang und den Wandel deutlich.

"Die Revolutionen mit Totalanspruch sind über die Jahrhunderte hinweg Bruchstücke einer politischen Unterhaltung. Zum Beispiel, Luther erwiderte wörtlich dem Innozenz III., Lenin dem Robespierre, Robespierre der *Bill of Rights* von 1689, Cromwell wandte sich gegen Martin Luther.

Die Revolutionen setzen sich in einem Rhythmus von Empörung und Demütigung durch. Jede Revolution beruft eine neue Trägergruppe zur Führung, deren Namen in dreifacher Gestalt hier gegeben wird, als Ehrenname, als Schimpfname und als nachträgliche Beschreibung der zwei Gruppen, die in den neuen Trägern durch die Revolutionen verschmelzen werden." (S. 642)

Zum Beispiel für die Reformation: *Evangelische* als Ehrenname, *Tyrannen* als Schimpfname (weil die Fürsten es waren, die die evangelische Nation darstellten), *Professor und Obrigkeit* als Träger, der Sprachstil ist *theologisch systematisch*, der Rhythmus wird mit den Jahreszahlen 1517-1525-1555 und 1618-1648-1654 gefaßt. "Für *Geburtstage* bei der Reformation setze der Leser alle ihm heut vertrauten Geburtagsfeiern: Luthers Geburtstag war der erste, aber Königs Geburtstag, Kaisers Geburtstag, Goethes Geburtstag sind Variationen über dasselbe Thema." (S. 642)

Du siehst daran – wie es in die Schulen gelangen müßte, dieses Wissen! Das Revolutionenbuch gibt es meines Wissens inzwischen auf deutsch, englisch, niederländisch, teilweise – meine ich – auf russisch – erst wenn es auch auf italienisch und französisch erschienen und gelesen wird, ist die Zeit, daß es als Grundlage für den Unterricht in allen europäischen Ländern diene.

Meine Kurse in der Volkshochschule in Kunst, Literatur und Musik sind von der Epochenbildung durch die Revolutionen, wie Rosenstock-Huessy sie gesehen hat, durchdrungen gewesen.

12\. DER IRRTUM DES LOHNBÜROS

Es heißt auf Seite 713f. der "Vollzahl der Zeiten":

"Die erste Wahrheit und die letzte Wahrheit der christlichen Zeitrechnung lautet daher: Die Zeit eines bestimmten Menschen ist unmeßbar. Kein Mensch kann daher auf seine angeblich eigene Zeit eingeengt weden. Der Geist einer einzelnen Zeit wäre Todesgeist. Jeder maßgebliche Mensch tritt zu der eigenen Zeit in Gegensatz: der lebendige Mensch in seiner unmeßbaren Zeitenfülle steht zu dem Käfig der eigenen karg bemessenen Zeit in dem Gegensatz des Lebendigen zum Toten.

Die eigene Zeit eines maßgeblichen Menschen ist die ganze Zeit von Anfang bis zu Ende. Hingegen ist der Zeitgeist ein fortgesetzter Anschlag gegen das keimende Leben der Zeitenfülle. Er zerfetzt es. …

Wir werden nur dadurch Menschen, daß wir uns unsere eigene Zeit nicht abschneiden lassen, daß wir dem Anspruch der angeblichen Zeit, die uns umringt, widersprechen und Widerstand leisten. "Der Mensch seiner Zeit" ist noch gar nicht Mensch, da er vor der Aussonderung in *Enkel und Ahn, in Erbe und Stifter, in Freier und Objekt* dem bloßen Augenblick verfallen bleibt."

Und dann zum Lohnbüro:

"Das Ereignis der Industrie, kraft dessen die Zeit ermordet, d.h. unzusammenhängend gemacht wird, sei nun aus einem Versteck aufgestöbert. Denn weder Gewerkschaft noch Betriebsrat, weder Kommunisten noch Sozialisten, weder Nationalökonomen noch Soziologen haben dies Ereignis rückgängig machen können. Dies Ereignis ist das Lohnbüro.

Im Lohnbüro wird der Zeit doppelte Gewalt angetan. Und diese doppelte Gewalt wolle sich der Leser einmal recht vergegenwärtigen. Denn gelingt ihm das, dann begreift er, daß unsere Industrie den Aberglauben der unzusammenhängenden Zeitabschnitte tagtäglich in ihren Lohnbüros hervorruft.

Die abgeschnittene Todeszeit der Physiker, der Naturforscher, der Journalisten, der Goebbels und Heideggers, die da beliebig je nach der Regierung sagen "heutzutage" oder "in unserer Zeit", ist ein hervorgerufenes Ereignis. Diese Denkweise ist ein Produkt der Industrie. Also kann ihm auch ein anderes Ereignis von uns entgegengesetzt werden.

Wir widersprechen dem Lohnbüro. Wir setzen uns zu ihm in Gegensatz. Wir sagen: im Lohnbüro rast sich die Welteroberung des zweiten Jahrtausends ähnlich aus wie sich die Theologie in den Hexenprozessen ausgerast hat. In den Hexenprozessen behaupteten die Theologen, von der Welt so viel zu wissen wie von Gott. Die Weltkinder aber haben damals die Theologen ihrer Unwissenheit überführen müssen. Kopernikus wußte mehr als Martin Luther von der Welt des Schöpfes.

In dem Industrieprozeß behaupten die Techniker soviel vom Menschen zu wissen wie von der Welt. Daher müssen wir Menschenkinder sie von Newton bis von Braun, von Manchester in England bis Detroit in Wisconsin ihrer Unwissenheit überführen.

Aber das steht erst bevor.

Der alte Oberbuchhalter in Leverkusen führte voriges Jahr den Ururenkel des alten Bayer in die Buchhaltung des Lohnbüros ein. Er war sehr durchdrungen von der Wichtigkeit seines Büros. Aber er schärfte dem jungen Herrn täglich ein: "Wir hier im Lohnbüro müssen ganz besonders sparen; denn wir rechnen zu den unproduktiven Kosten des Betriebes."

In diesem Satz steckt das doppelte Vergehen gegen die Zeit. Vielleicht weil es ein doppeltes Vergehen ist, wird es so lange ungestraft begangen. Sehen wir daher doppelt scharf hin.

Der Betrieb macht in der Fantasie dieses Lohnbuchhalters unproduktive und produktive Aufwendungen. Produktiv heißen die am einzelnen Stück entstehenden Unkosten. Unproduktiv heißen alle nicht am einzelnen Produkt errechenbaren Kosten. …

*Der erste Irrtum* besteht darin, daß jedes Stück ohne den Zeitpunkt, an dem es fabriziert wird, erscheint. Das Buch führt das erste Stück zuerst auf, das zehntausendste hernach. Dadurch bleibt die vielleicht improvisierte Anfangslage des Betriebes für das Lohnbüro maßgebend. Die erste Stunde und die vierundsiebzigtausendste Stunde reihen sich ihm aneinander wie die Stücke Damast in unserer Rechnung. Es gibt also hier nur eine in Stücke gehauene Zeit, und jeder in die Fabrikation eingestellt Mensch verläuft sich in diese unbestimmte, eintönige Zeit.

Die Industrie, so kann man sagen, verewigt den ersten Augenblick.

Die Lohntüte, der Stücklohn, die Tarifprobleme entspringen alle dem verzweifelten Bemühen des Lohnbüros, alle späteren Stunden an die erste Arbeitsstunde anzuhängen. …

Wahrhaftig, ein Kausalitätsdogma seltsamer Art, diese Pflichtschuldigkeit, die erste Stunde für maßgebend zu erklären. Aber deshalb findet das gesamte Ausbildungs- und Lehrlingswesen noch heute seinen Buchhaltungsplatz unter unproduktiven oder indirekten Kosten.

*Der zweite Irrtum*: Wer sich in einem Betrieb acht Stunden aufhält – ob nun einem Kloster oder einer Fabrik – stellt sich zur Verfügung. Und diese Residenzpflicht ist Goldes wert für den, der so über Anwesenheiten verfügen kann. Nirgends aber in der Buchführung erscheinen die ungeheueren Werte dieser Residenzpflicht. Nur aus ihr aber lebt der Betrieb als ein wachsender und wandelbarer Betrieb.

Betriebsfähig ist nur die Fabrik, die über einen Stamm von Arbeitern verfügt, den sie jederzeit als Repräsentanten der Fabrik hin und her senden kann. Ob der Monteur beim Kunden den Traktor abliefert, der Gärtner die Pflanzen beim Käufer einsetzt, der Einrichter die Maschine für die Stücklohnverdiener herrichtet, der Vorarbeiter und zwei erfahrene Arbeiter eine Zweigwerkstatt einrichten, überall wird da neues Leben durch Zeithingabe erzeugt. Und alle diese zeugenden, das Werk selbst erneuernden, umbauenden, verzweigenden Arbeiten fallen unter das Verdikt: *unproduktiv*! Sie werden also instinktiv von dem alten Oberbuchhalter bei Bayer zurückgeschnitten.

Was ist die Folge dieses Doppelirrtums?

Unsere Industriebilanzen balancieren auf den produktiven Kosten am Stück einen täglich wachsenden Überbau der "unproduktiven" Kosten. Dabei werden zu diesen unproduktiven Kosten nur die im Werke auflaufenden Kosten für die Erzeugung eines "Arbeiterstammes" veranschlagt. Was aber derselbe Fabrikbetrieb, der sich seiner sparsamen Buchhaltung rühmt, für die Straßenbahnen, Krankheiten, Kriminalität, Trunksucht seiner ihrer Zeit beraubten Lohnempfänger den Gemeinden und dem Staat und den Ärzten bezahlt, das läßt sich in seiner Kostenrechnung nirgends entdecken. Die sechs Wochen Krankenlohn – das weiß jeder – kosten jeden Betrieb eine Stange Gold. Sie sind die Folgen des Stundenlohns, der den Kranken zur Ausbeutung der Kasse reizt.

Im Laufe der Zeit ist also das Lohnbüro zur Gefahr für das Bestehen des Betriebes geworden.

Das Dogma, in dem sich des Lohnbüros Angst zur Verführerin der gesamten Wirtschaftswissenschaft aufgeschwungen hat, ist das Dogma von der Vollbeschäftigung. Es sei ein Unglück, arbeitslos zu sein für jede Arbeitskraft, es sei ein Unglück, auftragslos zu sein für jeden Betrieb. Klassische und marxistische Ökonomie wetteifern in ihrer Angst, ein einziges junges Mädchen könne unbeschäftigt bleiben. Der Zusammenbruch des Kapitalismus wird daraus vorhergesagt.

Angst ist aber der dümmste Ratgeber. Wie sie die Kehle verengt, so verengert sie auch den Sinn. Der Ausschnitt des vollen Daseins, den das Wirtschaften ausfüllt, verblödet, wenn ihn nicht der Sinn des Ganzen durchblutet. Wenn es nur ein Unglück wäre, arbeitslos zu werden, dann wäre der technische Fortschritt, der jeden gelegentlich aufs Pflaster zu werfen droht, sinnlos.

Das Leben erreicht seine Höhenlage erst da, wo unsere Arbeit unbezahlbar, unsere Dienste nicht in Geldeswert ausdrückbar werden. …

Die Leitersprossen mögen im Westen von unten nach oben, im Osten von oben nach unten reichen – die zwingende Aufgabe ist genau die gleiche. Da hat weder das zentrale Staatsdogma noch das anarchische Privatdogma irgendwelche Zukunft. Aber wir, denen es obliegt, Zeit zu gewinnen, ein Jahrtausend anzubahnen, in denen die Welt als Welt beherrscht wird und die Menschen sich gegenseitig in der Gesellschaft die Zeit entbieten, das Leben schenken, die Zukunft bestimmen, wir müssen von jeder Buchhaltung eine Revolution verlangen*: in dieser Revolution muß der seine ganze Lebenszeit einem Betrieb widmende Mensch den Ton angeben*. Und die kürzeren Zeiten der anderen Betriebsangehörigen werden zu ihm stimmen und mit ihm übereinstimmen, oder dieser Betrieb wird verfallen, nur vorübergehend darf sich der Stücklohn finden und als vorübergehend mag er gelten.

Die Epoche, in der produktive und unproduktive Kosten von Gewerkschaftssekretären anerkannt wurden, wird dann wie eine vorsintflutliche Zeit anmuten, nämlich eine Epoche, in der sich das Dritte Jahrtausend noch nicht gegen *die Übermacht der Räume* im zweiten Jahrtausend erhoben hatte. Was für eine Epoche, so wird man sich zuraunen, muß das gewesen sein, in der ein Zeitzerstäuber aufgestellt war und den Menschen ihre Unermeßlichkeit, ihre Lebenszeit zerstob!"

An dieser Revolution habe ich nach Kräften gearbeitet, lieber Niklas, und erlebe im 37. Jahr meiner Lebensarbeit an der Volkshochschule Köln, daß das Zerstückeln der Zeit exemplarisch an mir konstatiert wird (Abmahnung wegen Verletzung der Kernzeit, indem ich die Mittagspause nicht um 2 Uhr, sondern zum Beispiel 14.27 Uhr beendete)!

In der Entscheidung, die Mittagspause mit Euch zu teilen, gemäß den von Euren Schulstunden gesetzten Zeiten, habe ich mich gegen die Zerstückelung der Zeit gewandt. Für die letzten Monate meiner Berufsarbeit muß ich nun ableisten, was sonst jeder – ohne zu wissen, woran er da leiden muß – erbringt: das Ertragen der Ohnmacht gegenüber der Einteilung in *produktiv* und *unproduktiv*!

Ich möchte kurz fassen, was mir die zwölf Gesetze merklich hält:

Das *Gesetz der Technik* besagt, daß der technische Fortschritt immer die Gruppe zerschlägt.

Das *Erste Sozialgesetz* schützt die Bewunderung des Laien für den Fachmann und umgekehrt.

Die *Lebensalter* verschränken Jung und Alt, Mann und Frau so, daß sie in keinem Zeitpunkt ohne Aufforderung bleiben, sich nicht selbst als komplett anzusehen. Merke besonders: die Frauen sollten mit 45 Jahren studieren!

Der *Betrieb* fordert mich in der Eigenschaft 3=1 – und das Leben bleibt stets angewiesen, die zeugende Kraft der *Ehe* einzulassen: 1x1= 1 (2=1). Überall wo das geschieht, ist Ehe!

Das *Sprechen* verändert die Personen, die sprechen und über die gesprochen wird in ihrer gesamten sozialen Lage.

Der *Zwölfton des Geistes* offenbart den wahren Ursprung als das im biologischen Leben immer noch Kommende!

Die *Regierungsperiode* besagt, daß alle 15-20 Jahre ein neues Unrecht entsteht, für die es eine neue Regierung braucht, die gerade dieses wendet. Jede Regierung, die länger dauert, als das Unrecht, das sie gerufen hat, wirkt schädlich (Adenauer, Kohl).

Die Betrachtung der Krankheiten ist unzulänglich, wenn nicht die Ursachen aus dem *Zeitenspektrum* in ihrer Vielfalt erwogen werden:

*aus Ungehorsam gegen unsre Berufung,*

*aus Lieblosigkeit,*

*aus Trotz gegen die Logik,*

*aus Taubheit gegen den Rhythmus des Leibes,*

*aus Unvorsichtigkeit gegen en fallenden Stein -*

in dieser Reihenfolge!

Jeder bildet die *Epoche* in den Feiertagen, die er oder sie für sich einsetzt – keiner ist von dieser Aufforderung, die Zeit in Epochen zu gestalten, frei.

Die *vier Evangelien wenden die vier Antiken* Stamm, Reich, Israel und Griechenland einander zu.

Es ist eine Revolution anzusagen, damit *die wesentliche und maßgebende* *Arbeit für unbezahlbar angesehen* wird. Das Unbezahlbare daran bemißt sich an der Länge der Zugehörigkeit.

Mit dieser Präsentation der zwölf Gesetze, die mir aus Eugen Rosenstock-Huessys *Soziologie* entgegenleuchten, habe ich nun die Arbeit getan, die ich mir vorgenommen habe.

Der letzte abschließende Brief geht der Frage nach, was es denn – nach Eugen Rosentock-Huessy – mit dem Christentum im Dritten Jahrtausend nach Christus auf sich habe.

Dein Vater

**ZWÖLFTER BRIEF: CHRISTENTUM IM DRITTEN JAHRTAUSEND NACH CHRISTUS**

*Köln, den 16. Januar 2005*

Lieber Niklas,

wenn Kirche und Staat, Wissenschaft und Kunst die Darstellung der im Kreuz der Wirklichkeit erfaßten Kräfte sind,

*Kirche uns verbindend mit dem Ursprung,*

*der Staat mit den äußeren Grenzen alles Tuns und Lassens,*

*die Wissenschaft uns hineinnehmend in den*

*in allen Forschern verkörperten einen Forscher Mensch,*

*die Kunst den Reiz des Zukünftigen immer wieder neu präsentierend,*

dann kann das Christentum nicht mehr darin bestehen, einfach zur Kirche zu gehören, es besteht dann in der Möglichkeit, zwischen diesen Mächten zu wechseln, je nach dem Ruf, der unsre Seele erreicht.

Wie leicht scheint es doch so, daß Liturgie und Rituale der Kirche – in allen Konfessionen – lediglich fordern, die schon gebahnten Wege nachzugehen, in Gebet, Gesang, Schriftlesung und Auslegung, im Abendmahl, Credo, Vaterunser und in der Taufe, im Kirchenjahr mit Weihnachten, Ostern, Pfingsten und der langen Trinitatiszeit. Und solange uns nichts Besseres einfällt, ist das auch der richtige Weg.

Eugen Rosenstock-Huessy hat sich im Herbst 1906 unter Lukas 6, 5a (Codex D) taufen lassen. Mit der Soziologie, die er seit 1915/16 mit sich trug, hat er mit dem Titel: *Das Kreuz der Wirklichkeit* auch die Christen angeredet, wie Glaube, Liebe und Hoffnung nach dem Weltkrieg neu zu bewähren seien.

Als Zeugnis dessen bringe ich Dir – auf Englisch – ein *Pastoral Prayer* vom 28. Oktober 1956, aus dem Jahr, als der erste Band *Die Übermacht der Räume* erschien.

Es gehört zu dem Gottesdienst, bei dem Eugen Rosenstock-Huessy auch die Predigt hielt. Diese beginnt mit den Worten:

Last Sunday our minister led us into the heart of the Christian gospel. He prayed that we should not and could not ask for exemption from trouble, but this is precisely what everyone of us does ask for. He read to us the sixth chapter of the letter to the Ephesians on the powers and principalities which promise us just this: exemption from trouble.

We, the lay people of this church, have been asked by him to respond to his ministry on this layman's Sunday, the sunday of the Reformation. So today, if you will allow me this figure of speech, we respond to his psalm, sermon and prayer of last Sunday.

We go through the responsive readings in the back of our hymn book perhaps with less fervor than we should. The selections in the book are too short, too arbitrary, and I think in our congregation we might give more honor to the great texts of our Bible, because I think in the responsive readings more than any other part of our service the spirit of the living word comes home to life.

In the responsive readings there is great revelation because the word comes back to the speaker from our mouths of those who listen to him. Is it not so that when we have listened, only then do we know what we have said as a friend's mouth responds?

The listener will and must vary the line the first speaker has intoned. Only in this variation does the first utterance gain ist full force.

As a responsive reading written large, I ask you to consider today's sermon. I have listened to Mr. House in seriousness and my sermon will take up his text from Ephesians, chapter 6, verse 12:

"For our fight is not against any physical enemy. It is against organizations that are spiritual. We are up against overlords who sway us as far as we are part and parcel of this dark world, and against spiritual agents who are so impressive because they seem to stem from heaven."

Drei Beispiele erhellen, was es mit den Mächten auf sich hat.

I shall begin, that is, with the same negation as Paul – the Christian resistance against powers and hosts of Heaven.

A young Christian Negro school teacher in Kenya, Africa, has shown us. She declined to take the Mau Mau oath because it prescribed unlimited obedience to the Mau Mau order of hatred. And she was beaten to death for this refusal to acknowledge the intermediary power of the tribal order.

A very close friend of mine was the most popular Roman Catholic writer of the twentieth century in Germany, Joseph Wittig. We joined hands in our work und we have prayed in responsive reading of the psalms together.

But one day his *Life of Jesus* was put on the Roman Index, and as he requested to be told the reason for this, he, instead of being told, was excommunicated from his beloved church. The present Pope ten years later has said that he never read a more beautiful book than this which Rome condemned. So you must realize that a faithful son of his church was nearly killed by this excommunication which was based on Canon 2314 of the Canon Law Book. He, Joseph Wittig, wrote, "But I stand in the faith that neither fire nor water nor the canon 2314 can separate me from the love of Christ."

Here was the letter of the Romans in full action, in full agony and as a means of true salvation. For, to this friend writhing in shame and vexation, the Church, his Church, had been the beloved mother, the star from the sea. Rome had been a part of the heavenly host who praised Got and whom we beg to praise God.

Let me use a third example: in 1939 a German taught at Union Theological Seminary in New York. He was loved and respected here as well as in Great Britain, and a fine career was open to him. But he hated Hitler and all his works. And he felt that Germany was left without the Guardian Angel which is protecting every nation. The powers of darkness ruled in his native land.

Dietrich Bonhoeffer said: "I must pray for the downfall of Hitler who defiles Germany. But since I pray for his defeat, I must return to my country which I love and when it passes through the valley of defeat, I must share this defeat." Dietrich Bonhoeffer has become the shining example of a Christian martyr through this act of returning under the inermediary powers, yet resisting them. His violent death by Nazi hand has made his name shine with the saints: Dietrich Bonhoeffer, whose fight was not with a physical enemy, but with wickedness in high places, with "an impressive agent who seemed to be the government".

Out of these three examples, the Mau Mau oath refused, the Canon 2314 weathered, the guardian angel over Germany disappearing, we may find the solution of our riddle.

The tribal famliy, the ecclesiastical policy, the national government, they all are indispensable to our life on this globe. And there is a certain priority in their oppressing us which stems from the date of their appearance. But because they are indispensable, they tend to become God Almighty.

It is always the latest garment of God's creation which pretends to be God himself. In our time the latest ist the organization, hence the craze for organization, is playing God. Machines promise to replace our mind.

Ein viertes Beispiel ist so wunderbar, daß ich es nicht auslassen möchte:

A few days ago I received a letter from a fine medical student whose father is a good doctor of the poor in Lowell, Massachusetts. The student left one school of medicine because there science seemed to kill the spirit of healing. Now, in the new place, he listened to his first lecture with great joy.

This is what the professor said: "The doctor's duty is to comfort always, to relieve frequently, to cure rarely. The number of patients that a doctor sees that he can cure is very few; the number that he can relieve is very numerous; but he can comfort all. And those doctors that don't realize that their primary duty is to comfort, don't realize the limitations of medicine."

The student glowed with joy over this religious insight, and the professor will go to Heaven for it. In fact, he is in Heaven through this simple humility. And therefore, the professor of medicine and his student both have the rigth to intone our line, "Praise him above, Ye heavenly Hosts".

Zusammengefaßt wird das Wort Eugen Rosenstock-Huessys am Ende der Predigt:

As God's children we must take most seriously his angels. As Christians we must ridicule any exaggerated claim they make of being ends in themselves.

This, then, is the trouble from which we *never, never, never* may ask to be exempted.

The more troubles these powers promise to take from us, the more we have to take the trouble to see to it that they praise Him from whom all blessings flow, and that in praising him, they remain small.

Because at all times God himself may enter in his most unexpected manner.

Und zu diesem allen das:

*PASTORAL PRAYER*

*October 28, 1956*

How glorious is thy creation, o Lord.

Thou hast placed us in the midst of these green mountains, of these brooks and groves. And above, thy skies declare thy desire for teaching us the grandiose rhythms of the stars and their constellations in the firmament. Waterfalls and rains make haste. Slowly do sun and moon move; they proceed in great solemnity to that our hearts may learn the quick as well as the slow beat.

To our country thou hast given years of improbable plenty, aye of abundance and superabundance. Our poor would appear extremely rich to the eyes of the wealthy of former days. Thou allowest more than one hundred and sixty million people to live in these United States in peaceful cooperation, on a land on which some centuries ago perhaps one million eked out a miserable existence.

An who are these modern millions? Thou hast given this land to fugitives, refugees, immigrants, exiles, indentured servants, to the offspring of slaves, to religious sectarians and non-conformists, non-jurors, in short to those rejected by other orders or to those who rejected these other orders, to the hopeless, the stateless, the excommunicated; and thou hast shown thy mercy to their offspring so that they live under a constitution which is the envy and model for whole continents. And our machines change the surface of the globe in gigantic operations.

Free Churches and a powerful government take care of us, spiritually the Churches and materially the government, in helpless youth and in helpless old age.

In this thy glory we proudly participate. We are accepting those benefits as though we had done it all. Hence while we should look up to thy glory, how clearly must thou see our weakness from on high. Deep down in our heart, we know, every one of us, that thou art the giver; still when we get together, we are likely to boast that we did it ourselves. We bask in thy sunshine; and in the next minute, we brag about our own enlightened reason. While we peep in the cosmic secrets of thy matter, we explode the globe's resources for our certain destruction.

Thou hast placed us a little lower than the angels, and we align as often as not with the fallen angels who deny their fealty to thee and who strut – be it a science or an art or some fashionable "-ism" invading even thy church with their fads and organisations and their pagan manipulations of human beings without thy will.

Humiliated by our own faithlessness, exalted by thy infinite faith in us, we may not approach thy throne by our own merit or power. Not as individuals, cut off from the tree of our race by our pride and our rationalisations, we only may aproach you under the head thou hast given us, thy only begotten Son.

We recognize that the Spirit of Jesus must have reformed our minds, the Spirit of the Christ, before we are able to receive the Spirit, Thy Spirit, as thy marching orders for our next day.

We recognize that our proud institutions will not give us light unless they are manned by children of thy light.

We recognize that select men, Governors, the Congress must fail us, if we fail them through cowardice, indifference, greed.

Grant us not the television but the near vision. They who become unsettled by the far away sensation, will turn us into a mob. Instead transform us into a nation that can settle its unsettled affairs.

For fourty years there has been no unanimous peace been concluded. Teach us to form those new forms of peace, of collaboration, of intercourse, those new structures of Church or States which shall acknowledge thy perpetual reformation of thy peoples and thy continents.

Teach us to acknowledge thy reformation of our hearts.

Thou hast formed us, o Lord. Reform us for thy Kingdom.

Amen.

Mit diesem Gebet schließe ich meine zwölf Briefe an Dich, lieber Niklas Sebastian Nahum, in Castleton.

Nimm Sie Dir zu Herzen, nimm Dir Zeit dazu, vertraue darauf, daß ich Dich nicht zu etwas überreden will, was nicht seine Zeit bei Dir erwartet und findet.

Dein Vater

[zum Seitenbeginn](#top)
