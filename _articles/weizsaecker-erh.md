---
title: "Viktor von Weizsäcker über den Patmos-Kreis"
category: aussage
published: 2022-01-04
name: Weizaecker
---

„So aber, wie Rosenzweigs Scheitern an der Aufgabe einer religiösen Erneuerung seines Volkes aussah, so sahen auch die christlichen und weiterhin die geistigen Versuche gleicher Art aus. Da waren zunächst seine zwei Vettern Hans und Rudolf Ehrenberg, sein Freund Rosenstock, die gleichaltrigen; Werner Picht, der Volksbildungsmann, Leo Weismantel, der Dichter, Carlo Philips, der Übersetzer, und etwas entfernter noch ich selbst. Es lag nahe genug, daß die so Bestrebten auch Fühlung mit den aus der Kirche hervorgehenden Bewegungen bekamen, und das waren vor allem Karl Barth und seine Freunde Thurneysen, Gogarten und Merz. Ein neuer Kreis entstand so, der sich in Erinnerung an die Insel, auf der dem Johannes die Apokalypse offenbart wurde, als Patmos-Kreis bezeichnete und auch mit einer Schriftensammlung hervortrat; an diesem Kreis habe ich mich nicht beteiligt. Rosenzweig bezeichnete mich als den einzigen seiner Freunde, der die Wissenschaft noch ernst nehme, was sowohl Achtung wie wohl ein kleiner Hochmut von seiner Seite war. Aber er selbst gehörte auch nicht zum Patmos-Kreis; der war ihm wohl zu sehr aus Juden, Christen, Philosophen und unruhigen Geistern zusammengesetzt. Inselartig waren in der Tat diese Kreise, flüchtig aufleuchtend wie Inseln der Sage, aber doch radikal und hart genug gegen die Versuchungen der Zeitpolitik, wie Korallenriffe, die auch, wenn sie bald überflutet werden, die Struktur der geistigen Landschaft stark bestimmt haben, und die auch jetzt noch zum Untergrunde unserer Geschichte gehören.”
>*Viktor von Weizsäcker, Begegnungen und Entscheidungen, Stuttgart: K.F. Koehler Verlag 1949, S.15ff.*
