---
title: Wim van der Schee zu Der unbezahlbare Mensch
category: weitersager
order: 36
---

***SICH ABSTIMMEN***\
***aus dem Niederländischen von Eckart Wilkens***

*Havelte, 3. März 2006*

Liebe Annette und Robert-Jan, Hester und Arjon, Carlos und Evelien

*Solange Ihr mich kennt, bin ich mit Eugen Rosenstock-Huessys Büchern beschäftigt. Momentan läuft ein Projekt, bei dem, wer sich in seine Gedanken vertieft hat, in einem Brief an „die folgende Generation” darlegen mag, was er der Lehre von Rosenstock-Huessy beimißt.*

In diesem Brief (er ist mein Beitrag zu dem Projekt „Weitersager Mensch”)

möchte ich Euch also davon erzählen und hoffe, daß es Euch was sagt.

Mein Thema ist

**SICH ABSTIMMEN**

*Einleitung, der rote Faden*

Mein Thema ist das Sich-abstimmen. Kommt mir vor wie eine kleine Forschungsreise nach dem Wert unsres Lebens. Den finde ich eben in dem, was heißt: sich abstimmen. Denn Bedeutung kommt unter Menschen zustande, indem sie sich abstimmen.

Das geschieht im alltäglichen Umgang: es setzt in Beziehung zu allen, mit denen Ihr zu tun habt, und auch zur Welt, in der Ihr lebt.

Ich habe den Brief in Absätze eingeteilt. Nach einem Absatz über die Reichweite der Bedeutungshaltigkeit handelt der größere Teil des Briefs von dem Prozeß des Sich-abstimmens. Ich beschreibe aus meiner eigenen Geschichte heraus, daß Menschen vor allem zuerst Bedeutung empfangen, später in ihrem Leben Bedeutung verleihen können.

Das Sich-abstimmen ist ein wiederkehrender Prozeß. Darum halte ich bei „Verhaltensformen”. Verhaltensformen bilden den Verbund, in dem Wiederkehr funktioniert. In diesem Verbund kommen Menschen aus vier Richtungen zur Abstimmung miteinander: aus zwei „horizontalen”, und zwei „vertikalen”. Hauptpunkt ist: für jeden Menschen wie für jeden Lebensverband gilt, daß die vier Richtungen wiederum zu einem zusammenhängenden Ganzen gebracht werden müssen. Denn nur im Zusammenhang können Bedeutungen aufeinander einwirken und in ihrer Wirkung wertvoll werden. Wo das mißglückt, sieht man als Mensch wie Verbund im Zusammenleben, daß Probleme entstehen. Dann sehen die aufeinander Bezogenen keinen Sinn mehr darin oder noch schlimmer: ihre Entwicklung stagniert.

Ich schreibe darüber aus eigener Erfahrung. Für jedermann ist es schwer, sie zu durchschauen. Rosenstock-Huessy hat die Merkmale menschlicher Erfahrung verstanden. Das war mir immer eine große Stütze.

*1\. Dein Leben hat Bedeutung*

Was bedeutet unser Leben?

Die Antwort hängt daran, wie weit man reicht, oder wie tief man einteilt. „Bedeutung” ist ein weiter Begriff. Auf den ersten Blick (1) mag man denken: lebe das Leben und genieße es. Spontan trifft man Bedeutung an. Teilt man die Erfahrung der Bedeutung ein bißchen auseinander (2), kommt heraus, daß man andere braucht, um das Leben genießen zu können. Und die anderen haben Dich ebenso nötig. Bedeutung deutet darauf, daß man mit anderen verbunden ist. Selbst wenn man etwas allein genießt, ist die Lage dieselbe, als wenn man mit andren zusammen Bedeutung hat.

Tiefer gedacht (3) kommt man schnell dazu, daß wir nicht alle den Geschehnissen und Dingen dieselbe Bedeutung zuerkennen. Die Menschen sind verschieden. Der hat Talent für das eine, Bedarf für etwas zweites, für Freunde und Freundinnen, Partner und Familienmitglieder aber liegt das schon wieder anders. Die Unterschiede sind nicht so groß, daß sie nicht zu überbrücken wären. Auch ist die Frage, in welchen Punkten von Bedeutung die Unterschiede bestehen.

Ich will die Sache auf die Bedeutungen zuspitzen, die dem Leben als solchem Bedeutung geben. So geht es mir nicht darum, daß der eine mehr als der andre Wert auf ein aufgeräumtes Haus legt, sondern etwa darum, daß es für jeden wichtig ist, ein eigenes Haus, ein eigenes Zimmer oder einen eigenen Platz zu haben. Und wenn man mit jemandem ein Haus teilt, daß man dann ungefähr denselben Sachen dieselbe Bedeutung erteilen muß, wenn das Zusammenleben glücken soll.

Es gibt Bedeutungen, die das menschliche Zusammenleben oder Zusammenwirken fördern, und solche, die andere nicht mit einem teilen, die also zur Folge haben, daß man uneins wird. Die Mühsal der Verschiedenheiten kann so groß sein, daß die Beteiligten auseinander gehen.

Doch will ich mich in diesem Brief an Übereinkünfte von Bedeutung halten, die so groß sind, daß sie die Unterschiede überbrücken. Wir gönnen einander persönlichen Geschmack oder Vorliebe, wir teilen aber Bedeutungen miteinander, die jene übersteigen.

Wie kommt das? Und die auf der Hand liegende Antwort (4) ist, daß gemeinschaftliche Bedeutungen von der Tatsache herrühren, daß wir in derselben Welt leben. „Welt”: unsre Lebenslage, die miteinander geteilten Umstände. Wir leben hier in den Niederlanden. In diesem Land geht jedes Kind zur Schule, man muß arbeiten, um Einkommen zu haben, wir halten uns an die Verkehrsregeln usw. In solchen Lagen leben wir.

Es gibt aber auch die Einsicht, daß es Menschen gibt, die in anderen Umständen leben. Die anderen Umstände haben erkennbar dazu geführt, daß andere Bedeutungen zum Ausdruck gekommen sind. Es gibt für das Zusammenleben eine andere Grundlage. Wohl nur teilweise, denn wer zum Beispiel in einer anderen „Welt” krank wird, kriegt auch Hilfe, weil die Menschen fast auf der ganzen Welt finden, daß Kranken geholfen werden muß.

Die Frage ist dann, ob es gemeinschaftliche Bedeutungen unabhängig von jeweils geteilten Umständen gibt. Gibt es universell geteilte Bedeutungen (5)?

Im Verbund der Vereinigten Nationen sind Menschenrechte formuliert. Gibt es Bedeutungen, die wir als Mensch im ganzen dem Leben erteilen?

Da fällt mir auf, daß Menschen wo immer auf der Welt und wie denn auch füreinander und für sich selber Bedeutung geben. Frage ist, wie sich das füllt. Man kann fragen, ob alle Menschen ungeachtet der Umstände im Verhalten zueinander miteinander dieselben Bedeutungen teilen.

Bei *Rosenstock-Huessy* finde ich eine Antwort. Er weist uns auf die Entstehungsgeschichte des Menschlichen (5a). Immer ging und geht es darum, daß Menschen miteinander in Übereinstimmung darüber kommen, was gut und böse ist. Es sind immer praktische Probleme, die gelöst werden mußten. In jeder Lebensgemeinschaft entstanden je nach Gelingen Bräuche und Auffassungen. Mit einem Wort: Kulturen.

Die Wirkung der Kultur begreift man am besten, wenn man auf dem untersten Maßstab an die eigene Familie denkt.

So gedenke ich zum Beispiel mit Liebe und Respekt meiner Großeltern. Wie sie mit uns umgingen, zeigten sie – ohne drauf aus zu sein -, was gut ist, was nicht, was witzig ist, was nicht, womit ein Mensch Beschwer hat, womit nicht. Das taten sie wie ihre Eltern und Großeltern. Jede Generation ist ein Glied in der Kette der Geschlechter. Wo die neue Generation Bedeutung übernimmt, setzt sich die Kultur fort.

Wie in der kleinen Familie geht es auch im großen in den Kulturen. Durch Kultur sind die Menschen durch selbstverständlich gewordene Bedeutungen, die sie miteinander teilen, miteinander vertraut. Menschen hören aufeinander, die von selbst wissen, was „sich gehört”.

*Rosenstock-Huessy fügt – nüchtern betrachtet – geteilte Umstände geteilte Geschichte hinzu. Geteilte Geschichte gibt Menschen gemeinschaftliche Bedeutung. Bedeutungen finden ihre Herkunft in miteinander geteilten Umständen und Kultur.*

Wir erleben mit, wie stark verschieden Kulturen sind. Oberflächlich muß die Frage, ob es universelle gemeinschaftliche Bedeutung gibt, verneint werden. Wirklich? Kulturelle Unterschiede werden doch auch überbrückt? Wir leben doch in einer multikulturellen Gesellschaft. Müssen wir pessimistisch auf jegliche Vorbilder von Nicht-begreifen und gar Haß zwischen Menschen hinweisen, oder haben wir Grund zum Optimismus?

Die Frage ist (5b), ob sich Bedeutungen aus verschiedenen Kulturen verbinden und zu einer gemeinschaftlichen Kultur sich entwickeln können.

Wieder gehe ich da bei Rosenstock-Huessy zu Rate. Durch ihn habe ich verstanden, daß die Anfangsphase jeder Kultur eine Forschungsreise zu gemeinschaftlichem Werden von Bedeutungen ist.

Es geht darum, wié die Menschen ihre Entgegengesetztheiten überbrücken. Sicher spielte Zwang eine Rolle. Die mächtigsten Menschen und Völker haben andere ihrem Willen unterworfen, so haben sie Gemeinschaftlichkeit abgezwungen. Das ist die eine Seite. Aber es gibt auch die andere. Jede Kultur hat mit dem Gebrauch von Gegenseitigkeit zwischen Menschen angefangen. Zuallererst: zwischen Mann und Frau. Selbst wenn die Frau aus einem anderen Stamm geraubt war, gab es doch entscheidende Erfahrungen, bei denen sich Mann und Frau aufeinander verlassen konnten, wie sie zusammen Kinder kriegten und beide dem Kind ihre Liebe und Sorge zuteil werden ließen.

Man denke auch an die notwendige Arbeitsteilung, um Essen und Trinken zu besorgen. Die Menschen mußten zusammenarbeiten um zu überleben: untereinander Absprache treffen über die Art des Jagens, wer zuhause bleibt und die Frauen beschützt. Und so weiter. Man mußte einander die Hände reichen, und wenn es nur war, um Unheil abzuwenden.

Das hat alles eine positive Seite. Menschen sind imstande, einander zu schätzen, zu erkennen, zu lieben. Sie können sich an Absprachen halten, wollen Besitz teilen und einander der Sicherheit versichern.

Weil in allen Kulturen gleichartige Probleme überwunden werden, weil die „menschlichen” Reaktionen darauf gleichartig sind, weil also die Aufgabe, „Gemeinschaft” zu sein, in jedem Fall gleiche Bedeutungsmuster liefert, ist jede Kultur auf dieselben Urbedeutungen gegründet. Dann können verschiedene Kulturen auch zu neuer Gemeinschaftlichkeit zusammenschmelzen. Denn „Futter” für die Erneuerung liegt jeder besonderen Kultur zugrunde.

Ist der Begriff „Bedeutung” nun erschöpfend behandelt? Tief am Grunde liegt aber noch eine Frage. Die Frage, was wir selbst damit zu tun haben, daß unsere Bedeutungen so tief in Geschichte und Umständen verwurzelt sind. Was haben wir damit zu tun (6)?

Antwort: sie sind das „Material”, mit dem wir selber zeichnen sollen. Die Praxis aller Zeiten ist auch die Eure! Der Anfang von allem liegt nicht nur zu Beginn der Kulturen, sondern in Euch. Denn jeder Mensch fängt dem Wesen nach neu an.

Neu ist Eure Zukunft. Ihr müßt Eurer Zukunft Bedeutung verleihen.

Ihr gehört schon zu einer Kultur, und so habt Ihr Eure Bedeutung schon empfangen, aber Ihr selbst müßt mit Blick auf die Zukunft Bedeutung verleihen. Der andre, mit dem Ihr zu tun habt, muß Euch Bedeutung verleihen, die Umstände, mit denen Ihr zu tun kriegt, müssen Euch Bedeutung verleihen, ebenso die Situationen, an denen Ihr teilnehmt. Und Ihr müßt andere suchen, durch die Ihr Bedeutung erlangt. So bekommt jeder Mensch einen eigenen Inhalt, nämlich als „bedeutsam”. Was bedeutsam ist, kommt auf Euch zu.

Wie ich diesen Brief schreibe, ist es teilweise eine die Vergangenheit vergegenwärtigende Stimme. Aber doch klingt das Heute darin durch, denn wir sind füreinander bedeutsam, und ich will mich darauf berufen (nehmt mich ernst!).

Geht es um die Zukunft, ists an Euch. Ihr stiftet neue Bedeutung mit Menschen und an Orten, an denen ich nicht bin.

Nun ist es das besondere Kennzeichen von Bedeutungen, daß Ihr sie nicht selbst bedeutsam machen könnt. Ihr tut es für andere, andre für Euch. Wo Ihr Gemeinsamkeit und Gemeinschaftlichkeit gestiftet habt, da seid Ihr bedeutsam geworden und habt Bedeutung zustande gebracht. Dann habt Ihr Euer Leben vorwärts gelebt. So gehört es sich, denn „Bedeutung” ist in tiefster Tiefe: mit der Zukunft in Verbindung treten.

Ihr könnt Euch selber nicht bedeutsam machen. Trotzdem könnt Ihr Euren Willen darauf richten. Was Ihr wollt, ist wichtig, denn damit gebt Ihr Eurem Einsatz eine Richtung. Menschen können wählen, jawohl. Und das Gehirn zu gebrauchen ist auch nicht verkehrt.

Wißt also, daß das, dem Ihr Bedeutung geben wollt, die größte Chance hat, etwas zu werden,

*wenn Ihr es deutlich und bewußt wählt (6),*

*wenn Gegenseitigkeit zu erwarten ist, mit der der eine den anderen erfüllt (5b),*

*wenn Ihr die Unterschiede unter Euch respektieren könnt (5a),*

*wenn die nüchternen Tatsachen es zulassen (4),*

*wenn jeder im Zusammengehn frei ist, eigenen Ertrag herauszuholen (3),*

wenn dieses Füreinander-Bedeutung-haben auch gemeinschaftlichen Ertrag bietet (2), und

*wenn es wirklich in Eurem Herzen begründet ist (1).*

Ich will Euch nun in den Prozeß mitnehmen, durch welchen Bedeutungen zustande kommen: das Sich-abstimmen. Überall wo Menschen miteinander umgehen, wo sie irgend miteinander zu tun haben, sieht man, daß sie sich aufeinander abstimmen oder das wenigstens versuchen. Sich-abstimmen ist so viel wie Vorbereiten, Abtasten und Prüfgrenzen-setzen in dem Be-deuten.

Ein gutes Bild für „Sich-abstimmen” sind die Orchestermitglieder, die ihre Instrumente aufeinander abstimmen, so daß guter Zusammenklang entstehen kann. In diesem Prozeß des Sich-abstimmens auf dem Weg zur Bedeutung ist der Mensch selber das „Instrument”.

2\. *Bedeutung empfangen: Sich-abstimmen, Verstimmen, Einstimmen*

Wie kommt ein Mensch zu Bedeutungen? Erst empfangt Ihr sie, und jedes Kind stimmt sich darauf ab, mindestens solange die Bedeutungen nicht einander widerstreben.

An meine Kinderjahre habe ich warme Erinnerungen. Ich bin mit Liebe empfangen worden. Aber es gab Mühseligkeiten, die mich geprägt haben.

Als ich zwei Jahre alt war, verließ mein Vater unser Haus in *Oegstgeest*. Vater und Mutter konnten einander nichts mehr „bedeuten”. Da vermietete meine Mutter Zimmer, um Einkommen zu haben, und ging arbeiten, wodurch meine Schwester und ich für Mittwochnachmittag zu einer Aufpaßadresse gingen. Jeder zu einer anderen.

Ich kriegte mit „fremden” Menschen zu tun.

Etwas älter geworden entdeckte ich, wie sehr meine Großeltern beiderseits in Lebensweise und Auffassungen verschieden waren: auf der väterlichen Seite christliche Dorfleute, auf der mütterlichen sozial-demokratisch orientierte Stadtmenschen.

Unsre Familie verzog nach Rotterdam, so daß Opa und Oma van Leening zum zweiten Zuhause wurden, weil Mutter außer Haus arbeitete. Aber in allen Ferien wohnten wir in *Katwijk aan Zee* bei Opa und Oma van der Schee. Da ging es ganz anders zu.

Meine Schwester und ich haben gelernt, die Gegensätze zu überbrücken.

Hinterher kann ich sagen, daß ich die Gegensätze – auseinanderlaufende Bedeutsamkeiten – auf die Dauer als sich einander ergänzende verstehen konnte. Die beiden Familien gaben uns den Raum, weil es hin und her Respekt gab, wenn auch nicht ohne Reserve. So konnte ich Erfahrungen und Gefühle integrieren. Das ist nötig, um innerlich in Harmonie zu kommen.

In unseren Rotterdamer Jahren machte ich neuerdings mit, wie viel Mühe es kostet, sich mit jedermann in der Umgebung abzustimmen. Meine Schwester und ich waren die ersten Jahre täglich bei Opa und Oma van Leening. Tante Annie, Omas Schwester, wohnte auch da, eine Quelle des Ärgers. Und sehr oft kam Tante Jo, die älteste Schwester meiner Mutter, vorbei. Sie hatte Wutanfälle, Kummer, machte auch viel Streit. Sie ließ sich von ihrem Mann scheiden, das dauerte mehr als vier Jahre. Am Ende wurde sie krank und starb. Zu der Zeit verstarb auch Oma van Leening. Fast ein ganzes Jahr lang lag sie krank im Hinterzimmer. Insgesamt heftige Ereignisse, wobei manchmal in Vergessenheit geriet, daß Kinder dabei waren.

Demzufolge habe ich früh erfahren, daß zu dem Sich-abstimmen so viele Facetten gehören. Das hat mich beeinflußt. So lernte ich die Balance zwischen Kritik und Solidarität, zwischen Vorsicht und Vertrauen halten.

Im zweiten Jahrzehnt unsres Lebens waren meine Schwester und ich mehr im eigenen Haus. Oft allein, denn Mutter arbeitete auch mittags und abends. Wir mußten einen Teil der Hausarbeit übernehmen, es gab strenge Lebensregeln, wie Gerummel aufräumen und zeitig zu Bett gehn. Man hielt es sehr genau mit dem Sich-abstimmen, Absprachen (eigentlich Aufträge) mußten ausgeführt werden.

Sich-abstimmen verlangt, daß ein Mensch sich an sein gegebenes Wort hält.

Ist das Sich-abstimmen nicht leicht, wird man selektiv: nicht offen für alles, manchmal verschlossen. Von hinterher gesehen wählte ich aus, was ich in eigener Hand halten konnte. Ich wurde zum Lese- und Musikkind. Ich lernte gut Blockflöte spielen. Beim Lesen und der Musik kann man sich selbst zurückziehen.

Vor allem das Lesen gibt Einsicht, wie andere Menschen ihre Gegensätze auflösen, während man selbst in den Dingen nicht befangen ist. Mit dem Musizieren ist es anders. Man muß sich selber hören lassen, man muß sich selber abstimmen. Schöne Musik ist Resultat gelungener Abstimmung.

Ich spielte jahrelang in kleinen Musikensembles, das gab mir große Befriedigung. Vermutlich auch innere Entspannung. Wir spielten oft Musik, die durch die *Vereinigung für Hausmusik* herausgegeben war. Die Vereinigung organisierte Jugendcamps. Zwischen meinem elften und achtzehnten Jahr habe ich jedes Jahr daran teilgenommen. Natürlich waren das die Jahre, wo ein Mensch entdeckt, daß er jemand ist. Die Musikwochen haben mich stark geformt. Sicher war Musik die Hauptsache, aber ebenso ging es doch um die junge Gruppe als Lebensgruppe und Freundesgruppe.

Ich entdeckte, was im Leben der Mühe wert ist, mein Gefühl für den Eigenwert wuchs. „Folglich” entwickelte ich Ideale. Ich hörte von Krishnamurti, der die Menschen in denselben Zimmern angesprochen hat, wo wir musizierten, und ich hörte von Kees Boeke, von dessen Schülern wir Anleitung kriegten. Sie vergegenwärtigten Ideale, über die mit Begeisterung gesprochen wurde. Es ging um weltweites Sich-abstimmen! Mein Verlangen nach Sich-abstimmen richtete sich mehr und mehr auf die Gesellschaft. Zuhause in Rotterdam war genug zu tun. Ich kam in Kontakt mit der Friedensbewegung: dem *Comité 1961* für den Frieden, Demonstrationen gegen den Krieg in Neu Guinea.

Als ich 18 Jahre alt wurde, meldete ich mich als Mitglied der politischen Partei P.S.P. (*Pazifistische sozialistische Partei*) und wurde später Kriegsdienstverweigerer. Es war eine Zeit, in der Prinzipien eine große Rolle spielten. Ich eiferte für den Frieden, debattierte und übte mein Denken. Wie kommt es, daß die Gesellschaft so wirkt, wie sie wirkt? Mit einiger Leidenschaft erprobte ich den Unterschied zwischen Gut und Böse. Waren die Menschen eigentlich ehrlich? Hielt man sich an Absprachen? Falsche Abstimmung in der Gesellschaftdagegen war ich allergisch.

Bei Oma und Opa van der Schee hatte ich die Kinderbibel von vorn bis hinten gelesen. Ich entdeckte nun die Bedeutung des Glaubens als Wegweiser und Maßstab, wo es um Lebensprinzipien geht. Die biblische Geschichte ist eine Forschungsreise zu guter Abstimmung zwischen den Menschen. Im Jahre 1964 wurde ich Mitglied bei der *Remonstratenser* Gemeinde von Rotterdam (sicher auch ein bißchen deshalb, weil es gut anstand, als noch nicht anerkannter Kriegsdienstverweigerer einer Kirche anzugehören).

Sich auflehnend, was die Verhältnisse zwischen den Menschen betrifft, eine idealistische Einstellung. Das machte mich reif für die Lektionen von Ko Vos. Ich lernte bei ihr auf der Dalton-HBS, danach fünf Jahre auf der Lehrerbildungsanstalt (1960-1965).

Das erste, was sie ihre Schüler lehrte, bestand in ihrem eigenen Auftreten. Sie paarte Fachkenntnis mit Menschenkenntnis, gegründet auf Liebe zum Fach und aufrichtige Aufmerksamkeit für die Schüler. Sie konnte ebenso gut sprechen wie zuhören, sie konnte eisig nüchtern sein. Danach stellte sie ihre Prioritäten und traf sehr gut den Kern der Sache. Wir merkten, daß Worte und Taten bei ihr übereinstimmten, dasselbe erwartete sie von uns.

So war sie ein effektives Vorbild des Sich-abstimmens! Es machte anschaulich, daß Autorität-aufbauen etwas anderes ist als Macht-ausüben.

Ihre Lehrstunden betrafen die Lebensanschauung. Das schloß, zusammen mit Geistesgeschichte der Menschheit, an mein Interesse für Gesellschaft, Geschichte und Gottesdienst an. In der Mitte stand die Frage, wie Menschen den Sinn ihres Lebens finden. Das scheint in alten Zeiten nicht in erster Linie mit Ideologie, sondern mit praktischen Problemen des Lebens zu tun gehabt zu haben. Bei den primitiven Völkern war es an die Erklärung der Naturerscheinungen und wie die Menschen darauf zu reagieren hatten gebunden. Eigentlich ging es um die Frage, wie man überleben kann. Und auch später: im Krieg zwischen Griechen und Trojanern, in der Geschichte des jüdischen Volkes (denkt doch an Abraham und Isaak) drehte es sich immer um Fragen von Tod und Leben.

Im Kern ging es darum, welche Bedeutung den Ereignissen erteilt werden mußte.

Unterdessen wurde mir deutlich, daß diese Geschichten samt Bedeutungen nicht unabhängig voneinander, sondern in großem Zusammenhang stehen. Das ist die Straße, auf der sich die Menschheit einen Weg zu Sicherheit, Bildung und Kultur erarbeit.

Eines Tages fragte ich „Fräulein Vos”, woher sie ihre Kenntnisse hatte. Und es kam heraus, daß sie ganz in die Bücher Rosenstock-Huessys vertieft war. Interessierte Studenten wollte sie gern in ihr Studium einbeziehen.

So besuchte ich die Gesprächsabende bei ihr zuhause. Und kaufte mein erstes Buch von Rosenstock-Huessy! Auf Niederländisch: „De onbetaalbare mens”. Schwierig! Aber es förderte mein Interesse, denn was ich da über gesellschaftliche Verhältnisse las, Entwicklung und Hintergründe, gab mir total neue Einsichten.

Hier fing die Orientierung auf das Leben an, mit der ich noch immer beschäftigt bin.

3\. *Bedeutung geben: Beteiligt-sein – Einsatz und Abwägen*

Ich wurde Lehrer, mußte aber noch Militärdienst leisten. Wer dagegen Gewissensnot hat, hat die gesetzliche Möglichkeit, das kundzutun. Wird sie anerkannt, kriegt man eine Alternative. Ich wurde als Kriegsdienst-verweigerer anerkannt und arbeitete in einem Internat für schwererziehbare Jungen. Anschließend an die Dienstpflicht studierte ich Orthopädagogik an der Universität Amsterdam. Daneben habe ich gearbeitet.

Meine Interessen an Geschichte, meine Ideale von der Gesellschaft waren umgeschaltet auf die Praxis der Geschichte der Kinder, mit denen wir zu tun hatten, die Ideale wurden konkreter auf Erziehungsprobleme gerichtet, die sich bei den Kindern anzeigten.

1976 schloß ich das Studium der Orthopädagogik mit der Schrift: „Zeit und Gegenseitigkeit auf dem Weg zu einer Theorie des Helfens”. Deutlich zeigt sich darin der Einfluß Rosenstock-Huessys.

Wie soll ich nun diesen Einfluß aufweisen? Allgemein war selbstverständlich, daß ich Tatsachen, Ereignisse und Aufgaben in meinem Fachgebiet nicht als auf sich selbst stehende Gegebenheiten verstand, sondern doch nach ihrem *Zusammenhang* suchte. Was mit Menschen los ist, sah ich in Zusammenhang mit ihren Beziehungen und Umständen, ihrer Geschichte und Entwicklung.

Die wissenschaftliche Pädagogik von damals unterstützte mich bei diesen Einsichten, aber es gab doch einen Unterschied. Wo die sozialen Wissenschaften dazu neigen, die Gegebenheiten in systematischen Zusammenhang zu bringen – die Systemtheorie stand damals ganz in Aufmerksamkeit – blieb für mich doch sonnenklar, daß man einen Menschen aus dem System, in dem er lebt, nicht erklären kann. (So blieb ich auf der Fußspur Ko Vos.)

Ebensowenig kann Hilfe für Menschen vollständig systematisiert werden: etwa als auf der Tafel entworfene Strategie oder Technik. Denn zunächst ist Abstimmung zwischen Menschen etwas von innen nach außen Kommendes, von dem Innersten hat aber keine einzige Wissenschaft eine vollständige „Karte”, und zudem ist die Abstimmung selber nichts „gesetzmäßig” Verlaufendes.

Wie Erziehung und Hilfeleistung tatsächlich sind, darüber kann man wohl mit allem Rechenschaft halten, was die Wissenschaft anträgt, aber im Kontakt zwischen den Betroffenen, in der Bedeutung hin und wider, spielen wieder die Faktoren von innen her und die direkt wirkenden Umstände eine unvorhersehbare Rolle. Allgemein ausgedrückt: was sich von innen her formt, kann nicht von außen her gesteuert werden, es sei denn, man faßte Menschen als zu dressierende Affen auf.

Diese Einsicht beruht auf eigenen Erfahrungen, Rosenstock-Huessy hat diesen wichtigen Punkt aber sehr vertieft.

Die Wissenschaft auf meinem Gebiet müht sich um Erziehung und Hilfeleistung, aber ich fand sie nur von begrenztem Wert. Vielleicht weil ich bis 1974 schon in der Praxis war.

Noch immer finde ich das.

Es gibt eine Spannung im Verhältnis zwischen Praxis und Wissenschaft. Worin besteht sie? Man muß „subjektiv” (persönlich, von innen her) engagiert sein, wenn man jemandem helfen soll, aber ebenso ist es nötig, „objektiv” völligen Abstand zu halten zu dem, was die „Arbeit” ist.

Wie verbindet man das miteinander?

In dem kleinen Kreis von Menschen, mit denen ich die Bücher Rosenstock-Huessys las, fand ich keine Stütze. Eher stieß ich auf Widerstand gegen die Optik der Wissenschaft. Hatte Rosenstock-Huessy nicht geschrieben: „I am an impure thinker”? Es wirft ein Licht, daß wir um 1982 Information eingeholt haben, ob wir uns ans Rosenstock-Huessy Huis in Haarlem anschließen könnten, eine Art Kommune von Leuten, die die Einsichten von Rosenstock-Huessy in die Praxis umsetzen wollten. Man wies uns ab aus Angst, man könnte ihrer Arbeit eine wissenschaftliche Absicht anhängen (ich war ja ein professioneller Orthopädagoge), dabei muß man die Arbeit doch aus dem Herzen heraus tun. Das fand ich natürlich auch, sah das aber nicht als unvereinbar mit Professionalität und Wissenschaft an.

Und so denke ich noch immer darüber.

Verschiedentlich habe ich versucht, diese Einsicht ins Werk zu bringen, auch in diesem Brief, aber der Geist jener Zeit war gegen mich.

Orthopädagogik und Psychologie sind in den letzten zwanzig Jahren mehr auf den Kurs objektiver Annäherung gekommen. „Objektiv” sein heißt: anderen etwas vorschreiben, was man für „gewiß” hält. Kommt etwas anderes dabei heraus, liegt das eben am Klienten.

In dieser Beurteilung liegt eine unauflösbare Entgegensetzung. „Erziehung” und „Hilfeleistung” betreffen menschliches Verhalten. Die Verhaltensweisen objektiviert man, als wäre Verhalten ein „Apparat”, den man auseinandernehmen und reparieren kann. Aber: das Wirken der Verhaltensweisen findet man nicht *in* den „Unterteilen”, sondern *dazwischen*.

Ein Psychologe kann zum Bespiel richtig feststellen, es sei jemand voller Versagensangst, aber was sagt das zu der Frage, wie Versagensangst (die wiederum zwischen Menschen auftritt) überwunden werden kann?

Beim Arzt sind wir mit einer objektiven Beurteilung zufrieden, der Arzt untersucht Unterteile eines nicht gut funktionierenden Leibes. Ein Pädagoge ist aber kein Psychologe oder Arzt. Eine Mutter, die für die Erziehung Hilfe sucht, fragt im wesentlichen, wie sie trotz der Probleme die Verbindung zwischen sich und dem Kind handhaben und kräftigen kann. Die Frage „wie muß ich das Gute tun?” ist eine ganz andere Frage als „was ist los mit meinem Kind?”

Ein guter pädagogischer Rat umfaßt mehr als das Rezept, das „eingenommen” werden muß. Der Pädagoge soll nicht nur Schlüsse aus der objektiven Lage ziehen, er muß sie aus einer intersubjektiven Verfassung heraus finden. Das heißt: er muß Verständnis dafür haben, was Mutter und Kind von innen her bewegt, wie die Abstimmung untereinander ist. Das geht nur mit wirklichem Kontakt mit den Betroffenen, denn nur dann ist Einsicht in die Übertragung von Bedeutungen möglich, wie sie zwischen jenen eine Rolle spielen.

Bei einer Beratung geht es schließlich nicht um das Recht des Pädagogen, sondern darum, wie er imstande ist, die Erziehenden zu den benötigten Einsichten und Werten zu bringen. Der Pädagoge stützt sich auf einen anderen Typus von Wissen als der Psychologe.

Es geht um den Unterschied zwischen konstatierender und Einsicht gewährender Kenntnis.

Rosenstock-Huessy hat mir enorm geholfen, diesen Unterschied zu sehen.

Im Rest des Briefes will ich versuchen, davon etwas zu verdeutlichen.

***4\. In Verhältnissen***

Wer sich abstimmt auf einander, ist auch von innen her in Beziehung gebracht. Man gibt etwas von sich selbst. Heißt das nun, daß nur Abstimmung das Persönliche zum Ausdruck bringt? Manchmal wohl, aber nicht immer. Abstimmung kann auch Wiedergabe von „etwas” sein, das Bedeutung hat. Etwas von Bedeutung vorbringen schließt auch ein, daß man den Gehalt des Gesagten oder Getanen persönlich qualifiziert hat, aber gleichzeitig etwas mitvergegenwärtigt, das mehr allgemein Bedeutung hat. Man stellt selber eine Qualität menschlichen Verhaltens dar.

Dieser Brief beleuchtet das. Ich habe etwas gelernt und will es gern weiterreichen, damit es Kompaßteil wird, mit dem Ihr im Leben auf Fahrt gehen könnt. Der Brief ist meine Initiative und insofern persönlich, aber daß ich meinen Kindern etwas von Wert mitgeben will, ist allgemeine menschliche Erscheinung. So geht es doch oft zwischen Vätern und Kindern. So ereignet sich „das” Leben nun einmal.

Von der Frage, wie „das” Leben in jedem wühlt – ansteckend und auflodernd – bin ich umgetrieben. Die Frage geht tiefer als was in und zwischen uns spielt. Es geht auch darum, was durch uns beide – das Persönliche übersteigend spielt.

Was hat man wie jeder, der lebt, erfahren? Daß Du Tochter bist oder Sohn, Freundin oder Freund. Kein Mensch steht nur für sich. Wir leben in Verhältnissen. Nennen wir sie „Zellen”. Bedenkt: wenn Ihr sagt, ich bin Mensch, ist es nur eine undeutliche Umschreibung von dem, wie Ihr seid. „Mensch” ist der Hinweis auf eine Sorte. Es sagt noch nichts über Euch. Deutlich werdet Ihr erst,

*wenn Ihr sagt, wie Ihr heißt,*

*zu wem Ihr gehört, welchen Beruf Ihr habt*

*und dergleichen.*

In all diesen Fällen weist Ihr darauf hin, worauf Ihr als Menschen bezogen seid. Als Bezogene seid Ihr dann Unterteile einer „Zelle”. In den Zellen findet „das” Leben als menschliches – humanes – Leben statt. Selbst wer einsam und allein ist, trägt noch den Namen als Hinweis auf die Familie, kann von seinem Leben erzählen. Und Erzählung erzählt zweifellos davon, zu wem und zu was man gehört hat.

Daß jeder Mensch für sich steht, ist wahr, denn immer ist man das Selbst, das „eigenen” Anteil all den Zellen gibt, und man muß selber nachdenken, was man wählt: was man gut kann und willund was man nicht kann und will. Aber wenn es dabei bleibt, daß man darüber nachdenkt, kann man sich als Mensch nicht verwirklichen. „Das” Leben leben heißt in etwas einsteigen, das mit anderen zusammen der Mühe wert ist. Dabei gibt es zwei Werte als Voraussetzung. Den einen, daß die Menschen, die miteinander zu tun haben, sich gegenseitig in ihrem Wert belassen. Den anderen, daß es in der „Zelle”, die man zusammen bildet, auch um etwas gehen muß, das für das Leben als solches Wert hat.

Das Leben, „als solches”? Das Leben spielt sich in Zellen ab. Jede Zelle hat ein Leben von innen nach außen, beruht auf der Basis von Menschen, die aufeinander bezogen sind, gegenseitig sich ihren Wert lassen und den Wert von jedem Beteiligten noch mehren. (Wie zum Beispiel Kinder den Wert des Lebens ihrer Eltern vermehren.)

Die Zelle lebt auf der Basis dieses Binnengeschehens.

Aber jede Zelle besteht auch dank des Sich-abstimmens auf die anderen Zellen: denn „Leben” ist ein großes Ganzes. So muß sich die Zelle „Familie” abstimmen auf die Zelle „Schule”, die Zelle „Schule” auf die Zelle „Berufsgruppe” usw. In jeder Zelle muß das Sich-abstimmen mit anderen Zellen garantiert werden, damit die Verbindungen im Leben als Ganzem fortbestehen können.

Dieses Außengeschehen gibt jeder Zelle eine „Sache”, die zusammen gehalten werden muß, weil sie sonst die Verbindung mit dem Ganzen verlieren.

Erziehung, Lehre, Hilfeleistung usw. haben also eine „Sache” zu beherzigen. Zum Beispiel kann ein Lehrer nicht unbedingt lehren, womit er sich persönlich beschäftigt, sondern er oder sie muß auslegen, wofür er oder sie angestellt sind. Wohl spricht der gute Lehrer aus eigener Qualität, füllt es persönlich aus, doch er oder sie kann die „Zelle” nur zusammenhalten, wenn die Schüler in dem unterrichtet werden, wofür sie auch kommen.

So bestimmend spielt sich das Leben in menschlichen Verhältnissen ab, daß es Illusion ist, einen Menschen nur auf sich selbst gestellt kennen zu lernen. Nehmen wir an, daß jeder eigene Merkmale hat. Wir prunken förmlich mit Etiketten: ist sportlich, hat Sinn für Humor, ist musikalisch, eifersüchtig, gierig, ist Neurotiker, ist beschränkt, schlecht entwickelt, debil usw. Aber wenn man dann jemanden mit all diesen Andeutungen „plaziert” hat, kennt man ihn etwa als Person?

Will man jemanden für sich selbst kennen lernen, trifft man eine Feststellung. Nicht ausgestreckte Hand, sondern kühler Blick. Keine Begegnung, sondern Beurteilung. Feststellungen erbauen keine Zellen, sondern schaffen Abstand.

In einem Verhältnis kann ja vorkommen, daß jemand Neurotiker ist, ziemlich debil, eifersüchtig usw., aber das ist nur ein einziger „Faden” in einem viel größeren Gewebe.

Natürlich erfahren Menschen voneinander, wie sie in den einzelnen Verhältnissen sind, „tu nicht so debil!” kann man sagen, aber schon wenn man das tut, beruft man sich auf einen Begriff des anderen, dass er doch erkennbar mehr als nur debil ist.

Mit unseren „Eigenschaften” steht es so, daß sie sich den Möglichkeiten und Umständen des Verhältnisses anpassen!

Wenn eine Feststellung definitiv über allem steht, zerbricht die Zelle, denn da wird fixiert und durch Fixieren einer Veränderung und eventuellen weiteren Entwicklung entzogen. Das Leben der Zelle bricht ab, oder die so fixierte Person bricht ab, wird kaltgestellt. Das gilt nicht nur von negativen Feststellungen. Wen man nur „lieb” findet, ist auch damit ausgeschaltet.

Das Leben spielt sich in „Zellen” ab. In meinem Fach sprechen wir von „Kontexten”. Die Zelle oder der Kontext ist die Lebensform, worin sich ein oder mehrere Verhältnisse abspielen.

Betrachtet Ihr zum Beispiel Eure Freunde, verweist das auf konkrete Verhältnisse, aber das Wort „Freundschaft” weist daneben auch auf die Form hin, worin dieses Verhältnis besteht.

Als Form wirken zum Beispiel die Erwartungen, die in einer Kultur bestehen, wie Menschen eine Freundschaft erfüllen sollen.

In konkreten Verhältnissen tun sich Veränderungen und Entwicklungen schneller hervor als im Kontext. Nehmt das Verhältnis zwischen Eltern und Kindern: zwischen Kindergarten und Pubertät sieht das Verhältnis ganz anders aus, aber der Kontext „Familie” ist noch derselbe. Das kommt daher, daß die „Sache”, um die es geht (Erziehung) weitergeht.

Durch den Kontext schon für sich laufen sachliche wie persönliche Linien. Auf allen Linien ist Verkehr. Jeder Verkehr bedarf des Sich-abstimmens miteinander. Es muß Verkehr in beide Richtungen sein.

Auf die Art des Verkehrs muß ich noch tiefer eingehen.

In allem, was Abstimmung erfordert, kommen zwei Bestandteile verschiedener Herkunft vor: immaterielle und materielle. Der immaterielle kommt zum Beispiel in unseren Auffassungen, Sympathien und Antipathien zum Ausdruck; der materielle weist auf das leibliche Bestehen und die natürlichen Bedingungen der Welt, worin wir leben. (Leib und stoffliche Welt stehen auf einer Linie. Sie sind Welt-Teile.) Man fasse diese Bestandteile bitte nicht als Wiedergabe des Gegensatzes auf, der im vorigen Absatz zwischen persönlichen Verhältnissen im Kontext und der Sache, die da beherzigt wird, gemacht wurde! Denn persönliche Verhältnisse wie Sachen umfassen beide Bestandteile. Jeder Mensch, jedes Verhältnis zwischen Menschen, jeder Kontext und jede Sache in diesem Kontext umfaßt eine Mischung aus materiell und immateriell. Die letzte repräsentiert, um es mit traditionellen Worten zu sagen, der Geist, in dem wir leben.

Das Verhältnis zwischen Materie und Geist bildet einen zentralen Punkt im Denken Rosenstock-Huessys. Auf alle mögliche Weise arbeitet er diesen Punkt aus. Er ist der Meinung, daß es gegenwärtig den Menschen an Bewußtsein des Geistes oder der Geister mangelt, aus denen sie leben. Der Geist meldet sich in allen Fragen, die bei dem Sich-abstimmen aufkommen, mit dem gemeinsamen Nenner: „Worum geht es eigentlich?”

In aller Schärfe muß es ihm aufgekommen sein, als er als Offizier des deutschen Heeres am Ersten Weltkrieg teilnahm. Die Frage schien nicht leicht zu beantworten: Der Krieg muß gewonnen werden. Gutaber worum geht es in diesem Krieg? Die Antwort darauf ist schwieriger. Noch schwieriger wird die Antwort, wenn Witwen und Müttern und Kindern mitgeteilt werden soll, daß Sohn oder Vater gefallen sind.

Und diese Frage steht ja nicht nur im Geschichtsbuch, sondern geht auch heute als ein Zittern durch uns hin!

Rosenstock-Huessys Schluß ist, daß in fast allen Abstimmungen zwischen Menschen heute etwas beschlossen ist, worin übermächtig ist, daß Menschen zu „Material” geworden sind. Das ist der herrschende Geist. Darum geht es bei der Ideologie: Papa hat sein Leben hingegeben, um dem Irak die Demokratie zu bringen.

Mit solchem Trost wird der Geist gekleidet, aber nackt und bloß habe ich eher den Gedanken, daß der Freie Westen seinen Wohlstand sichern will. Wohlstand – Materialismus – unser „Gott”?

Was stimmt bei diesem Geist nicht? Seinem Wesen nach zerteilt er das Vermögen der Menschen, miteinander in Verbindung zu treten. Einerseits werden die großen „Sachen” in den Kontexten des Zusammenlebens durch Eigeninteresse geleitet. Ein Soldat der USA muß einswerden mit der großen Sache der Demokratie und – wenns sein muß – das Opfer seines Lebens bringen. Andrerseits paßt das nicht zu den kleinen Sachen der Familien und Lebensverbände, wo der Soldatentod der Geliebten Unglück bringt.

Kurz gesagt: wenn das Glück des großen Kontexts das Unglück des kleinen ist, wird die Verbindung zwischen beiden Kontexten belastet.

Rettung kann für den kleinen Kontext sein, daß er materialistisch wird: genieß das Leben und ergreife, was die ergreifen kannst. Und das wurde immer mehr zur Lebensweise, wo jeder für sich selbst besteht. Ein zerteiltes Zusammenleben.

Rosenstock-Huessy stellt dem gegenüber, daß die menschlichen Verhältnisse im Kern zusammenhängen, damit der eine für den anderen einsteht. „Einsteht” bis zum Widernatürlichen, nämlich daß das Interesse des anderen vor das Eigeninteresse geht. Denn In ihren Verhältnissen bestehen Menschen eben nicht für sich selbst. Und auch jeder Mensch mit einiger Persönlichkeit besteht aus dem in der Erfahrung gefundenen Zusammenhang zwischen Leib und Geist

Im Verhältnis zwischen Geist und Leib entbrennt leicht ein Krieg, wo der Leib herrscht. Weil die Menschen einander nur gebrauchen. Da herrscht das „Homo homini lupus”. Das nötige Gegengewicht ist, daß Menschen den Geist der Zusammengehörigkeit verleiblichen. Es ist der einzige Weg, auf dem die Sachen aller Kontexte der Welt zur Harmonie miteinander gelangen können.

Der Geist, der vereinigt, hat von altersher Zusammenarbeit und Friede zwischen den Menschen gebracht.

Viele Bücher Rosenstock-Huessys handeln davon.

***5\. Verhältnisse in „horizontaler” Perspektive***

***(von innen und außen)***

Da ich als Orthopädagoge anderen beistehen muß, die praktisch arbeiten, statt selber zu erziehen, zu lehren und Trainingsseminare abzuhalten, wurde ich mir schärfer des Gegensatzes zwischen In-der-Praxis-stehen, Im-Kontext-mitarbeiten und Von-außen-her-die-Praxis-betrachten und darüber Regie führen bewußt.

**Wenn man draußen steht, merkt man nicht mehr, wie Menschen sich aufeinander abstimmen, was sie dabei fühlen, auf welcher Grundlage sie ihre Wahl treffen usw. Man sieht wohl Ergebnisse. Man sieht, ob etwas gut geht oder nicht, aber das ist nicht dasselbe wie zu wissen, „wie es sich anfühlt”. Wenn man aber nicht weiß, was die Menschen bewegt, ist es schwer, Rat zu geben.**

Nehmen wir zum Beispiel, daß man als Sachverständiger einem Gruppenleiter den Rat gibt, in eine Lage, wo ein Kind das andere immer ärgert, direkt einzugreifen,. Es kann ein guter Rat sein, aber man steht doch nicht in den Schuhen des Begleiters. Der muß entscheiden, wie er eingreift, wann der Augenblick da ist. Böser Ton, oder sachlich? Mit einem Witz oder mit ausführlicher Darlegung dessen, was verkehrt ist? Die Frage ist: was kommt zu denen rüber, die den Ärger machen?

Der wissenschaftliche Ratgeber soll persönlich in die Verhältnisse einbezogen sein, um Rat geben zu können.

Aber ich stehe da draußen.

Wie kann man von außen einen sinnvollen Rat geben? Worauf beruht solcher Rat?

Schematisch gesagt: wer bei der Einwirkung nicht einbezogen ist, muß sich auf die Auswirkung dessen stützen, was zwischen den Menschen geschieht. Zum Beispiel kann sich der Ratgeber darauf gründen, daß das Ärgerkind aus der Gruppe wegläuft, zu der es gehört. Dann fällt es aus dem Kontext heraus und entzieht sich der Sache, von der es Nutzen haben soll. Was in der Lebensgruppe eines Kinderhauses nicht angeht. Da muß dann am Kontext etwas verändert werden.

Zu der sachlichen Seite des Kontexts kann von außen her schon Rat gegeben werden. Der Ratgeber stellt seine Fragen. Wurde dem geärgerten Kind der Eigenwert genommen, bringt es selbst etwas in die Gruppe, worauf sich niemand abstimmen kann usw. Je nach der Antwort entsteht eine Idee, was zur Verbesserung geschehen könnte. Nicht als Rezept, sondern als Möglichkeit, die auszuprobieren ist.

Im Laufe meiner Arbeit habe ich gelernt, daß ein Problem zwischen Menschen sich sehr wohl in einem bestimmten Verhalten ausdrückt, aber aus dem Wirken mehrerer Kontexte unterhalten sein kann. Das geärgerte Kind aus meinem Beispiel gibt, so schien es mir, Anlaß zum Ärger, weil es stinkt. Dann liegt das Problem nicht allein in der Aktivität, die den Ärger macht, sondern verweist auf den Kontext der Fürsorge und primären Erziehung.

Es gehört zur Professionalität des Sachverständigen, alle mitspielenden Momente zu identifizieren. Anleitungspunkt kann eine „Sache” sein oder die Einrichtung des Aktivitätenraumes; so gibt es vieles mehr.

Danach schauen – von außen her – liefert nützliche Kenntnis, solange man nicht meint, damit die ganze Sache schon geklärt zu haben. So wird objektiviert. Das heißt: man grenzt das Gebiet ab, worin etwas geschehen ist, geht selber einen Schritt zurück, konzentriert sich auf das Geschehene, setzt das Geschehene auf einen Film, spult den Film ab und zieht dann den Schluß, wie das eine das andere zuwegebringt. Kann solcher Schluß gezogen werden, bringt man einen Vorschlag, wie die Kette von Ursache und Wirkung verändert werden kann.

Solange im Umgang zwischen den Menschen wenig Arbeitsteilung und Spezialisierung ist, kommt es vor, daß jemand Erzieher (Gruppenleiter) und Filmer (Sachverständiger) zugleich ist. Dann gibt es eine logische Verbindung zwischen allem, was man tut, denn man erzieht, filmt, was nicht gelingt, entdeckt, wo der Schuh drückt, und wendet das Wissen anund danach gelingt es besser. Aber wenn die Probleme größer werden oder wenn das Aktionsfeld unübersichtlich ist, entsteht Arbeitsteilung. Dann erzieht der eine, und der andere filmt und trägt die Problemlösung an.

Besonders wo es um schwierige menschliche Verhältnisse geht, ist es schwer zu „filmen”, was man selber gerade tut. Denn das hat ja zur Voraussetzung, daß man auch selbst in allem, was man tut, den Kontext durchschaut. Hat man es aber im Kontext schwer, gelangt man nicht so gut in die objektive Position von außerhalb des Kontexts, man nimmt ja Vorurteile mit und kann nicht unbefangen schauen.

Dann taugen die Filme nicht mehr.

Andrerseits hat auch der spezialisierte Filmer ein Problem, denn wer nur filmt und nicht mehr erzieht, dem mangelt es an Beziehung, Kommunikation, an Kontakt innerhalb des Kontexts. Wer also filmt, sieht den Zusammenhang von Ursache und Folge und sagt: „Das ist es!” Aber Vorsicht!der Filmer hat längst nicht alles gesehen, hat nicht alles verfolgen können, denn er steht von innen her nicht mehr in Beziehung. Sein Rat kommt ihm selber nicht schlecht vor, aber er betrifft nur ein *etwas* im Kontext.

Wie verhält sich dieses „etwas” zu all dem anderen? Und weil alles andere auch zählt: was ist sein Rat dann wert?

Um 1980 arbeitete ich in einer jugendpsychiatrischen Klinik. Die Sachverständigen, von denen ich einer war, hatten die Neigung, immer mehr „Filme” zu machen. Alle Spezialisten filmten aus ihrem unterschiedlichen Sachverständnis heraus. Das löst die Frage nicht, wie die Ratschläge in den alltäglichen Umgang zwischen Gruppenleitern und Kindern integriert werden könnten. Es war ein Glück, daß wir begriffen: die Integration müsse zustandekommen in einem Gespräch zwischen „Filmern” und „Erziehern”.

Nicht nur Frucht der Ratschläge von „außen” ist das endliche Resultat", sondern vor allem die Einwirkung von innen her.

Die Ratschläge wurden oft angenommen, aber wirkten nur auf Teilaspekte des Umgangs, während doch das Ganze des Umgangs die innerliche Veränderung zuwege bringt, die nötig wird, damit das Kind aus sich selbst an den Kontexten teilnimmt, in denen es lebt.

Nicht die Filmchen tun die eigentliche Wirkung, sondern die Begegnung zwischen den Menschen, die Konsequenzen für Beziehungen und Kontext hat.

Solches Bewußtwerden enthielt übrigens auch, daß die Gruppenleiter über ihren inneren Bezug zu den Kindern zu sprechen kamen: über ihre Motive – gut und schlecht, denn kein Mensch ist ohne Fehler, subjektiven Geschmack und Vorurteil.

***6\. Verhältnisse in der „vertikalen” Perspektive***

**(zwischen Vergangenheit und Zukunft)**

Nun weiter.

Es gibt noch mehr als die Interaktion zwischen „innen” und „außen, zwischen „Person” und „Sache”. Sonst entstünde der Eindruck, als wär es ein Hin und Her: zwiefach. Aber zumindest geht es um Dreifaltigkeit.

Ihr müßt Euch vorstellen, daß der Leib und die Welt (beide Repräsentanten der Materialität) direkt Einwirkungen offenstehen, unser Geist und der Geist, in dem der Kontext lebt, aber nicht. „Geist” ist mehr als menschlicher Verstand und sachliches Kaliber. Beim Verstand geht es ums Denkvermögen. Ein geübter Verstand ist direkt auf sich selbst zu beziehen: 2x2=4. Bei einer Sache geht es um die Funktion des Kontexts in größerem Zusammenhang. Haben wir dagegen mit dem „Geist” zu tun, ist das Vermögen gefordert, Bedeutung zu verleihen. Dieses Vermögen geht hervor aus allem, dem wir schon Bedeutung verliehen haben, wie aus dem, dem wir noch gerne Bedeutung verleihen wollen.

So gesehen gelingt es besonders bei der Eigenheit jedes Menschen. Und die Eigenheit wirkt in Verhältnissen und in den Kontexten, worin man lebt.

Der Geist des Kontexts ist anzutreffen in der Bedeutung, die die davon miteinander Betroffenen den Sachen, die sie instand halten und voranbringen, erteilen.

Geht es nun um etwas von Bedeutung – Person, Verhältnis oder Kontext -, ist mehr zu bedenken, als jenes Zwiefache von außen nach innen und von innen nach außen. Denn alles, was einen trifft, was Bedeutung hat, woran man die eigene Bedeutung heftet, was einen in Persönlichkeit, Identität oder Verhältnis in Bewegung bringt, muß verwirklicht werden.

Darum ist Sich-abstimmen kein Zwie-, sondern ein Dreifaches: (1) Einwirken, (2) Verwirklichen (im Innern wachsen lassen) und (3) Auswirken.

Schreibend vom Geist schließe ich mich der alten Weisheit an, die besagt, daß jeder Mensch bei der Geburt den Geist empfängt und im Sterben den Geist von sich gibt. Damit ist angedeutet, wie sehr jeder Mensch Unterteil des in menschlichen Verhältnissen herrschenden „Geistes” ist. Später geht es vielleicht um den Eigenwert, anfangen tut es damit, daß man ihn empfängt. Im Laufe des Lebens gibt man ihm die eigene Füllung.

Außer dieser persönlichen hat der Geist auch eine „sachliche” Seite, nämlich daß er eine Qualität des menschlichen Lebens als solchem ist. Denn wo man Wert hat, verkörpert man nicht nur sich selbst, sondern gleichzeitig Werte, die das menschliche Leben selbst zu einem Wert machen.

Zum Beispiel: man hält sich an ein Versprechen. Einerseits kommt es ganz auf einen selber an, andrerseits repräsentiert man den allgemein menschlichen Wert, „zuverlässig” zu sein.

Was ist „Verläßlichkeit”? Davon hat niemand Kenntnis von sich aus. Willkürlich kann man nicht selbst füllen, was „verläßlich” bedeutet, denn der Wortgehalt wurzelt im Zusammenleben, im Ganzen der menschlichen Verhältnisse. Was man verspricht, muß man selber auch wahrmachen, denn in allem Begonnenen streckt sich eins zum andren, das das Versprechen empfangen will und so auch Anteil daran nimmt. Im Strecken des Versprechens verkörpert man zusammen die Verläßlichkeit als reale Gestalt der Menschlichkeit. Zuverlässigkeit, wenn man sie so sieht, entfaltet ihre ganze Bedeutung in den Verhältnissen. In tausend Beziehungen kann Zuverlässigkeit auf tausenderlei Weisen vorkommen. Und doch werden alle Varianten als „zuverlässig” erkannt. Das kommt daher, daß im Zusammenleben die Erfahrung aufgetan wird, was in dem„wirkt”, das wir verläßlich nennen. Versuchen wir es unter Worte zu bringen: verläßlich – Einstehen füreinander – die Tat zum Wort fügen usw. Eine einzige Definition schösse daneben, denn es gibt tausend Erzählungen, in denen doch deutlich wird, daß es sich um eine bestimmte „Sache” handelt.

Die Realität des Geistes umfängt uns.

Das Sich-abstimmen, das den Geist in Anspruch nimmt, Euern Geist und zugleich den Geist, in dem Ihr mit anderen Menschen lebt (dem Geist der Arbeitsgruppe, der Familie, der Niederlande usw.), kostet Zeit. Dann es muß ja erwirkt werden: (1) daß es Dich angeht, und (2) daß es in einem durchwirkt, sodaß man es ihm selbst Bedeutung verleihen kann. Gesetzt man ist auf dem Punkt, etwas zu versprechen. Der Geist sucht Verbindung mit Deinem Inneren, wo sich befindet, was sich in Dir an Bedeutungen gebildet hat. Kannst Du das Neue mit dem Früheren in Verbindung bringen?

Neues und Früheres müssen in Zusammenhang kommen, das bringt einen vielleicht zu Entwicklung und Veränderung. Und auch Dein Geist streckt sich nach dem Verhältnis, wo das Versprochene, wenn man es tut, wirkt. Willst Du Dich wirklich auf solche Weise verbinden? Was bedeutet das für *später*?

Dieses Nach-vorne-schauen ist mehr als einfältig, weil es um ein Verhältnis geht (oder die Fortsetzung davon). Und zudem steht ein Verhältnis durchgängig nicht auf sich selbst. Wer zum Beispiel neu Freundschaft schließt, kann feststellen, wie das allerlei andere Verhältnisse beeinflußt oder gar verändert.

Kinder fassen eher schnelle und wenig durchdachte Beschlüsse, aber die Frage der Verwirklichung kommt dann in den folgenden Wochen. Neue Freundschaft kann dauern, aber auch schnell wieder aufgesagt werden.

Auch bei Erwachsenen ist Verwirklichen nicht immer Nachdenken, sondern zugleich: etwas auf Probe tun. Wie auch immer: das Unter-einen-Eindruck-geraten und das (Er-)proben von Verbindungen nimmt den Geist in Beschlag. Das ist schwer greifbar, kostet aber jedenfalls Zeit.

Der Übergang von Verwirklichen zu Auswirken ist gleitend, die Begriffe sprechen für sich selbst. Alles ist „ausgewirkt”, wenn es nicht mehr „neu”, sondern in das Ganze des Bestehenden integriert ist. Im Verwirklichen erhält und gibt man Bedeutung, im Auswirken reagiert man aufgrund dessen, daß man Bedeutung erlangt hat.

Rosenstock-Huessy lehrt, daß alles, was der Abstammung nach leiblich/ natürlich/ stofflich/ weltlich ist, von „räumlicher” Art ist. Es bezieht sich auf alles, was Platz einnimmt und wo man für die Gründe Rechenschaft geben muß. Jeder Mensch nimmt Platz ein – das hat viele Konsequenzen. Es bringt Bewegung zwischen „innen” und „außen”.

Aber um die Realität der menschlichen Verhältnisse und der menschlichen Identität zu begreifen, kann man nicht nur von räumlichen Visionen erfüllt sein. Essentielle Fragen des menschlichen Wertes, der Identität und des Zusammenlebens und wie wir alle einander in Abstimmung Bedeutung geben, werden nicht einbezogen. Um diese Wirkungen zur Geltung zu bringen, gilt es nach der räumlichen Dimension eine andere zu erkennen: die Dimension der „Zeit”.

Bedeutung besteht der Form nach aus Zeit. Denn die einzige Realität besteht darin, daß wir gleichzeitige Bedeutungen vollbringen: die des Selbst und zusammen miteinander. Wir halten daran fest, wie es in *„ik houd van jou* (ich halte zu dir = ich liebe dich)” noch durchklingt. Es verbindet das Frühere mit dem Späteren, es schafft Kontinuität, macht aber auch die Bewegung zwischen „früher” und „später” als Entwicklung und Erneuerung nachvollziehbar.

Jedes Verhältnis zwischen Menschen ist Voraussetzung, die in Dauerhaftigkeit vollbracht wird.

Menschliche Qualitäten: wie verläßlich sein, Geduld haben, Hoffnung hegen usw. haben immer damit zu tun, was wir in der Zeit aushalten und vollbringen. Sich-abstimmen zwischen Menschen, die Tatsache, daß sie einander etwas bedeuten, daß Zusammenhang und Bund bestehen: gut sind sie nur als Bewegungen in der Zeit, als Gestalten der Zeit zu begreifen.

*Leib und Welt müssen sich dazu hergeben, anders geht es nicht.*

Jedes konkrete Ereignis von wirklichem Belang ist daher zu begreifen als Kreuzpunkt räumlichen Austausches und der Wirkungen der Zeit.

Welchen Nutzen hat nun diese Einsicht?

Sie hat mich einiges gelehrt. Woher holen wir die Zeit von heute?

Eine Quelle habe ich schon: meine eigene Zeit. Die gilt für uns alle. Wer wissen will, was für Menschen von Bedeutung ist, muß nicht auf ihre Worte, sondern auf ihre Taten zugehen. Bedeutung hat, was Zeit kostet, wo Menschen sich hingeben. Auch wird deutlich, daß wir auf jede Veränderung und Verbesserung von Bedeutung geduldig warten müssen. Weil es nun einmal Zeit kostet: Zeit, um etwas zu verwirklichen, Zeit, um etwas aufzubauen. Die Haltung: „wird da noch was draus” ist von daher gesehen schlecht. Denn dann steht man ja als das Wahre hinter der Filmkamera und ruft: „Komm zum Vorschein!” Aber so geht es nicht. Man kommt erst zum Vorschein, wenn man sich endlich traut, wenn man dazu bereit ist. Nicht auf Kommando, nicht nach Plan. Wohl können die Menschen einander bei der Verwirklichung helfen. Das geschieht im Umgang, wo sie einander Zeit und Aufmerksamkeit, Freundschaft und Mitleben geben.

Wer auf diesem Gebiet raten soll – und davon schreibe ich die ganze Zeit – muß den Umgang den Betroffenen selber überlassen. Als Ratgeber kann man mitleben, kann sich einleben und gut zuhören (das kostet Zeit!).

Auch habe ich gelernt, daß Konflikte eine gute Seite haben. Es ist gut, wenn Menschen darum streiten, einander zu gewinnenwenn etwas von Wert ist, kann es auch verteidigt werden. Für jemanden, der „auf schlechtem Pfad” ist, tut nichts so gut, als jemandem zu begegnen, der ihm den Pfad blockiert, mit Einsatz von sich selbst, um des anderen willen.

Verantwortung tragen und Verantwortung nehmen sind Begriffe, die jetzt gehaltvoll werden. Es geht um eine Art Abstimmen, wobei man alles einsetzt, was in einem begrenzten Kontext Bedeutung hat, damit man etwas von dieser Bedeutung auf eigene Rechnung, auf das eigene Wort hin beiträgt. Das kann man nur, wenn das, was man auf sich nimmt, auch in Verbindung steht mit dem, was in einem selbst als wertvoll auskristallisiert ist. Das gehört dann zu dem, was man selbst verwirklicht hat, was man nicht für sich behalten, sondern mit anderen teilen will. Auf daß es der Qualität der Verhältnisse, wo man als verantwortlicher Mensch auftritt, zugute komme. Es ist wie das Austragen Eurer und unsrer Vergangenheit in die Aktualität.

Diametral gegenüber – in umgekehrter Richtungsteht die Qualität des „Verfügbar”-seins. Nicht immer können wir im Leben auf Begriffen und verwirklichten Bedeutungen bestehen. Ihr könnt jemandem begegnen oder in eine Situation kommen, wo einfach sonnenklar ist, daß jemand Euch braucht, daß die Hände gerührt werden müssen. Manchmal müssen wir vertrauen auf Intuition oder jemanden anderes.

Dann muß man einfach da sein, verfügbar, damit es so auf Euch zukommt (zu-kommt – Zukunft). Wagt es, Euch umdeuten zu lassen! Der Weg zur Zukunft schließt sich nicht immer an das Trajekt an, das Euch geformt hat.

*7\. Zerbrochener Zusammenhang*

Wir kommen ins Leben (nehmen Raum ein), wir entfalten Persönlichkeit (der Stempel der Zeit), beides wirkt sich darin aus, wie wir „hier-und-jetzt” bestehen. Unser aktuelles So-sein ist immer Niederschlag der Weise, wie wir Wirken des Leibes und des Geistes integrieren. „Leben” ist zuerst und vor allem die Aufgabe, diesen Zusammenhang zu festigen, zu handhaben und zu stärken.

Das Festigen dieses Zusammenhangs geschieht in fortdauerndem Prozeß des Sich-abstimmens. Fortdauernd, weil wir stets neue Eindrücke auftun.

Wenn es glückt, uns selbst abzustimmen und Zusammenhang zwischen den Abstimmungen zu finden, ergibt es sich aus dem Gegebenen, daß wir unsre Erfahrungen integrieren können. Aber „integrieren” ist in vielen Fällen ein zu schönes Wort, weil doch manch einer balancieren muß, um im Gleichgewicht zu bleiben. In einem leidlichen und gesunden Leben kriegen die Menschen Zeit und Gelegenheit, ihre Eindrücke, die innerliche Verwirklichung und ihre Äußerungen in Zusammenhang zu halten. Plötzliche Ereignisse wie Unglück, zu große Lebensaufgaben, Jammerund Bruch in den Verhältnissen verstören den Zusammenhang.

Was Menschen nicht verwirklichen können, stecken sie tief in sich weg oder fassen Vorurteile, werden unglücklich oder krank. Das Weiterleben mit Brüchen und Narben in dem Zusammenhang führt zu allerlei Erscheinungen. Bei den einen zu fortwährender Unsicherheit, bei anderen zu schnellem Aufbrausen, bei dritten dazu, daß sie „keinen Sinn” mehr sehen. Ernstliche Verstörungen des Zusammenhangs führen zu Schiefwuchs in der Entwicklung, zu Ungleichgewicht und Unzuverlässigkeit, zu kriminellem Betragen, zu psychischer Krankheit und zu Identitätsstörungen.

Bei den vielen Problemen der Klienten, die ich in meinem Berufsleben angetroffen habe, bin ich da angelangt.

In den Jahren, als ich in *Groot-Emaus* (1986-1993) gearbeitet habe, führte ich Regie über die Behandlung der aufgenommenen Jungen und Mädchen. Das war eine indirekte Aufgabe, denn es waren mehr als 200. Meine Aufgabe war also mehr die Vision der Behandlung, das Setzen der großen Linien. Leidlich ist es mir damals geglückt, der Vision einen Inhalt zu geben, wie ich es hier skizziert habe.

Wir suchten nach den Bruchlinien, die das Leben der Klienten verstört hatten, und versuchten daran anzuschließen, indem wir ihnen Zeit und Raum gaben, um neue Erfahrungen zu erschließen: Erfahrungen, mit denen sie nachträglich bessere Abstimmung erlangten und einen stetigen Zusammenhang der Abstimmungen entwarfen.

Abstimmung geschieht im täglichen Umgang. Sie beinhaltet vor allem, daß ein Verhältnis zwischen Klient und seinen Begleitern aufgebaut wird. Dieses Verhältnis muß Erfahrungen liefern, die von Bedeutung sind. Die neuen Bedeutungen müssen die alten Bilder beiseiterücken oder verändern, wahrscheinlich bis in tiefere Schichten (siehe den Anfang meines Briefs).

Eine derartige Behandlung kostet Zeit und völlige Geräumigkeit an Freiheit im Umgang, so daß auf Dauer eigene Verantwortlichkeit und Gefühle des Bezogenseins zu ihrem Recht kommen können.

So versuchten wir ein Gleichgewicht herzustellen zwischen dem Film-machen (der Diagnose), dem Schaffen von Bedingungen, unter denen der tägliche Umgang mit den Jüngeren gedeihen konnte (Raum zum Entwickeln von innen her: von Einwirkung zur Verwirklichung), versuchten eine Teilnahme der Jüngeren – Eigenverantwortlichkeit – am Leben der Einrichtung (von der Verwirklichung zum eigenen Auswirken).

Unsre Behandlung der Hilfeleistung diente dazu, das Spielfeld zu schaffen, in dem die Jüngeren ihre Erfahrungen erschließen und integrieren konnten (in horizontaler und vertikaler Perspektive). Aber nach und nach schränkten zwei immer mächtiger werdende Tendenzen dieses Spielfeld ein. Diese zwei Mächte waren die Sachverständigen und die Finanziers.

Kurz gesagt: für die Sachverständigen wurde das „Filmchen-drehen” immer wichtiger. Aus den Filmchen holten sie die Ursache-Folge-Ketten, täglich wollten sie mehr Leben in Regie nehmen, um die Ursache-Folge-Ketten anzupacken. Die negative Folge: der tägliche Umgang konnte nicht mehr „frei” verlaufen. Das heißt die Gruppenleiter konnten sich in Auftreten und Tempo nicht mehr darin abstimmen, was ihnen im spontanen Verkehr mit den Klienten auffiel, weil sie dem von außen ausgetüftelten „Plan” der Sachverständigen folgen mußten.

Die Finanzleute verstärkten diese Tendenz. Denn auch sie wollten die Regie von außen her verstärken. Geld wurde nicht „so eben mal” gereicht, um ein paar Jüngere mit ein paar Gruppenleitern endlos in einer Lebensgruppe miteinander umgehen zu lassen. Nein, man mußte ausweisen, an welchem „Leiden” bei jedem Kind gearbeitet wurde, wieviel Zeit es im Durchschnitt braucht, um dieses Leiden mit Erfolg zu behandeln, und wieviel Geld das wohl kostet.

Noch nie habe ich etwas gegen effiziente Organisation gehabt, aber es kann nicht sein, daß sie die eigentliche Arbeit des Begleitens torpediert. Wie soll bei Erziehung und Begleitung etwas herauskommen, wenn man mit dem Jüngeren umgeht, als wäre er Objekt?

Was schafft das für Verhältnisse, wenn man auf den „Menschen” im Klienten nicht eingehen kann, indem nur das „Leiden” Aufmerksamkeit bekommt? Wie kann jemand lehren, eigene Verantwortung zu tragen, wenn sein Leben von anderen ganz in Regie genommen wird (mehr manipuliert als Bedeutung erlangend)?

Die Kontexte funktionieren nicht, wie sie von außen her ineinander geschachtelt werden, wenn die direkt Betroffenen nicht Zeit und Chance erhalten, sich innerlich aufeinander abzustimmen, so daß sich ein Verhältnis entwickeln kann.

Im meinem letzten Arbeitskreis, *Hoeve Boschoord* kamen die Mächtigen der Finanziers mit ihrer Bürokratie und der Sachverständigen mit ihren Behandlungsmethoden und --techniken noch stärker vor. Ich zweifelte, ob noch ein „Spielfeld” übrigblieb. Es waren anstrengende Jahre.

In kleinem Maßstab geben meine Erfahrungen wieder, was im Großen des Zusammenlebens im Gange ist. Technokratie und Bürokratie beherrschen alle Verhältnisse: als Mächte von „außen”.

Wir erleben das in einigermaßen bequemer Lage. Die meisten Menschen in der Welt haben es schlechter.

In allen Facetten wird das Leben so sehr von außen gelenkt, daß wirs schon beinah nicht mehr besser wissen, als gehöre es so. In vieler Hinsicht schreibt uns die Technik vor, wie wir leben müssen. Gut, daß das Haus warm ist, die Cola gekühlt, aber immer mehr ist es doch ein Leben, in dem wir gezwungen sind, passiv auf die Umstände zu reagieren, wie sie von außen her konstruiert werden. Wir profitieren von Elektrizität, Telefon und Computer, aber wenn sie ausfallen, können wir selbst nichts mehr tun. Ein Fachmann ist nötig, oder wir kaufen ein neues Gerät. Wer kein Geld hat, ist ausgeschaltet.

Es ist ein „Stil”, der unsere Kultur durchdringt. Es läuft darauf hinaus, daß das Innere des modernen Menschen unterentwickelt bleibt, in so vielen Kontexten haben wir nichts mehr zu bringen und können nicht mehr üben, den eigenen Anteil an den Verhältnissen in die Hand zu nehmen.

Selbst die Gefühle werden von außen manipuliert. Das hat Folgen. Es macht uns unsicher. Wir gucken scheel, um zu wissen, was „schön” und „gut” ist. Es hat auch die Tendenz zu verdummen, denn was wissen wir von den Verhältnissen in der Welt anderes, als was wir den journalistisch angereichten Berichten nachreden? Es macht auch hartherzig. Wie oft wird gesagt: „Jeder muß doch für sich selbst aufkommen.”

Im Zusammenleben ist die Wissenschaft sehr wichtig geworden, aber es ist eine sehr beschränkte Art der Wissenschaft, bei der der Faktor „Mensch” so weit wie möglich ausgeschaltet oder manipuliert ist.

Ob zwischen Harlingen und Leeuwarden ein Zug fährt, wird beschränkt durch die Berechnungen der Direktion der Niederländischen Eisenbahnen. Berechnungen geben Anhaltspunkteaber es sind die Anhaltspunkte der Machthaber! Hört man auf sie und kann bezahlen, hat man den Nutzen, aber o weh, wenn man nicht auf sie hört.

Seht doch, wie unser Staatsapparat mit „Fremden” umgeht.

8\. *Sich-abstimmen – Bestimmung*

Wenn unser Leben sich in Verhältnissen abspielt, in die wir nur scheinbar einbezogen sind: nicht „echt”, nicht persönlich, verbrauchen sich die Bedeutungen, auf denen die Verhältnisse beruhen, denn sie werden nicht unterstützt. Jede Unterstützung beruht auf dem, was die darauf bezogenen Menschen aus sich selbst geben. In Verhältnissen, wo die Menschen nicht sich selbst geben, können sie ineinander keine Bedeutung mehr finden.

Dann wird das Leben lahm, weil dann die Verhältnisse uns selber auch nicht erfüllen. Dann kriegt man keinerlei Bedeutsamkeit dafür zurück.

Die großen Kontexte, in denen wir zusammenleben, können nur auf der Bedeutung beruhen, die ihnen die Betroffenen in Person geben. Dann akzeptieren wir Regeln und Gesetze und achten sie folgsam. Wenn sie gut sind, ist noch innere Übereinstimmung mit dem „Geist”, in dem wir miteinander leben. Entsteht auch darüber Uneinigkeit, bleibt übrig der Zwang der Vorschriften.

Meist sind es die Mächtigen und Wohlgestellten, die mit den Vorschriften eins sind, die Machtlosen aber nicht. Kommt es erst so weit, wird das Zusammenleben gefährlich, denn in jeder Lage, in der die Glieder des Zusammenlebens miteinander konfrontiert werden, steckt dann das Abwarten, ob die Reaktion des anderen verheißungsvoll, begreiflich und voller Respekt ist, oder ob man mit Gleichgültigkeit, als Gegenpart oder gar Feind angesehen wird. Dann gebricht es an gemeinschaftlichem Wert, und die Folge ist, daß man in der Konfrontation den gegenseitigen Wert nicht erkennt. Dann muß man abwarten, ob man in seinem Wert belassen wird.

Was heißt das, einander in seinem Wert belassen? Es ist mehr als in Ruhe gelassen werden. Beläßt die Kassiererin im Supermarkt uns in unserem Wert, wenn sie uns „Guten Tag” wünscht? Wir haben ein Sinnesorgan dafür, was „echt” ist. Jemand kann so routinemäßig guten Tag sagen, als wären wir Hunde. Ruhe gründet auf dem Vertrauen, daß wir, wenns drauf ankommt, als „Menschen” behandelt werden, ungeachtet der Routine, wie die Sachen zu gehen haben. Manchmal scheint es, als könnte die Menschlichkeit von „außen” geregelt werden: durch Instruktion durch Supermarkt oder Landesregierung. Aber das ist nicht der Grund, von dem aus sie wirkt. Solange man über mich redet als „Kunde” oder „55 plus”, allgemein: solange Menschen einander von außen unter Begriff bringen („Neger”, Jude", „Araber”), steht man nicht offen für jedermanns Menschlichkeit. Aus Sortierbegriffen ist kein menschliches Verhältnis aufzubauen. Menschen begegnen einander nur, wenn sie im anderen jemanden erkennen, als wäre er man selbst.

Das muß im Sich-abstimmen gelernt werden. Es löst die Sache nicht, daß man sich in dem einen Kontext wohl abstimmen kann, das aber in einem anderen bleiben läßt. Alle Kontexte, in denen wir leben, stehen miteinander in Verbindung. Wenn man einen Kontext von der Möglichkeit und Rechtmäßigkeit des Sich-abstimmens und Bedeutung-gebens/-empfangens abschneidet, entstehen Kontexte, wo (anderen) Menschen Unrecht getan wird.

Das Leben ist ein Ganzes. Wo nicht Gutes getan wird, reckt das Schlechte den Kopf empor.

Wird das Gute oder das Böse gewinnen? Das ist seit Adam und Eva bis und mit Harry Potter die große Frage.

Bei Adam und Eva fängt die Geschichte als menschliche Geschichte an. Das hat also nichts mit der Evolutionslehre zu tun, sondern mit dem Miteinander-finden der Bedeutungen, mit dem Herausfinden, was „gute” Bedeutung hat.

Also auch: was heißt „gut”. Bedeutung wird uns erst gegeben, das schrieb ich schon. Faßt das nicht auf, als könnte das Gute importiert werden, wie Bush das im Irak will. In Freiheit muß alles Gute von denen angenommen werden, die es empfangen.

Es ist das Problem aller guten Unternehmungen, wie jetzt auch das Schreiben dieses Briefs. Unternehmungen sind gut, wenn sie an die Bedürfnisse derer anschließen, auf die sie gerichtet sind. Es kann helfen, wenn die Sache, um die es geht, wirklich eine gute ist.

Ich vergegenwärtige eine gute Sache, meine ich. In diesem Brief über das Sich-abstimmen liegt das volle Gewicht auf der gegenseitigen Abstimmung zwischen Menschen, die einander auf diese Weise recht tun und auf der Suche nach einem glücklichen, sinnvollen, wertvollen Bestehen – Erfüllung für das gegenseitige Bestehen geben.

Sich-abstimmen hat auch eine noch viel weitere Bedeutung. Das heißt man versucht, dem Guten in der Kette der Generationen Stimme zu geben, es weiterzugeben.

Wie auch immer, das Vermögen zum Sich-abstimmen muß geübt werden.

Ich will noch anmerken, daß Das-Gute-weitergeben nicht etwas ist wie die Reproduktion der „Werte” und „Normen”. In dem, was gut ist, steckt eigener Fortgang und Entwicklung, und dann auch: jeder Mensch muß es auf seine oder ihre Weise wahr machen.

Jeder auf eigene Weise, aber nur dadurch, daß man Bedeutung hat, anderen Bedeutung gibt und zu Sachen von Bedeutung beiträgt.

Das ist mehr als Sich-abstimmen:

*das ist unsre Bestimmung.*

*Nachschrift*

Ich habe versucht, einen Faden des Werkes von Rosenstock-Huessy auszuspinnen. Es hätte auch ganz anders gehen können, aber Ihr kriegt einen Eindruck.

Wenn ich seine Bücher lese – es ist leichter, seine Bücher in einem Lesekreis zu besprechen – ist mir, als wär ich auf Reisen. Auf der Reise durchs Leben.

Es trifft, ich bin fasziniert, und wenn ich lese oder zuhöre, habe ich noch immer Einsicht in meine Probleme bekommen, darein, was bei meiner Arbeit wichtig ist und was die großen Probleme dieser Zeit sind. Es löst sich nicht alles auf, aber es orientiert mich wohl und hat meine Reiseroute beeinflußt. Sehen, was von Wert ist, macht auch selig.

So habe ich auch die Bücher von Rosenstock-Huessy gesammelt, lese weiter, und hoffe, mehr und mehr Menschen dafür zu interessieren.

Meine Betroffenheit ist durch die Arbeit, die ich getan habe, mitbestimmt, Arbeit, deren Einsichten und Wirkweisen ich gerne verbessern willdenkt aber aber nicht, daß Rosenstock-Huessy nur für mich als Fachmann wichtig ist. Er kann jeden erreichen, der bewußter im Leben stehen will und das Bedürfnis hat, über die Zukunft nachzudenken.

Ich schließe mit einer Übersicht der Bücher, die ich für meine Erzählung am meisten gebraucht habe.

Von Eugen Rosenstock-Huessy:

- Der unbezahlbare Mensch
- Sprache und Wirklichkeit
- Ich bin ein unreiner Denker
- Das Wunder der Sprache

- *(De onbetaalbare mens (1966) Rotterdam: Lemniscaat*
- *Spraak en werkelijkeid (1978) Haarlem: Vereniging Rosenstock-Huessy Huis*
- *Ik ben een onzuivere denker (1996) Zutphen: Dabar-Luyten*
- *Het wonder van de taal (2003) Vught: Skandalon)*

Von Ko Vos:

- Auf dem Weg zum Planeten

- *(Op weg naar de planeet (1974) Uitgegeven in eigen beheer)*

Mit einem herzlichen Gruß von Eurem Vater,

Wim van der Schee

***NOTIZ DES ÜBERSETZERS***

Der Text des Briefes Wim van der Schees an seine Kinder ist getreu übersetzt – bis auf die Einteilung in Abschnitte und Absätze: die habe ich hinzugefügt.

Der Text ist so dicht gehäkelt von Satz zu Satz, daß ich lange nicht wußte, wie der Verlauf des Ganzen eigentlich ist. So zart nähert sich der Vater den Kindern, daß er auf keinen Fall eine Überforderung im gedanklichen In-Anspruch-nehmen zulassen will.

Um das im Deutschen deutlich zu machen, habe ich die Verlangsamung durch Abschnitte und Absätze zugefügt.

Es ist bedrückend genug, daß der Brief im ganzen den Titel des ersten Teils der Soziologie Eugen Rosenstock-Huessys bestätigt, der lautet: *Die Übermacht der Räume*. Er ist so aktuell, daß wir um so glühender den zweiten Band in uns erwarten sollen: *Die Vollzahl der Zeiten*.

*Köln, 14. Juni 2006*\
*Eckart Wilkens*
