---
title: Jahrestagung 2010
created: 2010
category: jahrestagung
thema: Das Gesetz der Technik
---
![Haus Salem]({{ 'assets/images/haus_salem.jpg' | relative_url }}){:.img-right.img-large}

### {{ page.thema }}

26.-28. März 2010 im Haus Salem:

*"Der technische Fortschritt vergrößert den Raum, verkürzt die Zeit und zerschlägt menschliche Gruppen."*

Eugen Rosenstock-Huessy in: Soziologie Band I, 1956, Exkurs: Das Gesetz der Technik, S. 80-84

Die globale Finanzkrise regt dazu an, über Wachstum und Gedeihen nachzudenken. Das Gesetz der Technik, wie es Eugen Rosenstock-Huessy formuliert hat, setzt uns vielleicht instand, genauer über das Kostengesetz der Gesellschaft nachzudenken: Was kostet das Wachstum, das als Heilmittel der Krise fast überall (nicht überall) beschworen wird?

Melden Sie sich mit dem untenstehenden Formular zu dieser Tagung an.

Das ausgearbeitete Programm können Sie im Pdf-Format herunterladen.


Eingeladen sind alle Mitglieder und Freunde der Gesellschaft sowie an Eugen Rosenstock-Huessy und diesem Thema interessierte Personen.
