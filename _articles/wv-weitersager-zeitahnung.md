---
title: "Wilmy Verhage: Zeitenahnung – Eine Einführung in das Projekt «Weitersager Mensch»"
category: weitersager
order: 5
---
( in deutscher Übersetzung) Wilmy Verhage

Amsterdam, 26 Juli – 3 August 2006

Liebe Eveline,\
Du fragtest mich, wo ich mir Kraft und Inspiration hole, wenn nicht von Buddhismus oder Alkohol, nicht aus der Arbeit oder von Männern…
Laß dich erstmal beruhigen: ohne Menschen geht's bei mir auch nicht. Vom Buddhismus habe ich gelernt, mich manchmal passiv, aber offen zu verhalten. Gelegentlich trinke ich ein Gläschen und rauche meine Pfeife. Arbeit und Geldverdienen sind für mich nicht unbedingt synonym, undich kann mich noch immer verlieben.
Aber meine Geheimwaffe heißt: Eugen Rosenstock-Huessy.

1

Er hat mich darauf aufmerksam gemacht, daß die westliche Religion, das Christentum, von den Theologen (katholisch/protestantisch, beiden) in einen räumlichen Begriffsapparat eingesperrt ist. Jeden Zeitaspekt wußten sie zu beseitigen.

Seit Thomas van Aquino (1224-1274), also vor fast acht (!) Jahrhunderten, ist das Denken der Kirche mehr und mehr räumlich geworden. Das sieht man zum Beispiel am theologisch geprägten Gegensatz Natur/Übernatur.

Mit Natur wird dann das Tastbare, Sichtbare gemeint und mit Übernatur die unsichtbaren Kräfte und Phänomene. Beachte: die Wörter unsichtbar, untastbar sind negativ gefaßt.
Von diesem negativen Wert her zeigte mir Rosenstock-Huessy, daß es faktisch doch um Zeitphänomene, Prozesse, konkret geht.

Somit ist für die religiöse Wahrnehmung so etwas wie ein „oben” geschaffen, wo wir doch „jenseits” sagen. Denn es geht um die Zeit, das nach einem Tod anbrechende Zeitalter.
Ob es nun den Tod eines Menschen betrifft, oder einen Teil Deiner selbst, ein politisches System, eine Freundschaft, eine Ehe, eine Werkstatt, die Industrie, einen Gedanken oder was weiter noch alles sterblich ist, weil es doch lebt, weil es verkörpert wird.

Das große Vorbild des „Vorher” und „Nachher” ist der Tod Jesu Christi. Bevor Jesus lebte, konnte Christus nie der Maßstab sein, denn er war einfach noch nicht da, er wurde nur erwartet. (Wohl haben die Juden von Generation zu Generation auf ihn gehoff!). Aber nach seinem Tod konnte er der Maßstab werden.

Und tatsächlich ist, seitdem und soweit die Welt von Generation zu Generation gewandelt, eine Heilsgeschichte im Werden.

Jetzt sind wir an dem Punkt, wo wir alle auf einem Planeten leben und lernen müssen, so zusammen zu leben, daß jeder funktionieren kann, wie Gott ihn oder sie beabsichtigt hat. Aber davon, wie jeder täglich konstatieren kann, sind wir noch weit entfernt.
Die Zeit ist seit Thomas von Aquino aus dem Denken über Gott unspürbar langsam entfernt worden. Und damit sind die Wörter, mit denen Zeit und Zeitphänomene benannt werden, degradiert als zu unwichtig.

Die so unbewußt gemachte und also geschwächte Zeitgehen Sprache und Bewußtsein nicht immer zusammen?ist seit Kopernikus (1473- 1543), also schon fünfhundert Jahre(!) lang, als nächstes in astronomische Denkströme geraten und wird von unseren denkenden Mitmenschen, wie sie die Universitäten und Akademien noch unaufhörlich produzieren, eigentlich nur in der Kapazität als vierte Dimension des Raumes ernst genommen: da drückt sie den Aspekt der Bewegung aus.

2

Die wirklichen Dimensionen der Zeit aber werden vom lebendigen Heute gestaltet; von dem, was aus der Vergangenheit zu uns gekommen ist, Gutes und Böses beides; und von dem, was auf uns zukommt aus der Zukunft: ein drohendes Gewitter, oder wie umgehen mit den Dschihad Ansichten einiger islamitischer Mitmenschen, oder einer neuen Liebeum dir Beispiele zu geben, was ich meine.

Nämlich in dieser Reihenfolge. Die Dimensionen, die von wirklichen Erfahrungen der Menschen ausgehen, werden von der Uhrzeit hoffnungslos geknechtet.
Die astronomische Uhr ist ein ingeniöser Gebrauchsartikel, um mechanisch planen zu können, aus linearer Vorstellung heraus gedacht: Vergangenheit führt automatisch zum Heute, führt zur Zukunft.

Leider dominiert dieses Konzept der Zeit, obwohl ein jeder sie erlebt in ihrer ganzen reichen Mannigfaltigkeit des ewigen Jetzt.

Unter der Hand ist die Zeit unmerklich leise, schließlich ausschließlich als universell verwendbares und meßbares, unserem Willen untergeordnetes Phänomen betrachtet worden.
Geht es um Materie und totes Material, das dem Leben dient, sind Küchenwecker und Armbanduhr natürlich ein Segen, ein enormer Fortschritt, der Kern des Fortschrittsdenkens, die Frucht der Aufklärung. Was den Komfort betrifft – es ist uns noch nie so gut gegangen: jedem Arbeiter sein eigenes Schloß. Und Adam (Eva übrigens auch) erlöst vom Schweiß des Angesichts, worin er (und sie auch) arbeiten müssen.

Also das ist nicht das Problem. Das ist der Triumph aus fünf Jahrhunderten Naturwissenschaften, seit Leonardo da Vinci (1452-1519) und Kopernikus. Sie haben mit ihrem astronomischen Blick aus unserem Planeten eine Welt gestaltet. Das Internet ist doch wohl ihr letztes Geschenk.
Aber das herrschenden Denken, das daraus entstanden ist, macht uns glauben, wir lebten als winziges Stäubchen in einem unermeßlichen All zwischen unvordenklichen Zeiten und wären doch gleichzeitig als Maß aller Dinge imstande, diese ganze objektive Winzigkeit zu denken.
Das Problem ist also: das Leben ist dem Techno-Rhythmus der Armbanduhr unterworfen worden.
Sogar die Frauen im Haushalt bekamen die eiserne Formel: Montag Waschtag und Mittwoch Frikadelle. Das ist zur uniformen Haushaltschule geworden, die alles Erfahrungslernen zwischen Mutter und Tochter beseitigen sollte, um auch uns auf die Fahrt mit den Völkern zu nehmen. Als Babys werden wir schon diszipliniert auf die Uhr, nicht den Hunger.

3

Der wirkliche Gegensatz, sagt Rosenstock-Huessy, liegt aber nicht zwischen Natur und Übernatur, sondern zwischen Sprache und Natur. Merke, wie er die Sprache an erster Stelle platziert. Er gründet sich auf das Johannes Evangelium (1, Vers 1): Im Anfang war das Wort.
Er ist ein Zeit- und Sprachdenker.

Wenn das wissenschaftliche Thema uns Menschen, also die Gesellschaft betrifft, gründet er das nicht mehr auf Rechenmethoden und Mathematik, wie die Naturwissenschaftler es schon fünfhundert Jahre mit Riesenerfolg tun, sondern auf die Grammatik, die er sogleich verwandelt.
Unsere Grammatik stammt von einem Griechen aus Alexandria, ich weiß seinen Namen nicht mehr, jedenfalls 250 Jahre vor Christus, und ist aus der Art des Sprechens seiner Zeit destilliert. Von ihm lernt noch jeder Schüler die Reihenfolge: Ich, du, er/sie/es usw., um Zeitwörter zu konjugieren und richtig zu buchstabieren, aber alles auf eine ganz gleichgültige, gleichschaltende Weise.

In Wirklichkeit liegen Ozeane von Unterschied zwischen der Belebung des Ich, Du und des Es eines Menschen. Und gerade da fängt Rosenstock-Huessy an. Darauf begründet er seine Meta-Grammatik.

Auf dem Paradox: Alle Menschen sind gleich, und doch sind alle verschieden.
Er sagt auch ungemein packende Dinge über den Raum. Er zeigt, daß unser Raumbegriff: Höhe mal Breite mal Länge plus Bewegung eigentlich nur von einer Seite des Raumes handelt, nämlich der Außenseite, obendrein dann auch nur von einem Aspekt der Außenseite.
Wenn man von der Belebung des Menschen redet, also soziologisch, geht es beim Raum um innen und außen.

Gefühle, Gedanken, Gespräche, Kreise, wozu einer gehört wie zu Familie, Freunden, Arbeit, Staaten, Länderndas sind alles Innenerlebnisse. Wozu man nicht gehört, oder was nicht deines ist, das ist Außenwelt. Diese Außen- und Innenwelten jedes Menschenniemals handelt es sich um eine Einzahl! –gestalten das „hier”. Vergangenheit und Zukunft gestalten das „Jetzt”.
Zusammen gestalten sie die Gegenwart.

Jede Gegenwart hat also zwei Zeitaspekte und zwei Raumaspekte, die von der Sprache her zustande kommen, sich halten (oder durch Sprache wieder vergehen). Jeder Mensch, sogar jedes lebendige Wesen, befndet sich in diesem Kreuz der Wirklichkeit, wie Rosenstock-Huessy es nennt. Nur Menschen, weil und soweit sie wirklich sprechen können, sind Aggregate, die jedesmal Innen und Außen, Zukunft und Vergangenheit anders verbinden.

4

Wie es Dir jetzt geschieht, mit Deiner neuen Wohnung. Daß Du eine neue Wohnung bekommen hast, die Du einrichten kannst, wie Du willst, und die zumindest Deinem Sohn und Dir eine bessere Zukunft verspricht, hat damit zu tun, daß Du und Dein Ex-Mann euch anders verhalten, seit Du aus der Ehe ausgestiegen bist. Es hat ihn anscheinend auf andere Gedanken gebracht, weshalb er mit anderen, besseren Antworten kommt.
Einfach so.
So wirkt das, vom Herzen her, von jeglichem Leben aus, aus Erfahrung gedacht.

5

Mit dem Ausdruck Übernatur stellten die Theologen Gott unerreichbar über uns. Und der Deist Voltaire hat Gott dazu nochmal defniert als Anfangspunkt der All-Existenz, in weiten, weiten Vergangenheiten: als BIG BANG, als großen Knall. Also da hat Gott mit unserem Leben gar nichts mehr zu tun: er war einst da oder er ist noch immer da, ist aber unerreichbar über uns, oder befndet sich als zu analysierender Gegenstand tief unter den Theologen. Und zudem ist er auch noch im Jenseits, in unzugänglicher Zukunft.

Kein Wunder, daß er vor einigen Generationen für tot erklärt wurde.
Das hat mich lange Zeit zur Agnostikerin geprägt, diese Denkkonstrukte konnten mein religiöses Gefühl nicht nähren, es bekam nicht Sprache.

Inzwischen weiß ich und merke ich (und das letzte ist wichtiger), daß Gott in uns wirkt, denn Er/Sie ist der Gott von Tod und Leben. Gott fndest du nicht in einer Defnition, als Gegenstand. Er stand am Anfang unserer Existenz, gewiß, er wird in einer jetzt undenklichen Zukunft da sein, sicher, aber Du fndest Ihn/Sie in Deinem Leben in der Liebe, die Du empfndest: empfangend oder gebend, beides, was Dich in Bewegung setzt, in Taten, in Dir selber, in Deinen Wechselbeziehungen, in den Kräften, von denen Du getragen wirst.
In der geglaubten Sprache.
Und der Teufel? Er steckt in den Lügen, in bloßer Nachahmung, in Unechtheit.

6

Das große Geschenk an uns Menschen ist die Sprache. Das Vermögen, einander zu benennen, zu verstehen, zu glauben, zu kritisieren, geschieht durch Sprechen.
Erst kommt die Sprache, danach erst das Denken. Erst muß etwas geschehen sein, um überhaupt darüber nachdenken zu können. Erst bekommst Du bei Deiner Geburt einen Namen, bist du ein Du, Jahre, bevor Du Ich sagen kannst.
Sprache ist kein räumliches Geschehen. Es ist nicht faßbar, wirkt aber. In der Zeit. In den Zeiten.

Dort fndest Du Gott, das Subjekt aller Subjekte, das Ich, das alle Ichs einschließt, den Brunnen des Vertrauens. In der Zeit fndest Du den Menschen, männlich/weiblich, die Wechselseitigkeit jedes Du, zwischen denen die Liebe walten kann. Und Du fndest die Welt da, das Es (er/sie), jedes „es” ist Chance zu objektiver Wahrheit.

Du, ich, esdas sind wir alle, wie jeder auch Teil eines Wir, eines Euch, eines Ihr ist, je nach der Perspektive des Augenblicks. Wir sind ein anderes Dich in jedem Kontakt. Und jeder Kontakt macht uns, je nach unseren Abenteuern zusammen, mehr zu einem Wir. Und zusammen sind wir ein „es”, nämlich sobald etwas von uns beiden gesagt wirddann sind wir der Gegenstand.
Wie Du das zum Beispiel tatest in Deinem Brief, als Du unsere Freundschaft unter die Lupe nahmst.

Liebe, Vertrauen, Wahrheit, Treue: Du, ich, es, wir. Die höhere Grammatik Rosenstock-Huessys. Wenn Du das Du, das Ich, das Es und das Wir in Dir selber und in andern kennenlernst, artikulieren lernst, erkennen lernst, in aller Konkretheit, dann weißt Du zu aller Zeit, wo Du stehst, wo Gott steht und wo die Welt sich befndet.
Und dann stimmt Deine Perspektive und Du kannst unbefangen leben.

7

Die Sicht Rosenstock-Huessys ist also ganz anders, als jedenfalls ich es bis jetzt angetroffen habe. Als ich ihn las, kam alles bei mir auf den richtigen Platz.
Das tut es noch immer.
Lange ist er schon in meinem Leben.
Zuerst von meiner Tante Ko, einer Freundin meiner Mutter, aber auch meines Vaters, ihrer Hausfreundin, die von Rosenstock-Huessys Ansicht ergriffen war, sobald sie sie kennenlernte und somit in unsere Familie einführte. Das muß irgendwann Anfang der 60er Jahre gewesen sein.
Tante Ko war in meinem Leben die einzige Erwachsene, die große Worte verwenden konnte und ich doch fühlte, daß sie verstand, was sie sagte. In meinem Leben als Jungerwachsene, als ich mit Mühe Balance in der großen, bösen Außenwelt zu fnden suchte, hatte sie eine priesterlichen Funktion: die paar Male, daß ich wirklich nicht mehr wußte, wie nun weiter, konnte ich immer zu ihr kommenund Ihr Rat war gut, hat mich gebildet.

Zwischen meinem zwanzigsten und dreißigsten Jahr begann ich Rosenstock-Huessy zu lesen. Zwei Bücher: Speach and Reality (Sprache und Wirklichkeit), eher ein wissenschaftliches Werk, und Fruit of Lips (Frucht der Lippen) über die vier Evangelien.

Zwanzig Jahre habe ich mit diesen zwei Büchern gelebt. Lesen, von neuem lesen, langsam auf mich einwirken lassen, was er da eigentlich sagt.

Als ich Andragogik studierte und nachdem in der Erwachsenenbildung tätig wurde, war Sprache und Wirklichkeit mein sparring partner. Dieses Buch hat mir sehr geholfen, die sozialen Realitäten zu verstehenjedenfalls besser als die damals und noch immer herrschenden soziologischen und psychologischen Theorien von der Universität.

Frucht der Lippen war fast dreißig Jahre der einzige, hauchdünne Faden, der mich ans Christentum band.

Ich hatte durchaus nicht die Absicht, sein Denken anzunehmen, außer wenn es mich durch seine Wirkung überzeugte. Gefügiger Gehorsam war nie meine Schokoladenseite. Und zur protestantischen Fassung des Christentums, zumal mit seinen Anständigkeitsnormen für Frauen, verlangte ich schon gar nicht zurück. Aus deren Perspektive gesehen verlief mein Leben sowieso hoffnungslos sündhaft.

Aber als ich ins Burnout geriet, mußte ich zugeben: Rosenstock-Huessy hatte gegen das herrschende akademische Denken über Menschen gewonnen, so daß ich, um ihm weiter folgen zu können, auch wieder Christ werden mußte.\
(Und mein Deutsch aufholen, denn er schrieb auf Deutsch und Englisch, obwohl manches Werk auch ins Niederländische übersetzt worden ist.)

Von diesem Moment an, jetzt also vor zehn Jahren, habe ich angefangen ihn zu lesentausende von Seiten, immer wieder, langsam seine Einsichten in mich aufnehmend, noch immer.
Alles was ich in die Hand bekam: die Revolutionsbücher: Die Europäischen Revolutionen und Out of Revolution beides, wie die westliche Welt aus Revolutionen entstanden ist. Seine Soziologie „Im Kreuz der Wirklichkeit”, zwei Bände. Das große Werk, auch zwei Bände, Die Sprache des Menschengeschlechts. Die Tochter. The Origin of Speech. I am an impure Thinker. Die Zwillinge Der Atem des Geistes und Heilkraft und Wahrheit. Das Alter der Kirche, das er zusammen mit Joseph Wittig geschrieben hat. Die Gesetze der Christlichen Zeitrechnung, Des Christen Zukunft, Werkstattaussiedlung, Der Unbezahlbare Mensch, Dienst auf dem Planeten.

8

Als mein Mann Peter starb, kam die Zeit, Kontakt zu suchen mit denen, die sich ihm widmeten. Drei Jahre hat es noch gedauert, bis ich dazu genügend Kraft und Mut beisammen hatte.
Und jetzt bin ich Teil des Vorstands einer der Gesellschaften, die sich um ihn bilden.
Zurück aber in eine der offziellen Kirchen kann ich nicht mehr. Zwar bin ich den Protestanten dankbar für das, was sie zustande gebracht habenaber zurück kann ich nicht mehr. Auch nicht zur katholischen Kirche.

Nur die jüdische Spritze, die Eugen Rosenstock-Huessy dem Christentum zuführt, macht die Kirche für mich zu dem heilenden Weg, der sie zu sein verspricht.

9

Er lebte von 1888 bis 1973. Er war der außergewöhnlich talentierte Sohn assimilierter deutscher Bourgeoisie-Juden aus Berlin. Als junger Mann trat er über zum Christentum. Als der Erste Weltkrieg ausbrach war er, 26 Jahre alt, schon Professor der Rechtsgeschichte. Er wurde eingezogenihm ist an der Front in Nord-Frankreich Hören und Sehen vergangen. Vor seinen Augen sah er die Welt, wie sie bis dahin war, untergehen. Das hat sein Denken geprägt.
1933 nach Hitlers Machtsübernahme ist er in die Vereinigten Staaten eingewandert. Dort hat er erst in Harvard doziert, später am Dartmouth College in Hanover New Hampshire.
Also. Ich hoffe, daß Du hieraus ein bißchen Suppe kochen kannst, wie wir auf Niederländisch sagen.

Wenn nicht, sagst Du es mir, ja? Ich spreche nicht so einfach von ihm und seinem Werk, lieber reagiere ich von daher.\
Nun ja.

Ich bin gespannt, wie Du reagierst,

herzlich,

Wilmy
