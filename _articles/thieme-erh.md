---
title: "Hans Thieme über Rosenstock-Huessy"
category: aussage
published: 2022-01-04
name: Thieme
---

Anläßlich des 65. Geburtstages von Eugen Rosenstock-Huessy erinnerte sich dessen zeitweiser Lehrstuhlvertreter in Breslau, Hans Thieme, an den bleibenden Eindruck, den dessen Eintrag in das „Album der Rechts- und Staatswissenschaftlichen Fakultät“ auf ihn gemacht hatte.1 Trotz Eintragungen Theodor Mommsens, Otto Gierkes oder Felix Dahns habe er seine eigene Lage angesichts von nationalsozialistischer Diktatur und Weltkrieg vor allem in den Worten Eugen Rosenstocks gespiegelt gesehen.
* Rosenstock sprach darin von der Erschütterung die ihn 1918 nach dem Zusammenbruch befallen und am Nutzen der Rechtshistorie für das Leben hatte zweifeln lassen.
>*Hans Thieme, Die Epochen des Rechts, in: JZ, 8.Jg., Nummer 14/15, 31. Juli 1953, S.418.*
