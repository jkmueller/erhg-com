---
title: "Marlouk Alders: reactie op Wilmy Verhage: Dochters"
category: weitersager
order: 12
published: 2022-01-30
---

Dochters\
Die Tochter – Das Buch Rut


>*Oktober 2007*

Lieve Wilmy,

Door je brief ”Dochters” rechtstreeks aan mij te richten, heb je me aangesproken en ik kan niet anders dan antwoorden: Respondeo etsi mutabor.

Ik kreeg “Die Tochter/Das Buch Rut” toen - zo'n zeventien jaar geleden - gevierd werd dat ik als pleegdochter bij een gezin ging horen. In de jaren ervoor was er langzaam iets gegroeid, was ik er al steeds meer bij gaan horen en dat werd toen bevestigd.

Ik had mij nooit dochter van mijn biologische ouders gevoeld. Dus was deze stap voor mij, op dat moment, van enorm grote betekenis: Een erkenning dat ik de moeite waard was (pleeg)dochter te zijn en bij een gezin te horen.

In “Die Tochter” schreef de vader van dat gezin een tijding aan mij, waarin stond dat mijn leven zich nu inschreef in de verhalen van al die grote dochters die ons menselijk ras opriepen tot de eenheid van een groot gezin om zo ooit tot vrede te geraken. En dat ik weer recht stond in de rij van al die vrouwen (…) die het licht, temidden van mannelijk begeren en krijgen, heen droegen naar het einde van die strijd.

Wat een geweldige erkenning voor de eenzame strijd die ik als vrouw met mannen tot dat moment geleverd had, zo dacht ik toen.

Ik begon met het lezen van Martin Buber's vertaling van “Das Buch Rut' en vond het niet makkelijk te doorgronden. Deze Bijbelse novelle had mij iets te zeggen waar ik mijn vinger niet achter kon krijgen. Dat hield mij bezig.

Mijn worsteling met dit eerste deel van “Die Tochter/Das Buch Rut” lijkt achteraf een metafoor voor het feit dat het pleegdochterschap net zo moeilijk was. De (volwassen) kinderen konden de keus van hun ouders niet aan. Er was geen eenheid in dit kleine gezin en mijn “dochterschap” werd om die reden weer ongedaan gemaakt. Ik kon niet in den vreemde, zoals Ruth, worden opgenomen!

Dit was voor mij een verbijsterende ervaring. Het deed mij opnieuw twijfelen: Twijfelen aan mijn waarde als “ dochter”. Twijfelen of het waar was wat daar in die tijding stond. Twijfelen of er voor mij ooit nog een plekje op deze wereld zou zijn. Twijfelen aan de mensheid op zich.

Hoe moest ik nu nog mijn flakkerend lichtje, temidden van het mannelijk begeren en krijgen heen dragen? Hoe moest dit gebeuren ooit nog tot vrede geraken?

De keus die destijds gemaakt is, doet mij nog altijd zeer. Ik heb het boek Die Tochter/Das Buch Rut” sindsdien niet meer opengeslagen! Het tweede deel: “Die Tochter” van Eugen Rosenstock-Huessy bleef ongelezen. Het werd als een afgesloten hoofdstuk, dat zich tegelijkertijd nooit helemaal liet afsluiten.

En dan nu jouw brief over “Die Tochter”: Je roert daarmee dat uiterst heikele punt aan, waar zoveel aan vast zit.

Er borrelen allerlei vragen in me op:  
Waarom heb ik het boek “Die Tochter” nooit meer open kunnen slaan?
Ben ik inderdaad bang dat het van alles vraagt wat ik niet wil of kan geven? Ben ik bang dat het me met een nieuwe vorm van bevoogding opzadelt, terwijl ik het oude juk nog nauwelijks heb afgeschud?

Of is het iets anders waar ik bang voor ben? Wat betekent “dochter” zijn? Is dat iets waar ik niet meer in geloof? Ben ik bang dat ik opnieuw iets aan moet gaan waar ik geen vertrouwen meer in heb? Ben ik bang dat ik opnieuw zal ondervinden te worden afgewezen? Wil ik (nog) wel “dochter” zijn?  Wat is daar mee gemoeid? Wat is eigenlijk: “dochterschap”?

Inmiddels heb ik zoveel van Eugen Rosenstock-Huessy gelezen dat ik weet dat hij met zijn “Dochter” iets anders aanduidt dan de pleegdochter die ik graag had willen zijn. En dat wetende, waarom kon ik mij er even goed niet toe zetten, “Die Tochter” te lezen?

Tjonge Wilmy, je hebt wat bij me in gang gezet. En oh, wat mis ik nu het overleg en de steun van een van mijn toeverlaten, Ko Vos, jouw tante! Ik zou mijn antwoord aan jou, aan haar hebben voorgelegd en … ze zou ongetwijfeld iets scherp/zinnigs over de kern van dit alles gezegd hebben.

Door over dit alles na te denken en er nog eens secuur naar te kijken, deed ik de ontdekking dat Rosenstock's “dochterschap” en het pleegdochter zijn, door de tijding van de vader in mijn exemplaar van “die Tochter', met elkaar verbonden waren en dat ik die twee dingen van elkaar los moet gaan koppelen. Wat een ontdekking! Zover heb je me al geholpen! Dank je wel.

Ik ben ook heel blij met jouw uitleg van “Die Tochter/Das Buch Rut”:

Dat het boek niet over vrouwen, maar over mannen blijkt te gaan, werkt op zich al als bevrijding.

Rosenstock-Huessy's alinea: ”We zeggen het elkaar niet. Want we kunnen het wezenlijke niet meer hardop zeggen. Elk luid woord is onverdraaglijk geworden. De geordende taal is ons al bijna te sleets. We zoeken een taal die niet meer hoeft te worden uitgesproken, waarin we kunnen schuilen. Het is geen leeg zwijgen, dat we zoeken. Konden we elkaar maar bereiken; we moeten elkaar bereiken” is mij uit het hart gegrepen.

Misschien geeft jouw brief aan dat het tijd is te doorbreken.

Er is hoop. Na een leven, waarin ik van alles dat ik had, beroofd werd, heb ik, ondanks alles, mijn ziel, mijn onderscheidingsvermogen van wat echt en levend is, weten te behouden. Bij het leren benoemen daarvan heb ik veel steun en liefde van anderen ondervonden.

Lieve Wilmy, ik heb het boek van de plank gehaald. Het wordt tijd het boek “Die Tochter/Das Buch Rut” opnieuw open te slaan. Ik laat je weten wat het me zegt.

Liefs,

Marlouk
