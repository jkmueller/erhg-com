---
title: Mitgliederbrief 2009-06
category: rundbrief
created: 2009-06
summary:
zitat: |
  >„Wo Jugend mit Ehre alt wird und wo die Lehre sich allzeit erneuert, wo die Lehre fröhlich den Tag grüßt, und die Jugend fröhlich das Alter, da sind Leib und Geist der Menschenkreatur geheilt und offenbaren sich als die ewige Zukunft und die heutige Gegenwart der gesunden Menschenseele.”
  >*Eugen Rosenstock-Huessy, Lehrer oder Führer, Die Kreatur 1, 1926, S.68*
---

## Eugen Rosenstock-Huessy Gesellschaft e.V.

{{ page.zitat }}

**Vorstand/board/bestuur:**\
*Dr. Eckart Wilkens (Vorsitzender);*\
*Wilmy Verhage; Andreas Schreck; Dr. Jürgen Müller; Thomas Dreessen*\
*Antwortadresse: Dr. Eckart Wilkens: Everhardstraße 77, 50823 Köln, Deutschland*\
*Tel: 0(049) 221 511265*

***Brief an die Mitglieder Juni 2009***

**Inhalt**

{{ page.summary }}


[zum Seitenbeginn](#top)
