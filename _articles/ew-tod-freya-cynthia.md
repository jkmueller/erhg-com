---
title: "Eckart Wilkens: Zum Tod von Freya von Moltke und Cynthia Harris"
category: bericht
created: 2010
---
An Neujahr verstarb Freya von Moltke in Four Wells, Norwich/ Vermont im 99. Lebensjahr. Und das verändert die Lage der Eugen Rosenstock-Huessy Gesellschaft in radikaler Weise: denn sie war es doch, die die vierte Epoche im Leben und Wirken Eugen Rosenstock-Huessys hervorgerufen und nach seinem Tode verkörpert hat, die Epoche, die durch das vierte große Werk „Die Sprache des Menschengeschlechts” allen zugänglich ist. Sie war bei der Gründung der Eugen Rosenstock-Huessy Gesellschaft noch unsichtbar. Jetzt darf sie umgestaltend und erleichternd auf uns einwirken.

Daß eine Woche später Cynthia Harris verstarb, die Eugen Rosenstock-Huessy das existentielle Übersetzen in die englische Sprache beseelte, wirkt fast als Bestätigung des Wandels, der mit Freyas Hinzukommen geschah. Wir versuchen, ganz offen dafür zu sein, was die Trauer, soweit sie an uns ist, auftut, dahinten läßt und wo sie Fortgang gewährt.

Eckart Wilkens

Köln, im Januar 2010
