---
title: "Hans-Ulrich Wehler über Rosenstock-Huessy"
category: aussage
published: 2021-11-24
name: Wehler
---

* Aus einem Interview with Hans-Ulrich Wehler:  
*To conclude with two general questions, what would you say are the three or four most important influences on the writing of German history in the twentieth century?\
When it comes to the greatest influences, I would have to say again that Max Weber and Karl Marx have offered the most stimulating ideas. The most important German historian of the first half of the twentieth century was Otto Hintze. After that, I would mention Eugen Rosenstock-Huessy (the only genius I ever met) and Ernst Troeltsch*

* Auf der Homepage von Hans-Ulrich Wehler:
"Eugen Rosenstock - Huessys "Europäische Revolutionen", in: D. Felken Hg., Ein Buch, das mein Leben verändert hat, München 2006, 438-40:
*Ginge es um das Werk eines einzelnen Autors, das zu einer fundamentalen Lernerfahrung geführt hat, käme kein Zweifel auf: Es wäre das Werk Max Webers, von dem auf Dauer die nachhaltigste Wirkung ausgegangen ist. ...  
Soll aber nur von einem einzigen Buch die Rede sein, ergibt der prüfende Rückblick, daß von dem bedeutendsten Buch Eugen Rosenstock-Huessys der intensivste Einfluß ausgegangen ist: Die europäischen Revolutionen und der Charakter der Nationen gerieten dank der nachdrücklichen Empfehlung des Kölner Soziologen René König 1954 in meine Hand. ...\
Denn Rosenstock-Huessy erkannte in den in den großen Krisen seiner Revolutionen das Laboratorium, in dem jeweils nationale Identität geschaffen, geprägt und auf Dauer gestellt wurde. ...\
Mir imponierte zum einen die weitläufige Architektur seiner großzügigen Synthese, obwohl mir die Fixierung auf den inneren Zusammenhalt aller Revolutionen unglaubwürdig erschien. Deshalb bin ich bald mit fliegenden Fahnen zu Webers Modernisierungstheorie des Okzidents übergelaufen. ...\
Der persönliche Eindruck, als er 1961 als Gastprofessor in Köln und ich als sein Assistent delegiert war, bestätigte die Lektürewirkung: Eugen Rosenstock-Huessy ist der einzige geniale Mann, den ich bisher kennengelernt habe.*
