---
title: Mitgliederbrief 2011-09
category: rundbrief
created: 2011-09
summary:
zitat: |
  >Die Gemeinschaft kann nicht vorausgesetzt werden, wie beim gemeinsamen Bekenntnis. Nein: sie ist Aufgabe. Sie mißlingt auch.
  >Sie wirkt daher als frei erkämpfte und zugleich geschenkte Überraschung.
  >*Eugen Rosenstock-Huessy ‚Das Alter der Kirche’, Band II Seite 155 Agenda Verlag Münster, 1998. Zuerst erschienen 1927.*
---

## Eugen Rosenstock-Huessy Gesellschaft e.V.

{{ page.zitat }}

**Vorstand/board/bestuur:**\
*Dr. Eckart Wilkens (Vorsitzender);*\
*Wilmy Verhage; Thomas Dreessen; Andreas Schreck; Dr. Jürgen Müller*\
*Antwortadresse: Dr. Eckart Wilkens: Everhardstraße 77, 50823 Köln, Deutschland*\
*Tel: 0(049) 221 511265*

***Brief an die Mitglieder September 2011***

**Inhalt**

{{ page.summary }}


[zum Seitenbeginn](#top)
