---
title: Mitgliederbrief 2011-01
category: rundbrief
created: 2011-01
summary:
zitat: |
  >Nach der Kirche und nach der Staatenwelt\
  >ist die weltweite Gesellschaft entstanden, die alle Grenzen sprengt.\
  >„TOCHTER Gesellschaft” ist sie getauft worden,
  >denn die Tochter ist der Weg zurück in die Schöpfung, nachdem Mannesgeist uns ihrer Nähe entfremdet hat.
  >*Eugen Rosenstock-Huessy, Nachwort zu Die Tochter / Das Buch Rut, S. 45*
---

## Eugen Rosenstock-Huessy Gesellschaft e.V.

{{ page.zitat }}

**Vorstand/board/bestuur:**\
*Dr. Eckart Wilkens (Vorsitzender);*\
*Wilmy Verhage; Andreas Schreck; Dr. Jürgen Müller; Thomas Dreessen*\
*Antwortadresse: Dr. Eckart Wilkens: Everhardstraße 77, 50823 Köln, Deutschland*\
*Tel: 0(049) 221 511265*

***Brief an die Mitglieder Januar 2011***

**Inhalt**

{{ page.summary }}


[zum Seitenbeginn](#top)
