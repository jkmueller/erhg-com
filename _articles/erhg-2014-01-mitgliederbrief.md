---
title: Mitgliederbrief 2014-01
category: rundbrief
created: 2014-01
summary:
zitat: |
  >Es ist seltsam, aber wahr, daß lebendige Gegenwart die Selbständigkeit von Individuen oder von Gegenständen ausschließt. Gegenstände, „Objekte”, gibt es nur als getötete Gegenwart. Gegenstand und Gegenwart verhalten sich wie Tod und Leben. Man muß also zwischen Gegenstand und Gegenwart wählen. Beides gibt es nicht zugleich, den Tod und das Leben. Planck und Einstein bilden zusammen die gegenwärtige lebendige Physik. Die Elektronen oder Quanten aber mögen der Physik Gegenstände sein; nimmermehr sind sie ihre Gegenwart.\
  >Liebe heißt also, sich heraussehnen aus der Selbständigkeit und den Preis zahlen für diese Befreiung aus unserem Zeitgefängnis, indem wir unsere Solidarität mit denen anerkennen, die uns die fehlenden Zeiten zubringen. Mithin ist alle Gegenwart das überschneiden mindestens zweier an sich getrennter Zeiten zu einer aussprechbaren, gemeinsamen Zeit.\
  >*Eugen Rosenstock-Huessy, Der Atem des Geistes, S. 140f.*
---

## Eugen Rosenstock-Huessy Gesellschaft e.V.

{{ page.zitat }}

**Vorstand/board/bestuur:**\
*Dr. Eckart Wilkens (Vorsitzender);*\
*Thomas Dreessen; Andreas Schreck; Dr. Jürgen Müller*\
*Antwortadresse: Dr. Eckart Wilkens: Everhardstraße 77, 50823 Köln, Deutschland*\
*Tel: 0(049) 221 511265*

***Brief an die Mitglieder Januar 2014***

**Inhalt**

{{ page.summary }}


[zum Seitenbeginn](#top)
