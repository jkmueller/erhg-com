---
title: "Vermischte Anmerkungen zu Rosenstock-Huessy"
category: rezeption
published: 2021-11-23
---
### Tagebuch der Menschheit

>Offenbar ist Partnerschaft entweder eine uralte Sache oder ein auferlegter Schwindel. Denn unmöglich kann eine menschliche Grundeigenschaft zum ersten Male entdeckt werden.\
Wenn aber das uralt ist, wessen heut mit einem Male der Mund übergeht, dann muß von uns zweierlei erklärt oder herausgefunden werden:
- erstens: in welcher Weise haben die Alten hiervon gewußt und gesprochen?
- Zweitens: wie konnte dieses Wissen eine Zeitlang vergessen werden?

>Wir wollen den zweiten Punkt zuerst aufblenden und beantworten.\
Es ist allerdings etwas geschehen. Durch einige Jahrhunderte hindurch bestand eine Verblendung. Die Aufklärung aller Philosophieprofessoren hatte ein Interesse, den Menschen ohne die Folgen der Arbeitsteilung als den Normalmenschen auszugeben. Aus ihren Schulen kamen Freidenker, Individuen, Studenten, Gebildete. Und ihnen allen war es eingetrichtert worden, daß der Kopf des Menschen durch seine soziale Tracht oder Klasse nicht abgewandelt werde.

>Erst seit Karl Marx wird die Arbeitsteilung wieder ernst genommen.\
Aber Marx hat das Wort Partner nicht anerkannt. Und so klafft sogar bei ihm noch eine weite Lücke gegen die allgemeinste Tradition des Menschengeschlechts in Sachen Partnerschaft.

>Wir also wollen einen Augenblick uns von den Schulbänken der Hörsäle erheben und wollen uns fragen, ob seit zehntausend Jahren niemand das Pro und Contra der Partnerschaft seinem Denken zugrunde gelegt hat.
Wenn dem so wäre, dann wäre die heutige Partnerschaft, wie gesagt, ein ebensolcher Rummel wie die Gruppenmechanik und ähnliche Laboratoriumsexperimente. Der wirkliche Mensch kann sich mit seinem wirklichen Leben auf kein Experiment einlassen.

>Nun, die Bibel nennt die Partnerschaft oder die durch sie uns befallende Milieutheorie die Ursünde. Durch die Arbeitsteilung, so zeigt sie, werden wir jeden Tag neu in die gesellschaftlichen Irrtümer verflochten.
Denn da wir nicht als einzelne, sondern als Männer und Weiber existieren, so müssen wir uns zu immer neuen Gruppen vereinigen, um auch nur das kleinste Ding zu meistern. Diese Vereins- oder Assoziationsbildung verkrüpple unsere innere Freiheit und Verantwortlichkeit und wir heulten mit den Wölfen.

>Die „Assoziation“, jede Arbeitsverbindung – und wir kommen um sie nie herum – ist einerseits unentbehrlich, andererseits uns abträglich.\
Nie dürfen wir unsere Kraft zur Dissoziation einbüßen. Denn dann würden wir Gott weniger gehorchen als den Menschen.\
Das lehren übereinstimmend die Bibel und Karl Marx. Beide sehen, wie leicht wir die Assoziation, d. h. die Arbeitsteilung, Herr über uns werden lassen.

>Diese biblische Lehre von der Ursünde wird von den Philosophen und von der Aufklärung seit langem lächerlich gemacht. Auf ihren Schulbänken sitzen allerdings bloß Köpfe, und der Kopf schwebt außerhalb der Arbeitsverbindungen und Betriebe; im Hörsaal kann man die Geschichte ohne die Arbeitsteilung als Sieg der Vernunft oder als Wille zur Macht oder als Untergang des Abendlandes ausdeuten. Denn es kommt ja nur auf die Königsherrschaft der Philosophie, des Liberalismus hinaus.\
Der freie Unternehmer wurde der Abgesandte aus dem Hörsaal in die Gesellschaft. Er wurde als Freier, Liberaler, Robinson Crusoe, Émile, Cartesischer Denker der Normalmensch.

>Da weder den Philosophen noch den freien Unternehmer die Arbeitsteilung einzuschlucken schien, so mußte die biblische Quelle für die Milieutheorie und für den Fluch der Arbeitsteilung umgedeutet werden.
Die Quelle ist das Kapitel in Genesis über den Fall. Die liberale Kritik der Bibel macht sich über diesen Bericht lustig. Sie las aus ihm eine dunkle Anspielung auf den Geschlechtsverkehr zwischen Adam und Eva heraus. Und lachte daraufhin um so lauter.

>Zweihundert Jahre dieses liberalen Hohns verhindern sicher manchen Leser meiner Schrift, in der Bibel das zu lesen, was da steht.\
Ich fühle ihnen das nach. Denn die vereidigten Sachverständigen, Wellhausen, Gunkel, Noth usw., sind ja die hohnvollsten Leser jenes Bibeltextes. Diese liberalen Kritiker trugen selber das Ideal des arbeitsfreien, ungeteilten Individuums, des Akademikers, in ihren Herzen.
Deshalb waren sie wohl durchaus gutgläubig, wenn sie über die Sätze der Genesis weglasen und nirgends merkten, daß jeder vom Weibe Geborene in der gleichen Lage wie Adam und Eva sich vorfindet.

>Trotzdem steht das mit dürren Worten in der Bibel.
Aber ich bilde mir nicht ein, die liberalen Leser im ersten Anprall von ihrem Urteil über die Erbsünde abzubringen.

>Wenn ich trotzdem die Urtradition wiederherstelle, so bin ich dazu auch ohne Rücksicht auf den Erfolg bei diesen Lesern gezwungen. Denn ich muß auch mir selber eine Erklärung liefern, weshalb unsere Frage der Arbeitsteilung erst von Marx entdeckt worden sein soll.
Nur wenn ich mir sagen darf: „Er hat sie wiederentdeckt“, darf ich ihm glauben.

>Deshalb also mache ich auch das heutige Partnergeschwätz erst ganz glaubwürdig, wenn ich sage, die Alten haben die Arbeitsteilung nie übersehen. Nein, sie haben sie in das Herz ihres Glaubens gesetzt.

>Also was steht geschrieben?\
Die Bibel legt uns auf unsere Bestimmung von vornherein fest. Daher gibt sie die Condition humaine gleich zu erkennen, sobald sie eben überhaupt von der Existenz der ersten Menschen auf Erden handelt. In diesem ersten Augenblick hat der Mensch eine Werksgenossin, eine Gehilfin. Diesen Ehrentitel gibt ihr die Schrift.
Das Sexuelle interessiert den Schreiber gar nicht, sondern Eva, die Gehilfin, steht für alle Arbeitsteilung und jede Gesellschaftsordnung.

>Wie anders sollte die Arbeitsteilung und Partnerschaft denn als Grundgesetz verankert werden? Die Feigheit des Milieuprodukts Mensch ist eben mit jedem Milieu gegeben.
Ob ich mich hinter die öffentliche Meinung verschanze, „Jeder sagt so“, oder hinter meine Frau und diese wieder hinter ihre Hausschlange, kommt auf eines hinaus.
Jedes Milieu befreit seine Insassen von der persönlichen Verantwortung. Adam entweicht aus dem Plural in das Kollektiv.

>So wichtig aber ist der Bibel eben diese tagtägliche Wirkung der Arbeitsteilung, daß sie von ihr im ersten Atemzug schreibt.\
Wenn aber im ersten Atemzug von dieser Schwäche durch Assoziation die Rede sein sollte, dann mußte sie sich an dem baren Minimum von Assoziation in der Urmenschheit bereits nachweisen lassen.

>Es ist die Großtat der Bibel, daß ihr diese Vereinfachung gelungen ist. Denn, so sagt sie mit Recht, schon die Hälftelung in die zwei Geschlechter birgt das Geheimnis von Herrschaft und Dienst, Leitung und Angestelltem, Kapital und Arbeit.
Alle Klassengegensätze sind Abschwächungen, Schattierungen des Geschlechtergegensatzes. Denn schon durch ihn sind wir aufeinander für den kleinsten Werkerfolg angewiesen.
Wo aber Mehrzahl von uns erheischt wird, da trachten wir, in die Kollektive zu flüchten.\
Wo wir das erfahren, da verfallen wir dem Milieu und verschanzen uns hinter seine Sitten.\

>Also nicht der Sexus, sondern die Arbeitseilung bringt den Herrn Adam zu Fall, der sich hinter seine Angestellte verschanzt, wie die deutschen Tabakfabrikanten ihre Arbeiter vor den Reichstag zu schicken pflegten, um sie für höhere Zölle demonstrieren zu lassen.

>Wegen dieser Drückebergerei jedes hinter jedem schämen sich die Beteiligten und stecken sich in die schützenden Kleider des Milieus.\
Der Mißbrauch der Arbeitsteilung durch die Gruppenglieder Adam und Eva führt also zu ihrer Einbettung in ihr Milieu und damit zu der Vergesellschaftung der Arbeit in dem Gebot: „Im Schweiße eures Angesichts...“.

>Die drei Tatbestände:\
Eva heißt die Gehilfin Adams.\
„Keiner will es gewesen sein.“ Wie der Finger Gottes sie einzeln herausfindet, schämen sie sich,
und der Abschluß: die Arbeit wird nun euer Gliederungsprinzip bleiben,\
beweisen, daß die Bibel in Adam und Eva nicht Freudsche Lust, sondern volles Menschentum hineinliest, die ewigen Gebote der Arbeitsteilung und der Gruppenabschüttlung.
>*Eugen Rosenstock-Huessy: [Der unbezahlbare Mensch]({{ '/ew-der-unbezahlbare-mensch' | relative_url }}), 1935*

siehe auch\
[Carel van Schaik, Kai Michel: Das Tagebuch der Menschheit: Was die Bibel über unsere Evolution verrät](https://www.amazon.de/-/en/Carel-van-Schaik/dp/3498062166)

Rezension:\
[Friedrich Wilhelm Graf: Gott als Survivalstrategie](https://www.welt.de/print/die_welt/literatur/article158777626/Gott-als-Survivalstrategie.html)
