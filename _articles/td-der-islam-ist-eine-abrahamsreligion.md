---
title: "Thomas Dreessen: Der Islam ist eine der Abrahamsreligionen - was unterscheidet uns? "
created: 2002
category: veroeff-elem
published: 2024-04-28
---
### Stimmstein 7, 2002

#### Mitteilungsblätter  2002

### Thomas Dreessen

### Der Islam ist eine der Abrahamsreligionen – was unterscheidet uns?

#### Vorbemerkung

Ich bedanke mich bei Ihnen allen für die Ehre über dieses Thema zu Ihnen sprechen zu dürfen. Aus der Formulierung, die Sie mir aufgaben, entnahm ich, dass Sie und ich heute abend unter einer gemeinsamen Voraussetzung miteinander hören und sprechen. Wir sind hier versammelt unter der Voraussetzung, dass der Islam eine der drei Abrahamsreligionen ist! Eine grundlegende Anerkennung haben wir schon vollzogen - einen der Grundschritte die den interreligiösen Dialog kennzeichnen, sind wir schon gegangen. Wir erklären mit diesem Schritt: Du - Islam - sollst selber zu uns sprechen. Wir wollen ohne Vorurteil hören und sind bereit alle alten Urteile zu verändern.

Nun, ich bin nicht Muslim, sondern Christ. Ich spreche zu Ihnen heute abend sozusagen in Vertretung für unsere muslimischen Bürger, Nachbarn, Partner und Freunde, soweit es die Darstellung des Themas Islam betrifft. In Hinsicht auf unseren Christlichen Glauben spreche ich als evangelischer Theologe zu Ihnen. Beides tue ich in dem Vertrauen, dass die Anerkennung des Islam, die Sie und ich vollzogen haben, ein wichtiger Schritt ist in der Ökumene der drei Abrahamsreligionen in Meiderich und darüber hinaus. Ich gebe Ihnen jetzt einen kurzen Überblick:

Zunächst will ich mit Ihnen einen Blick auf unsere Situation werfen.
- Wir Menschen leben im 20. Jahrhundert in einer Welt.
- Die Zukunft des Lebens des Menschengeschlechtes und der ganzen Schöpfung ist keineswegs sicher.
Wir entdecken, dass zur Erschaffung unserer Zukunft die Hoffnung auf den selben Ursprung-Gott-untrennbar hinzu gehört.

Der zweite Abschnitt heißt: Der Islam - eine der Abrahamsreligionen. Der Schwerpunkt der Darstellung liegt auf den uns verbindenden Gemeinsamkeiten.

Der dritte Abschnitt heißt: Islam - Hingabe an den einen Gott. In diesem Abschnitt versuche ich, notgedrungen in Kürze, das Zentrum, das Herz des Islam aufzuzeigen und zu würdigen.

Im vierten Abschnitt: Unterscheidung und Ergänzung versuche ich aus christlicher Sicht die Unterschiede darzustellen und als Momente im Gespräch der Wahrheit zu begreifen, dass das Menschengeschlecht zum Lobe Gottes da ist. Hölderlin: Seit ein Gespräch wir sind und hören können voneinander. Rosenstock-Huessy:  Respondeo, etsi mutabor!


### 1. Unsere Situation

#### Eine Welt
  Wenn wir Fernsehen, Radio hören oder die Zeitung aufschlagen, dann werden wir selbstverständlich mit den Problemen fremder Länder, von den Schicksalen von Menschen betroffen, denen wir zumeist leiblich nie begegnen werden oder begegnet sind.

Wenn wir einkaufen, bekommen wir die Produkte dieser Völker angeboten - das brauche ich in einer Frauenhilfe nicht besonders zu betonen - und viele Menschen in unserem Land bereisen die ganze Welt geschäftlich und privat.

Die Erde ist heute unsere Welt, so wie sie es noch nie war. Es gibt keinen Ort und keinen Menschen mehr, der nicht mit allen anderen in Verbindung steht (Telefon, Fernsehen, Radio, Ökonomie, Reisen, Forschung, Gefährdung der Schöpfung usw.) das ist unsere Lebenssituation: Wir leben heute weltweit – die Welt ist nur noch eine! Diese weltweite Lebensweise hat auch ein sehr konkretes Gesicht im Inland: Das sind die vielen Ausländer und insbesondere die Gastarbeiter, die seit ca. 30 Jahren in der Bundesrepublik Deutschland leben und arbeiten. Diese Menschen sind das bei uns anwesende menschliche Gesicht unseres weltweiten Lebens. (Ohne ihre Gegenwart hätten viele vielleicht nicht gemerkt, dass die Zeiten nationaler Alleingänge und Verirrungen ein für allemal vorbei sind.)

Ich fange mit dieser Analyse unserer Situation an, weil ich Sie auf einen wichtigen Widerspruch in unserem gesellschaftlichen Leben aufmerken werden lassen möchte. Wir leben bis ins kleinste Dorf weltweit  – doch unsere Politik und unser Bewußtsein, auch bei vielen Kirchengliedern, ist noch sehr auf die primäre Durchsetzung unserer Interessen fixiert. Das heißt, wir haben die Situation der einen Welt in der wir heute leben noch nicht wirklich begriffen und verhalten uns noch nicht diesem Lebenshorizonte angemessen. Ein praktisches Beispiel ist die Nichtanerkennung der „Gastarbeiter“ als vollberechtigte Bürger.


#### Das eine Menschengeschlecht

 In dieser einen Welt, in der wir heute leben, die von Zerstörung bedroht ist, in dieser Welt fragen wir als Christen, als gläubige Menschen: „Was hören wir für uns als den Willen Gottes? Wenn wir fragen, haben wir uns nicht irgendwelchen Untergangsvisionen hingegeben! Wir fragen danach, was Gott mit uns vor hat – wir fragen nach Orientierung durch Gott!

Ich glaube, dass wir uns heute in einem ganz entscheidenden Stadium der Erschaffung des einen ‚großen Menschen’, des einen Menschengeschlechtes befinden. Wir stecken mitten in einem Schöpfungsprozeß drin und viele merken es; denn die Erschaffung eines neuen Menschen geht nicht ohne Veränderungen, – die Geburt einer neuen Epoche nicht ohne Schmerzen ab.

Vielleicht fragen Sie: „Das sind doch Glaubenssätze. Erschaffung eines neuen Menschen – was soll das heute abend in diesem Gespräch und – woher weißt Du das?“

Ich antworte: Ja - es geht um die Erschaffung eines neuen Menschen und diese Schöpfung ist eminent wichtig für unser Thema heute abend! Unser Thema und die Analyse unserer heutigen Lebenssituation, – sie machen uns doch schmerzlich bewusst, dass viele alte liebgewordene Gewohnheiten uns heute nicht mehr erfüllen. Alles ist in Fluss gekommen. - Ich glaube, dass in jedem von uns schon sehr viel von diesem neuen Menschen lebt, sonst säßen wir heute nicht zusammen.


#### Ein Gott

 Die ökumenischen Bewegungen in den Kirchen in unserem Jahrhundert sind die sichtbare Gestalt dieses Schöpfungsprozesses des einen Menschengeschlechts in der einen Welt. Ich will an dieser Stelle – in Erinnerung an die letzte Vollversammlung des Ökumenischen Rates der Kirchen in Vancouver 1983 – drei Charakteristika dieser Bewegung nennen. Sie ist, erstens, gekennzeichnet durch eine immer zunehmende Vielfalt und Buntheit der Teilnehmer und Teilnehmerinnen. Diese Vielfalt lebt, zweitens, im Bewusstsein, dass wie Menschen eine gemeinsame oder keine Zukunft haben werden. Drittens, gewann die ökumenische Bewegung (wie auch die katholische Kirche im Vaticanum II) die Einsicht, dass Gott nicht nur der Schöpfer aller Menschen ist, sondern dass Gott auch außerhalb der Kirche angerufen wurde und wird. Diese Einsicht erhielt in Vancouver Gestalt in der Einladung und Teilnahme von muslimischen, buddhistischen, jüdischen und auch indianischen Gästen. Es ist eine Neuentdeckung des Wortes Pauli:

„Gottes unsichtbares Wesen, das ist seine ewige Kraft und Gottheit, wird seit der Schöpfung der Welt ersehen aus seinen Werken, wenn man sie wahrnimmt“ (Röm 1,20).

In den genannten drei Momenten, der vielfältigen Gestalt des Menschengeschlechts, seiner gemeinsamen Zukunft und seines gemeinsamen Ursprungs in „Gott, der der Vater des Menschengeschlechts und ihr Ziel ist“, Vaticanum II, erblicke ich Zeichen des Heiles, Anzeichen des neuen Friedens. Auf dieser Grundlage wird auch das Verhältnis von Juden, Christen und Muslimen neu geschaffen, – den dreien, die doch auch in Abraham eine weitere gemeinsame Wurzel entdecken.

Zu der heilsgeschichtlich besonderen Rolle des Christentums im Prozeß der Erschaffung des einen Menschengeschlechtes möchte ich an dieser Stelle nur soviel sagen: Diese Erschaffung ist das Ziel Christi (siehe Eph 2,11 und 1. Kor 15, 20-28).

### 2. Der Islam - eine der Abrahamsreligion

Dieser Satz ist programmatisch zu verstehen. Von seinem Selbstbewusstsein her, ist der Islam nicht keine neue Religion. Nein! – er ist die wahre Religion, das wahre Verhältnis der Menschen zu Gott immer schon gewesen (2,129). Die dem Propheten Mohammed gegebene Offenbarung des Koran gilt allerdings als die Offenbarung des Islam für das Volk der Araber und als die letzte abschließende Offenbarung Gottes für seine Menschen (13,39; 43,4; 5,48). Diese letzte Offenbarung korrigiere die Fälschungen, die von Menschen an den älteren Offenbarungen vorgenommen worden seien (5,13.14). Als ältere Offenbarungen werden genannt: die Taurat – das ist die Thora des Mose und das Indschil - das Evangelium, das Jesus gegeben wurde (2,137). Nach dem Koran war Abraham weder Jude noch Christ (2,130). Er repräsentiert den Islam in ursprünglicher Reinheit, in ihm haben Juden, Christen und Muslime eine gemeinsame Wurzel. Nach Islamischen Verständnis gehören Adam, Abraham (Ibrahim), Noah (Nuch), Mose (Musa), David (Davut), Jesus = Isa ben Mirjam und abschließend Muhammad zu den besonderen Gesandten Gottes, den Hanifen (siehe. 3,60; 3,67). Bei ihnen finden wir die Religion Abrahams (4,124; 2,129). Sie sind die wenigen wahren Gläubigen.

Trotz der Veränderungen der hl. Schriften – nach Islamischer Sicht–, nennt Mohammed aus dieser Gemeinsamkeit heraus Juden und Christen die „Leute (Volk) der Schrift...“ (3,58ff und 65; 5,70; 29,47; 2,140). Sie gelten als gläubige Menschen (2,140). Mit ihnen sollen die Muslime wetteifern in Taten der Liebe (5,48). – Dass es im Koran auch sehr kritische Urteile über Christen und Juden gibt, soll nicht geleugnet werden (5,56)[^1] Diese ändern aber nichts an dem positiven Grundverhältnis, das ich ein geschwisterliches nennen möchte. –

[^1]: Cf. M. S. Abdullah: a. a. O. ...politische Anweisungen

Das genannte positive Grundverhältnis zu Juden und Christen hat sich in der Geschichte des Islam bewährt in der Toleranz gegenüber den jüdischen und christlichen Mitbürgern. Das Zusammenleben der drei im mauretanischen Spanien bis zum 13. Jahrhundert oder in Palästina bis in das 19. Jahrhundert (denken Sie an Lessings Nathan der Weise und an den Sultan Saladin), in Afrika (siehe Jasper) ist in der christlichen Geschichte ohne Entsprechung.

Ich werde jetzt die Wahrnehmung Abrahams im Islam noch etwas vertiefen, so dass Sie aus den Themen der koranischen Darstellung Abrahams verstehen lernen, mit welch tiefem Recht er „Vater des wahren Glaubens“ von den Muslimen genannt wird.[^2]

[^2]: cf. Röm, 4,16-17: Vater vieler Völker

Das höchst Fest des Islamischen Jahres heißt auf Türkisch Qurban Bayrami - Opferfest. Qurban (hebr. Korban) heißt Opfer, genauer: Schlachtopfer. Im Schlachtopferfest, das jedes Jahr am Ende der Pilgerreise nach Mekka begangen wird, vollzieht jeder Muslime das Opfer Abrahams nach. Es ist die Erinnerung an Abraham, der in seiner Hingabe an Gott sogar bereit war seinen Sohn, den einzigen, den verheißenen, Gott zu opfern (37,98-113). Juden, Christen und Muslime haben diese Geschichte als gemeinsames Fundament, weil Gott sich dem Abraham offenbart als der, der nicht den Tod des Sohnes will. Gott will den Sohn für sich, frei von dem absoluten Herrschaftsanspruch des Vaters. Im Alten Testament heißt deswegen Gott nach der Opferung Isaaks der Gott Abrahams, Isaaks und Jakobs. Derselbe Gott ist allen dreien jeweils neu offenbart, Vater, Sohn und Enkel. Koranisch: Der Gott Abrahams, Mose, Jesu, Mohammads.

Als zweite Geschichte nenne ich den Auszug Abrahams. Dieser wird in der Bibel nur sehr kurz angesprochen (gen 12,1): Und der Herr sprach zu Abraham: „Geh aus deinem Vaterland und von deiner Verwandtschaft und aus deines Vaters Hause in ein Land, das ich dir zeigen will... in dir sollen gesegnet werden alle Geschlechter auf Erden“.

Der Schwerpunkt dieser biblischen Erzählung liegt in der Verheißung der Nachkommenschaft, in dem Versprechen, dass Abraham ein Segen sein soll für alle Menschen. Anders die koranische Überlieferung: Sie erzählt sehr ausführlich von den Umständen, die zum Auszug Abrahams geführt haben. Abraham ist aufgestanden im Namen des einen Gottes gegen die vielen Götter-Götzen seiner Heimat und seiner Familie. (21,50-70). Diese Geschichte erinnert sehr an die Götzenkritik, wie wir sie bei Jeremia 10 und öfter in der Bibel finden. Abraham zerstört die vielen kleinen Götzenbilder und lässt nur ein großes heil. Gefragt, wer die Götter zerstört habe, macht er die Leute lächerlich, indem er behauptet, der große Götze habe sie alle zertrümmert. Hier erkennen wir schon den Herztrieb des Islam, – Gott ist einer (Sure: 1,112).

Auf diesem geschichtlichen Hintergrund können wir verstehen, warum diese Abrahamerzählung Mohammed so besonders wichtig war. In seiner Vaterstadt Mekka, deren Zentrum, die Kaaba ist (ein von Abraham nach nicht biblischer Überlieferung für den einen Gott gestiftetes Heiligtum), – in dieser Stadt hatten die reichen Familien viele (nach der Überlieferung 360) Götzen neben die Anbetung Gottes gestellt. Ja, – die Anbetung des einen Gottes hatte man aufgegeben. In diese Situation hinein erfuhr Mohammed eine Offenbarung des wahren Glaubens Abrahams.

Die Bibel kennt diese koranisch selbständige Überlieferung von Abraham. Wir erkennen, dass Mohammed keineswegs als ein Plagiator – so ein christliches Vorurteil – angesehen werden kann. Gerade die Selbständigkeit dieser Überlieferung führt uns weiter auf dem Weg ihrer Achtung. Die Mitte, das Herz dieser Offenbarung ist der Glaube an den einen, einzigen Gott, der uns Menschen befreit von der Herrschaft der von Menschen gemachten Götzen (Tawhid).

In der Sure 112 wird dieses Herz des Glaubens eindrucksvoll ausgesprochen:
Sprich: Er ist der eine Gott, \
Der ewige Gott;\
Er zeugt nicht und wird nicht gezeugt,\
Und keiner ist Ihm gleich.

In der Einheit Gottes haben Juden, Christen und Muslime ihre tiefe und unzerstörbare Gemeinsamkeit. Das Urteil, der Gottesname Allah sei von dem christlichen Gottesnamen verschieden und deshalb wäre ein anderer Gott angesagt, greift zu kurz. Allah heißt in arabischer Sprache Gott (hebräisch el/elohim). Arabisch sprechende Christen beten zu Allah.

Muslime lehnen die Gottessohnschaft Jesu – als leibliche Sohnschaft verstanden – ab. Sie kritisieren Tritheismus, nicht Trinität. Diese kennen sie theologisch nicht. Der Islam sieht in Jesus mehr als nur den Rabbi aus Nazareth.

„...er ist für den gläubigen Muslim Teil des eigenen Heilsweges, Person der Heilsgeschichte, Profet Israels, Barmherzigkeit von Gott und Beispiel für die Menschen. Er wird auch Messias genannt“ (Masih).[^3]

[^3]: cf. MS Abdullah ; Belege:...

### 3. Islam - Hingabe an den einen Gott

 Das Bekenntnis zu dem einen Gott ist im Islam untrennbar von der Lebenshaltung und -führung. In der Übersetzung: Islam – Hingabe an den einen Gott – kommt dieses meines Erachtens gut zum Ausdruck. Ich will dieses Verständnis des Islam im Folgenden ausgehend vom Grundgebet Fatiha und mit Hilfe der sogenannten fünf Säulen des Islam entfalten, soweit das in Kürze möglich ist. Ein Besuch in einer Moschee und Gespräche mit Muslimen sind zur Vertiefung des hier Angesprochenen unbedingt erforderlich und zu empfehlen. Hören Sie die 1. Sure des Koran, genannt „Fatiha“ – die Eröffnende:

Im Namen Allahs, des Erbarmers, des Barmherzigen!\
Lob sei Gott, dem Weltenherrn,\
Dem Erbarmer, dem Barmherzigen,\
Dem König am Tage des Gerichtes!\
Dir dienen wir und zu dir rufen um Hilfe wir;\
Leite uns den rechten Pfad,\
Den Pfad derer, denen Du gnädig bist,\
Nicht derer, denen Du zürnst, und nicht der Irrenden.

Die Fatiha ist das Grundgebet des Islam. Sie wird täglich zu allen fünf Gebetszeiten gebetet. Der in ihr genannte Name Gottes, als des Erbarmers, des Barmherzigen, der der vornehmste Name Gottes ist, er liegt den Muslimen sozusagen auf der Zunge bei allem was sie tun und lassen: Bismillah i’rahmani Rahim.

Wenn wir uns den Text der Fatiha anschauen, dann entdecken wir Gott als den Weltenherrn – das heißt als Schöpfer des Himmels und der Erde, als Erbarmer – Gott ist Liebe, und als den König am Tag des Gerichtes – wir hören: jüngstes Gericht. Weiter bitten die Gläubigen in der Fatiha um Rechtleitung durch Gott.

Ich denke, dass wir beim Hören dieses Gebetes erneut eine tiefe Gemeinschaft mit unseren muslimischen Brüdern und Schwestern spüren. Die Fatiha wurde zu Recht mit dem Herrengebet verglichen[^4]. Eine Gebetsgemeinschaft ist hier  möglich. Allerdings werden sowohl die Fatiha, als auch die Basmala, als auch das Glaubensbekenntnis Shahada in arabischer Sprache, in der Sprache des Koran, gesprochen. Das schließt ein Verstehen nicht aus, – aber das intellektuelle Verstehen ist für den Islam nicht das erste und nicht das entscheidende (sie mögen hier einen Vergleich erlauben mit der Bedeutung der lateinischen Sprache als Gottesdienstsprache der katholischen Kirche bis zum zweiten Vaticanum).[^5] Dies gilt jedenfalls für den sunnitischen Islam, der in der Vielfalt des Islam die Mehrheit bildet und auch im christlich-muslimischen Gespräch in Duisburg unser Gesprächspartner ist.

[^4]: Annemarie Schimmel: Denn Dein ist das Reich./ Ulrich Schoen: Muslime unsere Nachbarn S. 22ff
[^5]: siehe dazu kritisch Franz Rosenzweig: Neues Denken

Hingabe an Gott wird also zuerst eingeübt in der Rezitation der heiligen Texte, im dem rituellen Gebet und den dazugehörigen Reinigungen. Dies alles geschieht in Gemeinschaft. In dieser Gemeinschaft steht jeder Muslim vor Gott, – Mann und Frau.

Von Kind auf lernen die Muslime den arabischen Koran. Sie sollen sozusagen Sprachrohr, Mund werden, aus dem die heiligen Worte erneut erklingen (cf. 1. Kor. 4,6; Ps 103 „Du meine Seele singe“). Wer den Koranvortrag einmal erlebt hat, der weiß, dass wir eher singen, denn sprechen oder lesen erleben. Das Wort des Koran, Wort Gottes erschafft auf diesem Weg seine Gläubigen.

Ich erinnere Sie an dieser Stelle an die Bibel. Im 1. Buch Mose wird Gottes Schaffen beschrieben mit den Worten: Und Gott sprach: Es werde Licht, und es geschah also. ....( Kapitel 1 – bara). Oder grundlegend im Johannes Evangelium: Am Anfang war das Wort und das Wort war bei Gott und Gott war das Wort... alle Dinge sind durch dasselbe gemacht... (1,ff) und das Wort ward Fleisch (cf. Sure 2,117; 40,68 u. ö.).

Die Erschaffung durch das Wort eint die drei Abrahamsreligionen.[^6] Die Rezitation ist die immer wieder erfolgende Prozedur der Begeisterung der Nachwachsenden mit der Sprache der Gemeinschaft (siehe dazu Eugen Rosenstock-Huessy: Soziologie II). Hierin liegt die Botschaft des Islam für uns evangelische Christen. In jedem Muslim soll der arabische Koran klingen, denn der Koran ist nach muslimischem Verständnis Gottes Wort, – nicht geschaffen, sondern ursprünglich.

[^6]: Wolfgang Ullmann zu Eugen Rosenstock-Huessy Sprache und Religion: ...Sprache und nicht Religion sind die primäre Ebene und das primäre Medium der Relation Gottes und des Menschen. Die Sprache ist das Indiz dafür, dass man den Menschen in letzter Instanz nur als den Angeredeten Gottes definieren kann. (in stimmstein 2, 174f.) - a. a. O 156: Rosenzweig Abrahams Nachkommenschaft ( Hebr. 10,25;  Röm 11,25;  1. Kor 15,27).

Selbstverständlich wissen die Muslime, dass es von Menschen aufgeschrieben ist, doch diese haben nur geschrieben, was Mohammed Wort für Wort von Gott offenbart bekam und verkündet hatte. Diese Auffassung der wörtlichen Inspiration (= Begeisterung) des Koran war im Bezug auf die Bibel lange Zeit auch christliche Lehre. Wir teilen diese Lehre so heute nicht mehr, sondern betrachten und hören die heiligen Schriften in ihrer Zeit und fragen dann: Was will Gott uns heute sagen?

Die Muslime stellen diese Frage auch, aber sie stellen sie zuerst an den Koran. Was hat Gott uns im Koran gesagt, was wir tun sollen? Denn: Im Koran hat Gott den Muslimen die rechte Kenntnis seiner Lebensordnung angegeben. Wir verstehen dieses falsch, wenn wir es als aufgelegtes Gesetz, als Zwang verstehen. Gottes Lebensordnung wird als heilsam erfahren, weil sie Frieden und Leben bewahrt. So deuten wir sie muslimisch besser als Wohltat, als heilsame Weisung, als Rechtleitung:

 „Gott, der eine, erwartet von den Menschen, dass sie ihn in seiner Größe und Erhabenheit anerkennen, sich ihm unterordnen und ihm gehorchen. Das ist Islam, die vollständige Hingabe. Islam bedeutet Selbstauslieferung und Dienst... Darin ist zugleich die Verheißung eingeschlossen, dass der Mensch durch diese Hingabe den ihm von Gott zugewiesenen Platz findet und so Frieden erlangt.“ (Sure 55) [Sure 55 kann nicht stimmen! Soll hier wirklich ein Info 3 als Beleg genannt werden? A. M.]

Diener Gottes zu sein, darin besteht die Würde es Menschen. Er ist geschaffen wie alle anderen Geschöpfe, seine besondere Würde besteht darin, dass er zum Statthalter Gottes (Khalifa) ernannt ist (siehe 2,30). Die Menschen können zur Rechtleitung ja oder nein sagen. Darin besteht ihre Verantwortlichkeit, die Verantwortung jedes einzelnen Menschen für sein Verhalten vor Gott. Wie im christlichen Glauben werden die Menschen im Islam im Gericht zur Verantwortung gezogen: „Jede Seele schafft nur für sich, und eine belastete (Seele) soll nicht einer anderen Last tragen“ (Sure 6,164 u. ö.). Die eine unvergebbare Sünde besteht nach dem Koran darin, dem einen Gott andere Götter beizugesellen: Shirk. „Neben dem Koran gilt als zweite Quelle der Rechtleitung die Berichte vom Leben des Propheten Muhammad. Sie heißen ‚Hadithe’ oder Sunna, d. i. verbindliche Tradition (Weg). Die sogenannten fünf Säulen des Islam sollen diesen Abschnitt abschließen.

***Das Glaubensbekenntnis – Shahada***\
*Ich bezeuge, dass es keinen Gott gibt außer Gott und\
Ich bezeuge, dass Muhammad der Gesandte Gottes ist.*

In der Schahada werden die Einheit Gottes, die Verbindlichkeit der Tradition und die zentrale Bedeutung Mohammads, des abschließenden Gesandten Gottes, bekannt.

***Das rituelle Gebet – Salat***

Im Salat geben sich alle Muslime fünfmal täglich in der örtlichen und in der Weltgemeinschaft des Islam Gott hin. Hierdurch wird die muslimische Gemeinschaft erschaffen und bewahrt. – Es gibt auch Dua, das freie Gebet.[^7]

[^7]: Siehe Texte bei A. Schimmel: a. a. O; Khoury uam

***Die Armensteuer - Zakat***

ist die dritte Säule des Islam. Sie war einige Jahrhunderte lang ein sehr wirksames Sozialhilfesystem mit klaren Richtsätzen. Soziale Hilfe als Recht, nicht als Gnade. Die soziale Verantwortung untereinander ist eine direkte Entsprechung zur Unterwerfung unter Gott. – Die Zakat ist auch hier in Duisburg in den muslimischen Gemeinen noch lebendig (Beispiel: bulgarisch muslimischer Asylant). 

***Das Fasten im Monat Ramadan (türkisch Ramazan) –Saum.***

In diesem Monat ist den Muslimen von Sonnenaufgang bis Sonnenuntergang ein vollständiges Fasten aufgegeben. Es dient der verstärkten Hingebung und Konzentration auf Gott und endet nach muslimischem Kalender mit der ersten Offenbarung des Koran.

***Die Pilgerfahrt nach Mekka - Hadsch***

Jeder Muslim sollte sie nach Möglichkeit einmal in seinem Leben durchführen. Wirtschaftliche Not u. a. entbindet von dieser Pflicht.

Wenn Sie diese fünf Säulen näher betrachten, dann werden Sie feststellen, dass sie eine Gesamtkomposition darstellen, die die Hingabe an Gott sehr umfassend dem Leben jedes Muslimen voranstellt, einsenkt, einübt.

*Jeden Tag – Schahada und Salat\
Jedes Jahr – Saum\
Einmal  im Leben – Haddsch\
Immer – Zakat*

Hingabe an Gott schließt von vornherein auch die Konstituierung der von Gott rechtgeleiteten Gemeinschaft ein: Umma.


### 4. Unterscheidung und wechselseitige Ergänzung

 Im Bekenntnis der Gottessohnschaft Jesu und im Glauben an den dreieinigen Gott „Vater – Sohn – Heiliger Geist“ sehen wir Christen die Mitte unseres Glaubens. Genau gegen diese Mitte wendet sich der Islam seit seinem Anfang kritisch:

„...der Islam stößt sich am Kreuz, an der von den Christen geglaubten Erlösungstat. Zwischen Christen und Moslems steht nicht der neue Mensch Jesus, sondern der Heiland und Erlöser, steht nicht der Messias Israels, der getreue Gottesknecht, sondern der Gottessohn in seiner nicaenischen Ausdeutung, steht das Dogma der Kirche der Konzilien.“[^8]

[^8]: M. S. Abdullah: Abrahams Söhne... unveröffentlichtes Manuskript, 1982, S. 12

Denn der Islam sieht in der Trinität die Einheit Gottes aufgehoben (Sure 4 u. ö.). Bis heute besteht immer wieder der Verdacht, wir Christen würden dem einen Gott andere Götter beigesellen (Jesus, Maria, Heiliger Geist). Die Namen „Gott der Vater – der Sohn – der Heilige Geist“ haben dieses islamische Missverständnis der Trinität sicher befördert; denn die vielen Götzen im vorislamischen Mekka waren als Götterfamilie gedacht und verehrt worden.

Obwohl nicht auszuschließen ist, dass eine Aufspaltung der Trinität in drei Götter in der christlichen Geschichte auch geschah, so bleibt doch festzuhalten, dass die Trinität im christlichen Verstehen von Anfang an nicht eine Vielgötterei war, sondern der Versuch ist, Gottes Handeln an uns Menschen in Jesus Christus zu verstehen. Die Einheit Gottes wird dadurch nicht in Frage gestellt.[^9]

[^9]: Siehe Imam Mehdi Razwi auf dem Deutsch Evangelischen Kirchentag 1987

Im modernen christlich-islamischen Dialog treten muslimische Theologen auf, die dieses anerkennen. Der Pakistani Prof. Khalid Alawi, den ich 1986 in Birmingham kennenlernte, beschreibt Gott in dreifach verschiedener Weise: als Person – in seinen Attributen – und in Aktion. Christliche Theologen hören hier deutliche Anklänge an die christlichen Begriffe: Gott der Schöpfer – Gott der Versöhner – Gott der Erlöser und in allen einer.[^10]

[^10]: cf. Georges Khodre: Das Christentum in einer pluralistischen Welt – das Werk des Heiligen Geistes. St. Pölten 1984; Dialog mit anderen Religionen, hg.Margull,Samartha 1972 [Kann diese Fußnote stimmen? A. M.]

	Näher können sich Christen und Muslime im Glaubensbekenntnis vielleicht nicht kommen. Die besondere Offenbarung Gottes in Christus Jesus ist kein Begriff, sondern namentlich. Sie begründet für uns die Möglichkeit der Rede von „Vater – Sohn  – Hl. Geist“ und den Auftrag diesen zu bezeugen.

Wir Christen versuchen mit der Trinität die Erfahrung von Gottes Handeln zu beschreiben, wie sie uns in Christus Jesus widerfährt. Aus christlicher Sicht dürfen wir keine der drei Personen Gottes leugnen oder vergessen. Hier erkennen wir die Leugnung Gottes, des Schöpfers wie sie in unserer Christlichen Kultur und der aus ihr hervorgegangenen Wirtschaft und Wissenschaft heute die Welt beherrscht, als Sündenfall der Christen (cf. 1. Kor. 15,22-28).

In der Trinität ist das Geheimnis beschrieben, der Weg zum Vater und zum Erlöser markiert, wenn denn wir Menschen wieder von Gott getrennt leben.[^11]

[^11]: Siehe Trinität in Zettelkasten / ERH

Jesus kam, als totaler Unfriede zwischen den Generationen herrschte (siehe Mal 3) und besonders auch zwischen den verschiedenen Sprachen und Religionen im römischen Reich, –zwischen Juden und Griechen, Freien und Sklaven, Mann und Frau, vgl. Gal 3,28; Eph 2,11ff u. ö.). Jede einzelne Gruppe hatte ihr Gesetz und ihre Lebensregeln verabsolutiert. Diese Abgeschlossenheit gegeneinander nennt das Neue Testament die Sünde, den Tod. Diese Sünde heißt konkret: Der andere, der in meiner Stadt lebt, ist nicht von Gott geliebt, er kann nicht zu Gott gehören, eben weil er anders ist als wir.[^12]

[^12]: cf.E.Rosenstock-Huessy: Atem des Geistes 101

So erkennen wir, dass das Besondere Christi gerade darin besteht – das ist seine Sohnschaft – dass er alle Trennungen im Vertrauen auf Gott und in Liebe zu den Menschen zwischen Menschen, Völkern, Generationen überwindet. Er zeigte uns Menschen den Weg zu Gott, zu neuem Leben aus dem Ursprung, indem er sein Leben freiwillig dahin gab. Ostern, da erklingt der große Jubel: Christus ist wahrhaft auferstanden! Der Tod hat nicht das letzte Wort (Eph 2,4 u. ö.; 1. Kor. 15,12ff).

Fast 2000 Jahre lang hat die Kirche diese Entdeckung Jesu Christi über die ganze – Welt getragen. Heute aber gehört die Kraft zur Überwindung der Trennungen nicht mehr der Kirche. Sie gehört jedem Menschen. Wenn Sie sich an meine Einleitung erinnern, dann beschrieb ich Ihnen unsere heutige Zeit als die, in der wir uns weltweit gegenwärtig geworden sind (Weltkriege, Wirtschaft, Fernsehen usw.). Dies nenne ich den Erfolg Christi: Christus hat die Welt und die Menschen weltweit füreinander geöffnet in dem er jede Verabsolutierung einer Teilwelt aufgehoben hat - nur eine Verabsolutierung blieb bis in unsere Tage. Das sagt der Satz: Extra ecclesiam nulla salus. Der Heilige Augustinus verweist uns darauf, wenn er schreibt: „Wenn die Mission Christi erfüllt ist, dann muß das Kreuz selber gekreuzigt werden“ - crux haec ipse crucifigenda est (zit. nach Eugen Rosenstock-Huessy: Die Europäischen Revolutionen). Ich übersetze: Dann muss die Kirche anerkennen, dass Gott größer ist als seine Kirche ( 1. Kor 15,23-28). Diese Anerkennung entdecke ich im heutigen interreligiösen und interideologischen Dialog.[^13] Die Kirche kann heute das Bekenntnis zum Namen Christus nicht mehr als allgemeine Bedingung des Heils setzen (so stellt das II. Vaticanum fest); denn die Einheit, die in 2000 Jahren erschaffen worden ist, saugt heute alle Verschiedenheiten auf, sie zerstört sämtliche Ursprünge des Lebens, auch die vorchristlichen. Die wissenschaft-technische Revolutionierung unserer Welt hat eine christliche Wurzel. Und deswegen ist auch der Christusname als Heilsname heute mit Augustinus zu kreuzigen. Er ist zu sehr verabsolutiert worden und zerstört heute eher, denn dass er befreit: die Mission der Welt ist zu Ende.

[^13]:Siehe in „Die europäischen Revolutionen“ das Motto von Franz Rosenzweig: „...den Dialog aus diesen Monologen halte ich für die ganze Wahrheit...“. In „stimmstein“ 2, 1988, S.147 von Wolfgang Ullmann: Die Entdeckung des Neuen Denkens, eine Interpretation des Religionsgespräches Rosenzweig, Rosenstock, Ehrenberg 1913, 1916 ... (163) „Aber nun hat Rosenstock seinerseits die Initiative ergriffen. Initiative wozu? Zu der Frage, ob Jude werden post Christum natum nicht notwendigerweise ein Ausdruck jüdischer Verstockung gegenüber Christus sein müsse. Das Einzigartige dieses Briefgespräches besteht nun darin, dass man (nicht) in den nur zu beliebten Austausch interreligiöser Höflichkeiten mit all ihren unvermeidlichen Trivialitäten verfällt oder die Grenze der Verständigungsmöglichkeiten für erreicht erklärt. Rosenzweig tut weder das eine noch das andere, sondern beantwortet die Frage nach seiner Entscheidung und ihrer möglichen Identität mit jüdischer Verstockung durch ein rückhaltloses Ja. Er kann das, weil er ganz sicher ist, nicht in Trotz und Selbstbehauptung zu verfallen. Die Psychologie des Kampfes um eigene Identität und Selbstverwirklichung war ja gerade in jenem Religionsgespräch für immer verlassen worden, in dem ein ganz neues Kapitel der gemeinsamen Geschichte von Christen und Juden aufgeschlagen worden war. Thema dieses neuen Kapitels war die plötzliche Klarheit darüber, dass es gerade die Einheit der Offenbarung Gottes ist, welche die beiden nicht nur verschiedenen, sondern sogar gegensätzlichen Existenzweisen und Lebenswege des Christen und des Juden begründet. Rosenzweig berichtet, wie ihm angesichts der Lektüre der Schriften des Tertullianus gegen Marcion aufging, dass nicht die Konzilsdogmen des 4.-8. Jahrhunderts, sondern das antignostische Bekenntnis des 1. und 2. Jahrhunderts den substantiellen Inhalt des christlichen Dogmas ausmacht, das in Christus den Sohn des Schöpfers als Herrn anruft. Und er wagt es, von hier aus die atemberaubende Perspektive einer Konkordanz der christlichen und der jüdischen Dogmengeschichte zu entwerfen. Ein Zusammenhang, den Rosenzweig für intellektualisierbar hält durch und durch, dies aber nicht auf Grund der historischen Relativierbarkeit des Dogmas, sondern im Gegenteil auf Grund der Gewißheit von „dem großen sieghaften Einbrechen des Geistes in den Ungeist, das man Offenbarung nennt“ (Briefe 671). Rosenzweigs Frage: Erklären Sie mir Ihren jetzigen Begriff vom Verhältnis Natur und Offenbarung (Briefe 675) ...Rosenstocks zwei Antworten basieren auf Johannes 1,14  ( siehe auch Kleine Schriften 358): Offenbarung ist Perspektivenwechsel,...Offenbarung ist Orientierung in der Natur, Orientierung in der Gesamtwirklichkeit durch die Erleuchtung ihres Inhaltes und Umfanges... (165)..Johannes 1,14 ist auch in dem Sinne eine soteriologische Aussage, dass die Inkarnation – und zwar sie allein – unser Bewußtsein von der Absolutheit und undurchdringlichen Identität, ja Tautologie seiner Selbstreflexion befreit: Hier setzt die Logoslehre des Heilandes ein. Der Logos wird von sich selbst erlöst, vom Fluche, immer nur in sich selbst sich zu berichtigen. Er tritt in eine Verbindung mit dem Erkannten. Das Wort ward Fleisch -– an dem Satz hängt wohl alles“ (Briefe 679). Und diese Sprachbefreiung ist es, deretwegen Glaube im jüdisch-christlichen Sinne zu einer metareligiösen Wirklichkeit werden läßt. Widerfährt doch allein dem Glauben, der den archimedischen Punkt außerhalb seiner selbst gefunden hat, die Erfahrung des Kreuzes der ganzen Wirklichkeit....Rosenstock:“ Der Glaube ist ein Vermögen, das nur moralisch Gesunden voll innewohnt, das durch Unreinheit zerbricht und sich auflöst, ist ganz handfest einer sonstigen Naturkraft vergleichbar. Christus hat den Durchbruch dieser auf Erden latenten, gebundenen Kraft in den Weltraum gen Himmel, uns vermittelt. Wo vordem nur Abrahams Schoß war, ist jetzt lebendige Ewigkeit und Aufstieg der Geister von Stern zu Stern. Die Offenbarung bedeutet den Anschluß auch unseres Bewußtseins an den über die Erde hinaus reichenden Welt- und Himmelszusammenhang.“ (Briefe 676) ....(166) ..Begriff der Inkarnation: Einheit von Offenbarung und Inkarnation I. nicht mehr wunderhafter Anfang des Lebens Jesu, sondern Ausdruck für den Offenbarungscharakter seines gesamten Lebens als Christus,..... Wo aber ist in dem Welt und Himmelszusammenhang von Offenbarung und Inkarnation ein Platz für den Juden? .....basiert die individuelle Möglichkeit der Verstockung auf einer geschichtlichen Realität, verkörpert von der Synagoge als dem Bild der   offenbarungslosen Wiederholung, einer zukunftslosen Lebendigkeit, die tatsächlich bis zur Parusie dauern wird, aber immer auf der Stelle tritt ( So Rosenstock, Briefe 681) . ....(168).Rosenzweig..:“ Nur für Juden und Christen besteht jene feste Orientierung der Welt in Raum und Zeit, besteht die wirkliche Welt und die wirkliche Geschichte, besteht Norden und Süden, Vergangenheit und Zukunft, die nicht Gottes sind ( das ist verdammt leicht gesagt - im Koran und übersetzt im west-östlichen Diwan), sondern Gottes geworden sind, werden sollen und deswegen auch sind“ (Briefe 717)..........

„Er (Christus) muß herrschen, bis Gott ihm alle Feinde unter die Füße legt. Der letzte Feind, der vernichtet wird, ist der Tod. Denn alles hat Er unter seine Füße getan. Wenn es aber heißt, alles sei ihm unterworfen, so ist offenkundig, dass der ausgenommen ist, der ihm alles unterworfen hat. Wenn aber alles ihm unterworfen sein wird, dann wird auch der Sohn selbst sich dem unterwerfen, der ihm alles unterworfen hat, damit Gott sei alles in allem“ (1. Kor. 15,25-28).

Gleichzeitig beginnt eine neue Entdeckung der Nachfolge Jesu. Heute gilt es in seiner Nachfolge die Vielfalt der menschlichen Überlieferungen wieder in das Leben zu rufen. Die toten Seelen der einheitlichen Welt müssen heute neu begeistert werden. Dieser Prozeß hat schon eingesetzt. In diesem Prozeß zählt allein das Zeugnis, die Glaubwürdigkeit durch die Einheit von Wort und Tat. Sie hören schon: die neue Nachfolge Jesu rückt als brennende Frage an jeden einzelnen von uns heran – trägst Du dazu bei, einen der Tode heute zu überwinden. Beispiele: Tierschutz, Greenpeace, Aktion Sühnezeichen...etc.  Diese Frage stellt sich den Christen ebenso, wie auch den Muslimen, denn alle stehen heute vor dem Problem, dass selbst die geheiligsten ewig gültigen Traditionen versinken und die natürlichen Lebensgrundlagen zerstört werden (cf. Mk 13 : Himmel und Erde geraten ins Wanken...). Ohne diese aber sprechen und singen wir nicht, ohne das Wort wären wir keine Menschen mehr und ohne die Natur auch nicht.

Der besondere Schwerpunkt des Islam im Glauben an den einen Gott kann und muß jetzt von uns Christen gehört werden. In der Hingabe an Gott im Gebet, in der Rezitation der hl. Schrift und in der rechtgeleiteten Gemeinschaft, darin liegt ein wesentliches Wahrheitsmoment für das Leben der Schöpfung und aller menschlichen Gemeinschaften. Jesus selber hat uns gesagt (Mt 5,17.18), „er sei nicht gekommen auch nur ein Jota von der Weisung Gottes hinweg zu nehmen“. Weisung Gottes ist Rechtleitung des Lebens in Gebet, Gesang und Tun: Thora und Koran sind Weisungen Gottes. Juden und Muslime fragen uns Christen: Wie lebt ihr ohne Gebet, ohne Gesang, ohne Gemeinschaft?

Wenn alle Gebete aufhören, aller Gesang verstummt ist und keine Gemeinschaft unter uns Menschen, dann muß das nicht das Ende sein. Jesus Christus hat uns den Weg gewiesen, den Tod, auch diesen Tod zu überwinden, auf das immer wieder neu Menschen beten, singen und gemeinsam handeln in weltweiter Vielstimmigkeit zum Lobe und zur Ehre Gottes.

Keiner kann mehr die Wahrheit mit Worten, mit Begriffen beweisen. Sie wird sich erweisen in den Taten der Liebe und Gerechtigkeit, die unseren Worten folgen und in den Menschen und der Schöpfung, die wir von den Toten erwecken.

Beschämend nenne ich deshalb die immer noch skandalösen Behandlung der ausländischen Mitbürger und Asylanten. Sie sind Menschen zweiter und dritter Klasse in unserem Land, ohne Rechte – mit allen Pflichten. Haben wir Christi Gebote zu Mt 25 u. ö. vergessen? (Lev 19,33f).

Jede und jeder von uns steht heute für die Wahrheit als Zeuge mit ihrem oder seinem eigenen Namen und Leben ein. Das Merkmal der „neuen Menschen ist nicht die Unterscheidung von anderen, sondern die Solidarität mit ihnen“[^14]. In der Offenbarung des Johannes Kap. 2,17 lese ich eine Verheißung:

[^14]: Mitteilungen der Eugen Rosenstock-Huessy  Gesellschaft 11/1969, S. 6,  H. J. Schulz: „Er (E. R.-H..) habe nimmermüde darauf hingewiesen, dass unsere Welt über die Blässe offiziellen Kirchentums hinaus eine christuserfüllte Welt ist. Das „Jahrtausend des Samariters, dass er ankündige, soll die Epoche des christianus sine nomine im lutherischen Sinne sein, d.h. „des unkenntlichen, des verwechselbaren, des die religiöse Dualisierung überwindenden Menschen.“ „Sein Merkmal ist nicht die Unterscheidung von anderen, sondern die Solidarität mit ihnen.“ W. Ullmann zu ERH Sprache und Religion, weist darauf, hin, dass „Sprache und nicht Religion die primäre Ebene und das primäre Medium der Relation Gottes und des Menschen ist. Die Sprache ist das Indiz dafür, dass man den Menschen in letzter Instanz nur als den Angeredeten Gottes definieren kann. (in stimmstein 2, 174f.) - a. a. O 156: Rosenzweig Abrahamsnachkommenschaft (Hebr. 10,25; Röm 11,25; 1. Kor 15,27). Hans Ehrenberg: Der wahre Begriff von Gott – das Gebet / Bibelstellen: Acta 10,15 – Gott sieht die Person nicht an; Röm 1,19.20 – Gottes Wesen; 2,10-16 – Sinn des Gesetzes; 2,26.29.30. – Gott auch der Heiden; Ps 32,1.2; Matth 12 – Sünde wider den Hl. Geist; Röm 4,17f– Abraham – Vater vieler Völker cf. 1. Mose 17,5; Acta 15 – Apostelkonzil; Acta 17 – Athen; Röm 12 – Ein Leib – viele Glieder; 1. Kor. 15,25-28 – Ende der Völkermission.

Dem der überwindet, dem will ich vom verborgenen Manna geben und ich will ihm einen weißen Stimmstein geben und auf diesem Stein ist ein neuer Name geschrieben, welchen niemand kennt, außer dem, der ihn empfängt.

Ich danke Ihnen.

 
