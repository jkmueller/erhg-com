---
title: "Rosenstock-Huessy: Concordance (1962)"
category: online-text
published: 2022-08-21
org-publ: 1962
language: english
---

**CONCORDANCE,** in medieval thought, the method by which opposing or contradictory viewpoints were brought into accord. Bringing concord was also conceived as being a historical task, and concordance was regarded by some historians of the Middle Ages as a historical process, as well as a way of thinking. Concordance had to do with the resolution of paradoxes, whether in the realm of ideas or in the social order, usually in terms of the relative importance of conflicting ideas or actions.

The development of concordance as a method of thought was linked to the development in Europe, during the twelfth century, of the university, which came to be regarded as a place where opposing schools of thought were taught to the same student body at the same time in the same place. In this the university differed from the academy, where only one point of view could be taught. The universities were places of dispute, and the method of disputation was concordance. By the year 1000 the church was heir to a vast quantity of authoritative but frequently contradictory statements by the church fathers on every conceivable aspect of divine and human existence. Concordance developed first as an attempt to resolve the conflicting ideas contained in the early writings. The schoolmen assumed that while affirmation of a statement purporting to be truth could be total, negation could never be more than partial since all parties to a dispute agree on the credo (that which could not be doubted by reason of its overriding importance beyond logic) but they agreed to disagree on the unimportant or accidental things or the notions which could be rejected apart from the credo. Hence all agreed on the existence of God, but statements by human beings about supposed aspects of God were considered disputable.

**Philosophy and Theology.** Pierre Abélard's *Sic et Non* (Yes and No) was the first, and some have said the greatest, attempt to bring into concord conflicting assertions; in it he juxtaposed conflicting truths from the Fathers and then concorded them. At the University of Paris, the problem of universals was in dispute: the doctrine of realism dominated the local archbishop's school on the island, while the opposing doctrine of nominalism was prevalent within the international student body at the free schools on the left bank. Abélard earned the enmity of the realists by denying that all words are eternal, since only God is eternal; but he rejected nominalism by saying that an expression (*sermo*) may partake intimately of eternity by being the right and necessary word at the
time. His concordance, then, consisted of distinguishing between unimportant words (horse, house) which are arbitrary, and important words (God, Trinity) which are necessary. Virtually all of the scholastic philosophers adopted the method of Abélard's *Sic et
Non*, although some varied it a little. In each of his articles, St. Thomas Aquinas proposed a question, set forth objections to the affirmative position, replied to the objections, and came to a conclusion that may serve as the initial question (or proposition) in an-
other article. See ABÉLARD, PIERRE; CONCEPTUALISM; NOMINALISM; REALISM; SCHOLASTICISM.

**Law and Medicine.** At Bologna, the leading center of juridical studies, canon lawyers competed with civil (Roman) lawyers. One of the most important questions in dispute was that of marriage: which had primary jurisdiction, church or state? The state, which permitted cradle engagements and the marriage of cousins, said that it should have jurisdiction so as to insure continuity of property ownership, inheritance of titles, and the like. The canon lawyers denounced cradle engagements and marriage between cousins, asserted that marriage was a sacrament, and claimed full jurisdiction for the church. There were sound reasons and authorities in support
of both positions. The concordance, effectively ending the dispute, was to give the church authority over personal aspects: impediments abolishing cradle engagements and other abuses were established, as was the idea of *unioprolium* (a man's children by several marriages
became equal before the law). The state assumed control over civil aspects - inheritance of land, titles, and the like. Within canon law
itself, Franciscus Gratianus' *Concordia discordantium canorum* was virtually the beginning of systematic canon law. At Salerno, base hospital for the Crusaders, Arabic medicine competed with the classic medicine inherited from the Greeks and Romans.

**Modern Times.** After the middle of the ninetenth century, with the development of existentialism and its concern with paradox, there was growing interest in concordance both as a method and as a process in the development of thought and action (see EXISTENTIALISM).
This interest is exemplified in Eugen Rosenstock-Huessy's *Heilkraft und Wahrheit: Konkordanz der Politischen und der Kosmischen Zeit* (Healing Power and Truth: Concordance of Political and Cosmical Time, 1952).

Entry in the American People’s Encyclopedia (Grolier Inc., 1962)

“ANIMISM,”8: 201-204. {Unsigned.  Francis Squibb, co-author.} Reel 11, Item 545, Frame 6733.

[„Animism” as part of the PDF-Scan](http://www.erhfund.org/wp-content/uploads/545.pdf)

[zum Seitenbeginn](#top)
