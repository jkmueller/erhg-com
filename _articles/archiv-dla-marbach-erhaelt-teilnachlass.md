---
title: DLA Marbach erhält Teilnachlass
created: 2023-08
published: 2023-12-28
category: archiv
---
24.08.2023

Das Deutsche Literaturarchiv Marbach übernimmt einen Teilnachlass des Religionsphilosophen, Sprachforschers und Soziologen Eugen Rosenstock-Huessy (1888-1973), der zu den originellsten Köpfen des deutsch-jüdischen Religionsgesprächs in der ersten Hälfte des zwanzigsten Jahrhunderts gehörte. Ursprünglich Rechtshistoriker, Soziologe und Philosoph, interessierte sich Rosenstock-Huessy im Lauf seins Lebens immer stärker auch für das Thema Sprache, so z.B. in seiner zweibändigen Studie Die Sprache des Menschengeschlechts (1963/64). Rosenstock-Huessy war jüdischer Herkunft, konvertierte aber schon als Jugendlicher zum protestantischen Christentum. In den Jahren vor dem Ersten Weltkrieg war er eng mit dem jüdischen Philosophen Franz Rosenzweig (1886–1929) befreundet. Anteil am interreligiösen Gespräch der Freunde hatte auch Margrit Huessy – die Liebesbeziehung von Margrit Huessy und Rosenzweig ist in einem umfangreichen Briefkonvolut in Rosenstocks-Huessys Nachlass in den USA dokumentiert.

Seit 1923 lehrte Eugen Rosenstock-Huessy in Breslau Rechtsgeschichte. Direkt nach der Machtergreifung der Nationalsozialisten emigrierte er 1933 mit seiner Familie in die USA. Zurück blieb ein großer Holzkasten, ein sogenannter Offizierskoffer, mit Manuskripten, Briefen und anderen Materialien, der von der Haushälterin der Familie gerettet und viele Jahre verwahrt wurde. Dieser sogenannte Breslauer Koffer bildet den Kernbestand des Teilnachlasses, der dem DLA nun von der Eugen-Rosenstock-Huessy-Gesellschaft gestiftet wurde. Dieser Bestand wird ergänzt u.a. durch Fotos, Briefe und Bücher, eine Porträtplastik, geschaffen von Sabine Leibholz, der befreundeten Zwillingsschwester von Dietrich Bonhoeffer, sowie durch Tonaufnahmen unveröffentlichter Vorträge und Interviews.

Ende der zwanziger Jahre hatte Eugen Rosenstock-Huessy gemeinsam mit dem jungen Helmuth James von Moltke die Löwenberger Arbeitsgemeinschaft initiiert, die unterschiedliche soziale Schichten, Glaubensrichtungen und Überzeugungen miteinander in Verbindung bringen wollte, um die Probleme der sozial und wirtschaftlich schwierigen Nachkriegszeit in Deutschland zu bewältigen. Aus dieser Arbeitsgemeinschaft entstand im Nationalsozialismus die Widerstandsgruppe des Kreisauer Kreises, deren Mitglieder Ideen für die Zeit nach dem Ende der Diktatur entwickelten. Unter den von den Nationalsozialisten ermordeten Mitgliedern des Kreises war auch Helmuth James Graf von Moltke, dessen Witwe, Freya von Moltke, die letzte Lebensgefährtin von Eugen Rosenstock-Huessy war.

siehe: [DLA Marbach PM 44/2023](https://www.dla-marbach.de/presse/presse-details/news/pm-44-2023/)
