---
title: Ausstellung im Archiv der ERHG
created: 2013
category: archiv
---

Einladung zur
Ausstellungseröffnung

„Respondeo etsi mutabor”

Eugen Rosenstock-Huessy (1888-1973)\
im Gespräch mit Freunden

Thomas Dreessen | Grußwort\
Amt für Jugendarbeit der EKvW\
Stv. Vorsitzender der Eugen Rosenstock-Huessy Gesellschaft e.V.

Dr. Knut Martin Stünkel | Einleitungsreferat\
Ruhr-Universität Bochum

Ausstellungsort:\
Landeskirchliches Archiv\
der Ev. Kirche von Westfalen\
Bethelplatz 2 | 33617 Bielefeld\
0521-594 164\
[Archiv@LkA.EKvW.de](mailto:Archiv@LkA.EKvW.de)

Ausstellungsdauer und Öffnungszeiten\
1.10.-1.11.2013\
Montag-Donnerstag 9-16 Uhr, Freitag 9-12 Uhr und nach Vereinbarung

1\. Oktober 2013 I 18 Uhr

Weitere Informationen unter:\
[www.archiv-ekvw.de](https://www.archiv-ekvw.de)

Absender:\
Landeskirchliches Archiv\
Bethelplatz 2\
33617 Bielefeld

Ausstellungsdauer: 1. Oktober bis 1. November 2013\
Öffnungszeiten: Montag-Donnerstag 9-16 Uhr, Freitag 9-12 Uhr und nach Vereinbarung
