---
title: Jahrestagung 2019
created: 2019
category: jahrestagung
thema: „Was ist unsere Heimat nach dem Weltkrieg? Rosenstock-Huessys Zeitansage 1919”
---
![Haus am Berg]({{ 'assets/images/Haus_am_Turm_Essen.jpg' | relative_url }}){:.img-right.img-large}

### {{ page.thema }}

Textgrundlage: „Der Heimfall der Heimat”, Eugen Rosenstock-Huessy, Die Hochzeit des Krieges und der Revolution, 1920

Datum: 12.-14.4.2019

Ort: Haus am Turm, Essen

| Freitag, 12.4 | 18:00 | Eintreffen |
|               | 18:30 | Abendessen |
|               | 19:30 | Der Heimfall der Heimat (I-IV): Heimatlos 1919!Jeder 2019 (Thomas Dreessen) |
|               | 21:00 | Geselliges Beisammensein |
| Samstag, 13.4 |  8:30 | Frühstück |
|               |  9:30 | Der Heimfall der Heimat V: Die Prophetie des Lügenkaisers (Andreas Schreck) |
|               | 11:00 | Pause |
|               | 11:30 | Der Heimfall der Heimat VI: Kabelstück – Authentisch und die Bescheidenheit des Glaubens (Andreas Schreck) |
|               | 12:15 | Pause |
|               | 12:30 | Mittagessen |
|               | 14:30 | Mitgliederversammlung |
|               | 16:30 | Pause |
|               | 17:00 | Der Heimfall der Heimat (Schluß I): Neue ahnenlose Menschen: Heide, Jude, Christ (Eckart Wilkens) |
|               | 18:30 | Abendessen |
|               | 20:00 | Die unsichtbare Welt und wie wir sie erleben Gesang und Gespräch (Eckart Wilkens) |
|               | 21:30 | Geselliges Beisammensein |
| Sonntag, 14.4 |  8:00 | Morgenandacht |
|               |  8:30 | Frühstück |
|               | 10:00 | Der Heimfall der Heimat (Schluß II): Ein jeder stehe also in seinem Lager (Jürgen Müller) |
|               | 11:30 | Abschlußrunde |
|               | 12:30 | Mittagessen |
|               | 13:00 | Abreise |
