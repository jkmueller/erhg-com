---
title: "Karl-Ernst Nipkow über Rosenstock-Huessy"
category: aussage
published: 2022-11-21
name: Nipkow
---

„Rosenstock-Huessy hat dem „Cogito ergo sum“ des Descartes sein Motto „Respondeo etsi mutabor“ entgegengestellt, um damit den Weg vom „ich-einsamen Denken der neuzeitlichen Philosophie zur gelebten Sprachvernunft“ im Chaos des Weltkriegsjahrhunderts zu bahnen: „Ich antworte, wenn ich mich auch wandeln lassen muss“. In der Person des Descartes entschloss sich die Menschheit, gewiss des göttlichen Segens, das dunkle Chaos der Natur in Gegenstände verstandesmäßiger Beherrschung umzuformen. Aber: ‚Insofern das Menschengeschlecht heute auf Grund gemeinsamer Anstrengung entscheiden muss, wie die Wahrheit im Sozialleben darzustellen ist, hat die cartesianische Formel nichts auszusagen.‘“
>*Karl-Ernst Nipkow*
