---
title: Mitgliederbrief 2020-02
category: rundbrief
created: 2020-02
summary: |
  1. Einleitung                                                  - Jürgen Müller
  2. Tagungsort und Anmeldung                                    - Andreas Schreck
  3. Tagesordnung der Mitgliederversammlung                      - Jürgen Müller
  4. Programm der Jahrestagung                                   - Jürgen Müller
  5. Nachruf Andreas Möckel                                      - Andreas Schreck, Jürgen Müller, Thomas Dreessen, Eckart Wilkens
  6. Geleitwort Helmuth James von Moltke und der Kreisauer Kreis - Thomas Dreessen
  7. Adressenänderungen                                          - Thomas Dreessen
  8. Hinweis zum Postversand                                     - Andreas Schreck
  9. Jahresbeiträge 2020                                         - Andreas Schreck
zitat: |
  >Herr J. Vendryes hat ein schönes Buch geschrieben: Le Language, Introduction Linguistique a l' Histoire. Obwohl auf Französisch geschrieben, hat es ein Register. In diesem Register findet man die Wörter, die die Akte des Hörens, Zuhörens, Gehorchens, Verstehens bezeichnen, nicht, sogar das Wort „oreille”, das Ohr, fehlt. Das ist kein Zufall.
  >Unsre Philologie ist um den Vorgang des Redens herum gebaut, des Sprechens, des Schreibens. Den Vorgang des Hörens überläßt man den anderen Abteilungen: Gehorsam dem militärischen Training, Verstehen der Psychologie, Hören und Lernen der Akustik und der Erziehung. Alles Künste, die von der Sprache nur gelegentlich handeln. Zum Beispiel weiß man wohl, daß die Stimme rechter Art für den, der befehlen soll, die kostbarste Qualität ist. Das wird aber nicht als universales Problem der menschlichen Natur behandelt, sondern kommt nur bei der Soldatenausbildung vor.
  >Lassen Sie uns daher das System des Hörens mit dem des Sprechens vergleichen. Es ist nicht unwahrscheinlich, daß Vielfalt und Arten des Hörens uns überraschen mögen. Vielleicht finden wir auch, daß der Apparat, mit dem wir Menschen hören, sich nicht auf das Ohr beschränkt. Wäre solche Beobachtung nicht wertvoll für die Interpretation der Sprache? Ist es möglich, den Vorgang des Sprechens auf fünfzig Prozent des einheitlichen Vorgangs zu beschränken, eben auf die Operationen, die nur im Sprecher vor sich gehen? Das wäre, als beschränkten wir jeden Stoffwechsel im Leib auf eine der willkürlich gewählten Phasen. Der Vorgang im Ganzen erklärt doch aber allein die Intention des Anfangens.
  >Bei der Verdauung halten wir für selbstverständlich, daß für den Darmtrakt Ballaststoffe notwendig sind, daß nur eine geringe Menge der Nahrung im Leib zurückbleibt. Die Frage ist doch berechtigt, die wir uns stellen müssen, wie die Sprache zusammengesetzt sein muß, um den Hörer so zu erreichen, daß er in Bewegung gesetzt wird und anfängt, auch nur ein Fragment der Information und des Inhalts dessen sich anzueignen, was der Sprecher gesagt hat?
  >*Rosenstock-Huessy: The Listener's Tract, 1944 (dt. Eckart Wilkens)*
---

## Eugen Rosenstock-Huessy Gesellschaft e.V.

{{ page.zitat }}

**Vorstand/board/bestuur:**\
*Dr. Jürgen Müller (Vorsitzender);*\
*Thomas Dreessen; Andreas Schreck; Dr. Eckart Wilkens; Sven Bergmann*\
*Antwortadresse: Jürgen Müller: Vermeerstraat 17, 5691 ED Son, Niederlande*\
*Tel: 0(031) 499 32 40 59*

***Brief an die Mitglieder Februar 2020***

**Inhalt**

{{ page.summary }}

## 1. Einleitung

Rosenstock-Huessys Betonung der Sprache ist bekannt. Peter Sloterdijk nannte ihn anlässlich der Verleihung des Sigmund-Freud-Preis 2005 der Deutschen Akademie für Sprache und Dichtung den "bedeutendste[n] Sprachphilosoph[en] des 20. Jahrhunderts …, so viel man auch zugunsten Heideggers, Wittgensteins, Searles oder Derridas vorbringen mag". In "The Listener's Tract" widmet Rosenstock-Huessy sich einem übersehenen Aspekt der Sprache. Diesem wollen wir uns auf unserer Jahrestagung unter dem Titel "Der verborgene Kontinent des Hörens" zuwenden. Zwei Texte Rosenstock-Huessys werden dabei im Zentrum stehen. Der methodische Teil aus "Die Hörspur" wird am Beispiel des Textes "Die Sprache des Westens" verdeutlicht.

Wir möchten Sie zur Jahrestagung und zur Mitgliederversammlung 2020 herzlich einladen.
>*Jürgen Müller*

## 2. Tagungsort und Anmeldung

Die Jahrestagung findet vom 3. bis 5. April 2020 statt im\
Haus am Turm, am Turm 7, 45239 Essen-Werden, Tel. 0201-404067.\
Beginn ist am 3. April 2020 um 18 Uhr mit dem Abendessen.

***Kosten***\
Die Kosten betragen € 120 bei Unterbringung im Doppelzimmer, € 145 bei Unterbringung im Einzelzimmer. Die Anzahl der Einzelzimmer mit Bad/WC ist begrenzt.

Interessenten, die die Tagungskosten nicht in voller Höhe aufbringen können, mögen sich bitte an Andreas Schreck oder ein Vorstandsmitglied wenden.

***Anmeldung***
bitte an Andreas Schreck, Tel. 0551-28047871; schreckschrauber@web.de
>*Andreas Schreck*

## 3. Einladung zur ordentlichen Mitgliederversammlung

**am 4. April 2020, 14:30 Uhr\
Haus am Turm, Am Turm 7, 45239 Essen**

- TOP 1: Begrüßung der Mitglieder, Feststellung der Beschlussfähigkeit und
   Festlegung der Protokollführung
- TOP 2: Genehmigung des Protokolls der Mitgliederversammlung
   vom 13.4.2019 in Essen
- TOP 3: Anträge zur Änderung/Erweiterung der Tagesordnung
- TOP 4: Bericht und Ausblick des 1. Vorsitzenden mit Aussprache
- TOP 5: Kassenbericht für das Geschäftsjahr 2019
- TOP 6: Berichte der Kassenprüfer
- TOP 7: Entlastung des Vorstands für das Geschäftsjahr 2019
- TOP 8: Berichte von Respondeo, vom Eugen Rosenstock-Huessy Fund
 und der Society und von Projekten einzelner Mitglieder
- TOP 9: Verschiedenes

## 4. Programm der Jahrestagung

Der verborgene Kontinent des Hörens

| Freitag, 3.4 | 18:00 | Eintreffen |
|              | 18:30 | Abendessen |
|              | 19:30 | Einleitung (Jürgen Müller) |
|              |       | Lesung: Die Sprache des Westens (Eckart Wilkens) |
|              | 21:00 | Geselliges Beisammensein |
| Samstag, 4.4 |  8:30 | Frühstück |
|              |  9:30 | Rundgespräch: Hören der Eindrücke (Sven Bergmann) |
|              | 11:00 | Pause |
|              | 11:30 | Narrativ: Erzählen eigener Eindrücke (Thomas Dreessen, Jürgen Müller) |
|              | 12:15 | Pause |
|              | 12:30 | Mittagessen |
|              | 14:30 | Mitgliederversammlung |
|              | 16:30 | Pause |
|              | 17:00 | Nach dem Lesen/Hören (Thomas Dreessen) |
|              | 18:30 | Abendessen |
|              | 20:00 | Sven Bergmann: Meine Hörspur mit Rosenstock-Huessy |
|              | 21:30 | Geselliges Beisammensein |
| Sonntag, 5.4 |  8:00 | Morgenandacht: audi ut vivamus (Otto Kroesen) |
|              |  8:30 | Frühstück |
|              | 10:00 | Die acht Schritte der Hörspur und ihre Herausforderungen (Eckart Wilkens) |
|              | 11:30 | Abschlußrunde |
|              | 12:30 | Mittagessen |
|              | 13:00 | Abreise |

## 5. Nachruf Andreas Möckel

Andreas Möckel war von 1986 bis 1992 für drei Amtszeiten Erster Vorsitzender der Eugen Rosenstock-Huessy Gesellschaft. In diese Zeit fiel der 100. Geburtstag, der mit einem Pfingstwochenende im Rosenstock-Huessy-Huis in Haarlem und der großen internationalen Konferenz an Dartmouth College in Hanover N.H., USA, gefeiert wurde. Untrennbar mit Andreas Möckel sind die beiden großen, jeweils einwöchigen Tagungen in Gulpen 1987 und 1989 verbunden. Andreas arbeitete mit dem Vizevorsitzenden Frans Meijer und den weiteren Vorstandsmitgliedern gut zusammen, heute würde man es wohl „kollegialen Führungsstil” nennen. Sein Anliegen, Jüngere in die Verantwortung zu rufen, kam mir persönlich zu Gute. Und was ganz vorsichtig 1987 bereits angedeutet, 1988 schon konkreter besprochen worden war, bekam im Frühjahr 1989 plötzlich Gestalt: Den Vorstand erreichte eine Einladung aus Breslau in Polen, an der ersten internationalen, noch konspirativen Kreisau-Konferenz teilzunehmen. Andreas griff dies sofort auf, einige Vorstandsmitglieder fuhren im Juni 1989 nach Schlesien. Die Rosenstock-Huessy Gesellschaft wurde einer der „Aufbau-Steine” Kreisaus. Andreas Möckel erarbeitete wesentlich die pädagogische Konzeption der geplanten Bildungsstätte – auch heute noch von Bedeutung.

Gemeinsam mit seiner Frau Anneliese und unter Teilnahme seiner Tochter Änno leitete Andreas 1991 ein dreiwöchiges workcamp in Kreisau, dabei auf die Arbeitslager-Idee der 20er Jahre zurückgreifend. Irgendwelche Komfortansprüche stellte der damals über 60jährige nicht.

1987 erschien der erste Stimmstein, damals mit dem Anspruch, in jährlichem Rhythmus Fragen der Zeit mit „Antworten” Rosenstock-Huessys und seiner Freunde zu verbinden.

Nach Ablauf seiner 3. Amtszeit als Vorsitzender meinte Andreas, es sei Zeit, die Leitung in jüngere Hände zu geben. Seine Idee einer „Doppelspitze” von Michael Gormann-Thelen und Lise van der Molen ist nicht geglückt, wie leider auch das Projekt einer Neuausgabe der „Soziologie” in Verantwortung der Rosenstock-Huessy Gesellschaft zu tiefen Zerwürfnissen geführt hat. Bis zuletzt hat Andreas wie ein „pontifex”, ein Brückenbauer, daran festgehalten, dass inhaltliche Differenzen keine unauffüllbaren menschlichen Gräben aufreißen sollten.

Noch ein persönliches Wort: Ich habe mich im Hause von Luxburgstraße 9 in Würzburg seit 1980 immer willkommen und „wertgeschätzt” gefühlt. Die Würde des evangelischen Pfarrhauses wehte dort. Auch wenn es nur ein Pfarrkinder-Haus gewesen ist.
>*Andreas Schreck*

Ich habe Andreas Möckel vor 13 Jahren als Ermutiger und Vermittler kennen gelernt. Vor allem zwei Situationen bleiben mir in Erinnerung. 2007 auf der Jahrestagung nach einer schwierigen Situation im damaligen Vorstand ermutigte er mich als noch-nicht Mitglied mich als Kandidat zur Wahl zu stellen. Dies war eine Entscheidung, die mein Leben verändert hat. Über die Vorstandsarbeit lernte ich Rosenstock-Huessy zu verstehen, er hat mich seither nicht mehr los gelassen. Auch in der Soziologie-Krise versuchte Andreas Möckel zu vermitteln. Da kam ich allerdings im persönlichen Gespräch zu der Überzeugung, daß Vermittlung nicht weiterhelfen wird und daß der Vorstand daraus Konsequenzen zu ziehen habe.
>*Jürgen Müller*

Meine persönliche Begegnung mit Andreas beginnt eigentlich mit seiner Kritik eines fehlerhaften Zitates meinerseits. In einem Schreiben hatte ich hinter respondeo etsi mutabor in Klammern Friedrich Nietzsche gesetzt. Ich errötete, denn mir war bewusst, dass dieses Wort vor ihm von Rosenstock-Huessy war. So kamen wir ins Gespräch und es kreiste immer um den so notwendigen Dienst auf dem Planeten.

Aufmerksam und begeistert und begeisternd begleitete er seit 2012 meine Versuche, die dem respondeo etsi mutabor entsprungene Methode Rosenstock-Huessys in Projekten gemeinsamen Dienstes auf dem Planeten mit der Evangelischen Jugendarbeit heute zu realisieren. Dazu schickte er mir eine Reihe seiner Versuche, in die Pädagogik weiterzusagen, was er von Rosenstock-Huessy gelernt hatte. Sie begleiten mich seither. Zwei dieser Texte nenne ich hier: Lebendige und tote Zeiten und Eugen Rosenstock-Huessy – Erziehung zum Frieden.(Interessierten sende ich sie gerne)

Wie wichtig ist doch in unserer Zeit der „Vereindeutigung” (Thomas Bauer) und des Wiedererstarkens des Nationalismus die Erkenntnis: „Der Friede ist die hohe Kunst der Unterredung, in der die Unterredner ein Drittes, das mehr ist als sie selbst, einlassen und über sich gelten lassen.” Andreas fand bei Rosenstock-Huessy Hoffnung und intellektuelle Orientierung, seine persönlichen Erfahrungen der Verführbarkeit durch pervertierte Lehre in der NS-Zeit zu verstehen und zu überwinden. Ich habe ihn als Lehrer und Gesprächspartner erfahren, der bewusst als Pädagoge und Andragoge dafür lebte und arbeitete, die Sprachlosigkeit zwischen jung und alt, und zwischen verschiedenen Gruppen, Völkern und Religionen in unserer Zeit zu überwinden. Wie schwer das in unseren Bildungsinstitutionen ist, das habe ich selber erfahren. Der Begriff Selbstbildung ist doch hoch anspruchsvoll. Er setzt Mündigkeit voraus. Unsere Gesellschaft in ihrem irrsinnigen Veränderungstempo verhindert aber gerade diese vielen, denn sie zerstört permanent gemeinsame Zeit (Gesetz der Technischen Erneuerung). Hier setzt der Pädagoge Möckel an zu beschreiben, was pädagogisch not-wendend ist. Ich wünsche seinen Werken Aufmerksamkeit um unserer gemeinsamen Zukunft willen auf unserem Planeten. Noch einmal Danke! Andreas!
>*Thomas Dreessen*

Das Pfingstfest 1975 im Rosenstock-Huessy Huis in Haarlem bestimmte die Freundschaft zu Andreas und Anneliese Möckel lebenslang – ein Beispiel, wie die „Hörspur” sich in ihrer Majestät erst fühlbar macht, wenn die Zeitspannen sich darstellen und als erwiesen gelten können. Der Charme, die Strenge, die Ermutigung, das Ausrufen der Epoche nach den Weltkriegen – diese Säulen bildeten dann auch die Grundlage für die „Stimmsteine”, die Andreas Möckel, Andrej Sikojev und ich nach dem Treffen in Gulpen sozusagen unter freiem, blauem Himmel begonnen haben und die liebevoll bittend neben die Jahrgänge der „Kreatur” getreten sind, aller fünf Inspiratoren eingedenk: Eugen Rosenstock-Huessy, Franz Rosenzweig, Joseph Wittig, Victor von Weizsäcker, Martin Buber. Das Vorläufige, das unter uns allen die Greuel und Scheuel der Schoah hinterlassen haben, blieb bei Andreas Möckel stets anwesend, auch die damit gegebene Mischung aus blitzartiger Darstellung und Verschweigen zu tief gelangter Erfahrungen. Die Mündlichkeit als Medium wirklichen Weitersagens ist auch bei ihm Teil des Incognito-Lebens geworden, das jeden umfängt und schützt, der sich auf die dritte Form der abendländischen Hochschule „Respondeo etsi mutabor” eingelassen hat: ich antworte und gehe damit immer das Risiko des Todes und der Auferstehung ein.
>*Eckart Wilkens*

## 6. Geleitwort Helmuth James von Moltke und der Kreisauer Kreis

Vor kurzem noch mit dem Friedensnobelpreis geadelt, steckt die Europäische Union, ja die herrschende Weltordnung, jetzt in einer tiefen Krise. Warum gelingt es nicht das Erstarken des Nationalismus zu überwinden und miteinander glaubwürdige wirksame Antworten zu geben auf die ökologischen und sozio-kulturellen Zerstörungen, die heute das Leben von Millionen bedrohen.

Der französische Historiker Benjamin Stora sagte 2015 nach den Anschlägen von Paris: „Wir werden den Terror nur überwinden, wenn wir unsere Geschichte neu erzählen – mit den anderen!”

In ihrem 2016 in den Niederlanden erschienenen Buch „Als Hitler valt – Helmuth James von Moltke en de Kreisauer Kreis” erzählt Marlouk Alders dem niederländischen Volk die Geschichte des Kreisauer Kreises und wie diese Entdeckung ihr Leben veränderte: „Ich möchte erzählen, was der Kreisauer Kreis war und wer dazu gehörte. Und vor allem auch, warum sie ihr Leben aufs Spiel gesetzt haben, um gegen Hitler und seine Nazi-Regierung an zu gehen. Ich beginne jedoch mit der wichtigsten Frage, was der Kreisauer Kreis uns heute noch zu sagen hat. Die Geschichte richtet sich an jeden, der – ebenso wie früher – wenig über den deutschen Widerstand weiß und gerne mehr darüber wissen will.”

Im Kreisauer Kreis, so entdeckte Marlouk Alders, haben Menschen aus den verschiedensten Weltanschauungen stammend, zusammengearbeitet. Durch solche Zusammenarbeit legst du Samen einer neuen Sprache des Vertrauens, das beweist die Autorin mit ihrer spannenden Erzählung vom Kreisauer Kreis. Im Zusammenarbeiten für die Überwindung gemeinsamer Not findet sie in der Erinnerung der Kreisauer den Weg zu einer neuen Zukunft für uns verheissen.

Karol Jonca, der Initiator der polnischen Forschung zum Kreisauer Kreis hat die Kreisauer seinem Volk im selben Geiste vorgestellt: „Denken mit Moltke – das bedeutet nachdenken über die zukünftige Ordnung Europas.”

Günter Brakelmann freut sich über die Entdeckung der Kreisauer durch die Niederländerin Marlouk Alders: „Die Ausarbeitung ist ein hoch sympathisches Dokument einer persönlichen Aufarbeitung der eigenen Geschichte. Ansonsten ist noch einmal zu sagen, dass der vorliegende Text ein guter Einstieg für Leute ist, die sich mit Kreisau beschäftigen wollen.”

So hat sie angefangen mit der Aufarbeitung der eigenen Geschichte. Jedoch Marlouk fragt weiter nach der Bedeutung der Kreisauer für unsere Zukunft. Deshalb erzählt sie uns von ihnen als unserer gemeinsamen Geschichte. ‚Nur Namennicht Zahlen und Begriffe!vermögen zu orientieren, und wenn es nicht Heilsnamen sind, wie die der Märtyrer unserer Völker, auf die wir uns berufen können, weil wir ihnen glauben können, dann sind es gegen unseren Willen die Unheilsnamen, denen wir aus Furcht nicht widersprochen haben."

Ich hoffe, dass Frau Alders viele motiviert in das neue Kreisau zu reisen, selber den Kreisauern zuzuhören, selber zu forschen nach den Namen derjenigen Menschen aus allen Völkern, die wir um unserer Zukunft willen weitersagen müssen.

Danke Marlouk Alders!
[zum Buch]({{ 'assets/downloads/Alders_Marlouk_Pioniere_unserer_gemeinsamen_Zukunft_.pdf' | relative_url }})
>*Thomas Dreessen*

## 7. Adressenänderungen

Bitte senden sie eine eventuelle Adressenänderung schriftlich oder per E-mail an **Thomas Dreessen** (s. u.), er führt die Adressenliste. Alle Mitglieder und Korrespondenten, die diesen Brief mit gewöhnlicher Post bekommen, möchten wir bitten, uns soweit vorhanden ihre Email-Adresse mitzuteilen.
>*Thomas Dreessen*

## 8. Hinweis zum Postversand

Der Rundbrief der Eugen Rosenstock-Huessy Gesellschaft wird als Postsendung nur an Mitglieder verschickt. Nicht-Mitglieder erhalten den Rundbrief gegen Erstattung der Druck- und Versandkosten in Höhe von € 20 p.a. Den Postversand betreut Andreas Schreck. Der Versand per e-Mail bleibt unberührt.
>*Andreas Schreck*

## 9. Jahresbeiträge 2020

Die Mitglieder, die nicht am Lastschriftverfahren teilnehmen, werden gebeten, den **Jahresbeitrag in Höhe von 40 Euro** (Einzelpersonen) auf das Konto Nr. 6430029 bei Sparkasse Bielefeld (BLZ 48050161) zu überweisen, soweit dies noch nicht geschehen ist.

Nach Einführung des einheitlichen Zahlungssystem SEPA mit der Internationalen Kontonummer (IBAN): DE43 4805 0161 0006 4300 29 SWIFT-BIC: SPBIDE3BXXX ist diese „IBAN”-Nummer auch für Mitglieder in den Niederlanden und im ganzen „SEPA-Land”, in das sich sogar die Schweiz hat einbetten lassen, ohne Zusatzkosten nutzbar.
>*Andreas Schreck*


[zum Seitenbeginn](#top)
