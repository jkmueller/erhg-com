---
title: Mitgliederbrief 2010-01
category: rundbrief
created: 2010-01
summary:
zitat: |
  >"Je tiefer\
  >ein Wort gefaßt wird,\
  >desto bestimmter wird der Zeitpunkt,\
  >an den es gehört."\
  >*Eugen Rosenstock-Huessy, Glückhafte Schuld, in: Das Geheimnis der Universität S. 148*
---

## Eugen Rosenstock-Huessy Gesellschaft e.V.

{{ page.zitat }}

**Vorstand/board/bestuur:**\
*Dr. Eckart Wilkens (Vorsitzender);*\
*Wilmy Verhage; Andreas Schreck; Dr. Jürgen Müller; Thomas Dreessen*\
*Antwortadresse: Dr. Eckart Wilkens: Everhardstraße 77, 50823 Köln, Deutschland*\
*Tel: 0(049) 221 511265*

***Brief an die Mitglieder Januar 2010***

**Inhalt**

{{ page.summary }}


[zum Seitenbeginn](#top)
