---
title: Die Veröffentlichungen der Eugen Rosenstock-Huessy Gesellschaft
created: 2008
category: veroeffentlichung
---

Die Eugen Rosenstock-Huessy Gesellschaft hat in den verschiedenen Phasen ihres Wirkens Mitteilungsblätter herausgegeben, die alle im Archiv der Gesellschaft in Bethel bei Bielefeld eingesehen werden können.

Man kann seit der Gründung der Gesellschaft im Jahr 1963 fünf Abschnitte der Veröffentlichungen unterscheiden.

I. **«Mitteilungen der Eugen Rosenstock-Huessy Gesellschaft»**\
Sie erschienen von 1963-1979 als Privatdrucke von Dr. Georg Müller, dem ersten Präsidenten ERH-Gesellschaft, in 28 Folgen. Nach seinem Tod im Jahr 1978 gab sein Nachfolger Prof. Dr. Dietmar Kamper (Berlin) die letzte Folge heraus. Sie haben einen Umfang von etwa einem Druckbogen, also 16 Seiten.

II. **«Mitteilungsblatt der Eugen Rosenstock-Huessy Gesellschaft»**\
Von 1978-1985 erschienen jahrgangsweise Mitteilungen als Privatdruck unter neuem Namen und ohne Numerierung, hrsg. von Dietmar Kamper und Dr. Rudolf Hermeier.

III. **«stimmstein» mit dem Untertitel «Jahrbuch der Eugen Rosenstock-Huessy Gesellschaft»**\
Von 1987-2000 erschienen fünf Ausgaben des «stimmstein», dazu zwei Ausgaben «beiheft stimmstein», unter wechselnden Redaktionen und in zwei verschiedenen Verlagen.

IV. **«Mitteilungsblätter der Eugen Rosenstock-Huessy Gesellschaft»**\
Sie erschienen von 1993-1999 als Privatdruck zusätzlich zum «stimmstein». Herausgeber dieser insgesamt sechs Hefte Michael Gormann-Thelen.

V. **«Mitteilungsblätter der Eugen Rosenstock-Huessy Gesellschaft» mit dem Untertitel «stimmstein»**\
Sie erscheinen seit 2001 im Verlag Argo Books und setzen die 1987 begonnene Numerierung fort.

Prof. Dr. Andreas Möckel, Würzburg, März 2008
