---
title: Mitgliederbrief 2021-02
category: rundbrief
created: 2021-02
summary: |
  1. Einleitung                                                                 - Jürgen Müller
  2. Ohne gemeinsame Erinnerung keine Zukunft – Impfung gegen den bösen Geist!  - Thomas Dreessen
  3. Warum St. Georgs-Reden?                                                    - Sven Bergmann
  4. Die graue Eminenz der Familie                                              - Sven Bergmann
  5. Tom Holland: Herrschaft                                                    - Jürgen Müller
  6. Manfred A. Schmid: Grammatik statt Ontologie                               - Jürgen Müller
  7. Jahrestagung und Mitgliederversammlung 2021                                - Jürgen Müller
  8. Adressenänderungen                                                         - Thomas Dreessen
  9. Hinweis zum Postversand                                                    - Andreas Schreck
  10. Mitgliederbeitrag 2020 und 2021                                           - Andreas Schreck
zitat: |
  > Wir brauchen da nur zusammenfassen, was wir schon ermittelt haben. Nur deshalb aber fassen wir hier die Stimmträger zusammen, damit dem Erbinventar Leben eingehaucht werden kann. Das Inventar besteht nunmehr nicht aus Wörterbüchern, Sprachlehren, Inschriften, Literaturen, die du, arme Schülerin Seele, auf Schulbänken lernen sollst. Das Inventar besteht aus Rufern, Sprechern; statt aus Erbstücken stehen Vorgänger auf und hießen dich, ihnen in der Rede nachzutun.
  >
  > Dieser Sprecher sind nicht zu viele. Wir wollen sie aufzählen:
  > * Die Toten sprechen zu den Lebenden.
  > * Die Himmel reden zur Erde.
  > * Die Zukunft ruft in die Vergangenheit.
  > * Die Dichter singen ihren Freunden.
  >
  > Medizinmann, Priester, Prophet und Sänger haben uns sprechen gelehrt, uns als Überlebende, uns als Erdensöhne, uns als Laien, uns als Freunde. Stämme, Tempelkulte, Israel und Hellas, weil sie Vergangenheit, Gegenwart, Zukunft und Freundschaften zu bleibenden Stimmen stiften, deshalb sind sie ewig unsere Sprachbürgen. Das gilt für das weite Erdenrund von Nagasaki bis nach Island, von Moskau bis Rio de Janeiro. Wer den Toten nie gelauscht hat, kann in der Arbeitsteilung der Erde keinen Platz finden. Wer das „Höre Israel” nicht hört, verfehlt seine Lebensgeschichte. Und wem Homer nicht singt, dem fehlen die Freunde. Heim, Wirtschaft, Freiheit, Freundschaft gibt es nur dank Sprache.
  >
  > *Eugen Rosenstock-Huessy, Die Sprache des Menschengeschlechts I, p.345-346*
---

## Eugen Rosenstock-Huessy Gesellschaft e.V.

{{ page.zitat }}

**Vorstand/board/bestuur:** *Dr. Jürgen Müller (Vorsitzender);\
Thomas Dreessen; Andreas Schreck; Sven Bergmann\
Antwortadresse: Jürgen Müller, Vermeerstraat 17, 5691 ED Son, Niederlande,\
Tel: 0(031) 499 32 40 59*

## Brief an die Mitglieder Februar 2021
Inhalt

{{ page.summary }}

### 1. Einleitung

Liebe Mitglieder und Freunde,
nach Rosenstock-Huessy sind wir Erben der Sprache des Menschengeschlechts. Um selber sprechen zu können ist selektives Hören notwendig. Welchen Stimmen schenken wir unser Ohr, auf wen hören wir. Thomas Dreessen erinnert an Zeugen, die Opfer des Nationalsozialismus wurden. Sven Bergmann berichtet aus seinen Studien über den Protestantismus Rosenstock-Huessys und seine erwachsenbildnerischen Aktivitäten im ersten Weltkrieg. Ich stelle zwei Bücher vor, die für die Didaktik von Rosenstock-Huessy wichtig sind.
Die Jahrestagung und die Mitgliederversammlung müssen wir leider in den Herbst verschieben (siehe unten).
Für diese schwierige Zeit wünsche ich Ihnen, daß sie auch feststellen, daß Rosenstock-Huessys Texte bleibende Relevanz haben.
> *Jürgen Müller*

### 2. Ohne gemeinsame Erinnerung keine Zukunft – Impfung gegen den bösen Geist!

Am 27.Januar 2021 sagte und fragte der Historiker Brechtken: „Wir hoffen etwas für die Gegenwart zu lernen: Wie wandelt sich eine Gesellschaft, warum verwandeln sich Menschen, sodass es mit der Zeit möglich wurde, dass Lager wie Auschwitz entstehen?” Seine These ist: „Wenn wir von diesen Fragen ausgehen, können wir für unsere Gegenwart lernen, dass sich so etwas nicht wiederholt.”

Ich danke ihm für die offene Frage und möchte eine Antwort versuchen, denn ohne Erinnerung gibt es keine Zukunft – lehrt uns die ganze Bibel.

Der 27.Januar hat 2 Namen. Er heisst Holocaust Gedenktag und Gedenken an die Opfer des Nationalsozialismus. Brechtken sagt zu Recht, dass der Name Holocaust Gedenktag repräsentativ für Verbrechen der Nationalsozialisten steht. Leider ist dieses nötige Nein! zum Holocaust die einzige offiziell gemeinsame Erzählung Deutschlands geworden. Ein Nein! zu den im Namen Deutsch begangenen Verbrechen aber reicht nicht aus, die völkisch-nationalistischen Erzählungen in der Bevölkerung zu überwinden. Wenn wir gelernt haben Nein! zu sagen zu Hitler und seinen Genossen, dann wird das nur dann nachhaltig, wenn wir eine andere Erzählung von glaubwürdigen Menschen aus dieser finsteren Zeit erzählen.

Der zweite Name – Gedenken an die Opfer des Nationalsozialismus ruft uns auf auch diejenigen zu erinnern, die sich geweigert haben mitzumachen, die widerstanden haben, die für eine gemeinsame Zukunft von deutschen und nicht deutschen in dieser Zeit gelebt haben und ermordet wurden. Davon hat es nicht wenige gegeben: Deutsche und Nichtdeutsche verschiedenster Konfession und Weltanschauung, Arbeiter, Adelige, Bürgerliche, die mit ihrem Leben bezahlt haben für ihre Kritik und ihren Widerstand. Wenige sind bekannt in den Erzählungen, in den öffentlichen Erinnerungen der Namen.

In seiner bedeutenden Gedenkrede zum 20.Juli 1944 nach seiner Wiederwahl 1954 zum Bundespräsidenten hat Theodor Heuss an diese Menschen erinnert und geschlossen mit den Worten: „Die Scham, in die Hitler uns Deutsche gezwungen hatte, wurde durch ihr Blut vom besudelten deutschen Namen wieder weggewischt. Das Vermächtnis ist noch in Wirksamkeit, die Verpflichtung noch nicht eingelöst.”[^1] Übrigens hat Theodor Heuss es strikt abgelehnt nach Bayreuth zu pilgern, wie es dann Sitte geworden ist unter den Eliten. Er hat gewußt, dass der Nibelungenmythos eine wesentliche Wurzel des deutschen völkischen Nationalismus ist.[^2]

Als ich 2019 im Frühjahr nach 25 Jahren die Gedenkstätte Plötzensee besuchte, erschrak ich. Es war wie damals irgendwie versteckt im Hinterhof. Die neue offizielle Homepage der Gedenkstätte Plötzensee nennt ein paar Namen und Widerstandsgruppen. Die Kreisauer fehlen! Die offizielle und öffentliche Erinnerung kennt Stauffenberg – und das wars dann schon für die meisten. Dann bleibt der Name Deutsch im Gefühl und im Narrativ nationalistisch. Die Verpflichtung, von der Theodor Heuss sprach, ist noch nicht eingelöst.

Rosenstock-Huessy erinnert uns: „Die innere Zukunft ist nicht bei Stauffenberg; die Zukunft ist eher bei dem Grafen Moltke. Denn der hat gesagt: „Bringt den Hitler nicht um, daran liegt gar nichts; der muß seinen Krieg verlieren. Aber nach dem Krieg muß es Menschen geben, die sich von diesem Unrecht lossagen können; und wir müssen uns heute schon lossagen, damit uns unsere Lossagerei morgen geglaubt wird.”[^3]

Einer der dort am 8.September 1943 Ermordeten, Julius Fucik aus Prag, ruft uns in seinem Abschiedsbrief auf:

*„Jeder, der treu für die Zukunft gelebt hat und für sie gefallen ist, ist eine in Stein gehauene Gestalt…
Um eines bitte ich: Ihr, die ihr diese Zeit überlebt, vergeßt nicht. Vergeßt die Guten nicht und nicht die Schlechten. Sammelt geduldig die Zeugnisse über die Gefallenen. Eines Tages wird das Heute Vergangenheit sein, wird man von der großen Zeit und den namenlosen Helden sprechen, die Geschichte gemacht haben. „Ich möchte, daß man weiß: daß es keinen namenslosen Helden gegeben hat, daß es Menschen waren, die ihren Namen, ihr Gesicht, ihre Sehnsucht und ihre Hoffnungen hatten, und daß deshalb der Schmerz auch des letzten unter ihnen nicht kleiner war als der Schmerz des ersten, dessen Name erhalten bleibt. Ich möchte, daß sie Euch alle immer nahe bleiben, wie Bekannte, wie Verwandte, wie ihr selbst.”[^4]*

Allein in Berlin-Plötzensee wurden von Januar bis April 1945 22 Menschen wegen ihres Widerstandes hingerichtet. Von 1933 bis 1945 waren es über 2800. Kennen wir sie? Ehren wir sie?

| Julius Leber                |  5.1.45 | Helmuth James von Moltke | 23.1.45 | Nikolaus Groß            | 23.1.45 |
| Ludwig Schwamb              | 23.1.45 | Erwin Planck             | 23.1.45 | Franz Sperr              | 23.1.45 |
| Busso Thomas                | 23.1.45 | Theodor Haubach          | 23.1.45 | Hermann Kaiser           | 23.1.45 |
| Eugen Bolz                  | 23.1.45 | Reinhold Frank           | 23.1.45 | Carl Friedrich Goerdeler |  2.2.45 |
| Johannes Popitz             |  2.2.45 | Alfred Delp              |  2.2.45 | Fritz Goerdeler          |  1.3.45 |
| Franz Leuninger             |  1.3.45 | Fritz Voigt              |  1.3.45 | Oswald Wiersich          |  1.3.45 |
| Ernst von Harnack           |  5.3.45 | Hasso von Boehmer        |  5.3.45 | Franz Kempner            |  5.3.45 |
| Ewald von Kleist-Schmentzin | 15.4.45 |                          |         |                          |         |

Wo gibt es Helmuth James von Moltke Kirchen? Oder Julius Leber Schulen? Oder Alfred Delp Plätze? Oder, oder…? Gibt es Lieder, Gedichte, Filme die ihre Geschichte erzählen?

In einer Diskussion in der evangelischen Männerarbeit zum Thema Drittes Reich vor 2 Jahren am 9. November sagten viele und andere nickten: „Da konnte man nichts machen!” Ein Dortmunder protestierte heftig und sagte: „Das ist nicht wahr! Ich weiß, dass man nicht mitmachen mußte, denn meine Großmutter hat den Gefangenen im örtlichen Lager Brot zugesteckt. Sie hat es getan, wohl wissend, dass darauf Todesstrafe stand.”

Ich glaube, es gibt noch viel mehr Unbekannte, auf die wir stolz sein können, weil sie damals widerstanden haben. Einlösung der Verpflichtung heisst, wir müssen unsere deutsche Geschichte neu erzählen, denn nur weil es Menschen in dieser Zeit gab, die den Namen Deutsche/Deutscher trugen, denen wir glauben können, in denen unsere gemeinsame Zukunft angefangen hat mit den anderen Menschen und Völkern in Europa und auf dem ganzen Planetenvon denen wir deshalb unseren Kindern erzählen müssen, gibt es eine Zukunft im Namen Deutsch.

Das ist wichtig auch für die sogenannte Integration. 2019 haben Bürgermeister in Baden-Württemberg öffentlich beklagt, dass in Deutschland geborene und gut gebildete junge Männer mit Migrationshintergrund sich nicht mit Deutschland identifizieren. Ich sage: Wenn wir nur Nein! oder Ja zu einer nationalistischen Geschichte erzählen können, dann gibt es nichts, mit dem sich ein junger Türke oder Afghane oder Syrer hier identifizieren kann. Er erfährt zum Beispiel oft bei der Job- oder Wohnungssuche, dass der Pass ihm keine Zugehörigkeit verschafft.

Wir werden uns auch erinnern müssen an die Zeugen unserer gemeinsamen Zukunft aus allen Völkern. „Wir müssen unsere Geschichte neu erzählen, um den Terrorismus zu überwinden – mit den anderen!” sagte 2015 Benjamin Stora, Historiker aus Paris.[^5]

In diesem Sinne hat Harold Berman, Rosenstock-Huessys Schüler, gesagt: *„Indem wir uns unserer eigenen Geschichte bewußt werden, der Geschichte des Westens, müssen wir mit der Erkenntnis beginnen, dass wir vor einem neuen Zeitalter stehen.”[^6]* Er kritisierte den herrschenden Glauben an idealistische Rechtssetzungen als Weg: *„Recht, das in der Art der Naturwissenschaft prozedieren will, ist Unrecht gegenüber Menschen und Völkern.,….”[^7]*

Henry Kissinger hat in seinem Alterswerk „World Order through Chaos or Insight” (2014) endlich gelernt, dass Ordnungen nicht Völkern auferlegt werden können, sondern in den Erfahrungen der Völker und ihrer Menschen wurzeln müssen. Die Erfahrung von Gerechtigkeit ist die Voraussetzung von Rechtsprechung und demokratischer Rechtsordnung.

Sind nicht die Ereignisse in den USA eine Bestätigung dieser Erkenntnis? Die Jahrzehnte währende Mißachtung durch die Eliten und der rationalistische Mißbrauch der öffentlichen Sprache in Werbung, Politik und Massenmedien erzeugten den Mob, dessen Maul Trump war. Schon 1954 hatte Rosenstock-Huessy deswegen einen Diktator in USA prophezeit. (Lecture: Universal History) Ich erinnere an seine Folgerung: *„Ehe nicht der Weltkrieg die Lehre der Völker erneuert hat, eher dürfen die Gelehrten nicht demobil machen!”[^8]*

Es ist eine große Aufgabe, dass wir diese Namen, und die noch unbekannten, in unseren Ländern, Städten und Dörfern, in unseren Familien, Schulen und Vereinen bekannt machen, öffentlich nennen, sie kennenlernen, sie gemeinsam verehren. Straßen, Plätze und Gebäude müssen ihre Namen tragen. Stolpersteine an sie erinnern, Lieder sie besingen. Ihre Todestage müssen in die Kalender unserer Orte aufgenommen werden. International müssen wir ihrer gedenken. Das wäre wie eine Impfung gegen den bösen Geist, den unser Bundespräsident 2019 in Yad Vaschem schmerzlich auch in Deutschland erkannte und die der Vizepräsident des Zentralrats der Juden, Abraham Lehrer aktuell fordert!!

Durch Corona und die Klimakrise haben viele Menschen gemerkt, dass niemand allein aus der Not herauskommt, ja, dass wir Menschen einander brauchen, damit wir Menschen werden und bleiben können. Erinnerung lehrt uns: Moltke und die Kreisauer haben zwei not-wendende Wege aufgezeigt, um den Individualismus und die Massen(Mob)gesellschaft zu überwinden. Die kleinen Gemeinschaften müssen anerkannt werden, in denen aufwachsend jede und jeder Verantwortung lernen kann. Der gemeinsame Dienst auf dem Planeten in jedes Menschen Leben kann neue politische Sprache stiften. Ermutigend finde ich, dass in der neuen Missionserklärung des Weltrats der Kirchen: Together towards Life Mission and evangelization in Changing landscapes wie auch im Common Word der Muslime und in des Papstes Franziskus Enzykliken diese Orientierungen in eigenen Worten zu finden sind.

> *Thomas Dreessen*

 [^1]: Dank und Bekenntnis Gedenkrede zum 20.Juli 1944, Berlin 1954.
 [^2]: Cf. Rosenstock-Huessy: Frankreich-Deutschland u.ö.
 [^3]: Eugen Rosenstock-Huessy: Unterwegs zur planetarischen Solidarität S.135-38.
 [^4]: Du hast mich heimgesucht bei Nacht, Abschiedsbriefe und Aufzeichnungen des Widerstandes 1933-1945, hrsg.v. H.Gollwitzer, K.Kuhn, R.Schneider 6.Auflage 1980, S.117f.
 [^5]: Dazu gibt es jetzt viel Literatur, z.B. Pankaj Mishra: Aus den Ruinen des Empires; Tamim Ansary: Die unbekannte Mitte der Welt und viele mehr.
 [^6]: Stimmstein 6, S.41.
 [^7]: AaO, S.100.
 [^8]: Stimmstein 6, S.9.

### 3. Warum St. Georgs-Reden?
Deutsche Eigenart im Spiegel Englands

Zwei Ereignisse haben im Ersten Weltkrieg zu erregten Diskussionen im deutschen Heer geführt: Der „Offiziershaß” und die „Judenzählung”. Auf den handgreiflichen Autoritätsverlust im Kriegsverlauf versuchte die Oberste Heeresleitung mit Schulungskursen zu reagieren. Als Leutnant organisierte Eugen Rosenstock dreitägige Lehrgänge für seine 103. Infanterie-Division. Im Rahmen dieser „Volkshochschule an der Front” entstanden die „St. Georgs-Reden”. Von den geplanten 49 Reden sind 16 überliefert. Dabei konnte er auf seine Überlegungen „Ein Landfriede” von 1912 aufbauen. Grundlegend war der Anspruch, keine „Stimmungsmache” im Sinne der offiziellen Kriegspropaganda zu betreiben. Die Herzen der Mitkämpfer sollten durch Würdigung der gemeinsamen Not des Tages und ein aufrichtiges Mitleiden gewonnen werden, gegen machtlüsterne Drachen wie gegen winselnde Würmer: „Schon 1915 entwarf ich einen Dialog, in dem die Veteranen des ersten Weltkrieges aus allen Völkern dadurch Frieden schlossen, daß sie sich zu des Feindes ständiger Gegenwart überwanden und mit ihm ins Gespräch kamen.” (Eugen Rosenstock-Huessy, Liturgisches Denken in zwei Kapiteln, in: ders.: Die Sprache des Menschengeschlechts. Eine leibhaftige Grammatik in vier Teilen, Bd.1, Heidelberg: Verlag Lambert Schneider 1963, S.485ff.).

Aber warum stellte er seine Reden seit 1915 in Kontext zum Heiligen Georg? Einen Hinweis gibt seine Freundschaft zu Werner Picht und dessen Interesse für England sowie für Stefan George (auch Eugens Schwager Hermann U. Kantorwicz war dem Frühwerk des Dichters zugetan). Der ein Jahr ältere Picht hatte 1912, nach beendetem juristischem Studium bei Alfred Weber, mit einer Arbeit über „Toynbee Hall und die englische Settlement-Bewegung: Ein Beitrag zur Geschichte der sozialen Bewegung in England” promoviert. Darin griff er einen Gedanken Max Webers auf: „Eine dankbare Aufgabe wäre der Vergleich mit dem christlichen Sozialismus Englands.” (Max Weber, Rez.: Was heißt Christlich-Sozial? Gesammelte Aufsätze von Friedrich Naumann <1894>). Genau dieser Aufgabe hatte sich Werner Picht in Anknüpfung an Pionierarbeiten der „Kathedersozialisten” Gerhart von Schulze-Gaevernitz und Lujo Brentano gestellt. Bei Picht ging es um die Niederlassungen Gebildeter in den Armenvierteln der englischen Großstädte und die Arbeiterbildungsbewegung. Dabei widmete er sich vor allem den „zwei großen Propheten” Thomas Carlyle und John Ruskin. Es war Ruskin, der einen „Code of the Guild of St. George” aufgestellt hatte: „We will try to take some small piece of English ground, beautiful, peaceful and fruitful. We will have no steam-engines upon it, and no railroads; we will have no untended or unthought-of creatures on it; none wretched, but the sick; none idle, but the dead.”

Im Jahr 1900 unternahm ein Hamburger Kandidat der Theologie, Walter Classen, eine Reise nach England, um die dortigen Settlements und speziell Toynbee Hall kennen zu lernen. Er hatte ein Gefühl für die Gefahr, welche in der Spaltung des Volks in zwei völlig getrennte Hälften, in die „zwei Nationen” nach dem Wort Disraelis, liegt, und er wollte sehen, ob man von den Engländern lernen könne, die Klassengegensätze zu überbrücken.
(Werner Picht, Toynbee Hall und die englische Settlement-Bewegung. Ein Beitrag zur Geschichte der sozialen Bewegung in England (= Archiv für Sozialwissenschaft und Sozialpolitik : Ergänzungsheft IX), Tübingen: J.C.B. Mohr (Paul Siebeck) 1913, S.121.)

Einige Gedanken aus seiner Dissertation stellte Werner Picht im „soziologischen Seminar” der Universität Heidelberg vor. Ein weiterer Referent zum Thema „Kunst und Volk” war Fritz Wichert, der neue Direktor der Mannheimer Kunsthalle (seit 1909). Der vergessene Vorgänger des Frankfurter Kulturdezernenten Hilmar Hoffmannhatte mit einer „Akademie für Jedermann” Aufmerksamkeit und Interesse gefunden. Das Doktoren- und Dozentenseminar galt intern als „Max-Weber-Seminar”, auch wenn dieser nicht beteiligt war, und ist nicht zu verwechseln mit dem „Baden-Badener Gesprächskreis” von 1910. In Heidelberg wurde vereinbart, daß Werner Picht seine Forschungen bei einer öffentlichen Veranstaltung in der benachbarten Industriemetropole Mannheim vorstellt. Im Anschluß an den Vortrag stieß der Grundgedanke eines harmonischen Austauschs zwischen den „zwei Nationen” auf Skepsis. Für den Heidelberger Mitstudenten, russischen Emigranten und Mannheimer Sozialdemokraten Eugen Leviné waren diese Hoffnungen trügerisch oder würden bald durch die bevorstehende Revolution überholt. Für ihn war klar: „Solange die Proletarierkinder mit den Bürgerskindern nicht zusammen spielen, solange glaube ich nicht an Eure Menschheitsversöhnung und an Eure brüderliche Gesinnung.” (Zitat aus „Dienst auf dem Planeten”)

Durch diese Position fühlten sich Werner Picht und Eugen Rosenstock herausgefordert. An eine Lösung der sozialen Frage durch gemeinsames Teetrinken glaubten auch sie nicht. Aber in der Überbrückung der Gegensätze durch gemeinschaftliche Arbeit sahen sie einen deutschen Weg. Diesen Gedanken brachten sie in ihr Engagement für die Erwachsenenbildung ein und später beim Aufbau gemeinsamer freiwilliger Arbeitslager für Arbeiter, Bauern und Studenten. Sicher hatten sie dabei Goethes Mahnung im Hinterkopf: „Wer sich Sankt-Georgen-Ritter nennet, denkt nicht gleich Sankt Georg zu sein”.

Die geplante „volksbildnerische Initiative” einer Zusammenarbeit zwischen Arbeitern und Bildungsbürgertum, zwischen Universität und Industriestadt, kam letztlich nicht zustande. Eugen Rosenstock vermutete eine Intervention Eugen Levinés bei der lokalen Mannheimer Sozialdemokratie. Ein kleines Vorspiel seiner Erfahrungen bei Gründung der Akademie für Arbeit in Frankfurt. Nur rührte hier der Widerstand eher aus Richtung des ungebrochenen akademischen Überlegenheitsgefühl des Lehrkörpers.

1917 fiel der Name St. Georg noch in einem anderen Kontext. Der Mitarbeiter Friedrich Naumanns, Wilhelm Heile, berichtete in der liberalen Zeitschrift „Die Hilfe” über den abgesetzten Reichskanzler Georg Michaelis:

Der Mann, den die Ritter vom großen Wort als den ersehnten starken Mann und neuen Sankt Georg begrüßten, der sich selbst mit einer krätzigen Sprache einführte, sich die Führung nicht aus der Hand nehmen lassen wollte: er ist nach hundert Tagen gegangen. Und der nun nach ihm als siebenter Kanzler an den Platz getreten ist, an dem einst Bismarck stand, ist derselbe Graf Hertling, dem der Kaiser schon im Juli das Amt des Kanzlers und des preußischen Ministerpräsidenten angeboten hat.
(Wilhelm Heile, Die neue Regierung, in: Die Hilfe, 23.Jg., Nr.45 (1917, 8. Nov.), S.676).

>*Sven Bergmann*

### 4. Die „graue Eminenz” der Familie

Weiteres zum Protestantismus von Eugen Rosenstock-Huessy

Aus der preußischen Provinz Posen stammten nicht nur die Familien Rosenstock, Ehrenberg und Kantorowicz, sondern auch Paul Waldstein und seine vier Geschwister (Amalie, Charlotte, Dorothea, Julius (Juda)). Der am 5. Mai 1836 geborene Waldstein hatte Philosophie studiert und seinen Doktortitel an der Universität Breslau erworben. Als liberaler 48er wandte er sich dem Journalismus zu und wurde Vorkämpfer der Pressefreiheit. 1868 setzte er sich auf dem 3. Journalistentag in Berlin für eine Statistik aller polizeilichen Beschlagnahmungen und Prozesse gegen Presseorgane im deutschsprachigen Raum ein. Im Deutschland der Konfliktzeit mußte er seine oppositionelle Haltung gegen Bismarck mit einer kurzen Haft bezahlen und orientierte sich auch deshalb Richtung Südosteuropa. Zugänglich ist sein Aufsatz „Posener Zustände” aus dem Jahr 1862, der eine Bestandsaufnahme der preußischen Provinz von 1815 bis zu den Wahlergebnissen d.J. unternimmt. Darin sprach er sich gegen eine Bevorzugung der Deutschen aus. Jeder Bürger solle einen Beitrag leisten, „mag er nun Pole oder Deutscher, Katholik, Protestant oder Jude heißen.” Und im übrigen sei die Provinz Posen „die preußische Türkei.” Die Polen seien „erfahrene Guerillakrieger” und in den ca. 60.-70.000 Juden sah er ein „zukunftsreiches Element”. Als Journalist und Burschenschaftler war Paul Waldstein befreundet mit dem noch unbekannten Korrespondenten der Wiener Freien Presse, Theodor Herzl. Pauls Schwester Charlotte Waldstein (1833-1913) war verheiratet mit dem Schulleiter der gemischtkonfessionellen Samsonschule in Wolfenbüttel: Moritz Rosenstock, dem Vater von Eugens Mutter Paula. Paula hatte drei Schwestern sowie den Bruder Siegfried und besuchte in der Stadt Lessings das Lehrerinnenseminar (Paulas Schwester Agnes, verheiratet mit Justizrat Dr. Leo Wurzmann, hat ihre Memoiren hinterlassen: Lebenserinnerungen der Tochter von Dr. Moritz Rosenstock, Direktor der Samsonschule von 1871- 1887. Familie Wurzmann lebte in Frankfurt. Eugen nächtigte gelegentlich in der Beethovenstr. 55. Sein Cousin Reinhold starb im Kriegsjahr 1917).

Aber warum „graue Eminenz”? Als journalistischer Pionier war Paul Waldstein nicht nur publizistisch, sondern auch ökonomisch erfolgreich. Dank des blühenden Annoncenwesens konnte die neuartige Massenpresse Vermögen aus dem Nichts wachsen lassen oder pulverisieren. Daß sich Waldstein eher auf der Sonnenseite bewegte, zeigen seine Tätigkeiten als Vorstand der ehemals „Fürstenberg‘sche Montanwerke” in Böhmen oder als Aufsichtsrat der Ungarischen Ostbahn, auch wenn der „Budapester Börsenkrach” ihm 1873 schmerzhafte Verluste abverlangte. Jedenfalls konnte der „Großonkel” Eugens Vater Moritz Theodor 5.000 Thaler Startkapital zur Gründung der privaten „Terobank” in Berlin leihen. Ein Thaler entsprach nach 1871 dem Wert von drei Mark. Paul Waldstein war praktizierender Protestant und seit 1882 Mitglied der „Gesellschaft für die Geschichte des Protestantismus in Österreich”. Auch für die Frauenbildung engagierte er sich. Außerdem verfügte er über viele Kontakte in höchste Kreise der k.u.k. Doppelmonarchie. „Er war einer der gründlichsten Kenner der Balkanverhältnisse und Balkanpolitiker und hatte auch wesentlichen Anteil an der Kandidatur des Prinzen Ferdinand von Sachsen-Koburg und Gotha für den bulgarischen Fürstenthron.” 1906 kurte Waldstein im gleichen Hotel Bauer in Bad Ischl wie Fritz Mauthner, einer der berühmtesten Schriftsteller seiner Zeit. Eugen Rosenstock sollte dessen Schriften zur Sprache und zum Atheismus nicht erst während des Krieges studieren. Als Eugen in seinem ersten Züricher Studiensemester der Blinddarm entfernt werden muß, besuchten ihn seine Mutter und Paul Waldstein aus dem nahen Wien und bestärkten ihn, sich taufen zu lassen. Paula Rosenstock hatte schon die Beschneidung ihres einzigen Sohnes verhindert. Kurz vor seinem Tod am 24.11.1914 berichtete der Junggeselle Eugen Rosenstock von einem Gespräch mit Theodor Herzl:

In Wien lebte mein Großonkel Dr. Paul Waldstein und nach Wiener Art hielt er, damals 58jährig, jeden Vormittag Hof im Café Himmelpfortgasse 25. Es war ein Januartag 1895, daß der damals 34jährige Dr. Herzl zu ihm trat: „Herr Dr. Waldstein, ich komme aus Paris. Was ich da erlebt habe, hat mir die Augen geöffnet. Ich habe als Burschenschaftler fest an die Assimilierung der Juden geglaubt.
(Eugen Rosenstock-Huessy, Die Sprache des Menschengeschlechts. Eine leibhaftige Grammatik in vier Teilen, 2 Bd., Heidelberg: Verlag Lambert Schneider 1964, S.209. Es handelte sich um das Café im „Etablissement Ronacher” im 1. Bezirk; vormals das Palais des Prinzen Eugen)

Eigentlich als „Feuilletonist” und Theaterkritiker für die „Neue Freie Presse” nach Paris beordert, hatte Herzl seit 1893 prominent über die Regierungskrisen im Zuge der Dreyfus-Affäre berichtet (Neue Freie Presse: 10535 v. 19. Dezember 1893; 10603 v. 7. März 1894; 106019 v. 17. März 1894; 10874 v. 30. November 1894; 10906 v. 3. Januar 1895; 10939 v. 9. Februar 1895).

Wenn der Preuße Eugen Rosenstock, in einem Brief an Carl Schmitt, den Satz: „Deutschland lebt aus der Mehrzahl seiner Staatlichkeit.” als den wichtigsten Satz des deutschen Staatsrechts bezeichnet, so wird Paul Waldstein eine seiner intimsten Quellen aus der „Österreichischen Staatlichkeit” gewesen sein.

> *Sven Bergmann*

### 5. Tom Holland: Herrschaft

Am 20 März erscheint die deutsche Übersetzung des im September 2019 erschienen Buches: Dominion: The Making of the Western Mind. Schon der Titel des Buches war für den englischen Verlag ein Politikum, was sich sehr schön an den Titeln der bereits erschienenen Übersetzungen sehen läßt:
- en: Dominion: The Making of the Western Mind
- us: Dominion: How the Christian Revolution Remade the World
- nl: Heerschappij: Hoe het christendom het Westen vormde
- es: Dominio: Una nueva historia del cristianismo
- fr: Les Chrétiens: Comment ils ont changé le monde.

Holland beschreibt, wie unsere Welt christlich geworden ist und wie die meisten von uns es auch sind. Egal ob sie sich als bewußte Christen, oder Atheisten oder Buddhisten oder Wissenschaftler verstehen.

Das Buch ist in drei Teile gegliedert und besteht aus 21 Kapiteln. Jedes Kapitel ist um ein historisches Geschehen herum geschrieben. Holland beginnt mit dem Hellespont 479 AC und endet mit Merkel in Rostock 2015 AD. Bei zwei Kernpunkten, der Papstrevolution und der soziologische Nietzsche-Interpretation, ist er ganz bei Rosenstock-Huessy. Im Literaturverzeichnis erwähnt er ERH: Driving Power of Western Civilisation, The Christian Revolution of the Middle Ages und Harold Berman: Law and Revolution, The Formation of the Western Legal Tradition. Im Zusammenhang mit der Papstrevolution verweist er auf R.I. Moore: The First European Revolution, c. 970-1215.

Das Ende der christlichen Zeit sieht er bei den Nationalsozialisten, bei denen nur der Starke gut sein kann, ganz wie bei den antiken Römern, Griechen, Persern. Er erzählt wie er beim Beschreiben der antiken Kulturen entdeckte, daß er den ungebrochenen Stolz, wie Caesar eine Million Gallier getötet und ebenso viele versklavt zu haben, nicht teilen konnte. Er sieht darin seine eigene Christlichkeit. Er zitiert dann einen ungenannten indischen Philosophen, der sagte, daß das Christentum sich auf zwei Arten ausbreitete: Bekehrung und Säkularisation.

Erfrischend frei von theologischen Anachronismen ist bei Tom Holland die Erzählung unser kulturgeschichtlichen Wurzeln. Für ihn ist Jesus mit seinem Kreuztod das wichtigste Ereignis der Weltgeschichte und Paulus ihr revolutionärster Autor. Da Rosenstock-Huessy dies wie selbstverständlich voraussetzt, dies heute allerdings als religiöses Sondergut angesehen wird, wird Tom Hollands Buch zu einer wichtigen Hinführung zum Verständnis von Rosenstock-Huessy.

Der deutsche Verlag bewirbt das Buch mit folgenden Worten:

Von Babylon bis zu den Beatles, von Moses bis #MeToo

Souverän und fesselnd schildert Tom Holland die historischen Kräfte und Ereignisse, die die westliche Welt und unsere Wertvorstellungen bis in die Gegenwart prägten und revolutionierten. Ein grandios und elegant erzähltes Geschichtspanorama, das zeigt, wie wir wurden, was wir sind. Wie wurde der Westen zu dem, was er heute ist? Welches Erbe schlägt sich in seiner Gedanken- und Vorstellungswelt nieder? Mit unvergleichlicher Erzählkunst schildert Tom Holland die Geschichte des Westens ausgehend von seinem antiken und christlichen Erbe. Dabei zeigt er, dass genuin christliche Traditionen und Vorstellungshorizonte auch in unserer modernen Gesellschaft sowie ihren vermeintlich universellen Wertesystemen allgegenwärtig sind – sogar dort, wo sie negiert werden: etwa im Säkularismus, Atheismus oder in den Naturwissenschaften. Holland schlägt einen großen erzählerischen Bogen von den Perserkriegen, den revolutionären Anfängen des Christentums in der Antike über seine Ausbreitung im europäischen Mittelalter bis hin zu seiner Verwandlung in der Moderne. In packenden Szenen schildert der Autor welthistorische Ereignisse und zeichnet in lebendigen Porträts die zentralen Akteure oder auch die Antagonisten des Christentums (u. a. Jesus, Paulus, Abaelard und die Heilige Elisabeth, Spinoza, Darwin, Nietzsche und die Beatles). Über große zeitliche Distanzen hinweg macht Holland Verknüpfungen und Parallelen aus und zeigt auf diese Weise, wes Geistes Kind die westliche Kultur noch immer ist.

Tom Holland: Herrschaft – Die Entstehung des Westens, Klett-Cotta, 2021, €28,00

> *Jürgen Müller*

### 6. Manfred A. Schmid: Grammatik statt Ontologie

Noch ein zweites Buch möchte Ihnen ans Herz legen. Es ist die 2011 von Stephan Grätzel et al. herausgegebene Dissertation von Manfred Schmid. Die Herausgeber schreiben in der Einleitung: „Eine hervorragende Einführung in sein [Rosenstock-Huessys] Denken bildet die hier erstmals in Buchform publizierte Dissertation von Manfred A. Schmid aus dem Jahre 1975. Schmid ist es in dieser Arbeit gelungen, das innere System des »Gelegenheitsdenkers«, wie Rosenstock sich selbst charakterisiert, herauszuarbeiten. Damit hat Schmid etwas erreicht, was Kommentatoren vor und nach ihm nicht geschafft haben, den mühsamen und aufwendigen Weg durch Rosenstocks Werk weitgehend zu erleichtern. Schmids umfassende Textkenntnis von Rosenstocks Schriften ist dabei ebenso beachtlich, wie die geradezu virtuose Sicherheit, mit der er Kernstellen und Sätze auswählt und in einen systematischen Zusammenhang stellt.”

Den Kernpunkt der Lehre Rosenstock-Huessys sieht Schmid bereits im Frühwerk: Angewandte Seelenkunde: „Und wir können am Schluß dieses ersten Entwurfs einer Grammatik der Seele jetzt auch sagen, was Grammatik Deutsch heißt: Sie ist die Lehre vom Gestaltenwandel. Abwandlung, Umwandlung, Zeitwandel sind ihre Inhalte. Die Schulgrammatik weiß von Umlaut und Ablaut; die Urgrammatik von Gestaltenwandel! Von hier aus, von der allgemeinen oder richtiger Urlehre des Gestaltenwandels gewinnt auch die Schulgrammatik wieder unsere Bewunderung. In der Tat ist es eine ungeheure Leistung, daß der Mensch alle Personen der „ich liebe, du liebst, er liebt” handhaben kann, daß jeder Mensch sogar im Ablauf der Zeiten diese Verwandlungen der Personen, der Zeiten, der Modi sich angeeignet hat. Es ist das gerade so ungeheuer und gerade so irreführend, wie daß ein jeder Mensch beten, erzählen, singen, befehlen und gehorchen kann, daß jeder denken, rechnen und dichten lernt heutzutage. Die primitivste Grammatik enthält schon das ganze Wunder des Menschseins wie die höchste „Kultur”. Die Menschen haben jene wie diese von wenigen Urschöpfern empfangen und handhaben jene wie diese vielfach nur scheinbar.” Rosenstock-Huessy: Die Sprache des Menschengeschlechts I, p.768

Schmid bringt die in vielen Einzelstücken ausgearbeiteten Gestaltkonstellationen Rosenstock-Huessys in Tabellen zur Übersicht, insbesondere die viergliedrige Leibhaftige Grammatik und die dreigliedrige Trinitarische Grammatik. Dadurch wird z.B. auch klar, warum Rosenstock-Huessy in manchen Texten viergliedrig: Du – Ich – Wir – Es und in anderen dreigliedrig: Du – Ich – Es argumentiert..

Schmids Arbeit zeigt, daß Rosenstock-Huessy mit Dialogik in Verbindung zu bringen, irreführend ist: „Gegenüber jener weitverbreiteten Gepflogenheit, Martin Buber, Franz Rosenzweig, Ferdinand Ebner, Hans Ehrenberg, Gabriel Marcel und Eugen Rosenstock-Huessy unter der Sammelbezeichnung »Dialogik«, »Dialogismus« Oder »Philosophie des Dialogs« zusammenzufassen —wobei meist von Martin Bubers »lch und Du« als dem repräsentativen Werk dieser Strömung ausgegangen wird, während die übrigen Denker allenfalls als ergänzende bzw. »korrektive« Beiträge mitberücksichtigt werden setzt sich neuerdings immer mehr die Meinung durch, den Ausdruck »Dialog« nur für Martin Buber zu reservieren. Auch wir haben uns diesen differenzierenden Standpunkt zu Eigen gemacht, wenn wir von Anfang an Eugen Rosenstock-Huessys Denken in seiner Eigenständigkeit mit dem Terminus »Grammatik« bzw. »grammatisches Denken« belegten, weil vor allem für ihn die Bezeichnung »dialogisch« irreführend wäre.”

Der traditionelle Philosoph, der aus der Perspektive des denkenden Ich ein System errichtet, wird den sozialen Gestaltkonstellationen und Gestaltwechseln nicht gerecht. Diese sind nämlich Inkarnationen, benötigen Lebenszeit und lassen sich nur in abstrahierender Weise in einem System darstellen. Philosophen haben darum, nach Rosenstock-Huessy, eine wichtige Funktion als Spezialisten für Snapshot-Analysen und -Konstruktionen (ihre Zeit in Gedanken erfasst).

Die Lesereihenfolge beginnend mit Teil I, Abschnitt 3: Grammatik, dann Teil II: Leibhaftige Grammatik und dann zurück zum Beginn von Teil I: Ontologie und Grammatik hat sich für mich bewährt.

Manfred A. Schmid: Grammatik statt Ontologie, Eugen Rosenstock-Huessys Herausforderung der Philosophie, Karl Alber Freiburg, €22,00

> *Jürgen Müller*


### 7. Termin der Jahrestagung und Mitgliederversammlung 2021

Die Jahrestagung und die Mitgliederversammlung 2021 haben wir jetzt vom 19.21.November 2021 im Haus am Turm in Essen-Werden geplant. Die Mitgliederversammlung wird sich mit den Jahren 2019 und 2020 befassen. Wir bitten Sie, sich diesen Termin vorzumerken. Thema und Programm werden wir Ihnen noch mitteilen. Der früher geplante Termin im März 2021 läßt sich angesichts der aktuellen Corona-Lage nicht realisieren.

> *Jürgen Müller*

### 8. Adressenänderungen

Bitte senden sie eine eventuelle Adressenänderung schriftlich oder per E-mail an Thomas Dreessen (s. u.), er führt die Adressenliste. Alle Mitglieder und Korrespondenten, die diesen Brief mit gewöhnlicher Post bekommen, möchten wir bitten, uns soweit vorhanden, ihre Email-Adresse mitzuteilen.

> *Thomas Dreessen*

### 9. Hinweis zum Postversand

Der Rundbrief der Eugen Rosenstock-Huessy Gesellschaft wird als Postsendung nur an Mitglieder verschickt. Nicht-Mitglieder erhalten den Rundbrief gegen Erstattung der Druck- und Versandkosten in Höhe von € 20 p.a. Den Postversand betreut Andreas Schreck. Der Versand per e-Mail bleibt unberührt.

> *Andreas Schreck*

### 10. Mitgliederbeitrag 2020 und 2021

Die „Selbstzahler” unter den Mitgliedern werden gebeten, den Jahresbeitrag in Höhe von 40 Euro (Einzelpersonen) auf das Konto Nr. 6430029 bei Sparkasse Bielefeld (BLZ 48050161) zu überweisen. IBAN-Kontonummer: DE43 4805 0161 0006 4300 29; SWIFT-BIC: SPBIDE3BXXX

Die Lastschrift-Abbuchungen des Beiträge 2020 und 2021 erfolgen in Kürze. Soweit Sie kein Mandat erteilt haben, nehmen Sie bitte die Überweisung vor, soweit noch nicht geschehen.

> *Andreas Schreck*

[zum Seitenbeginn](#top)
