---
title: Archivbericht 2008
created: 2008
category: archiv
---

### 1. Besucher

Es war mir eine große Freude, den Vorstand unserer Gesellschaft im Archiv begrüßen zu können, der sich sogar einen ganzen Tag lang im Archiv aufhielt und Archivalien zu „Frucht der Lippen” bearbeitete.

Außerdem: Fritz Herrenbrück, der an einen großen Aufsatz über Eugen Rosenstock-Huessy und Karl Löwith schrieb, der jüngste Sohn von Georg Müller, der mit Klassenkameraden der Aufbauschule anlässlich des Goldenen Abiturjubiläums das Archiv besuchte.

### 2. Anfragen

Neben diversen Anfragen von Fritz Herrenbrück, Eckart Wilkens, Rudolf Hermeier, Andreas Schreck, Mark und Raymond Huessy gab es eine Anfrage von einem Wolfgang Herzog, eine von Prof. Dr. Wilhelm Rimpau, einem der Herausgeber der Viktor von Weizsäcker-Gesamtausgabe, und von weiteren Mitgliedern der Weizsäcker-Gesellschaft, an deren Tagung in Bethel ich teilgenommen habe und über die beiden Freunde Viktor von Weizsäcker und Eugen Rosenstock-Huessy erzählen konnte. In diesem Zusammenhang bekam das Archiv 3 bisher unbekannte Briefe von ERH an Viktor von Weizsäcker in Kopie von der Tochter Viktor von Weizsäckers, Frau Cora Penselin.

Ich habe im Laufe des Jahres ca. 20 Briefe verschickt und viele E-Mails geschrieben.

### 3. Arbeiten im Archiv

Fortsetzung meiner bisherigen Arbeit: Inventarisierung, Vervollständigen der Chronik des Lebens Eugen Rosenstock-Huessys. Hoffentlich werde ich mit der Inventarisierung fertig bis 2012, wenn ich, wie beabsichtigt, mich aus der Arbeit im Archiv verabschieden möchte! Teilnahme an der Tagung in Frankfurt/M über Franz Rosenzweig und Eugen Rosenstock-Huessy im Juni 2008. Mein Kontakt zu der Tochter von Albert Mirgeler ist leider abgebrochen.

### 4. Neuerwerbungen

Außer den genannten Briefen an Weizsäcker besitzt das Archiv nun eine interessante Prüfungsarbeit von Jan Kroeze, der zwischen 1959 und 1961 an Vorträgen teilnahm, die Rosenstock-Huessy in den Niederlanden hielt.

Bücher: Hartmut von Hentig (Bewährung – Von der nützlichen Erfahrung, nützlich zu sein), Jochen Köhler (Helmuth J. v. Moltke), ältere Dissertationen von Manz und von Beyfuß sowie Gedichte Joseph Wittigs (abgeschrieben von Eckart Wilkens)

### 5. Neues Archiv

Der Neubau des großen Archivgebäudes in Bethel für die Archive von Bethel, der Ev. Landeskirche von Westfalen und des Diakonischen Johanniswerkes macht Fortschritte, vielleicht kann tatsächlich wie geplant 2010 eingezogen werden.

Gottfried Hofmann, Mai 2009
