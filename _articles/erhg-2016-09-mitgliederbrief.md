---
title: Mitgliederbrief 2016-09
category: rundbrief
created: 2016-09
summary:
zitat: |
  >Wir haben den Widerspruch zu dem abgelaufenen Äon von Abailard bis Hegel weit aufgerissen,\
  >damit der Leser weiß, in welchem Äon wir weilen:
  >
  >nach Nietzsche, nach Freud, nach Marx, nach Darwin.
  >
  >Wir sind dazu gezwungen, weil wir nicht mehr Wissen vom toten Raumobjekt\
  >mit Streit, Verheißung, Gesang lebender Zeiten verwechseln dürfen,\
  >bei Strafe der Vernichtung.\
  >Die Schicht, die sterben will und dem Äon des Thomismus, Kants oder Fichtes verhaftet denkt,\
  >ist achtenswert und zahlreich.\
  >Ja, es sind die guten, die Elite, die Kaloi Kagathoi,\
  >die sich auf eines dieser Denksysteme festlegen.\
  >So wollen sie in die Zeiten starren oder schauen.\
  >Tausende von Offizieren sind so 1914 und 1939 stumm in den Tod marschiert.
  >*Eugen Rosenstock-Huessy, Soziologie, Die Vollzahl der Zeiten, 1958, p.19*
---

## Eugen Rosenstock-Huessy Gesellschaft e.V.

{{ page.zitat }}

**Vorstand/board/bestuur:**\
*Dr. Jürgen Müller (Vorsitzender);*\
*Thomas Dreessen; Andreas Schreck; Dr. Eckart Wilkens; Rudolf Kremers*\
*Antwortadresse: Jürgen Müller: Vermeerstraat 17, 5691 ED Son, Niederlande*\
*Tel: 0(031) 499 32 40 59*

***Brief an die Mitglieder September 2016***

**Inhalt**

{{ page.summary }}


[zum Seitenbeginn](#top)
