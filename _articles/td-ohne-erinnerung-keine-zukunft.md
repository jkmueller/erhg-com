---
title: "Thomas Dreessen: Ohne Erinnerung keine Zukunft!"
category: einblick
published: 2022-01-25
---

#### Heute:  In memoriam Eugen Rosenstock-Huessy (†24.2.1973)

*Eugen Rosenstock-Huessy, geb.1888 in Berlin, ließ sich 1908 taufen, war Rechtshistoriker und dann Kriegsteilnehmer 1914-18: er erlebte den Selbstmord Europas im Gespräch mit seiner Frau Margret und seinem jüdischen  Freund Franz Rosenzweig. Rosenstock hat den „Lügenkaiser“ Hitler schon 1918 prophezeit.  Die wichtigste Frucht seines Kampfes um die Erwachsenenbildung von 1918 bis 1933 ist der Kreisauer Kreis um Helmuth James von Moltke. Die Kreisauer sind Märtyrer der Epoche der planetarischen Gegenseitigkeit- Vorläufer unserer Zukunft. Rosenstock-Huessy ist 1933 in die USA eingewandert und  lebte bis zu seinem Tode 1973 in Vermont. Er lebte die letzte Dekade seines Lebens mit Freya von Moltke.   Er wirkte in USA in Harvard und Dartmouth, gründete das Camp William James. In den 1950er Jahren hat er an den Universitäten Münster und Göttingen Gastvorlesungen gehalten; und ist im Rundfunk und der Erwachsenenbildung in Deutschland und Europa zur Sprache gekommen.*  

***Respondeo etsi mutabor!*** – Ich antworte, auch wenn mich meine Antwort verändert!

Dieses Leitwort meines Lehrers Eugen Rosenstock-Huessy  (1888-1973) muß um der Zukunft menschlichen Lebens auf dem Planeten Erde willen an die Stelle des die Moderne beherrschenden Leitwortes: *Cogito ergo sum* – Ich denke, daher bin ich! (Rene Descartes) treten.  In der aus Descartes entspringenden rationalistischen Herrschaft über Menschen und Völker wurzeln nämlich die Kriege, der Rassismus, die Zerstörung des Lebens und seiner Grundlagen in unserer Zeit. Die Moderne entlarvt sich heute als unbelehrbares ancien regime, das die Zukunft des Lebens zerstört, weil es wähnt sie berechnen und verplanen und wissen zu können. Die Moderne setzt Frieden als „Natur“, dabei bleibt Frieden immerwährende Aufgabe. Die Moderne hat den Heiligen Geist verlassen!

Dieses Wort stellte Rosenstock-Huessy über die Epoche der Gegenseitigkeit, die mit der Weltrevolution 1914-1945 angefangen hat.  Rosenstock-Huessys Leitwort führt uns zur Wiederentdeckung der Sprache als des Trägers des Geistes in allen Völkern ohne die es menschliches geschichtliches Leben nicht gibt  (cf. 1.Korinther 14).  Die wirkenden Kräfte in der Gesellschaft sind Glauben. Hoffen und Lieben:
Er sagt:  „Und was ist geistig(spiritual? Daß ihr sprechen könnt. Und daß Ihr durch Sprechen dem großen Lebensstrom des Menschengeschlechts vom Anfang bis zum Ende beitretet. Durch Sprechen erschaffen wir die Gezeiten der Geschichte. Ohnedem gibt es zwischen den Generationen keine Verbindung.“[^1]  „Der Geist wird durch Übersetzung am Leben gehalten. In jeder Generation übersetzen wir erneut. ... Nur wenn Menschen verschiedenen Geschlechts, verschiedenen Alters und verschiedener Mentalität im selben Geist dienen, bleibt der Geist echt. Dasselbe zu tun, ist nicht desselben Geistes zu sein.“[^2]

[^1]: „And what is spiritual? That you can speak. And that by speaking, you enter into great life stream of humanity from beginning to end.” (The Rosenstock-Huessy Lectures, Universal History, 1954, 8.)
“By speaking, we create the times of history. There is no connection between the generations without it.“ (The RosenstockHuessy Lectures, Universal History, 1954, 5.)

[^2]: „Spirit is kept alive by translation. In every generation we retranslate….The spirit is only true when people of different sex, and different age, and different mind serve in this spirit. To do the Same thing is not to be of one spirit.“ (The Rosenstock-Huessy Lectures, Universal History, 1956, 9.)

*Theologie ist die Lehre von den Mächten, die Menschen sprechen machen. Philosophíe handelt von den Dingen, über die wir sprechen wollen. Beide lehren nicht auf die Frage unserer Zeit: dass nämlich einjeder und eine jede sprechen müssen. Rosenstock-Huessy propagiert daher Soziologie als eine dritte Wissenschaft: Soziologie – Sie schafft die Umgebung, in der wir sprechen.* In seiner Sprachlehre entdeckt er uns die Grammatik, als das Organon, mit dem wir die wirkliche zeitliche Ordnung der Gesellschaft in ihren Verwandlungen, der Erneuerung und Erhaltung des Lebens dienend, erkennen und rechtzeitig ermöglichen können.  Als Antwortende auf Anrede treten wir in das wirkliche Leben ein, das noch nicht abgeschlossen ist: das gilt für Babies, für Be-rufungen, auch für Liebe und  Ehe und Zusammenarbeiten.  Heute kommt es darauf an, dass wir Menschen verschiedener Sprachen gemeinsame Not anerkennen und Gelegenheit bekommen darauf gemeinsam rechtzeitig zu antworten mit unserem Leben.  

Für die Erneuerung des Lebens in planetarischer Solidarität wird Rosenstock-Huessys Lehre  gebraucht. Deshalb empfehle ich Euch/Ihnen die deutsche Neuausgabe von seinem 1946 in USA veröffentlichten Werk: Des Christen Zukunft – oder: Wir überholen die Moderne, die die Kernlehre enthält.

[Leseprobe](https://agenda-verlag.de/wp-content/uploads/Dreessen.pdf)

[Mehr zu Rosenstock.Huessy](www.rosenstock-huessy.com)
