---
title: Mitgliederbrief 2017-05
category: rundbrief
created: 2017-05
summary: |
  1. Einladung zur Jahrestagung und Mitgliederversammlung - Jürgen Müller
  2. Tagungsort und Anmeldung                             - Andreas Schreck
  3. Programm der Jahrestagung                            - Jürgen Müller
  4. Einladung und Programm der Mitgliederversammlung     - Jürgen Müller
  5. Adressenänderungen                                   - Thomas Dreessen
  6. Hinweis zum Postversand                              - Andreas Schreck
  7. Jahresbeiträge 2017                                  - Andreas Schreck
zitat: |
  >Wenn heute in Deutschland Menschen zusammenkommen, geht es immer um das Wiedergewinnen der Ruhe. Die Bewunderung für die Betriebsamkeit ist aus unseren Herzen gewichen, aber nicht aus den Muskeln. Die wenigsten Menschen wissen sich zurückzuziehen aus dem Getriebe.
  >Wie ist die Arbeit über das ganze geistige Leben und auch über die Kirche doch Herr geworden!\
  >…\
  >Zwei Dinge sind in ihrer ganzen Wichtigkeit deutlich:\
  >Die Arbeit selbst steht heute zur Debatte, weil sie nicht mehr unter der Hand geschieht;
  >das andere, das sich daraus ergibt, ist, daß aus diesem Aufbrechen der Arbeitsfrage als solcher sich die Ablösung von der Berufsfrage ergibt, weil der Mensch als Kreatur von den Gesetzen der Arbeit zerrissen zu werden, pausenlos und heimatlos zu werden droht.
  >Daraus erwächst die Missionsaufgabe. Die Lage befällt die ganze Menschheit.
  >*Eugen Rosenstock-Huessy, Die Verklärung der Arbeit, in Das Alter der Kirche, Band 2 1927*
---

## Eugen Rosenstock-Huessy Gesellschaft e.V.

{{ page.zitat }}

**Vorstand/board/bestuur:**\
*Dr. Jürgen Müller (Vorsitzender);*\
*Thomas Dreessen; Andreas Schreck; Dr. Eckart Wilkens; Rudolf Kremers*\
*Antwortadresse: Jürgen Müller: Vermeerstraat 17, 5691 ED Son, Niederlande*\
*Tel: 0(031) 499 32 40 59*

***Brief an die Mitglieder Mai 2017***

**Inhalt**

{{ page.summary }}

### 1. Einladung zur Jahrestagung und zur Mitgliederversammlung
Für Rosenstock-Huessy ist die Reformation ein so wichtiges Ereignis, daß er sie in seiner Autobiograhie des westlichen Menschen als eine der fundamentalen Revolutionen beschreibt, die uns geformt haben.\
Wenn wir dieses Jahr den 500sten Jahrestag von Luthers Thesenanschlags feiern, wird oft die Frage nach der Aktualität Luthers gestellt. Gerade die lutherische Kirche betont den bleibenden Wert seiner Aussagen.\
Rosenstock-Huessy hat sich vor 90 Jahren zu dieser Frage in dem Essay: Die Verklärung der Arbeit geäußert. Wie hat die evangelische Freiheit im Leben der Menschen Gestalt gewonnen? Rosenstock-Huessys Analyse führt dazu, daß Luthers Ansatz für uns heute neue Relevanz gewinnt.
Auf der Jahrestagung vom 23. bis 25. Juni 2017 in Bebra-Imshausen wollen wir uns durch Rosenstock-Huessys *Die Verklärung der Arbeit* herausfordern lassen und versuchen eigene Antworten zu finden.\
Ich möchte Sie zur Jahrestagung und zur Mitgliederversammlung (am 24. Juni) herzlich einladen.
>*Jürgen Müller*

### 2. Tagungsort und Anmeldung

Der Tagungsort, die Stiftung Adam von Trott zu Solz e.V., Imshausen bei Bebra (zwischen Kassel und Fulda in Hessen), ist seit 1938 bis heute durch einen kommunitären Geist geprägt. Wir tagen und wohnen im „Herrenhaus”. Wegen der eher einfachen Ausstattung – Zimmer mit eigenem WC gibt es (noch) nichtwird es auch Möglichkeiten zur Hotelübernachtung im nahen Bebra (8 km) geben.\
***Kosten***\
Die Tagungskosten bei Übernachtung im Einzelzimmer oder auf Wunsch im Doppelzimmer im Tagungshaus (einfache Ausstattung, kein Bad/WC im Zimmer) betragen € 125. Für Bettwäsche werden € 5 berechnet.\
Für Gäste, die ein Einzelzimmer mit Bad/WC wünschen, sind Zimmer im nahen Hotel Röse, Bebra, Hersfelder Str. (150 m vom Bahnhof Bebra entfernt) reserviert. Die Kosten mit Frühstück betragen ab € 45 pro Nacht. Hinzu kommen Tagungskosten in Höhe von € 80. Gäste, die das Hotel bevorzugen, geben dies bitte mit der Anmeldung bei Andreas Schreck an (bitte nicht direkt beim Hotel Röse anmelden).\
***Anreise***\
Bebra-Imshausen liegt im Dreieck Kassel-Eisenach-Fulda. Aus diesen Städten besteht stündliche Zugverbindung. Am Freitag 23.06.2017 verkehrt die Buslinie 315 um 16.07Uhr/18.07 Uhr ab Bahnhof Bebra nach Bebra-Imshausen, Fahrzeit 11 Minuten. Im Dorf ist die Stiftung Adam von Trott ausgeschildert.\
Hinweis zu den Mahlzeiten: Da unsere Gruppe „privat” von 2 Damen bekocht wird, können individuelle Wünsche direkt besprochen werden. Das Essen wird überwiegend vegetarisch ausgerichtet sein.\
***Anmeldung***\
bitte an Andreas Schreck, Tel. 0551-28047871;

### 3. Programm der Jahrestagung

| Freitag, 23.6 | 18:00 Eintreffen |
|               | 18:30 Abendessen |
|               | 19:30 Die Frage, die uns das Leben stellt – Dritter Teil: Die Arbeit der Kreatur? (Thomas Dreessen) |
|               | 21:30 Geselliges Beisammensein |
| Samstag, 24.6 | 8:30 Frühstück |
|               | 9:30 Zweiter Teil: Luthers Tafel der Werte (Andreas Schreck) |
|               | 11:00 Pause |
|               | 11:30 Abschluß des zweiten Teils: Friedensschluß (Jürgen Müller) |
|               | 12:15 Pause |
|               | 12:30 Mittagessen |
|               | 14:45 Erster Teil: Kirchenlehre und Arbeitswissenschaft (Rudolf Kremers, angefragt) |
|               | 15:30 Kaffeepause |
|               | 16:00 Mitgliederversammlung |
|               | 18:30 Abendessen |
|               | 20:00 Konzert: Scholastik, Akademik, Argonautik in der Musik (Eckart Wilkens) |
|               | 21:30 Geselliges Beisammensein |
| Sonntag, 25.6 | 8:00 Morgenandacht |
|               | 8:30 Frühstück |
|               | 10:00 Der neue Sonntag: Sabbat und Sabbatjahr (Eckart Wilkens) |
|               | 11:30 Abschlußgespräch |
|               | 12:30 Mittagessen |
|               | 13:00 Abreise |

### 4. Einladung und Programm der Mitgliederversammlung

***Eugen Rosenstock-Huessy Gesellschaft e.V., Bielefeld***\
Einladung\
zur ordentlichen Mitgliederversammlung am Samstag, 24. Juni 2017, 16:00 Uhr\
Stiftung Adam von Trott zu Solz, Bebra-Imshausen

TOP 1: Begrüßung der Mitglieder, Feststellung der Beschlussfähigkeit und
     Festlegung der Protokollführung\
TOP 2: Genehmigung des Protokolls der Mitgliederversammlung vom 5. November 2016
     in Bebra-Imshausen\
TOP 3: Anträge zur Änderung/Erweiterung der Tagesordnung\
TOP 4: Bericht des 1. Vorsitzenden Jürgen Müller mit Aussprache\
TOP 5: Kassenbericht für das Geschäftsjahr 2016\
TOP 6: Bericht des Kassenprüfers\
TOP 7: Entlastung des Vorstands für das Geschäftsjahr 2016\
TOP 8: Kurzbericht des Archivverantwortlichen Thomas Dreessen\
TOP 9: Berichte von Respondeo, vom Eugen Rosenstock-Huessy Fund und
       der Eugen Rosenstock-Huessy-Society und von Projekten einzelner Mitglieder\
TOP 10: Wahl eines Wahlleiters/einer Wahlleiterin für die Neuwahl des Vorstands\
TOP 11: Wahl des/der 1. Vorsitzenden\
TOP 12: Wahl des/der stellvertretenden Vorsitzenden\
TOP 13: Wahl des dritten geschäftsführenden Vorstandsmitglieds\
TOP 14: Wahl zweier weiterer Vorstandsmitglieder\
TOP 15: Verschiedenes

### 5. Adressenänderungen
Bitte schreiben sie eine eventuelle Adressenänderung schriftlich oder per E-mail an Thomas Dreessen (s. u.), er führt die Adressenliste. Alle Mitglieder und Korrespondenten, die diesen Brief mit gewöhnlicher Post bekommen, möchten wir bitten, uns soweit vorhanden ihre Email-Adresse mitzuteilen.
>*Thomas Dreessen*

### 6. Hinweis zum Postversand
Der Rundbrief der Eugen Rosenstock-Huessy Gesellschaft wird zukünftig als Postsendung nur noch an Mitglieder verschickt.\
Nicht-Mitglieder erhalten den Rundbrief gegen Erstattung der Druck- und Versandkosten in Höhe von € 20 p.a.\
Den Postversand betreut Andreas Schreck. Der Versand per e-Mail bleibt unberührt.
>*Andreas Schreck*

### 7. Jahresbeiträge 2017
Die „Selbstzahler” unter den Mitgliedern werden gebeten, den Jahresbeitrag in Höhe von 40 Euro (Einzelpersonen) auf das Konto Nr. 6430029 bei Sparkasse Bielefeld (BLZ 48050161) zu überweisen.
Nach Einführung des einheitlichen Zahlungssystem SEPA mit der Internationalen Kontonummer (IBAN): DE43 4805 0161 0006 4300 29
SWIFT-BIC: SPBIDE3BXXX ist diese „IBAN”-Nummer auch für Mitglieder in den Niederlanden und im ganzen „SEPA-Land”, in das sich sogar die Schweiz hat einbetten lassen,
ohne Zusatzkosten nutzbar.
>*Andreas Schreck*

[zum Seitenbeginn](#top)
