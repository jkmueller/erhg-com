---
title: "Bibliographie Georg Müller"
category: rezeption
published: 2023-07-11
---
BIBLIOGRAPHIE GEORG MÜLLERS \
in Georg Müller: Pädagogische Beiträge, Bielefeld 1960

BIBLIOGRAPHIE AUS FÜNFUNDDREISSIG JAHREN DIENST \
AN DER FRIEDRICH VON BODELSCHWINGH-SCHULE

### 1. Für den Schulkreis bestimmte Druckveröffentlichungen:

- Schule und Leben; Die Aufbauschule in Bethel; Auf dem Wege zur Friedrich von Bodelschwingh-Schule: Sonderdrucke von je drei Aufsätzen über die innere Haltung, die Stellung der Schule im Rahmen des höheren Schulwesens und ihre schulpolitische Begründung aus den Jahren 1924 bis 1926.
- Jahresberichte über die beiden Gründungsschuljahre 1925/26 und 1926/27, ferner über die Schuljahre 1927/28,1928/29 und 1929/30 mit pädagogischen und wissenschaftlichen Beilagen aus den Jahren 1927 bis 1930.
- Kükenshover Blätter. Aus der Arbeit der Friedrich von Bodelschwingh-Schule in den Schuljahren 193O/31 und 1931/32, 80 Seiten, 1932.
- Ds.2. Folge über die Schuljahre 1932/33 und 1934/35, 116 Seiten, 1934.
- Weihnachtsrundbrief 1940.
- Kriegsrundbrief mit Beilage "Rund um den Hohen Meißner" 1941.
- Kriegsrundbrief mit Beilage "Idealismus und Wirklichkeit" (i.gl.J.).
- Kriegsrundbrief mit Einführung: Das Vertrauen zur Schöpfung, 1942.
- Kriegsrundbrief mit Einführung: Die Geschichtlichkeit des Menschen (i.gl.J.).
- Kriegsrundbrief mit Einführung: Das Verfangensein im Schicksal, 1943.
- Kriegsrundbrief mit Einführung: Die Entschlossenheit zum Schicksal (i.gl.J.).
- Kriegsrundbrief mit Einführung: Die Gemeinschaft der Seelen jenseits des Grabes, 1944.
- Absage an Nietzsche, 56 Seiten, 1945.
- Gedenkwort für Pastor D. Friedrich von Bodelschwingh; Schafft Jugendgelände!; Ansprache bei der Entlassung des ersten Förderlehrgangs für Kriegsteilnehmer am 16. September; Ansprache anläßlich des gemeinsamen Elternfestes der Friedrich v. Bodelschwingh-Schule und der Sareptaschule am 29. September: Sonderdrucke aus dem Jahre 1946.
- Kükenshover Blätter 16. Folge, 12 Seiten, 1947.
- Kükenshover Blätter 17. Folge, 40 Seiten, 1948.
- Kükenshover Blätter 18. Folge, 70 Seiten, 1950.
- Gedenkheft der Friedrich von Bodelschwingh-Schule, 59 Seiten, 1950.
- Kükenshover Blätter 20. bis 22. Folge, 1OO Seiten, 1953.
- Kükenshover Blätter 23. bis 25. Folge, 92 Seiten, 1956.

### 2. Für die Offentlichkeit bestimmte Darstellungen aus unserer Schularbeit

- Die Aufbauschule. Aufwärts 31/26.
- Die Deutsche Oberschule. Aufwärts 32/26.
- Die Betheler Aufbauschule. Aufwärts 43/26; Tägliche Rundschau 23 und 30/27.
- Schule und Evangelium. Neuwerk 9/26.
- Vom Sinn einer evangelischen Schule. Die Tat 11/27.
- Die Friedrich von Bodelschwingh-Schule, eine evangelische Lebensschule. Auf dem Weg zur neuen Schule. Funkdienst Berlin 1929; Beth-El 4/29.
- Vom Sinn einer evangelischen Schule. München, Verlag Christian Kaiser, 92 Seiten, 1931.
- Zehn Jahre Friedrich von Bodelschwingh-Schule. Beth-El 8/35.
- Aus dem Leben einer evangelischen Oberschule. Gladbeck, Verlag Martin Heilmann, 38 Seiten, 1950.

### 3. Schul- bzw. kulturpolitische Äußerungen

- Zur Jugendbewegung. Aufwärts 67/24.
- Aufruf zur Planwirtschaft. Ebd. 86/24.
- Zum Tode Paul Natorps. Ebd. 203/24.
- Jugendbewegung und Evangelium. Ebd. 217/24.
- Das Gebot der Stunde. Ebd. 231/24.
- Kirche und Jugendbewegung. Ebd. 276/24.
- Öffnet die Kirchen! Ebd. 175/25.
- Die neue Lehrerbildung und der Lehrernachwuchs. Ebd. 200/25.
- Zur Neuordnung des preußischen höheren Schulwesens. Ebd. 258 u.260/26; Jahresbericht 1927/28.
- Pestalozzi heute. Aufwärts 40/27.
- Richtlinien für ein verfassungsmäßiges Reichsschulgesetz. Ebd. 129/27; Deutsche Lehrerzeitung 23/27.
- Zur Frage der Pädagogischen Akademien. Aufwärts 276 und 277/27; Zwiespruch 62/27
- Sätze zur Schulfrage. Zwiespruch 3/28.
- Wie steht's um Deutschlands Jugend? Aufwärts 57/28.
- Entschließung der Bündischen Schulkonferenz. Zwiespruch 11/28.
- Die Vertreter der Deutschen Oberschulen in Weimar. Aufwärts 248/28; Jb. 1928/29.
- Autonomie der Pädagogik? Zwiespruch 42/28.
- Die seelische Lage unserer Jugend angesichts der verzweifelten Berufsaussichten. Deutsches Philologenblatt 31/31; K.Bl. 1. Folge.
- Zum Schulprogramm der Reichsregierung. Äfwärts 216 und 217/32; K. Bl. 2. Folge.
- Freizeitgestaltung in den Lagern des freiwilligen Arbeitsdienstes. Evangelische Schulzeitung 3/33.
- Askese oder Umweltgemäßheit? Gedanken zur Typisierung der höheren Schule. Der Volksstaat 9 u. 10/33; K.Bl. 1. Folge.
- Leitsätze für die Gestaltung einer praktischen höheren Knabenschule. Schule und Evangelium 3/33.
- Freiwilliger- Arbeitsdienst und Volk-Bildung. Das Elvangelische Westfalen 4/33.
- Die evangelische Schule im nationalsozialistischen Staat. Korrespondenzblatt der Evangelischen Schulvereinigung 9/33.
- Zur Frage schulpolitischer Forderungen seitens der Kirche. Das evangelisch Westfalen 1/32.
- Die Aufbauschule als ländliche Sammelschule. Pädagogische Rundschau 7/49; K.Bl. 1950.
- Staatsbürgerlicher Unterricht. Der Stahlhelm g/52.
- Die Aufbauschule als Deutschkundliches Gymnasium. K.Bl. 1953.
- Unsere Schularbeit im Blick auf die theologisch-pädagogische Aussprache. K.Bl. 1956.
- Von der theologischen Grundhaltung zum neuen Sprachdenken. Eb.

### 4. Pädagogische und geisteswissenschaftliche Veröffentlichungen
#### 1924 bis 1932

- Vom historischen Verstehen. Jahrbuch der philosophischen Fakultät in Marburg 1924.
- Der Krieg und wir. Der junge Wandervogel 3/4/24; Zwiespruch 31/28.
- Wandern und Schauen. Aufwärts 207/24.
- Von Begeisterung und Nüchternheit. Wandervogel 1/2/25; Zwiesspruch 38/28.
- Evangelische Führererziehung. Der Geisteskampf der Gegenwart 4/25.
- Ein deutsches Eton? Aufwärts 22/25.
- Vom Wandervogeltum. Wandervogelwarte 4/25.
- In die Natur hinaus - in die Bibel hinein. Aufwäts 132/25.
- Vom Führertum in der Schule. Wandervogel 5/6/26.
- Evangelische Erziehung oder nicht? Schule und Evangelium 1/27; Evangelische Jugendhilfe 12/27.
- Der Weg zum neuen deutschen Bildungsideal und die Jugendbewegung. Zwiespruch 6/28.
- Vom Wesen der Strafe. Jahresbericht 29/30.
- Zur Aussprache über die Begründung einer evangelischen Pädagogik. Ebd. 29/30.
- Trotzkt als Sphinx. Aufwärts 35 bis 38/30; Zwiespruch 5 und 6/30.
- Das Rätsel der Weltgeschichte, Beth-El 1/32.
- Gedanken zu einem christlichen Geschichtsunterricht. Korrspondenzblatt der Evangelischen Schulvereinigung 2/32.
- Der christliche Staatsmann. Zwischen den Zeiten 2/32.
- Geschichtlichkeit. Schule und Evangelium 7/32.
- Die geschichtsphilosophischen Begriffe des Verstehens und Deutens und das Anliegen eines christlichen Verständnisses der Geschiche. Korrespendenzblatt 7/8/32.

#### 1933 bis 1939

- Die Reformation, ein "erklärbarer" Vorgang? Zwischen den Zeiten 2/33.
- Unsere völkische Geschichte als Weg zur Volkwerdung. K.Bl. 2. Folge.
- Autorität in der Erziehung. Christentum und Wirklichkeit 5/33.
- Rasse, Ur- und Vorgeschichte. Korrespondenzblatt 9/33.
- Knabenerziehung. Beitrag zu M. v. Tiling und K. Jarausch, Grundfragen pädagogischen Handelns, 1934.
- Rings um den Hohen Meißner. Beitrag zu Will Vesper: Deutsche Jugend. Dreißig Jahre Geschichte einer Bewegung 1934.
- Wunschbilder und Wirklichkeit germanischer Religion. Witten, Evangelischer Presseverband für Westfalen, S. 35, 1931.
- Vorgeschichte als Uroffenbarung? Ebd. S. 36, 1934.
- Zeugnisse germanischer Religion, München, Verlag Christian Kaiser, 184 Seiten, 1935.
- Vom Urriesen Ymir oder von der Bedenklichkeit weltanschaulich bestimmter Mythendeutung. Evangelisches Schulblatt 3 und 4/35.
- Wodan. Deutsches Pfarrerblatt 22 u.23/35.
- Germanischer Himmelsglaube. Ebd. 26/35.
- Germanischer Fruchtbarkeitskult. Ebd. 27/35.
- Bestattungsgebräuche und Totenehre. Ebd. 28/35.
- Ahnenkult und Heldenglaube. Ebd. 29/35.
- Totenvorstellung und Totenbeschwörung. Ebd. 30/35.
- Totenörter: Helgafell und Valhall. Ebd. 32/35.
- Die germanisch he. Christliche Volkswacht 11/12/35.
- Erziehung unter dem Evangelium. Zeitwende 10/36.
- Die germanische Frau vor dem Evangelium. Evangelische Frauenhilfe 1, 4, 5 und 8/39.

#### 1946 bis 1959

- Nietzsche und die deutsche Katastrophe. Gütersloh, Verlag Bertelsmann, 43 Seiten, 1946.
- Goethe und die deutsche Gegenwart. Ebd. 83 Seiten, 1946.
- Die evangelische Kirche und die geistig-seelische Welt. Zeitwende 7/47.
- Zum Deutschunterricht an einer evangelischen höheren Schule. Beitrag zu: Erziehung unter dem Wort, Bethel 1948.
- Geschichtstheologie? Evangelisch-Lutherische Kirchenzeitung 20 u. 21/48.
- Biblische Wurzeln unseres Geschichtsverständnisses. Gladbeck, Verlag Martin Heilmann, 35 Seiten, 1949.
- Zur Neuorientierung des Geschichtsunterrichts. Evangelisxch-Lutherische Kirchenzeitung 8/49.
- Ursprung und Vollendung der idealistischen Geschichtsphilosophie. Ebd. 13/49.
- Goethe und das Christentum. Deutsches Pfarrerblatt 14/49.
- Goethe und Dostojewskij. Ebd. 18/49.
- Um das neue Geschichtsbild. Ebd. 5/50.
- Von der Geschichte als Schuldzusammenhang. Ebd. 6/50.
- Über den dualistischen Grundzug aller Geschichte Ebd. 8/50.
- Das Grundthema der abendländischen Geschichte. Ebd. 11/50.
- Last und Trost der deutschen Geschichte. Bielefeld, Verlag Karl Eilers, 437 Seiten, 1950.
- Zu Nicolai Berdjajews Religionsphilosophie. Evangelisch-Lutherische Kirchenzeitung 11/50.
- Die Freiheit zum Bösen als Grund der Geschichte. Ebd. 12/50.
- Europäertum als Kultureinheit. Beitrag zu Edgar Stern-Rubarth: Europa, Großmacht oder Kleinstaaterei? Bielefeld 1951.
- Ein Weg zu verantwortlichem Geschichtsunterricht. Evangelische Unterweisung 11 u. 12/51.
- Die Botschaft Eugen Rosenstocks. Zeitwende 3/52.
- Zur Grundlegung der Lehre von der Staatsallmacht in der politischen Theorie des 17.Jahrhunderts. Geschichte in Wissenschaft und Unterricht 5/52.
- Vom Analogieschluß zum hörenden Vernehmen der Geschichte. Evangelische Unterweisung 8/52.
- Vom christlichen Sinn der Geschichte. Der evangelische: Erzieher 1/2/53.
- Das Ende der Revolutionen. Zeitschrift für Religions- und Geistesgeschichte 3/53.
- Von der Bedeutung der Philosophie für die Weltorientierung der Theologen. Evangelische Theologie 8/53.
- Martin Heidegger und die Geschichte. Evgl. Theologie 7/8/53.
- Der Sprachdenker Eugen Rosenstock-Huessy. Ebd. 4/54.
- Menschenbild und Pädagogik der Jugendbewegung. Erkenntnis und Tat 4154.
- Goethe und die deutsche Gegenwart. Glaube und Forschung. Veröffentlichung des Christopherus-Stiftes in Hemer, Bd 7. Luther-Verlag Witten 1955, 181 S.
- Wiederbelebung des germanischen Ethos und der germanischen Eschatologie im 19.Jahrhundert. Evgl. Unterweisung 7/55.
- Kerygma und Sophia in der Papstrevolution. Ebd. 9/55.
- Das Pathos des Gebundenen. Friedrich Schiller als homo religiosus. Zeitwende, 7/155.
- Das Kreuz der Wirklichkeit. Ebd. 11/55.
- Das Christentum inkognito. Evgl. Welt 4/56.
- Zum Problem der Sprache. Erwägungen im Anschluß an das Sprachdenken Eugen Rosenstock- Huessys. Kerygma und Dogma 2/56.
- Eugen Rosenstock - ein Hegelianer? Archiv für Rechts-und Sozialphilosophie XLII 2/56.
- Das Individuum und der wirkliche Mensch. Evangelische Unterweisung 6/56.
- Der Weise von Four Wells. Der evangelische Erzieher 6/56.
- Einführung zu Eugen Rosenstock-Huessy: "Zurück in das Wagnis der Sprache." Käthe Vogt Verlag, Berlin 1952.
- Weltgeschichte und christliche Hoffnung. Ev. Unterweisung 2/57.
- Kurzer Entwurf einer evangelischen Erziehungslehre auf der Grundlage des Rosenstockschen Zeit- und Sprachdenkens, Ev. Unterweisung 7/57.
- Das neue Sprachdenken. Neue Deutsche Hefte 9/57.
- Religionsphilosophie und Heilsgeschichte. Zeitwende lO/57.
- Soziologie ohne Statistik. Archiv für Rechts- und Sozialphilosophie XLIV-3, 1958.
- Vorchristliche Heilsgeschichte. Ev. Unterweisung 4/58.
- Eugen Rosenstock zum siebzigsten Geburtstag. Ev. Welt 13/58, Ev. Unterweisung 7/58.
- Einführung zu Eugen Rosenstock-Huessy: "Das Geheimnis der Universität." W. Kohlhammer Verlag, Stuttgart 1958.
- Vom Stern der Erlösung zum Kreuz der Wirklichkeit. Junge Kirche 4/5/59.
- Der evangelische Lehrer und Erzieher und der Geschichtsunterricht. Der evangelische Erzieher 7/59.
- Die Zeit - ein Geschenk der Liebe. Basler Nachrichten 32/59.
- Ein neuer Kontinent des Denkens. Christ und Welt 4/59.
