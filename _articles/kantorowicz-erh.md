---
title: "Hermann Kantorowicz über Rosenstock-Huessy"
category: aussage
published: 2022-01-04
name: Kantorowicz
---

„Das Urteil, dass Du zuviel Phantasie besitzest, wird Dir bei der jüngeren Generation, die binnen kurzem die germanistischen Lehrstühle beherrschen wird, nicht schaden. Deine gelehrte Quellenforschung hält schon das Gegengewicht[,] das Übrige wird die Ausgabe des Diplovataccius tun. (Deine in einer Minute des Unmuts mir übermittelte Erklärung, von dieser Arbeit zurückzutreten, kann ich nicht einmal einer Erörterung unterziehen. Ich kenne Dich genug, um für ausgeschlossen zu halten, dass Du das Vertrauen täuschen wirst, indem Fritz Schulz und ich allein an unsere Arbeit herangetreten sind und sie nahezu beendigt haben – während sich natürlich über den Zeitpunkt der Arbeit und schlimmsten Falls über Stellung eines Ersatzmannes debattieren liesse.)“
>*Hermann Kantorowicz an Eugen Rosenstock, Freiburg am 29.1.1915, 2 Seiten, LKA EKvW 5.16, 327.*
