---
title: Die Eugen Rosenstock-Huessy Gesellschaft
---

## Ziel

Die "Eugen Rosenstock-Huessy Gesellschaft" hat sich zur Aufgabe gesetzt, die vielfachen Anregungen des Werkes von Rosenstock-Huessy in allen Lebensbereichen fruchtbar werden zu lassen. Dazu fördert sie die Herausgabe seiner Werke, veranstaltet Konferenzen und Tagungen, verwaltet das Rosenstock-Huessy Archiv und setzt sich ein für die Verwirklichung der von ihm initiierten neuen wissenschaftlichen Sprechweise in Kommunikationsformen, die in die heutige Industriegesellschaft passen. Die "Eugen Rosenstock-Huessy Gesellschaft" ist durch eine deutsch-holländische Arbeitsgemeinschaft geprägt.

## Mitgliedschaft

Die Mitgliedschaft in der "Eugen Rosenstock-Huessy Gesellschaft" kostet 40 Euro. Mitglieder empfangen in der Regel einmal im Jahr Mitteilungsblätter und zweimal einen Rundbrief. Sie werden aktuell informiert über Neuausgaben, Tagungen und wichtige Begebenheiten in Zusammenhang mit der Rezeption des Werkes von Eugen Rosenstock-Huessy. Einmal jährlich findet im Rahmen einer Konferenz eine Mitgliederversammlung statt.

## "aus der Satzung"

"Der Verein stellt sich die Aufgabe, im Geiste Eugen Rosenstock-Huessys der Pflege und Förderung der Wissenschaft zu dienen und damit die vielfachen Anregungen seines Werkes in allen Lebensbereichen fruchtbar werden zu lassen."

## Vorstand

| Vorsitzender:          | Stellvertreter:         |                         |                          |
| ---------------------- | ----------------------- | ----------------------- | ------------------------ |
| Dr. Jürgen K. Müller   | Thomas Dreessen         | Sven Bergmann           |  Dr. Otto Kroesen        |
| Vermeerstraat 17       | Insterburger Str. 13    | Bismarckstraße 10       |  Leeuweriklaan 3         |
| NL-5691 ED Son         | D-45964 Gladbeck        | D-35510 Butzbach        |  NL-2623 RB Delft        |
| Tel. 0(031) 499 324059 | Tel. 0(049) 2043 959090 | Tel. 0(049) 163 9661806 |  Tel. 0(031) 15 262 6714 |

![Der Vorstand im Bild]({{ 'assets/images/erhg-vorstand-2021.jpg' | relative_url }}){:.img-center.img-xxlarge}

| Geschäftsstelle                           |
| ----------------------------------------- |
| Eugen Rosenstock-Huessy Gesellschaft e.V. |
| Thomas Dreessen                           |
| Insterburger Str. 13                      |
| D-45964 Gladbeck                          |
| Tel. 0(049) 2043 959090                   |
