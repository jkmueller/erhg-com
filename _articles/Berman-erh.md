---
title: "Harold J. Berman über Rosenstock-Huessy"
category: aussage
published: 2022-01-04
name: Berman
language: english
---
„Above all, Rosenstock-Huessy's writings show how the experience of the second millennium of the Christian era can serve as a prophecy of the future of the human race.”
>*Harold J. Berman*

I have no doubt that one day—perhaps soon—the academic historians will discover that Rosenstock-Huessy was also one of the great pioneers in a new and significant interpretation of the history of mankind . . .  His distrust of academic pretensions to total objectivity is shared today by the best scholars, as are his basic insights into the power of language, or speech, to draw people together into a common future . . .  Rosenstock-Huessy was a prophet who, like many great prophets, failed in his own time, but whose time may now be coming.
>*Harold J. Berman (1993)*

„I think he needs to be challenged; and we need to challenge him, and not only to appreciate him.“
>*Harold J. Berman, Renewal and Continuity: The Great Revolutions and the Western Tradition, in: Eugen Rosenstock-Huessy. Studies in His Life and Thought, ed. by M. Darrol Bryant and Hans R. Huessy (= Toronto Studies in Theology; Vol.28), Lewiston/Queenston: The Edwin Mellen Press 1986, S.19-29.*
