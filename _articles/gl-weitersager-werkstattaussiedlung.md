---
title: Gudrun Elisabeth Lemm zu Die Werkstattaussiedlung
category: weitersager
order: 15
---


*Gudrun Elisabeth Lemm, Pastorin i.R in der Nähe von Weißenfels*

*an Lutz Bayer, autistisch begabt, in Sindelfingen Mai 2005*

Lutz,

vor längerer Zeit erfuhr ich über Deine Mutter, die meine Cousine ist, daß Du am liebsten Theologie studieren möchtest, und weil ich schon einige Jahre zu lesen bekomme, wie Du die Weihnachtsbotschaft aufnimmst, spüre ich, wie da Dein Herz brennt. Du bist auf lebendige Adern gestoßen, wohl schon durch Deinen Konfirmandenunterricht und Du willst Dich mit Deinen so eingeschränkten und so besonderen Kräften dem Wesentlichen widmen. Aber da gibt es frustrierende Sperren und haushohe Hürden.

Theologin bin ich geworden, nachdem ich im Sozialismus keine Lehrerin mehr sein durfte. Da wußte ich schon etwas von Enge und Verbautheit in der Kirche, aber ich wollte zu den Wurzeln zurück und dort lebendige Kraft schöpfen. Und ich habe auch heilsame Quellen aufgetan und bin noch weiter im Suchen und Graben, auch außerhalb und im Vorfeld der christlichen Überlieferung.

Gegen Ende des Theologiestudiums traf ich auf Eugen Rosenstock-Huessy, indem ich 1972 ein kleines Büchlein geschenkt bekam: Ja und Nein, Autobiographische Fragmente, von Georg Müller zusammengestellte Texte Eugen Rosenstock Huessys. Das war Pfingstwind, der mich aus meinen Gehäusen wehte, ohne daß ich verstand, wie mir geschah. Aber das war fortan meine wesentliche Orientierung, obwohl ich damit nirgends wirklich landen konnte. Erst nachdem ich im Jahr 2000 in den Vorruhestand gegangen bin, ist es mir gelungen, die Eugen-Rosenstock-Huessy Gesellschaft ausfindig zu machen, die Georg Müller vor etwa 40 Jahren in Bielefeld gegründet hat, und so treffe ich Menschen, mit denen ich davon sprechen kann und die ähnlich betroffen und fasziniert sind. Das wurde auch Zeit.

Als ich dann im Vorstand dieser Gesellschaft mitzuarbeiten begann, bekam ich als Begrüßungsgeschenk die „Werkstattaussiedlung” geschenkt, ein Buch, das ich vom Titel und seinem Anliegen her schon lange kannte, aber nicht bekommen konnte. Aus Versehen bekam ich es nun sogar zweimal und hatte sofort den Impuls, das zweite Exemplar zu Euch nach Sindelfingen zu schicken, denn bei Daimler-Benz ist es entstanden und Daimler-Benz hat diese Neuauflage unterstützt und Daimler Benz hat auch mit seinem sozialen Engagement dazu beigetragen, daß Du aufgehoben und gefördert werden konntest außerhalb Deiner Familie.

Nun lese ich in dem Buch mit der Frage, was es für Dich bedeuten kann, und merke die Hürden und Sperren. Schon daß Du dieses schwere Buch allein kaum halten und die Seiten aufschlagen kannst, ist ja ein Problem. Aber auch die Inhalte – hattest Du schon Grund, Dich mit den Lebensbedingungen der Industriearbeiter zu beschäftigen und mit den Versuchen, sie menschlich zu machen in den sozialistischen und anderen Bewegungen?

Dazu sieht sich Eugen Rosenstock-Huessy genötigt, der Jura studiert hatte und schon Professor in Leipzig war, als der 1.Weltkrieg ihn aus all dem herausriß, und der aus diesem Krieg so verändert wiederkam, daß er keine der ihm angebotenen Laufbahnen in Politik, Kirche, Lehramt annahm, sondern zu Daimler Benz ging, den Hund des Chefs ausführte und sich anschickte, in einer Werkszeitung das Gespräch innerhalb des Betriebes in Gang zu setzen, in Gang zu halten.

Dazu schreibt er in seinem Buch „Die Sprache des Menschengeschlechtes”(Lambert Schneider 1963) s.108: „Aus Objekten und Subjekten versuchte vor den Weltkriegen das gebildete Bürgertum die Wirklichkeit zu konstruieren, mit Gott und den Proletariern als Objekten, und dem Reichsbankpräsidenten und dem Handelskammerpräsidenten als Subjekten. Man sprach von den Menschen als den Objekten der Gesetzgebung, und die soziologischen Denker wie Max Weber hatten ihre Mitmenschen, einschließlich Jesus Christus, diesen charismatischen „Typ”, zum Gegenstand ihrer Forschung.

Als ich 1919 aus der Bekehrung des Weltkriegs in dieses akademische Leichenschauhaus der Herren Simmel, Max Weber und Alfred Weber zurückkehrte, rief ich die Arbeiter, zu denen ich etwas sagen wollte, zu meinen Partnern aus, auf die ich zu hören habe, ehe ich von ihnen reden dürfe[]. Chesterton wußte, daß wir in den Reigen des Lebens als"Diche" hineingetanzt werden. Im Contredance verneigen wir uns dann, in der antwortenden Tanzfigur, als Iche, und daraufhin geht der Tanz des Lebens in bunter Umschlingung durch Dich und mich, mir und Dir, Deiner, meiner, Uns und Euch, die und die weiter, bis uns Gott, der Einzige, bleibende Ich zu einem seiner Du liebend erlöst."

Ich stelle mir vor, daß in diesem kurzen Ausschnitt etwas aufklingt, was mit Deinen Erfahrungen korrespondiert: Gott sei Dank bist Du immer wieder angesprochen worden von den Deinen mit Deinem Namen, mit warmen Worten, mit guter Musik, Doch Du konntest sehr sehr lange nicht deutlich antworten, und bist fraglos immer wieder als Objekt betrachtet und behandelt worden mehr als jeder und jede von uns, und Du weißt, wie das tut. So bist Du solidarisch mit den Industriearbeitern, wenn sie in der üblichen wirtschaftswissenschaftlichen Betrachtung und der dazugehörigen Wirtschaftspraxis wie Maschinen als austauschbare Objekte behandelt werden.

Nach einer Einleitung gibt Rosenstock-Huessy uns Lesern dieser Abhandlung Gelegenheit zum Zuhören, indem er Eugen May von seinen Erfahrungen erzählen läßt. Das ist dann die Basis, auf der das Nachdenken und Vordenken gemeinsam mit dem Leser geschehen kann. Einbezogen in dieses Denken wird das Gespräch mit den Vordenkern, Vorkämpfern und die Darstellung der Möglichkeit der Werkstattaussiedlung als eine Antwort auf die Herausforderung der Industriealisierung, bei der die Lebendigkeit und die ganze Möglichkeit der Arbeiter wahrgenommen werden kann.

Als professioneller Denker und umsichtiger Praktiker hat er nicht vergessen, dieses Vorgehen auch vom bestehenden Recht her beleuchten zu lassen.

Herzhaft hat er nach Antworten gesucht und noch anderwärts Projekte ins Leben gerufen und in Schriften vorgestellt, besonders in „Der unbezahlbare Mensch” und „Dienst auf dem Planeten”.

Eugen Rosenstock-Huessy hat damit gerechnet, daß seine Umwälzung des Denkansatzes wahrscheinlich mehr als 50 Jahre brauchen wird, ehe sie Einlaß finden kann. Inzwischen sind über 80 Jahre vergangen. Die Lektionen des ersten Weltkrieges mußten im zweiten wiederholt werden und warten 60 Jahre danach noch auf gehörige Wahrnehmung. Noch immer wird Wirtschaft betrieben, als wäre das Soziale dazu ohne Belang und das Soziale wird nach Verfahren zu kitten gesucht, die aus Traditionen kommen, die ihre Tragkraft verloren haben.

Also, es ist dran, daß wir hellhörig werden, und so wende ich mich heute an Dich, ob Du nicht mit Deiner besonderen Situation und Deiner besonderen Aufmerksamkeit und Deinen besonderen Gaben in dieses Gespräch hineinkommst, Dich hineinnehmen läßt, auf daß heilsame Umkehr geschehen kann, ihre wunderbaren Wurzeln, Zweige, Blüten und Früchte treiben und uns schließlich nähren.

So grüße ich Dich herzlich in der Erwartung, daß uns Sprechen verbindet,

Deine Großtante Gudrun Elisabeth Lemm, geb. Gudrun Eipper
