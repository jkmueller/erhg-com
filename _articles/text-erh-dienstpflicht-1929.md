---
title: "Rosenstock-Huessy: Dienstpflicht? (1929)"
category: online-text
org-publ: 1929
published: 2023-07-01
---

### Dienstpflicht?
von
Eugen Rosenstock

**Übersicht:**
1. [Die Gefahr der Mythologie](#1-die-gefahr-der-mythologie)
2. [Dienstpflicht im Frieden](#2-dienstpflicht-im-frieden)
3. [Mensch und Maschine](#3-mensch-und-maschine)
4. [Heer und Fabrik](#4-heer-und-fabrik)
5. [Dienstpflicht für den Frieden](#5-dienstpflicht-für-den-frieden)
6. [Die Vollziehung des Friedens](#6-die-vollziehung-des-friedens)
7. [Wehrpflicht für den Frieden](#7-wehrpflicht-für-den-frieden)
8. [Die Gestaltung des Wehrwillens](#8-die-gestaltung-des-wehrwillens)
9. [Die neue Sitte](#9-die-neue-sitte)

Dienstpflicht - wer weiß noch etwas von ihr? Von ihrer Wirkung und von ihrem Sinn? Wem steht ein Urteil über sie zu, darüber, ob sie in die historische Rumpelkammer gehört, oder ob sie Zeichen für einen im Volke notwendigen Vorgang war?

Urteilen, besonders aber verurteilen kann nie der Fuchs die sauren Trauben. Das heißt, wem eine Gegend des Lebens verschlossen geblieben ist, eine Laufbahn oder eine Schicht, der schweige öffentlich über sie. Er hat zwar ein Recht, auch darüber sich seine Gedanken zu machen. Aber wegen der Sauerkeit der Trauben gebricht es ihm an der Autorität, über diese Dinge öffentlich für andere verbindlich zu urteilen. Über die Dienstpflicht kann daher nur noch der Kriegsteilnehmer mitreden. Die jüngeren Jahrgänge haben kein Urteil. Der Dienst in der Reichswehr läßt sich ja mit dem Volksheer von einst nicht vergleichen, weil eben niemand verpflichtet ist zum Dienst. Dienst besteht, Dienstpflicht nicht.

Doch war die Dienstpflicht etwas zu Großes und Einschneidendes, um nicht bei Freund und Gegner noch nachzuleben. Die alten Freunde und die alten Feinde der Dienstpflicht können aber eine Stellung der Nachkriegsjugend zur Dienstpflicht oder gegen die Dienstpflicht nur dadurch erreichen, daß sie eine Legende um sie wuchern lassen.

>*[nach oben](#top)*

#### 1. Die Gefahr der Mythologie

Die Dienstpflicht wird also schon heute für die Studenten, die 1908 oder 1910 geboren sind, zum Mythos. Das ist zu früh. Denn anders als die Staatsform, die neue Grenzziehung und anderes mehr widerspricht das uns diktierte Verbot der Volksbewaffnung nicht nur unserer eigenen Geschichte, sondern auch dem Aufbau aller anderen Gemeinwesen des Festlandes. Dies Verbot ist aus rein äußeren Gründen uns diktiert worden. Irgendeine geistige Auflösung der Dienstpflicht in andere Volksaufgaben konnte daher weder vorbereitet, noch vollzogen werden. Zum Mythos werden mag das Vergangene, das nicht vergehen soll. Aber ein gewaltsam amputiertes Glied des Volkslebens schon heute mythologisieren, das birgt Gefahren. Man hängt nicht ungestraft über den jungen Jahrgängen die Bilder eines Mythos auf, zu dem ihnen jedes eigene innere Verhältnis fehlt. Die Einbildungskraft die nicht durch Erfahrung gereinigt werden kann, tobt sich stets ungemessen aus. Als fast die letzten Kriegsteilnehmer von 1870/71 tot waren, da erst brachen die Franzosen zur „Revanche” auf. Jetzt erst konnte der „Mythos” seine Wirkung tun. Europa ist heute in der äußersten Gefahr, daß sich seine vielen Völker den Luxus leisten, unnachprüfbare Mythen ihrer Jugend einzuimpfen.

Daraus wird klar, daß der Generation der deutschen Kriegsteilnehmer eine Verantwortung aufliegt. Sie muß den vergeßlichen Nachfahren eine Unterredung über das Volksheer und über die Volksdienstpflicht aufnötigen damit nicht Mythen sie umgeistern. Diese Generation umfaßt etwa die dreißig Jahrgänge von 1869 bis 1899.

Dabei kann gerade der Kriegsteilnehmer davon ausgehen, daß es sich bei solchem Dienst keineswegs gerade um den Kriegsdienst handeln muß. Dienstpflicht als Volksbegründender, Volksschaffender Vorgang ist das Thema; auch Dienstpflicht im Frieden oder Dienst für den Frieden mag daher geprüft werden. Mittelpunkt ist nicht die militärische Frage, sondern die Stellung der Dienstpflicht im Volksganzen.

Meine Kenntnis von der Sache ist diese:\
Ich habe vor dem Kriege gedient und geübt, war viereinhalb Jahre draußen, davon die letzten zwei Jahre als Führer einer rein technischen Truppe; ich habe dann in einer großen Fabrik anderthalb Jahre mich umgetan. Ich habe 1920/23 eine „Akademie der Arbeit” mit aufgebaut und an zwei technischen Hochschulen gelehrt. Zuguterletzt bin ich im Jahre 1928 nach Bulgarien gefahren und habe dort ausführlich die bulgarische Arbeitsdienstpflicht kennenlernen können[^1]. Diese letzte Erfahrung hat mir also ein Land mit reiner Zivildienstpflicht gezeigt, das einzige seiner Art. Praktisch arbeite ich seit vielen Jahren an den Arbeitslagern mit, die im Laufe der Darstellung zur Sprache kommen werden.

[^1]: Über die reichen Erfahrungen dort berichtet die von den deutschen Freischaren veranlaßte Schrift von Hans Raupach, Arbeitsdienst in Bulgarien 1929.

>*[nach oben](#top)*

#### 2. Dienstpflicht im Frieden

Wenn ein Staat mit Flugzeugen, Gasen, Tanks Krieg führt, so ist das gesamte Staatsvolk von selbst Kriegsteilnehmer. Für den Kriegsfall kann kein Zweifel bestehen: wer des Staates Schutz beansprucht, muß auf seiner Seite zu Schutz und Trutz antreten. Kriegsdienstverweigerung in Ehren. Aber der Verweigerer würde es immer billigen müssen, daß ihm sein Staat dann auch seinerseits „kündigt”. Man kann nicht den guten Tropfen des Friedens dieses Staates genießen, ohne den bösen mit in Kauf zu nehmen. Und auf den Willen des einzelnen wird es künftig bei dieser Entscheidung auch herzlich wenig ankommen. Bombengeschwader nämlich nehmen auf Dienstverweigerer keine Rücksicht. In passiver Weise wird heute mithin jeder Gebietsinsasse Kombattant, Mitkämpfer.

Nicht um die Kriegsbereitschaft handelt es sich also bei der Frage der Dienstpflicht. Denn die französische Revolution hat ja einfach ihre von Talleyrand angekündigte Reise um die Welt vollbracht. Den Fürstenheeren ist das Volksheer gefolgt, und dadurch sind alle Kriegshandlungen wieder Handlungen des gesamten Volkes geworden. Die Frage der Dienstpflicht hat aber eine zweite Seite, die dem Friedensstande des Volkes zugekehrt ist. In Preußen ist das Heer gegen äußere Feinde zwischen 1815 und 1914 fünf- oder sechsmal mobilisiert worden. Von 100 Jahren haben also nur höchstens fünf Jahre die Maschine unter Dampf gesehen. Bleiben 95 Jahre Dienstpflicht im Frieden. Das Verhältnis 1 : 19 spricht für sich selbst. Unwiderleglich wird dadurch dargetan, daß es die Dienstpflicht auch als eine reine Friedensfrage gibt.

Was bedeutet das Einrücken der jungen Männer Jahr um Jahr aus allen Landesteilen in die Garnisonen? Was bedeutet die künstliche Gewöhnung des Bauernjungen an die zweifelhaften Genüsse der Stadt? Es sind die beiden wichtigsten Gebiete des täglichen Lebens, deren Abhängigkeit von der Dienstpflicht schon aus diesen beiden Fragen erhellt:

Volkserziehung und Volkswirtschaft stehen unter dem Einfluß der Dienstpflicht oder des Fehlens der Dienstpflicht. Arbeitsdisziplin, Landflucht, Wanderbewegung - ob ein landwirtschaftlicher Fortschritt technischer Art unter den Kleinbauern sich durchsetzt, ob die Volkswirtschaft ein Vorbild oder einen Hemmschuh in der Staatswirtschaft zu sehen hat, das kann weitgehend von der Dienstpflicht, auch von der Art ihrer Durchführung abhängen.

Wie die Volksgenossen miteinander verkehren bei Spiel und Arbeit, an Werk- und Feiertagen, das kann durch das Beispiel im Heere, in Kasino und Kantine, gestaltet oder verunstaltet werden bis in die letzte Werkstatt hinein. Das Selbstgefühl des einzelnen Jahrganges kann einen Aufbruch der Jugend des Volkes zu einem Ver Sacrum bedeuten. Die Bindung an ein gemeinsames Ziel, in ein gemeinsames Leben kann Wunder tun an Aufbau und Vernichtung. Die soziale Rangordnung kann zeitlebens durch das Heereserlebnis aufgeprägt werden usw.

Nach Dienstpflicht im Frieden darf man also mit Recht fragen. Auch vor dem Kriege hat man schon oft von dieser Seite der Dienstpflicht gesprochen. In einer Denkschrift von 1912 habe ich bereits Vorschläge dazu gemacht. (Siehe Picht-Rosenstock, „Im Kampf um die Erwachsenenbildung 1912- 1926”, Leipzig 1926).

Wir Deutsche müssen energischer diese Aussprache einleiten als irgendein anderes Volk. Denn wir müssen fragen, ob uns im Frieden sonst genug Dinge einigen. Nach dem Ausbrennen des Heeresorgans gilt es, über die Dienstpflicht nachzudenken, ohne Rücksicht auf den Krieg, um der Friedensordnung willen. Das deutsche Volk ist ein Heervolk gewesen. Sein Kriegsschicksal hat es geprägt, bis auf seinen Namen. „Deutsch” hieß nämlich die Sprache des fränkischen Heeres des großen Kaiser Karl. Und deshalb erwarb die Qualität des deutschen Mannes jeder, der die Sprache des Heervolkes teilte[^2].

[^2]: Dies ist das Ergebnis meiner Untersuchung „Unser Volksname Deutsch und die Aufhebung des Herzogtums Bayern”, Mitteilungen der Schles. Ges. f. Volkskunde 1928, S. I-66.

Der Heeresverband hat den einzigen Zusammenhalt gebildet, als die Konfessionen, die geographischen und Stammesgegensätze uns spalteten.

Wenn jetzt der Heeresverband entfällt, wenn ein Krieg für uns Selbstmord wird, was eint dann die geistig so auseinandergeborstenen, so widerborstigen Deutschen? Diese Frage darf nicht beiseite geschoben, sie muß gelöst werden. Gehen wir so nüchtern, so gegenwartsnah wie nur möglich an sie heran.

>*[nach oben](#top)*

#### 3. Mensch und Maschine

Das preußische Heer hat in einem bestimmten Verhältnis zum Geist des 18. und 19. Jahrhunderts gestanden. Das preußische Heer ist ja keine Naturpflanze, sondern ein Kunstprodukt. Es ist ein Instrument der Staatsräson. Die Hohenzollern entdeckten, daß sie ihre eigenen Feldmarschälle sein müßten. Als Entdeckung Friedrich Wilhelms I. und seines Sohnes tritt ihr Heeresinstrument in Zusammenhang mit den anderen Instrumenten ihrer Zeit und Wirtschaft. Dies Instrument wird aber auch in einem bestimmten Verhältnis stehen zu späteren Wirtschaftsinstrumenten.

Die Technik des preußischen Heeres und die moderne Technik - wie verhalten sie sich zueinander?

Im Jahre 1751 starb am Hofe Friedrichs des Großen der Franzose Lamettrie. Er hatte bei dem Preußenkönig eine Zuflucht gefunden gegen Verfolgung. Seine Gedanken waren es, die ihn in Gefahr gebracht hatten. Er brachte sie 1748 zu Papier in seinem Buche : L'homme machine. Der Mensch ist aufzufassen als eine überaus sinnreiche Maschinerie. Was ist denn das Kühne und Gefährliche an diesem Gedanken? Das Kunst- und Sinnreiche der Einrichtung ist gewiß keine Beleidigung für den Menschen. Nein, aber die Maschine ist klarer für den philosophischen Konstrukteur als für sie selbst. Ihr Sinn liegt außerhalb ihrer selbst. Was und ob die Maschine über sich selbst denkt, ob sie z.B. glücklich sein möchte, wird ein untergeordnetes Beiwerk, wenn sie als Maschine einen „Zweck” hat. Dieser Zweck entzieht ihr, der Maschine, die in sich selbst ruhende Bedeutung! Als Maschine bezeichnet, wird der Mensch Objekt eines außer ihm selbst verankerten Bestrebens. Der Mensch als Maschine ist eine radikale Versachlichung seines Wesens. Er wird damit aus einem Bruder zum Neutrum, zur dritten Person. L'homme machine, 1748 ein revolutionäres Buch, ist heute uns allen, Christen und Juden, Arbeiterführern und Industriellen, Politikern und Erziehern geläufig geworden. Man spricht zwar nicht wörtlich vom Menschen als Maschine. Aber der volle Gehalt jenes Gedankens ist verkörpert in einem Ausdruck, den wir ohne zu erröten, täglich gebrauchen. Die dritte Person für den Menschen hat sich restlos durchgesetzt, jene Denkfigur, die im Mittelalter, aber auch noch in den ersten Jahrhunderten der Neuzeit, vermieden wurde: Wir sprechen alle vom Menschenmaterial. Erstklassig mag dieses Material sein oder Bruch - jedenfalls wird es verbucht als Material, als Es[^3].

[^3]: Vgl. dazu die genauere Durchführung dieser grammatischen Kategorien in meinen „Die Kräfte der Gesellschaft” (= Soziologie I), Berlin 1925, und in „Angewandte Seelenkunde”, Darmstadt 1924.

Die moderne Industriewirtschaft rechnet mit Menschenmaterial und mit Arbeitskräften als mit einem ihrer Rohstoffe und Kraftquellen. Lamettrie hat dazu die erste gedankliche Figur geliefert. Der alte Fritz aber die erste Nutzanwendung. Ob der Mensch eine Maschine sei, mag offen bleiben. Sicher aber ist das Preußische Heer als eine kunst- und sinnreiche Maschine konzipiert worden. Man betrachte die Bilder der großen Revuen des friderizianischen Heeres, die schönen Stiche auf denen der Besuch Alexanders von Rußland in Potsdam 1802 - also direkt vor Jena - verewigt worden ist: die Freude am Eingliedern des Mannes in eine Heermechanik ist unverkennbar. Alles persönliche ist wie weggeätzt; Tracht des Bartes, Form der Beinkleider, die Anrede Er - alles soll den Mann zum „Rädchen” in der Kriegsmaschine machen, ähnlich wie sich heute der Arbeiter selbst willig als Rädchen im Produktionsprozeß sieht. Die Uniform hat nicht nur den Zweck der Gleichmacherei. Gewiß, sie soll den Einzelwillen auslöschen. Aber sie soll zugleich den unwiderstehlichen Tausendfüßler hervorbringen, dessen Parademarsch als das technische Wunderwerk der Präzision wirkt. Wie eine elektrische Zündung zuckt der Befehl durch Tausende. Sie reagieren, wie nur die technisch bemeisterte Natur reagiert: in blindem Gehorsam. Friedrichs Ideal war eine Staatsgestalt von der Präzision eines Uhrwerks[^4]. Ein Jahr nach dem Erscheinen des Lamettrieschen Buches hat Friedrich selber geschrieben: „Man würde in einer Sammlung vollständiger Gesetze eine derartige Einheit des Planes und genauer, ineinandergreifender Regeln gewahren, daß ein durch diese Gesetze regierter Staat einem Uhrwerk gleichen würde”.

[^4]: Rosenstock, „Vom Industrierecht” 1926, S. 124.

Am Heer hat sich diese Vorstellungswelt am ungehemmtesten auswirken können. Vor der Dampfmaschine und vor den Motoren des 19. Jahrhunderts ist das Heer in Europa als Maschine durchkonstruiert worden. Die Bewohner des Landes lebten an sich in Häusern, Sippen, Gemeinden, Zünften, Ständen und hatten in diesen eine Überlieferung und Verwurzelung, eine eigene Mundart und Gesprächsverbindung (Idiom und Dialekt) aus brüderlicher oder genossenschaftlicher Quelle. Der Söldner des Heeres tritt unter diese Gemeinschaften als neutraler Mensch, als Menschenmaterial. Der Mensch als Objekt wird erfunden. Dieser umstürzende Grundsatz steht hinter Aufklärung und Staatsräson als gemeinsamer Hintergrund. Das Heer wird ein Staat im Staate.

Es war schon vorher aus dem persönlichen Dienst des Rittertums zum zünftigen Kriegshandwerk geworden. Und wenn man sich die strengen Zunftformen der Landsknechtsfähnlein vergegenwärtigt, so drängt sich die weitere Feststellung auf, daß gerade das Handwerk des Krieges als erstes Handwerk in den großen Umwandlungsprozeß der Industrialisierung eingetreten ist. Der Mensch war die erste Natur, die sich technisch meistern zu lassen schien.

Deshalb ist das preußische Heer seitdem für alles vor der Aufklärung verharrende Menschenvolk, für den pommerschen und für den oberschlesischen Bauernknecht das große Wundererlebnis der Arbeitsdisziplinierung und der technischen Erkenntnis geworden. Hier lernte der abergläubische Litauer mit der einzigen Maschine seines Lebens, dem Gewehr, umgehen. Aber wie wurde er auch selber exakt, präzis, sauber an Koppel, Knöpfen, Stiefeln wie ein Maschinenteil.

Und nicht nur dem Landmann fährt es nun in den Rücken, so als ob er einen Ladestock verschluckt hätte. Alle Menschenart, die ihr Gepräge vor 1700 erfahren hat, wird durch die Heereserziehung in Berührung gebracht mit einem neuen, technischen Prinzip. Die Intellektuellen, die Handwerker, die Kleinbürger wurden hier ausgelüftet und lernten sich einordnen und anpassen, kehrtschwenken und in Reih und Glied sich bewegen und haben hier ein Gegengewicht gefunden gegen ihre eigene Lebensart. Das hat sich immer wieder bewährt, eben so lange wie das Preußische Heer ihresgleichen gedrillt hat.

Die einzelnen Äußerungen dieses Drills könnten als Äußerlichkeiten erscheinen. Sie sind es aber nicht, wenn man an jenes Gleichnis zurückdenkt, das Friedrich der Große selbst braucht. In diesem Gleichnis steckt nämlich das Geheimnis, das die Arbeitsweise der Europäer seitdem mehr und mehr von aller überlieferten Arbeitsart unterscheidet. Ein Uhrwerk soll konstruiert werden. Ein Uhrwerk aber ist pünktlich. Noch heut hören dort, wo auf dem Balkan etwa Europa aufhört, unsere europäischen Zeitbegriffe auf. Der Orientale, der vorkapitalistische Mensch, ruht in der Zeit und strömt mit der Zeit. Er lehnt es ab, sich die Zeit so gegenüber zustellen, daß er "pünktlich" werden muß, daß er seine Arbeit, seine Bewegungen, seine täglichen Verrichtungen auf Punkt Neun oder Punkt halb Zehn bemißt und errechnet. Das stehende Heer ist der erste weltliche menschliche Verband, der auf die Minute tätig wird. Die Erziehung zur Pünktlichkeit, vom 7<sup>15</sup> Antreten bis zum Zapfenstreich trägt eine klösterliche Askese in das Werksleben, die vorher unbekannt war. Der Zeitsinn des Europäers, der Reiz der Pünktlichkeit fehlt den älteren Epochen. Jedes Rechnen mit der Minute zerstört etwa im Orient das Vertrauen. Besuche, Besprechungen usw. haben dort kein Ende und keinen Anfang. Die Pünktlichkeit also ist die erste Sprosse einer Leiter, die über das vegetative Hinleben durch die Zeit des Jahres mit seinen hundertsiebzig Feiertagen hinaufgebaut worden ist bis zur Stoppuhr, dem Fahrplanbetrieb, der Präzision des Taylorschen Maurers, der Rationalisierung des Ostertermines usw.

Auf die moderne Welt hin erzieht das Heer den traditionellen Menschen. Es ist eine Rationalisierungsanstalt größten Stils. Der Staat ist so zum ersten Unternehmer im seelischen Sinne der Arbeitsdisziplinierung geworden. Er stellt
die Mühle auf, in der die moderne Menschheit zuerst gemahlen worden ist.

>*[nach oben](#top)*

#### 4. Heer und Fabrik

Aber hier höre ich gleich die Einwürfe gegen diese These: wie kann man die steifen unerträglichen Paraden von 1720 in einem Atem nennen mit einer Großkraftstation von 1925? In der Tat. Wer hat nicht vor dem Kriege die Abneigung des aktiven Heeres gegen technische Fortschritte feststellen müssen? Die Artillerie und der Train spielten Reiterei, die Intendanturbeamten spielten Offiziere; hingegen wurden der moderne Handel und Wandel, Industrie und Forschung vom Heere aus als minderwertige Schleichwege des Lebens empfunden. Das bedeutet aber nur: man blieb im Heere bei der Technik, die man beherrschte, stehen. Die Kunst der Menschenbehandlung hatte Pate gestanden bei der Knetung des Rekrutenmaterials. Sie blieb daher die eigentliche Ingenieurkunst bei der Rekrutierung des Materials.

Damit blieb das Heer seinem Geburtstag im 18. Jahrhundert verhaftet. Und neue Menschenströme, die später in das Bett der Geschichte eingemündet sind, haben zu ihm kein natürliches Verhältnis mehr gewonnen.

Denn alle die Menschen, deren Schicksal von dem Eindringen des Maschinenwesens in immer entlegenere Teile der Naturkräfte abhängt, müssen im Heere etwas Rückständiges sehen. Kräfte werden hier vergeudet. Noch wird vom Menschen das gefordert, was längst der Automat leistet. Je moderner eine Maschine, desto mehr verkörpert die in ihr noch zugelassene Arbeitskraft ein Stück Aufsicht und Leitung über die anderen Kräfte des Prozesses. Im Heere ist der Einzelmensch sozusagen das Grundelement, aus dem die ganze Maschine gebaut ist. In einer Maschinenhalle ist der Mensch inmitten all der rein stofflichen Moleküle aber ein Elektron, ein bewegendes Element.

Anders wie auf dem Exerzierplatz und bei der Parade kämpft die moderne Armee der Arbeit gegen die Natur in gelockerter Kampfordnung. So wie die rhythmische Gymnastik mit ihren federnden Bewegungen das steife Turnen überwunden hat, so ist die Arbeitsdisziplin am laufenden Band eine viel weniger äußerliche als die in Reih und Glied der Kompagnie. Pünktlichkeit, Präzision, Sauberkeit sind hingegen im rationalisierten Betrieb noch größer als beim Heere. Vor allen Dingen wird viel weniger Zeit verwartet und vertan.

So hat im Kriege Schritt für Schritt die technisch-industrielle Lösung des federnden und schwingenden Arbeitsgefüges den alten Drill verdrängen müssen, um Höchstleistungen zu erzielen. Der Kampf um eine rein zweckmäßige Uniform gehört in dies Ringen hinein.

Das Heer steht also in Zwitterstellung zwischen Agrarverfassung und Industriewirtschaft eines Volkes. Es bedeutet Rationalisierung für die vorkapitalistischen Volksteile, Kraftverschwendung und Rückständigkeit für die bereits tiefer in die Industrialisierung verstrickten Klassen. Ein rein agrarisches Volk erfährt durch die Erziehung im Heer einen Fortschritt in der Richtung auf moderne Arbeitsordnung. Ein Industrievolk wird durch die Einstellung ins Heer auf einer Stufe zurückgehalten, die es im übrigen längst hinter sich gelassen hat, auf der Stufe der Manufaktur sozusagen, einer auf Menschendisziplinierung beschränkten Technik.

Diese Zwitterstellung erklärt die Verschiedenheit der Gefühle, die das Heer in den verschiedenen Schichten der Bevölkerung erregt. Solche Gefühle sind eben nicht in oberflächlichen Meinungen, sondern in tiefen geschichtlichen Tatsachen verwurzelt.

Ein in die Industrie immer stärker hineinwachsender Verband muß den Heeresdienst als Opfer empfinden. Ist die Disziplinierung bereits an dem Ort und der Stelle, an der heut jeder Mann seine Friedensarbeit verrichtet, besser durchgeführt als im Heer, so wird dies Opfer sogar sinnlos.

Rationalisierung in der Industrie, Technisierung des Haushaltes, Industrialisierung der Landwirtschaft sind aber heut die Schlagworte des Tages. Alle die Worte lassen sich zusammenfassen in das eine: Militarisierung des Werktags, der friedlichen Arbeit. Ein stehendes Heer neben die Arbeitsarmee zu stellen, wird also um so viel sinnloser, als das Heer der Arbeit selbst die Präzision des Heeres gewährleistet oder überbietet.

Die Nutzanwendung für die „Dienstpflicht im Frieden” ist nun zu ziehen. Schon das Opfer des Kriegsdienstes im Frieden ist nur für einen immer kleiner werdenden Teil des Volkes sinnvoll geblieben als Erziehungseinrichtung. Eine zivile Dienstpflicht kann sich nicht einmal auf das Pathos des Schutz und Trutz berufen.

Ihre Arbeitsmethoden würden also rückhaltlos unter die Lupe genommen werden. Sie müßten den Vergleich mit der modernsten Industriearbeitsdisziplin aushalten, um nicht als Raubbau zu gelten. Ehemalige Reserveoffiziere zu dieser Einrichtung zu verwenden, erscheint eben deshalb als undenkbar. Sie würden die technische Rückständigkeit des Heeres verewigen. Es ist wohl kein Zufall, daß die Arbeitsdienstpflicht bei uns nicht vom Fleck gekommen ist. Jedermann wußte, daß für ihren Aufbau die vielen tausende von Offizieren der Armee in Betracht kamen. Und so sehr hat ja das Heer durch die Reserveoffiziere abgefärbt auf alle anderen Lebensbereiche, daß selbst Ingenieure, Lehrer oder Juristen wahrscheinlich in dem ersten Jahrzehnt nach dem Weltkrieg sich mehr auf den Offizier in sich zurückbesonnen hätten als auf den Zivilisten, wenn sie eine Arbeitsdienstpflicht hätten organisieren sollen.

Dienstpflicht im Frieden kann nicht als Kern die Arbeitsdisziplin vermitteln wollen. Denn dieses Umwegs über eine besondere Veranstaltung von den Ausmaßen des Heeres bedarf es in unserer Industriewirtschaft nicht.

Wenn es eine Pflicht geben kann, die auch im Frieden Dienst für das ganze Volk und in dem Ganzen des Volkes für jeden fordert, so muß diese Pflicht anders verankert sein als in der Schicht der Arbeitserziehung und Arbeitsdisziplin. Die Züchtung der in die kapitalistische Wirtschaftsweise hineinpassenden Arbeitskraft oder des friedlichen Staatsbürgers ist kein genügender Antrieb, der mit dem alten Motiv „Mit Gott für König und Vaterland” den Vergleich aushalten kann. Der Weg von der Militärdienstpflicht zur Arbeitsdienstpflicht ist ein Weg vom Sturmtrupp zur Armierungskompagnie. Mit anderen Worten: Aus einer heiligen Tat wird eine ökonomische Tatsache, aus einem irrationalen Opfer eine rationale Leistung. Dazu aber ist das nüchterne tägliche Arbeitsleben selbst da. Die Industriemenschheit mit ihrer Qualitätsarbeit noch eigens in Armierungskompagnien zu stecken, damit sie hier in der primitivsten Form „arbeiten lernen”, wäre ein lächerlicher Luxus und eine seelische Qual.

Die Dienstpflicht im Frieden ist aber damit, daß sie nicht technischer Art sein kann, genauer: daß bei ihr das Technisch-Ökonomische nicht im Mittelpunkt stehen kann, noch keineswegs „erledigt”.

Wir haben bisher die technische Seite des Preußischen Heeres in den Vordergrund schieben müssen, um klar zu werden über die Wirkung, die es auf die moderne technische Welt ausüben mußte. Wir haben gesagt: trotz, nicht wegen seiner technischen Rückständigkeit hat man es geliebt und bewundert. Also weil man den vollen Einsatz des Mannes, den kriegerischen Mut hochhielt, nur deshalb konnte das Heer Pflanzstätte einer volksverbindenden Gesinnung sein, so wie wir es 1914 erlebt haben.

>*[nach oben](#top)*

#### 5. Dienstpflicht für den Frieden

Ist für einen solchen Einsatz der Volksgenossen auch heut Platz und Bedürfnis vorhanden?

Um das zu ermitteln, müssen wir an die technisch höchstgebildeten Stellen gehen, in die Großbetriebe, in die Fahrplanwerke und nun fragen, was hier an Ort und Stelle - ohne den Umweg eines Heeres als einer besonderen Volkseinrichtung - gerade durch die Technik verloren geht und verschlissen wird.

Ich möchte da eine alltägliche Redensart aufgreifen, um den Tatbestand deutlich zu machen. Man spricht heut in der Wirtschaft gern von eingefrorenen Krediten. Das Wort ist zu schön, um es der reinen Wirtschaft zu lassen. Die Betriebe der Arbeit, nicht nur die Unternehmungen der Wirtschaft sind heute mit eingefrorenem Kredit belastet. Das Vertrauen ist eingefroren. Die Unternehmerfunktion, die Kraft zum kaufmännischen Wagnis ist wenig geeignet, gleichzeitig dem selben Manne das Vertrauen als Arbeitgeber zu erwerben. So besteht kein Anlaß, in dem Auf und Ab der Gründungen und Auflösungen an die einzelnen Leute in der Leitung persönliche Gefühle zu verschwenden. Sie tun das auch selber ihrerseits nicht, schon weil Sie keine Zeit dazu haben.

Die Pflanze Vertrauen verdorrt schon durch die Willkür und den Zufall der menschlichen Beziehungen innerhalb der industriellen Sachherrschaft. Die verdorrt noch schneller dann, wenn die dem Betrieb immerhin verbliebenen Lebensfunktionen unterbunden werden. Ein gebrochener Akkord kostet ein Jahr Vertrauen. Aber kaum ein deutscher Unternehmer wird beschwören können, daß bei ihm kein Vertrauen durch Akkordbruch zerstört werde.

In diese eingefrorene Menschenwelt steckt man heute gern junge Akademiker als Praktikanten, Volontäre, Werksstudenten. Es wird sogar diese praktische Arbeit im Betriebe oft als Allheilmittel für ihre soziale Schulung gepriesen. Dagegen ist sicher nichts zu sagen, daß jeder die technische Arbeit kennenlernt. Aber alle diese jungen Männer kommen - das muß man nicht vergessen - in eine eingefrorene Welt, in eine erkrankte Arbeitsordnung, die festgefroren ist aus Mangel an Vertrauen. Sie ist nicht beweglich, nicht heiter, nicht so reibungslos tätig, wie es bei einem anderen Aggregatzustand der Arbeitskräfte sein könnte. Die eingefrorenen Kredite der Industrie führen zu einem steifen „Gesellungs”zustand ihrer Produzenten. Wo Mißtrauen ist, da ist Schwerbeweglichkeit die Folge. Denn nur in übersehbaren, genau bekannten Lagen verschärft sich das Mißtrauen nicht. Jede Veränderung, jeder "Neue" kann Gefahr bringen, Gefahr der Entlassung, eines Konkurrenten, einer Prämienkürzung, einer Bloßstellung.

Kann man den eingefrorenen Arbeitskräften des Betriebes zutrauen, daß sie die Jugend unseres Volkes richtig anlernen werden? Wird man nicht umgekehrt die Unternehmer, Ingenieure, Betriebsleiter, Geister und Vorarbeiter erst einmal herausholen müssen aus ihrer Welt und sie bekannt machen müssen mit einer wirklichen Ordnung des Vertrauens?

Jede Ordnung, in der Vertrauen herrscht, ist eine friedliche Ordnung. Der Friede ist aber so wenig wie der Krieg ein Zustand, sondern ein Beginnen, Unternehmen und Vollziehen.

Wir haben die seltsame Vorstellung, als ob "im Frieden" der Frieden sich von selber einstellte. "Im Krieg" - das wissen wir alle - da beginnt und vollzieht ein jeder in eigener Person den Krieg. Jeder trägt sein Scherflein bei zur Kriegführung, um den Krieg zu erringen. Krieg "führt" man. Und den Frieden? Der Friede ist genau so zerbrechlich wie der Krieg. Im Krieg zerbricht ein Volk an seinen Feinden, wenn es nicht kämpft. Im Frieden zerbricht ein Volk an sich selbst, wenn es nicht kämpft. Wenn der Krieg „geführt” wird, so kann vielleicht vom Frieden gesagt werden: Wir müssen ihn vollziehen.

>*[nach oben](#top)*

#### 6. Die Vollziehung des Friedens

Friede wird vollzogen wo wir Menschen einbeziehen in unsere Sphäre. Dies geschieht tagtäglich überall dort, wo wir Menschen aufnehmen in einen Kreis. Es geschieht aber diese Aufnahme in einem doppelten Verfahren tagtäglich: erstens durch Erziehung jüngerer Menschen. Diese werden damit den schon im Leben stehenden Volksgliedern nachgezogen, das Kind den Eltern, der Schüler dem Lehrer, der Lehrling dem Meister. Erziehung ist der Friedensvollzug zwischen Älteren und Jüngeren. Die Alten nehmen die Jungen durch Erziehung auf in ihren Kreis. Die Erziehung ist gelungen, wenn sie das leistet. Sie kann das ja nur dadurch, daß sie unbedingtes Vertrauen erzeugt in den Jungen und in den Alten; in den Jungen, damit sie sich willig ziehen und nehmen lassen, in den Alten, damit sie freiwillig hineinziehen und hinaufnehmen.

Es läuft aber noch ein zweiter unerläßlicher Weg des Friedensvollzuges im Volk. Auch er muß so unablässig beschritten werden wie der zwischen Alten und Jungen durch Erziehung. Man kann diesen zweiten Weg vielleicht kurz zusammenfassen als den der Ernennung. Ernennen heißt ja aufnehmen in eine öffentliche Verrichtung. Wer ernannt wird, der wird betraut. In der Erziehung vertrauen Alte und Junge aufeinander. In der Ernennung wird dem Neuling ein Amt im Volke anvertraut. Wer aber kann nur ernennen? Einer, der selber schon einen Namen hat! Der Kreis der namhaften Volksgenossen erweitert sich also durch Ernennung, durch „Kooptation”. Und schließlich ist es die Öffentlichkeit des Volkes selbst, die Männer und Frauen ihres Vertrauens „namhaft” macht und sie damit zu öffentlichen Charakteren prägt.

Vom Reichsminister bis zum Steuerassistenten „vollzieht” sich mithin eine ununterbrochene Aufnahme in das Vertrauen. Tausendfach wird sie alltäglich betätigt. So selbstverständlich ist dieser Weg der vertrauensvollen Aufnahme, der „Betrauung”, daß wir selten uns klar machen, es sei nur dieser ununterbrochene Strom von weitergeleitetem Vertrauen imstande, auch nur für eine Stunde unser Leben zu tragen. Und doch ist es so. Vollzöge sich dieser Vorgang nicht in allen Bereichen des öffentlichen Lebens täglich und stündlich, so würde der Friede uns nicht als „Zustand” erscheinen können. Wo nicht ununterbrochen ernannt wird, da stockt die Umwandlung der unbekannten Kräfte in bekannte, namhafte und öffentliche Kräfte. Da würde sich unverzüglich das Gefüge des Volkes auflösen. Denn wie im Aufziehen und Erziehen die Familie, so erkennt sich das Volk im Benennen der Menschen des Volkes in ihren Ämtern, Würden und Ehren. Wo niemand mehr ist, der befugt ist, aufzunehmen, hinzuzunehmen, namhaft zu machen, da ist das Volk am Ende mit seinem Frieden. Da herrscht das Chaos und der Bürgerkrieg mehrerer Völker. Jedes dieser Völker kann dann in sich selbst Frieden stiften. Ein einziges Volk kann nur dann wieder aus ihnen werden, wenn sie in Ernennung und Erziehung wieder einträchtig verfahren.

Dieser Friedensvollzug durch Erziehung und Ernennung ist das ABC des Volkslebens. Um dieses kümmert sich aber die moderne Gesellschaft nicht. Dort, wo heute gearbeitet wird, wird weder erzogen noch ernannt. Man nimmt die Leute zu Dutzenden, Hunderten hinein in den Betrieb und entläßt sie ebenso wieder, möglichst namenlos. Der Namenlose ist zu keinem Friedensvollzug verpflichtet. Denn er ist mit nichts betraut. Nur wo Vertrauen gewährt und genommen wird, sind wir im Bereich des Friedens.

Die Gesellschaft, die Industrie, der Betrieb zehren von dem im Volk anderweit erzeugten Frieden. Sie haben aber keine Zeit, ihn selbst ihrerseits zu erzeugen oder zu erneuern. Sie sind eine Betriebsform mit eingefrorenen Krediten.

Überall, wo Kinder von Alten erzogen werden, in Schule und Lehre, wächst Vertrauen. Lehre und Schule stehen aber heut neben dem Betrieb. Im Betrieb ist dafür keine Zeit.

Im Betrieb stören alle Gedanken, die dem Produkt und seiner Herstellung entzogen und auf den Produzenten verwendet werden sollen. Die Rationalität oder die „Vergeistung” der Betriebe bedeutet, daß die Menschen in ihnen zu Arbeitskräften fixiert, daß sie auf Teiläußerungen ihres Wesens festgelegt werden. Den ganzen Kerl mit seinen Launen, Freuden und Leiden kann kein Betrieb brauchen. Es muß am Schnürchen gehen, wie beim Militär. Damit ist aber zugleich über den Frieden im Betriebe das Wichtigste gesagt: Der Friede muß von außen in den Betrieb hineingeliefert werden. Der Betrieb verbraucht den Frieden. Er erzeugt ihn nicht selbst. Um das zu verstehen, muß man freilich den Frieden ernst nehmen als eine Macht der Volkszucht und Volkserziehung, ernster als er gewöhnlich genommen wird. Frieden hat der Mensch, der vertrauensvoll in der Gemeinschaft der Menschen so leben darf wie er wirklich ist, wie er auch mit sich selber ist. Der Friede macht die Masken des Lebenskampfes überflüssig; er gestattet das Visier zu lüften, die Rüstung abzulegen, das zweite, unstarre Gesicht aufzusetzen, das jedermann im Kreis der „Seinen" aufsetzt. Der Betrieb ist heut für niemanden der Kreis der „Seinen”. Der Betriebsleiter redet die Leute besser nicht „Kinder” an - was früher der Patriarch konnte - der Betrieb ist Fremdraum, in dem man gespornt und gestiefelt auftritt.

Der Friede wird dort nicht vollzogen; in den Werkhäusern der Menschen darf heute niemand so leicht sein wie er ist. Man hat keine Zeit für solch individuelles Wesen, es sei denn, daß man von vornherein damit gerechnet und es speziell engagiert und einkalkuliert hat.

Mit anderen Worten: Im Betrieb hat man zu bleiben, der man ist.

Dagegen müssen wir uns wehren. Jeder von uns lehnt sich auf gegen die Gefahr der Erstarrung, in die uns heut Amt und Funktion durch ihren Betriebscharakter bringen. Der Betrieb ist heut das Militär und der Schützengraben der modernen Gesellschaft. Deshalb wehren wir uns gegen seine Friedlosigkeit. Und deshalb gibt es eine Wehrpflicht für den Frieden, eine Pflicht, sich für den Frieden zu wehren!

>*[nach oben](#top)*

#### 7. Wehrpflicht für den Frieden

Wie wehrt man sich für den Frieden? Nun, der Friede ist Vertrauen zu anderen und ist Freiheit zu sich selbst. Beides ist ein Griff und Eingriff in den Mechanismus der Zeit, die damit aus ihren festen Terminen und „Termini”, aus ihrer feststehenden Tagesordnung verrückt wird. Im Krieg muß alles furchtbar schnell gehen. Die Beschleunigung ist deshalb ein Kennzeichen der modernen kriegerischen Wirtschaft. Die Beschleunigung ist das wichtigste Kampfmittel. Zum Frieden muß man sich Zeit nehmen. Diese Zeitnahme, diese Verlangsamung ist die Vorbedingung für alles weitere.

Für alles, mit dem ich in Frieden leben will oder muß, bin ich genötigt, mir Zeit zu nehmen. Wo Frieden vollzogen werden soll durch Erziehung, da müssen wir uns dieser Erziehung widmen. Wie langweilig ist oft diese Aufgabe, wie unendlich langsam. Aber ihre Langeweile und ihre weite Sicht zeigen gerade, daß sie Friedensarbeit ist, um die Jugend heranzuziehen an das alte, schon geformte Leben. Auch zum zweiten Vollzug des Friedens, zum „Ernennen” muß man sich Zeit nehmen und sich „widmen”. Denn wir müssen Dinge und Menschen erkannt haben, ehe wir sie beim rechten Namen nennen können. Und zur Erkenntnis gehört die innere Zeit, die innere Zeit, die das konzentrierteste Leben ist. Goethe hat ja diese Verdichtung des Lebens zur Kraft des Friedensvollzuges, zur Widmung und Eingabe, unübertrefflich als die zweite und dritte Potenz des Lebens gedeutet, wenn er ausruft: „Denn das Leben ist die Liebe und der Liebe Leben Geist” . . . . .

>*[nach oben](#top)*

#### 8. Die Gestaltung des Wehrwillens

Niemand ist einander so fern wie die Mietsparteien eines Großstadthauses oder die Mitarbeiter in einer Fabrik. Homo homini lupus hieß es früher, als die Menschen einander noch gefährlich schienen : der eine ein Wolf für den anderen. Ich glaube, nicht auf der Gefahr, sondern auf der Eiseskälte der Entfremdung liegt heut der Ton: der eine - so würde ich sagen - ist dem andern ein Eskimo.

Die letzten Jahrhunderte haben die Eskimos und Feuerländer entdeckt und mit viel Liebe die Fidschiinsulaner und Dschagganeger aufgestöbert. Die Entdeckungsreisen sind vollzogen und haben diese Völkerschaften erfolgreich einbezogen in unsere Welt.

Die Entdeckungsreisen, die übrig bleiben, sind die zu den neuen Eskimos, zu unsern nächsten Nachbarn. Der Anreiz ist gering. Sie sehen uns und einander verzweifelt ähnlich. Ich möchte vermuten, daß Liebe sie aufstöbern und die Glut unter der Asche und in der fast erstarrten Schlacke neu entfachen muß. Oder der Bürgerkrieg beginnt. Denn unaufhaltsam zerfallen wir dann in getrennte Völker.

Es ist auch keine Sorge, daß diese Entdeckungsreise langweilig oder ungefährlich sein könnte. Denn gerade die Berührung mit Eskimos und Peruanern hat unseren Blick für den Reichtum der menschlichen Natur geschärft. Wie groß ist Gottes Tierpark und Paradeisos! Eine Vorbedingung hat freilich die Entdeckungsreise. Man muß den anderen und Verschiedenen entdecken wollen, nicht seinesgleichen. Müller VI und Schulze IV aufzustöbern, das kann man nicht von mir verlangen. Nur die Gegensätze, Feinde, Widersacher und Gegner können das Leben des Friedenssuchers entzünden.

Die Wehrpflicht für den Frieden verlangt also die Gesellung des Verschiedenen und Entgegengesetzten. Sie stellt sich damit der heutigen Verbandswirtschaft und Organisationsarbeit entgegen. Alle Organisation und Organisationsarbeit hat als Grundlage: gleich und gleich gesellt sich gern. Unsere Wehrpflicht ist schwerer zu organisieren. Denn sie hat nur die Aufgabe, die Gegensätze in solche Reichweite voneinander zu bringen, daß sie aufeinander wirken können. Das Auseinandergefallene, räumlich Benachbarte in eine innere Zeiteinheit zu tauchen, damit es seine Urgegnerschaft nicht in sich zu vergraben braucht, sondern an den Tag leben kann, das ist die Aufgabe.

Überall in Deutschland vollzieht sich heut deshalb der Weg von der Organisation zur Gruppe. Die Gruppe ist der Bund der Gegensätze. Damit tritt sie in Widerspruch zur Organisation. Die Organisation uniformiert. Die Gruppe hingegen ist ein Gleichnis der Ehe. Denn Sie stiftet Beziehungen zwischen Gegnern, die an sich wie Feuer und Wasser zueinander stehen.

Infolgedessen ist es die Vorbedingung des Gesellschaftsfriedens, daß man die Unversöhnlichkeit der Gegensätze anerkennt. Schwarz kann nicht weiß, Weib kann nicht Mann werden. Das Raubtier soll nicht Haustier, der Unternehmer
soll nicht Pensionsempfänger werden. Wir brauchen alle Urkräfte des Menschen weil sonst nie Frieden werden kann. Denn im Frieden muß jeder sein können wie Gott ihn geschaffen hat. Verstümmelungen und Amputationen anerschaffener Triebe müssen sich am Frieden rächen. Wenn in der großen Friedensschau der Bibel verheißen ist, daß Löwe und Lamm beieinander friedlich lagern so wäre diese Verheißung wertlos, wenn der Löwe dann zum Lamm geworden wäre! So stellen sich die Bleichsüchtigen gern das Ende der Menschheit vor. Aber ich glaube, daß der Löwe Löwe und das Lamm Lamm sein wird, und daß Menschen es sein werden, die als vollkommene Löwen und vollkommene Lämmer diesen Endzustand zu verwirklichen haben.

Wenn der Schmied Wasser und Feuer erfolgreich zu verbinden gelernt hat, wenn Mann und Weib sich nicht auffressen vor lauter Liebe, sondern trotz der Urgegnerschaft gesunde Kinder miteinander kriegen, so ist es wohl auch des Erfindergeistes würdig, Wasser und Feuer unter den Berufen, Klassen und Charakteren der Menschen so zu binden, daß sie als Wasser und Feuer wirken dürfen. Diese Last des Andersartigen auf uns zu nehmen, ist die Brüderlichkeit der Zeit. Es ist eine Last, die es zu stemmen gilt. Aber Sie zu stemmen, kann eine Lust sein.

Und sie wird nur aus Lust gestemmt werden, nicht aus Moral und Pflicht.

Es gibt deshalb keine Wehrpflicht für den Frieden, sondern nur einen Wehrwillen aus Herzenslust. Der Wehrwille für den Frieden wird also nicht auf Gesetz und Recht aufgebaut werden können. Die Formen staatlichen Zwanges kommen für ihn nicht in Betracht.

Sondern es wird dieser Wehrwille auf Freiwilligkeit und Brauch aufruhen müssen, auf Beispiel und Sitte in der Gesellschaft[^5]. Eine zwingende Sitte, die unserer Uniformierung entgegenarbeitet - ist sie im Entstehen?

[^5]: Über die Bindekraft der Sitte siehe meine [„Ökonomie der Sitten”]({{ '/text-erh-symbol-sitte-1929' | relative_url }}) in der Zeitschrift „Die Erziehung” 1929, Märzheft.

>*[nach oben](#top)*

#### 9. Die neue Sitte

Unter der Jugend ist es bereits Sitte, den gegnerischen Bund zu „treffen”. Das Treffen ist eine Begegnung, in der sich die Gegner messen wollen, um ihres eigenen Lebens um so stärker innezuwerden. Das Treffen ist Selbstverstärkung. Älter geworden, werden die Jungen diese Arten der Begegnung gewiß nicht aufgeben. Sie brauchen die Gegner, um sich selbst zu bekräftigen in einer an sich kraftlosen, uniformierten Welt.

Die Lager der Jugend sind Wehrlager von Gegnern, die sich aneinander entzünden zur Einkehr zu sich selbst, zur höchsten Lebendigkeit. Ohne diese Lager überfiele die Trägheit und die Unentschiedenheit unvermeidlich die auf sich selbst Angewiesenen. Der Mut des Herzens und die Tapferkeit des Geistes können nur im Kampf mit dem Gegner erwachen. Der Geist, der von diesen Lagern ausgeht und allein ausgehen kann, ist der Funke, der aus der Reibung des Feuersteins hervorbricht. Er ist wenig umfassend, aber er ist echt.

Was führt nun Gegner zusammen, damit es solche echte Funken geben kann? Echte Not. Nur die Not, nicht irgendeine Theorie schafft heut dem Wehrwillen seine Form. Wir hier schreiben die Theorie eines Vorganges, der unser ganzes Volk längst ergriffen hat. Man sehe nur um sich. An die Stelle der sachlichen Kongresse und Tagungen, deren Formen veraltet sind, treten heut die persönlichen Treffen und Freizeiten. Wir sind am Erfrieren und Einfrieren - ich erinnere an die eingefrorenen Kredite und brauchen echtes Feuer. Die Not allein kann daher aus uns harten Kieseln Funken schlagen, wenn sie uns dicht genug zusammenführt.

Junge oder Alte, die ein Brandmal, einen Notstand gemeinsam ernst nehmen und ihr ganzes Wesen einsetzen, um dieser Not auf den Leib zu rücken, aus denen bricht der Funke.

Aber damit sie Funkenträger, Funkenerzeuger werden können, ist noch eine zweite Vorbedingung zu erfüllen. Alle geistige, wirtschaftliche, religiöse und Charakterverschiedenheit wird zugestanden. Weder der Klassenkampf noch die Entfremdung der Generationen noch Rassengegnerschaft brauchen wegdisputiert zu werden. Wir erschrecken vor keiner Tatsache. Aber wir überschätzen auch keine. Nur eine Bedingung ist zu stellen: eine Schwäche muß der homo sapiens zugeben, um des guten Willens für den Wehrdienst teilhaft zu werden. Denn diese Schwäche allein macht ihn tragfähig für den Funken. Funken braucht ja den toten Stein. So braucht der neue Brauch der Begegnungen den Erdhaften, den erdverhafteten ganzen Menschen. Ein Erdensohn muß es sein, der dem anderen begegnet, Art gegen Art, Schlag gegen Schlag. Keine Geister, keine Denker, keine schönen Seelen, keine freundlichen Gemüter sollen sich begegnen sondern festgefügte Leiber. Allein jener Adam den als Erdenkloß Gott doch zum Menschen machte, kann Träger der neuen Friedensarbeit sein.

Daraus entspringt eine zwingende Form der neuen Treffen. Die Erdensöhne können nicht nur miteinander debattieren, nicht nur Gruß und Handschlag miteinander tauschen, sie können nicht nur miteinander singen oder tanzen oder telephonieren. Sie müssen als Erdenmenschen aufeinander stoßen, Stein wider Stein, in ihrer Dürftigkeit und Bedürftigkeit. Dazu aber ist es notwendig, daß sie miteinander arbeiten und miteinander essen.

Arbeitsgemeinschaft und Tischgemeinschaft ist die Vorbedingung dafür, daß jeder auf diesem Grunde so aus sich herausgeht, wie er innen ist.

Die trostlose Art prinzipieller Diskussionen in unserem Volk zeigt ja nur, wie erloschen sein geistiges Leben ist. Jeder redet allgemeine Weltanschauung, niemand spricht aus, was in ihm lebt. Sie wissen nicht einmal, daß nur das ausgesprochen zu werden verdient, was in uns lebt. Sie glauben, das was man gehört oder was man erwünscht oder was man sich gedacht hat oder wofür man schwärmt, müsse immer wieder deklamiert und rezitiert werden. Geistiges Leben ist anderer Art.

Gemeinsame Arbeit, geselliges Leben, verschiedene Gedanken, das sind drei Grundregeln der neuen Wehrwilligkeit für den Frieden.

Aus diesen drei Linien entsteht das magnetische Feld für die beiden Kräfte des Vertrauens und der Freiheit.
Bei einer Ausbreitung dieser Sitte der Arbeitslager wird es praktisch auch zu beachtlichen wirtschaftlichen Leistungen kommen. Die Lager, an denen ich teilgenommen habe, wollten durchaus redliche und wirtschaftliche Arbeit leisten und haben sie geleistet[^6]. Äußerlich kommt daher ein solches Lager den Baracken einer Zivildienstkompagnie nahe. Aber sein Platz im Volksganzen ist ein anderer. Nicht Staatsfrohn, sondern Volkswehr ist der Sinn. Die Zerstörung der Volkskraft durch die Gesellschaftsordnung wird wettgemacht. Ein Heer von Freiwilligen tritt wieder an die zentrale Stelle, aus der heraus sich unser Volk als Volk erneuert. Denn ohne ein Heer kann Deutschland nicht bestehen. Eine leibhafte Gestalt muß das Volk annehmen, das mehr bleiben soll als ein Schulbuchmythos und eine industrielle Arbeitsarmee. Aber Deutschland braucht heut ein volksschaffendes Heer da, wo Preußen vor zweihundert Jahren ein staatsfassendes Heer aufgestellt hat. Auch das sogenannte Volksheer ist ein Staatsheer gewesen, das sich nur aus dem Volk erneuert hat. Die Erneuerung des Volkes hatte ihm nicht obgelegen.

Wir sind einen weiten Weg von der Staatsmaschine über die mechanisierte Gesellschaft zum Kampf um den Volksfrieden gegangen in unseren Ausführungen; aber unser Thema der Dienstpflicht steht so im Mittelpunkt unseres Schicksals,
daß es erst aus allen diesen Sehwinkeln heraus vollständig sichtbar wird.

Mehr sagen hieße den neuen Wehrwillen eher schwächen als fördern. Aber soviel durfte wohl ein Jahrzehnt nach dem Zusammenbruch unserer Wehrverfassung gesagt werden. Ja, es muß gesagt werden, um die öffentliche Meinung auf den unverrückbaren Mittelpunkt selbstvergessenen Dienstes wieder hinzuweisen, der seit dem Untergang des Volksheeres der Wiederentdeckung harrt.

[^6]: Über die Arbeitslager u. a. Fritz Klatt, Gestaltung der Freizeit, 1929. Rosenstock, Schlesische Monatshefte, März 1929 „Volksgruppe” Löwenberg, Schlesien 1928.


[„Dienstpflicht” als PDF-Scan](http://www.erhfund.org/wp-content/uploads/221.pdf)

[zum Seitenbeginn](#top)
