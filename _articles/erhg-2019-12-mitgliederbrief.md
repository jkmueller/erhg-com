---
title: Mitgliederbrief 2019-12
category: rundbrief
created: 2019-12
summary: |
  1. Einleitung                                       - Jürgen Müller
  2. Einladung zur Jahrestagung 2020                  - Jürgen Müller
  3. Der Zeitpunkt von ERHs „Die Sprache des Westens” - Sven Bergmann
  4. Wie übersetzen?                                  - Eckart Wilkens
  5. Kreisau-Bildband erschienen                      - Andreas Schreck
  6. Ohne Erinnerung keine Zukunft                    - Thomas Dreessen
  7. Bücher aus dem Nachlass von Elfriede Büchsel     - Andreas Schreck
  8. Walter Husemann und Andreas Möckel verstorben    - Andreas Schreck
  9. Adressenänderungen                               - Thomas Dreessen
  10. Hinweis zum Postversand                         - Andreas Schreck
  11. Jahresbeiträge 2019                             - Andreas Schreck
zitat: |
  >Ich scheine das tägliche Leben der Völker und ihrer Glieder als Spiegelungen der Trinität verfolgt zu haben. Die Sprachen der Einzelnen wie der Nationen, die Zeiten der Liebenden und der Hassenden, die Geschichte der Reiche, der Kirche, der Gesellschaft habe ich für Spiegelungen der göttlichen Dreieinigkeit angesehen. So setze ich einmal kurz nieder, wohin mich das Ungenügen der akademischen Wissenschaften erst getrieben und dann geführt hat. Diese Wissenschaften der Philologie, der Geschichte, des Rechts, der Philosophie habe ich als noch immer alexandrinisch erfahren. Da wo die Physiker längst christlich geworden sind in ihren Theoremen, sind die vier eben genannten „Geisteswissenschaften” immer noch höchstens mit dem Nilwasser bei Alexandria getauft. Noch immer sind sie nämlich vorchristlich. Wenn ich die Liste meiner eigenen Entdeckungen durchgehe, so werden alle diese bis heut von den Zünftigen unterdrückt und totgeschwiegen, die auf die Trinität von Vater, Sohn und Geist hinführen. Ich habe z. B. den Sinn des ägyptischen „Ka” und des germanischen „deutsch”, die Zeitlichkeit, den „Interims-Charakter” des Rechts und die Feindschaft von Arbeitszeit und Lebensarbeit aufdecken dürfen, ich habe Paracelsuś Lehre von den fünf Lebenssphären entfaltet, den Kreuzcharakter der Zeiten habe ich dem saublöden Gerede von Raum und Zeit entgegengehalten. Alle diese Einsichten stammen aus einem und demselben methodischen Ansatz, und die Fruchtbarkeit dieses Ansatzes ist seine beste Beglaubigung. Die akademische Wissenschaft ist unfruchtbar, und sie dreht sich im Kreise. Sie muß das, weil sie das Verhältnis von Zeit und Geschichte und Sprache auf den Kopf stellt. Um sich über die plebs zu erheben, denken die Idealisten ja nur für Denkende.
  >Ich aber spreche zu Hörenden und höre auf Sprechende. Und das tun die Akademiker nun einmal nicht, denn sie glauben damit ihren Rang einzubüßen. Nun haben sie gerade, weil sie unter Hitler nur gedacht haben, ihren Rang eingebüßt.
  >*Rosenstock-Huessy: SPRACHE – ZEIT – GESCHICHTE, Vorwort zu: Ja und Nein, 1968*
---

## Eugen Rosenstock-Huessy Gesellschaft e.V.

{{ page.zitat }}

**Vorstand/board/bestuur:**\
*Dr. Jürgen Müller (Vorsitzender);*\
*Thomas Dreessen; Andreas Schreck; Dr. Eckart Wilkens; Sven Bergmann*\
*Antwortadresse: Jürgen Müller: Vermeerstraat 17, 5691 ED Son, Niederlande*\
*Tel: 0(031) 499 32 40 59*

***Brief an die Mitglieder Dezember 2019***

**Inhalt**

{{ page.summary }}

### 1. Einleitung
Die sozialen Medien des neuen Internets ermöglichen grenzenlose Kommunikation unter geringem Aufwand für jeden. Die zugrundeliegende Datenarchitektur sammelt Daten durch eine Vielzahl von Sensoren und Aufnahmegeräten, verteilt sie in Echtzeit weltweit, speichert sie für zeitversetzte Anwendung und ermöglicht die Automatisierung vieler gesellschaftlicher Vorgänge. Daraus entstehen neue Kräfte, alte werden transformiert und ihre Konstellation verändert sich.
Wie gelingt in dieser Wunderwelt technischer Kommunikation ein größeres Miteinander und ein neuer Friede? Rosenstock-Huessy verweist uns auf die Sprache und auf Beispiele aus der Geschichte, wie dies in früheren Zeiten gelang. Die Jahrestagung 2020 soll unter dem Thema: „Der verborgene Kontinent des Hörens” stehen.
>*Jürgen Müller*

### 2. Einladung zur Jahrestagung und Mitgliederversammlung 2020
Wir möchten Sie zur Jahrestagung und zur Mitgliederversammlung 2020 herzlich einladen. Das Thema wird „Der verborgene Kontinent des Hörens” sein und zwei Texte Rosenstock-Huessys im Zentrum haben. Der methodische Teil aus „Die Hörspur” wird am Beispiel des Textes „Die Sprache des Westens” verdeutlicht.
Wie seit einigen Jahren wieder treffen wir uns an Palmarum (3.5. April 2020) im Haus Am Turm, in Essen-Werden. Wir bitten Sie sich diesen Termin vorzumerken.
>*Jürgen Müller*

### 3. Der Zeitpunkt von Eugen Rosenstock-Huessys Aufsatz „Die Sprache des Westens”
Gegenüber dem „Historiker” sieht Eugen Rosenstock-Huessy den „Soziologen” insofern in einer überlegenen Position, als dieser den Zeitpunkt, wann gesprochen oder geschrieben wird in seine Betrachtung einbezieht. Betrachten wir seinen Aufsatz von 1955 über die „Sprache des Westens” unter diesem Gesichtspunkt, ergeben sich folgende Hintergründe. Der Aufsatz erscheint in der offiziellen Verbandszeitschrift der Geschichtslehrer Deutschlands (GWU, 6. Jg., Heft 1, Januar 1955). Eugen Rosenstock-Huessy richtet sich also an die Berufspraktiker der Vermittlung von Geschichte in Deutschlands Schulen und Universitäten. Der Aufsatz erscheint mitten in der sich weiter verschärfenden Ost-West-Konfrontation zwischen Korea-Krieg und Kuba-Krise, in der quantitativ mit Streitkräften und qualitativ mit nuklearer Munition aufgerüstet wird. NATO und Warschauer Pakt schließen ihre Reihen. 1954 hat Deutschland die offizielle Einladung zur NATO erhalten. Truman und Stalin werden in dem Aufsatz genannt, sind zu diesem Zeitpunkt allerdings nicht mehr im Amt, bzw. schon gestorben. 1958 nimmt Herausgeber Georg Müller den Aufsatz in den Sammelband „Das Geheimnis der Universität” auf. Unter den Aufsätzen und Reden Eugen Rosenstock-Huessys aus den Jahren 1950 bis 1957 steht der Aufsatz dort unter der Überschrift: „Die Geschichte ist ein Heilmittel der menschlichen Gesellschaft”. Das „Frieden schließen” bleibt auch 2019 aktuell.
>*Sven Bergmann*

### 4. Wie übersetzen?Zu Bedeutung und Übersetzung von „Speech and Reality”
*Lehre beginnt erst da, wo der Stoff aufhört, Stoff zu sein, und sich in Kraft verwandelt – in Kraft, die nun selber den Stoff, und sei es um das bescheidenste Wort vermehrt, und so aus jener behaupteten Unendlichkeit des Stoffes erst eine Wahrheit macht.*
>*Franz Rosenzweig, Die Bauleute*

Als ich nach dem Erscheinen der „Sprache des Menschengeschlechts” die aus dem Englischen übersetzten Stücke las, hielt ich den Text für die Version, die Eugen Rosenstock-Huessy selber so formuliert hatte. Im Vorwort steht ja:
*Die letzten zwölf Jahre brachten mir die Freundeshilfe des Direktors der Bodelschwingh-Schule in Bethel bei Bielefeld, Dr. Georg Müller. Die englischen Manuskripte wanderten zu ihm. Er wandte die große Mühe einer ersten Übersetzung auf, um sie immer wieder in die öffentliche Diskussion hineinzuziehen. Er hat mir damit den unersetzlichen Dienst geleistet, in einer über die Junggrammatiker der siebziger Jahre kaum hinausgeschritten Welt – das Münchener Symposion der Sprache von 1960 ist absolut vor-nietzisch – auf die neue Fragestellung unermüdlich hinzuweisen. Er und ich hoffen, daß damit das Klima für die verlangsamte Publikation dieses Werkes besser vorbereitet ist.*
Als ich aber damit anfing, die Stücke aus dem 1970, also sieben Jahre später erschienenen Band „Speech and Reality” zu übersetzen, merkte ich, daß die deutschen Übersetzungen im Sprachbuch mangelhaft sind – vor allem berücksichtigen sie nicht die verschiedene Satzmelodie im Englischen und Deutschen, trotten vokabelmäßig daher wie eine Cäsar-Übersetzung in Untersekunda, und vor allem: Entstehungsgrund und Datum erscheinen so verborgen, daß das wirkliche Leben Rosenstock-Huessys von 1933 bis 1950 in Amerika, als des Kriegsgegners Hitlers, gar nicht merklich werden kann.
Bis auf das erste Stück „In Defense of the Grammatical Method” von 1939 und 1955) erscheinen sämtliche Stücke von „Speech and Reality” im zweiten Teil des Sprachbuchs: Wie wird gesprochen:
- *Articulated Speech 1937*
- *The Uni-versity of Logic, Language and Literature 1935*
- *Grammar as Social Science 1945*
- *How Language Establishes Relations 1945*
- *The Listener's Tract 1944*
- *The Individual's Right to Speak 1945*
- *Vom Artikulieren 1937 S. 312*
- *Die Einsinnigkeit von Logik, Linguistik und Literatur 1935, S. 525*
- *In Beziehung Treten 1945, S. 419*
- *Schizo-Somatik oder Wie die Grammatik die Sozialleiber spaltet, S. 432*
- *Hörer und Sprecher – Aufhören und Lossagen 1944, S. 339*
- *Des Individuums Recht auf Sprache 1962, S. 704*

Bei der Übersetzung konnte ich die Stellen einfügen, die der Autor in die deutsche Übersetzung eingefügt hat – was natürlich den Eindruck verstärkt, er hätte den ganzen Text so gestaltet.
Es ist ein Musterbeispiel für die Lehre des Zeitigens: dem deutschen Leser von 1963/64 wird schonend erspart, den Ernst der Existenz von Rosenstock-Huessy in Amerika wahrzunehmen. Die Wirksamkeit aber gerade des richtigen Zeitpunkts des Sprechens – daß das Sprachbuch eben kein Buch ist, sondern die Versammlung solcher zur rechten Zeit gesprochenen Worte mit bestimmten Hörernwird so matt, daß ich mich endlich nicht mehr wundere, daß die gehörige Resonanz auf dieses Alterswerk ausgeblieben ist.
Nach dem Englischunterricht zu urteilen, den ich in den ersten Gymnasialjahren Mitte der fünfziger Jahre des vorigen Jahrhunderts genoß, von Lehrern, die nicht einmal wirklich Englisch reden konnten, ist diese Vertaubung durch Übersetzung nicht erstaunlich. Und doch muß dem aufgeholfen werden, mehr als fünfzig Jahre nach dem Erscheinen der „Sprache des Menschengeschlechts”.
>*Eckart Wilkens*

### 5. Kreisau-Bildband erschienen
Die Bundeszentrale für politische Bildung hat kürzlich einen sehr schön gestalteten Bildband mit dem Titel "Kreisau/Krzyzowa 1945-1989-2019” herausgegeben. Die Autoren Waldemar Czachur und Gregor Feindt stellen den Werdegang des Projekts Kreisau anhand zahlreicher Dokumente und Zeitzeugnisse sowie herausragender Fotos dar. Auch Eugen Rosenstock-Huessy und einige Mitglieder unserer Gesellschaft kommen zu Wort. Selbst für Kreisau-Kenner dürften die zauberhaften Luftbilder ein "Muss" sein! Das 250seitige Buch
(ISBN 978-3-7425-0472-2) ist für sagenhaft wenige 7 Euro zu bestellen bei:
Bundeszentrale für politische Bildung c/o Versandservice GmbH Verbindungsstr. 1
D-18184 Roggentin
>*Andreas Schreck*

### 6. Ohne Erinnerung keine Zukunft: Helmuth James von Moltkeermordet: 23.1.1945 in Berlin Plötzensee
Wir leben in einer Zeitenwende und die ist jetzt weltweit in der Öffentlichkeit angekommen in der Resonanz auf Greta Thunberg's Fridays for Future. Es ist wie bei Jona (3.Kapitel): Wir haben gehört sagt auch die neue EU Kommissionspräsidentin Ursula von der Leyen.
Beim Propheten Jona entdecke ich Hoffnung für unsere Zeit: Das Volk von Ninive kehrt um, darauf kehren auch die Fürsten um! Und Ninive erhält eine neue Chance. Gibt es Hoffnung? Verheissung?
Benjamin Stora rief uns 2015 zu: „Wenn wir den Terrorismus überwinden wollen, müssen wir unsere Geschichte neu erzählen: die Frankreichs und die Europas – mit den Anderen!”(TTT November 2015)
Mit seiner Laudatio auf Norbert Lammert anlässlich der Verleihung des Leo Baeck Preises, erneuert Navid Kermani unsere Erinnerung an den Kreisauer Kreis und besonders Helmuth James von Moltke als „Quellgebiet für politisches Denken” [^1]. Der Geist von Kreisau habe zutiefst das Grundgesetz geprägt, zBsp. im Recht auf Widerstand, in der positiven Religionsfreiheit (Art 4) und der Subsidiarität.

Warum ist für Norbert Lammert und uns Helmuth James von Moltke wichtiger als Stauffenberg? Es sind ihre verschiedenen Ziele und die zugrundeliegenden Erfahrungen. „Die anderen, die Sozialisten, sogar ein Mann wie Leber, haben immer gedacht, man muß die deutsche Ehre verteidigen. Mein Mann sagte: „Die deutsche Ehre ist sowieso im Eimer, um's deutlich zu sagen. Da ist nichts mehr zu machen. Aber wie wird's später?” Dies Übernationale, das haben die anderen alle nicht so ganz mitgemacht.”[^2] „Anwalt der Zukunft!” wurde er genannt. [^3] Für dieses „menschheitliche”- so Helmuth James von Moltke im Brief vom 10.1.1945 an seine Frau, ist er zum Tode verurteilt worden.[^4]

Eugen Rosenstock-Huessy sagte dazu: „Stauffenberg war ein Patriot, ein Nationalist ähnlich wie Ballin. Er kam aus derselben Vorstellung der absoluten Größen, dass die Nation nämlich das Edle verkörpern solle. Gefallen ist er mit dem Ausruf „für das heilige Deutschland”. … Sein Ziel blieb aber dieses enge Deutsche Reich. … Die innere Zukunft ist nicht bei Stauffenberg; die Zukunft ist eher bei dem Grafen Moltke. Denn der hat gesagt: „Bringt den Hitler nicht um, daran liegt gar nichts; der muß seinen Krieg verlieren. Aber nach dem Krieg muß es Menschen geben, die sich von diesem Unrecht lossagen können; und wir müssen uns heute schon lossagen, damit uns unsere Lossagerei morgen geglaubt wird. … Im Augenblick ihrer Hinrichtung waren Helmuth James von Moltke und seine Freunde die einzige legitime Regierung Deutschlands. Denn Verbrecher können keine Regierung konstituieren, und in dem Augenblick, als diese Menschen fielen, herrschte das Verbrechen, soweit es herrschen kann. Es mag zwar machtmäßig herrschen, aber dadurch herrscht es noch längst nicht rechtmäßig. Daran, dass wir diesen Opfern des Nazismus die Legitimität zusprechen, hängt die Epoche für die nächste europäische Generation – nicht nur für die Deutschen. Diese Opfer des Nazismus bilden nämlich die legitimen Namen, die über dieser Epoche als wirkliche Menschheit stehen. … Niemand sah im Jahr 1944 diese Opfer. Und doch haben sie die Grundlagen dafür geschaffen, dass es heute überhaupt noch lohnt, von einer deutschen Nation zu reden. Wenn es diese Gerechten in Sodom und Gomorra damals nicht gegeben hätte, hätte Deutschland seine Geschichte endgültig verloren, denn in jeder Zeit muß die Bestimmung aus Vergangenheit und Zukunft geschehen; es muß die Gegenwart Gottes bezeugt werden, sonst bricht die Zeit ab. Wo die Zeit abbricht, da rollt ein Gebilde in den Abgrund der Vergessenheit und kann nie wieder heraufgeholt werden. … sie haben aus bloßen Abläufen von ihnen persönlich verantwortete und erlittene Ereignisse gemacht! So nämlich werde die Zeit richtig, weil ihre Stimmen nun den Sinn der Zeit bestimmen werden!”[^5] [^6]

Moltke selber sah sehr klar, dass er und die anderen Kreisauer hingerichtet werden würden weil sie die kurze Zeit des Naziregimes durchschauten und die Zeit danach vorbereiteten: „Sie [die Nazis td] haben eben begriffen, dass in Kreisau die Axt an die Wurzel des N.S. gelegt werden sollte und dass nicht nur wie bei Goerdeler eine gewisse Fassadenänderung vorgenommen werden sollte. Es ergibt sich auch aus einer Bemerkung von Freisler in der Sitzung, die ungefähr so war: Wenn Stauffenberg die Triebkraft im militärischen und Goerdeler im organisatorischen Sektor war, so war es Moltke im geistigen Sektor.”[^7] Schon 1941 erkannte er die Sinnlosigkeit allen Handelns und beschreibt dies als seine „Wandlung … Die Erkenntnis, das das, was ich tue, sinnlos ist, hindert mich nicht es zu tun, weil ich viel fester als früher davon überzeugt bin, dass nur das, was man in der Erkenntnis der Sinnlosigkeit allen Handelns tut, überhaupt einen Sinn hat. …”[^8] Das markiert noch einmal die Differenz zu Stauffenberg.

In seiner Biografie benannte der US-Diplomat George Kennan Moltke als „eine so große moralische Figur und zugleich einen Mann mit so umfassenden und geradezu erleuchteten Ideen, wie mir im Zweiten Weltkrieg auf beiden Seiten der Front kein anderer begegnet ist.” Er nannte ihn einen „der wenigen protestantischen Märtyrer unserer Zeit.”[^9].[^10]

Können wir in der Zeitenwende hören, was Moltkes Lehrer Rosenstock-Huessy sagt: „Nun ist es heute fast unbekannt, dass all unser Hören und Sagen, dass Wahrheit, dass Hervorrufen Tätigkeiten der Gattung sind, kraft deren wir in unsere stummen tierischen Selbst die Bestimmung des Menschengeschlechts und unsere Bestimmung innerhalb des Geschlechts hineingerufen, angesagt und hervorgerufen werden. Ihr Sprecher spricht immer für unsere Gattung, so wie jeder Wähler für den Staat wählt. Der cartesianische Traum von den denkenden und mündigen Individuen verhüllt das. … Nur wem durch Mitmenschen widersprochen wird, nur dessen Widersprüche bleiben heilbar. … bevor Deutschland zerstört, zerteilt und widerlegt wurde, hatte es jeden Widerspruch in seinem Inneren ausgeschlossen, hatte es das Volk des ewigen Widerspruchs, Israel, ausgerottet und in dieser Wut zu seiner eigengesetzlichen Widerspruchslosigkeit seinen Untergang selber hervorgerufen… Der Planet wird nur dann Planet aus bloßer Welt werden, wenn die weltliche Entwicklung durch liebende Teilnahme aufgehalten und aufgehoben werden kann.”[^11]

 Zwei Vorschläge Moltkes zu dieser liebenden Teilnahme will ich an dieser Stelle skizzieren.[^12]
  1. Innere Gebundenheit, Freiheit und Verantwortungsgefühl des Einzelnen Menschen wachsen in den kleinen Gemeinschaften. „Ich gehe davon aus, dass es für eine europäische Ordnung unerträglich ist, wenn der einzelne Mensch isoliert und nur auf eine große Gemeinschaft, den Staat, ausgerichtet ist. Der Vereinzelung entspricht die Masse; aus einem Menschen wird so ein Teil der Masse. Gegenüber der großen Gemeinschaft, dem Staat, oder etwaigen noch größeren Gemeinschaften wird nur der das rechte Verantwortungsgefühl haben, der in kleineren Gemeinschaften in irgendeiner Form an der Verantwortung mitträgt, andernfalls entwickelt sich bei denen, die nur regiert werden, das Gefühl, dass sie niemandem Verantwortung schuldig sind als der Klasse der Regierenden.”[^13] Das neue EU Projekt Global exchange on Religion in Society (Sept.2019) anerkennt die Leistungen der kleinen Gemeinschaften im Moltkeschen Sinne und will sie fördern.
  2. „Jeder muss die Möglichkeit haben, etwas für die Gemeinschaft Nützliches zu tun. Dazu muss ihm die Gelegenheit gegeben werden… Ich denke dabei an folgende Funktionen: freiwillige Arbeitslager, Gemeindeverwaltung, Verwaltung sozialer Einrichtungen, ehrenamtliche Kirchenverwaltung”.
  Dazu die Kreisauerin Freya von Moltke: „Dieser Gedanke, Menschen mit Spannungen, die nicht einer Meinung sind, miteinander sprechen zu lassen, das kommt von den Löwenberger Work Camps.”[^14]
  Konkret sieht sie die Aufgabe „die Umwelt heilen!”: „Für jeden Menschen ist die Umweltfrage eine entscheidende Frage. ..Vielleicht wird's gerade noch gelingen oder jedenfalls noch lange weitergehen. Ewig hält diese kleine Kugel sowieso nicht. Aber das dauert noch lange.” Ich höre ihr Wort als Testament: „Es kommt auf dieser Welt darauf an, Kompromisse zu machen zwischen diesen verschiedenen Menschenbildern. Wir müssen miteinander leben lernen. Das ist eigentlich die Aufgabe der kommenden Zeit. … Wenn zwischen zwei Menschen Frieden gemacht wird und viele Menschen zwischen zwei Menschen Frieden machen, dann wirkt sich das auf das ganze aus.”[^15]
Merksatz:
*Unsere Geschichte werde erzählt,*
*Unsere Zukunft werde verheissen*
*Die Gegenwart werde erkämpft.*
*Das Tote mag man wissen.*[^16]
>*Thomas Dreessen*

[^1]: Navid Kermani: Laudatio auf Norbert Lammert 2018, in: Morgen ist da, (2019) S.283ff
[^2]: Freya von Moltke in dies.: Die Kreisauerin, S.43
[^3]: HJM nach Freya von Moltke, J.Frisby und M.Balfour.
[^4]: Briefe 1939-1945, S.11+ 603
[^5]: ERH: Unterwegs zur planetarischen Solidarität S.135-38
[^6]: AAO S.418
[^7]: HJM Abschiedsbriefes 531, 21./22.1.1945
[^8]: HJM Brief vom 11.10.1941, Briefe an Freya 39-45 S. 300
[^9]: HJM Briefe an Freya 1939-45, 1988, S. 11//GK: Memoiren eines Diplomaten, 1968, S.127f
[^10]: siehe Brief vom 10.1.1945
[^11]: ERH:Unterwegs zur planetarischen Solidarität, S.163f,167
[^12]: Brakelmann S.159f (in Denkschrift HJM 24.4.1941
[^13]: HJvM: Die kleinen Gemeinschaften cf Brakelmann Der Kreisauer Kreis S.111ff
[^14]: Die Kreisauerin S.108
[^15]: Die Kreisauerin S.155
[^16]: Soziologie II, Die Vollzahl der Zeiten, S.21

### 7. Bücher aus dem Nachlass von Elfriede Büchsel
Im Herbst 2019 verstarb in Hannover Frau Dr. Elfriede Büchsel mit 96 Jahren. Sie war der Eugen Rosenstock-Huessy Gesellschaft seit Jahrzehnten verbunden, hat zum Beispiel Williams James` A moral equivalent of war ins Deutsche übersetzt und nun verfügt, dass ihre Bücher an Interessenten weitergegeben werden sollen. Die Bücher befinden sich bei Andreas Schreck und werden gegen eine Spende nach eigenem Ermessen zugesandt.
Die Liste besteht aus:
- A Guide to the Works of Eugen Rosenstock-Huessy by Lise van der Molen
- Out of Revolution, Autobiography of Western Man
- The Origin of Speech
- Die Sprache des Menschengeschlecht, Bd.1-4
- Im Kreuz der Wirklichkeit Bd.1-3
- The Multiformity of Man
- Das Wagnis der Sprache
- Des Christen Zukunft
- The Christian Future
- Mitteilungsblätter der ERHG 2007 / 2001-2004
- Jahrbücher der ERHG, Stimmstein 1-5 und 13
>*Andreas Schreck*

### 8. Walter Husemann und Andreas Möckel verstorben

Kürzlich haben uns zwei weitere Mitglieder verlassen, die unsere Gesellschaft seit über 50 Jahren begleitet und geprägt haben.
Walter Husemann, geb. 5. Februar 1932, verstarb in Münster am 15. Dezember 2019. Walter Husemann hat sich um unser Archiv verdient gemacht, indem er sowohl den Briefwechsel zwischen Rosenstock-Huessy und Georg Müller abgeschrieben hat als auch den mit Sabine Leibholz (Mitteilung von Gottfried Hofmann).
Prof. Dr. Andreas Möckel (30. Januar 1927 bis 11. Dezember 2019) ist an den Folgen eines Sturzes in seinem Haus verstorben. Ein Nachruf auf ihn, der zwischen 1986 und 1992 als 1. Vorsitzender die Gesellschaft erneuert und über weitere Jahre als Vorstandsmitglied gewirkt und bis zuletzt als Berater zur Verfügung gestanden hat, wird im nächsten Mitgliederbrief veröffentlicht.
>*Andreas Schreck*

### 9. Adressenänderungen
Bitte senden sie eine eventuelle Adressenänderung schriftlich oder per E-mail an Thomas Dreessen (s. u.), er führt die Adressenliste. Alle Mitglieder und Korrespondenten, die diesen Brief mit gewöhnlicher Post bekommen, möchten wir bitten, uns soweit vorhanden ihre Email-Adresse mitzuteilen.
>*Thomas Dreessen*

### 10. Hinweis zum Postversand

Der Rundbrief der Eugen Rosenstock-Huessy Gesellschaft wird als Postsendung nur an Mitglieder verschickt. Nicht-Mitglieder erhalten den Rundbrief gegen Erstattung der Druck- und Versandkosten in Höhe von € 20 p.a. Den Postversand betreut Andreas Schreck. Der Versand per e-Mail bleibt unberührt.
>*Andreas Schreck*

### 11. Jahresbeiträge 2019

Die Mitglieder, die nicht am Lastschriftverfahren teilnehmen, werden gebeten, den Jahresbeitrag in Höhe von 40 Euro (Einzelpersonen) auf das Konto Nr. 6430029 bei Sparkasse Bielefeld (BLZ 48050161) zu überweisen, soweit dies noch nicht geschehen ist.
Nach Einführung des einheitlichen Zahlungssystem SEPA mit der Internationalen Kontonummer (IBAN): DE43 4805 0161 0006 4300 29 SWIFT-BIC: SPBIDE3BXXX ist diese „IBAN”- Nummer auch für Mitglieder in den Niederlanden und im ganzen „SEPA-Land”, in das sich sogar die Schweiz hat einbetten lassen, ohne Zusatzkosten nutzbar.
>*Andreas Schreck*

[zum Seitenbeginn](#top)
