---
title: "Feico Houweling: Internationale Kontakte"
category: bericht
created: 2014
---
![Feico Houweling]({{ 'assets/images/Feico-Houweling.jpg' | relative_url }}){:.img-right.img-small}

Seit einigen Monaten bin ich auf der Suche nach Rosenstock-Interessierten in Ländern wie Frankreich, England und den skandinavischen Ländern. Im Dezember 2008 habe ich Briefe verschickt an 11 Adressaten, die ich von Mark Huessy bekommen hatte. Aber von diesen hat erst nur Frank Davidson, der einen Teil des Jahres in Paris wohnt, reagiert.

Im Frühjahr bin ich aber in Verbindung gekommen mit weiteren zwei Personen. Erstens Bogumil Jargolak, Freund von Chuck Hartman in den Vereinigten Staaten und Pfarrer der Evangelisch-Reformierten Kirche in Wroclaw (Breslau). Der andere ist Svein Loeng, Dozent an der Hochschule Nord-Trøndelag (nördlich von Trondheim in Norwegen). Er hat ein schönes Buch geschrieben über die Andragogik mit einem Kapitel über Eugen Rosenstock-Huessy. (***Andragogik***, bestilling@laringsforlaget.no. ISBN 978-82-93003-00-7. Preis NKr. 260,-).

Jetzt informiere ich mich einfach bei jedem, ob man jemanden kennt in diesen Ländern, der interessiert sein könnte. Namen und Adressen sind herzlich willkommen. Ich kann schreiben, aber schöner wäre es vielleicht, wenn Sie diese Personen bitten, Kontakt mit mir aufzunehmen. Meine Absicht ist, erst einmal eine Namensliste zusammenzustellen. Später können wir dann noch sehen, wie wir die Kontakte herstellen können. Der amerikanischen E-Mail-Liste (erhlist@mulistserv. millikin.edu) und dem Verein Respondeo habe ich die gleiche Frage gestellt.

Da ist es bemerkenswert, daß außerhalb Deutschlands und der Niederlande immer häufiger auf Rosenstock-Huessy Bezug genommen wird, zum Beispiel in

- ***Ferdinand Cuvelier: De bejegening tussen jou en mij***, Pelckmans, Kapellen, Be, 2002 ISBN-13:9789028929227. (www.pelckmans.be)
- ***Holland: Millennium. The End of the World and the Forging of Christendom*** (found by Otto Kroesen).

Auch hier gibt es vielleicht noch Möglichkeiten für neue Kontakte.

Seit 2007 arbeite ich als berufsmäßiger Erzähler von Geschichte, basierend auf den Arbeiten Rosenstock-Huessys. Über das Internet sind an jedem Arbeitstag sogenannte ***Podcasts*** (Hör-Erzählungen) zu bekommen von je fünf Minuten. Die Sprache ist Niederländisch, und ich produziere jetzt eine Reihe über *die niederländische Geschichte mit der Halbrevolution von 1581-1618* im Zentrum. Die Podcasts sind frei zu bekommen via [www.feicohouweling.nl](https://www.feicohouweling.nl)
