---
title: "Franz Rosenzweig über Rosenstock-Huessy"
category: aussage
published: 2022-01-04
name: Rosenzweig
---

„Without Eugen, I would never have written The Star of Redemption.”
>*Franz Rosenzweig, in a letter to Rudolf Hallo (1920)*
