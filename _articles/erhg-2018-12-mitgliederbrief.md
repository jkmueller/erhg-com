---
title: Mitgliederbrief 2018-12
category: rundbrief
created: 2018-12
summary: |
  1. Einleitung                                  - Jürgen Müller
  2. Bearbeitungen                               - Eckart Wilkens
  3. Entdeckungen                                - Thomas Dreessen
  4. Jahrestagung und Mitgliederversammlung 2019 - Jürgen Müller
  5. Adressenänderungen                          - Thomas Dreessen
  6. Hinweis zum Postversand                     - Andreas Schreck
  7. Jahresbeiträge 2018 und 2019                - Andreas Schreck

zitat: |
  >An den Stämmen der Germanen war ja die Zeit nicht annähernd in dem Maße erfüllt wie an den atomisierten Individuen, Bürgern, Sklaven und Provinzialen, der universalistischen antiken Stadt-kultur. … Das aber bedeutet, daß zur Zeit Sylvesters des anderen im Jahre 1000 sächsische Rechtsgedanken durchschlagen; Sippe, Geblüt und Haus, Hof und Gefolgschaft verdrängen die letzten Vorstellungen von jus publicum und privatum Roms. Das Kirchengut gilt als bloßes Inwärtseigen der weltlichen Herren- und Fürstenhöfe. In die Kirche brechen Laienbischöfe, Laienäbte, Adel und Erblichkeit ein. Die rückläufige Welle, die seit 400 die antike Kultur zersetzt, erreicht erst im 10. Jahrhundert ihren Höhepunkt. Erst im 10. Jahrhundert ist die Antike tot. … Jetzt muß die Kirche selbst die Arbeit verrichten, die ihr die weltliche Stadt bis dahin geleistet hatte: Individuen, atomisierte Einzelne zum Bau ihrer geistigen Ordnung herauslösen aus den Banden des Bluts.\
  >Wir Heutigen stehen ja wieder der Zersetzung und dem Synkretismus des ersten Jahrhunderts näher als der Stammes- und Hausverfassung des elften. Deshalb befremdet heut das Mißtrauen gegen die Laien, der Kampf gegen ihre Artgefühle, der die Wiedergeburt der Kirche begleitet, so daß es schon 1170 heißt: Ecclesia nihil dicitur nisi clerici. (Unter Kirche ist nur der Klerus zu verstehen.) Dieser Eifer ist nur aus der Bedrängnis der Kirche durch den artgebundenen Geist der getauften Stämme zu verstehen.\
  >Das Jahr 1000 zeigt also sozusagen den vorchristlichsten Moment der Welt seit Christi Geburt! Im Jahr 1000 ist nur noch der Klerus christlich. Es ist kein Zufall, daß sich damals der Glaube im Morgenlande auf das Athosgebirge und seine Mönchsrepublik zurückzieht.
  >Erst wenn wir das Vorchristliche in der äußerlich christianisierten Welt der letzten zwei Jahrtausende rücksichtslos mit Namen nennen, kann heut das Christentum zu neuer Wirkung kommen. Bis heute hat er gedauert, dieser Widerstand des Natürlichen, des Nationalen, des Heidnischen.
  >*Eugen Rosenstock-Huessy, Die Hochzeit des Krieges und der Revolution, 1920*
---

## Eugen Rosenstock-Huessy Gesellschaft e.V.

{{ page.zitat }}

**Vorstand/board/bestuur:**\
*Dr. Jürgen Müller (Vorsitzender);*\
*Thomas Dreessen; Andreas Schreck; Dr. Eckart Wilkens*\
*Antwortadresse: Jürgen Müller: Vermeerstraat 17, 5691 ED Son, Niederlande*\
*Tel: 0(031) 499 32 40 59*

***Brief an die Mitglieder Dezember 2018***

**Inhalt**

{{ page.summary }}

### 1. Einleitung

Liebe Mitglieder und Freunde,

Rosenstock-Huessy beschreibt in „Die Hochzeit des Krieges und der Revolution”, wie das zweite Millenium an ein Ende gekommen ist. Dieser größere Zusammenhang gab ihm die Klarheit, die Unterzeichnung des Versailler Vertrages als einen Neuanfang zu sehen. Wie schwierig dessen Gestaltung sein werde hat Rosenstock-Huessy an gleicher Stelle notiert und wir erleben es heute 100 Jahre später. Die Ereignisse des abgelaufenen Jahres brachten uns erneut diese Schwierigkeiten vor Augen und fordern uns heraus, zu einer friedvollen Gestaltung des dritten Jahrtausends beizutragen.
>*Jürgen Müller*

### 2. Bearbeitungen

In dem Briefwechsel zwischen Ko Vos und Freya von Moltke, den Marlouk von Lien Leenman fürs Archiv übergeben hat, steht zu lesen, daß Ko Vos während der Entstehung der erneuten Herausgabe der Soziologie von 1956/58 die Einsicht gekommen ist, daß die Zeit der Bücher vorbei ist. Oder vielmehr: daß das Lesen von der ersten Stelle an die zweite (oder dritte?) gerückt ist, das heißt vom Präjektivum, was es für die zu Rat und Tat Ge- und Berufenen seit der Reformation gewesen ist, zum Subjektivum, zum Feld, auf dem die Schätze erst ins Innere wandern müssen, ehe sie trajektiv, das heißt fruchtbar werden. Und die Editionsgeschichte mit dem Desaster, das die Eugen Rosenstock-Huessy Gesellschaft mit dem Talheimer Verlag erlebt hat, ist vielleicht Beleg für diese Erkenntnis.

So kann denn die Verwunderung, daß Eugen Rosenstock-Huessy bei so vielen Vorlesungen, die dank der Initiative von Russ Keep auf Tonband aufgenommen wurden, es dabei beließ, daß sie ihren Weg auch weiter zum Ohr, wie Ricarda Huch gesagt hat: dem Tor zum Herzen finden würden. Die Transkriptionen von Frances Huessy sind ja vordringlich dazu verfaßt worden, daß die Hörer beim Hören Hilfe hätten.

Ich bin nun dazu gelangt, etliche dieser Vorlesungen ähnlich wie die Soziologie und andere Werke Rosenstock-Huessys nach dem Gehör zu gliedern und dabei auch Editionsarbeit zu leisten, indem Floskeln der mündlichen Rede weggelassen werden und dem Leser das flüssige Verfolgen ermöglicht wird, das ein Hörer, mögen auch Störungen welcher Art auch immer, wie von selber aufbringt, weil das Gedächtnis doch streng wählt zwischen dem vom Sinn des Ganzen her Notwendigen und dem Beiwerk, das diesen Sinn, damit wir Zeit gewinnen, umgibt.

Und da kommt nun eine andere Art des Lesens heraus, die sich dem alten Gebrauch von Schrift annähert, nämlich daß sie verlautet wird. Ich habe immer so gelesen, daß ich innerlich die Worte verlautet habe – aber das ist doch längst nicht mehr Usus.

Meine Übersetzung des Gehörseindrucks mitsamt dem Fassen des Sinns fordert den Leser nun auf, die Geschwindigkeit, die das Sehen erlaubt, zu verlangsamen und also das Tor zum Herzen zu öffnen, die Hörkraft, die nämlich zwischen dem entscheidenden Augenblick und der Ewigkeit eine Brücke schlägt, von der es in einem seiner Gedichte heißt: Augenblicke, Ewigkeiten zueinander zu bekennen ist des Wortewesens Sinn.

Gerade bin ich mit der Bearbeitung der 25 Vorlesungen Universal History 1954 beschäftigt und möchte alle, die mögen, darauf anspitzen, die Zeit aufzubringen, das in Ansprache, Ausruf, Erzählung und Feststellung ausgebreitete Wissen der Soziologie von 1956/58 in aktueller Form nachzulesen.
>*Eckart Wilkens*

### 3. Entdeckungen

Ereignisse machen Geschichte, erzeugen gemeinsame Erfahrungen, Fragen, Aufgaben. Ich möchte Ihnen ein paar zeitgenössische Stimmen adressieren die Rosenstock-Huessy nicht kennen und doch mit ihm sprechen könnten: Weil die Welt zu hell geworden/ und mit selbstgespeistem Lichte/ unsre Augen müde beizte/ sank sie plötzlich in das Dunkel./ Heute liegt sie unsichtbar… Wie wenn erst noch alles wieder/ ungesehen solle werden,/ eh es wieder kommen darf.

Thomas Bauer (Jgg 1961, Professor für Islamwissenschaft und Arabistik/Uni Münster) veröffentlichte 2018 im Februar das Büchlein Die Vereindeutigung der Welt. Über den Verlust an Mehrdeutigkeit und Vielfalt. Mittlerweile gibt es 8 Auflagen!!
Seine These lautet: Es muß so etwas wie eine Disposition zur Vernichtung von Vielfalt geben.
Die zweite Hälfte des 19.Jahrhunderts brachte alle jene vollständig ambiguitätsintoleranten Ideologien hervor, die schließlich das 20.Jahrhundert zum blutigsten der Weltgeschichte werden ließen. Diese Ambiguitätsintoleranz habe drei Wesenszüge: Wahrheitsobsession, Geschichtsverneinung und Reinheitsstreben. Sie bilden damit die Basis jedes Fundamentalismus und andererseits der Gleichgültigkeit. Eindeutigkeit und Bedeutungslosigkeit liegen nach Bauer ganz nah beieinander.
Er zeigt auf wie die Vereindeutigung in Religion, Kunst und Musik zur Herrschaft gekommen ist. Dabei seien es weniger Mächtige, die Vereindeutigungsstrategien eingerichtet haben. Diese gingen von den Individuen selber aus. Dem Authentizitätskonzept liege die Vorstellung einer wahren Natur jedes Einzelnen zugrunde, die durch Kultur und Gesellschaft verdeckt und verfälscht werde. Das Ideal der Utopisten von Silicon Valley und der künstlichen Intelligenz sei der schwitzende, authentische, ambiguitätsfreie Maschinenmensch, der selbstoptimiert im kapitalistischen Verwertungsprozess völlig effektiv funktioniert.
Um diesen Prozeß der Bedeutungsvernichtung mindestens zu bremsen müssen nach Bauer zunächst Kunst, Religion, Wissenschaft, Politik und Natur wieder ihren Eigenwert zurückerhalten, anstatt auf die verführerische Eindeutigkeit ihres Marktwertes reduziert zu werden, die sie letztlich zu völliger Bedeutungslosigkeit verurteile. Man könnte dabei von vormodernen Gesellschaften lernen, in denen über lange Zeit eine ambiguitätstolerante Mentalität herrschte.

Der Soziologe Hartmut Rosa (Jgg. 1965, Prof. für Allgemeine und theoretische Soziologie/ Uni Jena) beschreibt in seinem Buch: Unverfügbarkeit (Wien-Salzburg 2018) das Weltverstummen als die Grundangst der Moderne das sie aber selber herstellt: *„Unablässig versucht der moderne Mensch, die Welt in Reichweite zu bringen: Dabei droht sie uns jedoch stumm und fremd zu werden: Lebendigkeit entsteht nur aus der Akzeptanz des Unverfügbaren. Die Moderne, so lautet seine soziologische These, ist kulturell darauf ausgerichtet und durch ihre institutionelle Verfassung strukturell dazu gezwungen, die Welt in allen Hinsichten berechenbar, beherrschbar, vorhersagbar, verfügbar zu machen: Durch wissenschaftliche Erkenntnis, technische Beherrschung, politische Steuerung, ökonomische Effizienz usw. Er nennt dies den Aggressionsmodus der Weltbeziehung, der dort zum Problem wird, wo er zum Grundmodus jeglicher Lebensäußerung wird, weil er verkennt, dass Subjekt und Welt nicht einfach als unabhängige Entitäten schon da sind, sondern dass sie aus ihrer wechselseitigen Bezogenheit erst hervorgehen. Resonanz aber lasse sich nicht verfügbar machen: Das ist das große, konstitutive Ärgernis dieser Sozialformation, es ist ihr Grundwiderspruch, das, was in immer neuen Varianten Wutbürger produziert.”*

Andreas Weber, (Jgg. 1967 Biologe und Philosoph) sagt in seinem Buch: Lebendigkeit eine erotische Ökologie(2014) *„es könnte sein, dass unser Planet in Wahrheit nicht in einer Umweltkrise steckt, oder in einer Wirtschaftskrise. Sondern dass die Erde derzeit unter einem Mangel unserer Liebe leidet – während sie in die sechste Aussterbewelle eingetreten ist, immer mehr Menschen über das Gefühl der Sinnlosigkeit klagen, Depressionen und Persönlichkeitsstörungen auf dem Vormarsch sind und immer noch Milliarden von uns in der Düsternis absoluter Armut leben.*

*Um die Liebe zu verstehen, müssen wir das Leben verstehen. Um lieben zu können, als Subjekte mit einem empfindsamen Körper, müssen wir lebendig sein können. In Fülle lebendig sein zu dürfen heißt, geliebt zu sein. Sich selbst seine Lebendigkeit zu erlauben heißt, sich selbst zu lieben – und zugleich die schöpferische Welt, die ihrem Prinzip nach zutiefst lebendig ist. Das ist die Grundthese der erotischen Ökologie.*
*Wer die Liebe ausblendet, kann die Wirklichkeit nicht verstehen. Das gilt für alle Wirklichkeit, die physische und die des Gedankens, insbesondere aber für die Biospäre, die Wirklichkeit der Körper. Keine biologische Beschreibung ist vollständig, wenn sie nicht als eine Biologie der Liebe angelegt ist.*
*Weil man über Liebe nur schreiben kann, indem man in einer bestimmten Weise liebt, werden es sehr subjektive, intensiv erfahrene Geschichten sein. … Eros ist das Prinzip schöpferischer Fülle, das Prinzip des Überfließens, des Teilens, des Mitteilens – der Selbstrealisierung, die in jedem Mineral schon schlummert, und um die wir, so schmerzhaft sie immer wieder ist, in dieser Welt nicht herumkommen, wenn wir mit der Wirklichkeit in Kontakt bleiben wollen, gleich wie: als Denker oder als jemand, der einfach nur ist.*
*Derzeit fehlt diese Dimension in den meisten ernsthaften Beschreibungen der Wirklichkeit.
Die Idee, von den anderen getrennt zu sein wie der Forscher in seinem Labor von den Objekten, die er untersucht, ist vielleicht der fundamentale Irrtum unserer Zivilisation. Erst die darin liegende Verblendung macht unsere sagenhafte Gleichgültigkeit gegenüber dem massenhaften Tod der Natur möglich.*
*Das Zentrum der Wirklichkeit: Alles antwortet einander
Heute, wo unsere Zivilisation nach zweihundert Jahren intensiver Versuche, „Aufklärung” und „Erhellung” zu schaffen, die Erde in die aufregendste Lage seit 200 Millionen Jahren gebracht hat, kommt vielleicht der Moment, sich vom Glauben an ein Leben ohne Tod zu verabschieden. Aber dieser Moment, dachte ich beklommen, wird selbst ein Sterben sein.
Leben heisst Sterben lernen. Das Universum ist nicht bloß zärtlich. Es ist ebenso tödlich. Und zärtlich kann es nur sein, weil es tödlich ist. Zärtlich kann es nur sein, indem sich diese Zärtlichkeit beständig gegen den Tod zur Wehr setzt. Das ist die Botschaft der erotischen Ökologie, die sie dem Darwinismus, dem Liberalismus und all den herrschenden Verzweckungsideologien entgegensetzt, all den Ideologien der Effizienz, des Zweikampfes, des Krieges als Vater aller Dinge.
Diesen realen Tod auszublenden, ja, ihn mit allen dem Menschen nur zugänglichen Mitteln abzuschaffen, ist das große Projekt der Moderne seit 500 Jahren.
Erst unsere Verleugnung der Tatsache, dass der Tod notwendig ein Teil des Lebens und gerade der eigene Tod für das eigene Leben unerlässlich ist, macht das Leben zu einem Ort des Sterbens, zu einem Ort, an dem sich keine Geburt mehr ereignet.*

Ich halte es für wünschenswert, wenn wir Bauer, Rosa, Weber ins Gespräch mit Eugen Rosenstock-Huessy, Patmos, Kreatur und Kreisau nehmen zum gemeinsamen Thema: Farewell Descartres.
>*Thomas Dreessen*

### 4. Jahrestagung und Mitgliederversammlung 2019

Wir möchten Sie herzlich zu unserer Jahrestagung mit dem Thema: „Was ist unsere Heimat nach dem Weltkrieg?Rosenstock-Huessys Zeitansage 1918”an Palmarum (12.14.4. 2019) ins Haus am Turm, Essen-Werden einladen. Die Textgrundlage wird „Der Heimfall der Heimat” in „Die Hochzeit des Krieges und der Revolution” sein.
Das detaillierte Programm und die Tagesordnung der Mitgliederversammlung wird Ihnen noch zugesandt werden.
>*Jürgen Müller*

### 5. Adressenänderungen

Bitte schreiben sie eine eventuelle Adressenänderung schriftlich oder per E-mail an Thomas Dreessen (s. u.), er führt die Adressenliste. Alle Mitglieder und Korrespondenten, die diesen Brief mit gewöhnlicher Post bekommen, möchten wir bitten, uns soweit vorhanden ihre Email-Adresse mitzuteilen.
>*Thomas Dreessen*

### 6. Hinweis zum Postversand

Der Rundbrief der Eugen Rosenstock-Huessy Gesellschaft wird zukünftig als Postsendung nur noch an Mitglieder verschickt. Nicht-Mitglieder erhalten den Rundbrief gegen Erstattung der Druck- und Versandkosten in Höhe von € 20 p.a. Den Postversand betreut Andreas Schreck. Der Versand per e-Mail bleibt unberührt.
>*Andreas Schreck*

### 7. Jahresbeiträge 2018 und 2019

Infolge Inanspruchnahme durch sehr schöne private Pflichten (Hochzeitsvorbereitungen) bittet der Schatzmeister um Nachsicht, dass die Abbuchung des Jahresbeitrags 2018 zusammen mit dem Beitrag für 2019 erst Anfang 2019 erfolgt.

Die „Selbstzahler” unter den Mitgliedern werden gebeten, den Jahresbeitrag in Höhe von 40 Euro (Einzelpersonen) auf das Konto Nr. 6430029 bei Sparkasse Bielefeld (BLZ 48050161) zu überweisen.
Nach Einführung des einheitlichen Zahlungssystem SEPA mit der Internationalen Kontonummer (IBAN): DE43 4805 0161 0006 4300 29
SWIFT-BIC: SPBIDE3BXXX ist diese „IBAN”-Nummer auch für Mitglieder in den Niederlanden und im ganzen „SEPA-Land”, in das sich sogar die Schweiz hat einbetten lassen, ohne Zusatzkosten nutzbar.
>*Andreas Schreck*

[zum Seitenbeginn](#top)
