---
title: "Rosenstock-Huessy: Des Glaubens Tageszeiten (1951)"
category: online-text
published: 2024-04-28
org-publ: 1951
language:
---
### Stimmstein 6, 2001

#### Mitteilungsblätter  2001

>*Ostermontag 1951*
### Des Glaubens Tageszeiten

Abend-vorabendlich sind wir geschaffen.\
Abend und Morgen wölben einen Tag.\
Die Nur-Vorabender sind Menschenaffen.\
Die nur Tagtagenden der Menschenschlag.

Doch wenn die Tagenden sich blutig schlagen,\
weil jeden eine andre Stunde krönt,\
muß Genesis I, 3 uns wieder sagen:\
der Abend ist's der Tageszeit versöhnt.

Bei Sonnenaufgang springt zu Pferde\
Der Genius wie ein Held zur Schlacht.\
Vorabends Schöpfung ist vergessen:\
Er weiß nur, daß er aufgewacht.

Die Vesper schlägt, ein heilger Beter\
kniet seine Knie, hebt seine Hände.\
Er sühnt des Tages Übertreter\
Zu Ewigkeit wird ihm das Ende.

Des Mittags Blitze und Gewitter\
zertreten fast den Sohn des Herrn;\
doch aus der Leidenskelter tritt er\
auf seinen Weg als neuer Stern.

Drei Tageszeiten unsrem Glauben\
Prometheus, Israel und Christ.\
Kannst Du denn einer uns berauben,\
wenn Du nicht ganz von Sinnen bist?

Der Schoß der Zeiten vor dem Morgen,\
vor Vesper und vor Mittamtag\
Hat ewig weiblich uns geborgen\
als einen einzigen Menschenschlag.

Jud', Christ und Heide in drei Zeiten,\
des Glaubens Tageszeiten, stehn.\
Doch alle drei aus Einem schreiten:\
Vorabend mußte erst geschehn.

[zum Seitenbeginn](#top)
