---
title: Mitgliederbrief 2020-12
category: rundbrief
created: 2020-12
summary: |
  1. Einleitung                                       - Jürgen Müller
  2. Im Advent                                        - Thomas Dreessen
  3. Mitleidenschaft im Dritten Jahrtausend           - Sven Bergmann
  4. Um Eugen Rosenstock-Huessys Taufspruch           - Sven Bergmann
  5. Eugen Rosenstock-Huessy in Frankreich            - Sven Bergmann
  6. Argonauten                                       - Thomas Dreessen
  7. Entdeckung des Todes – Wissenschaften im Advent  - Thomas Dreessen
  8. Bücher                                           - Andreas Schreck/Thomas Dreessen
  9. Jahrestagung und Mitgliederversammlung 2021      - Jürgen Müller
  10. Adressenänderungen                              - Thomas Dreessen
  11. Hinweis zum Postversand                         - Andreas Schreck
  12. Mitgliederbeitrag 2020 und 2021                 - Andreas Schreck
zitat: |
  > Wir Menschen kommen zu einander und sogar zu uns selber immer zu spät, sobald wir vorher wissen wollen, was zu tun sei. Denn das können wir nur wissen, indem wir es ausprobieren. Und inzwischen treibst du mit deinem Lebensschiffchen in eine andere Fahrtrinne.
  >
  > Nie dürfen die Philosophen den Staat regieren. Vielmehr müssen sie die Gründe studieren, weshalb sie ihn nicht regieren dürfen! Die einzige Macht, die in unsere Existenz eingesenkt ist, um die Herren Nachbedacht und Vorexperiment in Schach zu halten, ist bekanntlich jene merkwürdige Antizipation, die Vorwegnahmekunst, welche die Märchenbücher füllt. Da sind Bären und Kröten, Störche und Schlangen, mit anderen Worten, lauter mißratene Geschöpfe. Und sie würden mißraten bleiben und nie ihr schön menschlich Antlitz wieder kriegen ohne die seltsame Vorwegnahme eines unverdorbenen Auges. Dies Auge sieht den tapsigen Bären: „Mit Geduld”, so sagt sich der Träger des Auges der Vorwegnahme, „mit viel Geduld wird das ein herrlicher Prinz. Er braucht Beistand. Als sein Beistand werde ich manchmal Prügel beziehen. Aber um diesen Preis wird es gehen. Am Ende, so nach dreißig Jahren, wird jedermann sich wundern, den Bären je für einen Bären angesehen zu haben. Alle werden ausrufen: Ein Prinz, ein Prinz! Und was ich heut allein sehe, wird dann seine offizielle Benennung im ‚Wer ist’s?’ (engl.: Who is who) der Geschichte geworden sein.”
  >
  > Die Menschen hören auf, Teile der Welt zu sein, weder durch Theologie noch durch Psychologie noch durch Genealogie oder Statistik. Der einzige Weg der Menschwerdung ist die liebende Vorwegnahme. Gelingt es, den ersten Augenblick dieser Vorwegnahme durch ein liebendes Auge zu verewigen, dann ist dieser Menschwerdung der Weg gebahnt.
  >
  > *Eugen Rosenstock-Huessy, Soziologie – Die Vollzahl der Zeiten, 1958, p.728*
---

## Eugen Rosenstock-Huessy Gesellschaft e.V.

{{ page.zitat }}

*Dr. Jürgen Müller (Vorsitzender);\
Thomas Dreessen; Andreas Schreck; Sven Bergmann\
Antwortadresse: Jürgen Müller, Vermeerstraat 17, 5691 ED Son, Niederlande,\
Tel: 0(031) 499 32 40 59*

## Brief an die Mitglieder Dezember 2020

**Inhalt**

{{ page.summary }}

### 1. Einleitung

Liebe Mitglieder und Freunde,
die aktuelle Corona-Krisis dauert an. Erreicht sie jetzt ihren Höhepunkt? Sie greift stärker in unser Leben ein, als frühere Krisen. Die Normalität ist unterbrochen. Wird sie sich ändern, sich ändern müssen? Wie können wir bereit werden notwendende Veränderungen zu bejahen und positiv anzugehen? In welche Richtung soll es dabei gehen und gibt es dafür Vorgänger?

Rosenstock-Huessy empfiehlt nicht, alles Vergangene zu vergessen um als globale Menschen, Weltbürger, versuchen zu leben. Im Gegenteil, beide Aspekte, der der größeren Gemeinschaft und unsere eigene Geschichte, sind wichtig, sollen zusammenkommen, sich gegenseitig befruchten. Sobald die Einschränkungen es zulassen, wollen wir uns dafür zusammensetzen, Erfahrungen teilen und uns durch Vorgänger inspirieren lassen. Im vergangenen Jahr kreiste unsere Diskussion in der Eugen Rosenstock-Huessy-Gesellschaft um den Begriffe der Hörspur. Üblicherweise steht das Hören im Schatten des Sprechens, der Vorschläge, Konzepte, Ideen Pläne und Nachrichten. Im Zeichen von Corona stieg die mediale Dauerbeschallung auf bisher ungeahnte Höhen. Täglich, stündlich, minütlich überschlagen sich Neuigkeiten, Wiederholungen, Ereignisse aus aller Welt. Finden Sie sich in „der” öffentlichen Meinung wieder? Fühlen Sie sich in ihrer persönlichen Betroffenheit wahrgenommen. Schreiben Sie uns Ihre Erfahrungen und vielleicht positiven oder negativen Erlebnisse an:

[Email](info@rosenstock-huessy.com)

> *Jürgen Müller*

### 2. Im Advent

Heut am 3.Advent sitze ich vor dem Ofen mit meinem kleinen PC. Die Flammen tanzen und zucken suchend wie meine Gedanken: Was schreibe ich unseren Lieben, unseren Freunden und Bekannten in dieser Zeit? Immer wieder spreche ich den Wochenspruch, das Wort des Profeten Jesaja: Bereitet dem Herrn den Weg; Er kommt gewaltig, und sein Arm wird herrschen!”

Einen Hirten aus unserer Krippe mit seinem Schaf – aus der Werkstatt meiner Mutter vor über 30 Jahrenhabe ich auf meinem Tisch platziert, inspiriert durch Rosenstock-Huessys katholischen Freund Joseph Wittig.

![Hirte]({{ 'assets/images/Hirte_im_Advent.jpg' | relative_url }}){:.img-left.img-small}
Der kleine Hirte mit dem freundlichen Gesicht aus unserer Krippe, der Bartholomäus, hat mir zugehört und – er sprach:

*„Die Zeit ist vorbei in der es reichte, dass wir Krippenhirten, unsere Schafe, Ochs und Esel, Euch den Advent und unsere Sehnsucht und dann das damals neue Licht erinnert haben. Ihr selber lebt jetztwie wir damalsim Dunkel. Corona, dass Klima, die Spaltungen unter Menschen und Völkern und der „kaputte Planet” sind Zeichen, dass eine Zeit zu Ende ist. Ihr selber Alte und Junge, Frauen und Männer, alle Völker müsst jetzt leben voller Sehnsucht nach neuem Licht in Eurer Dunkelzeit.*

*Ein jüngerer Freund, Celaluddin Rumi, hatte in seiner Zeit gesagt: ‚Gehe den Weg Mohammeds. Es ist der Weg in der Gemeinschaft mit den gewöhnlichen Menschen zu leben. Aber wenn Du das nicht kannst, dann gehe den Weg Jesu. Es ist der Kampf gegen die Einsamkeit und die Überwindung der Begehrlichkeit.‘*

*Vor ein paar Jahren haben weise Frauen bei uns in Palästina und Israel ein Lied angestimmt, das ich Dir heute singen möchte zur Stärkung für Euren langen Weg im Advent. Höre:*

*Komm Heilger Geist, mit Deiner Kraft, die uns verbindet und Leben schafft.*\
*Komm Heilger Geist, mit Deiner Kraft, die uns verbindet und Leben schafft.*\
*Wie das Feuer sich verbreitet und die Dunkelheit erhellt*\
*So soll uns Dein Geist ergreifen, umgestalten unsre Welt.*\
*Komm, erfülle unsre Herzen. Deine Gaben uns verleih.*\
*Weck uns auf aus unsrer Trägheit und mach unser Leben neu.*\
*Schenke uns von Deiner Liebe, die vertraut und die vergibt.*\
*Alle sprechen eine Sprache, wenn ein Mensch den andern liebt.*\
*Komm Heilger Geist, mit Deiner Kraft, die uns verbindet und Leben schafft.*\
*Komm Heilger Geist, mit Deiner Kraft, die uns verbindet und Leben schafft.”[^1]*

Er verstummte und stand wieder vor mir als kleine Krippenfigur mit dem Schaf.
Ich aber wusste, dass ich Euch diese Geschichte erzählen muss. In dem Licht, das die Hirten damals erblickten in ihrer Dunkelheit: dem Stern und dann dem Kind in der Krippe, darin war der Völkerfrieden von Pfingsten in ihren Herzen geboren – eine neue Zeit war angebrochen. Eugen Rosenstock und Joseph Wittig sind adventliche Hirten unserer Zeit. Ihr gemeinsames Werk [^2] lässt mich singen: *„Weil Gott in tiefster Nacht erschienen, kann unsre Nacht nicht traurig sein! Er sieht dein Leben unverhüllt, zeigt dir zugleich dein neues Bild!”[^3]* Ich höre: Du wirst gebraucht! – Ihr allekleine und große – nahe und ferne – Frauen und Männer – Stämme und Völker werdet gebraucht zusammen Wege zu bereiten für die Erneuerung des Lebens mit allem Lebendigen, unser Haus Planet Erde!

Ja! Komm Heiliger Geist, mit Deiner Kraft, die uns verbindet und Leben schafft in unserer Zeit!

Shalom Frieden Salam

> *Thomas Dreessen*

[^1]: Lied 173 in: Zwischen Himmel und Erde
[^2]: Das Alter der Kirche, 1927; neu hg. 1998
[^3]: EG 56, vs.3


### 3. Mitleidenschaft im Dritten Jahrtausend

Weltweit raubt Corona Menschen den Atem. Wie aus dem Nichts hat sich das Virus 2020 entlang der Wirtschafts-, Handels- und Touristenströme verbreitet. Dabei zerstört das Virus das eigentlich Soziale des Menschen. Distanz statt Miteinander ist der Imperativ der Pandemie. Zwar haben vor allem einige asiatische Staaten die Ausbreitung durch drakonische Isolierungen unter Kontrolle gebracht, aber um welchen Preis. Es scheint, als stelle das Übergreifen des Virus vom Tier auf den Menschen die moderne Hybris in Frage, die Natur beherrschen zu können. Bei weiter wachsender Weltbevölkerung scheint es nur eine Frage der Zeit, wann der nächste Angreifer überspringt. Dabei ist das Phänomen alles andere als neu. Die spanische Grippe liegt genau einhundert Jahre zurück. Eugen Rosenstock berichtet von einem Treffen mit dem Stabsarzt Viktor von Weizsäcker und Franz Rosenzweig nach der Marneschlacht an der Westfront. Am 19. Juli 1918 hätten sie gewußt, daß der Krieg verloren sei, durch die spanische Grippe. Aber der wirkliche Zusammenhang weist noch weiter in die Geschichte zurück, in die Zeit der Züchtung der klassischen „Haustiere” über tausende von Jahren. Im „goldenen Halbmond” haben Menschen nahezu alle bekannten Tiere auf ihre Zuchtfähigkeit untersucht und dann entsprechend gezüchtet. So wurden aus Wildtieren Haustiere wie wir sie noch heute kennen (Klaus Theweleit: „Warum Cortés wirklich siegte" (Pocehontas Quadrologie, Band III). Schon dabei fanden viele Menschen den Tod. Bei der Eroberung Amerikas durch die „Europäer” sind die Ureinwohner nicht durch Kämpfe oder eingeschleppte Masern gestorben, sondern durch ihnen unbekannte Viren. Pferde, Kühe oder Schweine wurden erst von den Eroberern importiert. Corona bringt also einen lange vergessen Kampf zwischen Mensch und Tier in Erinnerung.

Rasse ist das Endprodukt der Welteroberung in dem Jahrtausend seit den Fahrten der Nordmänner, der Normannen, bis zur Astronautik. Diese Weltunterwerfung sah vom Sprechen grundsätzlich ab, denn sie wollte hinter die Natur der Dinge dringen und setzte daher die rein aktive Ratio, einen über die Gegenstände denkenden Verstand, als Ausgangspunkt an, abgelöst vom Logos. Eine nicht mithörende Natur wurde untersucht. Dieser gigantische Säkularisierungsprozeß der Weltentdeckung löschte alle Namen aus. (Eugen Rosenstock-Huessy, Die Rasse der Denker oder der Schindanger des Glaubens, in: ders., Die Sprache des Menschengeschlechts. Eine leibhaftige Grammatik in vier Teilen, 1. Bd., Heidelberg: Verlag Lambert Schneider 1963, S.616ff.)

Ist Corona ein Anzeichen für das dritte Jahrtausend, für den nachchristlichen Menschen im Zeichen des barmherzigen Samariters?

Es ist erstaunlich, welche Melodie uns dieses dritte Jahrtausend – 30 Jahre nach dem Tod von Eugen Rosenstock-Huessyschon gespielt hat. Am 11.09.2001 brachten vier Flugzeuge in vier Stunden die Unverwundbarkeit der stärksten Militärmacht des Planeten zum Einsturz. Dieser Tag hat sich unauslöschlich in die Erinnerung eingebrannt. Kaum ein Jahrzehnt später erschüttere die Finanzkrise den Traum immerwährender Prosperität, wie er vor allem von Alan Greenspan und der amerikanischen Zentralbank geschürt worden war. Die Katastrophe zeigte, wie total die wechselseitige Abhängigkeiten des internationalen Finanzsystems waren. Keine Spur von nationaler Souveränität. Und dann 2020 Corona. Sind die drei globalen Zuckungen nur zufällig? Lassen sie sich nach einige Zeit einfach wieder abhaken, um dann weiter zu machen wie bisher? Bisher sieht es nicht so aus, als hätten die Menschen schon die entsprechenden Konsequenzen gezogen und sich auf die Zukunft, auf die „Vollzahl der Zeiten” eingestellt. Aus welcher Ecke droht der nächste Schock? Auch ohne „Prophetie” sollten die Ereignisse zu denken geben.

*Bereits der Marxismus hat richtig erkannt, daß der Schritt in die Wissenschaft erst dann vollzogen wird, wenn die Krise zum Ausgangspunkt der Erklärung gemacht wird. Erst die Krankheit belehrt über die Gesundheit. Erst die Depression über die normale Produktion. Erst der Krieg über den Frieden. Dann erst hören wir auf, von einem willkürlichen egozentrischen Begriff des uns zusagenden Normalen auszugehen und alles das für die Ausnahme zu erklären, was uns nicht paßt. In Amerika z.B. gilt der Friede als natürlich, der Krieg als unnatürlich. Also haben die Amerikaner die Kunst Frieden zu schließen, weder 1865, noch 1918, noch 1945 geübt. Der Friede war ja da, sobald nicht mehr geschossen wurde, dachte der gemeine Mann; er ließ deshalb seine Regierung jedesmal im Stich, wenn sie sich zu dem langsamen Weg in den Tatbestand „Frieden” anschickte.* (Eugen Rosenstock-Huessy, Die Zeitweiligkeit der Sprache, in: ders., Die Sprache des Menschengeschlechts. Eine leibhaftige Grammatik in vier Teilen, Bd.1, Heidelberg: Verlag Lambert Schneider 1963, S.684/685.)

> *Sven Bergmann*

### 4. Um Eugen Rosenstock-Huessys Taufspruch

Unter dem Bann der Philologie, der Liebe zur Sprache hat Eugen Rosenstock-Huessy seine Jugendzeit beschrieben. Dieses Fach markierte zu seiner Zeit die „Königsdisziplin” der deutschen Universität, vielleicht noch mehr als die der Historiker. Die Entdecker Olympias und „Trojas” waren Philologen, Barthold Georg Niebuhr, Ernst Curtius und Theodor Mommsen leuchtende Vorbilder. Die Schätze der kanonischen Texte des Altertums und der vorbildlichen griechischen und römischen Geschichte für die allgemeine Bildung wurden von Philologen gehoben. Homer, Tacitus oder Seneca setzten die Maßstäbe menschlichen Handelns. Textfragen spornten Eugens Geist an. Er hat von seiner Begeisterung für die evangelischen Choräle seiner Schulzeit gesprochen, in Aufführungen seines Musiklehrers Hermann Kawerau, der gleichzeitig die Berliner Singakademie leitete und damit die Tradition der Bachpflege seit 1800 fortführte. Franz Rosenzweig hat ihn als „anima naturaliter christiana” benannt und Eugen Rosenstock-Huessy hat diesen Namen gerne angenommen und auf sich sitzen lassen. Aber welche Bewandtnis hat es mit seinem Taufspruch, Lukas 6.5?

An demselben Tage sah er jemanden am Sabbat arbeiten und sprach zu ihm: Mensch, wenn du weißt, was du tust, so bist du selig zu preisen; weißt du es aber nicht, so bist du verflucht und ein Übertreter des Gesetzes.

Weder in der Lutherbibel noch in modernen Bibelausgaben ist dieser Spruch zu finden. Warum wählte Eugen Rosenstock dann gerade diesen Text und keinen anderen?

Fritz Herrenbrück hat nach intensiven Quellenstudien das Taufdatum feststellen können und die Textstellen zum Taufspruch zusammengeführt (Herrenbrück, Fritz, Eugen Rosenstocks Taufdatum und Tauftext, in: Ins Kielwasser der Argo. Herforder Studien zu Eugen Rosenstock-Huessy. Festschrift für Gerhard Gillhoff zum 70. Geburtstag, hrsg.v. Knut Martin Stünkel, Würzburg: Königshausen & Neumann 2012, S.31-57.). Aber warum schreibt Eugen Rosenstock, daß selbst der Pfarrer den Spruch nicht verstanden habe? Gibt es Indizien dafür, warum der Täufling gerade diesen Spruch wählte?

Einen Hintergrund bildet die gerade in den ersten Jahren des 20. Jahrhunderts intensive theologische Diskussion um die Quellen zu Lukas, den Arzt und griechisch gebildeten Evangelisten. Welche Quelle, welcher Papyrus ist zuverlässig? Die Quelle Q wie Adolf von Harnack nicht müde wurde zu betonen, oder kann daneben auch der sogenannte Codex Bezae Codex D bestehen? Immerhin handelte es sich bei diesem um eine der ältesten erhaltenen Papyryhandschriften aus dem 6. Jahrhundert. Der Papyrusforscher James Randel Harris hatte sich intensiv mit dessen Geschichte befaßt: Codex Bezae. A study of the so called western text of the New Testamment, 1891. Und bei Johannes Beza handelte es sich immerhin um den engsten Vertrauten Johannes Calvins. Und Beza hatte diese Handschrift aus Frankreich nach Cambridge gebracht.

In seinem Austausch mit Bas Leenman hat Eugen Rosenstock festgestellt, alle wissenschaftlichen Studien zu Jesaja studiert zu haben. Warum sollte er ausgerechnet zu seinem Taufspruch nicht die namhaften Lukas Kommentare konsultiert haben?

Zur Zeit vor Eugens Taufentscheidung beschäftigte diese Frage die Altertumsforscher und Theologen Julius Wellhausen, Johannes Weiß, Adolf Jülicher, Bernhard Weiss und Eduard Meyer. In seinem Kommentar zum Neuen Testament hatte Johannes Weiß festgehalten: „Hinter 6.5. bietet Codex D noch eine interessante Sabbatgeschichte, die etwa aus dem Hebräer-Evangelium stammen könnte.” (Johannes Weiß, Kommentar, in: Die Schriften des Neuen Testaments, hrsg.v. Johannes
Weiß, Göttingen: Vandenhoeck & Ruprecht 1906, S.411). Die Bedeutung der Sabbatstelle deutet Adalbert Merx an:

*Nach dem dort mitgeteilten talmudischen Satze gestatten die Rabbinen das Brechen eines Sabbatgesetzes wegen des menschlichen Bedürfnisses oder der menschlichen Würde. Nach dem Satze, den D erhalten hat, soll es aber das subjektive Urteil des Menschen sein, das über die Verbindlichkeit des Sabbathgesetzes – und damit natürlich über die Verbindlichkeit der Ceremonialgesetze überhaupt – zu entscheiden berechtigt ist, und darin liegt der gewaltige Unterschied zwischen dem talmudischen Satze und der Lehre des Textes von D.* (Adalbert Merx, Die Evangelien des Markus und Lukas nach der syrischen im Sinaikloster gefundenen Palimpsesthandschrift, Berlin: Verlag von Georg Reimer 1905, S.221.)

Und genau auf diese Sabbatgeschichte und die dort angedeutete „Grenzüberschreitung”, auf den Kreuzweg zwischen Judentum und Christentum zielte auch Eugen Rosenstock ab. Aber damit nicht genug. Es gibt noch eine mögliche Quelle für diesen Taufspruch. Im Jahr 1903 war ein weiterer Band der nachgelassenen Werken Friedrich Nietzsches erschienen. Fragment 233 bot ausgerechnet diesen Taufspruch und das mit der expliziten Autorisierung „Jesus von Nazareth”: *„Heil dir, so du weisst, was du thust. Doch weisst du es nicht, so bist du unter dem Gesetze und unter des Gesetzes Fluch.”* (Nachgelassene Werke. Unveröffentlichtes aus der Umwerthungszeit (1882/83-1888) (= Nietzsches Werke; 2. Abt., Bd. XIII.), Leipzig: Nauman 1903, S.98). Zum damaligen Zeitpunkt glaubte man noch an ein Originalwerk Nietzsches mit dem Titel „Vom Willen zur Macht”. Tatsächlich kompilierten Nietzsches Schwester und ihre Helfen diesen Text aus unsortierten Fragmenten. Es ist aber mehr als wahrscheinlich, daß der Büchersammler Eugen diesen Band für seine Bibliothek erwarb. Seine intensive Nietzsche Lektüre ist vielfach belegt.

Eine weitere Facette ergibt sich über die Verbindung zu Calvin. Immerhin hatte die Beschäftigung mit dem Calvinismus durch Max Webers Studien zur protestantischen Ethik nach 1904 einen rasanten Aufschwung erlebt, der im 500. Geburtstag 1909 kulminierte. *„Seit Troeltsch und Max Weber vom Ursprung des modernen Kapitalismus gehandelt haben, ahnt die Welt, daß hier ein religiöses Problem vorliegt.”* (Eugen Rosenstock, Unternehmer und Volksordnung (zuerst veröffentlicht 1924), in: Werner Picht und Eugen Rosenstock, Im Kampf um die Erwachsenenbildung 1912 – 1926 (Schriften für Erwachsenenbildung; 1.Bd.), Leipzig: Quelle & Meyer 1926, S.176.). In den Jahren vor der Taufe hatte Ernst Troeltsch in der Historischen Zeitschrift fast ein Glaubensbekenntnis der modernen Kultur abgelegt:

*„Die moderne Kultur ist hervorgegangen aus dem großen Zeitalter der kirchlichen Kultur, die auf dem Glauben an eine absolute und unmittelbare göttliche Offenbarung und auf der Organisation dieser Offenbarung in der Erlösungs- und Erziehungsanstalt der Kirche beruhte. Nichts ist mit der Macht eines solchen Glaubens zu vergleichen, wenn der Glaube wirklich naturwüchsig und selbstverständlich ist. Dann ist überall Gott, sein unmittelbarer, genau erkennbarer und von einem unfehlbaren Institut getragener Wille gegenwärtig. Dann kommt alle Kraft zu höherer Leistung und alle Sicherung des letzten Lebenszieles aus dieser Offenbarung und aus ihrer Organisation in der Kirche.”* (Ernst Troeltsch, Die Bedeutung des Protestantismus für die Entstehung der modernen Welt, in: HZ, 97.Bd. (1906), S.4ff.)
Es darf als sicher gelten, daß Eugen Rosenstock jeden Aufsatz in der Historischen Zeitschrift zur Kenntnis nahm, zumal da es sich um den Eröffnungsvortrag auf dem Historikertag 1905 handelte. 1914 sollte er eine Calvinistin heiraten.

Aber genug der Anknüpfungspunkte und Spuren, die Eugen Rosenstock möglicherweise bewußt zu diesem Spruch greifen ließen, denn im Kern geht es ihm ja um die Bedeutung der Christusworte.
Zeit seines Lebens haben ihn die Zusammenhänge von Regel und Ausnahme beschäftigt, er ist immer wieder auf diese Zusammenhänge zurückgekehrt, so in der „Werkstattaussiedlung”, im „Industrierecht” und in der „Soziologie”. Den Rhythmen des Lebens und der Arbeit, der Arbeitszeit und der Feiertage ist er in unzähligen Schriften nachgegangen.

Und genau dies ist Thema von Lukas 6.5. Natürlich soll niemand am Sabbat arbeiten. Diese Regel bleibt unangetastet. Aber es gibt auch Notlagen und in diesem Notlagen sind Ausnahmen möglich, solange dem Menschen bewußt ist, daß es sich um eine solche handelt und nicht um Leichtsinn oder Mißachtung. Es gibt nicht den einen verbindlichen Kalender, der für alle Menschen gleichermaßen gilt, deshalb verbietet sich eine sklavische oder sinnwidrige Bindung an das Gesetz, selbst am Sabbat. Und es gibt auch die Möglichkeit, zwischen diesen Kalendern zu wechseln. *„Ich habe meinen eigenen Glauben aufgebaut auf das Wort bei Lukas, das dem 5. Kapitel zugesetzt ist, wo der Herr die Mehrheit von „mehr als einem Kalender” einsetzt. Seltsam ist, daß die erste freie Wahl, die uns Christus anbietet, die der Kalender betrifft und daß ebenso dieser Pluralismus geeignet ist, den modernen monistischen Menschen zu verwundern.”* (Eugen Rosenstock-Huessy, Was proklamieren unsere Kalender?, in: ders., Die Sprache des Menschengeschlechts. Eine leibhaftige Grammatik in vier Teilen, 1. Bd., Heidelberg: Verlag Lambert Schneider 1963, S.523f.) Das bedeutet aber nicht „anything goes” oder heute so und morgen so. Es gibt Maßstäbe von denen man abweichen kann, aber trotzdem bleiben sie maßstäblich. Erst an den Früchten wird das richtige Handeln zu erkennen sein. Aber der Einzelne hat nur den guten Willen, nicht den Erfolg in seiner Hand! Eugen Rosenstock hat dies in seinem „Kreuz der Wirklichkeit”, in der „leibhaftigen Grammatik” an vielen Beispielen durchbuchstabiert. Natürlich kann es hauptberufliche Pflegerinnen oder Schwestern geben, aber die Maßstäbe setzen die pflegenden Angehörigen. Natürlich kann es hauptberufliche Fachlehrer geben, aber den Maßstab für die Erziehung von Kindern setzen die Eltern und nicht umgekehrt. Seelsorger und Theologen bilden eine ähnliche Stufung. In der „Umwandlung” kommt Eugen Rosenstock am Ende seines Werkes erneut auf die Evangelien zurück, die er in ihrer Verschränkung als Abfolge zu einem gemeinsam gesprochenen Wort versteht. In seinen „Gleichnisreden Jesu” hatte Adolf Jülicher von der „reizvollen Aufgabe” gesprochen, *„die Stellung der einzelnen Evangelien in dem stufenweis fortschreitenden Prozess der Parabelaufzeichnung, der ja zugleich ein Prozess der Parabelumschaffung ist, zu zeichnen”* (Jülicher, Adolf, Die Gleichnisreden Jesu, 2 Bde. 1888 und 1899.) Ob Eugen Rosenstock-Huessy sich dieser Aufgabe in seiner „Umwandlung” gestellt hat?

> *Sven Bergmann*

### 5. Eugen Rosenstock-Huessy in Frankreich

Konsultiert man den französischen Marktplatz von Amazon, so findet man dort nicht eine Übersetzung der Werke Eugen Rosenstock-Huessys. Umso erfreulicher ist der Aufsatz von Thierry Laisney in der Zeitschrift „En-attendant-Nadeau” im Dezember 2019. 50 Jahre nach der Veröffentlichung von „Speech and Reality” beschreibt Laisney den grammatischen Ansatz für die Sozialwissenschaften und für ein Verständnis der modernen Gesellschaft. Nach einigen biographischen Angaben gelingt es dem Autor sehr eingänglich und anschaulich, nicht nur „Sprache und Wirklichkeit” vorzustellen, sondern auch den grundlegenden Ansatz des „Kreuz der Wirklichkeit” für das menschliche Zusammenleben in Vergangenheit, Gegenwart und Zukunft. Laisney beginnt mit den vier Grundübeln jeder Gesellschaft: Anarchie, Dekadenz, Revolution und Krieg: „Anarchie (auch „Krise" oder "Depression" genannt) ist der Mangel an innerer Solidarität, ein "Mangel an Zusammenarbeit und gemeinsamer Inspiration". Dekadenz ist die Unfähigkeit, sich die Zukunft vorzustellen, sie weiterzugeben. Revolution, der Wille, die Vergangenheit zu liquidieren. Was den Krieg betrifft, so ist er der Versuch, sich ein fremdes Territorium anzueignen.” Diese vier Grundübel zeigen sich immer auch in der Sprache, dem Dreh- und Angelpunkt der Lehre von Eugen Rosenstock-Huessy. Denn die Sprache gibt dem Menschen die Chance, die Übel zu bekämpfen und gemeinsam in neue Verbindungen jenseits aller Trennungen hineinzuwachsen und über die bloß tote Welt der Physik zu triumphieren. „Und die grammatikalische Methode ist die Art und Weise, in der sich der Mensch seiner vierdimensionalen Berufung bewusst wird.” (Eugen Rosenstock). Eine „dogmatische Grammatik” muß diesen Zusammenhang verkennen und führt zu einer weiteren Versteinerung der sozialen Beziehungen: „Rosenstock schreibt dieser Gleichgültigkeit sogar einen Großteil unserer Verwirrung in den sozialen Beziehungen zu, insbesondere den Konflikt zwischen der realen Person und dem Bildungssystemalle anderen Sozialwissenschaften halten ihre Existenz nur von der Notwendigkeit ab, Abhilfe zu schaffen. Eine „höhere" Grammatik, die der "entscheidenden" Liste des Autors entspricht, würde aus den folgenden Formen bestehen: ama ("Liebe!"), amem ("darf ich lieben"), lover ("sie lieben"), amavimus ("wir haben geliebt"). Dazu gehören (zum Beispiel): Politik, Poesie, Wissenschaft, Geschichte.” (Thierry Laisney). Ohne die höhere Grammatik einer Bewertung zu unterziehen schildert der Autor die wesentllichen Unterschiede von traditioneller und höherer Grammatik: „Personalpronomen sind die subjektive Kraft der Sprache. Substantive sind gegensätzlicher Natur, sie stehen außerhalb. Adjektive werden verwendet, um Neues mit bekannten Begriffen zu beschreiben. Verben hingegen kennzeichnen die Transformation des Universums. Nach Rosenstock sind die pronominalen, nominalen, adjektivischen und verbalen Formen ewig. In ihnen werden wir das vom Autor identifizierte Vierfache der Raumzeit xe erkannt haben (jeweils: einwärts, auswärts, rückwärts, vorwärts).” Thierry Laisney beschließt seine Musterung des „originellen” Werks von Eugen Rosenstock-Huessys mit der Vermutung, daß es zu sehr mit Theologie in Verbindung gebracht werde und daher nicht mehr so oft zu Rate gezogen werde.

*Thierry Laisney, Eugen Rosenstock-Huessy. Speech and Reality, in: En-attendant-Nadeau, Numéro 91, du 20 novembre au 3 décembre 2019, S. 52-54.*

> *Sven Bergmann*

### 6. Argonauten

Claus Friese, ein Argonautiker unter unseren Korrespondenten, Freund von Bas Leenman und Eckart Wilkens, schrieb uns nach dem letzten Mitteilungsbrief:

*„DANKE möchte ich Ihnen sagen –und nochmals DANKE für diese Zusendung, da ich doch ganz gewiß der kleinste der Kleinen bin, was einen „Korrespondenten” (dieser Gesellschaft) betrifft. Ich habe ganz einsam alles durchgelesen, und fühlte mich noch einsamer werdend dabei. Es sind halt schon so viele gegangen, aus der Zeit ausgestiegen (wenn ich nur an Bas Leenman denke…wie schwer es mir seit über einem Jahrzehnt nun fällt, auf meine Besuche in Holland bei den beiden Leenmans verzichten zu müssen).*

*Umso tiefer versuchte ich auf die Texte zu hören, zu lauschen. Es kann doch nicht sein, dass ich der „Einsamkeit” lauschen werdejetzt, in den nächsten Jahren. Da muss doch noch ein Gespräch möglich sein, das uns in die Argonautik leitet, selbst, wenn es zunächst so aussieht, als wenn NIEMAND an unserer Seite stünde.*

*Vielleicht interessiert es Sie, welche vier Stellen aus den zugesandten Texten mich – in diesem Gefahrenbereich Einsamkeit – besonders angeredet haben. Hier sind sie (neben der verblüffenden Zurkenntnisnahme, dass Gandhi doch tatsächlich ein Kreuz in seinem Arbeitszimmer hatte):*
1. *„Vom Anfang, von der Mitte, vom Ende her zu erleben – das sind Erfahrungsformen, die jedem Menschen beschieden sind. Heute ist die Zeit dazu angebrochen.” ….*
2. *Aber es machte mir beim Schreiben auch deutlich, dass ich während der Corona-Krise bisher von niemandem, von niemandem ein Wort gehört habe, das auch nur die Spanne eines Jahres oder Jahrzehnts beim Sprecher spüren lässt! Das heißt, dass die Verantwortlichkeit gegenüber dem eigenen gesprochenen Wort als das unser Leben haltende Wirken des Geistes ganz unbekannt ist – und bitter nötig.*
3. *In der in Deutschland so erfolgreichen Lehre Martin Bubers vom ICH und DU findet Eckart die Ursache, warum die Stimme Eugen Rosenstock-Huessys nicht gehört wird. Bubers ICH ist eben da und sucht das Gespräch mit seinem Gott, seinem DU. Dieses Ich ist unfruchtbar, weil vor der Begegnung schon da, weil fertig. !!!!!!!!!*
4. *Eckarts Beziehung zur ERHG war nie einfach gewesen. Er hatte den Eindruck, daß die Gesellschaft in ihrem Kern ein akademisches Unternehmen war, das sich akademisch mit Rosenstock-Huessy befassen wollte (ist auch mein Eindruck/CF), was der Intention Rosenstock-Huessys entsprechend nur ein Teil der Arbeit ist. Die Argonautik, als dritte Phase der Sozialwissenschaften nach Saint Simon und Goethe baut auf den Einsatz des eigenen Lebens.*

In seinem Buch „Rio Acima – Stromaufwärts, Mit den Surui Indios auf der Suche nach einer neuen Zeit” haben Claus Friese wie er schreibt „zentrale Gedanken Rosenstock-Huessys” inspiriert: *„… denn vielleicht haben auch wir westlichen Menschen die Sehnsucht behalten danach, dass wir unseren Namen im besten Klang hören, ohne uns zu täuschen, und in dem Anruf unseres Namens vernehmen, wer wir sind und noch sein können weit über die bisher angstvoll gesicherten Grenzen hinausdass wir unser Wesen und die Wahrheit unserer Person auf beste Art in dem, wie man uns anspricht interpretiert fühlen. Und dass eine solche Erfahrung allemal eine Reise wert ist, koste sie, was sie wolle”.* (Auszug aus Kapitel 3)

Claus Friese hat ein Wort aus Eckart Wilkens Notiz zu Rosenstock-Huessys Ichthys als Motto seines Buches gewählt. Er schrieb uns: *„Wie fantastisch, dass ich E.W. nun wenigstens noch als einen zukünftigen Ahnen zitieren, und, wenn alles gut geht, ihn voranstellen kann (auch wenn der Titel des Buches auf den ersten Blick gar nicht zu ihm zu passen scheint; aber ich mühe mich seit mindestens zwanzig Jahren um die Sprachwerdung vom theologischen in des Menschen Sprache (seit ich 1996 endgültig, mit meiner Kündigung, das „amtliche Sprechen von Gott" aufgab) Mit verbindlichstem Dank, auch für den ICHTYS Claus Friese”*

> *Thomas Dreessen*

### 7. Entdeckung des Todes – Wissenschaften im Advent

An der Krise, dem geschichtlichen Ereignis, wird der Tod, die Endlichkeit der herrschenden Weltvorstellung offenkundig. Ich möchte an dieser Stelle hinweisen auf vier Sprecher aus Philosophie, Sozialpsychologie und Biologie bei denen ich spüre, dass sie den akademischen Elfenbeinturm öffnen, geöffnet haben. Ist die Zeit nahe, in der Rosenstock-Huessy neu gehört werden wird?

In den Jahren 1919 bis 1929 war die letzte große Zeit der deutschen Philosophie. Über diese Dekade mit Martin Heidegger, Ludwig Wittgenstein, Walter Benjamin und Ernst Cassirer geht es in "Zeit der Zauberer" (2018) dem internationalen Bestseller von Wolfram Eilenberger. Ich frage: Was kommt nach dieser letzten Zeit der Philosophie, Herr Eilenberger?

In die Sternstunde Philosophie vom 7.12. ist Markus Gabriel eingeladen. Er zählt zu den originellsten und mutigsten Denkern der Gegenwart (Uni Bonn) und ist der jüngste Professor in Deutschland – mit sehr hoher Anerkennung und Beachtung: „Falsch sind alle Philosophien der letzten 2500 Jahre!” Ausgehend von der ältesten Frage der Philosophie – der nach dem Unterschied zwischen Sein und Schein – fordert er eine radikale Neubeschreibung unseres Daseins und seiner leitenden Werte. Auch der Schein ist wirklich! Mit einer radikal neuen Philosophie will Markus Gabriel unsere Gegenwart von ihren grössten todbringenden Irrtümern heilen. Er benennt sie als:1.Physikalismus 2. Neurozentrismus 3. Moralischer Nihilismus. Im Zentrum seines „Neoexistentialismus” steht der Mensch als ein Wesen, das ständig in Gefahr steht, den wahren Schein mit dem falschen Sein zu verwechseln. Denn erst wenn der Mensch seine Stellung im Kosmos richtig versteht, öffnet sich ein Weg zur Bewältigung heutiger Sinnkrisen – seien diese politischer, ökologischer, moralischer oder existentieller Art. Denn dem Philosophen sei die Flucht aus der Wirklichkeit prinzipiell unmöglich. Im Gespräch mit Wolfram Eilenberger erklärt der Bonner Philosoph, worin moralischer Fortschritt wirklich besteht, weshalb die Rede vom «postfaktischen Zeitalter» Unsinn ist und warum Fiktionen mindestens so real und rettend sind wie wissenschaftliche Tatsachen. Hören Sie sich dieses Gespräch an und achten Sie darauf, wie begeistert Herr Eilenberger dem Infragesteller zuhört. Ob Markus Gabriel Patmos und die Kreatur kennt?

Sternstunde Kultur vom 7.12.20

Harald Welzer – Jahrgang 1958ist Direktor von Futur2 und vielgefragter Vordenker für die Suche nach Zukunft. Sogar die Gewerkschaften fragen nach seinem Rat. Er sagt: (208f)”Das metaphysische Programm der Moderne besteht .. in der Überwindung von Endlichkeit, ihre Wirtschaftsform ist der säkulare Versuch der Überwindung des Todes.” Und weiter: „In einer Kultur, der es gelungen ist, die einzelnen Lebensläufe aus, die einem folgen…” der Generationenfolge herauszunehmen und so intensiv zu individualisieren, dass sie nur noch für sich vor der Aufgabe maximaler Lebenszeitnutzung stehen, gibt es nicht einmal mehr die tröstliche Vorstellung, dass das Leben Verlängerungen nach hinten zur Vorgängergeneration und nach vorn in die Generationen hat, die einem nachfolgen.” Welzer orientiert seine Hörer: „es gründet das Wissen, das man auf dem Weg in die nachhaltige Moderne gebrauchen kann, auf anderen Voraussetzungen als auf Daten und fakten: Es gründet sich auf Hoffnungen, Wünschen, Träumen und Gefühlen – und auf eine solche Praxis, die solche Produktivkräfte der Zukünftigen ernster nimmt als alle Technik – und Machbarkeitsphantasien.”(247) Glauben – Lieben – Hoffen, die Kräfte, die die Gesellschaft zusammenhalten, werden hier neu entdeckt und gehört. Was können wir Welzer aus unserem Schatz geben und wie?

H.Welzer: Selbst denken – eine Anleitung zum Widerstand 20178

„Ohne Bindung kein Leben!” (9) und „Leben heisst sterben lernen!” heisst ein Kapitel in Andreas Webers Buch: Lebendigkeit – eine erotische Ökologie. Diese Botschaft setze sie „dem Darwinismus, dem Liberalismus und all den herrschenden Verzweckungsideologien entgegen..”(91) „Sterben lernen heisst.., die Wirklichkeit sehen, ohne sie in eine angenehmen Richtung zu bürsten. Nur das bedeutet, wirklich zu sehen. Und nur das heisst, plastisch zu sein, schöpferisch zu sein, ohne sich für seine Unvollkommenheit schämen zu müssen.”(95) Weber kommt über Kalevi Kull von Uexküll her. Nicht zufällig ist seine Sprache zutiefst poetisch und von Poeten inspiriert. Ich habe ihm die Metabiologie Rudolf Ehrenbergs gesandt. Er kannte sie nicht! Wie er mir schrieb. Welch eine Aufgabe ihm und den anderen Biologen, die Bionomie und den bionomischen Charakter der Sprache zu eröffnen.
Die Wiederentdeckung Viktor von Weizsäckers und seines Schülers Kütemeyer in der aktuellen Ausgabe der Zeitschrift Sinn und Form weist in dieselbe Richtung. Kennen Sie Weber?

A.Weber: Lebendigkeit – eine erotische Ökologie, 20143 ISBN 978-3-466-30988-7

> *Thomas Dreessen*

### 8. Bücher

Neuerscheinung:

Wolfgang Ullmann: Eis ho Theos – Der Eine Gott. Die Geschichte von Dogma und Bekenntnis der Kirche. 3 Bände, ISBN: 978-3-8260-6739-6, Herausgeber:Jakob Ullmann, 2020, Preis: 198€
Prof. Jakob Ullmann hat uns die Herausgabe des letzten unvollendeten Werkes seines Vaters Wolfgang Ullmann angezeigt. Viele von uns werden sich an ihn erinnern aus Jahrestagungen oder den Stimmsteinen, als Anstifter der Runden Tische in der DDR und Europapolitiker, als Lehrer am Sprachenkonvikt und tief verbunden mit dem neuen Kreisau. Er fehlt uns in der Krise der Demokratie und der Kirchen.

Bücher aus Nachlässen:

Im Lauf des letzten Jahres sind mit Andreas Möckel (Dezember 2019), Eckart Wilkens und Gerold Neusser Mitglieder verstorben, die unserer Gesellschaft wie dem Werk Eugen Rosenstock-Huessy besonders verbunden waren. Alle Genannten bzw. deren Erben haben verfügt, die hinterlassenen Werke von Rosenstock-Huessy und seiner Freunde sowie bedeutende „Sekundärliteratur" der Eugen Rosenstock-Huessy Gesellschaft zu überlassen. Dankenswerterweise konnten daraufhin einige Lücken in unserem Archiv in Bielefeld-Bethel gefüllt werden. Die etwa 300 weiteren Titel stehen zunächst exklusiv zur Abgabe an Mitglieder unserer Gesellschaft bereit. Fordern Sie bei Interesse an einzelnen Titeln die Gesamtliste beim Vorstand an. Eine Spende zu Gunsten der Gesellschaft wirdje nach individuellen Möglichkeitenerwartet.

> *Andreas Schreck/Thomas Dreessen*



### 9. Termin der Jahrestagung und Mitgliederversammlung 2021

Die Jahrestagung und die Mitgliederversammlung 2021 haben wir vom 19.21.März 2021 (zwei Wochen vor Ostern) im Haus am Turm in Essen-Werden geplant. Die Mitgliederversammlung wird sich mit den Jahren 2019 und 2020 befassen. Wir bitten Sie sich diesen Termin vorzumerken. Ob wir die Tagung und Jahresversammlung durchführen können, sowie Thema und Programm werden wir Ihnen noch mitteilen.

> *Jürgen Müller*

### 10. Adressenänderungen

Bitte senden sie eine eventuelle Adressenänderung schriftlich oder per E-mail an Thomas Dreessen (s. u.), er führt die Adressenliste. Alle Mitglieder und Korrespondenten, die diesen Brief mit gewöhnlicher Post bekommen, möchten wir bitten, uns soweit vorhanden ihre Email-Adresse mitzuteilen.

> *Thomas Dreessen*

### 11. Hinweis zum Postversand

Der Rundbrief der Eugen Rosenstock-Huessy Gesellschaft wird als Postsendung nur an Mitglieder verschickt. Nicht-Mitglieder erhalten den Rundbrief gegen Erstattung der Druck- und Versandkosten in Höhe von € 20 p.a. Den Postversand betreut Andreas Schreck. Der Versand per e-Mail bleibt unberührt.

> *Andreas Schreck*

### 12. Mitgliederbeitrag 2020 und 2021

Die „Selbstzahler” unter den Mitgliedern werden gebeten, den Jahresbeitrag in Höhe von 40 Euro (Einzelpersonen) auf das Konto Nr. 6430029 bei Sparkasse Bielefeld (BLZ 48050161) zu überweisen. IBAN-Kontonummer: DE43 4805 0161 0006 4300 29; SWIFT-BIC: SPBIDE3BXXX

Die Lastschrift-Abbuchung des Beiträge 2020 und 2021 erfolgen in Kürze. Soweit sie kein Mandat erteilt haben, nehmen Sie bitte die Überweisung vor, soweit noch nicht geschehen.

> *Andreas Schreck*

[zum Seitenbeginn](#top)
