---
title: "Marlouk Alders: tweede reactie op Wilmy Verhage: Dochters"
category: weitersager
order: 13
published: 2022-01-30
---

Dochters\
Die Tochter – Das Buch Rut


>*April/Mei 2010*


Lieve Wilmy,

wat mij bezielt:                                                     

In mijn eerste antwoord (oktober 2007) op jouw brief over het boek “Die Tochter/Das Buch Ruth” heb ik gewezen naar mijn verleden en naar waarom ik “Die Tochter” niet lezen kon. Ik wist dat mijn antwoord aan jou niet af was. Waarom dat zo voelde, wist ik niet. Ik had enkel een fundamenteel gevoel dat het echte antwoord nog gegeven moest worden. Het was van wezenlijk belang en had te maken met “De Dochter”. Wat er aan ontbrak, zo weet ik nu, was het wijzen naar de toekomst, mijn toekomst!

Ik heb mijn belofte van toen gehouden en heb het boekje “Die Tochter/Das Buch Ruth” opnieuw open geslagen. Ik vertel je hoe het mij daarbij is vergaan:

Ik heb het talloze malen gelezen en elke keer dat ik het las, ervoer ik het weer anders en iedere keer las ik weer iets nieuws. Steeds opnieuw had ik andere vragen: Klopte het wat daar stond of wat ik er van begreep? Was ik het daarmee eens of niet? Wat betekende dat? Betekende dat iets voor mij?

De ene keer kwam het mij voor dat Eugen Rosenstock-Huessy alle mannen de grond in boorde en vrouwen ophemelde.

De andere keer worstelde ik met: Gaat een man mij nu dicteren hoe het verder moet? Word ik hier inderdaad opgezadeld met een nieuw vorm van bevoogding? En er was verontwaardiging: Waarom moeten de vrouwen het opknappen terwijl de mannen het verknoeid hebben? Waarom moeten vrouwen het gewonde hart van de man redden? Wat te denken van de mannen die het hart van de vrouw hebben verwond!
En in het verlengde daarvan:
Wat en waar is de verantwoordelijkheid van de mannen zelf? In hoeverre is dit alles geschreven vanuit het mannelijk (failliete) perspectief? Is het hier niet het perspectief van de dochter dat juist ontbreekt? Is het niet beter als vrouwen zelf bedenken, vanuit dat vrouwelijke denken, hoe het in hun opinie verder moet? Ik hield een uitgesproken slag om de arm en was zeer kritisch bij het lezen!

Ook raakte ik verstrikt in naar wie Rosenstock-Huessy nu eigenlijk wees en op een gegeven moment scheen het mij toe dat hij het zelf ook niet altijd precies wist. Ondanks dat ik meende te begrijpen hoe hij het bedoeld had, leek alles een pot nat of op zijn minst een stoeipartij met woorden:
De moeder (de/een vrouw) en/of het moederlijke/het vrouwelijke in de mens. Een dochter (de/een vrouw) en/of de dochter/het vrouwelijke in de mens. De vader (de/een man) en/of het vaderlijke/het mannelijke in de mens. De zoon (de/een man) en/of de zoon/het mannelijke in de mens. Over wie ging het?
En ik verbaasde me over het feit dat men wel moederlijk/vrouwelijk/vaderlijk/mannelijk kan zijn, maar niet dochterlijk, laat staan zoonlijk. De Dikke van Dale bracht geen uitkomst.

Ik verloor mij, telkens weer, in alle belangrijke bijzinnen en uitwijdingen. De kern van wat er stond, kreeg ik (nog) niet te pakken.

En dan mijn worsteling met het woord dienen of dienstbaarheid (nog erger). Dienstbaarheid word vaak ingevuld als de zorg voor anderen (vaak een man) in ondergeschiktheid en gehoorzaamheid (meestal een vrouw). Soms uit eigen belang, opdat men zich goed kan voelen over zichzelf of aardig gevonden kan worden door de ander. De “disease to please”! Of dienstbaarheid in ondergeschiktheid en gehoorzaamheid uit angst, omdat je de ander machtiger weet dan jezelf. Met als triest gevolg dat je jezelf wegcijfert uit lijfs- en/of levensbehoud en daardoor ontrouw bent aan jezelf in lichaam, geest en ziel. Dienen en dienstbaarheid hebben daardoor als woorden een wrange bijsmaak gekregen.

Een van de zijwegen die ik insloeg, was een speurtocht naar een positievere uitleg van dit woord en het onderscheid dat gemaakt dient te worden! De Russische priester/filosoof Zelinsky zegt het als volgt: “Echte dienstbaarheid weigert de mens te reduceren tot de functie die hij vervult, de functie van werkkracht, de functie van consument.” (En welke functies er nog meer te vervullen zijn!)
“Echte dienstbaarheid overstijgt tegenstellingen en verdeeldheid, slaat bruggen en brengt verzoening tussen personen, tussen gemeenschappen, tussen mens en natuur, tussen de mens en God. Zo is de bereidheid om te dienen en gediend te worden van essentieel belang voor het voortbestaan van de mensheid.”

Het “Nee” zeggen, zoals Eugen Rosenstock-Huessy een aantal keren deed en daarbij een bewuste keus maakte de wereld te dienen zoals hij met hart, ziel en geest ervoer dat nodig was, kun je ook zo zien. Dienen is vanuit een eigen kern aanreiken wat je bezielt. Of zoals in de inleiding van Dienen op de planeet staat: “Wat mij be-stem-t.”

En als ik dan nog eens kijk naar het woord dienstbaarheid, dan blijkt dat het niet zozeer gaat om wat ik kan of wil geven of een nieuwe vorm van bevoogding. Nee, het gaat veel meer om mijn vermogen te onderscheiden of het een levende of dode, een zuivere of bedorven, een echte of onechte zaak dient. Het gaat over inhoud en betrekking. (Watzlawick) En om mijn ja en nee daarop en om het juiste moment!

Een volgende keer dat ik het boekje las, kon ik zien hoe Eugen Rosenstock-Huessy in het jaar 1919 al probeerde duidelijk te maken hoe zeer het nodig was ruimte en tijd te maken voor dit vrouwelijke perspectief, de vrouwelijke manier van reageren. En dan vooral in de gestalte van de dochter.

Ik kreeg meer grip op de kern van het boek, maar mijn antwoord kon ik je nog steeds niet geven. Ik besloot het toeval te laten regeren en gaf mijn zoektocht op. Ik moest erop vertrouwen dat mijn antwoord geboren zou worden als de tijd daar was. Zoals je me ook doceerde!

En het gebeurde! Diverse mensen reikten mij ontbrekende puzzelstukjes aan. Dienen en gediend worden.

Uit een van de dozen met de nalatenschap van jouw tante Ko kwam een stapeltje papieren. Allemaal toegespitst op “De Dochter”. Het leek of ze vanuit de hemel mij alsnog wijze woorden schonk en me raad en uitleg gaf. In de taal die ik begreep, maakte ze mij verder wegwijs in hoe  “De Dochter” begrepen kon worden. Een cadeautje uit het hiernamaals! Ik citeer hieruit wat voor mij belangrijk was:

Ko Vos in de voorloper van “De Rozenstok” nieuwsbrief oktober 1988

“De man in ons is gericht op de ruimte waarin wij leven, hij wil de problemen zakelijk, technisch en snel oplossen. De vrouw in ons kijkt heel anders aan tegen de problemen van honger, oorlog en een vervuild milieu. De vrouw in ons weet van de gang door de tijden, ze weet dat het generaties duurt voor nieuwe dingen hun beslag krijgen. Ze durft te verwachten, ze weet dat geboren worden een heel ander proces is dan iets organiseren of maken. Oorlogen en geruchten van oorlogen zijn voor de vrouw in ons de geboorteweeën van een nieuwe tijd. Weeën laten zich niet wegwerken, daar moet je doorheen.”

De tegenpool van de moeder: “de dochter, die niet naar het verleden maar naar de toekomst is gericht en daar het “alsjeblieft” uitspreekt. Ook dat ambt moet geleefd worden willen we het mens-zijn niet verliezen. Het betekent dat we in de toekomst moeten geloven. Het woord “geloven” is een moeilijk en vooral zwaar belast woord. Iedereen denkt er wat anders bij. Maar hier gaat het om de simpele en elementaire functie van het mens zijn naar de toekomst toe. De toekomst ken je niet, die weet je niet, die kun je niet berekenen, je moet erin geloven, anders ga je die toekomst niet binnen.”

“De dochter spreekt een taal die je op geen enkele cursus leren kunt. Ze heeft niet geleerd, ze heeft geen diploma’s. Ze volgt de stem van haar hart. Omdat ze met hart en ziel de toekomst toebehoort en die toekomst verwacht, heeft ze geleerd de vele stemmen die om haar heen klinken te onderscheiden. De dochter kan “horen”. Haar hart zegt haar tegen welke stemmen ze “ja” en tegen welke ze “nee” moet zeggen. Ze zegt “ja” tegen de stemmen die de schepping naar de bestemming voeren, ze zegt “nee” tegen de stemmen die de vrede blokkeren. Haar hart beslist die keuze.”

“Als vandaag de roep om de vrouw naar voren komt dan heeft dat alles te maken met het feit dat het heersen (beheersen, overheersen) plaats moet maken voor dienen en dienstbaar zijn. Het heeft ook alles te maken met de verhouding der geslachten waar we ook tot over onze oren in de problemen zitten. Het is in feite de roep om de dochter. De dochter die gelooft in een toekomst, in een nieuwe tijd en die zo goed kan “horen”, dat ze alleen nog die man wil toebehoren bij wie ze als bruid de bruidegom op kan roepen. Anders gezegd, de man die nog bereid is tot verandering, tot omkeer, tot dienst aan de planeet. De kinderen uit zo’n huwelijk geboren zullen weer namen dragen die vol belofte zijn!”

Ook vond ik twee brieven die me troost gaven en me, net als jij vertelden, dat het niet zo erg was dat ik moeite had de finesse van “Das weisse Buchlein” te begrijpen. Het was Freya von Moltke die in een brief aan Ko Vos schreef: “Wir sind erst im Anfang” en “Und ich muss sie noch weiter ofters lesen.” En Ko schreef in haar antwoord: “Ich denke dass Du recht hast, dass dies kaum noch verstanden werden kann.”:

Brief van Freya aan Ko, 1 maart 1989 over het net verschenen “Weisse Buchlein”:

“Ich kann mir ja immer nicht vorstellen, dass “Die Tochter” verstanden wird. Ich glaube ich weiß ein gut Teil von dem, was drin steht. Drin ist auch Eugens Prophetie. Ich weiß, woraus alles geflossen ist. Aber es steht mehr drin als Eugen selber weiß. Das ist das Grosse. Ich glaube, die Frauen von heute sind gezwungen, Umwege zu machen. Darum fürchte ich, dass sie keine Ohren für „Die Tochter“ haben. Vielleicht die Männer - aber auch nur wenige, denn die Männerwelt aufzubrechen, das wird mehrere 100 Jahre brauchen. Wir sind erst am Anfang.”

“Als Eugen begann, mir Briefe zu schreiben, hat er zuerst gemeint, ich brauche garnichts von ihm zu lesen. Dann beginnt er aber mehr und mehr mir Sachen ans Herz zu legen zum lesen. Da ist „Die Tochter“ ganz vorn. Und ich muss sie noch weiter ofters lesen. Das weiß ich.”

Antwoord van Ko aan Freya, 18 maart 1989

“Denn schreibst Du noch über die Tochter. Ich denke dass Du recht hast, dass dies kaum noch verstanden werden kann. Doch bin ich überzeugt dass es die höchste Zeit ist dass auch diese Stimme jetzt da ist. Gerade weil man überall bespürt die Frau ist irgendwie an die Reihe, sollen alle Stimmen da sein und kann Eugens Stimme nicht fehlen. Er bringt eine Dimension die soweit ich weiß nur ihm gegeben ist zu sehen.”

Is het niet mooi dat juist deze vrouwen in dit “Weitersagen” opnieuw hun stem kunnen laten horen?

“Stel je vertrouwen op God voor steun en zie zijn verborgen hand werkend door alle bronnen.”

Je schreef in jouw brief dat Ruth erin slaagde een eigen plek te krijgen, door het toeval te laten regeren en de kennis, ervaring en intuïtie van Naomi te volgen. Door het toeval te laten regeren kon ik de kennis, ervaring en intuïtie van Ko en Freya volgen. Maar slaag ik in die eigen plek? Ben ik een van de verworpenen van wie niemand iets verwacht en die met een alles omverwerpende kracht vrij de schepping binnentreedt?

Toen ik “Die Tochter” daarna nog een keer las, kon ik voor mezelf de kern van het boek samenvatten:
“Uit het geestelijk failliet van het mannelijk denken, moet uiteindelijk een nieuw antwoord geboren worden. We moeten ophouden de vrouw te zien als verleiding/verlokking/speelbal, haar niet ketenen en kleineren. We moeten ophouden de bron te vervuilen. Omdat het mannelijk denken teveel heeft ingegrepen in de krachten van de menselijke natuur en in de schepping en daarbij de bodem uitgeput en verwoest heeft door de roofbouw die het pleegde, is het, met het oog op de toekomst, belangrijk dat het eeuwige vrouwelijke dat geduld en lijden bezit, in de gestalte van de dochter bezield aan het werk gaat en met bloed, zweet en tranen dat levenssap brengt naar Gods akker, zodat de kiem in het verdorde zaad weer kans krijgt om te ontkiemen. Daaruit ontstaat een nieuwe plant. En alleen zo kan de steppe weer tot bloei komen en zal de aardschoot opnieuw vrucht en zaad dragen. Zo word de nieuwe geest verwekt en gaat hij weer stromen uit de bron. Door oog te hebben voor vorm en schoonheid, bederf en zuiverheid, dood en leven, onecht en echt kun je ontdekken wie “de dochter” werkelijk is. Opdat wij uit liefde voor elkaar, met elkaar een huwelijk aan gaan dat ons heilig is. Om zo de samenleving te dienen. Want: “Het leven is de liefde en de liefde levens geest!”

Op het moment dat ik als (pleeg)dochter opgenomen dacht te worden in een gezin, kreeg ik van de beoogde pleegouders het boek Die Tochter - Das Buch Ruth. De vader van dat gezin schreef als persoonlijk voorwoord: “dat mijn leven zich nu (1990) inschreef in de verhalen van al die grote dochters die ons menselijk ras opriepen tot de eenheid van een groot gezin om zo ooit tot vrede te geraken. En dat ik weer recht stond in de rij van al die vrouwen (…) die het licht, temidden van mannelijk begeren en krijgen, heen droegen naar het einde van die strijd.”

In de correspondentie die we naar aanleiding van mijn eerste brief hadden, maakte je er een punt van dat ik het woord tijding gebruikte voor het persoonlijke voorwoord. Je onderwees me: “Zo’n persoonlijk voorwoord in een boek, heet een persoonlijke opdracht.”
En ik liet je weten dat ik er goed over nagedacht had en dat ik voor het woord tijding had gekozen omdat het gewag maakte van iets wat op een bepaalde tijd gemeld werd. Niet vroeger of later maar toen op die tijd. En die tijd, dat moment, was exact waarom het voor mij op een worsteling met “De dochter, de pleegdochter en Die Tochter” was uitgelopen. Mijn basta was: Het moet tijding heten! Meer toelichting kon ik niet geven. Die krijg je nu:

Hoewel ik die tijding in eerste instantie als een persoonlijke opdracht had opgevat, was het ook zo dat ik die opdracht intuïtief weer afwees op het moment dat ik uit(af)geschreven werd als (pleeg)dochter! Vanaf dat moment kon ik het alleen nog als tijding zien die mij gedaan was!

Ruth kiest in zielsverbondenheid, met een fundamenteel vertrouwen in wie ze is en mag zijn. Zij schat zichzelf naar waarde. Daardoor wordt ze gezien en krijgt ze de plek die haar toekomt. Zij wordt naar waarde geschat! Dienen en gediend worden. Daar waar haar bestaan in scherven uiteen viel, keek ze niet naar hoe ze lijmen moest, maar naar hoe uit die gebeurtenissen iets nieuws kon ontstaan dat weer hoop op de toekomst gaf.

De tijd na jouw brief ben ik bezig geweest het juk van betutteling en bevoogding nog verder van mij af te werpen. Ik moest ook afrekenen met de tijding! Bijvoorbeeld met de bepaling dat mijn leven zich blijkbaar in 1990 inschreef in de verhalen van grote dochters. Werd ik toen werkelijk ingeschreven? Nee! Ik werd met dezelfde vaart weer uit het register van inschrijving geschrapt! Er zat geen echte toekomst in het pleegdochter zijn. Het bracht me in een spagaathouding waar ik niet makkelijk uit kwam. Ingeschreven worden op deze manier kon ontaarden in een nieuwe vorm van bevoogding.

En stond ik werkelijk, op dat moment, ineens weer recht in de rij van al die vrouwen die het licht, temidden van mannelijk begeren en krijgen, heen droegen? Of was ik zo’n verworpene der aarde waarvoor geen plaats was in de herberg? Was ik bij mijn geboorte ingeschreven als dochter? Mijn vader is dat, in zijn mannelijk begeren en krijgen, zeker vergeten! Ik droeg slechts een flakkerend lichtje en dat maakte mijn toekomst onzeker. Ik had tijd en ruimte nodig om die bagage achter me te laten. Ik had tijd en ruimte nodig om te kijken hoe uit die gebeurtenissen iets nieuws kon ontstaan dat weer hoop op de toekomst gaf.

Net als Ruth kon ik me daarin laten leiden, door de geloofskracht van dochters die mij voorgegaan zijn. In zielsverbondenheid, met een fundamenteel vertrouwen in wie ik ben en mag zijn. Zo schat ik mijzelf naar waarde! In mijn verleden werd ik niet gezien en kreeg ik niet de plek die mij toekwam. Ik werd niet naar waarde geschat. In de toekomst kan ik worden gezien en de plek krijgen die mij toekomt. Ik kan naar waarde worden geschat. In de toekomst ligt wat mij toekomt. Dienen en gediend worden!

Het werd duidelijk dat het erom ging dat ik mijzelf inschreef. Door mijzelf in te schrijven in het leven, in de verhalen van al die grote dochters die ons menselijk ras oproepen tot de eenheid van een groot gezin, kan dit alles ooit tot vrede geraken. Ik schaar mijzelf in de rij van al die vrouwen (Ruth, Ko, Freya, ........) die het licht, temidden van mannelijk begeren en krijgen, heen droegen naar het einde van die strijd. Door me te richten op de toekomst, kan ik met oerkracht vrij de schepping binnentreden. Dat is het antwoord dat ik je diende te geven!

En bij deze inschrijving blijkt dan, dat ik het woord tijding kan laten vallen en het persoonlijke voorwoord weer kan oppakken als een persoonlijke opdracht.

Ruth vraagt Boaz: Waarom genade gevonden in uw ogen, dat gij mij kent, daar ik een vreemde ben?
Boaz antwoordt: Je hebt het avontuur aangedurfd.

En er welt zowaar een lied omhoog: Wat de toekomst brengen moge, mij geleidt des Heren hand. Moedig sla ik dus de ogen naar het onbekende land.

Liefs,

Marlouk
