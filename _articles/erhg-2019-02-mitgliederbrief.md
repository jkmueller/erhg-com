---
title: Mitgliederbrief 2019-02
category: rundbrief
created: 2019-02
summary: |
  1. Einladung zur Jahrestagung und Mitgliederversammlung - Jürgen Müller
  2. Tagungsort und Anmeldung                             - Andreas Schreck
  3. Programm der Jahrestagung                            - Jürgen Müller
  4. Tagesordnung der Mitgliederversammlung               - Jürgen Müller
  5. Adressenänderungen                                   - Thomas Dreessen
  6. Hinweis zum Postversand                              - Andreas Schreck
  7. Jahresbeiträge 2019                                  - Andreas Schreck
zitat: |
  >Es gilt heut gegen all die Fronten zu fechten, die uns um eine der drei Gewalten Leib, Geist oder Seele, betrügen wollen.\
  >Nicht darum darf der heidnische Geist zerstört worden sein, damit die heidnische Lebenskraft vernachlässigt werde.\
  >Nicht darum darf der christliche Leib, darf der Kirchenstaat und die Staatskirche zerstört worden sein, damit uns der christliche Geist verloren gehe.\
  >Nicht darum darf der Jude den christlichen Geist und seine Gedankenwelt auf sich genommen haben, sodaß er nun nicht nur der eigenen Erde, sondern auch des eigenen Geistes darbt, damit die jüdische Seele zertreten werde.\
  >Ein jeder stehe also in seinem Lager.\
  >Aber ein jeder ahne jenseits auch die Dreipersönlichkeit der lebendigen Gestalt.\
  >Ein jeder wisse, daß sein eigenes Drittel ein Jenseits hat, in dem es erst versöhnt wird.
  >*Eugen Rosenstock-Huessy, Die Hochzeit des Krieges und der Revolution, 1920*
---

## Eugen Rosenstock-Huessy Gesellschaft e.V.

{{ page.zitat }}

**Vorstand/board/bestuur:**\
*Dr. Jürgen Müller (Vorsitzender);*\
*Thomas Dreessen; Andreas Schreck; Dr. Eckart Wilkens*\
*Antwortadresse: Jürgen Müller: Vermeerstraat 17, 5691 ED Son, Niederlande*\
*Tel: 0(031) 499 32 40 59*

***Brief an die Mitglieder Februar 2019***

**Inhalt**

{{ page.summary }}

### 1. Einladung zur Jahrestagung und zur Mitgliederversammlung

Unser diesjähriges Thema der Jahrestagung: „Was ist unsere Heimat nach dem Weltkrieg? - Rosenstock-Huessys Zeitansage 1919” ist eine Fortführung des Themas des letzten Jahres:
„Deutsch – Europa – Planet Erde: Wie übersetzen wir den Volksnamen Deutsch in die Sprache des Menschengeschlechts?”

Das letztjährige Thema basierte auf einem Text, den Rosenstock-Huessy gegen Ende der Zwischenkriegszeit schrieb, als er ahnte, daß seine Vorhersage eines Lügenkaisers in Erfüllung gehen konnte. 1919 hatte er geschrieben: „Denn alles Nationale ist selbst ein geistiger Dornröschenschlaf, ist eine Verzauberung, in die sich die Völker auf ihrem dunklen Gange für Jahrtausende hineinverirrt haben. Kein Volk saß tiefer im Märchen und in der Sage, als die Deutschen. Keines hatte leidenschaftlicher die Siegfriedsage und den Wotanglauben dem „römisch-orientalischen” Kreuzesglauben zuwider entwickelt, bis ums Jahr 1900 sogar die Gebildeten nicht mehr wußten, daß Heldensage und Edda- glaube erst als trotziger Widerspruch gegen die Offenbarung zur Entfaltung gebracht worden sind. Damit war ein letzter Höhepunkt des Heidentums erreicht; als schon kein Großmütterlein im niedern Volk mehr an Zwerge oder Riesen glaubte, da glaubten die Gebildeten um so krampfhafter, daß ihre Altvordern einer reifen „eigenen Religion” angehangen hätten. Die geistigen Träger Neudeutschlands vermuteten in ihren Ahnen statt der trüben Barbaren, die in ihrem verzweifelten Dunkel das Licht des Geistes von Osten froh begrüßt hatten, Lichtgestalten und Weisheitskünder.” 1928 in: „Unser Volksname Deutsch und die Aufhebung des Herzogtums Bayern” zerstörte er den Mythos eines germanischen Ursprungs Deutschlands.

Im diesjährigen Text: „Der Heimfall der Heimat” beschreibt Rosenstock-Huessy Deutschland, nach Unterzeichnung des Versailler Vertrages, als erstes Land in einer ganz neuen Lage. Inzwischen ist diese damalige deutsche Lage vielerorts Wirklichkeit geworden.
Dieser Aktualität des Textes für unsere momentane politische Situation wollen wir uns stellen um gemeinsam Wege zu finden, die herausführen aus den vorherrschenden Gegensätzen.

Im Rahmen der Tagung wird Eugenio Muinelo von der Universität Complutense, Madrid sein Dissertationsprojekt unter dem Arbeitstitel: „Eugen Rosenstock-Huessy als Denker jenseits der Fakultäten” vorstellen.

Ich lade Sie zur Jahrestagung und zur Mitgliederversammlung (am 13. April 2019) herzlich ein.
>*Jürgen Müller*

### 2. Tagungsort und Anmeldung

Die Jahrestagung findet vom 12. bis 14. April 2019 statt im Haus am Turm, am Turm 7, 45239 Essen-Werden, Tel. 0201-404067. Beginn ist am 12. April 2019 um 18 Uhr mit dem Abendessen.
Kosten
Die Kosten betragen € 120 bei Unterbringung im Doppelzimmer, € 145 bei Unterbringung im Einzelzimmer. Die Anzahl der Einzelzimmer mit Bad/WC ist begrenzt.
Interessenten, die die Tagungskosten nicht in voller Höhe aufbringen können, mögen sich bitte an Andreas Schreck oder ein Vorstandsmitglied wenden.

Anmeldung
bitte an Andreas Schreck, Tel. 0551-28047871; schreckschrauber@web.de
>*Andreas Schreck*

### 3. Programm der Jahrestagung

*„Was ist unsere Heimat nach dem Weltkrieg? -
Rosenstock-Huessys Zeitansage 1919”*

| Freitag, 12.4 | 18:00 | Eintreffen |
|               | 18:30 | Abendessen |
|               | 19:30 | "Der Heimfall der Heimat (I-IV): Heimatlos 1919! Jeder 2019 (Thomas Dreessen)" |
|               | 21:00 | Geselliges Beisammensein |
| Samstag, 13.4 |  8:30 | Frühstück |
|               |  9:30 | "Der Heimfall der Heimat V: Die Prophetie des Lügenkaisers (Andreas Schreck)" |
|               | 11:00 | Pause |
|               | 11:30 | "Der Heimfall der Heimat VI: Kabelstück – Authentisch und die Bescheidenheit des Glaubens (Andreas Schreck)" |
|               | 12:15 | Pause |
|               | 12:30 | Mittagessen |
|               | 14:30 | Mitgliederversammlung |
|               | 16:30 | Pause |
|               | 17:00 | "Der Heimfall der Heimat (Schluß I): Neue ahnenlose Menschen: Heide, Jude, Christ (Eckart Wilkens)" |
|               | 18:30 | Abendessen |
|               | 20:00 | "Die unsichtbare Welt und wie wir sie erleben Gesang und Gespräch (Eckart Wilkens)" |
|               | 21:30 | Geselliges Beisammensein |
| Sonntag, 14.4 |  8:00 | Morgenandacht |
|               |  8:30 | Frühstück |
|               | 10:00 | "Der Heimfall der Heimat (Schluß II): Ein jeder stehe also in seinem Lager (Jürgen Müller)" |
|               | 11:30 | Abschlußrunde |
|               | 12:30 | Mittagessen |
|               | 13:00 | Abreise |


### 4. Einladung zur ordentlichen Mitgliederversammlung
am 13. April 2019, 14:30 Uhr\
Haus am Turm, Am Turm 7, 45239 Essen

TOP 1: Begrüßung der Mitglieder, Feststellung der Beschlussfähigkeit
               und Festlegung der Protokollführung\
TOP 2: Genehmigung des Protokolls der Mitgliederversammlung vom 24. März 2018 in Essen\
TOP 3: Anträge zur Änderung/Erweiterung der Tagesordnung\
TOP 4: Vorstellung neuer Mitglieder\
TOP 5: Bericht und Ausblick des 1. Vorsitzenden mit Aussprache\
TOP 6: Kassenbericht für das Geschäftsjahr 2018\
TOP 7: Berichte der Kassenprüfer\
TOP 8: Entlastung des Vorstands für das Geschäftsjahr 2018\
TOP 9: Buchprojekte\
TOP 10: Berichte von Respondeo, vom Eugen Rosenstock-Huessy Fund und der Society und von Projekten einzelner Mitglieder\
TOP 10: Wahl eines Wahlleiters/einer Wahlleiterin für die Neuwahl des Vorstands\
TOP 11: Wahl des/der 1. Vorsitzenden\
TOP 12: Wahl des/der stellvertretenden Vorsitzenden\
TOP 13: Wahl des dritten geschäftsführenden Vorstandsmitglieds\
TOP 14: Wahl zweier weiterer Vorstandsmitglieder\
TOP 15: Verschiedenes

### 5. Adressenänderungen

Bitte schreiben sie eine eventuelle Adressenänderung schriftlich oder per E-mail an Thomas Dreessen (s. u.), er führt die Adressenliste. Alle Mitglieder und Korrespondenten, die diesen Brief mit gewöhnlicher Post bekommen, möchten wir bitten, uns soweit vorhanden ihre Email-Adresse mitzuteilen.
>*Thomas Dreessen*

### 6. Hinweis zum Postversand

Der Rundbrief der Eugen Rosenstock-Huessy Gesellschaft wird als Postsendung nur an Mitglieder verschickt. Nicht-Mitglieder erhalten den Rundbrief gegen Erstattung der Druck- und Versandkosten in Höhe von € 20 p.a. Den Postversand betreut Andreas Schreck. Der Versand per e-Mail bleibt unberührt.
>*Andreas Schreck*

### 7. Jahresbeiträge 2019

Die „Selbstzahler” unter den Mitgliedern werden gebeten, den Jahresbeitrag in Höhe von 40 Euro (Einzelpersonen) auf das Konto Nr. 6430029 bei Sparkasse Bielefeld (BLZ 48050161) zu überweisen.\
Nach Einführung des einheitlichen Zahlungssystem SEPA mit der Internationalen Kontonummer (IBAN): DE43 4805 0161 0006 4300 29
SWIFT-BIC: SPBIDE3BXXX ist diese „IBAN”-Nummer auch für Mitglieder in den Niederlanden und im ganzen „SEPA-Land”, in das sich sogar die Schweiz hat einbetten lassen, ohne Zusatzkosten nutzbar.
>*Andreas Schreck*

[zum Seitenbeginn](#top)
