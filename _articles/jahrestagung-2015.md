---
title: Jahrestagung 2015
created: 2015
category: jahrestagung
thema: Das wirkliche Lebensalter des Lehren?
---
![Imshausen]({{ 'assets/images/Imshausen.jpg' | relative_url }}){:.img-right.img-large}

### {{ page.thema }}

Grundlegender Text:\
***Eugen Rosenstock-Huessy: Das Versiegen der Wissenschaften und der Ursprung der Sprache (VI)***,\
in: *Die Sprache des Menschengeschlechts, Band 1, pp. 676-683, Lambert Schneider, 1963*

Datum: 23-25.10.2015
Ort: Stiftung Adam von Trott, Bebra-Imshausen

---

Programm Jahrestagung 2015

| Freitag, 23. Oktober 2015 | 18.30 Uhr | Empfang und Abendessen |
|                           | 19.30 Uhr | Begrüßung und Präsentation der 4 Töne |
|                           |           | Stifte (Eckart Wilkens), Lehre (Andreas Schreck), Prophezeie (Thomas Dreessen), Regiere (Jürgen Müller) |
| Samstag, 24. Oktober      |  8.30 Uhr | Frühstück |
|                           |  9.30 Uhr | Textlesung |
|                           | 11.00 Uhr | Pause |
|                           | 11.15 Uhr | Gruppengespräch |
|                           | 12.30 Uhr | Mittagessen und Pause |
|                           | 14.30 Uhr | Vorbereitung der Mitgliederversammlung |
|                           | 15.15 Uhr | Pause |
|                           | 16.00 Uhr | Weiterarbeit am Tagungsthema |
|                           | 18.00 Uhr | Abendessen |
|                           | 19.00 Uhr | Mitgliederversammlung mit Vorstandswahl |
| Sonntag, 25. Oktober      |  7.30 Uhr | Andacht |
|                           |  8.30 Uhr | Frühstück |
|                           |  9.30 Uhr | Was sollen wir, die ERHG, zur Stärkung der Argonautik jetzt tun? |
|                           | 11.00 Uhr | Pause |
|                           | 11.15 Uhr | Abschlußrunde |
|                           | 12.30 Uhr | Mittagessen und Ende der Tagung |

Anmeldung bitte an Andreas Schreck, Tel. +49 (0)551-28047871; schreckschrauber :at: web.de
