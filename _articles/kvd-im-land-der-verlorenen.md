---
title: "Klaus von Dohnanyi: Im Lande der Verlorenen"
created: 1991
category: veroeff-elem
published: 2024-04-23
---
#### Stimmstein 6, 2001

#### Mitteilungsblätter  2001

### Klaus von Dohnanyi

### Im Lande der Verlorenen

#### Zum Tode von Sabine Leibholz-Bonhoeffer

Am 7. Juli starb in ihrem 94. Lebensjahr Sabine Leibholz in Göttingen. Sabine Leibholz war die letzte der einmal acht Bonhoeffer-Geschwister; mit ihr verlöscht diese große Familie endgültig, auch wenn es weit verstreut in Deutschland und Europa Enkel und Urenkel gibt. Doch das, was diese Familie einmal zusammenhielt, das gibt es schon lange nicht mehr: Jenes geistige, ständig um seine Kultur ringende Deutschland. Das Deutschland, dessen unruhige innere Suche nach Sinn und Aufgabe seine unvergleichbare Stärke, aber eben auch eine unpolitische Schwäche war, die dann in den Abgrund führte. Eine Gefahr, die auch Thomas Mann in den „Gedanken eines Unpolitischen“ (1918) nicht sehen wollte.

Die Berliner Bonhoeffer-Familie wurde mit diesem alten Deutschland zerstört. Ein älterer Bruder von Sabine Leibholz, Walter, fiel schon im Ersten Weltkrieg. Ihr Zwillingsbruder Dietrich und ein ebenfalls älterer Bruder Klaus wurden von den Nazis ermordet, ebenso wie zwei Schwäger, Rüdiger Schleicher und Hans von Dohnanyi. Sie selbst, mit Gerd Leibholz, dem hochbegabten Rechtswissenschaftler verheiratet, mußte Deutschland 1938 wegen dessen „nicht-arischer“ Abstammung fliehen, und als sie dann 1947 mit ihm und ihren beiden Töchtern aus der neuen Heimat England zurückkehrte, fand sie ein geteiltes und zerstörtes Land. Deutschland, wie sie es geliebt und gekannt hatte, war für immer vergangen.

Adolf Hitler, dieser österreichische Zufall auf preußischem Boden, dessen böse Gewalt soviel Gutes in Deutschland in so kurzer Zeit in verbrecherische Energien verwandeln konnte, wirft noch heute seinen Schatten auf diese alte, vergangene und verschüttete Zeit. So, als habe Auschwitz eine logische Wurzel in der deutschen Geschichte. Aber zu dieser Geschichte gehörten ja gerade auch die vielen jüdischen Deutschen, die dieses Land liebten und von ihm kaum lassen wollten, als seine Regierung mit Mord drohte. Gerade die Zwiespältigkeit des alten Vor-Hitler-Deutschland machte seine kulturelle Bedeutung aus.

Das Vor-Hitler-Deutschland war eben ganz anders, als viele simple Vereinfacher im Nach-Hitler-Deutschland heute meinen. Karl-Friedrich Bonhoeffer (ein weiterer älterer Bruder Sabines, nach dem das Max-Planck-Institut für physikalische Chemie in Göttingen benannt ist) schrieb noch 1931 von Frankfurt aus an seinen Bruder Dietrich in New York, er selbst habe einmal einen Ruf nach Harvard wegen des amerikanischen Rassismus abgelehnt, denn im Vergleich zu diesem sei das, was man in Deutschland an Antisemitismus spüren könne, fast ohne Bedeutung. 1931 aus der Frankfurter Universität!

Die Bonhoeffers waren eine Familie zwischen Wissenschaft (auch Max Delbrück ein Schwager), Theologie, Musik und den Künsten (Sabine Leibholz hat selbst bildhauerisch gearbeitet), eine Familie eigentlich fern der Politik, aber nahe an bürgerlicher Verantwortung und erzogen im zivilen Anstand: doch am Ende wurde die Familie zerstört eben von dieser zu lange auf Abstand gehaltenen Politik. Sabine Leibholz selbst hat hierüber in „Vergangen, erlebt, überwunden“ (1968) berichtet, ebenso wie Eberhard Bethge in seiner bedeutenden Biographie Dietrich Bonhoeffers.

Manchmal scheint es mir, als spiegele die Familie Bonhoeffer in diesem zu Ende gehenden „deutschen“ Jahrhundert Deutschlands Schicksal beispielhaft wider: Mit soviel Hoffnung hatten sie alle begonnen; begabt, ernst, verantwortungsvoll − und patriotisch. Sie scheiterten und wurden von den Nazis vernichtet. Doch danach kam noch einmal von Gerd Leibholz, dem Emigranten und treuen Heimkehrer, ein nachhaltiger Beitrag zu unserer neuen und so ganz anderen Demokratie. Das Alte war verloren, aber das Neue scheint Bestand zu haben.

Alle europäischen Völker mußten in diesem Jahrhundert schweres durch-machen. Und die Deutschen haben zu diesem Schrecken am meisten beigetragen. Aber die anderen Völker konnten sich dennoch erhalten und wiederfinden; nur die Deutschen haben sich in wenigen Jahren selbst verloren. Unwiederbringlich. Die Trauer hierüber wird den nachdenklichen Deutschen nie vergehen.[^1]

[^1]: Frankfurter Allgemeine Zeitung vom 14. Juli 1999
