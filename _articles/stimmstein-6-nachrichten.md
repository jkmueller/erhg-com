---
title: "Stimmstein 6: Nachrichten"
created: 2001
category: veroeff-elem
published: 2024-04-22
---
### Stimmstein 6, 2001

#### Mitteilungsblätter  2001

### Nachrichten aus der Eugen Rosenstock-Huessy Gesellschaft

#### Neuer Vorstand
Die Mitgliederversammlung wählte im Herbst 2000 einen neuen Vorstand:
- *Fritz Herrenbrück*, 1. Vorsitzender, Löffingen, Deutschland 
- *Otto Kroesen*, 2. Vorsitzender, Delft, Nederlande
- *Wilfried Gärtner*, Körle, Deutschland
- *Arnulf Groß*, Güttingen, Schweiz
- *Gottfried Hofmann*, Bielefeld, Deutschland
- *Ruth Mautner*, Wien, Österreich (bis August 2001)
- *Lise van der Molen*, Winsum, Niederlande.

#### Internetanschluss
Die ERH Gesellschaft hat im Internet eine Home-Page: Sie enthält eine kurze Biographie Rosenstock-Huessys, Zitate aus seinen Werken, Literaturhinweise und biographische Daten. Ferner stehen auf der Home-Page die Anschriften der Vorstandsmitglieder. Die *Mitteilungsblätter* 2001 werden ins Internet  gestellt.
http://home.wanadoo.nl/kroesenbos/Rosenstock-Huessy.htm
Die amerikanische Seite findet sich unter http://www.erhfund.org
Rosenstock-Huessy breitet sich im Internet aus. „Google“, eine häufig benützte Suchmaschine, fand im August ca. tausend Erwähnungen.

#### Jahrestagung
Die  Jahrestagung der Eugen Rosenstock-Huessy Gesellschaft findet in diesem Jahr in Zusammenarbeit mit der Evangelischen Akademie zu Berlin vom 26. bis 28. Oktober 2001 in Schwanenwerder statt. Das Thema der Tagung lautet:  „Europa − ein rechtes Laboratorium? Visionen und Strategien der europäischen Rechten". Tagungsleiter seitens der Evangelischen Akademie ist Ludwig Mehlhorn

#### ARGO BOOKS Verlag
Der Verlag ARGO BOOKS bietet Bücher und Tonband-Kassetten an und versendet auf Anfrage einen umfassenden Katalog.
- ***Für die US und Kanada:***
  Mark Huessy (ARGO BOOKS), 88 Old Pump Rd.. Essex VT 05452-2742, USA.\
  Tel 802-899-5158, Fax 802-899-2986)\
  E-Mail:  mark@erhfund.org
- ***Für Deutschland:***
  Michael Gormann-Thelen, Altenbekener Damm 41, D-30173 Hannover, Tel. 0511-880371, Fax 0511-8013511\
  gormann-thelen@debitel.net\
  und Wilfried Gärtner, Melsunger Str. 24, D-343278 Körle.\
  Gaertner-Koerle@t-online. de  
- ***Für die Niederlande:***
  Lise van der Molen, ´t Olde Hof 22, NL-9951 JZ Winsum (Gr.), Nederlande\
  Tel/Fax: 0031-595-442673\
  lisevander molen@hotmail.com

#### Der ERH Fund
Der Eugen Rosenstock-Huessy Fund USA, mit Geschäftsführer (Executiv Director) Mark Huessy, ist dabei, das Gesamtwerk Eugen Rosenstock-Huessys über CD-ROMs zugänglich zu machen. Dieses Projekt könnte ein erster Schritt sein, mehrere Rosenstock-Huessy Texte ins Internet zu stellen. Clint Gardner bereitet die Veröffentlichung von *Speech and Reality* und *Life Lines* im Internet vor (ab Frühjahr 2001 unter www.erhfund.org). Der Fund hat *Out of Revolution* ins Russische übersetzen lassen. Das Buch liegt vor.

#### Chat-Gruppe
Clint Gardner hat eine Internet Chat-Gruppe eingerichtet, die in den Vereinigten Staaten einen guten Zuspruch erfährt. Sie ermöglicht Austausch, regt zu Berichten und gelegentlich auch zu Kontroversen an: rosenstock-huessy@yahoogroups.com>.

#### Neuauflagen und Übersetzungen
Im  ARGO BOOKS Verlag ist eine Neuauflage mit neuem Cover von *Multiformity of Man* (ISBN 0912148-06-3) erschienen. Eine Anregung: Es handelt sich um den ersten Teil von *Der unbezahlbare Mensch*. Zu hoffen ist, dass in die nächste Auflage auch der zweite Teil, der auf Englisch vorliegt, aufgenommen wird.

Eine Neuauflage von *Judaism Despite Christianity*, seit 1997 vergriffen, wird Fordham University Press 2002 herausbringen. Es handelt sich um die Übersetzung des Briefwechsels Eugen Rosenstock mit Franz Rosenzweig, den die beiden im  Ersten Weltkrieg führten und der zuerst unter dem Titel *Judentum und Christentum* im Anhang zu *Franz Rosenzweig: Briefe*, Berlin 1935,  S. 637ff. erschien.

Der Fund arbeitet daran, *The Christian Future* ins Chinesische über-setzen zu lassen. Probeübersetzungen von *Out of Revolution* werden ebenfalls angefertigt. An einer Übersetzung von *Origin of Speech* ins Portugiesische ist eine Gruppe in Brasilien interessiert.

In den Niederlanden wird an der Übersetzung von *Out of Revolution* und „The Origin of Speech" gearbeitet.
 #### Franz von Hammerstein 80
Dr. Franz von Hammerstein wurde am 6. Juni 2001 achtzig Jahre alt: Die Evangelische Akademie zu Berlin feiert am 23. September 2001 zu Ehren von Marianne und Franz von Hammerstein ihr Regensburger Sommerfest: Widerspruch und Brückenbau. Herzliche Glückwünsche von dieser Stelle und noch viele gute Jahre.
 
#### Briefwechsel aus dem Nachlass von Sabine Leibholz-Bonhoeffer
Frau Marianne Leibholz hat dem Archiv der Eugen Rosenstock-Huessy Gesellschaft in Bethel die Korrespondenz ihrer Mutter Sabine Leibholz-Bonhoeffer übergeben, den diese nach dem Zweiten Weltkrieg mit Rosenstock-Huessy führte. Die ERH Gesellschaft dankt herzlich für diese kostbare Gabe.

#### stimmstein 2 bis 5
Die Jahrbücher *stimmstein 2* bis *5* und die Beihefte „Hitler and Israel or On Prayer“ und „The Moral Equivalent of War“ sind noch im Buchhandel. Sie können außerdem direkt beim Verlag Talheimer (Mössingen) bestellt oder  von ARGO BOOKS Verlag und Verlagsbuchhandlung bezogen werden.
 
#### Mitgliedschaft in der Gesellschaft
Wenn Sie Interesse daran haben, Mitglied der Eugen Rosenstock-Huessy Gesellschaft zu werden, wenden Sie sich bitte an die Geschäftsstelle: Dr. Fritz Herrenbrück, Lärchenweg 2, D-79843 Löffingen.
 
#### Mitteilungsblätter 2002
Die Mitteilungsblätter 2002 (stimmstein 7) sollen dem Thema Islam − Israel  − Christentum gewidmet werden. Haben Sie Vorschläge dazu? Sind Sie bereit, ein Buch zu dem Thema zu rezensieren? Wollen Sie ein Thema des Jahres 2001 aufnehmen? Bitte schreiben Sie der Schriftleitung oder der Redaktionsgruppe. 
 
#### Bitte um und Dank für Spenden
Die Eugen Rosenstock-Huessy Gesellschaft ist ein eingetragener, gemeinnütziger Verein. Seine Projekte, über die auf den Jahresversammlungen Rechenschaft abgelegt wird, finanziert er fast aus-schließlich aus Spenden.  Wenn Sie die Rosenstock-Huessy Gesellschaft finanziell unterstützen wollen, geben Sie bitte Namen und Anschrift deutlich an, damit Ihnen eine Spendenbescheinigung zugesandt werden kann. Allen Spendern sei herzlich gedankt.


 
