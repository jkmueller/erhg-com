---
title: "Thomas Dreessen: Sach: Wat is‘n Relligion?"
category: bericht
created: 2022
published: 2022-04-14
---

![Thomas]({{ 'assets/images/Thomas-Dreessen.jpg' | relative_url }}){:.img-right.img-small}

Relligion iss wien Betriebssystem beim  
Computer - ohne geht nix;  
Kennse ja: Wennze nen PC hast, musste  
erstmal starten und einrichten  
Bei windows geht dat mit exe Dateien:  
kannse nich sehen und anfassen, aber haben  
Wirkung.  
ExeDateien Im Leben von uns Menschen  
sind de Namen  
Auf die wir hörn, die wir tragn und kriegn und  
gebn.  
Einmal gibbet Namen für die schon da sind:  
Menschen, Völker, Staaten, und und und  
Na klar auch für Sachen.  
Dann gibbet Namen für die neu sind:

Wennse auf de Welt kommst,  
dann starten se Dich mit Dein Name  
Hans oder Hassan, Monika oder Ayse  
Hörsse da das erste mal!  
Da brauchst schon n‘paar Jahre    
Mitglied werden als Hans ... und dann eignen  
Weg finden  
Unter all die annern, mit ihnen und auch   
gegen sie.  
Machsse alles unter Dein Name  
Hat aber nicht angefangen mit Dir!

immer musse auch Namen löschn  
wie ne kaputte Datei, oder nen virus.  
Son Name iss zBsp Hitler! Oder  
wennse update verschlafn hasst und  
Systemabsturz gibbt:
Nix funktioniert mehr zusammen!  
Systemabsturz inne Gesellschaft heisst  
vornehm:
Dekadenz, Anarchie, Krieg, Revolution.  
Brauchsse heut nur in Web gehen oder  
Zeitung lesen!

Relligion iss Betriebssystem von Völkern,  
Installation oder Neustart nach Systemabsturz  
Wie et verschiedene Systemabstürze gab und  
gibbt  
So gibbet auch verschiedene Relliggion‘.  
Sozusagn Neustart von Leben nach  
Absturz mit neue oder erneuerte exe-Datei.  
Stecken viele Erfahrungen drin vonn alten  
Programmierern  
und wies neu funktioniert hat mittn Leben  
auffer Erde von uns Menschen und Völkern.

Brauchsse deshalb Kenne von diese Namen  
und Geschichten  
zum Einüben und vorbereiten auf et Lebn,  
reicht abba nich.  
heute musstu wie alle Menschen auch  
Programmierer werden –  
Update un Neustart vorbereiten und in Gang  
setzen –  
in Deinen Namen mit die annern auffe Erde  
zusammen.  
Dat is neu für viele.

Ich sach: Wie Relliggion früher Betriebssystem  
von Völkern und Staaten war,  
konnze erleben in Tempel mit Haus und  
Priestern und Zeremonien.  
War Betriebsanleitung für viele  
Hasse nich mitgemacht- warsse draussen –  
aussem Leben. Ging eigentlich gar nich!  
Heut ham se dafür Börse inn Fernsehen.   
Tempel von Götze Mammon.  
Hasse kein Vermögen – kannze nich  
mitspielen -bisse draussen aussem Leben.  
Genau wie früher!  
Ich sach: Heut soll kein Tempel mehr sein aus  
Stein und nich aus Geld.  
Tempel war ja nur ne Abbildung von den  
Unsichtbaren,  
dat doch Wirkung hat und ohne dat nix geht.  
Dat haben wir nur inne Sprache und fängt  
immer mit de Namen an,  
den wir vertraun.  
Un dat war von Anfang so! Und dat war von  
Anfang so!

[Noch'n Lied](https://www.youtube.com/watch?v=ThfziLXA4dE)
