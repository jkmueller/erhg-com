---
title: Jahrestagung 2020
created: 2020
category: jahrestagung
thema: Der verborgene Kontinent des Hörens
---
## !!! Die Tagung ist ABGESAGT !!!

### {{ page.thema }}

Textgrundlage: Der methodische Teil aus „Die Hörspur” wird am Beispiel des Textes „Die Sprache des Westens” verdeutlicht.

Datum: 3.-5.April.2019

Ort: Haus am Turm, Essen

| Freitag, 3.4 | 18:00 | Eintreffen |
|              | 18:30 | Abendessen |
|              | 19:30 | Einleitung (Jürgen Müller), Lesung: Die Sprache des Westens (Eckart Wilkens) |
|              | 21:00 | Geselliges Beisammensein |
| Samstag, 4.4 |  8:30 | Frühstück |
|              |  9:30 | Rundgespräch: Hören der Eindrücke (Sven Bergmann) |
|              | 11:00 | Pause |
|              | 11:30 | Narrativ: Erzählen eigener Eindrücke (Thomas Dreessen, Jürgen Müller) |
|              | 12:15 | Pause |
|              | 12:30 | Mittagessen |
|              | 14:30 | Mitgliederversammlung |
|              | 16:30 | Pause |
|              | 17:00 | Nach dem Lesen/Hören (Thomas Dreessen) |
|              | 18:30 | Abendessen |
|              | 20:00 | Sven Bergmann: Meine Hörspur mit Rosenstock-Huessy |
|              | 21:30 | Geselliges Beisammensein |
| Sonntag, 5.4 |  8:00 | Morgenandacht: audi ut vivamus (Otto Kroesen) |
|              |  8:30 | Frühstück |
|              | 10:00 | Die acht Schritte der Hörspur und ihre Herausforderungen (Eckart Wilkens) |
|              | 11:30 | Abschlußrunde |
|              | 12:30 | Mittagessen |
|              | 13:00 | Abreise |
