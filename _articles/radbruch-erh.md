---
title: "Gustav Radbruch über Rosenstock-Huessy"
category: aussage
published: 2022-01-04
name: Radbruch
---

„Bald sammelte sich um uns eine Schar junger Menschen, statt aller führe ich nur Eugen Rosenstock und seinen Kreis an. Er wurde später Professor der Rechtsgeschichte in Breslau und lebt jetzt als Emigrant in Amerika. Damals war er von hinreißender Jugendlichkeit. Eine andere Gruppe, die uns nahestand, waren die in Heidelberg studierenden Russen, die großenteils in ihrer Heimat später eine hervorragende revolutionäre Rolle spielen sollten, meist Mitglieder der Partei der Sozialrevolutionäre.”
>*Gustav Radbruch, Biographische Schriften, bearb.v. Günter Spendel (= Gesamtausgabe; Bd.16), Heidelberg: C.F. Müller Juristischer Verlag 1988, S.222;*
