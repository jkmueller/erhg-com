---
title: Jahrestagung 2013
created: 2013
category: jahrestagung
thema: "50 Jahre ERH-Gesellschaft: Am Scheideweg"
---
![Haus Salem]({{ 'assets/images/haus_salem.jpg' | relative_url }}){:.img-right.img-large}
### {{ page.thema }}

22.- 24. März 2013 im Haus Salem, Bielefeld
Haus Salem

Ist mit der Verlagerung des Archivs im Jahre 2012 der Kern dessen erfüllt, was die Eugen Rosenstock-Huessy Gesellschaft 1963, als sie gegründet wurde, sich vorgenommen hatte? Welche Aufgabe kann jetzt an diese Stelle treten?
Muß sie, um damit wirken zu können, ihren Namen ändern?

Für diese Frage ziehen wir Eugen Rosenstock-Huessy's Schrift Die Interims des Rechts, November 1964 zurate.

Ist mit der Verlagerung des Archivs im Jahre 2012 der Kern dessen erfüllt, was die Eugen Rosenstock-Huessy Gesellschaft 1963, als sie gegründet wurde, sich vorgenommen hatte?
Welche Aufgabe kann jetzt an diese Stelle treten?
Muß sie, um damit wirken zu können, ihren Namen ändern?

Für diese Frage ziehen wir Eugen Rosenstock-Huessys Schrift Die Interims des Rechts, November 1964 zurate


| Freitag, 22. März 2013, | 16-18 Uhr | Ankunft |
|                         |    18 Uhr | Abendessen |
|                         | 19:30 Uhr | Begrüßung und gemeinsame Lektüre der Interims des Rechts 1964 |
|                         |           | (ohne Diskussion an diesem Abend) |
|                         | 20:30 Uhr | Andreas Möckels Zeugnis von dem Wirken Eugen Rosenstock-Huessys in seinem Leben |
| Samstag, 23 März 2013,  |  8:30 Uhr | Frühstück |
|                         |  9.30 Uhr | PRÄJEKTIV: Vergegenwärtigung der drei Stimmen in den Interims des Rechts: das alte Recht, der Protest, die Lossagung, Gesprächsführung:  Andreas Schreck |
|                         | 11:00 Uhr | Pause |
|                         | 11:15 Uhr | SUBJEKTIV: Was bedeutet das für uns? Gesprächsführung: Thomas Dreessen |
|                         | 12:15 Uhr | Mittagessen |
|                         | 14:30 Uhr | Otto Kroesens Zeugnis von dem Wirken Eugen Rosenstock-Huessys in seinem Leben |
|                         | 16:00 Uhr | Pause |
|                         | 16:30 Uhr | TRAJEKTIV: Das Mit- und Auseinander der deutschen, niederländischen und amerikanischen Gruppen, die sich zu dem Erbe Eugen Rosenstock-Huessys bekennen, Gesprächsführung:  Wilmy Verhage |
|                         | 18:00 Uhr | Abendessen |
|                         | 19:30 Uhr | Mitgliederversammlung der Eugen Rosenstock-Huessy Gesellschaft e. V 2013 am neuen Standort des Eugen Rosenstock-Huessy Archivs, Adresse: Bethelplatz 2, Bielefeld mit dem ersten Teil: Begrüßung, Verlesung des Protokolls von 2012, Bericht des Vorsitzenden Eckart Wilkens danach Auszug nach Haus Salem, Bodelschwinghstr. 181, Bielefeld-Bethel und Fortsetzung der Mitgliederversammlung mit Zeit für die Stimmen der Mitglieder |
| Sonntag, 24 März 2013   |  7:30 Uhr | Andacht in der Kapelle (Otto Kroesen) |
|                         |  8:30 Uhr | Frühstück |
|                         |  9:30 Uhr | Simon Wilkens' Zeugnis von dem Wirken Eugen Rosenstock-Huessys in seinem Leben |
|                         | 11:00 Uhr | Pause |
|                         | 11:15 Uhr | OBJEKTIV: Aussprache, wie es, nach der Wahl des neuen Vorstands, mit der Eugen Rosenstock-Huessy Gesellschaft weitergehen soll, Gesprächsführung:  der oder die neue Vorsitzende |
|                         | 12:15 Uhr | Mittagessen |
|                         | 13:15 Uhr | Abschied |

Eingeladen sind alle Mitglieder und Freunde der Gesellschaft sowie an Eugen Rosenstock-Huessy und diesem Thema interessierte Personen. Bei der Anmeldung bekommen Sie den Text geschickt.

Übernachtung und Verpflegung von Freitag, 22. März, bis Sonntag, 24. März 2013
Einzelzimmer: 110€, Doppelzimmer p. P: €100, Tagesgast: €50.

Anmelden können Sie sich bei Wilmy Verhage:

Wilmy Verhage, Olympiaplein 32 A 1, 1076 AC Amsterdam, Nederland
