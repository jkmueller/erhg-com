---
title: "Sven Bergmann: Brief an Ernst und Nelly Michel"
category: einblick
published: 2021-12-01
---

Ernst Michel und Eugen Rosenstock lernten sich 1920 an der Frankfurtere Akademie für Arbeit kennen. Eugen Rosenstock als deren erster Leiter und der Linkskatholik Ernst Michel als Referent. Richtig schätzen gelernt haben sich die beiden allerdings erst nach dem Abschied des Gründers aus Frankfurt. Es wurde eine Lebensfreundschaft. Ägypten faszinierte schon den Berliner Schüler, der Vorlesungen bei dem berühmtesten deutschen Ägyptologen der Zeit Adolf Erman hörte. Nach dem zweiten Weltkrieg besuchte er mehrfach die Altertümer der pharaonischen Kultur. Ägypten hat den Himmel auf die  Erde geholt, deshalb sind die Pyramiden auch eckig und nicht rund. Ägypten symbolisiert die Brücke von den Stämmen zum Reich, bildet eine wesentliche Voraussetzung für die jüdische Geschichte und damit eine Etappe auf unserem Weg von heute in die Zukunft. Im Zusammenhang des Briefes ist zu bemerken, das Ernst Michels Buch über die Ehe von 1948 von der Kirche auf den Index gesetzt worden ist.

Eugen und Margrit Rosenstock-Huessy an Ernst und Nelly Michel, Luxor, Oberägypten, 18. Feb. 1950\
*Lieber Ernst und so dürfen wir wohl heut schreiben, liebe Nelly,\
in zehnstündiger Fahrt sind wir von Kairo das unbeschreiblich schmale Band Oberägyptens hinaufgefahren und residieren zu unserem eigenen Staunen in dem durch 25 Jahre liebevoller Jahre der Hingabe angepflanzten Chicago Hotels in Luxor, d.h. mitten unter den Tempelsäulen Thebens am Nil. Tropisch. Mendrisio sieht von Vermont so unerreichbar südlich aus; wie überhaupt der Name Tessin seit meiner Kinderzeit diesen Mignonfarbenen Ton an sich trägt. Aber nun haben wir Euch überboten, aber nur räumlich. Die Seelen hingegen leben in der Zeit. Und da gibt es kein überbieten; sondern nur die Herrlichkeit des zur rechten Zeit lebens für alle Beteiligten oder genauer, alle Zuerteilten.\
Als solche Zuerteiler segnet Euch der Himmel, die ihre Zeiten in uns hineinwirkende, gefährliche, großartige, liebliche, herzgebietende Macht, von der in uns eben das Herz abgesenkt ist, denn es gehört schwerlich bloss dem eigenen Leibe zu.\
Und so ruft in uns diese Macht den Gruß hervor, der sich Euren Lebens froh zu werden helfen möchte. Nichts ist geheimnisvoller, als „weiter“ leben zu dürfen, weiter, nachdem doch das 60. Jahr Schranken errichtet. Im Alter ist ja alles greifbarer, körperlicher. Der Geist braucht nicht so weit zu fliegen wie in der Jugend. Denn jeder Stengel, jeder Kelch verwirklicht das ganze Geheimnis.\
Möge Euch der räumliche Himmel über den zeitlichen lächeln und so, weil Zeit und Raum übereinstimmen sollen, Euren Bund bestätigen. - Eugen und Margrit*
Quelle: UB Frankfurt, NL Ernst Michel, Korr, Prof. Dr. Rudolf Ehrenberg

aus: [Mitgliederbrief 2021-07]({{ '/erhg-2021-07-mitgliederbrief' | relative_url }})
