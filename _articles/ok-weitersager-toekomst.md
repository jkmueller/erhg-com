---
title: "Otto Kroesen: Toekomst – het Christelijk levensgeheim"
category: weitersager
order: 63
---

Helemaal in het begin van het boek, in de paragraaf over "functioneren afspreken" voert Rosenstock-Huessy een student op, die met zijn vriendin uit eten gaat, die er netjes uitziet en goed verzorgd is, maar die, terwijl zij meestal het woord voert, haar alleen maar telkens weet te onderbreken met de woorden "Damn it…". Rosenstock-Huessy is getroffen door dit gebrek aan spreken. Het taal-repertoire van deze jongen is zo beperkt, dat hij eigenlijk helemaal niets weet te zeggen. Hij heeft niets te zeggen. Een dergelijke taalarme toestand leidt ook tot een nietszeggend bestaan. Onbetekenend. Dat is het wel waar mensen in de goed of slecht functionerende megamachine van de moderne samenleving het meest onder te lijden hebben. We zouden het boek "Des Christen Zukunft" (de Duitse titel), zoals eigenlijk al het werk van Rosenstock-Huessy, kunnen lezen als lessen in spreken. Door de taal zijn wij verbonden met de machten, die mensen in het verleden aangedreven hebben en door zelf te spreken herscheppen en vernieuwen wij die stroom doorheen de tijd. Het merkwaardige is, dat als wij mensen niet verbonden zijn met deze stroom van taal doorheen de tijden, en teruggeworpen worden op onszelf en binnen onze beperkte horizon, dat wij dan ook zelf helemaal niets te zeggen hebben. Terwijl ook het omgekeerde het geval is: naarmate ook wij bewogen worden door wat talloze andere mensen bewogen heeft, vinden wij ook onze eigen persoonlijke en unieke bijdrage aan dit voortgaand gesprek van het menselijk geslacht.

## Eerste deel: het grote interim

### I. Wat mij diskwalificeert

#### Functioneren of spreken

Er is een verlies aan taal, dat plaatsvindt, waar mensen alleen maar functioneren. Mensen hebben elkaar dan niets meer te zeggen. En het lukt niet meer om een gemeenschappelijke taal te vinden voor de warboel, waarin wij leven.

#### Woorden of namen?

Rosenstock-Huessy zet zich af tegen het streven naar een nieuwe taal, alsof je die zelf maakt! Woorden worden door mensen gemaakt, d.w.z. voor een deel hebben mensen de taal in hun macht en kunnen die naar hun hand zetten. Met namen lukt dat niet. Namen zijn die woorden, die niet zonder respect uitgesproken kunnen worden, die ons inspireren en richting geven. Daarover gaat dit boek, hoe opnieuw namen kunnen klinken in een verwarrende situatie. Die verwarrende situatie bestaat uit de mechanisering en instrumentalisering van het bestaan in een overgrote globaliserende samenleving.

### II. Interim-Amerika, 1890 tot 1940

#### De voorstad

De voorstad staat voor de binnenwereld, de privé-wereld, de wereld van de vrijetijdsbesteding. In hun woonomgeving en in hun privé-leven hebben mensen, buren, een vrijblijvende houding tegenover elkaar. Een oppervlakkige harmonie. Mensen gaan nooit helemaal ergens voor. Dat gebrek aan bewogenheid leidt tot neuroses en innerlijke eenzaamheid. Wanneer men met liefde voor één ding gaat, wordt men lid van een gemeenschap, waarin men door anderen wordt aangevuld.

#### De fabriek

Het leven in voorstad en woonwijk staat in het teken van de binnenwereld. Het leven in de fabriek staat in het teken van de buitenwereld, de arbeid en de strijd om het bestaan. De industriële arbeid kent een ononderbroken ritme. Zo is de strijd met de natuur nu eenmaal georganiseerd. Die behoeft ononderbroken aandacht. De snelle wisseling van het arbeidsritme, de snelle veranderingen op het werk maken van het leven een soort ononderbroken onderbreking. De industriële maatschappij zal de middelen moeten vinden om de kwalijke kanten daarvan te overwinnen.

#### De ziel op de snelweg

De scheiding tussen vrije tijd en arbeidstijd maakt, dat in de ene wereld, die van de vrije tijd, de mens onder allerlei namen leeft, en in de andere helft zonder naam blijft. Want in de sfeer van de industrie blijft de mens arbeidskracht. Overal voor inzetbaar. Wie is nu deze mens zelf uiteindelijk nog een keer.? De echte mens moet groter blijven dan de vormen die hij aanneemt in zowel werk als vrije tijd. De ziel op de snelweg" is een uitdrukking die symbool staat voor deze vormeloosheid van de mens, die in de file niet meer hoeft te luisteren naar de baas en nog niet overvallen wordt door de claims van thuis. Daar is de mens nog aan zichzelf toekomstig. Vanuit deze toekomst, waar een mens zijn ziel heeft, moet de vrijblijvende wereld van de voorstad en de geobjectiveerde wereld van de arbeid op de één of andere manier weer bij elkaar gebracht worden, moet de gespletenheid van deze twee gestalten opgeheven worden.

#### Het nieuwe karakter van de zonde

Het enkele individu is te midden van overgrote instituties eigenlijk niet meer een factor van betekenis. Zijn kleine morele zonden doen er niet veel toe. De grote instituties, arbeidsorganisaties, belangenorganisaties en dergelijke, zondigen in plaats van het kleine individu. Ze zetten voort waar ze mee bezig waren. De toekomst kan op die manier niet geschapen worden.

#### Het loslaten van de christelijke tijdrekening

Het leven in voorstad en fabriek heeft de Christelijke tijdrekening, een gemeenschappelijk je richten op de toekomst, buiten werking gesteld. Mensen leven enkel nog in de ruimte en niet in de tijd.

#### Het kind is geen vader van de man

In de tijd na de reformatie werden zowel op katholiek als op protestants erf de binnenruimte van de vrije tijd en de buitenruimte van de productie samengebonden door het christelijk huisgezin. De vader als priester in het gezin maakte van het gezin, met gezellen en bedienden er omheen, zowel een economische als een morele eenheid. Vader en moeder verhielden zich in het gezin als kerk en staat. Die tijd ligt achter ons. Veeleer zijn met de scheiding van produceren en wonen wetenschap en kunst voorbeeldig geworden, zodat man en vrouw zich verhouden als wetenschap en kunst. Daarmee is er echter een onopgelost probleem. De ruimtelijke opsplitsing in binnen en buiten wordt niet meer gedragen en samengebonden door een gang door de tijd.

#### Onze invasie door China

De scheiding tussen voorstad en productiesfeer wordt weerspiegeld door de tegenstelling tussen pacifisme en oorlog. Veel mensen denken pacifistisch (een vredige binnenwereld), maar worden door de harde realiteit betrokken in de oorlog. China is het grote voorbeeld van een land, waarin de burgers in een binnenwereld harmonie probeerden te stichten en de oorlog verafschuwden. Die benaderingswijze haalt zonder zich dat bewust te zijn het pragmatisme in Amerika binnen.

#### John Dewey

John Dewey is voor het interim Amerika, wat Confucius geweest is voor China, nadat de verschillende stammen in één rijk samengebracht waren. Pragmatisch moesten de verschillen en tegenstellingen uitgewist worden in een soepel harmonisch functionerende maatschappij. Kwaliteiten, waar in het Amerika van John Dewey door vorige generaties offers voor waren gebracht om überhaupt zover te komen, ziet hij vanaf nu aan voor natuurlijke kwaliteiten van rationele mensen. Hij ziet de noodzaak van wetenschap, democratie, samenwerking, intelligent en pragmatisch en rationeel functioneren om de maatschappij in stand houden. Hij ziet niet, dat mensen in oorlogstijd en door moedig optreden een nieuwe toekomst binnenhalen en dat mensen in vredestijd deze nieuw binnengehaalde toekomst veilig stellen tegen over oudere vormen. Dat maakt hem hulpeloos tegen de dan toch weer plotseling optredende strijd.

#### Charles Darwin

Naast en samen met het ideaal van een mens, die subjectief een rationele afstemming nastreeft, treedt het ideaal van een intellectuele blik, die overal objecten ziet. John Dewey maakt de mensen liefdeloos, omdat ze nergens volledig zichzelf aan geven en onpersoonlijk functioneren. Charles Darwin geeft de eenheid van het universum op, omdat hij overal het gevecht van de sterkste ziet. Beide manieren van denken lopen parallel met enerzijds het bestaan in de onpersoonlijke sfeer van de voorstad, waar alles afstandelijk en harmonisch lijkt en geen ziekte of dood aanwezig is, en met anderzijds het bestaan in de harde werkelijkheid van de productie, waar alleen meetelt wat krachtig is. Dit gereduceerde ruimtelijke bestaan plaatst de mens voortdurend voor verrassingen, omdat hij niets begrijpt van de tijd. Pas wanneer men bezorgd is over crises en breuken komt men de oorspronkelijke vraag van het christendom tegen.


## Tweede deel: "De tijd is ontwricht"

### III. Toekomst scheppen

Soms vraagt men of het christendom wel toekomst heeft. Merkwaardig, omdat de uitvinding van het christendom zo ongeveer gelijk staat met de uitvinding van de toekomst. Het dode achter je te kunnen laten en een nieuwe toekomst te beginnen, dat is de kern ervan.

#### Het heidendom overwonnen

Heidens levensbesef betekent eigenlijk cyclisch levensbesef, geloof in eeuwige herhaling en in een neerwaartse spiraal. Heidendom treedt de wereld met verstand en dapperheid tegemoet, overwint weerstanden ervan, houdt zich aan ervaringsfeiten, maar de toekomst ontbreekt er aan. Het kan niet breken met zichzelf. Heidendom is zelfverabsolutering. Mythen dienen er steeds toe een bepaalde levensopvatting en -interpretatie te vereeuwigen. Wat de bijbel doet, reeds het Oude Testament, is de wandaden laten zien, waar de geschiedenis vol van is. Achter alle verhalen, waarmee mensen zich rechtvaardigen, doemt dan de ene God op. Christelijk geloof betekent de dood overwinnen door haar een plek in het leven te geven. Midden in het leven aan zichzelf sterven en een nieuw begin maken.

#### Het vooruitlopen op de dood

Jezus was de eerste die de richting omdraaide naar de toekomst toe. Om die reden is hij het middelpunt van de geschiedenis. Wie zijn leven behouden wil, die zal het verliezen en wie zijn leven om Christus wil verliest, maakt een nieuw begin. Laten sterven waaraan men gehecht is en onbekend gebied betreden dat is de ware gang door de geschiedenis. De eschatologie, de leer van de laatste dingen, is in de tijd van de verlichting als irrationeel van de hand gewezen. Seculiere ideologieën als die van Marx moesten daarom de toekomst levend houden. De leer van de laatste dingen vraagt volledige overgave aan iets buiten de bestaande orde.

#### De zin van de geschiedenis

Door vanuit het einde te leven kan men de betrekkelijkheid van elk tijdperk inzien en daarmee elk tijdperk zinvol betrekken op het einde. Men kan daardoor volledig betrokken in de eigen tijd leven, om door daarin te treffen beslissingen een stuk toekomst in het heden binnen te halen. Zo ontstaat er één geschiedenis door de vele geschiedenissen heen. Dat is de zin van de christelijke tijdrekening. Wie de christelijke tijdrekening achter zich meent te kunnen laten vervalt in eeuwige herhaling.

#### Vooruitgang: christelijk of modern?

Condorcet schreef in de achttiende eeuw een boek over de "vooruitgangen" van de mens. Hij gebruikte het meervoud, wellicht onbewust, omdat hij in de vele technische stappen vooruit nog steeds een toepassing zag van de ene echte geestelijke vooruitgang en beweging de toekomst in. Sinds 1850, de Wereldtentoonstelling in het Cristal Palace in Londen is als het ware het uitvinden uitgevonden. Maar al deze uitvindingen zijn slechts een toepassing op het wereldse en maatschappelijke bestaan van de geestelijke beweging de toekomst in. Die is ook niet te vergelijken met het geliefde beeld van een spiraal, waarin op- en neergang verenigd worden, maar toch vooruitgang geboekt zou worden. Wie zo naar de geschiedenis kijkt, neemt er niet echt aan deel, maar neemt een toeschouwershouding aan. Zo iemand staat niet zelf in het kruis van de werkelijkheid, waarin hij of zij moet beslissen te breken met een erfenis uit het verleden en het te wagen met een nieuwe stap de toekomst in, met al de moeite en het lijden, die daarbij horen.

#### Natuurwetenschap en de christelijke tijdrekening

Natuurwetenschappers realiseren zich vaak niet de maatschappelijke oorsprong van hun bedrijf. De maatschappij heeft hen krediet gegeven doordat de filosofie de publieke opinie geleerd had het belang van de wetenschappen te erkennen. Met moeite is deze erkenning bevochten, voordat de natuurwetenschappen zich werkelijk konden vestigen. Als de wetenschappen deze christelijke, toekomstgerichte basis prijsgeven, als er geen verlangen is naar meer, als niet de onopgeloste problemen van de geschiedenis ons helpen de wetenschap verder te brengen en zelfs de eindeloze stof van de geschiedenis zelf te ordenen, dan vallen de wetenschappen terug in herhaling. Herhaling, de cirkelgang, waarbij niet lijden en dood doorstaan worden omwille van het geloof in ongekende mogelijkheden, is kenmerk van heidendom.

#### Het wegvallen van het geloof

Is het christendom bankroet? Van tijd tot tijd is het bankroet. Het weet echter steeds opnieuw te beginnen, daar waar mensen met pijn in het hart oude vormen overleven. Er is een nieuwe gestalte van toekomstgericht leven nodig. Daarover gaat dit boek.


### IV. Het geloof in de levende God

Nadat Rosenstock in het vorige hoofdstukje gesteld heeft, dat het geloof in de toekomst weggevallen is, legt hij in dit hoofdstukje uit waarin wat hem betreft het geloof bestaat. Nadat het in een oude vorm zijn werking verloren heeft, moet het in nieuwe vorm op ons afkomen, om opnieuw met mensen iets te kunnen doen. Wat ziet Rosenstock als kern van het geloof, en wat is het dus, dat wat hem betreft ons toekomst wijst?

#### Nietzches drie goden

De eigenlijke religieuze vraag valt samen met de existentiële vraag, het is de vraag hoe de dood te overwinnen. "Iets" moet je de moed geven te leven, om niet te verzinken in het niets. Door te verwijzen naar de "drie goden" van Nietzsche wil Rosenstock laten zien, dat deze, hoewel hij Gods bestaan ontkent, toch gehoorzaamt aan deze levenswet. Deze levenswet verloopt namelijk in drie stappen. Eerste beërft een mens wat er al is. In het geval van Nietzsche is dat de God van de bestaande moraal, waar hij zich als volwassene tegen verzet. Hij kon zich alleen maar vrijmaken voor een nieuwe toekomst door zich tegen deze God te keren. In zijn laatste levensfase ontdekt hij, dat goed te leven zelf een goddelijke last is, die hij alleen niet kan dragen. Deze drie stappen kan men ook zo aanduiden: beërven, vrijmaken, waarmaken.

#### De volwassenen en de geloofsbelijdenis

De geloofsbelijdenis is niet een objectieve waarheid over een stand van zaken, maar een oude formulering van de levenswet, waaraan Nietzsche gehoorzaamt. Het geloof in God als schepper staat gelijk met het geloof in de waarde van de lessen uit het verleden. Die zijn goed, ook al kun je er niet bij stil blijven staan. Midden in het leven vindt een mens zijn bestemming, die hem vrij maakt van herhaling van het verleden. Dat is het tweede geloofsartikel, het geloof in de vrijheid van de zoon. De doorwerking van deze nieuwe vrijheid in de mensengemeenschap is het derde geloofsartikel, over de werking van de heilige Geest. De objectieve en "dogmatische" kant van de christelijke leer is een legendevorm van deze levenswaarheid. Vanuit deze ingrijpende levenservaringen interpreteren wij ook de kosmos, niet andersom.

#### De godheid van Christus

In het stukje over de drie goden van Nietzsche is de binnenpool, de belevingspool van de werkelijkheid behandeld. In het stukje over de volwassenen en de geloofsbelijdenis is de objectpool, de objectieve formulering van het dogma behandeld. Met het opschrift "de godheid van Christus" kijken wij naar het verleden. Ooit is er een nieuwe kwaliteit van leven verworven. Voor het eerst heeft een mens vanuit het einde van alle tijden in het heden terug geleefd. Dat is het bijzondere van Jezus' leven en dood. Dat maakt hem normatief en zo heeft hij gewerkt op wie hem volgden. Dat Jezus de zoon Gods was, betekent dat hij het definitieve gebeuren in de goddelijkheid van de mens was.

#### Laat ons de mens maken

In dit stuk blikken wij in de toekomst: een nieuwe vorm van existentie wekt navolging. Dat is niet een kwestie van maakbaarheid, niet iets wat je moedwillig doet, en daarom noemt Rosenstock waarschijnlijk Abraham Lincoln als voorbeeld, die van zichzelf nooit geweten heeft, dat hij in het spoor van Franciscus van Assissi ging door in burger en te voet met een overwinnend leger van de strijd terug te keren. Zo werkt ook de nieuwe ervaring, die met Jezus begonnen is, een nieuwe menstype tot leven.

### V. De heilseconomie

Er is een periodisering van geschiedenis mogelijk, waarin steeds de christelijke traditie in een bepaalde vorm vastgelopen is, maar ook steeds in een nieuwe vorm weer tot leven komt. Zo kan het ons ook gaan.

#### De drie tijdperken

Hier blikken wij opnieuw terug de geschiedenis in. De laatste 2000 jaar geschiedenis is te beschouwen als een ontwikkeling naar eenheid. Eerst eén God, dan één wereld, en nu staan wij voor de taak één mensheid in alle verschillen te creëren. De eerste duizend jaar gaat om de strijd om één God, Christus tegen de stamheroën. De tweede duizend jaar om de eenheid van de schepping, de ontdekking van de wereld. Voor de derde duizend jaar staat de mens en de inspiratie door de heilige Geest centraal. Zo worden de drie artikelen van de christelijke geloofsbelijdenis in de geschiedenis gerealiseerd.

#### Geïnspireerde navolging: de wedergeboorte van de betekenis

Filosofie maakte van oudsher wel school, maar kent niet echt een traditie van navolging. De religie daarentegen van oudsher was erfelijk van geslacht op geslacht. Het christendom staat daartussenin door bekering, een voortzetting door de onderbreking heen. Zo is dat door de geschiedenis heen ook gegaan. Alle levensvormen kunnen alleen levend blijven als ze met behulp van nieuwe vormen bevrijd worden uit de stagnatie door herhaling. Dat geldt dus ook voor ons.

#### Vleselijkheid contra vleeswording

Waar moet nu de nieuwe stap de toekomst in gezet worden? De huidige verleiding is de vitaliteit en sensatiezucht, om los te komen van een overmaat aan disciplinering, die het vorige tijdperk ons gebracht heeft. Onze generatie wil ten volle leven. Telkens zijn er ook in de geschiedenis stromingen geweest, die de gemakkelijke weg namen, d.w.z. tegemoet kwamen aan de behoefte van hun tijd, zonder daarin het kruis te plaatsen. Bewegingen zoals gnosis en humanisme en onze tijd dus het vitalisme. Nu is het taak van de christenen om niet uit de wereld weg te trekken of aan de wereld te sterven, maar in het leven bezieling te brengen, leven met bestemming. Dat is vleeswording.

#### Het christendom incognito

Het onderscheid uit vorige periodes tussen christen en niet christen vervaagt. Ieder heeft wel een stuk heidendom in zichzelf en ook een stuk christendom, zonder het te willen of te weten. De formule ik hoop te geloven geeft aan hoe wij in het leven iets van het geloof proberen te realiseren, zonder ons zelf als gelovig te onderscheiden van anderen. In die formule wijst het woord hoop ook op de realisatie van de verwachting die de christelijke traditie bracht.

#### Dood en opstanding van het woord

Een oude, zeg maar kerkelijk gestempelde, taal werkt niet meer. Werkt niet meer in de zin van: maakt geen indruk meer. Ook het woord neemt deel aan de gang van dood en opstanding. Elke generatie moet anders kunnen handelen juist om hetzelfde als de vorige te kunnen doen. Jezus heeft zelf in zijn optreden aan die regel gehoorzaamd, door in zijn verschillende levensperiodes geheel anders op te treden naar zijn volgelingen. En ook tussen Paulus en Christus is een dergelijk verschil te maken. Wij hebben dus goede papieren, als wij ons ook in die traditie van vertalen stellen.


## Derde deel: het lichaam van onze tijdrekening: achterwaarts; voorwaarts; nu

Nu wordt duidelijk, dat met name het eerste deel van het boek ons in het nu plaatste, dat het tweede deel vooral een terugblik is geweest op onze voorgeschiedenis, en dat het derde deel aan de orde stelt wat ons in de toekomst te doen staat. Rosenstock brengt zelf in praktijk, wat hij als richtlijn voor het onderwijs in de geschiedenis geeft. De onopgeloste problemen van de toekomst zijn de aanleiding om orde te brengen in de ervaringen van het verleden, omdat je met het oog op de problemen, die je voelt daarin iets zoekt, dat richting heeft. Zo gaat hij ook in dit hoofdstuk te werk. Hij kijkt terug op het tekortschieten van de kerk in het verleden. Zijn stelling is, dat dat tekortschieten toch vrucht gedragen heeft. Net als in een niet al te goed huwelijk de partners toch iets voor elkaar betekenen. In hoofdstuk 6 kijkt hij dus naar de zonden en tekorten van de kerk, maar zo, dat hij tegelijkertijd laat zien hoe het kruis daar in gewerkt heeft. In hoofdstuk 7 kijkt hij naar die tradities, die tot op heden niet door het kruis beroerd zijn, met name Azië. (Hij wist niet en kon niet weten, dat daaraan vooraf nog de confrontatie met de Islam zou gaan). In hoofdstuk 8 belanden we dan weer bij de taken van het heden: hoe moeten wij nu verder? En nu is: 1947, de tijd, dat de soldaten juist uit de oorlog teruggekeerd zijn en hun plek weer moeten vinden in een samenleving, die het liefste doet alsof die oorlog er niet geweest is.


### VI. Zegenrijke schuld of terugblik op de kerk

#### Mechanisme of zwakheid?

De kerk heeft veel fouten gemaakt maar is ten minste een levende werkelijkheid geweest, niet vergelijkbaar met een machine, maar eerder met een gezin, waarin niets volgens grondwettelijke regeling gaat, en toch de partners alles met elkaar moeten oplossen wat zich aandient. Kenmerkend voor de twintigste eeuw, is dat voor het eerst weer de oosterse orthodoxie opgemerkt wordt.

#### Het vierzijdig gebeuren: Chalcedon, Frankfurt, Florence, Stockholm

Aan deze vier namen zijn vier gebeurtenissen verbonden, die ons op de toekomst oriënteren. Om die reden krijgen ze betekenis voor Rosenstock. Deze gebeurtenissen belichten elkaar, en laten iets zien van de doorgaande dialoog in de ontwikkeling van het menselijk geslacht, waarin het antwoord op een probleem van honderden jaren geleden, soms honderden jaren later komt. In deze vier gebeurtenissen is steeds de kerk tekort geschoten, maar toch is dat tekortschieten van betekenis, omdat de kerk zich richt op iets dat groter is, dan zijzelf kon realiseren. Altijd blijft de realisatie ten achter de bij het verlangen waar het door gedreven wordt. Maar er wordt zelfs geen minimum gerealiseerd van dat verlangen, als dat verlangen niet overgroot is. Dat is de wijze waarop de zonde en het gebrek toch vrucht draagt. Van groot belang is daarin het kerkelijk schisma, waarvan de betekenis vaak onderschat wordt.

#### Het eerste beeld: gebrek aan geloof

De oosters orthodoxe kerk heeft te veel tegen vadertje staat aan geleund. Zij heeft op het concilie van Chalcedon in 451 bijvoorbeeld expliciet uitgesproken, dat Rome alleen maar het primaat had, omdat daar de keizer gevestigd was. Daarmee impliceerde zij: nu de keizer in Constantinopel zetelt, verliest Rome het primaat. Deze voor-christelijke werkelijkheid van het keizerrijk heeft de oosterse christenheid niet kunnen offeren. Dat is gebrek aan geloof, voorzover geloof juist betekent te breken met het verleden.

#### Het tweede beeld: gebrek aan liefde

De hoftheologen van het keizerrijk van Karel de Grote traden op hun beurt in concurrentie met het keizerrijk van het oosten. In een poging zichzelf als het rechtmatige keizerrijk voor te stellen is aan het liturgische gebruik van de apostolische geloofsbelijdenis het zogenaamde filioque toegevoegd, wat inhoudt dat de Geest niet alleen van de Vader maar ook van de Zoon uitgaat. Ook de bijbel gebruikt wel zulke formuleringen, maar het credo, de apostolische geloofsbelijdenis, kende die formulering niet, en door die formulering opzettelijk in de liturgie te brengen werd een breuk met de oosterse orthodoxie geforceerd. Dat is een gebrek aan consideratie voor de ander, gebrek aan liefde.

#### Het derde beeld: de renaissance van de menselijke geest

In het jaar 1439 is een poging gedaan tot hereniging. De moslims veroverden Constantinopel en patriarch en paus probeerden de crisis van die tijd met een versterkte eenheid te boven te komen. Niet dat dat lukte, maar zo is wel de filosofie van Plato naar West-Europa gekomen en daardoor weer het natuurwetenschappelijk onderzoek op gang gekomen. De eenheid, die in de kerk niet bereikt werd, werd in afgeleide zin in de wetenschap bereikt.

#### Het vierde beeld: het weer toelaten van de economie

In Stockholm is in 1925 de oecumenische beweging begonnen, nog zonder Rome, maar in ieder geval met orthodoxie en protestantisme bij elkaar. Nu werd de eenheid van kerken nagestreefd, waarbij aangetekend moet worden, dat in de constitutie van de wereldraad tot op heden staat, dat het een samenwerkingsverband tussen kerken betreft, die elkaar niet per se als kerk erkennen! Opnieuw is dus eenheid bijvoorbaat mislukt, maar Rosenstock spreekt het vermoeden uit, dat van deze oecumenische beweging wel een werking uitgaat, namelijk deze, dat de nietsontziende economisering van het bestaan geremd wordt. M.a.w., een kerk, die leert omgaan met verschillen kon wel eens voorbeeldig worden voor wat in de economie staat te gebeuren.

#### Kerkgeschiedenis

Werkelijk leven doorbreekt de causaliteit. Iemand die de kerkgeschiedenis bestudeert, zoekt vaak naar causale samenhang, maar vergeet dan de levensinzet van talloze mensen. Mensen hebben een prijs betaalt voor wat zij als doel probeerden te bereiken en natuurlijk zijn hun doelen mislukt, maar in plaats daarvan hebben zij hun bestemming gevonden. Wanneer wij een bloesem zien, doen wij er goed aan, die niet te herleiden tot de eerste oorzaak in de wortels, maar die waar te nemen met het oog op zijn toekomst, als vrucht in wording.

Als Rosenstock de oosterse orthodoxie een gebrek aan geloof verwijt, is vooral bedoeld, dat zij niet voldoende toekomstgericht geleefd heeft (de oosterse orthodoxie laat zich juist voorstaan op de zuiverheid van haar geloof). Het verwijt aan de westerse kerk van een tekort aan liefde betekent, dat de westerse kerk niet voldoende in het heden geleefd heeft (wat zij juist wilde doen door haar bedoeling hier op aarde het leven te verbeteren). De derde christelijke deugd heeft Rosenstock al eerder genoemd: de hoop. Hoop heeft te doen met de krachten van het verleden. Dat klinkt ongewoon om te zeggen, maar als mensen hoop koesteren, dan hopen zij altijd op voortleven in de toekomst van iets dat hen bekend is uit het verleden. Rosenstock heeft er al eerder op gewezen, in de uitdrukking ik hoop te geloven, dat wij leven in de tijd van de hoop. Dat is de tijd, waarin beloften gerealiseerd worden in het werkelijke leven. Om dat goed te kunnen doen, daartoe zijn de tradities uit Azië, die altijd in het heden geleefd hebben, nodig. Dat is de confrontatie van de toekomst, waarin de werkelijkheid van het kruis pas echt tot het kruis van de werkelijkheid wordt.

Intussen zij opgemerkt, dat de natuurwetenschappen natuurlijk aan de binnenpool staan en dat de economie vanzelfsprekend aan de buitenpool staat, arbeid en overleven. Twee kerkelijke pogingen tot eenheid hebben meegewerkt van deze wereldlijke vormen van eenheid, die wel gerealiseerd zijn. Dat is wat Rosenstock bedoelt met de formule, die hij in dit hoofdstuk gebruikt: liever een beetje ongelukkig getrouwd, dan helemaal geen man. Het minimale, dat je bereikt, bereik je dankzij een inzet voor het maximale, dat je ontglipt. We zien zo hoe de gelovige gang door de tijd gestalte gegeven heeft aan de organisatie van de ruimte in een groter verband. Later zullen wij er nog op terugkomen, dat ook heden ten dage het bestaan in de ruimte ingebed dient te zijn in onze gang door de tijd, ook al omdat anders deze ruimtelijke organisatie van het maatschappelijk leven niet eens stand houdt. Een ruimtelijke organisatie, die in herhaling valt en zich niet vernieuwt, raakt in verval.


### VII. Het binnendringen van het kruis

#### Het kruis in werking

Wij leven in twee ruimten en in twee tijden: binnen en buiten, verleden en toekomst. Op alle vier de fronten moeten wij ons kunnen handhaven, anders kunnen wij niet voortleven. De maatschappij past daarop echter ook een soort arbeidsdeling toe, waarbij wij elkaar als het ware naar dit of dat front toesturen om daar taken voor elkaar waar te nemen. Telkens staan mensen echter ook in de verleiding het front, waarop zij staan te verabsoluteren. Zo probeert bijvoorbeeld de wetenschap alles als object te behandelen. Wij kunnen echter nooit compleet tot één tijd of tot één groep behoren en staan bloot aan voortdurende veranderingen: mutabor, ergo cogito.

In de laatste 800 jaar heeft de filosofie zich vooral geconcentreerd op het bestaan in de ruimte en de theologie op het bestaan doorheen de (heils-)geschiedenis. Beiden moeten van hun eenzijdigheid verlost worden. De werkelijkheid van het kruis moet heden ten dage ook in de wetenschap doordringen. De confrontatie en uitwisseling met de grote culturen van het oosten kan daarbij helpen. Want Boeddha, Lao Tse, Abraham en Jezus vormen ieder een pool van het kruis der werkelijkheid, waar ook wij in binnentreden moeten, ook de moderne wetenschap.

#### Boeddha

Boeddha leefde in een cultuur die bezeten was van het buitenfront: de godenstrijd van het hindoeïsme. Dat was ook de strijd tussen kasten als van elkaar afgesloten grootheden. Alles werd waargenomen als een kracht in de ruimte. Boeddha laat de mens volledig oog worden, d.w.z. ook het eigen driftleven wordt objectief in het oog gevat, gaf en daarmee als het waardige geëxternaliseerd en in de ruimte buiten ons gezet, waardoor het leven aan de buitenpool gerelativeerd wordt. Want dit schept de mogelijkheid tot zelfverloochening.

#### Lao Tse

Chinees van karakter is de eeuwige afstemming, het harmoniseren en inspelen op elkaar. In de harmonie van de binnenwereld van de samenleving stemt iedereen zijn handelen op elkaar af. Lao Tse relativeert de stress, die dat met zich meebrengt. In alle drukte van belang, die het leven is, wist iemand het belang van zijn eigen drukte uit. Hij zoekt daarmee in een extra stap naar binnen als het ware nog eens ook de harmonie met zijn eigen drukte en ontdekt: De naaf van het wiel staat stil. Doe doende alsof je niets doet en als niets doende doe je toch wat. Wie de dolgedraaide werkelijkheid relativeert merkt na een poosje, dat zijn stilstand het centrum wordt van de werkelijkheid. Maak je niet druk, maar laat de kosmos bewegen, zo maakt men zich vrij van de stress, die de harmonisatie en het te veel aan vrede aan de binnenkant opleveren.

#### Abraham

Abraham en Jezus weten de tijden te verenigen. Abraham doet dat door de claim op zijn zoon los te laten: zijn zoon mag in een andere tijd leven, dan hij. Daarmee erkent hij God als vader van alle mensen. God overstijgt zijn eigen bestaan. De jood laat door zijn wachtende houding alle bestaande trouwverbintenissen relatief worden. Zo geeft hij betekenis aan het vaderschap, want vader is niet degene die zijn wil doorzet, maar die zich omwendt tot en die daardoor ruimte laat voor zijn kind. Dat is het onderscheid tussen tirannie en autoriteit.

#### Jezus

Maakt Abraham zich los van zijn eigen tijd met het oog op een grotere toekomst, Jezus maakt de omgekeerde beweging: hij leeft vanuit de toekomst terug in de eigen tijd en verbindt zo alle tijden. Elke tijd wordt een verbindingspunt en een ontmoetingspunt van het onvoltooide verleden met de zich voltooiende toekomst.

#### De sociale wetenschappen als ons Oude Testament

Ook genoemde religies hebben traditie gemaakt, maar men moet zich op die tradities niet blindstaren maar het creatieve moment erachter zoeken. De vier polen van de werkelijkheid, die bestaan in deze vier namen, maken het ons mogelijk ons los te maken van de objectiverende wetenschap en de mentaliteit van plannen maken en aansturen. Want deze vier namen openbaren de menselijke ziel en laten zien dat beslissingen, waar de menselijke ziel niet bij betrokken is, geen lang bestaan hebben. De ziel en niet planning van bovenaf maakt de mensen eensgezind. Dan ontstaat autoriteit in plaats van tirannie, dienst in plaats van drukte, gemeenschap in plaats van objectiverend denken, schepping in plaats van causaliteit. De moderne geschiedenis (verleden), economie (buiten), psychologie (binnen), en sociologie (toekomst) proberen deze innerlijke betrokkenheid van de mensen te vermijden door te objectiveren, te regelen, te rationaliseren en te plannen.


### VIII. Het vredesritme van ons heden

#### De vijand van de feestdag

De saaiheid van de arbeid, het doorgaande ritme van de strijd tegen de natuur, doet de feestdag, die gemeenschap sticht, verloren gaan. Vrijetijdsbesteding is iets anders dan de feestdag. De vrije tijd beweegt zich naar de randen van het bestaan. De feestdag bepaalt ons bij het centrum van het bestaan en is vaak een overwinning op een tragische gebeurtenis. Een gedenkdag.

#### De komende zondag

Hier komt Rosenstock terug op de tweedeling tussen voorstad en fabriek. De echte zondag, de feestdag, moet de eenheid tussen die beide herstellen. Het moet tenminste zo zijn, dat één en dezelfde mensenziel onder beide vormen kan voorkomen van voorstad en fabriek. Dat betekent, dat ons leven in deze twee ruimten, voorstad en fabriek, ingeschakeld moet worden in onze gang door de tijd. Daartoe is ritme nodig. Het traditionele ritme van het kerkelijk jaar is slechts een reflectie van de grote ervaringen van de mensheid, waaraan wij deelnemen op deze feesten.

#### Economie op korte termijn

Het gebrek aan ritme en aan gemeenschapstichtende hoge tijden heeft geleid tot mythen als van een duizendjarig rijk ter compensatie van dit verlies. Nu de economie alleen maar denkt in korte ritmen, heeft de mens in zijn eigen biografie langere ritmen nodig. Ontbreekt het daaraan, dan levert één of andere mythe een surrogaat voor een bestaan, dat spanning en beslissende momenten, kortom de gang door de tijd mist.

#### Strijders en denkers

De soldaten die (1947) uit de oorlog terugkeren, hebben moeten strijden onder de urgentie van de noodzaak een nieuwe toekomst mogelijk te maken. De denkers daarentegen hebben alle tijd, harmoniseren en systematiseren. Wat nodig is, is dat denkers en strijders, de mensen die het verleden voortdurend hernemen, en de mensen die handelend aan het front van de toekomst staan, elkander wederzijds doordringen. De denker moet zijn denkarbeid zien onder het voorteken van de urgentie van het probleem, dat opgelost moet worden. De strijdende en handelende mens moet zijn handelen in een langer tijdsperspectief leren zien.

#### Denken vanuit het uur

De academische wereld moet het tijdloze denken overwinnen, d.w.z. het denken laten leiden door de problemen, die zich in de tijd aandienen. Er is niet alleen een moreel tegenwicht tegen de oorlog nodig, maar ook een geestelijk tegenwicht. Alleen als een werkelijk probleem het denken dwars zit, kan het denken ook creatief worden. Daarmee treedt het denken en de wetenschap onder het kruis. Het is niet meer vrijblijvend en laat zich dwarszitten.

#### Het ritme van de nieuwe wereld (Amerika)

De droom van een nieuwe wereld in het Amerikaanse continent heeft een eigenaardig ritme geschapen. De pioniers waren gedreven door een groot ideaal, maar moesten telkens handelend optreden tegen de urgentie van het moment en hadden geen tijd van denken. De kloof tussen voorstad en fabriek wordt weerspiegeld in een soortgelijke kloof tussen harmoniseren en vreedzaam denken enerzijds en tegelijkertijd voortdurend verrast worden door een nieuw conflict, waarin men al betrokken wordt, alvorens er over nagedacht te kunnen hebben anderzijds. Omdat de Tweede Wereldoorlog de tweede is, zal Amerika daarin een gelegenheid kunnen vinden denken en strijden, en daarmee ook voorstad en fabriek op een nieuwe wijze op elkaar te betrekken. Vrede kan er niet altijd zijn. Elk conflict is een weg naar vrede en elke vrede voorlopig. Een soort sociale explosiemotor is nodig, een reeks van kleine conflicten, om grote uitbarstingen te voorkomen.
