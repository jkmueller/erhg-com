---
title: Mitgliederbrief 2010-10
category: rundbrief
created: 2010-10
summary:
zitat: |
  >"Jeder von uns braucht seinen geistigen Feind zur Gesundheit."
  >*Eugen Rosenstock-Huessy, Frankfurter Hefte 14, 1959, Heft 3, S.191*
---

## Eugen Rosenstock-Huessy Gesellschaft e.V.

{{ page.zitat }}

**Vorstand/board/bestuur:**\
*Dr. Eckart Wilkens (Vorsitzender);*\
*Wilmy Verhage; Andreas Schreck; Dr. Jürgen Müller; Thomas Dreessen*\
*Antwortadresse: Dr. Eckart Wilkens: Everhardstraße 77, 50823 Köln, Deutschland*\
*Tel: 0(049) 221 511265*

***Brief an die Mitglieder Juni 2010***

**Inhalt**

{{ page.summary }}


[zum Seitenbeginn](#top)
