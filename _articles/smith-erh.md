---
title: "Page Smith about Rosenstock-Huessy"
category: aussage
published: 2022-01-04
name: Smith
language: english
---


„He was a thinker of startling power and originality; in my view an authentic genius of whom no age produces more than a handful.”
>*Page Smith*
