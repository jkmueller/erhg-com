---
title: Mitgliederbrief 2012-12
category: rundbrief
created: 2012-12
summary:
zitat: |
  >*Heraklit zu Parmenides:*
  >In jeder feurigen Verwandlung werden Tugenden zu Lastern, Laster zu Tugenden. Deshalb ist meines Lebens „wird sein” nicht aus dem „ich war” allmählich abzuleiten. (…)
  >„War”, „ist”, „wird sein” kannst Du weder als Kreis, noch als gerade Linie zusammenfügen. Es sind drei entgegengesetzte Erscheinungen. Dazwischen liegen ausdrückliche Beerdigungen.
  >*Eugen Rosenstock-Huessy, (Zurück in) Das Wagnis der Sprache, Seiten 73, 75*
---

## Eugen Rosenstock-Huessy Gesellschaft e.V.

{{ page.zitat }}

**Vorstand/board/bestuur:**\
*Dr. Eckart Wilkens (Vorsitzender);*\
*Wilmy Verhage; Thomas Dreessen; Andreas Schreck; Dr. Jürgen Müller*\
*Antwortadresse: Dr. Eckart Wilkens: Everhardstraße 77, 50823 Köln, Deutschland*\
*Tel: 0(049) 221 511265*

***Brief an die Mitglieder Dezember 2012***

**Inhalt**

{{ page.summary }}


[zum Seitenbeginn](#top)
