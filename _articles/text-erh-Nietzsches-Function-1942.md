---
title: "Rosenstock-Huessy: Friedrich Nietzsche’s Function in the Church and the Crisis of Theology and Philosophy (1942)"
category: online-text
published: 2024-01-10
org-publ: 1942
language: english
---

#### Friedrich Nietzsche’s Function in the Church and the Crisis of Theology and Philosophy

*by*\
*Eugen Rosenstock-Huessy*

I. The unity of health and madness as a new historical phenomenon\
II. The conquest of space from 1100 to 1900: Thomas, Cusanus, Paracelsus, Descartes.\
III. Nietzsche’s program for the conquest of time.\
IV. Nietzsche’s masks as the price for his program.\
IVa. Insert 1944: Nietzsche’s Masks.\
V. The Deification of Man in the Church.\
VI. Blasphemy and Madness.\
VII. Theology and Philosophy have changed rapiers.

*(Paper prepared for the joint meeting of the American Association of Church History and the American Historical Association in December 1942. Cf. the letter of Stanley Pargellis May 18, 1943)*

**1. The unity of health and madness as a new historical phenomenon.**

Nietzsche is controversial. His significance has so many facets that Charles Andler dealt with five different aspects of his work in five separate volumes. A short paper on such a historical mountain can only have value when it proceeds by a severe discipline and method. Therefore, I shall not speak of Nietzsche’s place in the history of German thinking, nor in the crisis of Humanism in the narrower sense of its worship of Greek and Roman antiquity, nor even of his place in the decline of Europe or in the evolution of biography.

My topic is Nietzsche’s function in the Church and in the crisis of theology and philosophy.

...

*the complete text:* [„Friedrich Nietzsche’s Function in the Church and the Crisis of Theology and Philosophy”]({{ 'assets/downloads/ERH_Friedrich_Nietzsches_Function_1942.pdf' | relative_url }})

Eckart Wilkens translated the text into German: [„Friedrich Nietzsches Funktion in der Kirche und die Krise von Theologie und Philosophie”](https://www.eckartwilkens.org/text-erh-nietzsche-funktion-kirche/)
