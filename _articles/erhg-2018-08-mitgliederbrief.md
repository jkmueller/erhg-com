---
title: Mitgliederbrief 2018-08
category: rundbrief
created: 2018-08
summary: |
  1. Einleitung                                  - Jürgen Müller
  2. Rückblicke auf die Jahrestagung             - Sven Bergmann, Heinrich Treziak
  3. Entdeckung über Das Neue Denken             - Eckart Wilkens
  4. Wir müssen Beziehungen stiften              - Thomas Dreessen
  5. In Memoriam Feico Houweling                 - Otto Kroesen
  6. Jahrestagung und Mitgliederversammlung 2019 - Jürgen Müller
  7. Adressenänderungen                          - Thomas Dreessen
  8. Hinweis zum Postversand                     - Andreas Schreck
  9. Jahresbeiträge 2018                         - Andreas Schreck
zitat: |
  >Da kann die Ehe nur eine Bedeutung haben:\
  >Sie ist das Vorbild für alle Lösungsversuche, die nötig werden, um den Streit, den Krieg, den Haß zwischen Menschen zu überwinden. Wie die erste technische Erfindung vorbildlich bleibt für all die viel größeren nach ihr, so bleibt die Ehe das erste Beispiel eines geglückten geistigen Gesetzes. Auf sie wird jeder blicken müssen, der den Schlüssel zum Friedenstore seines eigenen Streitfalles sucht. Denn er findet nur bei ihr die rechte Mischung der Grundstoffe. Das Verschiedene muß verschieden bleiben; es darf sich nicht selbst preisgeben in feiger Entartung.\
  >…\
  >Weil die Zersetzung unter den Menschen täglich aufs neue hervorzubrechen droht, ist täglich eine neue Anstrengung des Geistes von nöten, das neue, das passende Gesetz für die hilflose Natur zu entdecken. Dies Entdecken des neuen Gesetzes ist die Aufgabe, die neben der Erfüllung des alten Gesetzes notwendig ist.
  >Wir dürfen also drei verschiedene Arten geistiger Kraftanwendung unterscheiden:
  >1. Die Kunst (Technik), mit der die äußere Natur gemeistert wird.
  >2. Das Gesetz (Ehe), mit der die eigene Natur geregelt wird.
  >3. Schließlich die Kraft, durch die jeder neuen Selbstzerstörung der eigenen Natur Einhalt getan wird durch Aufstellen einer neuen Lebensform. Diese sozusagen freie, unvorhergesehene Kraftanwendungsart des Geistes läßt sich technisch am genauesten durch das Wort „Liebe” bezeichnen.
  >*Eugen Rosenstock-Huessy, Die Hochzeit des Krieges und der Revolution, 1920*
---

## Eugen Rosenstock-Huessy Gesellschaft e.V.

{{ page.zitat }}

**Vorstand/board/bestuur:**\
*Dr. Jürgen Müller (Vorsitzender);*\
*Thomas Dreessen; Andreas Schreck; Dr. Eckart Wilkens*\
*Antwortadresse: Jürgen Müller: Vermeerstraat 17, 5691 ED Son, Niederlande*\
*Tel: 0(031) 499 32 40 59*

***Brief an die Mitglieder August 2018***

**Inhalt**

{{ page.summary }}

### 1. Einleitung

Liebe Mitglieder und Freunde,

Ende März fand in Essen unsere Jahrestagung und die Mitgliederversammlung statt. Mit dem Thema: „Deutsch – Europa – Planet Erde: Wie übersetzen wir den Volksnamen Deutsch in die Sprache des Menschengeschlechts?” gingen wir Rosenstock-Huessys Entdeckung des Volksnamens Deutsch und seiner in „JAKOB GRIMM SPRACHLOS” vorgeschlagenen Lösung nach. Es entstand ein Gespräch unter vielfältiger Beteiligung von dem zwei der Teilnehmer berichten.
 - Eckart Wilkens berichtet über seine Entdeckungen, die er bei der Textbearbeitung machte und Thomas Dreessen weist unter dem Titel „Wir müssen Beziehungen stiften!” auf einen vom Institut für Sozialstrategie veröffentlichten Beitrag hin.

Am 19. Mai diesen Jahres verstarb unser aktives Mitglied und ehemaliges Vorstandsmitglied Feico Houweling. Wir gedenken seiner in einem von Otto Kroesen verfassten Nachruf. Die Übersetzung aus dem Niederländischen ist von mir.

Das auf der Jahrestagung begonnene Gespräch wollen wir fortsetzen um neue Antworten für uns und unsere Zeit zu finden. Daß Anregungen, die Rosenstock-Huessy vor 90 Jahren gegeben hat, uns relevant und hilfreich erscheinen, ist eine freudige Entdeckung und ermutigt uns zu beidem, dem Studium Rosenstock-Huessys und dem Antworten auf die Herausforderungen unserer Zeit.
>*Jürgen Müller*


### 2. Rückblicke auf die Jahrestagung 2018

***Wider den egoistischen Blick auf „Ursprünge” deutscher und anderer Stammeswürden***

Die Eugen-Rosenstock Gesellschaft tagt in Essen-Werden, gegründet vom Friesen Liudger auf fränkisch-sächsischen Grenzgebiet

Der Name „deutsch” hört nicht auf, die Gemüter zu erregen. Dieter Borchmeyer, Professor für neuere deutsche Literatur und Theaterwissenschaften, geboren in Essen, Kenner Wagners, Nietzsches und Thomas Manns hat 2017 in seinem Opus Magnum die Frage „Was ist deutsch? Die Suche einer Nation nach sich selbst” erneut gestellt. Nahezu täglich wird in den Medien abgerechnet, was oder wer zu Deutschland „gehöre”. Der Name „deutsch” scheint dabei allen klar, wenn vielleicht auch nur wider besseren Wissens.
Dabei steht Deutschland nicht allein. Nicht nur in Europa blicken die Nationen eher zurück, als nach vorn, denken nicht groß, sondern ängstigen sich klein. Zwischen Spanien und Katalonien herrscht Sprachstörung. Die Südslawen zellteilen sich in neue Provinzen, Tschechen und Slowaken ziehen sich zurück, Ungarn und Polen wenden sich von Europa, ohne Wissen wohin, die Briten kappen gar elementare Lebensadern zum Kontinent, mit dem sie über mehr als tausend Jahre innigst verbunden sind. Shakespeares Dramen sprechen Bände! Der Separatismus feiert fröhliche Urständ. Die fast am stärksten verflochtenen Wirtschaftsmächte USA und China entzweien sich, fast wie Deutschland und England vor dem ersten Weltkrieg und der amerikanische Präsident setzt auf Kohle und Stahl, auf Oberarme statt auf Köpfe. Wen wundert es da, wenn auch in der islamischen Welt der Traum vom Kalifat als Erlösung scheint und heilige Kriege beschworen werden. Ganz zu schweigen von Stammeskriegen zwischen Hutu und Tutsi und Rohingya und Bengalen, auch wenn in diesen Konflikten, Land und Wasser, Eisenerz und Diamanten, Öl und Gold immer wieder befeuern.
Was hat uns Eugen Rosenstock-Huessy und ausgerechnet in einem Text über die Entstehung des Volksnamens „deutsch” im 8. Jahrhundert nach Christus zu all dem zu sagen? Denn keinesfalls ging es ihm um das „Totengräberamt” des Historikers.

Die Einleitung zeigt schon, dass die Organisatoren der Tagung vom 23. bis 25. März 2018Dr. Jürgen Müller, Thomas Dreessen, Andreas Schreck und Dr. Eckart Wilkenseinen Nerv der Zeit getroffen haben, als sie das Thema ihrer Versammlung im Gespräch über Eugen Rosenstock-Huessy bestimmten. Und der Ort „Haus am Turm” oberhalb der Werdener Abteikirche sorgte für weitere Symbolik, einem der bedeutendsten Bildungszentren im fränkisch-sächsischen Grenzgebiet, erschlossen vom Friesen Liudger und später mutmaßlich Entstehungsort des Heliand.
Dabei versteht sich die Eugen Rosenstock Gesellschaft ganz bewusst nicht als „akademische” Einrichtung. In der Gesellschaft steht das Hören dem Sprechen gleichberechtigt zur Seite und die akademische Stimme wiegt kein Gramm mehr als die Teilnahme jedes Einzelnen. Das bezeugte auch der Teilnehmerkreis, der etwa zur Hälfte Freunde von Eugen Rosenstock-Huessy aus den Niederlanden und Deutschland ins Gespräch brachte.

Neben der obligatorischen Mitgliederversammlung widmeten sich die Teilnehmer zwei weniger bekannten Texten Eugen Rosenstock-Huessys: dem Zeitungsbeitrag „Weshalb heißen wir Deutsche?” erschienen in der Kölnischen Zeitung vom 27. August 1932 sowie dem unveröffentlichten Gedicht „Jacob Grimm sprachlos”, das der Autor seinem Mentor Georg Müller zum 22. August 1953 zugeeignet hatte. Es war Georg Müller, Studiendirektor in Bielefeld-Bethel, der dem 1933 aus Deutschland Ausgewanderten nach Ende des Krieges als Erster wieder um seinen Rat bat, 1958 einen Sammelband „Das Geheimnis der Universität” mit Aufsätzen und Reden Eugen Rosenstock-Huessys herausgab und später mit dafür sorgte, dass der Nachlass im Landesarchiv der Evangelischen Kirche in Westfalen ein Haus und Archiv fand. Im angesprochenen Sammelband findet sich auch der Vortrag „Jacob Grimms Sprachlosigkeit” von 1953.
In dem auf eine breite Öffentlichkeit zielenden Zeitungsbeitrag kondensiert Eugen Rosenstock-Huessy Ergebnisse seiner Beschäftigung über mehr als 30 Jahre. Schon der 15-jährige hatte sich in Aufsätzen mit Notker dem Deutschen (St. Gallen) beschäftigt oder ein Jahr darauf Luthers Übersetzungsprinzipien erörtert.

*„Seit 1902 hat mein bewußtes Leben unter dem Kennwort „Sprache” gestanden. Ich war in meinem fünfzehnten Jahre und wünschte mir Kluges Etymologisches Wörterbuch der deutschen Sprache. Ich selber erstand Jakob Grimms Deutsche Grammatik von 1819 und seine Rechtsaltertümer, in denen ja das Wort eine gewaltige Rolle spielt. Hamanns Wort befiel mich damals: „Sprache ist der Knochen, an dem ich ewig nagen werde.”* (Eugen Rosenstock-Huessy, Ja und Nein, in: ders., Unterwegs zur planetarischen Solidarität: Sammeledition von Der unbezahlbare Mensch (1955), Ja und Nein – Autobiographische Fragmente (1968), hrsg. von Rudolf Hermeier, Münster: Agenda-Verlag 2006, S.239)

Im Studium bei Karl Zeumer in Heidelberg konzentrierte er seine Forschungen insbesondere auf den Ostfälischen Raum und die Entstehung des ersten Rechtsbuches in deutscher Sprache, dem Sachsenspiegel Eike von Repgows. Als Philologe und Rechtshistoriker und mit der ihm eigenen Akribie zog er ein vorläufiges wissenschaftliches Fazit in seinem Aufsatz von 1928, *„Unser Volksname Deutsch und die Aufhebung des Herzogtums Bayern”* in den Mitteilungen der Schlesischen Gesellschaft für Volkskunde, Bd.XXIX (1928). S. 1-66. Parallel zur deutsch-französischen Freundschaft nach dem 2. Weltkrieg fasste er 1957 seine Erkenntnisse erneut zusammen: *Eugen Rosenstock-Huessy, Frankreich – Deutschland. Mythos oder Anrede, Berlin: Käthe Vogt Verlag.*

Eugen Rosenstock-Huessy listete akribisch alle Quellenbelege auf und sondierte philologisch präzise die Kontexte: wer spricht in den Quellen zum wem, in welcher Form, mit welchem Inhalt und Zweck. 1934 wurden seine Forschungen durch Aufnahme in die von Götze editierte Auflage des Kluge, dem etymologischen Standardwörterbuch anerkannt, bevor in der folgenden Auflage auf Initiative von Leo Weisgerber alle Hinweise dem nationalsozialistischen Zeitgeist geopfert wurden. Erst Hermann Jakobs hat ab 1966 an Rosenstocks Erkenntnisse neu angeknüpft (zuletzt: ders., Diot und Sprache. Deutsch im Verband der Frankenreiche (8. bis frühes 11. Jahrhundert), in: Nation und Sprache. Die Diskussion ihres Verhältnisses in Geschichte und Gegenwart, hrsg. v. Andreas Gardt, Berlin/New York 2000, S. 7-46.).

Es kann hier nicht der Ort sein, die Arbeiten Eugen Rosenstock-Huessy zu referieren. Alle Texte stellen „deutsch” in seinen zeitgenössischen Kontext und rücken es damit in vollkommen neue Perspektive. Suchten Jacob Grimm, Richard Wagner oder Heinrich von Treitschke das deutsche Wesen in scheinbar urtümlicher Vergangenheit, in der „reinen” Sprache des Volkes, unverfälscht durch Politik, Herrschaft, Zivilisationund je älter ihre Belege, desto wertvoller, so entdeckt Eugen Rosenstock-Huessy genau das Gegenteil.
Festzuhalten sind wesentliche Aspekte seiner Argumentation:
1. Am Anfang steht der Name deutsch nicht für die Muttersprache, die einfache, unverbildete Sprache des Volkes, sondern für die stammesübergreifende königliche Herrschaftssprache in Rechtsprechung und als Befehl im Heeresverband, also als Sprache der militärischen Führungsschicht des Karolingerreiches.
2. Der Name deutsch führt nicht in eine mythische, möglichst ferne Vergangenheit. Die Quellenbelege deuten auf eine ganz konkrete politische Konstellation des späten 8. Jahrhunderts, bei der Formierung des Reiches Karls des Großen und die Zuwendung zu den bestehenden Stämmen. Der erste Beleg 786 liegt nur ein Jahr nachdem Karl der Große seinen Erzfeind, den sächsischen Anführer Widukind, nach der Taufe in Attingny, in sein königliches Haus aufgenommen hatte. Eine Generation später 919 ist Heinrich I. erster „deutscher” König aus dem Stamm der Sachsen.
3. Dreht Eugen Rosensock-Huessy die Perspektive des Namens „deutsch” aus der Vergangenheit in die Zukunft. Deutsch beschwört nicht ein Volk aus Urzeiten, dessen Quellen es noch zu erschließen gilt. Der Name deutsch bieten den Stämmen der Franken, Sachsen, Thüringer oder Bayern eine stammesübergreifende Zukunftsperspektive und eine gemeinsame Sprache. Deshalb schließen sich die Stämme auch nicht widerwillig an, sondern fühlen sich ernst genommen und geehrt.

Indirekt klärt Eugen Rosenstock-Huessy damit auch Fragen, die sich viele Schriftstelleretwa Stefan Zweig, Elias Canetti oder Thomas Mannimmer wieder gestellt haben. Etwa die eher eckige, aggressive und befehlsartige Tönung der Sprache. Die Parallele zum Griechischen und schließlich die immer wieder festgestellte Verwandtschaft zwischen Deutschtum und Judentum und daß das Deutsche eben gerade nicht ethnisch fundiert ist.
Deutschland ist eigenartig, das war in der Geschichte so und die Gegenwart beweist, dass es bei allen krampfhaften Glättungen auch weiter so ist. In einem Brief von 1931 an Carl Schmitt zieht Eugen Rosenstock-Huessy eine Art „deutsches” Fazit: „Deutschland lebt aus der Mehrzahl seiner Staatlichkeit. Vielleicht ist das der wichtigste Satz seines Staatsrechts!” Ist es Ignoranz, Dummheit, Überheblichkeit oder schlicht Faulheit, dass der Autor, ohne den es weder den Titel einer „Deutschen Gesellschaftsgeschichte” noch den eines „Wegs nach Westen” gäbe, in allen Diskussionen über einen deutschen „Sonderweg” unbeachtet bleibt?

***Namen besiegeln Frieden oder Krieg***

Vor diesem Hintergrund erklärt sich auch die besondere Bedeutung des besprochenen Gedichtes „Jakob Grimm sprachlos” mit der Widmung an Georg Müller. Kassel ist die Wirkstätte von Jakob und Wilhelm Grimm. Hier haben sie ihr Leben der deutschen Sprache gewidmet, an der sich auch Eugen Rosenstock-Huessy nach eigener Aussage sein Leben lang abnagen sollte. Während seiner Militärzeit in Kassel und später traf er sich mit Franz Rosenzweig in dessen Kasseler Elternhaus. Kassel ist auch der Geburtsort Georg Müllers, der sich als Lehrer Jakob Grimm besonders verbunden weiß. In Georg Müller sieht Eugen Rosenstock-Huessy die Begeisterung seiner eigenen Jugend lebendig. Aber von dieser Sicht hat er sich gründlich kuriert, ohne deshalb die Bedeutung Grimms zu verleugnen.

  *Das bißchen Brüder-Grimm-Grammatik*\
  *hat die Nationen neu befeuert,*\
  *in blut‘ger kriegerischer Pathik,*\
  *ward nun der Völker Du beteuert.*

Dies ist seine Mahnung an Georg Müller: Übertriebene Romantik, übertriebene Selbsterhöhung und ein übertriebenes „Zurück zur Natur” muss wie 1914 bis 1945 erneut die Völker gegeneinander aufbringen und neue Kriege zur Folge haben. Dabei ist es längst Zeit für

  *Dienst auf dem Planeten.*\
  *Liebt Pol‘ und Meere des Planeten,*\
  *nehmt eure Erd‘ aus Vaterhänden,*\
  *laßt euch zu Erdbewohnern kneten,*\
  *dann kann der Mutter Fluch sich wenden.*

In das mehr als 20-strophige Gedicht führte Dr. Eckart Wilkens kommentierend ein und rezitierte das gesamte Werk schließlich auch mit großem rhetorischen Ernst. „Georg Müller zugeeignet 22. August 1953” verweist auf den 4. Todestag seines Breslauer Freundes Joseph Wittig und die Zeit nach dem Tod Stalins. Die Liebe zu Polen und Meeren des Planeten steht über der Entzweiung der Erdbewohner in Ost und West. 2018, in Perspektive auf das dritte Jahrtausend sind wir noch nicht viel weitergekommen.
Es war ein kraftvoller Zug der Tagung in Werden, dass wirklich alle Teilnehmer vom ersten bis zum letzten Tag im Gespräch blieben. Vor dem üblichen Tagungstourismus klangvoller Referenten war das Gespräch in gemeinsamer Andacht vor dem Namen Eugen Rosenstock-Huessys gefeit.

*„Die erste Sprachstufe, die täglich daher zu erneuernde Sprachstufe des Menschengeschlechts ist die gegenseitige, reziproke namentliche Anrede. Auf ihr beruht aller Friede unter den Menschen. Ein Sohn kann seinen Vater als Vater anreden, ohne sich seiner Freiheit zu begeben. Denn im Namen „Vater” steckt für diesen Sohn ein Freiheitsmoment; wenn er den Vater so anspricht, muß der Vater ihn als Sohn behandeln! Als die Franzosen und Deutschen anfingen, einander als Gallier und Germanen zu schimpfen, verloren sie ihre Bruderschaft aus der karolingischen Erbschaft. Wer nämlich die fränkische Kommandosprache sprach, hieß Deutscher; wer nur im Frankenland ansässig war, hieß Franzose. Seit 786 binden wir diese Scheidung in Deutsche und Wälsche Franken. Im Römerreich aber hatten sich Gallier und Germanen nichts zu sagen. Und die Rebarbarisierung Europas ging mit dem leichtfertigen Einführen der Schulbuchvorstellung Germane und Gallier Hand in Hand. Die Reichsteilung von Verdun wurde nämlich durch diesen Rückgriff hinter die echten Namen um tausend Jahre zurückdatiert, und tausend Jahre sind kein Pappenstiel. Tausend Jahre falsche Geschichte ermöglichen Hitler die Besetzung Norwegens oder bringen die Polen nach Stettin. Als Provinzialen des Römerreichs hatten Helveter, Iberer, Germanen, Ägypter einander nichts zu sagen, es sei denn über den Umweg Rom. Aber Deutsche und Franzosen können sich immer gegenseitig anreden. Die Verwechslung des Namens „Deutsch” mit dem Begriff „germanisch” hat Europa vernichtet. Weil im Namengeben Gegenseitigkeit herrscht, weil ich Bruder bin, wenn ich „Schwester” sage, weil alle Namen Doppelköpfe auf einem einheitlichen Leib sind wie Vater und Mutter, deshalb hat nie ein Mensch zuerst die Sprache erfunden. Ebensowenig haben alle zugleich gesprochen. Sondern Menschen haben einander angesprochen, und den Namen, den du mir gabst, hat der Name, den ich dir gab, entsprochen. Sprache ist Korrespondenz, bevor sie irgendetwas anderes ist.”* (Eugen Rosenstock-Huessy, Der Atem des Geistes, Frankfurt am Main: Verlag der Frankfurter Hefte 1950, S.61/62.)
>*Sven Bergmann*


***Rückblick auf die Eugen-Rosenstock-Huessy-Tagung an Palmarum 2018 in Essen***

Wohltemperierte Gespräche im evangelischen Bildungshaus „Am Turm” in Essen prägten die Eugen-Rosenstock-Huessy-Tagung. „Da tagete es” (Muspilli) wirklich. Es erklärte sich der/die eine und andere Teilnehmer In zunächst durch Darlegung einer knappen Lebensskizze und Erläuterung des Motivs, an der Tagung teilzunehmen. Gut vorbereitete Beiträge und Hinführungen in die Thematik des „Diutisk” und freies Extemporieren im Blick auf die Hörer belebten den Vortrag. Leben des Geistes! Wichtig wurden die Gespräche in der Kleingruppe, wurden die eher zufälligen vis-a-vis-Begegnungen, wurden die Mahlzeiten. Die Sinnspitze der Tagung: Dr. Eckart Wilkens Erläuterungen zu Rosenstock-Huessys Gedicht „Jacob Grimm Sprachlos. An Georg Müller”. Hermetik versus Hermeneutik. Wer Orpheus hinab ins Abgründige nicht Schritt für Schritt folgt, bleibt außen vor. Zu verstehen freilich gibt es für jeden etwas, nicht aber fühlige Berührung mit des Meisters Stimme selber. Nochmals mein Danke!

Als Dietmar Kamper und ich im Frühherbst des Jahres 1964 Eugen Rosenstock-Huessy im Hause der Freya von Moltke in Heidelberg aufsuchten – ex post wurde diese einmalige Begegnung für uns zum Besuch –, sagte der Meister u.a. gar nicht beiläufig: „Wer vor mehr als zwölf Leuten spricht, der lügt.” Ein apostolisches Maß als Vorgabe für riskiertes Sprechen? Wir beiden damals jungen Leute, die natürlich wie alle jungen Menschen die Welt verändern wollten, konnten auf der Rückfahrt von Heidelberg nach München nur schweigen. Er – nicht es – hatte uns die Sprache verschlagen, Index vielleicht für Neues, für Geistgeburt. Ist wirkliches Sprechen als sprechende Wirklichkeit nur durch Schweigen ermächtigt? Eugen Rosenstock-Huessy sagt: „Das Du kommt nicht im Reden, sondern im Schweigen zur höchsten Entfaltung”. Das wäre ein Leben lang zu lernen und kann in Gesprächen, die diesen Namen verdienen, zum Austrag kommen.

Mir war die Essener Tagung eine Geistgeburt. Dass einig- einigender Geist einem nur selten an Tagungen teilnehmenden Menschen wie mir sich so freundlich- freundschaftlich schenkte, ist ungewöhnlich. Information ist immer auch Deformation. So gesehen, lassen sich die Abläufe eines Tagungsgeschehens natürlich referieren; aber vermittelte Unmittelbarkeit, die qua Begriff dem Verstand genügt, ist halt nicht die Sache selbst, die sich nur als Gespräch, das wir sind, ereignet. Man muss dabeigewesen sein, einsteigen in das Fahrzeug „Gespräch” und mitgenommen werden. Was jemand wirklich zu sagen hat, bringt er nicht immer schon mit, um es als festen Wissenserwerb weiterzugeben, vielmehr ist es das horchend-hörende Ohr des anderen, das als Organ das Wort im Sprechenden zeugt und ihm zur Geburt verhilft. Der Geist, der eine Gemeinschaft beseelt, ist ja nichts Abstraktes; er ist die Grundkraft des Lebens selber. Geistgeburt ereignet sich eher selten, aber doch allerorten, z. B.im Konzertsaal. Da herrscht zunächst ein eigentümliches Durcheinander, bis jedes Instrument auf den Kammerton „a” gestimmt ist. Das ist die Voraussetzung für gelingendes Spiel, für Einklang der verschiedenen Instrumente; Dissonanz und Chromatik einbegriffen. Wer den Kammerton „a” nicht akzeptiert, spielt falsch; konzertiert nicht, wetteifert nicht, streitet nicht im guten Sinne des Wortes (lat. concertare= wetteifern/streiten). Die Trompete ist ein lautes Instrument, die Harfe ein leises. Die Harfe darf nicht Trompete sein wollen, und die Trompete nicht Geige. Jedes Instrument ist ganz es selbst mit eigenem Ausdruck, dennoch aufs „größere Wir” hin den anderen Instrumenten gegenüber verpflichtet. Der Kammerton „a” ist so etwas wie – im übertragenen Sinne, versteht sich – der Urton des Ganzen und Heilen, das Brausen „Heiligen Geistes”, auf den hin sich alle Töne ins einige Eine versammeln; der aber auch jede Tonsprache wie jeden Sprachton in deren Mannigfaltigkeit freigibt.
So ist Musik m.E. ein besonders zeigendes Beispiel für Versammlung, Tagung, Gespräch, für deren Gelingen. Jeder Teilnehmer einer Tagung ist ein Exzenter der anstehenden Thematik. Jeder Teilnehmer ein Instrument, für dessen Stimmung er vor und während jeden Konzerts, jeder Tagung, jeder Begegnung zu sorgen hat und verantwortlich ist. Bleibt das Instrument verstimmt, legt sich dies negativ über den Geist des Ganzen. (Ein guter Geiger, habe ich mir sagen lassen, stimmt sogar sein Instrument, wenn nötig, auch während er spielt.)
Sollte der funktionale Term „Kammerton a” im Weltumgang von Personen einen Namen tragen, so den von Christus, aber auch den Namen derer, die entschieden dem Mensch gewordenen Logos in dessen Sprachspur folgen. In Essen war dies zu spüren.
>*P.Heinrich Treziak OMI*


### 3. Entdeckung über Das Neue Denken

Ich habe inzwischen Eugen Rosenstock-Huessys Soziologie in zwei Bänden 1956/1958, Franz Rosenzweigs Stern der Erlösung 1920, Rudolf Ehrenbergs Metabiologie 1950 und Ernst Michels Buch Ehe 1948 neu gliedernd nach der grammatischen Methode bearbeitet und dabei ist mir aufgegangen, wie die Lehre vom Kreuz der Wirklichkeit in diesen Sprechern, die alle in der Zeitschrift "Die Kreatur" versammelt waren, bezeugt ist. Dabei läßt sich sehen, daß die Lehre präjektiv von Rosenstock-Huessy, subjektiv von Ernst Michel (sein Ehebuch geht in die Nähe, die alle angeht), trajektiv von Rosenzweig, objektiv von Rudolf Ehrenberg vertreten wird. Ja, man kann sagen, daß das Hauptwerk Rosenstock-Huessys in deutscher Sprache nur erscheinen konnte, weil Ernst Michel und Rudolf Ehrenberg durch die Nazi-Zeit hindurch die Treue gehalten und Eugen Rosenstock-Huessy die Rückkehr nach Deutschland ermöglicht haben. Also nicht nur der Inhalt dieser Werke geht uns an, sondern die Sprechweise, die die Umkehr des Wortes auf den Sprecher zum Motor des Sprechens und Darlegens hat.
Wer auf sein Wort hört, gelangt ins Innere, wer weiterhört, tritt in die Geschichte ein, wer wiederum weiterhört, bringt eine mitteilbare Frucht. Wer nicht auf sein Wort selber hört, für den gilt, was von dem auf den Weg gefallenen Samen gesagt ist: der Böse kommt und raubt, was in sein Herz gesät ist. Wer in dem Inneren steckenbleibt (also auch der gesamten Welt des Reflexivums mit Kunst und Wissenschaft), von dem heißt es, daß das Wort verdorrt, weil es keine Wurzel in der Wirklichkeit hat. Wer nur in der Geschichte lebt und denkt, da sei nun alles erledigt, für den gilt, daß Sorgen und die Blendung des Reichtums das Wort ersticken. Wer diese Prüfungen des Hörens auf das Wort besteht, bringt Frucht. In der Reihenfolge des Erscheinens: Rosenzweig, Michel, Rudolf Ehrenberg, Rosenstock-Huessy von 1920 bis 1958 ist also der Auszug aus der Nationalepoche für uns alle nachzugehen.
>*Eckart Wilkens*

### 4. Wir müssen Beziehungen stiften

Freya von Moltkes Testament: Wir müssen miteinander leben lernen! weist uns auf die Lehre ihrer 2 Männer, Helmuth James von Moltke und Eugen Rosenstock-Huessy. Ohne Sprechen zueinander und hören aufeinander gibt es keine Zeit, ja ohne Sprechen gibt es uns Menschen nicht: Nicht die Generationen und auch nicht einzelne Menschen, aber auch nicht Frieden zwischen den Völkern. Frieden ist nicht Natur, kann nicht einfach vorausgesetzt werden, sondern muß ausdrücklich zwischen verschiedenen, getrennten, verfeindeten geschaffen werden. Frieden muß gestiftet werden.
Auf der website des Instituts für Sozialstrategie konnte ich jetzt zu diesem Thema einen Essay veröffentlichen der die Herausforderungen und die Chancen für das Miteinander-leben-lernen in unserer Zeit beleuchtet im Lichte ERHs: [Link](https://www.institut-fuer-sozialstrategie.de/2018/07/12/zur-epochenaufgabe-wir-muessen-miteinander-leben-lernen-freya-von-Moltke)

***Dazu eine kleine Geschichte:***\
Der Traum des Königs!\
Es war einmal ein König, dessen Völker konnten nicht zusammenkommen. Jedes lebte immer mehr für sich. Sie sprachen nicht mehr miteinander, sie stellten den Handel ein, schlossen Trefforte und Gasthäuser und verboten ihren Kindern Freundschaften und Heiraten außerhalb ihres eigenen Volkes. Auf der Straße, auf der Arbeit und in der Schule liefen immer mehr aneinander vorbei, den Kopf gesenkt und ohne Gruß. Immer kälter wurde es dort und in den Himmeln. Öfters geschahen böse Worte und Rempeleien, Häuser der anderen wurden angesteckt, Kriege entflammten, und das Land wurde unfruchtbar. Immer häufiger folgten die Menschen irgendwelchen dahergelaufenen Schwätzern, die ihnen nach dem Mund redeten und ihre Kraft auf die Vertreibung und Vernichtung der anderen richteten.
An einem Morgen, der König hatte wenig geschlafen vor Sorgen, klingelte es und er hörte eine Stimme:
*„Höre König! Ich, Salomo grüße dich und sage Dir: „Wo keine Offenbarung ist, wird das Volk wild und wüst.” In deiner Sprache gesagt: „Ohne Vision geht ein Volk zugrunde.”(Spr 29) Ich sage Dir: Steh auf! Bau mit Ihnen ein Haus der Völker. Bau es auf dem Berg, dass sie zusammen weit schauen können, und miteinander tanzen, und singen und Gott loben in vielen Sprachen. Dorthin sollen sie ihre jungen und alten Leute senden. Dort wird geschehen, was Gott dem Joel sagte: „Es soll geschehen in den letzten Tagen. Dann will ich ausgießen von meinem Geist auf alles Fleisch und eure Söhne und Töchter weissagen und eure Jünglinge sollen Gesichte haben und eure Alten werden Träume haben.”(acta 2,16f) Das Haus wird ein gemeinsames Sonntagshaus sein: Komm Heilger Geist, der uns verbindet und Leben schafft! Freundschaften entstehen und werden gepflegt und wachsen. Sie werden Orientierung bekommen, Kraft und Mut den Alltag in den Dörfern und Städten deines Reiches miteinander zu gestalten. Alle 7 Jahre werden sie ihr ganzes Leben lang ihren Alltag verlassen für eine Zeit und sich dort versammeln und sich stärken wie an einer Tankstelle. Dann ziehen sie aus wie die Argonauten und dienen gemeinsam ein Jahr für die Erneuerung und Erhaltung des Lebens auf unserem Planeten. Und Gott blickt auf Euch und segnet Euch und schenkt Euch Leben von Weltzeit zu Weltzeit.”*
Die Stimme verklang doch der König konnte sie nicht vergessen. Er holte tief Luft und atmete aus, und der Druck fiel von ihm ab. „Sie brauchen Ziele! Sie brauchen Möglichkeiten ihre tiefen Sehnsüchte nach Frieden und Gerechtigkeit miteinander zu leben! Dann kann der Krieg aufhören.”
Er stand auf und öffnete den Vorhang. Draußen war die Nacht vergangen und die Morgenröte erleuchtete sein Angesicht. Er rief seine Minister zusammen und erzählte ihnen von seinem Traum. Sie hörten ihm zu und ihre Gesichter leuchteten. Ein neuer Tag hatte angefangen.
>*Thomas Dreessen*

### 5. In Memoriam Feico Houweling

Im Laufe der Jahre habe ich persönlich viel mit Feico zusammengearbeitet und wir hatten vielfältigen Kontakt. In den 1990er Jahrenerst seit dieser Zeit wurde ich in Rosenstock-Huessy-Kreisen aktivreisten wir regelmäßig zu Konferenzen in Deutschland und einige Male in die USA. Rund um die Konferenz besuchten wir damals auch einige Leute. Wir haben viel zusammengearbeitet: bei der Übersetzung der "Europäischen Revolutionen", im Vorstand von Respondeo und bei vielerlei Anlässen. Längere Zeit zusammen im Auto zu reisen ist eine gute Gelegenheit, tiefer ins Gespräch zu kommen. Ein Gespräch nicht nur darüber, wie man über das Werk Rosenstock-Huessys denkt, sondern auch über das eigene Leben in Bezug dazu und darüber hinaus. Wie man mit seiner eigenen Arbeit umgeht und wie sie durch Erkenntnisse Rosenstock-Huessys beeinflußt wird und wie sie darin hängen bleiben kann. Ich will das hier nicht versuchen näher zu beschreiben. Vielleicht wird dafür die Zeit noch kommen. Ich möchte nur ausdrücken, wie es sich anfühlt, wenn solch ein Menschnoch dazu fast ein Altersgenosseeinem wegfällt. Die Toten und Lebenden sind stärker aufeinander bezogen als wir oft denken. Oder wie Heraklit es kompakt ausdrückt: "Sie sterben unser Leben; wir leben ihr Sterben". So nehme ich es auch wahr.

Feico arbeitete auch viele Jahre beim Rosenstock-Huessy-Fund in Amerika mit, bei Konferenzen, bei Hauskonferenzen, die jedes Jahr im Juni in Vermont von der Familie organisiert wurden und als offizieller Vertreter der Eugen Rosenstock-Huessy-Gesellschaft beim Fund. In den letzten Jahren war ihm dies nicht mehr möglich, aber Feico meldete sich regelmäßig auf der seit einigen Jahren bestehenden Mailingliste der Rosenstock-Huessy-Society, von der die Mailingliste der letzte Rest ist, zu Wort. Ich habe die folgende Nachricht, hier ins Deutsche übersetzt, auf dieser Mailing-Liste veröffentlicht.

Liebe Freunde,

In der Nacht vom 18. auf den 19. Mai verstarb unser Freund Feico Houweling im Schlaf. Er hat sich an dieser Mailing-Liste beteiligt und auch davon berichtet, wie sich seine Gesundheit verschlechterte.

Er war an Krebs erkrankt und nach der Entdeckung vor fünf Jahren, erholte er sich zunächst wieder und war weiterhin bei guter Gesundheit, allerdings ohne seine ursprüngliche Energie. Die Krankheit kehrte dann mehrmals zurück.

Feico Houweling studierte Geschichte und arbeitete als Journalist, Schriftsteller und Geschichtenerzähler. Er gab seinen Beruf als Journalist auf und startete anstelle dessen den Versuch, eine Hörerschaft als Geschichtenerzähler für Geschichtserzählungen zu finden. Er fand diese Hörerschaft bei vielen Gelegenheiten und besonders bei den Geschichtsvorlesungen der Senioren-Universität überall in den Niederlanden. Diese Hörerschaft fand er auch über seine Website und seine Podcasts. Er veröffentlichte auch mehrere Bücher auf Niederländisch, Romane, Geschichtsbücher, darunter auch kürzlich ein Buch über die Bedeutung von Erasmus für die niederländische und Rotterdamer Geschichte.

Er übersetzte auch das in Amerika erschienene Werk von Rosenstock-Huessy über die europäischen Revolutionen, "Out of Revolution; Autobiography of Western Man", was eine große Aufgabe war. Es ist das Standardwerk von Rosenstock-Huessy auf dem niederländischen Markt.

Des weiteren war er auch im Vorstand der Eugen Rosenstock-Huessy-Gesellschaft und des American Fund tätig und war drei Jahre lang Vorsitzender von Respondeo, der niederländischen Rosenstock-Huessy-Vereinigung.

Er lebte mit seiner Frau Joke und seinen Kindern vier Jahre lang im Rosenstock-Huessy-Haus in Haarlem. Dieses Haus fungierte als wichtiges Zentrum sozialer Aktivitäten und als Aufnahmezentrum für ehemalige psychiatrische Patienten. Er legte immer großen Wert auf die Bedeutung sozialer Dienste als Mittel zur Gesundung der menschlichen Seele. Er bewunderte die Ereignisse um die deutschen Arbeitslager, in die Rosenstock-Huessy involviert war, den Kreisauer Kreis und das Lager William James. Auch bei den amerikanischen Mitgliedern von Rosenstock-Huessy-Kreisen ist sein Interview mit Bob O'Brien über Camp William James wohlbekannt.

Feico hatte die Gabe eines unglaublich klaren und direkten Sprachgebrauchs sowohl in seinen Übersetzungen als auch in seinen literarischen Werken. Er konnte hart und mit Überzeugung und Hingabe arbeiten. Und er konnte professionell und präzise sein. Seine Leidenschaft war nicht direkt an der Oberfläche zu sehen, aber man konnte feststellen, dass hinter seinen Worten mehr verborgen war.

Wenn Gottes Schöpfung ein Gedicht ist, wie Rosenstock-Huessy dies ausdrückte, so hat Feico Houweling sicherlich einige Verse daran hinzugefügtfür Zeit und Ewigkeit.

>*Otto Kroesen*
>*Vorsitzender von Respondeo*

### 6. Jahrestagung und Mitgliederversammlung 2019

Die Jahrestagung und die Mitgliederversammlung 2019 möchten wir, wie dieses Jahr, an Palmarum (12.14.4. 2019) im Haus am Turm, am Turm in Essen-Werden abhalten.
Wir bitten Sie sich diesen Termin vorzumerken. Thema und Programm werden wir Ihnen noch mitteilen.
>*Jürgen Müller*

### 7. Adressenänderungen

Bitte schreiben sie eine eventuelle Adressenänderung schriftlich oder per E-mail an Thomas Dreessen (s. u.), er führt die Adressenliste. Alle Mitglieder und Korrespondenten, die diesen Brief mit gewöhnlicher Post bekommen, möchten wir bitten, uns soweit vorhanden ihre Email-Adresse mitzuteilen.
>*Thomas Dreessen*

### 8. Hinweis zum Postversand

Der Rundbrief der Eugen Rosenstock-Huessy Gesellschaft wird zukünftig als Postsendung nur noch an Mitglieder verschickt. Nicht-Mitglieder erhalten den Rundbrief gegen Erstattung der Druck- und Versandkosten in Höhe von € 20 p.a. Den Postversand betreut Andreas Schreck. Der Versand per e-Mail bleibt unberührt.
>*Andreas Schreck*

### 9. Jahresbeiträge 2018

Die „Selbstzahler” unter den Mitgliedern werden gebeten, den Jahresbeitrag in Höhe von 40 Euro (Einzelpersonen) auf das Konto Nr. 6430029 bei Sparkasse Bielefeld (BLZ 48050161) zu überweisen.
Nach Einführung des einheitlichen Zahlungssystem SEPA mit der Internationalen Kontonummer (IBAN): DE43 4805 0161 0006 4300 29
SWIFT-BIC: SPBIDE3BXXX ist diese „IBAN”-Nummer auch für Mitglieder in den Niederlanden und im ganzen „SEPA-Land”, in das sich sogar die Schweiz hat einbetten lassen, ohne Zusatzkosten nutzbar.
>*Andreas Schreck*

[zum Seitenbeginn](#top)
