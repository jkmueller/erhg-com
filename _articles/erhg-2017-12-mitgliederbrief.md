---
title: Mitgliederbrief 2017-12
category: rundbrief
created: 2017-12
summary: |
  1. Rückblick auf die Jahrestagung                               - Jürgen Müller
  2. Aus dem Bericht des Vorsitzenden                             - Jürgen Müller
  3. Die Fragen, die uns das Leben stellt: Die Arbeit der Kreatur - Thomas Dreessen
  4. Luthers Tafel der Werte                                      - Andreas Schreck
  5. Am Sonntagmorgen                                             - Eckart Wilkens
  6. Kurzbericht über die Mitgliederversammlung                   - Andreas Schreck
  7. Dank an Rudolf Kremers                                       - Jürgen Müller
  8. Zum Tod von Karl-Heinz Kämper                                - Andreas Schreck
  9. Jahrestagung 2018                                            - Jürgen Müller
  10. Adressenänderungen                                          - Thomas Dreessen
  11. Hinweis zum Postversand                                     - Andreas Schreck
  12. Jahresbeiträge 2017                                         - Andreas Schreck
zitat: |
  >Solange der Mensch noch Aufträge hat, bleibt der Körper auch im Alter noch gefügig. Die Arbeit zerstört ihn nicht, auch die 16-stündige Arbeit nicht, wenn er einen solchen Wirkungskreis ausfüllen kann. Er erneuert sich aus der geistigen Berufung heraus. Wir arbeiten "spielend"!\
  >Das ändert sich alles grundweg in dem Augenblick, wo die Arbeit nicht mehr im Dienst des geistigen Berufes steht, wo der körperliche Mensch nicht mehr an einen geistigen Auftrag gebunden arbeiten muß.\
  >Im Großen gesehen ist in der modernen Gesellschaft darüber kein Streit: für eine Unmasse Menschen, für viele Millionen, ist der Wirkungskreis nicht mehr da, der dem lutherischen Beruf entspricht.
  >Es ist so, daß der Beruf überall angekränkelt wird, weil die von den Individuen aufgebaute Wirtschaft heute ihre Eigengesetzlichkeit, ich sage lieber: ihre Gesetzlichkeit enthüllt.\
  >An Stelle der Freiheit im Reiche der Sittlichkeit, in der Wirtschaft, tritt heute plötzlich das Gesetz.\
  >Das ist die große Wende, vor der wir stehen.\
  >Die Wirtschaft wird zwangsläufig und rationell wissenschaftlich.
  >*Eugen Rosenstock-Huessy, Die Verklärung der Arbeit, in Das Alter der Kirche, Band 2, p.815, 1927*
---

## Eugen Rosenstock-Huessy Gesellschaft e.V.

{{ page.zitat }}

**Vorstand/board/bestuur:**\
*Dr. Jürgen Müller (Vorsitzender);*\
*Thomas Dreessen; Andreas Schreck; Dr. Eckart Wilkens*\
*Antwortadresse: Jürgen Müller: Vermeerstraat 17, 5691 ED Son, Niederlande*\
*Tel: 0(031) 499 32 40 59*

***Brief an die Mitglieder Dezember 2017***

**Inhalt**

{{ page.summary }}

### 1. Rückblick auf die Jahrestagung

Im Lutherjahr 2017 haben wir uns mit der Aktualität von Luthers „Die Freiheit eines Christenmenschen” befasst. Als Textgrundlage diente uns ein 90 Jahre alter Essay von Rosenstock-Huessy: Die Verklärung der Arbeit. Dieser Text erwies sich als verblüffend aktuell.
Rosenstock-Huessy stellt darin dar wie die evangelische Freiheit im Leben der Menschen zur Zeit Luthers Gestalt gewonnen hat. Luthers theologische Entdeckung fand eine Entsprechung im alltäglichen Leben. Diese Entsprechung besteht durch die zunehmende Verwissenschaftlichung vieler Lebensbereiche nur noch in sehr eingeschränktem Maße. Rosenstock-Huessy zeigt wie die moderne Arbeitswelt kreative Unterbrechungen für Tag, Woche und Jahr eingebaut hat. Größere biographische Einschnitte sind allerdings erforderlich und bedürfen einer sozialen Gestaltung.

Wir haben uns in mehreren Gesprächsrunden mit unserer veränderten Arbeitswelt und ihrem Einfluß auf den Rhythmus unseres Lebens befasst. Schwerpunkte waren dabei erfahrene Brüche im Arbeitsleben, die aus wirtschaftlichen Gründen erforderliche Inachtnehmung des ganzen Menschen und die Kräfte, die unseren tagtäglichen Kalender bestimmen.

Ich habe den Eindruck, daß Rosenstock-Huessys Arbeiten aus der Zwischenkriegszeit für unsere Situation mit ihrer Veränderungsdynamik und den sich daraus ergebenden Unsicherheiten besonders relevant sind.

Für das kommende Weihnachtsfest wünsche ich Ihnen, daß sie sich als „Weihnachtsgeschenk und Erbe aller guten Dinge von altersher” erleben, wie Rosenstock-Huessy herausgefordert durch Nietzsche die erste der „drei existentiellen Masken jeder vergöttlichen Seele” beschreibt.
>*Jürgen Müller*

### 2. Aus dem Bericht des Vorsitzenden

Welche Schwerpunkte sehe ich für die Eugen-Rosenstock-Huessy Gesellschaft?
1. Phase: Sicherung des Originalmaterials via Archivierung und Digitalisierung
2. Phase: Zugänglich machen via Buch, Publikation, Übersetzung, Bearbeitung
3. Phase: Textstudium und Anwendung – Netzwerk

Die erste Phase ist abgeschloßen. Für die zweite Phase sind mir folgende Beiträge bekannt:
- Norman Fiering (2017): The Structure of Significant Lives, The European Legacy
- Thomas Dreessen: „Wir schaffen das!” – aber wie, Frau Merkel? in: Dirk Siedler (Hg.) Dialog und Begegnung – Impulse für das Gespräch zwischen Christentum und Islam, Verlag Vandenhoeck und Ruprecht, Neukirchener Theologie 2017/4
- Marlouk Alders: Wenn Hitler fällt, dte Übersetzung von Ilse Bonow und Tijmen Aukes des nl Originals: Als Hitler valt, 2017 – Veröffentlichung der Übersetzung in Vorbereitung
- Soziologie I Übersetzung ins Englische unter dem Titel: In the Cross of Reality: The Hegemony of Spaces by Eugen Rosenstock-Huessy Herausgeber: Wayne Cristaudo, Francis Huessy; Übersetzer: Jurgen Lawrenz Taylor & Francis Inc\
  Aus dem Klappentext:\
  *This book makes the first volume of Eugen Rosenstock-Huessy's Soziologie available in English for the first time since its 1956 publication in German. Rosenstock-Huessy argues that social philosophy has favored abstract and spatially contrived categories of social organization over temporal processes. This preference for space-thinking has diverted us from recognizing the power of speech and its relationship to living on the front lines of life.
  Taking speech and the social responsibilities and reciprocities that accompany naming as the key to social reality, In the Cross of Reality provides a sociological exploration of -play- spaces as the basis for reflexivity. It also explores the spaces of activity and their correlation in war and peace to the spheres of -serious life.- If we are to survive and flourish, different qualities and reciprocal relationships must be cultivated so that we can deal with different fronts of life. Arguing that modern intellectuals and their obsession with space have created a dangerously false choice between mechanical and aesthetic salvation, Rosenstock-Huessy clears a path so that we better appreciate our relationship between past and future in founding and in partitioning time.*
- Eckart Wilkens hat zwischen der Jahrestagung vom November 2016 und unserer Jahrestagung im Juni dieses Jahres eine Vielzahl von Texten Rosenstock-Huessy auf seine bewährte Art und Weise bearbeitet. Hier die Liste von Vorträgen und Vorlesungen:
    - The cross of reality, 1965
    - A faith as old as Adam and Evefour lecture-series
    - A faith as old as Adam and Eve, 1960
    - Before and after Marx, 1954
    - The Bionomics of language, 1960
    - Man must teach, 1959
 Briefwechsel mit Franz Rosenzweig, 1916
 Briefe an Paul Tillich, 1935
 Protestantismus und Seelenführung, 1929
 Die Verklärung der Arbeit, 1927
 A classic and a founder, 1937
 Cabot Lecturesprivilege and future opportunity of the university, 1938
 The Artist and his Community, 1939
 The Religion of the Scientist - A Diagnosis of Education and Redirection of the Mind, 1939
 The Atlantic Revolution, 1940
 An Arthur Stein (Gedicht)
 Letters to Cynthia.
 A voice towards the Third Millenniumseven lectures between 1965-1968\
       1. The University                      (prejective)
       2. The lingo of linguistics            (subjective)
       3. Talk with Franciscans               (trajective)
       4. The cruciform character of history  (objective)
       5. Peace Corps                         (prejective on the level of realization)
       6. Economy of times                    (subjective on the level of realization)
       7. Fashions of atheism                 (trajective on the level of realization)

Die Jahrestagungen seit 2015 dienen der Unterstützung der dritten Phase.
>*Jürgen Müller*


### 3. Die Fragen, die uns das Leben stellt: Die Arbeit der Kreatur

Auf der Jahrestagung 2017 haben wir mit einer Vorstellung des 3.Kapitels begonnen, weil darin die Situation und Fragen der Menschen erörtert werden, denen Arbeit Pflicht, nicht Beruf und nicht Selbstverwirklichung ist. Es ist hochaktuell und kommt in seinen Antworten aus unserer Zukunft.
- Fragen, die uns das Leben heute stellt
 Arbeit der Kreatur - ERH
- S.Junger: „Die moderne Gesellschaft hat die Kunst perfektioniert, dem Einzelnen ein Gefühl vollkommener Nutzlosigkeit zu vermitteln … Worunter er leidet ist das Gefühl nicht gebraucht zu werden. Es ist an der Zeit, dem ein Ende zu setzen.” (4/2017 Tribe)
 1. Die Arbeit der Kreatur sehnt Anerkennung als Kind in der planetarischen Ökonomie
    2. Heute sehnen sich die Arbeitskräfte ihre Lebenszeit und Lebensarbeit als Ganzes zu erfahren
- Vom Beruf zum Job:\
  Ist Work-Life-Balance noch möglich? Ein Nebeneinander?\
  Wolfgang: „Einmal im Jahr bin ich Mensch im Urlaub. Dann lerne ich meine Frau kennen.”
  Rentner Peter, 62J., 2017: „Ich habe Mehrarbeit geleistet um Geld zu verdienen. Jetzt kann ich mir deshalb Freizeit leisten!"
 Arbeit statt Beruf\
    Organisierte geplante Arbeitspflicht macht den ehemals Berufenen zur Funktion, zur berechneten Arbeitskraft. Sie vermag die Seele nicht mehr zu bilden. Die Freiheit eines Christenmenschen wird daher in der maximal individuellen Frei-zeit gesucht außerhalb der Arbeit und nach der Arbeit."
- Realität für viele:\
  2015 60% Arbeitnehmer pendeln 17KM im Durchschnitt\
  On the Jobs! – Keine sichere Lebensplanung möglich\
  Work-Life-Balance scheitert, wenn die Firma zum Zuhause wird und zu Hause nur Arbeit wartet. Die Tribes in Firma und Hobby werden zur seelischen Heimat. Folge: Krise Familie und gleichzeitig: Überforderung von Familie und Ehe.
 Bei Luther sollte der Mann Hausvater werden, als individueller Geistträger (Berufener)
    bildete ihn, seine Seele, was er verantwortlich in lokaler Ökonomie lebenslang tat. Darin bestand seine Freiheit.
    Diese Freiheit ist den meisten verschwunden.
    Wirtschaft ist heute -> Regierung! -> GESETZ"
- Neue Herausforderung Industrie 4.0:\
  Wer wird da noch gebraucht? Wovon werden wir leben, wenn wir nicht /nicht mehr arbeiten materiell und spirituell?
 Frage des Arbeiters:
    Werde ich als Kind anerkannt?\
    Die Lebensarbeit muss erfahren werden."
- Generation What Studie 2016:\
  work life blending (Mischung): Eine neue Einheit von Arbeit und Leben wird heute gesucht. statt work life balance Dabei gilt: Arbeit ist primär Broterwerb und nicht Selbstverwirklichung und Weniger als der Hälfte ist die eigene Arbeit momentan wirklich wichtig
 Heute stehe an leibliche Erlösung; nur mit der ganzen Welt möglich.
    Die Kreatur ist nur bereit dem zu gehorchen, wenn sie als Kreatur anerkannt wird, als Mitglied, dass essen und leben darf und gebraucht wird.
    Nötig: Arbeit als Lebenskunst zu lernen und ermöglicht zu bekommen:
    1) altersgemäße Arbeit denn: Wir arbeiten in verschiedenen Lebensaltern anders und
    2) Lebensarbeit ( Sinn für's ganze Leben)
- Konfirmations/Jugendstudien:
  Nicht das Geld ist das Wichtigste, sondern die Familienfeier, denn die Familie sei das einzige, worauf man vertrauen könne im Leben. Alles andere ändere sich ständig. => Berufung ist nicht mehr in Konfirmation!
 Gemeinschaft und Lebensarbeit:\
    Es geht um die Aufschließung der seelischen Kräfte. Gemeinschaft ist Voraussetzung zu selbständigem seelischen Dasein und Erneuerung der Kraft zur Arbeit. Es gilt die Bildsamkeit zu schützen = die Verwandlungsfähigkeit.
    Ersehnt: Lebensperioden die größer sind als die Arbeitswoche. Nötig sei Heimat für Seele, da der Mensch nicht mehr geistig durch Beruf und nicht mehr körperlich durch den Boden gebildet wird. Die Bedeutung des Urlaubs.
- Ehrenamtssurvey 2015-7 Körber\
  Misstrauen gegenüber religiösen Institutionen und Politik-> trotzdem hohe Engagement Bereitschaft
  NGO- und Hilfswerk-Engagement ist attraktiver
  8 Millionen engagieren sich für Flüchtlinge in D. seit 2015 aber:
  Zukunftsangst der jungen Generation: Fast die Hälfte rechnet damit, dass die Zukunft der eigenen Kinder eher schlechter wird.
 Kirche:\
    statt den Sonn und Feiertagen werden höhere biografische Einschnitte erforderlich: Sabbatjahre, in denen die Lebensarbeit zum Segen wird. / Sonntag der Industriegesellschaft\
    Bsp Bobbies:\
    Du wirst gebraucht! Das gibt Deinem Leben Sinn! (Dienst auf dem Planeten)\
    Junger: "Warum waren alle Mitarbeiter so begeistert? …Sie wollten Teil von etwas Größerem sein. Etwas, das wichtiger war als sie selbst."
>*Thomas Dreessen*


### 4. Luthers Tafel der Werte
Wertewandel ist ein Modewort mit abschüssiger Tendenz: an die Stelle hehrer Ideale scheint der persönliche Vorteil zu treten. Ein Kulturpessimist ist nun Eugen Rosenstock-Huessy gerade nicht. Er nimmt wahr, dass der sichere Boden des Berufs nicht mehr trägt.\
Das habe ich erfahren in Euren freundlichen Ansprachen, in denen ich mal der Jurist, mal der Universitätsdozent, nie jedoch der „Geschäftsführer” sein sollte. Wenigstens irgend etwas Richtiges zu sein wurde mir freundlicherweise unterstellt.\
Wo kommen die Berufe heutzutage vor? Zum Beispiel bei der Kennzeichnung Verdächtiger – Menschen, die im Verdacht einer Straftat stehen, sie sind Arbeitsloser, Gelegenheitsarbeiter, Handwerker, schon seltener „Industriearbeiter”, Lehrer, Kraftfahrer, Arzt, Professor, Pfarrer. Islamist und Talibankämpfer genannt zu werden ist eine vergleichbare, ja sogar stärkere Markierung.\
1927, als Rosenstock-Huessy die „Verklärung der Arbeit” seinem „Alter der Kirche” einfügte – und Joseph Wittig hat da erstaunlicherweise mitgemacht 1927 wurde der Tod eines Mannes unter Nennung seines Berufs in der Zeitung veröffentlicht. Bäckermeister Krause …
Ein Blick in Todesanzeigen für Menschen, die 1927 geboren und 2017 gestorben sind, zeigt ein ganz anderes Bild: Berufe werden allenfalls erwähnt, wenn der Tod während oder kurz nach der „aktiven Zeit” eintrat. Ein Mann stirbt nicht mehr als Schlosser, eine Frau nicht mehr als „Landesmutter”. Lediglich akademische Titel werden mit ins Grab und vielleicht auf den Grabstein mitgenommen, und beim Bischof überragt das Amt sogar den Namen: Bischof Ansgar fast ohne Nennung des Familiennamens.\
Amt und Arbeit, das sind unterschiedliche Sphären. Ein Papst arbeitet nicht, ein evangelischer Bischof mehr und mehr doch. Der will auch Freizeit haben und erwartet öffentliches Desinteresse für sein Privatleben.\
Was also wird auf meiner Todesanzeige stehen? Hoffentlich weder der akademische Titel „ass. iur.” noch die belanglose Tatigkeitsbezeichnung „Geschäftsführer”. Am ehesten werden menschliche Qualitäten Erwähnung finden, womöglich verleiht man mir zukünftig noch Verdienstkreuze. Aber womit diese Orden erdient worden sind, das verschweigt die Annonce.
Mit der Berufsbezeichnung als Erweis ordentlicher Zugehörigkeit zum Gemeinwesen ist es also weitgehend aus. Das hat Rosenstock-Huessy ganz richtig erkannt. Ob er schon geahnt hat, dass heute das Leben soziologisch angeblich nicht mehr 3, sondern 4 Phasen kennt, nach Kindheit mit Ausbildung und Berufstätigkeit kommt vor dem Alter noch die Phase der „Best Ager, 60 plus”, der die Mehrzahl unter uns angehören dürfte, das kann ich nicht sagen.\
Ich habe viel mit Menschen zu tun, die kurz vor oder bald nach Abschluss ihrer „aktiven Zeit” noch etwas Sinnerfüllendes tun möchten. Die dafür verwendeten Ausdrücke wie „Ehrenamt”, „Freiwilligendienst”, „zivilgesellschaftliches Engagement” passen alle nicht so richtig. Wie mögen die Gründer der Rosenstock-Huessy Gesellschaft 1963 gedacht haben? Ich denke, sie waren mehrheitlich ihren Berufsbildern verhaftet und hofften, diese zukunftstauglich zu machen.
Ohne greifbaren, historisch bewährten Beruf dazustehen ist inmitten der Berufswelt gar nicht einfach. „Geschäftsführer” ist ein brauchbarer Strohhalm, weil nicht dazugesagt werden muss, ob dieser Mann auch bezahlt wird oder nicht oder nur halb.\
Ich nannte eben Wittigs Zustimmung erstaunlich. Das war voreilig. Gerade er war ja soeben aus seinem Beruf, aus seinem Wirkungskreis exkommuniziert worden. Und Rosenstocks „Verklärung der Arbeit” ist ja nichts anderes als eine genaue Altersbestimmung der einen Kirche, una sancta. Man kann sagen: Rosenstock-Huessy nimmt gerade bei Luther alles Evangelische aufs Äußerste zurück und bewahrt dessen Anspruch, die eine Kirche zur Besinnung zu führen.\
Der Kirche eröffnet Rosenstock-Huessy einen neuen Zugang zu den Menschen, wenn sie den soziologischen Befund aufnähme und die Menschen, wie man heute gern sagt, dort „abhole”, wo sie desorientiert stünden.\
Mit Luthers ungeprüfter alter Tafel könne man heute nichts mehr anfangen. Die Verschiebung der Freiheitssphären – von der Berufswelt in die Politik und in die Gedankenwelt – helfe nicht. Vielmehr bedarf der vergeblich auf Ausprägung seiner höchstindividuellen Persönlichkeit harrende Mensch der Anerkennung als Glied des arbeitsamen Menschengeschlechts. Woher kommt aber die Kraft, sich selbst als Glied „Wert zu schätzen”? Aus Erfahrungen, wie sie die Kirche zu stiften vermag. Und auch hier liegen das aktiv klingende Stiften und das eher demütig wirkende Anerkennen nahe beieinander.\
Auf dem Würzburger Symposium anlässlich des 90. Geburtstags von Andreas Möckel wurde folgender Beweis erbracht: 1. Andreas betonte, nahezu alles, was er zu lehren versucht habe, von Rosenstock-Huessy gelernt zu haben. 2. Alle Festredner betonten ihre Hochachtung für Möckels Lebensleistung. Freilich sei seine Lehre nicht anschlussfähig. 3. Die heutige Pädagogik, gerade die Heilpädagogik, rühmt sich, den Anschluss verpasst zu haben.\
Das ist traurig, so wie auch unsere geringe Zahl hier heute in Imshausen traurig stimmen kann. Denn welch großen Bogen schlägt Rosenstock-Huessy vom Unbehagen in den „Stellungen” (die von der Freiheit des Berufs kaum noch etwas ahnen lassen) bis zur Zukunft der Kirche! Wer müsste das alles lesen, begreifen, als Auftrag verstehen? Sogar Trumps Triumph in den USA findet ja in diesem kleinen Text Begründungen. In Demut oder wohl eher Angst vor der Wirtschaft kapert jemand vorsichtshalber den Gesetzgeber. Und das überzeugt die wählende Mehrheit.\
Gibt es in den Niederlanden noch Wandergesellen? Junge Männer und Frauen, die sich im Handwerk verdingen, nur mit einem Bündel von Baustelle zu Baustelle ziehen und zwei Jahre nicht ihr Heimat-Haus betreten? Sie leben den Beruf vor. Ihr Wandern ist ihre Sprache, ihre Montur mit dem Hut hebt sie heraus aus all den Passagieren. Sie sind zünftig. Hat es Zukunft? Welcher Handwerker, dem ein schlechtes Werk nachgesagt werden kann, bringt sich – weil er Ruf und Freiheit verspielt hat noch um?\
Noch gibt es in Deutschland eine überschaubare Anzahl von Handwerksberufen. Aber weit über 2.000 verschiedene Studienabschlüsse, die mühsam in die überlieferten Fakultäten eingegliedert werden. Und deren Feingliederung ist von Ort zu Ort verschieden. Ob die Universität Göttingen, die hier in Imshausen zukünftig mitspricht, unsere Tagung als theologisch, soziologisch, philosophisch, historisch, zukunftserforschend, arbeitswissenschaftlich oder vorsichtshalber als außerwissenschaftlich eingruppiert vermag ich nicht vorherzusagen. Ich würde alles durchgehen lassen außer „religionssoziologisch”.\
Rosenstock-Huessy referiert 1927 einen Befund, den ich für meine „Erwerbsbiografie” teile. Was noch nicht da ist ist die Sprache, die Sprache, die das Misstrauen gegenüber der Mission überwindet. Ohne das Buch von Marlouk Alders über Kreisau bereits gesehen zu haben, bin ich gewiss: Sie kann diese hohe Mauer überstrahlen. Sie vermag es, Wim und Lien zu übersetzen und ihnen Frieden zu schenken. Welche gewaltigen Kämpfe hat das gekostet, und wie süss ist der Sieg, den niemand als Person beansprucht!\
Der Gehorsam gegenüber dem Fürsten pflanzt sich fort in der Pflicht, den Souverän wenn nicht zu lieben, so mindestens ins Amt zu berufen. Also zur Wahl zu gehen. Je freier aber das Verhältnis zum Staat sich ausgestaltet, desto lascher die Pflicht zur Teilnahme an seinem Geschick. Was soll denn der einzelne Staat gegenüber der Gehorsamswelt der globalisierten Weltwirtschaft ausrichten können? Wenn in China, so Rosenstock-Huessy 1927, ein Sack Reis umfällt, so geht uns das etwas an. Er hat gesehen, was uns einzusehen schwer fällt.\
Eine Spekulation: wenn Eugen Rosenstock-Huessy so wenig gehört und aufgenommen worden ist, wenn seine Schriften so wenig „anschlussfähig” sind, dann könnte es damit zusammenhängen, dass er die Leser ihrer Berufe, ihres Zutrauens in die Zeitlosigkeit ihrer Stellung, beraubt? Wilmy Verhage ist mein Vorbild für eine Erkenntnisfähigkeit, die erst nach dem Abschied aus der „Berufsrolle” sich eröffnet.

Wo bin ich Berufungen gefolgt?\
1985 meinte ein Kommilitone (ein Katholik aus Österreich): Du machst das. So wurde ich Sprecher der ökumenischen Hochschulgemeinde Speyer. Andreas Möckel und Frans Meijer wollten mich 1986 im Vorstand der Rosenstock-Huessy Gesellschaft haben. 2005 geschah das erneut, als ich zu den Woodbroekers eingeladen wurde. Wim Leenman berief mich 1991 in die Leitung eines workcamps in Kreisau. Im Jahr 2000 war ich für 3 Wochen Ehrenamtlicher im Pavillon der christlichen Kirchen auf der EXPO 2000 Hannover. Wenn also Begründungen für Verdienstorden benötigt werden: am ehesten hier suchen!
Vielen Dank!
>*Andreas Schreck*

### 5. Am Sonntagmorgen

Bei dieser Tagung in Imshausen haben wir auf die gemeinschaftsbildende Kraft des gemeinsamen Zuhörens auf den Text verzichtet – dafür traten die Stimmen der einzelnen Vertreter der Tagungsteile hervor.

Am Sonntagmorgen versammelten wir uns, um über den Neuen Sonntag zu sprechen. Da trat in den Vordergrund der Satz, daß unsre Religion in dem besteht, was für uns im Kalender unabdingbar, wichtig oder nebensächlich ist. Nicht also die Formen, die religiöses Leben geschaffen hat: zur Kirche gehen, Hausandacht, Beichte, Buße und Reue, sondern schärfer gesehen, was nun wirklich wichtig ist: Zukunft, Besitz, Pflicht, Vermögen. Und wie wir uns leicht darüber hinwegtäuschen, daß das – könnte man es in Worte fassen – unsre Religion ist.

Das durch Weihnachten, Ostern, Pfingsten, Geburtstage, Namenstage, weltliche Feste (1. Mai, Helmut-Kohl-Tag), durch Urlaub, Geburt, Konfirmation, Hochzeit, Begräbnis, Höherstufung, Rente, Pension gegliederte Jahr gibt Auskunft, was in diesem Sinne unabdingbar, wichtig, nebensächlich ist.

Der Neue Sonntag nun soll über die Ebene des Jahres, der untersten biographischen Einheit, hinausheben und das Leben der Seele im Zwölfton des Geistes erfahrbar werden lassen. Atem des Geistes könnte uns dazu verhelfen, die siebzig, achtzig, neunzig Jahre unsres Lebens (Victor von Weizsäcker war der Meinung, es sei jedes Leben gleich lang, so daß also nachträglich der Zwölfton des Geistes auf die wirklich gelebte Lebenszeit zu verteilen wäre …) als sinnvolle, atmende Gliederung zu erfahren:

der Neue Sonntag könnte werden die Erneuerung des Protests Israels gegen die Acht, Zehn und Zwölf der Reiche (das chinesische I Ging hat acht Zeichen, das ägyptische Jahr war in zehnmal dreißig Tage eingeteilt, mit dem bekannten Überfluß in den Schalttagen, das Einfügen von Stier/Skorpion und Krebs/Steinbock in die Sternzeichen vervollständigte auf Zwölf),

wenn jeder Arbeitnehmer sechs Jahre arbeitete, dann ein ganzes Jahr freigestellt würde, um die seelische Erneuerung in der Erfahrung neuer Gemeinschaft erfahren zu können (was etwas anderes als Urlaub ist!).

Das bewußte Leben von 14 bis 84 würde sich dann in zehn solche „Jahreswochen” gliedern, faßbar, erinnerbar, instandsetzend für die die Zeit überwindenden Töne des Prophezeiens und Stiftens.

Wie könnte man dafür sorgen, daß also

von 14-21 sich der Name erneuert,\
von 21-28 das Wahrnehmen der Welt deutlich wirklich wird,\
von 28-35 der Dienst am Nächsten in den Vordergrund tritt,\
von 35-42 ein Werk geschaffen wird, das biographisch „stehenbleibt”,\
von 42-49 eine Umwälzung geschieht, bei der der Generationenbruch zur Rede steht,\
von 49-56 man sich kein X mehr für ein U vormachen läßt,\
von 56-63 mit dem Lebenswort vor Kinder und Enkel tritt,\
von 63-70 das Warten auf die Ernte des Lebens lernt,\
von 70-77 eine für die Mitmenschen lebenswichtige Verantwortung übernimmt,\
von 77-84 lehren darf, was das Leben gelehrt hat –

wobei diese Zeiten im konkreten einzelnen Leben variieren mögen, mal kürzer, mal länger. Und die Verschiedenheit der Lebensläufe von Mann und Frau sorgen immer für bunte Fülle von Verwirklichung.

Diese Schau klang nur behende und leicht gestreift an, darf aber doch hier zu denken geben.
>*Eckart Wilkens*

### 6. Kurzbericht von der Mitgliederversammlung am 24. Juni 2017
Ort: Bebra-Imshausen\
Die Mitgliederversammlung genehmigte das Protokoll der Versammlung 2016 und erteilte dem bisherigen Vorstand einstimmig Entlastung. Besonderer Dank ging an Rudolf Kremers, der sich intensiv an der Vorstandsarbeit – verbunden mit Reisen von Lörrach nach Gladbeck, Son und Köln – beteiligt hat, solange es seine Gesundheit zuließ. Der bisherige Vorstand wurde einstimmig wiedergewählt, der bisher von Rudolf Kremers innegehabte Platz des fünften Vorstandsmitglieds blieb unbesetzt. Das ausführliche Protokoll der Sitzung geht wie üblich auf Wunsch den Mitgliedern zu, sobald es genehmigt ist.
>*Andreas Schreck*

### 7. Dank an Rudolf Kremers

Rudolf Kremers war von 2015 bis 2017 Mitglied des Vorstandes der Eugen Rosenstock-Huessy Gesellschaft. Dem zwischen Hans R. Huessy und Rafael Rosenzweig geborenen, wurde Rosenstock-Huessy in einer Krise erneut wichtig. Durch sein Hinzukommen wurde unser Gespräch im Vorstand immer wieder auf die Bedeutung, die Rosenstock-Huessy Krisensituationen für die Gestaltwerdung der Seele gab, gerichtet. Seine offene und interessierte Gesprächshaltung und seine Fragen haben uns zu Antworten und gegenseitigem Zuhören herausgefordert. Für seine Beiträge, auch im Rahmen der Mitgliederbriefe, sind wir ihm sehr dankbar. Durch die Verschlechterung seiner gesundheitlichen Situation mußte er von einer weiteren Mitarbeit im Vorstand leider absehen.
Wir freuen uns jedoch mit ihm und gratulieren zur Fertigstellung seines Buch:
[”Theologie von unten: Querdenker des 20. Jahrhunderts”](https://www.amazon.de/Theologie-von-unten-Querdenker-Jahrhunderts/dp/3743963744) in dem er 5 für ihn wichtig gewordene Personen vorstellt:
- Viktor v. Weizsäcker,
- Paul Schütz,
- Joseph Wittig,
- Eugen Rosenstock-Huessy,
- Walter J. Hollenweger

Diese fünf in diesem Buch vorgestellten Denker verbindet die Wahrnehmung einer grundlegenden Zäsur der abendländischen Geistesgeschichte, äußerlich angezeigt durch die beiden Weltkriege, und die Überzeugung, dass ein Neuanfang »von unten« ausgehen müsse, vom Profanen, vom Menschen in seiner Leiblichkeit her.
>*Jürgen Müller*

### 8. Zum Tod von Karl-Heinz Kämper

Am 25. Juli 2017 verstarb in Bielefeld Karl-Heinz Kämper im 88. Lebensjahr. Er war, seinem Freund Wim Leenman vergleichbar, ein Kirchenmann der Tat. Ein Betheler Gewächs. So wenig ihm frommes Gehabe eigen war, so offen ging er auf Menschen zu und packte an. Für Kreisau bedeutete dies ganz praktische Aufbauhilfe in den Anfangsjahren als Teilnehmer und Mit-Leiter von workcamps. Viele Sommer verbrachte er mit seiner Frau Erika im winzigen Kastenwagen als Wohnmobil auf der Berghaus-Wiese oder in Kaluzas Garten.

Aufgrund gemeinsamer Schulzeit-Erfahrungen am Aufbaugymnasium in Bielefeld-Bethel mit Karl-Johann Rese, Karl Uffmann und Gottfried Hofmann verbunden, läßt sich Karl-Heinz Kämper nur bedingt als „Rosenstock-Huessy-Mann” bezeichnen. Bethels Schulleiter Georg Müller, 1963 Gründer der Eugen Rosenstock-Huessy Gesellschaft, verwies in seinem Unterricht seit Ende der 40er Jahren prägend und für manche gewiss abschreckend intensiv auf Eugen Rosenstock-Huessy (1888-1973). Kämper fragte in Diskussionen stets ziemlich bald nach Erdung und der Umsetzung. Und für tatkräftige Leute konnte er sich schnell begeistern. An gelingenden Beziehungen hatte er besondere Freude, nicht zuletzt als Traupastor für „Neue Kreisauer” nach dem Ruhestand 1990. Bis zum Tode leitete er einen an Rosenstock-Huessy orientierten Gesprächskreis in Bielefeld. Besonders verbunden fühlte sich Karl-Heinz Kämper mit Johannes Rau, den er schon als Kind kennengelernt hatte, und mit Antje Vollmer.

Beruflich ist der Name Karl-Heinz Kämper als Leiter der Jugendstrafanstalt Freistatt im Moor bei Diepholz eng verbunden mit der Abwendung von der „schwarzen Pädagogik” im Anstaltswesen. Sein Credo „Heilsame Offenheit” steht für einen menschenwürdigen, die Fähigkeiten und Entwicklungsmöglichkeiten von „schwierigen Jugendlichen” in den Vordergrund stellenden Erziehungsstil und Leitungsansatz. Bereits 1970 hatte Kämper dem damals neu gewählten Bundespräsidenten Gustav Heinemann neue Ansätze der Fürsorgeerziehung vorgestellt.

Im Trauergottesdienst wagte der Pfarrer die Spekulation, Karl-Heinz würde vielleicht auch im Himmel an der Verbesserung der Zustände mitwirken wollen und mit Engeln eine Arbeitsgruppe bilden.

Persönlich muss ich mich wundern, was ich nach 30 Jahren freundschaftlicher Verbundenheit von Karl-Heinz alles nicht gewusst und erst aus Nachrufen erfahren habe. Ich kann mich nicht erinnern, dass er jemals gesagt hätte: „Ich war viele Jahre Anstaltsleiter und weiß, wie man das macht …” Als Pastor im Ruhestand war er ein großartiges Beispiel für zuhören, dem Neuen Raum geben, ungeprüfte Wege wagen.
>*Andreas Schreck*
>*(verfasst für den Jahresrundbrief 2017 der Kreisau-Initiative e.V. (Berlin))*


### 9. Jahrestagung 2018 vom 23.25. März 2018

Angesichts der aktuellen Rückwendung auf die nationalen Herkünfte wollen wir uns im nächsten Jahr mit dem Thema: „Die Bestimmung des Namens Deutsch nach Eugen Rosenstock-Huessy (1928)” befassen.\
Die Grundlage ist wieder ein 90 Jahre alter Text von Rosenstock-Huessy: „Unser Volksname Deutsch und die Aufhebung des Herzogtums Bayern”.\
Die Tagung, zusammen mit der Jahresversammlung, findet vom 23.-25.3.2018 im Haus am Turm in Essen statt. Herzliche Einladung!

Adresse: Haus am Turm, Am Turm 7, 45239 Essen www.hausamturm.de

### 10. Adressenänderungen

Bitte schreiben sie eine eventuelle Adressenänderung schriftlich oder per E-mail an Thomas Dreessen (s. u.), er führt die Adressenliste. Alle Mitglieder und Korrespondenten, die diesen Brief mit gewöhnlicher Post bekommen, möchten wir bitten, uns soweit vorhanden ihre Email-Adresse mitzuteilen.
>*Thomas Dreessen*

### 11. Hinweis zum Postversand

Der Rundbrief der Eugen Rosenstock-Huessy Gesellschaft wird zukünftig als Postsendung nur noch an Mitglieder verschickt. Nicht-Mitglieder erhalten den Rundbrief gegen Erstattung der Druck- und Versandkosten in Höhe von € 20 p.a. Den Postversand betreut Andreas Schreck. Der Versand per e-Mail bleibt unberührt.
>*Andreas Schreck*

### 12. Jahresbeiträge 2017

Die „Selbstzahler” unter den Mitgliedern werden gebeten, den Jahresbeitrag in Höhe von 40 Euro (Einzelpersonen) auf das Konto Nr. 6430029 bei Sparkasse Bielefeld (BLZ 48050161) zu überweisen.
Nach Einführung des einheitlichen Zahlungssystem SEPA mit der Internationalen Kontonummer (IBAN): DE43 4805 0161 0006 4300 29
SWIFT-BIC: SPBIDE3BXXX ist diese „IBAN”-Nummer auch für Mitglieder in den Niederlanden und im ganzen „SEPA-Land”, in das sich sogar die Schweiz hat einbetten lassen, ohne Zusatzkosten nutzbar.
>*Andreas Schreck*

[zum Seitenbeginn](#top)
