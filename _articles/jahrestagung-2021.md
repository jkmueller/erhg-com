---
title: Jahrestagung 2021
created: 2021
category: jahrestagung
thema: Sprachnot in den Zeiten der Ereignisse - Rosenstock-Huessys  Entdeckung der sozialen Grammatik
---
![Haus am Berg]({{ 'assets/images/Haus_am_Turm_Essen.jpg' | relative_url }}){:.img-right.img-large}

### {{ page.thema }}

Textgrundlage: [„Im Notfall” oder „Die Zeitlichkeit des Geistes” (1963)]({{ '/text-erh-im-notfall-1963' | relative_url }})

Datum: 19.-21. November 2021

Ort: Haus am Turm, Essen

<!--more-->

| Freitag, 12.4 | 18:00 | Eintreffen |
|               | 18:30 | Abendessen |
|               | 19:30 | "Unser Zeitpunkt: Krise – planetarische Notfälle: Unsere Erfahrungen" |
|               | 21:00 | Geselliges Beisammensein |
| Samstag, 13.4 |  8:30 | Frühstück |
|               |  9:30 | Textlesung: „Im Notfall” oder „Die Zeitlichkeit des Geistes” |
|               | 10:15 | Pause |
|               | 10:30 | "Die grammatische Methode: Wann ist was dran?" (Thomas Dreessen) |
|               | 12:15 | Pause |
|               | 12:30 | Mittagessen |
|               | 15:00 | Kaffeepause |
|               | 15:30 | Mitgliederversammlung |
|               | 18:30 | Abendessen |
|               | 20:00 | "Argonautentheater" (Sven Bergmann) |
|               | 21:30 | Geselliges Beisammensein |
| Sonntag, 14.4 |  8:00 | Morgenandacht |
|               |  8:30 | Frühstück |
|               | 10:00 | "Aussprache zur grammatischen Methode" – Plenumsgespräch |
|               | 11:00 | "Welche Aufgaben hat die ERH-Gesellschaft heute" (Jürgen Müller) |
|               | 12:30 | Mittagessen |
|               | 13:00 | Abreise |
