---
title: "Otto Kroesen: Eugen Rosenstock-Huessy – Bereiche seines Denkens"
category: lebensbild
order: 10
---
![Otto Kroesen]({{ 'assets/images/Otto_Kroesen.jpg' | relative_url }}){:.img-right.img-small}


1888 geboren, gehört Rosenstock-Huessy zur Generation, die bei vollem Bewußtsein die Erschütterungen des Ersten Weltkrieges durchlebt hat. Er hat diesen Krieg nicht nur interpretiert als eine Krise der europäischen Nationalstaaten, sondern noch mehr als eine Krise des objektivierenden wissenschaftlichen Denkens.

Die Wissenschaften hatten nicht nur die Natur, sondern auch den Menschen vergegenständlicht. Der Vergegenständlichung auf dem Schlachtfeld entspricht die Objektivierung und Verzerrung lebendiger Menschlichkeit in den Analysen der Wissenschaft. Es ist das Lebenswerk Rosenstock-Huessys gewesen, nicht nur einzelne Methoden innerhalb der Wissenschaften zu ändern, sondern den ganzen Rahmen der Wissenschaften mit einem neuen Paradigma zu versehen.

Die Kluft zwischen Subjekt und Objekt wird überwunden, indem der Mensch wahrgenommen wird als lebend zwischen den Errungenschaften der Vergangenheit und den Herausforderungen der Zukunft. So hat Eugen Rosenstock-Huessy auch selber gelebt. 1933 ist er in die Vereinigten Staaten ausgewandert, einer neuen Zukunft entgegen. Dort hat er seine großen soziologischen Werke geschrieben, die uns Heutigen eine Aus-Lese aus der Vergangenheit gewähren in Blick auf eine zukünftige Geschichte des Menschengeschlechts.

Zwischen 1950 und 1969 ist er immer wieder nach Europa gereist, hat Gastvorlesungen und Seminare gehalten und seine wichtigsten Werke in Deutschland veröffentlicht.1973 ist Eugen Rosenstock-Huessy in den Vereinigten Staaten gestorben.

## Sprache

Zentrales Anliegen Rosenstock-Huessys ist die Sprache. Diese ist nicht nur Ausdrucksmittel in der Kommunikation, sondern führt die Regie über die Kommunikation: „Die Sprache ist weiser als der, der sie spricht”. Wir leben in Wendungen, Interpretationen und Artikulationen vergangener Sprachschichten, und die Sprache erneuert sich in unserem Munde, wenn wir auf die Herausforderungen antworten, die uns in unserer Zeit gestellt sind. Nur wenn das richtige Wort bei unseren heutigen Erfahrungen gefunden ist, öffnet sich eine neue Zukunft, ein Weg, der weiter führt. Sprachlosigkeit heißt Zukunftlosigkeit: Wenn wir das erlösende Wort, den richtigen Namen für unsere Lage noch nicht gefunden haben, sind wir noch nicht aus der Sackgasse und dem Teufelskreis heraus.

## Geschichte

Die geschichtlichen Perioden entsprechen genau den Sprechweisen, rechtlichen Verfassungen, Zeitbogen, worin sich eine Sprache über eine bestimmte Menschengruppe ausdehnt. Seit dem 11. Jahrhundert hat sich zum Beispiel ein Zeitbogen europäischer Revolutionen gebildet, worin Nationalstaaten im Zusammenhang mit universalen Rechtsprinzipien und allgemeingültigen wissenschaftlichen Methoden entstanden sind. In den zwei Weltkriegen hat diese Sprache ihre endgültige Grenze gefunden.

## Soziologie

In seinem historisch-soziologischen Werk verfolgt Rosenstock-Huessy die verschiedenen Zeitbogen und gesellschaftlichen Verfassungen. Er beschreibt die Geschichte als einen Dialog dieser gesellschaftlichen Verfassungen. Jedesmal ererbt und erneuert die jeweilige Gesellschaftsform die vorherige. Als z.B. die Stammesverfassung in eine Sackgasse geraten war, machten die Reiche von Ägypten und Babylon eine neue hierarchische und universellere Gesellschaftsordnung möglich. Als in der hierarchischen Gesellschaftsordnung von Ägypten das Leben und die Gerechtigkeit erstickte, sind die Hebräer herausgezogen und haben zum ersten Mal in der Geschichte eine zukunftsorientierte Gesellschaftsordnung gestiftet. Das Zeitalter der Kirche hat diese Orientierung auf die Zukunft hin ererbt und aus dieser Zukunft zurück eine Brücke gebildet zu den vorherigen Gesellschaftsformen.

## Das dritte Millennium

Als Aufgabe des dritten Millenniums hat Rosenstock-Huessy erkannt, daß der Zerstückelung und Atomisierung und der dazugehörigen Verwirrung aller Sprachformen mit Hilfe lebendiger Sprache entgegengetreten werden muss. Wo das Verständnis unter Menschen keine Selbstverständlichkeit mehr ist, soll die Sprache bewusstdas heißt eben nicht mehr selbstverständlichgeübt werden. Die Sprache des dritten Jahrtausends wird zwar eine Sprache vorübergehender, aber intensiv geübter Gemeinschaften sein. Rosenstock-Huessy hat diese Entwicklung interpretiert als eine Wiederbelebung des Vermächtnisses der Stämme.

## Die Rosenstock-Huessy Gesellschaft

Die Eugen Rosenstock-Huessy Gesellschaft ist darum bemüht die Keime der neuen Sprache, die sich in den Werken von Rosenstock-Huessy äußert, möglichst fruchtbar werden zu lassen. Dazu fördert sie Neuauflagen seiner Werke, veranstaltet Tagungen und Vorträge, verwaltet das Rosenstock-Huessy Archiv, und setzt sich ein für die Verwirklichung dieser neuen Sprache in den der heutigen Industriegesellschaft angemessenen Kommunikationsformen.

Otto Kroesen, im Jahre 2003
