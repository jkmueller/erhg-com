---
title: Mitgliederbrief 2016-12
category: rundbrief
created: 2016-12
summary:
zitat: |
  >Die dritte Jahrtausendordnung der Gesellschaft hat also das Rätsel der Hohen Zeiten und des Alltags zu ergründen. Und zwar ist das Neue gegenüber alten Hochzeiten eine veränderte Zeitmessung. In ein einziges Menschenleben werden heut die Wechsel von drei und vier Generationen alter Zeit hineingepreßt. Diese Vervielfältigung der Lagen macht jede Lage heut zu etwas anderem als früher. In jeden Lebenstunnel, in den wir einfahren, nehmen wir heut das Bewußtsein der Ausfahrt am anderen Ende, also der Kündigung, der Scheidung, des Abschieds mit. Die Königstochter nahm im Bären den Prinzen vorweg. Wir nehmen am Hochzeitstag die Scheidung vorweg. Das liegt daran, daß wir bei jeder Arbeitseinteilung die Entlassung vorwegnehmen müssen. In der modernen Gesellschaft geht der zugrunde, der sich als nicht entlaßbar einrichtet. Wir sind alle demnächst entlassene Menschen. …\
  >Hingerissen werden ins Prinzentum und trotzdem Vorwegnahme meiner Entlassung: das sind die beiden Pole des Lebens in die Zukunft hinein. Zwischen ihnen ringt die neue Zeit. Einer hat um sie vorweg gerungen. Niemand kommt in die neue Zeit, der nicht an ihm vorbeipassiert. Er hat offiziell von 1844—1900 gelebt, …\
  >Das also war Nietzsches Leistung, die Abschaffung irgendeiner Raumeinteilung oder Zeitenspanne als der einzigen. Kurze Angewohnheiten müßten wir schaffen, hat er einmal geschrieben. Das aber ist der Leitfaden in das Labyrinth einer mechanisierten Natur. …\
  >Das dritte Jahrtausend wird das Jahrtausend der Wahlverwandtschaften und der Zwangshasser. Menschenopfer werden es entstellen. Neue Rassen werden es erneuern. Die Weite der menschlichen Gesellschaft und die Intimität des einzelnen Stammes werden miteinander ringen, Marx und Nietzsche werden beide in dem Recht bekommen, in dem sie geliebt haben, und Unrecht in dem, in dem sie gehaßt haben.
  >*Eugen Rosenstock-Huessy, Soziologie – Die Vollzahl der Zeiten, 1958, p.728*
---

## Eugen Rosenstock-Huessy Gesellschaft e.V.

{{ page.zitat }}

**Vorstand/board/bestuur:**\
*Dr. Jürgen Müller (Vorsitzender);*\
*Thomas Dreessen; Andreas Schreck; Dr. Eckart Wilkens; Rudolf Kremers*\
*Antwortadresse: Jürgen Müller: Vermeerstraat 17, 5691 ED Son, Niederlande*\
*Tel: 0(031) 499 32 40 59*

***Brief an die Mitglieder Dezember 2016***

**Inhalt**

{{ page.summary }}


[zum Seitenbeginn](#top)
