---
title: Mitgliederbrief 2014-08
category: rundbrief
created: 2014-08
summary: |
  1. Begrüßung                                                                                     - Eckart Wilkens
  2. Einladung zur Jahrestagung in Imshausen bei Bebra, Reformationstag bis Allerseelen 2014       - Andreas Schreck
  3. Was die Vorstandsmitglieder von der Zukunft der Eugen Rosenstock-Huessy Gesellschaft erwarten
  4. Gottfried Hofmanns Chronik des Lebens Eugen Rosenstock-Huessys                                - Eckart Wilkens
  5. Finanzierung der Übersetzung von „Des Christen Zukunft” im Agenda-Verlag, Münster             - Andreas Schreck
  6. Zum Internet-Auftritt der Eugen Rosenstock-Huessy Gesellschaft                                - Jürgen Müller
  7. Beitragszahlung                                                                               - Andreas Schreck
zitat: |
  >Wer sich entwand der Mikrokosmos-Lüge, daß er allein ein Gleichnis sei der Welt, begreift der schwachen Taube Adlersflüge, die Ihn noch tröstete, den Trost der Welt.
  >*Eugen an Georg Müller, Weihnachten 1955*
---

## Eugen Rosenstock-Huessy Gesellschaft e.V.

{{ page.zitat }}

**Vorstand/board/bestuur:**\
*Dr. Eckart Wilkens (Vorsitzender);*\
*Thomas Dreessen; Andreas Schreck; Dr. Jürgen Müller*\
*Antwortadresse: Dr. Eckart Wilkens: Everhardstraße 77, 50823 Köln, Deutschland*\
*Tel: 0(049) 221 511265*

***Brief an die Mitglieder August 2014***

**Inhalt**

{{ page.summary }}

MITGLIEDERBRIEF AUGUST 2014


### 1. Begrüßung

Wir melden uns wieder! Die Eugen Rosenstock-Huessy Gesellschaft ist noch da – was ist dafür der Grund? Bei der Tagung des Lutherischen Weltbundes in Hannover 1952 erlebte ich, wie die Marktkirche von dem Singen der Choräle aus so vielen Männerstimmen bebte. Und solche Vorstellung von Stärke und Wirkung mag der Rosenstock-Huessy Gesellschaft vorgeschwebt haben. Und nun haben wir uns gefragt, was denn das Minimum des Geistes sei. Die Antwort: Wo zwei oder drei versammelt sind. Das heißt natürlich: das Vereinsminimum von sieben, die dann andere dazuholen.

Da wir beharrlich dieses Antlitzen in kleinen Gruppen geübt haben und dies für uns, die Vorstandsmitglieder, zur essentiellen Erfahrung geworden ist, die weit ausstrahlt, wer weiß wohin, sehen wir keinen Grund, die Rosenstock-Huessy Gesellschaft zu Grabe zu tragen.

Dazu haben uns vor allem die Briefe ermutigt, die Frau Ute Langewellpott und Herr Prof. Wilhelm Matzat geschrieben haben, die nämlich darin stärkten, daß es die Wirkung Eugen Rosenstock-Huessys ist, seines Lebens, seiner Lehre, seines Wirkens, die auf die Länge der Lebenszeit hin den Maßstab für die zu vermeldende Wirkung ist.

Und welch ein Wunder ist es doch, wenn diese Zeit der Seele bei einigen wenigen zueinander öffnet und die Nöte tragen hilft.
>*Eckart Wilkens*


### 2. Einladung zur Jahrestagung in Imshausen bei Bebra, Reformationstag bis Allerseelen 2014

So laden wir, etwas verspätet im Jahr, zu einer Jahrestagung ein, die dem Bedenken des Auferstehungsleibes dienen soll (wie Joseph Wittig es gesagt hat), der Frage, wie wir uns den Himmel vorstellen. Dazu nämlich hat Eugen Rosenstock-Huessy zweimal gepredigt, 1957 und 1962. Den Text dieser Predigten, die auf Englisch gehalten wurden, schicken wir in deutscher Übersetzung von Eckart Wilkens mit. Wir haben sechzehn Personen, nämlich:

I Werner Voggenreiter, II Egbert Schroten, III Wolfram Wehrenbrecht, IV Otto Kroesen, V Thomas Dreessen, VI Feico Houweling, VII Marlouk Alders,
VIII Jürgen Müller, IX Peter Wallrabenstein, X Andreas Schreck, XI Eckart Wilkens, XII Andreas Möckel, XIII Simon Wilkens, XIV Reinhold Ostermann,
XV Gottfried Hofmann, XVI Wilmy Verhage,

zu Paten der sechzehn Abschnitte der ersten Predigt von 1957 erklärt und bitten diese zur Teilnahme, indem sie den einzelnen, ihnen zugedachten Abschnitt bedenken mögen und von dem her, was ihnen da einfällt, erzählen, wo sie erlebt haben, daß das Alte nicht deswegen kostbar war, weil es alt ist, und das Neue nicht deswegen kostbar, weil es neu ist.

Die Beschäftigung mit diesem Text zu Philipper 3, 20-21 soll uns am Freitagabend, am Samstagvormittag und –abend und am Sonntagvormittag bewegen. Samstagnachmittag ist die Jahresmitgliederversammlung.

Weitere Hinweise:

Der Tagungsort, die Stiftung Adam von Trott zu Solz e.V., Imshausen bei Bebra (zwischen Kassel und Fulda in Hessen), ist seit 1938 bis heute durch einen kommunitären Geist geprägt. Einzelheiten zur Anmeldung und zu den Kosten werden demnächst versandt. Wegen der eher einfachen Ausstattung des Visser't Hooft-Hauses wird es auch Möglichkeiten zur Hotelübernachtung im nahen Bebra (8 km) geben, deren Konditionen noch nicht feststehen.

Zur Jahresmitgliederversammlung ergeht in Kürze eine gesonderte Einladung.
>*Andreas Schreck*

### 3. Was die Vorstandsmitglieder von der Zukunft der Eugen Rosenstock-Huessy Gesellschaft erwarten

 - Eckart Wilkens:\
Erst wer statistisch unbedeutend, wird\
im Lichtkranz Lebenszeit antlitzfähig.\
Verminderung ist unvermeidlich, wo\
die echte Freude tiefer dringt ins Mark.\
Geschriebenes, verwahrt, gehegt, schein-ewig,\
treibt die Seele je und je ins Leben.\
Die Zweiten streiten heftig, ob die Lehre\
dem Wirken vorzuziehen sei, da will\
der alte Ton behaupten, er sei besser,\
bis schlicht der Dienst auf dem Planeten\
den Fünfzehnjährigen befördernde Erfahrung wird.\
Juristische Personen werden still.

 - Andreas Schreck:\
Ich erwarte von der Zukunft der Eugen Rosenstock-Huessy Gesellschaft alles. Ihr verdanke ich die Verbindung zu wunderbaren Menschen und die beständige Mahnung wie Chance, die Welt ganz anders zu sehen. Als Jurist ist mir der eingetragene Verein wertvoll. Und zugleich umfaßt die Gesellschaft, wie ich sie empfinde, manche Menschen, die nie dem Verein angehört, ihn verlassen haben oder sogar von ihm ausgeschlossen wurden.

Zwar bin ich nicht über die Gesellschaft zu Eugen Rosenstock-Huessy gekommen, aber kurz nach erstmaligem Hören des Namens (in der DDR 1980) bin ich der Gesellschaft mit all ihren Schrumpfungssorgen und ihrem „alte Männer-Image” ins Netz gegangen. Diese alten Männer waren und sind nämlich sehr interessant – sie haben mir sofort ein Dabei-Sein geschenkt. Die Gesellschaft ist für mich der einzige Resonanzraum für das Sprechen über das, was Eugen Rosenstock-Huessys Worte in mir hervorrufen.

Mittlerweile bin ich selbst zum Zeugen und Erben geworden, wie ich gerade ganz praktisch erlebe, weil ich Auskunft geben soll, welche geistige Mitgift die Eugen Rosenstock-Huessy Gesellschaft Kreisau 1989 gespendet hat.

Ich sehe niemand ohne Anbindung an die Gesellschaft, der mit dem Namen Rosenstock-Huessy ruft und zum Beispiel zu einer Tagung einlädt, wo ich mich „guten Gewissens” hintrauen kann.

Mein Fazit: Die Eugen Rosenstock-Huessy Gesellschaft ist meine Gesprächsbasis für das Gespräch zu Eugen Rosenstock-Huessy. Dieses Gespräch wünsche ich mir weiterhin. Ist das nicht Zukunft genug?

 - Jürgen Müller:\
ERH:
nicht empirische mathematische Modelle = magisches Mittelalter\
Sozialwissenschaft als Argo-Gemeinschaft\
Startups und viele Betriebe sind Argo-Gemeinschaften\
ERHG:\
Seeleute verschiedener Besatzungen treffen sich im Hafen: Seemannsgarn oder neue Wissenschaft?\
ERH:\
von dialektischen Gegensätzen zu vierpoliger Gestalt des Kreuzes der Wirklichkeit\
Sprache als das Kernstück sozialer Interaktion

 - Thomas Dreessen:\
Was ich von der Zukunft der ERH Gesellschaft erwarte.

Warum ich glaube, dass die ERH Gesellschaft in unsere Zukunft gehört!

Wiederholung droht

Lüge zersetzt unsre Zeit
Propaganda erzeugt Mob
Mathematik regiert und
rechnet die Lebendigen
namenlos – dann leibeslos
Lüge zeugt Dämon – Geschwür
am Leib des Menschengeschlechts

Lüge gebärt kalte Kriege
die Mächtigen lernten nicht.
Wir haben keine Sprache
in der wir gegenseitig – Aus 14-45
Lüge zeugt heisse Kriege
Afghanistan! Afrika!
Irak! Ukraine! Gaza!

Zur Lüge verdorben
Menschenrecht und Völkerrecht
im Mund seiner Herolde!
Die Völker fragen sich jetzt
wie sie leben werden, denn
die Dämonen räkeln sich
fressen unsichtbar die Welt

Niemand kommt allein heraus
aus dieser Höllenweltzeit
Am Anfang erklingt das Wort
Ohne Waffen voller Kraft
geglaubt von seinen Sprechern
mir und Dir, geht es hinaus
Trägt Frucht zu Seiner Zeit.



### 4. Gottfried Hofmanns Chronik des Lebens Eugen Rosenstock-Huessys

Im Agenda-Verlag ist die Chronik zu haben, die Gottfried Hofmann in mühseliger und jahrzehntelanger Arbeit aus den im Archiv vorhandenen Schriftstücken erarbeitet hat. Sie ist insofern ein Ereignis für die Eugen Rosenstock-Huessy Gesellschaft, als sie bestätigt, daß die von Georg Müller mit dem Errichten des Archivs angehobene Epoche erfüllt ist. Die bange Frage, wieweit eine Chronik vom Leben, von der Lehre, vom Wirken etwas mitteilen
kann, so daß aus dem Wirken wiederum Lehre und Leben werden (so beschrieb Eugen ja das Verhältnis zwischen Jesus und Paulus), steht auf. Nur nach rückwärts, von der Begeisterung her, ordnen sich bloße Daten sinnvoll.
>*Eckart Wilkens*



### 5. Finanzierung der Übersetzung von „Des Christen Zukunft” im Agenda-Verlag, Münster

Unsere Bitte um Spendenzusagen für den Druck der neuen Übersetzung von mir (Eckart Wilkens) hat bisher einen Betrag von €1.745 erbracht. Herr Dr. Schneeberger ist bereit, die Auflage bei einem Zuschuß von €4.027 zu drucken. Die Differenz von €2.282, so viel ist in der Vereinskasse), wollen wir (Vorstand) aus der Vereinskasse finanzieren. Wir fragen daher die Mitglieder ausdrücklich, ob sie dagegen Einwände haben und bitten gegebenenfalls um Rückmeldung bis zum 22. August. Das Buch, für das ein Verkaufspreis von
€19 kalkuliert ist, würde bei Druckauftrag am 22. August bei der Jahrestagung vorliegen. Korrespondenz in dieser Sache bitte an Thomas Dreessen.

Der aktuelle Kassenstand beträgt € 4.207,72
>*Andreas Schreck, Kassier*


### 6. Zum Internet-Auftritt der Eugen Rosenstock-Huessy Gesellschaft

Die Software, auf der unsere Website basiert (CMS Joomla 1.5), war veraltet, entsprach nicht mehr den aktuellen Sicherheitsstandards und wurde deshalb vom Provider vom Netz genommen. Die Seiten, die momentan unter rosenstock-huessy.com zu sehen sind, bestehen aus dem reinen Text der ursprünglichen Seiten ohne Graphik und ohne die Möglichkeit, etwas daran zu verändern.
Für uns ergibt sich dadurch die Notwendigkeit, das weitere Vorgehen bezüglich der Website neu zu entscheiden. Dies wird zeitnah geschehen. Wir hoffen dann eine Lösung zu haben, die wir die folgenden Jahre nutzen und ausbauen können. Die Website ist für uns ein zentraler Anlaufpunkt mit eigenen Inhalten und Verweisen auf andere relevanten Seiten im Netz.
>*Jürgen Müller*

### 7. Beitragszahlung

Die Mitglieder werden gebeten, den Jahresbeitrag 2014 (üblicherweise € 40) auf das Konto der Eugen Rosenstock-Huessy Gesellschaft e.V.\
Nr. 6430029 bei Sparkasse Bielefeld (BLZ 480 501 61) bzw.
IBAN DE 43 4805 0161 0006 4300 29
SWIFT-BIC: SPBIDE3BXXX
zu überweisen.\
Wichtig: Dieses Konto ist auch für alle Mitglieder außerhalb Deutschlands ohne Zusatzkosten nutzbar (dank SEPA).\
Mitglieder, die eine Einzugsermächtigung erteilt haben, erhalten demnächst ein gesondertes Schreiben, damit auch zukünftig trotz Wegfall des Lastschriftverfahrens der Beitrag über den Kassier eingezogen werden kann.
>*Andreas Schreck*

[zum Seitenbeginn](#top)
