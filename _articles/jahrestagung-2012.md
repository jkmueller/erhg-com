---
title: Jahrestagung 2012
created: 2012
category: jahrestagung
thema: Arbeitslosigkeit
---
![Haus Salem]({{ 'assets/images/haus_salem.jpg' | relative_url }}){:.img-right.img-large}

### {{ page.thema }}

30. März1. April 2012 im Haus Salem, Bielefeld

Was geschieht mit einem Menschen oder einer Gruppe, die vom einen zum anderen Moment entlassen ist von die bisherigen Verpflichtungen? Wenn eine/r nicht mehr mitmachen kann oder darf? Was geschieht in dieser leergewordenen, freigewordenen Zeit? Auf eine eigene Art und Weise ist das unserer Gesellschaft selber passiert, jedenfalls wir als Vorstand haben das so wahrgenommen. Also schien es uns eine gute Idee, die Gestalt der Arbeitslosigkeit in ihrer Fülle (auch der Ruhestand gehört dazu, und abdanken in der Politik, und sogar als Vater und Mutter muss man einmal zurücktreten), und was Rosenstock-Huessy dazu zu sagen hat, zusammen zu erhellen.

Anhand des folgenden Textes:

Friedensbedingungen einer Weltwirtschaft, Eugen Rosenstock-Huessy, Juli 1958.

Arbeitslosigkeit, wie auch immer, gezwungen oder freiwillig, ist sie nicht die Gebärmutter der Erneuerung?

Eingeladen sind alle Mitglieder und Freunde der Gesellschaft sowie an Eugen Rosenstock-Huessy und diesem Thema interessierte Personen. Bei der Anmeldung bekommen Sie den Text geschickt.

Übernachtung und Verpflegung von Freitag, 30. März, bis Sonntag, 1. April 2012
Einzelzimmer: 110€, Doppelzimmer p. P: 95€, Tagesgast: 50 €.

Anmelden können Sie sich bei Wilmy Verhage: E-Mail:WVerhage (at) xs4all.nl
Brief: Wilmy Verhage, Olympiaplein 32 A 1, 1076 AC Amsterdam, Nederland
