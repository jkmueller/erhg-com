---
title: "Peter Glotz über Rosenstock-Huessy"
category: aussage
published: 2022-04-19
name: Glotz
---

„Die bedeutendste geistesgeschichtliche Analyse der europäischen Entwicklung; eine Darstellung der Reformation, der englischen Parlamentsrevolution, der Französischen Revolution, der Revolution der deutschen Großmächte, der russischen Weltrevolution; vor allem aber eine Analyse mit exakt ausgeleuchteten Begriffen (Europa, Abendland, Revolution) und aus dem Fundus einer universellen Bildung. Ein Buch im „pathetischen Sinn der europäischen Kultur“.“
>*Peter Glotz: „Manifest für eine Neue Europäische Linke“, Berlin: Siedler Verlag 1985, S.105. Er die „Europäischen Revolutionen“ als erstes von knapp 30 Werken der Europa-Literatur an.*
