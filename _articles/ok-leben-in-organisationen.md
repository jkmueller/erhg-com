---
title: "Otto Kroesen: «Leben in Organisationen. Ethik, Kommunikation, Spiritualität»"
category: bericht
created: 2013
---
![Otto Kroesen]({{ 'assets/images/Otto_Kroesen.jpg' | relative_url }}){:.img-right.img-small}


***«Leven in Organisaties: ethiek, communicatie, spiritualiteit»*** – Dieses Buch ist veröffentlicht worden, als ich das Studentenpastorat verließ und meine Arbeit an der Universität ausdehnte. Es bringt Texte zusammen, die ich sowohl im Studentenpastorat wie in der Universität geschrieben und gebraucht habe. Es fängt mit der praktischen Frage der Verantwortung an, speziell in der Technik, wie sie in Vorlesungen und Arbeitsgruppen an der Technischen Universität Delft mit zukünftigen Ingenieuren erarbeitet und besprochen wird.

Von daher wird der Kreis weitergezogen, weil ethische Fragen sich eigentlich nur im weiteren Bezugssystem der Kommunikation, der "Sprache" in dem emphatischen Sinne von Rosenstock-Huessy, lösen lassen. Wenn die ethischen Fragen nicht den ganzen dramatischen und grammatischen Kreis von Imperativ (Problem), Konjunktiv (Übereinstimmung suchen), Partizip (tragfähige Gruppen) und Indikativ (konkrete Maßnahmen) durchlaufen, ist es wirklich nicht mehr als individuelles Räsonieren.

Aber nur Kommunikation kann ebenfalls die ethische Frage nicht lösen. Entscheidend ist der Imperativ, der heute an der Tagesordnung ist. Damit tritt der einzelne ein in die Geschichte, wo von Zeit zu Zeit neue Nöte gewendet werden müssen und historische Erfahrungen und zukünftige Herausforderungen zusammen entscheiden, welche Fragen heute unumgänglich auf Lösung drängen. So bedeutet das Wort Spiritualität in diesem Buch Zeitorientierung, biografisch und historisch. Historisch heißt aber auch kulturgeschichtlich: die westlichen Errungenschaften sollen als neueste Erfindungen des Menschengeschlechts Verbindungen mit älteren „geologischen” kulturgeschichtlichen Errungenschaften eingehen.

Wenn ich dies alles auf deutsch schreibe, klingt es mir unerhört wissenschaftlich in den Ohren. Es ist aber ein holländisches Buch, geschrieben in einer, wie ich hoffen darf, nicht flachen, aber doch einfachen Sprache. Es wird heute als Vorbereitung in Vorlesungen und Arbeitsgruppen von Studenten gebraucht, die als Teil ihres Studiums eine Zeit in Entwicklungsländern als Ingenieure in technischen Projekten mitarbeiten.
