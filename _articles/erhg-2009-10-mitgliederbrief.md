---
title: Mitgliederbrief 2009-10
category: rundbrief
created: 2009-10
summary: |
  2. Rudolf Hermeier † - Eckart Wilkens/Andreas Möckel
zitat: |
  >„Indem heut die Älteren den Jüngeren statt gelebten Friedens die bitteren geistigen Erkenntnisse des verfehlten Friedens anbieten, stiften sie einen neuen Bund, ein neues Gefälle,
  >und zwar diesmal zwischen den Generationen,
  >damit wieder das Unmögliche, das nächste Unmögliche, geschehe.”
  >*Eugen Rosenstock-Huessy, Soziologie 1, Seite 259, Ed. 1956*
---

## Eugen Rosenstock-Huessy Gesellschaft e.V.

{{ page.zitat }}

**Vorstand/board/bestuur:**\
*Dr. Eckart Wilkens (Vorsitzender);*\
*Wilmy Verhage; Andreas Schreck; Dr. Jürgen Müller; Thomas Dreessen*\
*Antwortadresse: Dr. Eckart Wilkens: Everhardstraße 77, 50823 Köln, Deutschland*\
*Tel: 0(049) 221 511265*

***Brief an die Mitglieder Oktober 2009***

**Inhalt**

{{ page.summary }}

### 2. RUDOLF HERMEIER †

Mit Psalm 91, 11: *Denn er hat seinen Engeln befohlen über dir, daß sie dich behüten auf allen deinen Wegen* zeigte Inge Hermeier aus Dreieich-Offenthal nach der Beerdigung am 11. August 2009 an, daß ihr Mann Rudolf Hermeier, geboren am 1.12.1929, am 4. August diesen Jahres verstorben ist. Andreas Möckel in meinem ausdrücklichen Auftrag für die Gesellschaft und Gottfried Hofmann waren bei der Beerdigung anwesend. Das Wirken Rudolf Hermeiers in der Eugen Rosenstock-Huessy Gesellschaft ist jedem von uns markant vor Augen, wir werden lange brauchen, um ihm zu danken. Andreas Möckel fand dafür bei der Beerdigung die Worte, die ich hierher setze.

Nachruf für Dr. Rudolf Hermeier am 11. August 2009 von Andreas Möckel:\
*„Ich habe Rudolf Hermeier über die Eugen Rosenstock-Huessy Gesellschaft kennen gelernt. Die erste Begegnung war in Offenthal, Rudolf Hermeier hatte ganz in eigener Regie und ins eigene Haus eingeladen. Später trafen wir uns oft nicht nur auf Tagungen der Gesellschaft. Er war immer sehr bestimmt, er selbst, immer mit einer klaren und entschiedenen Ansicht.\
Ich möchte ein Wort des Dankes sagen, auch im Namen der Freunde um Eugen Rosenstock-Huessys, die es in und außerhalb der Gesellschaft gibt. Zunächst danke ich für seine selbstlose Unermüdlichkeit und Beharrlichkeit. Sie spiegelt ein lebenslanges Ringen, dessen Grund – wie ich meine – in der christlichen Tradition liegt.\
Seiner ersten Publikation stellte er eine Anmerkung von Franz Rosenzweig voran, die – wie ich meine – viel über Rudolf selbst aussagt. „Aber wie wir der Grenzen unseres Wissens zu achten haben, so auch, und nicht minder, der Grenzen unseres Nichtwissens. Jenseits all unsres Wissens wohnt Gott. Aber ehe unser Nichtwissen anfängt, schenkt sich dir, deinem Anruf, deinem Aufstieg, deiner Bereitschaft, deiner Schau, deinem Leben – dein Gott.”* [Franz Rosenzweig: Jehuda Halevi, 95 Hymnen und Gedichte. The Hague 1983, S. 57f]\
*Er war unermüdlich als Sammler und Herausgeber von Schriften Eugen Rosenstock-Huessys und der seiner Freunde; er war unermüdlich im Stiften von Beziehungen, wobei er weite Reisen und finanzielle Opfer auf sich nahm; er war unermüdlich in der Organisation von Tagungen, denen ausgedehnte Briefwechsel mit Referenten und Sponsoren voraus gingen und nachfolgten; er war unermüdlich im Schreiben von Aufsätzen zu Fragen, die ihm keine Ruhe ließen; er war unermüdlich im Verfassen von Zuschriften zu politischen Zeitungsartikeln, von denen er meinte, sie dürften im öffentlichen Interesse nicht einfach hingenommen werden.\
Er ließ sich von großen Namen nicht imponieren.\
Ich will sodann danken für seine Großzügigkeit, mit der er von seinen Entdeckungen andern mitteilte, zum Beispiel seltene Tonbandaufnahmen, die er sich aus Funkhäusern hatte kommen lassen und die er anderen zuschickte. Er behandelte sie wie einen Schatz, der sich durch Weitergabe vermehrt.\
Ich möchte schließlich danken für seinen Mut, mit dem er für eine Sache eintrat. Er scheute Konfrontationen nicht. Man wusste, woran man mit ihm war. Seine Freude an der Arbeit und am Eintreten für seine Sache brauchte keine Anerkennung von außen, sondern kam aus ihm selbst. Daher konnte er freigiebig und immer im Dienst der Sache damit umgehen.\
Beharrlich, großzügig, mutig, offen – so wird er mir in Erinnerung bleiben.”*

Wir haben Gottfried Hofmann gebeten, im Gedenken an Rudolf Hermeier eine Liste aller derer aufzustellen, die jemals in der Rosenstock-Huessy Gesellschaft gewesen sind, um so ein lebendiges Gedächtnis zu errichten, indem wir dann alle mit unserer Erinnerung beitragen, der Gesellschaft mithilfe des Gedenkens eine Zukunft zu öffnen.
>*Eckart Wilkens*

[zum Seitenbeginn](#top)
