---
title: "Heinrich August Winkler über Rosenstock-Huessy"
category: aussage
published: 2021-11-24
name: Winkler
---

* In Band 1 der [Geschichte des Westen](https://www.amazon.de/-/en/Heinrich-August-Winkler/dp/340659235X) schreibt Winkler:\
*Von der „ersten europäischen Revolution” spricht Robert I. Moore im Titel eines 2000 erschienen Buches[^1]. Er steht, ohne sich dessen bewußt zu sein, in einer Tradition, die auf den Universalhistoriker Eugen Rosenstock-Huessy zurückgeht. 1931 veröffentlichte dieser ein Buch über die europäischen Revolutionen[^2]. Als deren erste betrachtete er die 'Papstrevolution' des 11. Jahrhunderts. Der päpstliche Revolutionär war Gregor VII, der 1075 mit seinem 'Dictatus Papae' das Manifest dieser Revolution vorlegte. Gregors Behauptung, daß der Papst den Kaiser absetzen könne, stellte die Praxis der Kaiser auf den Kopf, die mehr als einmal Päpste ein- und abgesetzt hatten.*

[^1]: Robert I. Moore: Die erste europäische Revolution. Gesellschaft und Kultur im Hochmittelalter (engl. Orig.: Oxford 2000) München 2001.
[^2]: Eugen Rosenstock-Huessy: Die europäischen Revolutionen und der Charakter der Nationen (1. Aufl. unter dem Titel: Die europäischen Revolutionen, Volkscharaktere und Staatenbildung, Jena 1931), Stuttgart 1961, S. 131ff

* Neben Wehler nennt auch Winkler Eugen Rosenstock-Huessys Die Europäischen Revolutionen und der Charakter der Nationen in "Ein Buch, das mein Leben verändert hat", Detlef Felken (Hg), 461-463:\
*Das Buch wurde zu einem meiner aufregensten Leseerlebnisse, mehr noch: zu einer Zäsur. Rosenstock, Rechtshistoriker mit dem Schwerpunkt deutsches Mittelalter, aber zugleich Universalhistoriker, Soziologe und Philosoph, lehrte mich, die gesamte europäische Geschichte mit neuen Augen zu sehen und an die deutsche Geschichte neue Fragen zu stellen. Von ihm erfuhr ich, was es sich mit der 'Papstrevolution' Gregors VII. im 11.Jahrhundert auf sich hatte und daß Europa, genauer gesagt: der Okzident, noch immer geprägt war vom Ausgang jenes Ringens zwischen geistlicher und weltlicher Gewalt. Hätte sich damals eine der beiden Seiten durchgesetzt, wäre es um die Sache der Freiheit schlecht bestellt gewesen. In der Spannung zwischen beiden Gewalten lag der Schlüssel zum Verständnis der folgenden Geschichte.  
Und dann das Kapitel über die Reformation in Deutschland. Sie sei in ihrem politischen Teil eine Fürstenrevolution gewesen, lehrte Rosenstock und leitete aus den Ereignissen des 16.Jahrhunderts Folgerungen ab, die das 19. und 20.Jahrhundert besser verstehen halfen: 'Die Fürsten und ihre Professoren, die sind die deutsche Nation geworden durch die Reformation ... Die Religionsparteien sind die ältesten politischen Parteien ... Die Nation, die er (Luther) erweckt hat, wurde zunächst eine Fürsten-, Professoren- und Pfarrernation bis hin zum Professorenparlament der Paulskirche 1848 ... Das 'Unpolitische' des Durchschnittsdeutschen liegt in der freiwilligen Arbeitsteilung zwischen Luther und seinem Landesherren bereits angelegt ... Dankbar ist der Deutsche seither buchgläubig geblieben. Ob Helgels Philosophie, ob Marxens Kapital - das 'Es steht geschrieben' ist für die Deutschen die letzte Instanz geblieben bis zum Weltkrieg ... Es gibt keine Gesellschaft in Deutschland wie in England oder Frankreich, sondern es gibt nur 'die gebildeten Kreise'.'
Souverän, assoziativ, aphoristisch, provokativ und immer wieder blitzartig erhellend: Nur ein wahrhaft universal gebildeter Geist wie Rosenstock konnte die Langzeitwirkungen prägender Erfahrungen so treffsicher auf den Begriff bringen und die 'Dingen hinter den Dingen' sichtbar machen.*

* [Schwemmer, Borgolte zu Winkler und Rosenstock](https://edoc.hu-berlin.de/bitstream/handle/18452/2332/Borgolte.pdf)
