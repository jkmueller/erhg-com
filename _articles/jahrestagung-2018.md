---
title: Jahrestagung 2018
created: 2018
category: jahrestagung
thema: „Wie übersetzen wir den Volksnamen Deutsch in die Sprache des Menschengeschlechts?”
---
![Haus am Berg]({{ 'assets/images/Haus_am_Turm_Essen.jpg' | relative_url }}){:.img-right.img-large}
### Deutsch – Europa – Planet Erde:
### {{ page.thema }}

Textgrundlage:
* Rosenstock-Huessy: WESHALB HEISSEN WIR DEUTSCHE?, 1932 und
* das Gedicht von Rosenstock-Huessy für Georg Müller: „JAKOB GRIMM SPRACHLOS”, 1953

Datum: 23.-25.3.2018

Ort: Haus am Turm, Essen

| Freitag, 23.3 | 18:00 | Eintreffen |
|               | 18:30 | Abendessen |
|               | 19:30 | Einführung in die Tagung (Thomas Dreessen) |
|               | 21:00 | Geselliges Beisammensein |
| Samstag, 24.3 |  8:30 | Frühstück |
|               |  9:30 | Textlesung: Weshalb heissen Wir Deutsche?Erläuterung (Andreas Schreck) |
|               | 11:00 | Pause |
|               | 11:30 | Eigene Erfahrungen – Gesprächsgruppen |
|               | 12:15 | Pause |
|               | 12:30 | Mittagessen |
|               | 15:00 | Kaffeepause |
|               | 15:30 | Mitgliederversammlung |
|               | 18:30 | Abendessen |
|               | 20:00 | Eugen Rosenstock-Huessys Gedicht: Jacob Grimm Sprachlos an Georg Müller, 22. August 1953 (Eckart Wilkens) |
|               | 21:30 | Geselliges Beisammensein |
| Sonntag, 25.3 |  8:00 | Morgenandacht |
|               |  8:30 | Frühstück |
|               | 10:00 | Vorläufer der Zukunft – Plenumsgespräch |
|               | 11:30 | überwindung der Sprachlosigkeit (Jürgen Müller) |
|               | 12:30 | Mittagessen |
|               | 13:00 | Abreise |
