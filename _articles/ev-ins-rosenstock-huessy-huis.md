---
title: "Elias Voet: Im Rosenstock-Huessy-Huis in Haarlem"
created: 2021
category: veroeff-elem
published: 2021-12-18
---
### Stimmstein 2, 1988
#### Elias Voet

»Het verhaal van de Voeten.«

Om het niet te lang te maken enigszins in telegramstijl. 1956: Wim en Lien Leenman trekken bij mij, gescheiden en alleenwonende man, in. Wim gaat leiding geven aan »Evangelie & Industrie« in de regio IJmuiden, dat wil zeggen: in de eerste plaats bij »Hoogovens«. Ik word lid van een van zijn gespreksgroepen. 1957: Pie, zuster van Lien, en ik leren elkaar beter kennen. 1958: Wij durven het aan, wij trouwen. In die tijd leert Wim, via zijn broer Bas, via voetnoot in boek van Dr. Miskotte, de werken van Rosenstock-Huessy en later ook Rosenstock zelf, kennen. Naast de gespreksgroepen ontstaan ook Rosenstock-leesgroepen, waaraan ik ga deelnemen (dat was toen nog een exclusief manlijke aangelegenheid). 1963: Reorganisatie bij »Hoogovens« leidt tot splitsing van mijn afdeling (Oven-bouw) in twee zeer ongelijke delen: Niewbouwdeel (studie, ontwerp, leiding geven aan nieuwbouw van ovens) en Bedrijfsdeel (onderhoud bestaande ovens, inkoop van vuurvaste materialen). Ik kan chef blijven van het nieuwbouwdeel. Pie en ik aarzelen niet lang: ik stel »Hoogovens« voor, voor mij een afvloeiregeling te treffen. Ik ben dan 52 jaar.

1963 ook: Wij verhuizen naar Bloemendaal, waar mijn ouderlijk huis vrijkomt. Eind 1963 eindigt mijn verbintenis met Hoogovens. We beginnen een periode van anderhalf jaar van werken aan huis en tuin en kijken uit naar nieuwe mogelijkheden voor ons beider toekomst. In de loop van 1965 besluiten wij naar Zambia te gaan, waar we »Businessmanager«, respectievelijk »Hostess« kunnen worden van de »Mindolo Ecumenical Foundation«, een vormingscentrum voor mensen uit tal van Afrikaanse landen. Het leven op een campus, het contact met heel veel mensen, het veelzijdige werk de kennismaking met velen van een geheel andere cultuur, de waarde van die cultuur, de arrogantie van de blanke cultuur daar tegenover, maken een diepe indruk op ons. Het meest valt op: Afrikanen generaliseren niet (bij
voorbeeld blank is blank, zwart ist zwart), maar beoordelen mensen individueel:  Wie ben jij? Voor een Europeaan haast onvoorstelbaar. Hoe geweldig, in een land waar zoveel rancune en haat zou hebben kunnen ontstaan, hoe menselijk!

Begin 1968 terug in Nederland. Onze leeskring is er nog. We gaan weer meedoen, maar zeggen al gauw: lezen is prima, maar we moeten ook wat gaan doen. Maar wat? Intussen word ik secretaris van de stichting Oecumenische Hulp, in Utrecht, en doe dat een jaar. Leer het ontwikkelingswerk van de andere kant kennen: Geld werven, projecten selecteren, aandacht vragen voor de nood in de wereld, en de ontwikkelingsproblematiek.

Binnen onze groep ontstaan plannen voor een door een gemeenschap te bewonen huis, met ruimte voor mensen in sociale nood. De laatsten tijdelijk te herbergen, bijvoorbeeld voor een jaar. Dan houden we de mogelijkheid, telkens weer anderen te helpen. Het met elkaar bewonen van een groot huis moet goedkoper zijn dan ieder gezin ergens apart. Een gezamenlijke wasruimte moet goedkoper zijn dan ieder een eigen wasmachine. Samen wonen betekent, om de beurt koken, voor de gemeenschap inkopen, om de beurt afwassen. We krijgen dus meer tijd  (!). Onze verschillende gaven zullen ook meer tot hun recht komen: de een kan goed praten, de ander schrijven, de derde rekenen, de vierde knutselen, de vijfde organiseren: Je zal eens wat zien!

We beginnen dus met groot optimisme, financiëel, technisch, inhoudelijk. Voor ons beiden was nog een opmerkelijke ervaring dat de beslissing om dit te gaan doen uit wel honderd kleine beslissinkjes bestond, zodat we, toen het pijn ging doen (verlaten van een fijn huis met een prachtige tuin) merkten dat we niet meer terug konden! Achteraf: driemaal een sprong in het duister: weggaan bij Hoogovens, gaan werken in Afrika, gaan wonen in Haarlem. Maar achteraf ook: Toen pas is ons leven echt begonnen!

We vroegen Rosenstock het huis naar hem te mogen noemen. Zijn antwoord aan Bas (in de V. S.) was: Dat kan toch niet, zolang ik leef? Zijn antwoord aan ons: Doe het maar, heel goed vier gezinnen, net als de vier Evangelisten, maar: Es kann gar nicht langsam genug gehen.
>*Elias Voet*
