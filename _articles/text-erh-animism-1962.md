---
title: "Rosenstock-Huessy: Animism (1962)"
category: online-text
published: 2022-08-21
org-publ: 1962
language: english
---

**ANIMISM**, from the Latin anima (“soul”), a term originally used to denote the theory of the German chemist Georg Ernst Stahl, who early in the eight­eenth century developed and modified the classical theory which identified the vital principle with the soul. Stahl attributed to the soul the function of ordi­nary animal life in man, while the life of other crea­tures was assigned to mechanical laws. It was applied by Sir Edward Tylor in his Primitive Culture to express the doctrine which attributes a living soul, not merely to human beings, but also to the lower animals, and to inanimate objects and natural phenomena gener­ally. Since the publication of Tylor’s work it has been almost exclusively, used in that sense, though some anthropological writers have employed it more loosely to include the simpler conception that all beings, ani­mate and inanimate, are endowed with personality and conscious life. Many peoples believe or have be­lieved that a human being has more than one soul, among them being certain North American Indians, Melanesians, Negro tribes, Chinese, Hindus, and Egyptians. There are traces of this belief in Homer.

Another belief relative to animism is that the soul can exist apart from the body. Some peoples have  maintained that such souls or spirits haunt the air, the earth, the heavens. As the ethical sense grew with ad­vancing civilization, they began to be differentiated into favorable and hostile, *good and evil*. Many of the former thus developed into gods, the latter into devils. They were regarded as able to hold commerce with the human race, and even to enter into individuals, to inspire them and take entire possession of them. They were equally able to inhabit the lower animals, trees, and other natural objects. In polytheistic religions they are conjured by appropriate ceremonies into idols intended to represent them.

As a philosophical concept, animism ascribes life to nature as a whole. To some also it signifies the idea that all organic development springs from the soul.

Entry in the American People’s Encyclopedia (Grolier Inc., 1962)

“ANIMISM,”8: 201-204. {Unsigned.  Francis Squibb, co-author.} Reel 11, Item 545, Frame 6733.

[„Animism” as part of the PDF-Scan](http://www.erhfund.org/wp-content/uploads/545.pdf)

[zum Seitenbeginn](#top)
