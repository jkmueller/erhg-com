---
title: "Ausgewählte Zitate"
category: online-text2
published: 2022-04-19
order-to: 5
---
![Eugen Rosenstock-Huessy]({{ 'assets/images/erh_chess.jpg' | relative_url }}){:.img-right.img-small}

„Erfahrung zeigt, daß nur drei Wege uns aus unserer Gewohnheit herausreißen: Liebe, Leiden, Gebet. Der Leidende ist der klarste Fall. Daß er leidet, ist schon ein Aus-dem-Gleis-geworfen-Sein. Sobald er ‘ja' sagt und dem Leiden selber nicht entläuft, ist er schon auf dem umgekehrten Weg. Der zweite, der sein tägliches Gleise gern verläßt, ist der Verliebte und Begeisterte. Er will sich vergessen, und er kann sich selbst über der Liebe vergessen. Schließlich der Beter kann sich seines eigenen Willens entäußern. (...) Damit ist die Reihe erschöpft. Genie, d.h. der Liebende, Beter, das heißt der seines Willens Entäußerbare, und die Leidenden sind die dedizierbaren, absprengbaren, verwandelbaren Träger neuer Frage. Sie können sich widmen."  
*Der Atem des Geistes, S. 61*

„. ...; eine Gesellschaft, die es unanständig findet, an den Qualen unserer Seelen Anteil zu nehmen, bürdet dem einzelnen eine Last auf, für die er weitaus zu schwach ist. (...) Neurosen und Nervenzusammenbrüche wuchern in den Vororten, weil es an jener Gemeinsamkeit fehlt, nach der die tieferen Nöte und Leidenschaften schreien. Der Fluch des modernen Menschen ist deshalb der, immer unverbindlicher zu werden aus Angst, sich in irgendeine Sache zu weit einzulassen."  
*Des Christen Zukunft, S. 30*
<!--more-->

„Wir müssen des öfteren mit Leuten zusammenleben, die von uns denken, wir seien Versteinerungen, oder die selbst schon vor langer Zeit gestorben sind und als einzige das nicht bemerkt haben."  
*Des Christen Zukunft, S. 207*

„Die größte Versuchung unserer Zeit ist die Ungeduld in ihrer vollen ursprünglichen Bedeutung: die Weigerung zu dulden, auszuharren und zu leiden. Wir scheinen den Preis nicht länger zahlen zu können, der für das Leben in schöpferischen und tiefgreifenden Verbindungen unter den Menschen nun einmal gezahlt werden muß."  
*Des Christen Zukunft, S. 35*

„Gott ist der, der spricht: ‘Von jetzt an mußt Du mich wieder anderswo erwarten.'"  
*Des Christen Zukunft, S. 121*

„Die Welt hat ein ‘Schicksal '; du nicht."  
*Des Christen Zukunft, S. 90*

„Im Frieden schlafen die politischen Gedanken der Völker. Sich selbst überlassen treiben alte vererbte Vorstellungen fort, ohne auf ihre Tragkraft, ihre Lebensfrische geprüft zu werden. Der Krieg schüttelt den Baum der Träume, und alles Welke fällt ab.”  
*Die europäischen Revolutionen. Volkscharaktere und Staatenbildung, Jena: Eugen Diederichs Verlag 1931, S.III*

„Das mathematische Ideal moderner Logik richtet sich auf das Objekt und nach Objekten. Die wirkliche Sippe desselben Logikers oder Forschers gewinnt keine Wichtigkeit für seine Vorstellung von der Sprache. Wenn dann Herr Fichte als Kriegsfreiwilliger sich beim Kommando „Rechtsum kehrt!“ links herumdreht, erscheint das komisch und der Fachmann wundert sich. Kommt aber ein Hexenmeister, dann macht ihm der Forscher gehorsam Bomben, Kampfflugzeuge, V1- und V2-Waffen. Die Gefühlsspannung des ganzen Gemeinwesens in Kriegszeiten entkräftet des Philosophen Lehre von der Sprache. Er handhabt sie nicht, wie er doch denkt, sondern sie handhabt ihn. Er handelt nach ihrem Siegeswillen und zappelt in einem Sprachnetz, das sein Denken nur ausbeutet.”
*Brief an Cynthia, 19. Oktober 1944, in: Eugen Rosenstock-Huessy, Hitler and Israel, or on Prayer, in: Stahmer, Harold, Preface, in: Rosenstock-Huessy, Eugen (Ed.), Judaism despite Christianity. The 1916 Wartime Correspondence Between Eugen Rosenstock-Huessy & Franz Rosenzweig, Chicago and London: The University of Chicago Press 2011, S.178-194.*

"Ja, das Christentum ist heute bankrott. Aber nicht widerlegt. Das Christentum ist verschiedentlich bankrott gewesen. Wenn es bankrott macht, beginnt es von neuem. Darin liegt seine Kraft."  
*Des Christen Zukunft, S. 112*

„Aber die wahre Kirche bricht die Kette von Ursache und Wirkung."  
*Des Christen Zukunft, S. 175*

„Gehorcht du alten Namen oder neuer Gnadenerfahrung?"  
*Heilkraft und Wahrheit*

"Jedem von uns wird heute abverlangt, daß er Freude an der Rechtgläubigkeit und Mut zum Ketzertum habe."  
*Vortrag über Ketzer*

„Weil die Schultheologie aus Examensstudenten sich aufbaut, kostet sie herzlich wenig. Aber die richtige birgt für den Ermächtigten Gefahr, daß er sich lächerlich mache, seine Reputation und sein täglich Brot verliere; so kann nur der Glaube an Gottes Gegenwart in der neuen Frage jemanden den Mut geben, es in dieser eisig kalten, riskanten Lage auszuhalten."  
*Heilkraft und Wahrheit, S. 57*

"Der Atheist wünschte, ich sollte in die Theologie verschwinden. Die Theologen meinten, ich sei wohl ein Soziologe, die Soziologen murmelten: wahrscheinlich ein Historiker. Die Historiker waren darob entsetzt und riefen: ein Journalist. Aber die Journalisten verdammten mich als Metaphysiker. Die Metaphysiker ihrerseits hielten Wache am Tor der Philosophie und fragten bei den Staatswissenschaftlern meinetwegen an. Die Juristen sind ja schon im Mittelalter als schlechte Christen bekannt gewesen, und so wünschten sie mich in die Hölle. Damit konnte ich mich schliesslich einverstanden erklären, denn als Mitglied der gegenwärtigen Gesellschaft kommt unserseiner aus der Hölle ja nur für Augenblicke heraus."

### Zentrale Aussagen Eugen Rosenstock-Huessys zu den vier Evangelisten

Zu Matthäus:

„Das Evangelium des Matthäus ist eine Abschiedsrede, ein letzter Versuch, Jerusalem davon zu überzeugen, daß sie den Gerechten getötet habe, weil die Juden nicht mehr die Geduld hatten, einen radikalen Wandel in den Methoden zu erwarten, mit denen Gott die Welt regiert.“  
*Die Sprache des Menschengeschlechts,Bd. II, Seite 820 oben*

Zu Markus:

„Markus kniet in Petrus’ Gottesdienst. Petrus ist für ihn die letzte Autorität. Am ende seines Evangeliums erkennt er, dass er, Markus, auf Petrus nicht bauen könne, so wenig wie auf irgendeinen anderen sündigen Menschen. Markus (...) gewann genügend Mut seine Aufgabe als handlanger des Petrus zu überschreiten. (…) Die Kirche konnte nur entstehen, wenn ein Einziger seinen Namen ihrem Leibe gab. Und Markus ging nach Alexandria im Geiste Gottes, nicht in dem des Petrus.“  
*Die Sprache des Menschengeschlechts, Bd. II, Seite 837, 3ter Absatz*

Over Marcus:

“Marcus knielt gehoorzaam voor Petrus. Voor hem heeft Petrus het hoogste gezag. Aan het eind van zijn evangelie weet hij dat hij, Marcus, op Petrus net zo weinig bouwen kan als op welke zondaar ook. Marcus (…) kreeg tenslotte de moed uit te stijgen boven zijn functie van rechterhand van Petrus. (…) De kerk kan alleen dan één zijn, als er slechts Eén is die de naam geeft aan het lichaam van de kerk. Zo ging Marcus in Gods geest naar Alexandrië, niet in Petrus’ geest.”  
*Vrucht der Lippen, pag. 51, 4e alinea.*

Zu Lukas:

"Lukas war der erste Mann, der damit ausgezeichnet wurde, dass er diesen Wandel in der Bedeutung des Geistes in einem Zweiphasenbuch darstellen durfte. In seinen beiden Bänden wird gezeigt, wie der Geist sich über die Geister erhebt. Über den Genius von Christi eigenem Gang durch Judäa und über den Genius in den Handlungen des apostolischen Zeitalters wird so berichtet, dass es sich um die Facetten des einen Geistes handelt. (...) Jeder große Historiker nach Lukas hat eine Mehrheit von Zeitgeistern zugestanden und den Versuch unternommen, den einen Geist durch sie alle hindurch scheinen zu lassen. Thukydides hat das noch nicht gekonnt und Livius noch weniger."  
*Die Sprache des Menschengeschlechts, Bd. II , S. 829, 2ter Absatz*

Zu Johannes:

„Aber Johannes, der keine äußeren Bezeugungen oder Ereignisse nötig hatte, um seinem Freund Glauben zu schenken, gewann den Anhaltspunkt für sein Schreiben aus der Ewigkeit Gottes, der dem Sohn den Sieg über die losen Kreisläufe des Rituals, der Aionen und Revolutionen über Romane und Mysterien gab. Der Abgrund der Zeiten schließt sich bei Johannes. Er beginnt mit dem Schritt, der nur durch die Kraft des Wortes vollbracht wird, mit seinem Indicativus aeternus «Im Anfang war das Wort». Und so wird Jesus als die Freiheit zum Neuanfang ausgewiesen.“  
*Die Sprache des Menschengeschlechts, Bd. II , S. 881*
