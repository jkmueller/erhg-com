---
title: "André Sikojev: Der Stimmstein"
created: 1987
category: veroeff-elem
published: 2023-12-30
---
### Stimmstein 1, 1987
#### Der „Stimmstein” (ein nachgeschriebenes Vorwort)

Als Ossip Mandelstam in St. Petersburg 1913 seinen ersten Gedichtband »Kamen« (»Der Stein«) veröffentlichte, zählte die Revolution von 1905 ihren achten Jahrestag. Die abgedruckten dreiunzwanzig Gedichte markierten nicht nur den Eintritt eines überragenden Dichters in den Sprachstrom des Menschengeschlechts, sondern sie involvierten zugleich ein literar-ästhetisches Programm, den Akmeismus[^1].

Gemeint war damit mehr als eine entschlossene Abgrenzung von jedwedem Symbolismus in der Kunst - wenige Jahre vor der Verschmelzung von Weltkrieg und Revolution sagte Mandelstam derart die Krisis einer erkrankten Welt und die Hoffnung auf einen neuen Schöpfungstag, ein neues Zeitalter an:

> O Himmel, Himmel, träumen wirst du mir!\
  Es darf nicht sein, daß völlig du erblindest, \
  Und auch der Tag - ein weißes Blatt - verbrennt:\
  Zu etwas Asche und ein wenig Rauch.
>*1911*

Im September 1986 baten mich Freunde, Mitglieder einer internationalen Gesellschaft, die den Namen Eugen Rosenstock-Huessys (1888-1973) trägt, die Herausgabe einer Zeitschrift in die Wege zu leiten, die sich - in welcher Gestalt auch immer - den Lehren und dem Wirken dieses Mannes sowie seiner Freunde und Geistesgefährten Franz Rosenzweig, Joseph Wittig und Hans Ehrenberg verpflichtet fühlen sollte.

Nun kann nicht übersehen werden, daß trotz der mancherorts äußerst engagiert vollzogenen Bewahrung ihres Erbes, auch fünfzehn Jahre nach dem Tode Rosenstock-Huessys, er und die Genannten noch immer in Deutschland zu oft überhört und übergangen werden.

Schon deshalb trägt die fast mühelos gewachsene Erstlingsnummer des »Stimmstein« offen bekannten Interimscharakter. In einer Zeit, in der ein verquaster Streit deutscher Historiker sich erneut im Zerrspiegel einer falschen Epochenbestimmung wiederfindet - »nach Hitler« heißt die irritierte Datierung, so als hätte es das Jahr 1917 und seine Zeugen nie Gegeben -, ist Geduld und Bescheidenheit am Platze.

Denn, der Historiker, katholische Priester und große Schriftsteller christlicher Glaubenskraft Joseph Wittig muß noch immer als Vertreter schlesischer Wald- und Wiesenfrömmigkeit herhalten

Uber den Stand ihrer Unkenntnis ist sich, was das Leben und Werk des Philosophen und Arbeiterpfarrers Hans Ehrenberg betrifft, der größte Teil der akademischen Zunft noch nicht einmal bewußt. Die immense Bedeutung Eugen Rosenstock-Huessys für das 20. Jahrhundert ist kaum mehr als vereinzelt erfaßt worden - von einer angemessenen Würdigung kann noch nicht gesprochen werden, denn »er sollte in seinem Vaterland nicht mehr nur von den Menschen gehört werden, deren Aufnahmeorgane auf seine spezifische Wellenlänge ansprechen, sondern endlich von der Nation« (Walter Dirks). Allein Franz Rosenzweig tritt langsam wieder ins Licht neuer Lehrräume, was ganz sicher v. a. ein Verdienst jüdischer Gelehrter ist.

Es soll deshalb die Vision des Apokalyptikers (Johannes Apc. 2, 17) vom weißen Stimmstein[^2], zum Anlaß genommen werden, die folgenden Texte unter einem gemeinsamen Titel zu vereinen.

Dunkle oder helle Steine entschieden über das Schicksal der Menschen im alten Athen (Platon, Apologie XXV f.). Mit Sokrates schon verläßt die Entscheidung über »gut« und »böse« die Kategorien reiner Ethik - Wahrheit wird erlebt (Dostojevskij), und an Heilkraft und Wahrheit wird der Mensch gemessen werden.

Johannes erklärt: die geschichtliche Realisierung der einen Wahrheit beginnt mit der Identität von Person und Namen. Sie markiert die Überwindung jedwedes Götzen, selbst den des Mammon, sie kennzeichnet die Erlösung des leidenden, erniedrigten und hoffenden Menschen.

Von den Töchtern der Völker soll nun in diesem Buch vor allem berichtet werden, denn sie bilden die herausragende Mitte, die thematische Orientierung im Hinblick auf das heranrückende, das anzusagende und zu empfangende 3. Jahrtausend.

Daher kann die Kritik Rosenstock-Huessys am deutschen Idealismus, an der Fiktion der Feuerbachschen »Heiligen Familie« (mit ihrem Platzhalter, dem abstrakten Begriff des »Kindes«), als Erinnerung an die verdrängten bzw. als Ankündigung an die nun auftönenden töchterlichen Stimmen gehört werden (s. »Reflexionen zum Thema Quatember«).

Für die vier hier abgedruckten niederländischen Dichterinnen, von denen drei somit zum ersten Mal auf Deutsch erscheinen, gilt gleichfalls jenes Wort, welches Hans Ehrenberg für Selma Lagerlöf prägte: neben die großen Dichter Rußlands sind die Dichterinnen Europas zu setzen. Denn nur so könne es gelingen, die tödliche Krankheit Europas, die Barbarei, abzuwehren. Er schrieb dies 1929 (!) in seinem Aufsatz »Die großen Russen und unsere Bildungsfrage«.

Auch im Deutschland der Jahre 1933-1945, jener blutigen Zeit, die es nicht zu wenden gelang, waren es Frauen, Töchter, die es den Männern des »Kreisauer Kreises« ermöglichten, zu denken, zu reden und zu planen. Einer dieser Frauen ist deshalb dieser Band gewidmet: Freya von Moltke. Ihren Bericht hörte ich im Januar 1987 in München - als eine Märtyrerakte des 20. Jahrhunderts.

Im Mittelpunkt des Poems (Strophe XVIII) des in Köln lebenden Dichters Eckart Wilkens steht gleichfalls eine Begegnung mit Freya von Moltke. Die Lage der vom Weltkrieg getroffenen Nationen Italien, Deutschland, England, Frankreich und Rußland (Strophe IV-VIII), ihr Erlösung und ihre nunmehrige Verflechtung (Strophe XIX-XXIII) bilden die weiteren Grundachsen des Gedichts. Die Hoffnung Ossip Mandelstams will hier noch einmal erinnert werden.

Jenseits der Revolutionen traten ein neuer Himmel und eine neue Erde zutage. Es liegt an den Bewohnern des nunmehrigen Planeten Erde, ob sie das anbrechende, dieses neue »töchterliche« Zeitalter einzulassen bereit sind, die schmerzvollen Geburtswehen des Jahrhunderts endlich anerkennen und von den Vergewaltigungen des Friedens, des Rechts und der Schöpfung ablassen, die Hölderlins Ahnung vom Geheimnis aller Generationen und Geschlechter immer wieder aufs neue zur Farce zu erniedrigen drohen:

Denn ein Gespräch wir sind \
und hören können voneinander.

Die Freude dieser Erfahrung wünsche ich den Lesern der folgenden Seiten. Mein besonderer Dank gilt allen, die ohne Zaudern und gern die nun enthaltenen Beiträge zur Verfügung stellten, vor allem aber Frau Ko Vos aus den Niederlanden, deren kraftvolle Unterstützung dieses Jahrbuch entscheidend mit hervorgebracht hat.
>*André Sikojev, München, Juli 1987*

[^1]: Akmeismus; Akme (gr.), Höhepunkt, Spitze
[^2]: Stimmstein, gr. Psäphos, zur Abstimmung verwendete weiße und schwarze Steinchen (s. Walter Bauer, Wöterbuch zum NT)
