---
title: "Rosenstock: Vom Staat zum Stamm (1929)"
category: online-text
published: 2021-12-10
org-publ: 1929
---
### Vom Staat zum Stamm

von Eugen Rosenstock

#### 1. Die neuen Nomaden

Der moderne Industrialismus bedingt eine unausgesetzte Völkerwanderung. Vor kurzem stand in der Zeitung, ein Dorf von 1700 Menschen werde vom Braunkohlenbergbau buchstäblich weggefressen. Talsperren zwingen mehr als ganze Ortschaften, gleich einen Bezirk zu räumen. Als ich vor einiger Zeit nach Holstein kam, wurde dort in einem rein landwirtschaftlichen Distrikt ein Werk industrieller Art stillgelegt, das ein einziges Jahr zuvor errichtet worden war.

[ fehlende Seite ]

Dies Denken hat sich seine Apotheose, seine Verklärung geschaffen im Bilde der Antike. Die gesamte Kulur der Neuzeit ist humanistisch. Der humane Mensch, der humanistische Mensch hat sich gespiegelt in den Marmorbildern des antiken Hellas und in den Gesetzen des antiken Rom.

Die Völker Europas alle haben ihre festen Kulturbauten, ihre Verwurzelun in ein staatliches, örtliches und ländliches „Dasein” verklärt durch den Blick auf das klassische Altertum.

Wohin wendet sich nun der moderne Nomade, um sein „delokalisiertes” entortetes Völkerwanderungsdasein zu verklären? Er hat im buchstäblichen Sinne kein Dasein, sondern nur ein Ankommen und Fortgehen. Er ist nicht da, sondern kommt hin und her oder ab und zu. Wie verklärt nun dieser Mensch seine Zukunft?

Eins ist sicher: Hellas und Rom verblassen für ihn. Das humanistische Gymnnasium ist ja nicht zufällig aus dem Inbegriff aller Bildungsanstalt heut ein bescheidenes Fünftel oder Sechstel geworden, ein Typ unter vielen. Damit ist das klassische Ideal preisgegeben. Weder die griechische Kunst noch das römische Recht sind heute noch die Maßstäbe der eigenen Sozialordnung für diese neuen Gesellschaftsschichten. Wie sie die alten humanistisch vorgebildeten Akademikerschichten nicht als ihren „Typ” anerkennen, so sind auch die Ideale, die in diesen Typen gestaltet sind, nicht mehr die Ideale dieser neuen Massen. Hellas und Rom werden noch lange als Vorbehaltsgut der alten gesättigten Gesellschaftschichten lebendige Ideale bleiben. Aber sie sind an ihren sozialen Grenzen gekommen, über die hinaus sich ihre Geltung nicht mehr vorschieben läßt.

Denn die neuen Massen empfinden die Bauten mit den dorischen Säulen und den Renaissancefassaden nicht als Schöpfungen aus ihrer eigenen Persönlichkeit, sondern für sie ist alles gebaute und gefügte Werk ein in die äußere Natur, in den unpersönlichen Kosmos eingegangenes Element. Ein bebauter Raum wird heute das Gebäude, statt eines Ausdrucks des inneren Lebens seines Besitzers. Die neue Sachlichkeit macht aus dem Haus die Wohnmaschine. Hängebrücke und Stadion verlieren sich in die Natur so, als setzten sie nur mit größerer Weisheit die geologische Erdgeschichte fort. Werft, Fabrik, Massensiedlungen, Bahnhöfe, Schiffe, Hotels sind die charakteristischen Bauten des neuen Weltalter. Der einzelne Mensch kommt zu ihnen und verläßt sie als ein Vorübergehender. Ganz ohne Sentimentalität gilt der Satz heut als Gesetz: „Wir haben auf Erden keine bleibende Statt.” Häuser und Bauten stehen da und mögen dauern auf Jahrhunderte. Aber nicht „unsere” Häuser scheinen sie zu sein, sondern sachlichen Aufgaben gewidmet, vom Einzelnen nur vorübergehend betreten und bewohnt.

Das antike Kunstideal aber hatte alles Gebaute und Bebaute vermenschlicht. Die Polis ist ein der Erde abgekämpftes und vermenschlichtes Stück Boden, das sich nunmehr von der „Welt” und „Natur” draußen ein für allemal unterscheidet. „Die Stätte, die ein guter Mensch betrat, bleibt eingeweiht für alle Zeiten” ist das treffenste Humanistenwort.

Die Seele der Kultur zielt gerade auf diese Unterscheidung der zivilisierten Menschenhäuser von dem unbebauten „Raum”. Der Humanismus hat diese antiken Polisideale der Schönheit, der Göttertempel und der Raumbbeseelung auf die staatlich-nationale Kultur der Neuzeit noch einmal übertragen. Die Großstaaten Europas und der neuen Welt haben gewetteifert, das Stadtbild der Antike in alle ihre Bauten hineinzutragen.

Diese Epoche ist vergangen. Und der vorübergehende Mensch, der in den modernen „Erdteilen”, wie wir bezeichnend sagen, in einer von seiner Technik verwandelten Natur sich vorfindet, kann nur noch die ganze Erde als sein Haus empfinden. Die größten Einzelstaaten sind bestenfalls Säle und Stuben in diesem Hause der Erde. Und die steinernen Paläste der Schlösser, Rathäuser, Theater und Postämter sind allerhöchstens kleine Arabesken in der Tapete dieses Riesenerdhauses, das aus Gebirgen und Ebenen, Eisenbahnen und Strömen, Meeren und Wäldern, Bergwerken und Flugzeuglinien sich zusammenbaut.

#### 3. Die neuen Ideale

Die Ideale des „vorübergehenden” Menschcn werden notwendig dort gesucht werden müssen, wo das flüchtige, nomadenhafte Wesen des Menschen sich noch nicht durch eigene Bauten mit der Erde eingelassen hatte, vor der Entstehung dessen, was wir „Staat” nennen, also sowohl des Stadtvolkes der Antike in Athen oder Rom als auch erst recht der Staatsvölker und Nationen des letzten Jahrtausends. Wo liegt also dies Ideal? Bevor das jüdische Volk in Kanaan seßhaft wurde, zogen die zwölf Stämme in der Wüste umher. Bevor ein deutscher Staat sich bildete, wanderten germanische Stämme in das Römische Reich. Und die griechischen Stämme, „froh vereint”, treffen sich in Olympia von alters her, schon lange vor der Glanzzeit der griechifchen Polis. Der Stamm also und seine Lebensform eignet sich als Ideal für den moderenen Nomaden.

Und so ist es denn in der Tat. Um das festzustellen, braucht man sich nur einige allgemein bekannte Vorgänge des Alllags zu vergegenwärtigen.

Jn immer steigendem Maße wird zunächst das Interesse an der Staatengeschichte verdrängt durch die leidenschaftliche Teilnahme für die - Naturvölker.   Der Sozialismus z.B. hat im breitesten Ausmaß die Erkenntnisse der Ethnographie, der Völkerkunde seinem Programm zugrunde gelegt.  Arbeiter führen gern ihre Beweise durch Hinweise auf Naturvölker.  Die Wissenschaft der Soziologie lebt zu einem erheblichen Teil von dem Studium der „Primitiven” und der Naturvölker. Magie, Totemismus, Weiberherrschaft, Mutterecht, Vielehe, Kommunismus, Symbolik, Einweihungsritcn, Zahlenmystik, Totenzauber, diese und tausend ähnliche Fragen interessieren heut Forscher und Laien viel mehr als eine antike Statue oder eine Ciceronianische Rede. Heinrich Schurz' Buch „Altersklassen und Männerbünde” von 1905, eine rein völkerkundliche Untersuchung, hat mit einem Schlag die Jugendpsychologie, die Pädagogik, die Soziologie beeinflußt. Die Jugendbewegung ist kaum ohne das Buch denkbar: oder richtiger: das Buch fällt genau mit dem Aufbruch der Jugend in die neue Menschlichkeitsepoche hinein zusammen. Und das ist kein Zufall, sondern eine tiefe Gesetzmäßigkeit. Schurz, Frazer, Vierkandt, Morgan, Schmidt, Winthuis und alle die andern - sie richen ihre Augen auf eine bestimmte Menschlichkeitsstufe in dem Augenblick, in dem wir selber diesen neuen Spiegelbildes dringend bedürfen.

Die Naturvölker, das sind die Stämme in denen sich die moderne Menschheit wieder finden möchte und wieder finden kann. Wir treten in eine Renaissance der Primitiven, der Naturvölker, des Stammhaften, des Vorantiken und Vorchristlichen ein, ja wir sind schon mitten in ihr. Die Renaissance der Urvölker bricht an. Die Negerplastik war ein Auftakt dazu. Ich will zunächst an einem Einzelzug den grundsätzlichen Unterschied von Staatsbürger und Stammesgenossen aufzeigen. Die Staatsbürger besuchen dieselben öffentlichen Gebäude, Gericht und Kirche, Schule und Privathaus. Der Stammesgenosse aber spricht den gleichen Dialekt und trägt dieselbe Tracht. Deshalb gibt es immer gleichzeitig Staatliches und Stammhaftes an uns. Das Überwiegen der stetigen Kultur etwa im letzten Jahrhundert hat Dialekte und Trachen der alten Stämme zurückgedrängt. Morgen wird es wieder Stämme geben, freilich andere als die der Schlesier und Bayern. Sie werden sich in der Denkart noch mehr scheiden als in der Mundart, im Betragen noch mehr als in der Tracht. All die neuen Verbände, Bünde, Gruppen, Kreise, Vereinigungen, aus denen unser Volk im wesentlichen heut besteht, sind reine Personalstämme, Geistesstämme, die aus der Staatlichkeit plötzlich aufbrechen wollen.

Blicken sie nun in ihrem Werden hinüber auf die Urstämme, so verstehen sie plötzlich den Gebrauch jener Stämme, der uns als „Humanisten” so barbarisch erschien, und erscheinen muß, das Tätowieren.

Tätowieren nämlich muß sich der Mensch, der die ganze Lebensordnung, in der er steht, an sich selber herumtragen muß. Die Tätowierung ist die Tracht des Menschen, wenn sie die Rolle des einzigen sozialen Ausdrucksmittels für Rang und Stand und Zugehörigkeit spielen soll. Überall, wo Tracht und Kostüm alles ausdrücken sollen, was der Mensch in einem Verbande ist, und wo sie allein diese Aufgabe übernehmen müssen, da ist Tätowierung die Folge. Die soldatische Uniform, der Ordensbehang der staatlichcn Hierarchie, der weibliche Kleider- und Juwelenschmuck sind dort auf dem Gipfel, wo der Mensch allein durch seine Tracht die Sozialordnung zu verkörpern hat. Omnia mea mecum porto, ich trage alles an mir selber, kann er dann von sich sagen. In Carlyls unsterblichem „Wiedergenähten Schneider” findet man ja schon viel über diese in den Kleidern, die Leute machen, steckende Weisheit. Die Rolle des „Abzeichen” wird eine ungeheure.

#### 4. Die Veränderung der jüdischen Frage

Staat und Stamm hat es zu allen Zeiten nebeneinander gegeben als Tendenzen menschlicher Sozialbildung. Aber wir wechseln heut aus  einer Epoche des vorwiegend  Staatlich-Nationalen in eine solche des vorwiegend
Stammhaften und Bündischen hinüber.

Unser Geschichtsbild ändert sich dadurch. Wir projizieren uns unsere Nöte in ein anderem Feld der Menschheitsgeschichte hinüber, als die sogenannte Neuzeit von 1500 bis 1900 das getan hat.

An einem großen Zeichen will ich die Verschiebung für den Skeptiker belegen: für die Zeit von 1600-1900 ist der unstete, der rastlose, der „ewige Jude” der Gegenspieler der europäischen Staatlichkeit. Die Legende vom „Ewigen Juden” taucht im Zeitalter der Reformation zuerst auf. Das Mittelalter hatte die Figur nicht gekannt. Juda wurde damals als geborstene, verblendete Synagoge der erleuchteten wohlgebauten Gottesstadt der Kirche gegenübergestellt. Und bekanntlich sind Juden und Christen so als Kirche und Synagoge am Straßburger Münster einander gegenübergestellt.

Unser Zeitalter stellt bereits eine dritte Gleichung auf. Der Gegensatz des „steten Christen” und des „rastlosen Juden” paßt heut im Zeitalter der Weltrastlosigkeit nicht mehr. Vielleicht bekommen ja die Juden sogar einen Staat. Die Argumente werden also umgedreht, denn der Jude muß auch in der werdenden Stammesrenaisaance Grenzfall und Feind bleiben können.

Schon wird der Stamm, die Art, das Blut des Juden gegenübergestellt der Rasse und Art des nordischen, ostischen usw. Menschen. Und fast scheint es, als würde zugleich dem Juden nun gerade das Gegenteil vorgeworfen: nämlich seine Unveränderlichkeit, Stetigkeit, sein starrer „Genotypus” im Gegensatz zu den umgestaltbaren anderen Menschenrassen.

#### 5 Schluß

Sei dem aber wie ihm wolle: die Zukunft wird die Stammeslebensformen zu regenerieren suchen. Bei diesem Versuch werden Narrheit und Weisheit, Gemeinheit und Edelsinn, die Karikatur und die echte Ursprünglichkeit, der Judenhaß und die Gottesfurcht, konkurrieren. Denn wie in allen Zeiten besagt solch eine neue Zielsetzung und Spiegelung noch nichts darüber, ob die einzelne Gruppe oder Lehre dabei richtig oder falsch, steril oder fruchtbar, gut oder böse verfährt. Man hat auch die Antike in großartiger und in lächerlicher Weise nachgeahmt.

Aber das Menschengeschlecht der Zukunft sehnt sich nach Abstammung und Zustammung, wohl weil es von beiden so wenig in dem riesigen Gesellschaftsbau der Gegenwart spürt.

Natürlich geht jede Renaissance einer Vergangenheit unter gänzlich veränderten Voraussetzungen vor sich, als sie in iener Vergangenheit selbst bestanden haben. Die Natur, in der sich der einzelne Irokesenstamm durchschlug. war die unverwandelte, feindliche, vom Menschen unbesigte und deshalb gefürchtete Natur. Der neue Stamm der Adler oder Falken, der Faschisten oder der erwachenden Ungarn kommt zur Welt, weil diese Welt vom Menschen gezähmt, technisch unterworfen und rundherum entdeckt ist.

Dieser Unterschied hebt aber weder die Kraft noch den Sinn der Erneuerung aus dem Urtümlichen auf. Jahrhunderte werden daran daran abmühen. Wir wollen hier nur die Marschrichtung angeben: vom Staat zum Stamm.


[„Vom Staat zum Stamm” als PDF-Scan](http://www.erhfund.org/wp-content/uploads/231.pdf)
