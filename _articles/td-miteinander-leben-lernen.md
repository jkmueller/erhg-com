---
title: "Thomas Dreessen: Zur Epochenaufgabe: „Wir müssen miteinander leben lernen!”"
category: bericht
created: 2019
landingpage: front
order-lp: 60
---

Wir müssen miteinander leben lernen – im Horizont unserer gemeinsamen Heimat,
unseres Planeten Erde. Den Ursprüngen dieser Epochenaufgabe (Freya von Moltke) als Antwort auf die herrschende technische Erneuerung und deren rationalistischen Missbrauch der Sprache geht Dreessen in seinem Aufsatz zunächst nach. Bei William James, Rosenstock-Huessy und dem Kreisauer Kreis findet er Orientierungen für notwendige Erfahrungen in jedes Menschen und jeder menschlichen Gemeinschaft Leben denen Frieden verheißen ist: Gemeinsamer Dienst auf dem Planeten überwindet Grenzen und Trennungen und damit den Krieg.
<!--more-->
![Thomas]({{ 'assets/images/Thomas-Dreessen.jpg' | relative_url }}){:.img-right.img-small}


[zum Artikel beim Institut für Sozialstrategie](https://www.institut-fuer-sozialstrategie.de/2018/07/12/zur-epochenaufgabe-wir-muessen-miteinander-leben-lernen-freya-von-moltke/)
