---
title: Mitteilungen 2023-08
category: rundbrief
created: 2023-08
published: 2023-09-10
summary: |
  1. Einleitung									                                      - Jürgen Müller
  2. Der Kampf zwischen zwei Gesellschaften (4 Teile) 			          - Otto Kroesen
  3. Der Himmel über Berlin wackelt						                        - Sven Bergmann
  4. Eugen Rosenstocks „Kreuz der Wirklichkeit“				                - Sven Bergmann
  5. Nachruf Gerhard Gillhoff							                            - Gottfried Hofmann
  6. Tagungsort und Anmeldung						                              - Thomas Dreessen
  7. Programm der Jahrestagung						                            - Jürgen Müller
  8. Einladung zur ordentlichen Mitgliederversammlung			            - Jürgen Müller
  9. Adressenänderungen 							                                - Thomas Dreessen
  10. Hinweis zum Postversand						                              - Thomas Dreessen 
zitat: |
  > „Das Bild vom Baum der Menschheit ist sehr real gemeint. Das weltgeschichtliche Novum ist eingetreten, daß wir Eine Menschheit geworden sind, wenn auch die einzelnen Menschen noch in nationaler oder rassischer Selbstüberhebung verharren mögen. Mit dieser neuen Vollzähligkeit aber treten die Staaten als Strukturelemente zurück und die übergreifenden Kräfte des Gesellschaftlichen lenken unser Schicksal. Die Gesellschaft, die jene neue Ökonomik leisten soll, muß auch hinauskommen über die bloßen Begriffe von Ost und West, von Sowjetsystem und Kapitalismus. Entscheidend ist die Funktion, die in der zeitlichen Aufeinanderfolge der Geschlechter von den Lebenden geleistet werden muß. Der Name dieser Funktion spielt keine Rolle, sie ist nicht gebunden an Programme, die doch nur vorübergehender Natur sind. \
  Wir müssen die Funktion der europäischen Gesellschaft unter verschiedenen Namen wiedererkennen. Wovon ich spreche, ist selbstverständlich Mission, aber ich würde mich hüten, es so zu nennen. In der kommenden Gesellschaft werden gewisse Formen unter ganz neuen Namen sich durchsetzen; aber sie werden das Ewige darstellen, das die Mönche dem Bauern zugeführt haben, der Künstler dem Handwerker und was im Zeitalter der Technik die großen Naturwissenschaftler und Erfinder dem Arbeiter zugeführt haben. \
  Heute, wo es auf die Zusammenordnung aller arbeitenden Kräfte ankommt, wird man hierzu wieder eine Schar, eine Gruppe von Menschen heranbilden, die maßgebend sind für die Art; wie man geduldig, langsam, unauffällig und ohne sichtbaren Gewinn tätig ist. \
  Wenn also heute der Großraum der Erdteile unseres Planeten so bewirtschaftet werden soll, wie es der Schöpfer des Bodens und seiner Schätze verlangt, dann müssen Industrie, Handwerk, Bauernsame ihre drei Gaben zusammenwerfen:
  - die verläßliche Hingabe des unüberwachten freien Bauern, 
  - die zuverlässige Kunstfertigkeit eines Teams von Genossen, 
  - der Zusammenhalt der Antipoden in den weltweit gespannten Unternehmen.”
  > *Eugen Rosenstock-Huessy, Dienst auf dem Planeten, 1965*
---

## Eugen Rosenstock-Huessy Gesellschaft e.V.

{{ page.zitat }}

**Vorstand/board/bestuur:** *Dr. Jürgen Müller (Vorsitzender);\
Thomas Dreessen; Sven Bergmann; Dr. Otto Kroesen\
Antwortadresse: Jürgen Müller, Vermeerstraat 17, 5691 ED Son, Niederlande,\
Tel: 0(031) 499 32 40 59*

## Mitteilungen August 2023
Inhalt

{{ page.summary }}

### 1. Einleitung

Liebe Mitglieder, liebe Freunde,

hier erhalten sie die „Mitteilungen der Eugen Rosenstock-Huessy Gesellschaft”. Da der Leserkreis weit über die Zahl der Mitglieder hinausgeht und der Inhalt nur zu einem kleinen Teil gesellschaftsinterne Punkte enthält, hat sich der Vorstand zu dieser Namensänderung entschloßen.\
Otto Kroesen beschreibt den Weg wie sich aus Ostkirche und Westkirche zwei Gesellschaften entwickelt haben und wie in beiden eigene Ansätze, die im Hören auf die Sprache entwickelt wurden, Möglichkeiten zum Frieden aufzeigen. Wie eine Geschichtsvergessenheit leicht zu Oberflächlichkeit und Dekadenz führen kann, beschreibt Sven Bergmann in einem aktuellen Kommentar. In einem weiteren Beitrag beschreibt er Kontext und Ziel des Kreuzes der Wirklichkeit Rosenstock-Huessys.\
Gottfried Hofmann gedenkt unserem Mitglied Gerhard Gillhoff.\
Zu unserer Jahrestagung unter dem Thema: „Natürlich, künstlich oder unbezahlbar? Wie wollen wir arbeiten und leben?” von Donnerstag 5. bis Samstag 7. Oktober 2023 in Marbach möchte ich Sie ganz herzlich einladen. Der Beginn schon am Donnerstagabend gibt uns die Gelegenheit am Freitag für eine Führung im Literaturarchiv. Der 50-ste Todestag Eugen Rosenstock-Huessys, der 45-ste Todestag Georg Müllers und das 60-jährige Bestehen unserer Gesellschaft in diesem Jahr sind gute Gelegenheiten zu einer Bestandsaufnahme und einem Ausblick der Arbeit mit dem Erbe Eugen Rosenstock-Huessys.
>*Jürgen Müller*

### 2. Der Kampf zwischen zwei Gesellschaften (1)

#### Die Kuppel und das Schiff

In jedem Krieg, auch in dem in der Ukraine, geht es um Macht. Aber es geht nie um pure Macht allein. Es prallen zwei Gesellschaften aufeinander, zwei Lebensweisen, oder anders gesagt, ein unterschiedliches Verständnis der geistigen Mächte, die unsere Existenz lenken. Diesen Unterschied im Wertesystem (säkular gesprochen) oder in den geistigen Mächten (religiös gesprochen) versuche ich in einigen Beiträgen [^1] ein wenig zu schärfen. Dabei hoffe ich, dass wir den anderen und damit uns selbst besser kennen lernen.

#### Die Architektur der alten Kirche

Die Architektur verrät oft einen Unterschied in der geistigen Wahrnehmung der Wirklichkeit. Im alten Christentum gab es vor allem Kuppelkirchen, wie die Ayah Sophia in Konstantinopel. Unter dem offenen Himmel sind die Menschen in der Kirche versammelt. Oft sehen wir den verherrlichten Christus von der Decke herabblicken. Er hat alle Macht im Himmel und auf Erden. Und er zieht alles zu sich heran (Johannes 12,32). Auf den Himmel sollte man also seine Augen gerichtet halten.
![Ayah Sophia]({{ 'assets/images/ayah_sophia.jpg' | relative_url }}){:.img-left.img-large}
In der alten Kirche konnte man nicht wirklich erwarten, die Welt dramatisch zu verändern. Aber man konnte selbst anders leben, und zwar als Gemeinde, mit mehr Verantwortung und Fürsorge füreinander. Und das taten die Menschen auch. In der rauen Wirklichkeit war dies schwer genug, darum wurde es wenigstens in den Klöstern gemacht. Und auch dort blieb es schwierig. Man konnte seine Seele vor dieser Welt bewahren und retten [^2]. Das Wort Seele ist etwas aus unserem Sprachgebrauch verschwunden, aber es findet in unserer Zeit noch eine Entsprechung in der Formulierung "persönliche Integrität".

Das bedeutete aber auch, nicht zu versuchen, das öffentliche Leben durch direkte Intervention zu verändern. Vor allem im Oströmischen Reich lehnte sich die Kirche kontinuierlich an die Macht des Kaisers in Konstantinopel an und gab sich mit ihr zufrieden. Seit der Eroberung Konstantinopels im Jahr 1453 versteht sich die Russisch-Orthodoxe Kirche als Erbe ihrer Tradition und Lebenseinstellung. Für die persönliche Integrität mussten sich die Menschen auf Gottes Gnade verlassen, und für die irdische Gerechtigkeit mussten sie hoffen, dass auch der Kaiser unter den Einfluss dieser Gnade geriet [^3].

#### Die Architektur der Moschee

Übrigens hatte diese Lebenseinstellung schon im Oströmischen Reich zu großer sozialer Gleichgültigkeit geführt. Zwar zeichnete sich das Oströmische Reich nach Konstantin dem Großen durch ein viel humaneres und würdigeres Regime aus als das alte Römische Reich, doch als vor allem im fünften und sechsten Jahrhundert die Großgrundbesitzer immer mehr Macht gewannen und sich vor allem in Konstantinopel um ihre Interessen kümmerten, während in den Dörfern die Menschen zu Grunde gingen, gab es eine Reaktion der arabischen Stämme: Alle Menschen, auch die Großgrundbesitzer, mussten sich nun fünfmal am Tag im Staub verbeugen, und man konnte das Erbe nicht mehr ungeteilt weitergeben [^4].

Dieser Angriff des Islams auf ungleiche Machtverhältnisse findet seinen Ausdruck auch wieder in der Architektur. Die christlichen Kuppelkirchen sind nun so gebaut, dass gleichsam das Gebäude und die ganze Erde an den Säulen hängt, vom Himmel hängt, mit Minaretten wie Drähten nach oben. Die Allmacht Gottes verschlingt die irdische Wirklichkeit und bringt Stämme und Menschen zur Einheit und Gleichheit.

#### Die Architektur des Westens

![Neue Kirche Delft]({{ 'assets/images/neue_kirche_delft.jpg' | relative_url }}){:.img-right.img-large}
Der Westen kämpfte mit denselben Problemen, aber nach der Eroberung Roms durch Alarich im Jahr 410 hatte bereits eine etwas andere Orientierung eingesetzt. Die westliche Kirche konnte sich nicht mehr auf einen Kaiser stützen wie die östliche, und Augustinus schrieb nach der Eroberung Roms sein Buch De Civitate Dei. Darin vertritt er die Ansicht, dass die Eroberung Roms keine wirkliche Katastrophe ist, da das Ziel der Kirche darin bestehen sollte, die Stadt Gottes auf die Erde zu bringen, und das kann auch ohne Rom geschehen.

In den harten Jahrhunderten nach Karl dem Großen ging es unter der Herrschaft der Großgrundbesitzern ("Raubrittern") im Westen noch schlechter als im Osten. Im Jahr 1076 erklärte Papst Gregor VII., dass er über dem Kaiser stehe, was zu einem gewaltigen Machtkampf zwischen Anhängern des Papstes und Anhängern des Kaisers führte. Dabei handelt es sich jedoch nicht nur um einen politischen Konflikt, sondern auch um einen sozialen Konflikt, der die gesamte Gesellschaft betrifft. Viele Klöster und Großgrundbesitzer steckten unter einer Decke, und viele aufrichtige Mönche und einfache Menschen widersetzten sich dem mit aller Kraft [^5]. In diesem Konflikt entstand Raum für Zünfte und Bruderschaften, Organisationen von unten. Zünfte und Bruderschaften gab es schon früher, aber nun beruhten sie nicht mehr auf familiären Beziehungen, standen nicht mehr unter der Kontrolle des Kaisers und darüber hinaus garantierte das Körperschaftsrecht nun ihren Fortbestand über Generationen hinweg. In ihnen konnten die Menschen mehr oder weniger frei ihren Geschäften nachgehen, weil die päpstliche Partei sie unterstützte, so dass der Kaiser sie nicht wirklich aufhalten konnte [^6].

![Kirche in Moskau]({{ 'assets/images/kirche_moskau.jpg' | relative_url }}){:.img-left.img-large}
Damit entstand auch im Westen eine neue Art von Kirchenbau: Wer zum Beispiel die Nieuwe Kerk in Delft betritt (wohlgemerkt, es war ursprünglich eine katholische Kirche), sieht nicht eine Kuppel über sich, sondern eine Straße vor sich. Dieser Weg führt übrigens nach Osten, nach Jerusalem. Dieser Weg besteht aus Etappen. Man geht weiter und weiter in das Kirchenschiff hinein. Man geht auch immer weiter mit seinem Blick nach oben. Dort gibt es Zwischenstationen, einen Kranz, einen Querbalken, aber dann wird der Blick wieder weiter nach oben oder nach vorne gezogen. Die westliche Kirche ist auch in ihrem Kirchenbau nicht mehr nur auf den Himmel ausgerichtet, sondern auch auf eine schrittweise Umgestaltung der Erde.

Die orthodoxe Ostkirche hat dies immer als Häresie angesehen. Deshalb ist die orthodoxe Ostkirche auch "orthodox". Die östlich-orthodoxe Kirche behauptet, dass man als Kirche die Erde nicht erlösen kann, die politische Kontrolle über sie bleibt bei den weltlichen Mächten. [^3] Im Gegensatz zur Moschee symbolisiert der ostorthodoxe Kirchenbau eine Dichotomie: Die Kuppel bzw. die Kuppeln (es gibt mehrere Patriarchate) erheben sich über die Erde und sind deutlich vom irdischen Unterbau getrennt. Die östliche orthodoxe Kirche erlebt die westliche Neuerung nicht. Sie hat starke Einwände gegen sie. Es ist klar, dass bereits vor 1.000 Jahren die Weichen für eine ganz andere Gesellschaft gestellt wurden.

[^1]: Sie werden hier zur gleichen Zeit veröffentlicht, statt nacheinander wie auf der Website einer Kirchengemeinde in Delft. Sie wurden ebenfalls leicht bearbeitet und mit Fußnoten versehen.
[^2]: Noble, D.F., 1997. Die Religion der Technik. New York: Penguin Books S. 11.
[^3]: In Ehrenbergs Band I, Östliches Christentum, von 1923 weist Aksakow auf die Rollenverteilung zwischen Kaiser und Volk hin. Das Volk akzeptiert die Allmacht des Staates, lebt aber sein eigenes Leben abseits davon im weiten Land. Der Staat ist nicht mehr als äußerer Schutz, K.S. Aksakow, Ausgewählte Schriften, in Hans Ehrenberg, 1923. Östliches Christentum, Oskar Beck München 88, ff.
[^4]: Armstrong weist auf diese Bedeutung des fünfmaligen islamischen Gebets jedem Tag hin, Armstrong K., 1995. A History of God - 4000 years of Judaism, Christianity and Islam, Ambo, Amsterdam (Orig. 1993). Kuran weist darauf hin, dass nach dem islamischen Erbrecht nicht mehr als ein Drittel des Landes in der Familie zusammenbleiben darf in der Vater - Sohn Linie; der Rest wird aufgeteilt. Dies ist eine Maßnahme gegen den Großgrundbesitz im Oströmischen Reich, siehe Kuran, T., 2011. The Long divergence: how Islamic Law held back the Middle East. Princeton University Press.
[^5]: Siehe Moore, R.I., 2000. Die erste europäische Revolution, 970-1215, Blackwell Publishing, Oxford.
[^6]: Siehe Rosenstock-Huessy, E., 1989. Die Europäischen Revolutionen und der Charakter der Nationen, Moers, Brendow (Orig. 1931).
>*Otto Kroesen*

### Der Kampf zwischen zwei Gesellschaften (2)

#### Regeln oder mit Liebe durchdringen

Der Westen mit der katholischen Kirche und Russland mit der orthodoxen Kirche wuchsen auseinander. Im Oströmischen Reich blieb das Kaisertum bestehen. Politisch war die Kirche immer von ihm abhängig. Das Weströmische Reich ging mit dem Fall Roms im Jahr 476 unter, aber auch mit der Invasion vieler Stämme, die sich innerhalb seiner Grenzen niederließen. Die Entwicklung im Westen verlief infolgedessen wesentlich chaotischer. Dies veranlasste die Kirche, sich stärker als im Osten in die politischen Beziehungen einzumischen und die irdischen Verhältnisse zu regeln.

#### Diese Welt soll sich ändern

Karl der Große arbeitete bei seinen Eroberungen im achten Jahrhundert zwar mit dem Papst zusammen, aber dem Papst wurde dabei eine ihm untergeordnete Stellung zugewiesen. Für Karl war es wichtig, dass er nicht nur als Vertreter eines Stammes, der Franken, angesehen wurde. Wenn er als christlicher Kaiser mit dem Papst an seiner Seite angesehen wurde, konnte er seine Autorität mit viel mehr Überzeugung auch gegenüber den anderen Stämmen geltend machen [^7].

Nach seinem Tod begannen die Großgrundbesitzer, auf ihren Burgen und in ihren Klöstern das Gleiche zu tun. Sie unterstellten die Priester und Mönche ihren Interessen [^8]. Mit Mühe hielten die Nachfolger Karls des Großen die Dinge einigermaßen zusammen. Im 11. Jahrhundert versuchte der deutsche Kaiser Heinrich III., die Einheit zu fördern, indem er einen glaubwürdigen Papst in Rom einsetzte. Zu dieser Zeit waren die Päpste selbst kaum mehr als lokale Herrscher von Rom. Dieser glaubwürdige Papst wurde schließlich Gregor VII., und er begann gerade, die Abhängigkeit der Kirche in Frage zu stellen.

Im Jahr 1076 brach er aus: der so genannte Investiturstreit, der Streit um die Ernennung der Bischöfe. Dieser Kampf wurde auf allen Ebenen der Gesellschaft ausgetragen. Selbst auf lokaler Ebene wollten die Menschen der Machtgier der Raubritter ein Ende setzen. Es war die Kirche, die dafür kämpfte. Zum ersten Mal griff die Kirche weltverändernd ein [^9]!

#### Regeln und Geistlichen

Die Kirche versuchte vor allem, die politische Praxis zu vereinheitlichen: Gesetze mussten schriftlich festgehalten werden und durften nicht willkürlich sein. Überall sollten möglichst die gleichen Gesetze gelten, und die Kirche selbst begann damit, indem sie sieben Sakramente einführte. Zwischen der Partei des Papstes und der Partei des Kaisers gab es einen ständigen Kampf um die gegenseitigen Befugnisse und Beziehungen.

In diesem Machtkampf gab es Raum für die Städte und Zünfte, sich von unten zu organisieren. Dabei wurden sie vom Papst und von den Bettelorden unterstützt. Bürger und oft auch entlaufene Leibeigene beteiligten sich daran. Die Geistlichen waren nun keine Heiligen mehr, die von der Eingebung des Augenblicks abhängig waren, sondern sie wurden zu einem Berufsstand, der studiert hatte, sei es in Jura oder in Theologie [^10]. Eingebungen wurden ersetzt durch allgemeine Regeln.

Man muss auch wissen, dass alle weltlichen Herrscher, Könige und Kaiser, damals anfänglich nur militärische Führer waren. Alles andere wurde von der Kirche geregelt: Gesundheitsfürsorge, Bildungseinrichtungen, sogar technische Neuerungen in der Landwirtschaft und dergleichen.
Der Vorwurf der Orthodoxie

Die östliche Orthodoxie hat der westlichen Kirche immer Säkularisierung vorgeworfen. Die Kirche war mit der Regulierung der Gesellschaft befasst, und in der Tat ging es im Glauben wie auch in der Gesellschaft um Regeln, Dogmen und Gesetzen. Die Kirche ist dazu da, Gottes Liebe zu den unglücklichen Menschen zu verkünden. In den Heiligen kommt diese Liebe zum Ausdruck. Die Sünder werden durch diese Liebe gerettet. So sollte es sein [^11].

Umgekehrt kam aus dem Westen die Kritik, dass die orthodoxe Ostkirche gegenüber der Regierung kritischer sein könnte. Die orthodoxe Ostkirche hat gegenüber der politischen Macht immer eine passive Haltung eingenommen. Das entscheidende Argument war oft, dass jeder Sünder weiterhin den Zugang zu Gott finden können sollte [^11].

Was die Kirche damals im Westen begonnen hat, hat in der Arbeit säkularer Organisationen eine Fortsetzung gefunden: die Organisation der Gesellschaft durch Regeln und Gesetze und Argumentationen (Philosophie, Wissenschaft), die einen "zwingenden" Charakter haben. Auch das kennen wir: Für alles gibt es eine Regelung. Und wenn die Regeln selbst aus dem Ruder laufen, wie bei der (niederländischen) Beihilfenaffäre, kommt eine andere Regelung, um die Lücken zu füllen [^12]. Dann ist der Kampf gegen die Armut eher eine Kopf- als eine Herzenssache. Das ist der Vorwurf aus dem Osten: Der Westen hat zu viel Kopf und zu wenig Herz.

#### Die russische Gesellschaft

Aber dann die russische Gesellschaft: Die Kirche ist passiv, und die Regierung hatte nur eine militärische Rolle, als Schutz gegen Feinde. Zar Peter der Große erkannte, dass der Westen bei der gesellschaftlichen Organisation eine Vorreiterrolle übernommen hatte. Er versuchte, dort zu lernen, um Russland zu entwickeln. Aber das scheiterte immer. Die Gesellschaft blieb eine Angelegenheit von Großgrundbesitzern und armen Landarbeitern. Russland hatte fast keine Städte. St. Petersburg war auf den Westen ausgerichtet, und das war vor allem gut für die Einnahmen durch den Export. Wie sollte das weitergehen? Im 20. Jahrhundert, in der Russischen Revolution von 1917, kam es zum Ausbruch. Sicherlich musste die russische Gesellschaft irgendwie die westliche Organisationsfähigkeit erben. Aber wie schwierig ist das?


[^7]: Vgl. Rosenstock-Huessy, Die Furt der Franken, in E.Rosenstock-Huessy, E., Wittig, J., 1998. Das Alter der Kirche, in: Agenda, Münster, S.463 ff. (Orig. 1928).
[^8]: Siehe Moore, R.I., 2000. Die erste europäische Revolution, 970-1215, Blackwell Publishing, Oxford.
[^9]: Siehe Rosenstock-Huessy, E., 1989. Die Europäischen Revolutionen und der Charakter der Nationen, Moers, Brendow (Orig. 1931).
[^10]: Siehe K.S. Aksakow, Ausgewählte Schriften, in Hans Ehrenberg, 1923. Östliches Christentum, Band I, Oskar Beck München 88, ff.
[^11]: Karsawin weist in Der Geist des Russischen Christentums auf die All-einheit von Gott und Mensch hin, das die Möglichkeit des Bösen in der Liebe einschließt. Bulgakow wendet sich in Kosmodizee gegen den eschatologischen Charakter der katholischen Kirche im Westen, der eine dualistische Tendenz verrät. Selbst die Möglichkeit des Falschen und Bösen ist in der aufopfernden Liebe des Vaters und des Sohnes enthalten und umfasst, siehe Ehrenberg, Östliches Christentum, Band II, S. 314 ff. bzw. S. 462 ff.
[^12]: Für die deutschen Leser: Mehr als 10 Jahre lang haben die Steuerbehörden in den Niederlanden durch Profiling mit Hilfe von Algorithmen Tausenden von Menschen zu Unrecht Leistungen für Kinderbetreuung und ähnliches vorenthalten, auf die diese Menschen Anspruch hatten, was dazu führte, dass Menschen aus ihren Wohnungen vertrieben wurden, viele Ehen zerbrachen und Kinder außer Haus gesetzt wurden, alles wegen Schulden und Geldmangel.
>*Otto Kroesen*

### Der Kampf zwischen zwei Gesellschaften (3)

#### Verwestlichung Russlands und Russifizierung des Westens

Russland hat keine Zivilgesellschaft mit Zünften, Städten und Organisationen von unten nach oben entwickelt. Seit Zar Peter dem Großen wurden Versuche unternommen, Russland zu modernisieren. Das ganze 19. Jahrhundert hindurch war Frankreich mit seiner bürgerlichen Revolution und seinem Rationalismus sowie seiner demokratischen Regierungsform als Nationalstaat das Vorbild. In diesem dritten Beitrag beleuchten wir den privilegierten Moment der Wende vom 19. zum 20. Jahrhundert, der in der Russischen Revolution von 1917 gipfelte, die in Russland den wissenschaftlichen Sozialismus mit einer totalen staatlichen Planung einführte.

#### Die Russische Revolution und der wissenschaftliche Sozialismus

Im Jahr 1905 findet in Russland eine bürgerliche Revolution statt. Dies ist jedoch nur der Vorläufer des Ausbruchs von 1917, als mitten im Ersten Weltkrieg die radikalen Kommunisten, die Bolschewiki, unter der Führung von Lenin die Macht ergriffen. Lenin und seine Anhänger waren stolz darauf, dass ihr Land, die künftige Sowjetunion, als erstes die sozialistische Gesellschaft verwirklichte. Dies wäre übrigens ohne die Radikalisierung der Soldaten/Bauernsöhne an der Front in den Schlachten des Ersten Weltkriegs nicht gelungen.

Die Kommunistische Partei in Russland errichtete ein strenges Regime mit Staatseigentum an den Produktionsmitteln und totaler Planung der Produktion, das auch vor radikalen Maßnahmen wie Vertreibung der Bevölkerung, Verfolgung des bürgerlichen Mittelstandes, Straflager für Dissidenten nicht zurückschreckte. Alles drehte sich um die rationale Befriedigung gesellschaftlicher Bedürfnisse, und zwar unter staatlicher Kontrolle.

#### Sprachphilosophie in Russland

Im Westen nicht so bekannt sind die Namen einer Reihe russischer Philosophen/Theologen, die nicht vom Kommunismus, sondern von der russischen Orthodoxie inspiriert wurden. Dazu gehören Namen wie Solowjew, Bulgakow, Berdjajew, Florenski [^13]. Auch sie waren davon überzeugt, dass Russland westliche Institutionen und Werte übernehmen müsse, aber ihrer Ansicht nach sollte die russisch-orthodoxe Inspiration dafür verantwortlich sein. Die Namen der Schriftsteller Dostojewski und Tolstoi sind im Westen viel bekannter geworden. Auch sie wollten das westliche Erbe übernehmen und es gleichzeitig übertrumpfen, da sie mit der vorherrschenden westlichen Philosophie des Utilitarismus und Rationalismus nicht zurechtkamen.

Bemerkenswerterweise versuchten diese russischen Denker, die Schätze der russischen Orthodoxie durch eine neue Reflexion über die Sprache freizulegen. Die Menschen mögen fehlerhaft, ängstlich und manchmal böse sein, aber wenn sie einander ansprechen und sich einander öffnen, geschieht etwas. Dann kommen sie einander zu Hilfe und werden zu verflochtenen Freunden. So verflochten wie die Brüder in den russischen Klöstern, die alles miteinander teilen und Brüderlichkeit und Freundschaft praktizieren [^14]. Das ist die Lösung für die sozialen Probleme, die sie im Sinn haben.

Durch die Sprachphilosophie von Appell und Antwort fließt der Liebesüberschuss der russischen Seele in die Gesellschaft. Die Sprache selbst, so Florenski, hat eine trinitarische Struktur [^15]. Gott ist eigentlich der Einzige, der Ich sagen kann. Das Du, das Gegenüber von Gott, ist der Sohn, der sich in Christus offenbart, der aber uns alle als Angesprochene einschließt. Weil wir im Vater, der uns anspricht (Ich sagt), und im Sohn (angesprochen als Du) enthalten sind, verwandelt uns der Geist in ein Wir, eine Gemeinschaft von Menschen, die füreinander einstehen und gemeinsam an der Verwirklichung des Reiches Gottes arbeiten. So entsteht das Heil und nicht durch utilitaristische oder rationalistische Überlegungen. Diese sind eigentlich nur die säkulare Fortsetzung der Regulierung durch die katholische Kirche seit dem Mittelalter.

#### Die Krise der Nationalstaaten im Ersten Weltkrieg

Eine Gruppe hauptsächlich jüdischer Gelehrter in Deutschland sah um den Ersten Weltkrieg herum ebenfalls die Notwendigkeit, Nationalismus und Rationalismus zu überwinden. Deutschland hatte den Krieg verloren und die Nation drohte orientierungslos und verwildert zu werden. Da hilft die Vernunft nicht weiter. Im Appell und in der Antwort, im Sprechen und im Zuhören müssen sich die Menschen auch hier füreinander öffnen. Das ist der einzige Weg. Mit diesem Ansatz verbinden sich Namen wie Ehrenberg, Rosenzweig, Buber, Rosenstock-Huessy [^16]. Diese Sprachphilosophie sollte aus der Sicht Rosenstock-Huessys vor allem Brücken bauen zwischen Arbeitgebern und Arbeitnehmern, in gemeinsamer Verantwortung, trotz aller Widersprüche.

Ehrenberg nennt diese Wiederentdeckung der Sprache die Russifizierung des Westens. In der östlichen Orthodoxie sind die Schätze der Liebe Christi und der Hingabe an den Nächsten in der Liturgie und in den Klöstern bewahrt worden. Jetzt fließen Liebe und Respekt in die Gesellschaft ein und geben neue Impulse, wo die Pläne und das Denken der Menschen stecken bleiben.

#### Wo ist das geblieben - im Osten und im Westen?

Sowohl in Russland als auch im Westen ist diese neue Sprache eine Randerscheinung geblieben. Sie hat zwar einige Anhänger gefunden, konnte aber den Lauf der Gesellschaft nicht verändern. Russland blieb auf dem Weg des Staatskapitalismus mit einer wachsenden Rolle der Geheimdienste. Der Westen blieb auf dem Weg des Liberalismus mit zunehmendem Konsumismus, so dass viel von der bürgerlichen Zusammenarbeit, die das Erbe des katholischen Mittelalters und der reformatorischen Neuzeit war, verloren gegangen ist. Zwar wurden die schlimmsten Auswüchse des Liberalismus bekämpft, aber hauptsächlich im Westen selbst. Der Westen hat eine lange Tradition darin, die Errungenschaften, die das kulturelle Kapital der eigenen Gesellschaft ausmachen, anderen Gesellschaften zu vorenthalten [^17].

[^13]: In zwei bereits zitierten Bänden mit dem Titel Östliches Christentum, erschienen 1923 und 1924, lässt Hans Ehrenberg russische Schriftsteller, Philosophen und Theologen zu Wort kommen. Sowohl der erste als auch der zweite Band enthalten ein Nachwort von Hans Ehrenberg. Der erste Band und das erste Nachwort beschäftigt sich mit der Europäisierung Russlands, der zweite mit der Russifizierung Europas. Ehrenberg hat gemeinsam mit aus Russland emigrierten Theologen und Philosophen studiert, ist so Teil ihrer Diskussionen geworden und will mit diesen beiden Bänden in Worte fassen, was diese beiden Gesellschaften, die russische und die westeuropäische, einander zu sagen haben (siehe auch Hans Ehrenbergs Auseinandersetzung mit dem "Östlichen Christentum", Gedanken eines ökumenischen Visionärs, Nikolaus Thon, in Franz Rosenzweig und Hans Ehrenberg, Bericht einer Beziehung, Haag & Herchen, 1986, S. 150 - 194). Meines Wissens hat sich ein solches Experiment nicht wiederholt, abgesehen von dem Dialog zwischen Amerikanern und Russen, der von Clinton Gardner während des Kalten Krieges initiiert wurde. Darüber ist jedoch wenig verschriftlicht, siehe Clinton C. Gardner, Beyond Belief, White River Press, 2008.
[^14]: Florenski beschreibt diese Freundschaft und Brüderlichkeit als sehr offen und intim. Dabei verweist er auf den ungerechten Verwalter aus Lukas 16: Als der ungerechte Verwalter von seinem Chef wegen Misswirtschaft zur Rechenschaft gezogen wird, macht er sich Freunde unter seinen Untergebenen, indem er deren Schulden reduziert. Man kann dies als strategisches Verhalten ansehen. Aber laut Florenski sollten sich die Menschen im sozialen Leben so verhalten: Im Bewusstsein der eigenen Sünden entschuldigen und decken wir die Schulden der anderen zu, siehe An den Wasserscheiden des Denkens, S. 274 ff. in Östliches Christentum.
[^15]: Florenski, Der Pfeiler und die Grundfeste der Wahrheit, in An den Wasserscheiden des Denkens, editionKONTEXT, 1994, S. 75 ff.
[^16]: In seinem Nachwort zu Band II von Östliches Christentum weist Ehrenberg darauf hin, dass die östliche Orthodoxie johanneisch geprägt ist, weil sie geistig auf die Gesellschaft einwirkt; dies im Gegensatz zum Katholizismus, der durch Regeln, und zum Protestantismus, der durch Rationalität auf die Gesellschaft einwirkt. Das rechte Handeln ist nicht gesetzlich geregelt wie im Kirchenrecht und nicht logisch abgeleitet wie im Protestantismus des 19. Jahrhunderts (man denke an die Philosophie Kants), sondern das rechte Handeln findet seine Orientierung dank eines geistigen Appells, einer geistigen Macht. Wie im Johannesevangelium der Geist Christi die Richtung weist, so ist die lebendige Sprache der grammatischen Methode die Macht, die das freie Spiel der sozialen Kräfte lenkt. Zu dieser Interpretation von Ehrenbergs Östlichem Christentum siehe auch Rudolf Hermeier, Hans Ehrenberg und der Osten, in: Jenseits all unseres Wissens wohnt Gott, Brendow Verlag 1986, S.21 - 44.
[^17]: Winkler, H.A., 2011. Größe und Grenzen des Westens - Die Geschichte eines unvollendeten Projekts, LEQS No. 13/2011, https://papers.ssrn.com/sol3/papers.cfm?abstract_id=1762586, (26-4-2021).
>*Otto Kroesen*

### Der Kampf zwischen zwei Gesellschaften (4)

#### Zivilität oder Liberalisierung

Der Westen hat ein ziemlich beschränktes Verständnis von den Grundlagen, auf denen er selbst aufgebaut ist. Der Liberalismus geht davon aus, dass die Menschen aus freien Stücken einen Gesellschaftsvertrag zum gegenseitigen Nutzen abschließen. Der Mensch ist ein "homo oeconomicus", kalkulierendes Eigeninteresse führte ihn zur Kooperation. Dieses Menschenbild hat bei der Liberalisierung der Wirtschaft in Russland in den 1990er Jahren großen Schaden angerichtet. Der langfristige Schaden, den es im Westen selbst anrichtet, ist möglicherweise noch größer.

#### Die Zivilgesellschaft

Die Gesellschaft baut nicht auf kalkuliertem Verhalten auf, sondern auf Vertrauen und Verlässlichkeit auf der einen Seite und kontrollierenden Institutionen auf der anderen [^18]. Diese werden Schritt für Schritt aufgebaut, denn wenn man einander zunächst nicht vertraut, akzeptiert man auch nicht die Kontrollmechanismen der Institutionen. Nehmen wir als Beispiel die Zünfte und Städte im Mittelalter: Ein Eid auf die Gesetze der Stadt oder der Zunft war Voraussetzung für die Mitgliedschaft. Zünfte waren auch religiöse Institutionen, da die Vertrauenswürdigkeit und Loyalität jedes Mal explizit gemacht werden musste, um sie zu üben [^19].

Diese Institutionen wiederum formten den Charakter. Es entstand ein neues "Selbst", ein neuer Menschentyp. Wie sonst könnte man jemandem das Geld anvertrauen, der nicht zur eigenen Familie gehört? Zivilität bedeutet, dass man auch Nicht-Familienmitglieder behandelt als wären sie Familienmitglieder [^20]. Gegenseitiges Vertrauen ist nicht die einzige Errungenschaft. Hinzu kommt der Pluralismus: Man kann sehr unterschiedlicher Meinung sein und trotzdem etwas gemeinsam tun, wann immer es möglich ist. Man kann Kritik annehmen und Kritik üben, ohne dass die Beziehung zerbricht: Wir sind alle Sünder, das wissen wir. Diese Erkenntnis hat übrigens auch die Empfindsamkeit für Status drastisch reduziert. Aufgrund dieser Errungenschaften werden allerhand Problemen im gegenseitigen Geben und Nehmen gelöst, ohne dass der Staat eingreift. Dies ist kein natürliches Phänomen, sondern ein Lernprozess der Zusammenarbeit in immer größerem Umfang, der durch die europäischen Revolutionen ausgelöst wurde. Neben neuen Rechtsformen (Gewaltenteilung, Rechtsstaatlichkeit, Repräsentation) haben diese Revolutionen auch neue menschliche Eigenschaften hervorgebracht. Diese Eigenschaften und Werte wiederum halten diese Institutionen über Wasser.

#### Russland in den 1990er Jahren

Die 1990er Jahre waren ein Versuch, die russische Wirtschaft zu liberalisieren, ohne dass eine Zivilgesellschaft vorhanden war. Man ging davon aus, dass die Beseitigung von Hindernissen in Form von Staatsmonopolen ausreichen würde. Doch die Menschen vertrauten einander nicht und waren nicht verlässlich zueinander, und auch all die anderen Eigenschaften, die eine Zivilgesellschaft auf dieser Grundlage aufbauen könnte, waren nicht vorhanden. In dieser Lage eigneten sich ehemalige Direktoren von Staatsmonopolen, Mafiabosse und KGB-Agenten die ehemalige Staatsbetriebe an. Um einen von vielen Tricks zu nennen: Wenn ein Unternehmen versteigert wurde, geschah dies oft in abgelegenen Gebieten, so dass Konkurrenten entweder nichts davon wussten oder nicht dorthin gelangen konnten [^21]. Wertlose Fabriken wurden für Millionen verkauft und umgekehrt, wie es gerade passte. Überweisungen für verkaufte Staatsbetriebe wurden auf private Konten geleitet. Und so weiter. Nach den Überlegungen des liberalen "homo oeconomicus" sollte dieses Chaos des freien Wettbewerbs natürlicherweise zu einigen starken Unternehmen führen, die konkurrenzfähig sind, aber man vergaß, dass die Institutionen zur Durchsetzung von Fair Play ebenfalls nicht vorhanden waren. Mit Hilfe von Freunden an der Spitze ist es ohnehin möglich, Konkurrenten mit besseren Produkten auszuschalten.

Marshall Goldmann sagt dies in einem Artikel aus den 1990er Jahren: "Dies birgt das Potenzial für soziale Umwälzungen, die das bestehende Regime leicht untergraben und einer Form des Protofaschismus die Tür öffnen könnten, der behauptet, die einzige Autorität zu haben, um Ungerechtigkeiten der Vergangenheit zu beseitigen" [^22]. Dies sind prophetische Worte. Genau in dieser Richtung haben sich die Dinge entwickelt.

Gibt es also keinen Widerstand in Russland dagegen? Die russische Opposition hat es versucht und wurde im Keim erstickt. Die Menschen in Belarus haben es versucht und werden mit Putins Hilfe in Schach gehalten. Es wird argumentiert, dass die Ukraine eigentlich eine gemeinsame Geschichte mit Russland hat. Die Ukraine war schon immer ein Grenzland zwischen Ost und West, was auch der Name bedeutet. Wenn man so will, kann man die Ukraine auch als den Teil Russlands betrachten, der entschlossen ist, sich von dem russischen Knoten zu lösen, wie er sich nach den 1990er Jahren entwickelt hat. Wenn die Ukraine dabei erfolgreich ist, werden sich weitere Länder für diesen Weg entscheiden. Es ist immer wieder beeindruckend zu sehen, wie sich die Ukraine gegen den russischen Kurs wehrt: Alles besser als das!

#### Russifizierung nach Ehrenberg

Der westliche Liberalismus aber hat kein Gespür für den zivilen Unterbau, der das "freie Spiel der gesellschaftlichen Kräfte" erträglich macht und reguliert. Wie könnte er also für das völlige Fehlen einer Zivilgesellschaft in der Sowjetunion empfänglich sein? Auch der Westen hat ein Problem, denn durch den rein kalkulatorischen Umgang mit den Menschen schwindet auch im Westen das öffentliche und anonyme Vertrauen ineinander und in die Institutionen, und alle damit verbundenen menschlichen Qualitäten verflüchtigen sich langsam. Die Menschen werden zu Funktionen in einer Megamaschine und ihre Verantwortung wird durch Protokolle verletzt. Der Westen selbst sieht nicht, wie sehr seine eigene Gesellschaft noch dank der gegenseitigen Verantwortung funktioniert, die leider nicht gepflegt wird. Sowohl der Neoliberalismus als auch der Neostalinismus regieren die Gesellschaft durch eine totale Berechnung der sozialen Kräfte. Die moralischen Kräfte werden in beiden Gesellschaften so weit wie möglich ausgeklammert.

Die von Ehrenberg vorgeschlagene Lösung besteht in dem, was er als Russifizierung bezeichnet (siehe vorheriger Beitrag). Die Grammatik der Sprache der Liebe, des Appels und der Antwort, des Angesprochen- und Unterstütztwerdens, muss eine neue Gesellschaft in Ost und West verwirklichen. Er kann es Russifizierung nennen, weil es eine Anwendung der gegenseitigen Liebe der Brüder und Mütter der russischen Klöster und Kirchen in der Mitte der Gesellschaft ist. Die Grammatik der Liebe muss in den Bereich der harten Gesellschaft eintreten [^23].

Zu Beginn des 20. Jahrhunderts haben russische Schriftsteller und Philosophen sowie westliche (jüdische) Gelehrte dies versucht (siehe vorheriger Beitrag). Hier gibt es ein ungenutztes Reservoir für Ost und West. Beide brauchen es dringlich, sowohl um Frieden im Innern zu praktizieren als auch um Frieden in ihrer Grenzregion, in der Ukraine, zu schaffen.

[^18]: Fukuyama, F., 2011. Die Ursprünge der politischen Ordnung, Exmouth House, London.
[^19]: Rosser, G., 2015. The Art of Solidarity in the Middle Ages - Guilds in England 1250-1550, Oxford, Vereinigtes Königreich.
[^20]: Rosser, The Art of Solidarity in the Middle Ages - Guilds in England 1250-1550, 2015, 59. Die Worte aus Matthäus 12, 46-50 werden oft in den Satzungen von Zünften und Bruderschaften zitiert: "Wer ist meine Mutter und wer sind meine Brüder? Er wies auf seine Jünger und sagte: "Sie sind meine Mutter und meine Brüder. Denn jeder, der den Willen meines Vaters im Himmel tut, ist mein Bruder und meine Schwester und meine Mutter."
[^21]: Marshall Goldman, The Pitfalls of Russian Privatisation, in Challenge , Vol. 40, No. 3 (Mai-Juni 1997), S. 35-49 Taylor & Francis.
[^22]: Ebd.
[^23]: Kroesen J.O., 2015. Towards Planetary Society: the Institutionalisation of Love in the work of Rosenstock-Huessy, Rosenzweig and Levinas, in Culture, Theory and Critique, Taylor and Francis 56:1, DOI: 10.1080/14735784.2014.995770, S. 73-86.
>*Otto Kroesen*

### 3. Der Himmel über Berlin wackelt

#### Maßlose Arroganz „matcht“ historische Ahnungslosigkeit

In den Vereinigten Staaten ist die Ablehnung der abgehobenen Bürokratie Washingtons allgegenwärtig. Auch in Deutschland scheint der Verdruß über die Hauptstadt eine „Kultureffizienzwachstumschance“, im von Franziska Giffey etablierten PR-Gesetzesdeutsch. Dabei spielen Symbole eine besonders bezeichnende Rolle. Streit dreht sich immer wieder um Erinnerungsorte der preußischen und deutschen Geschichte, sei es bei der Umbenennung von Straßen, dem Wiederaufbau der Potsdamer Garnisonskirche, in der sich Hitler und Hindenburg ihr Jawort gaben, oder dem zentralen Symbol des Berliner Schlosses.

Man kann an einer Zeit zweifeln oder verzweifeln, die unfähig scheint, ihren Geist in Architektur zu fassen. Wer den Rohbau des Berliner Schlosses als gewaltigen Betonbunker gesehen hat, dürfte jedenfalls über dessen Zukunftsfähigkeit begründete Zweifel haben. Es sei nicht vergessen, daß die deutsche Geschichte, nach der Dominanz von Strukturen, Theorien und Systemen in den 60er und 70er Jahren, erst in den 80er Jahren wiederentdeckt wurde (nur in der Bildungspolitik herrscht der Strukturrationalismus nach wie vor). Zu erinnern ist an die große Stauferausstellung, die einen wahren Mittelalterboom auslöste. Und inzwischen dürfte es keine Epoche der deutschen Geschichte mehr geben, der nicht eine große Landesausstellung gewidmet war. Und in der gleichen Zeit wurde Preußen wieder „chick“, wie Hans Ulrich Wehler 1983 titelte. Er sprach schon damals von einer „Flucht aus der Wirklichkeit“. Preußen ist zwar viele Tode gestorben, aufgelöst wurde es aber erst 1947 durch Beschluss der siegreichen Alliierten. Auf der anderen Seite widmet sich seit 1957 die „Stiftung preußischer Kulturbesitz“ des großen historischen Erbes preußischer Kulturpolitik.

Dabei bleibt die Attrappe des Berliner Stadtschlosses auch nach Abschluss der Bauarbeiten Ziel kontroverser Diskussionen. Neben der Wiedererrichtung von Kuppel und Kreuz erregt vor allem die Umschrift der Kuppel Unmut: „Es ist in keinem anderen Heil, ist auch kein anderer Name den Menschen gegeben, denn in dem Namen Jesu, zur Ehre Gottes des Vaters. Dass in dem Namen Jesu sich beugen sollen aller derer Knie, die im Himmel und auf Erden und unter der Erde sind.“ Aus der zeitlichen Nähe zur Revolution von 1848 wollen Kritiker eine triumphale Geste der siegreichen Reaktion unter Friedrich Wilhelm IV., dem „Romantiker auf dem Thron” ablesen. Und als eine demonstrative Geste der Monarchie stehe die Kuppel für einen unüberbrückbaren Widerspruch zu einer gewählten Volksvertretung. Außerdem behaupte der König mit dem Bau einen allgemeinen Herrschaftsanspruch des Christentums.

Diesen schlichten Formeln, die vor allem von Seiten des Humboldt Forums geäußert wurden, hat der emeritierte Berliner Theologieprofessor Richard Schröder energisch widersprochen und den Akteuren „kulturgeschichtliche Blindheit“ attestiert.[^24] Wer „frühere Zeiten und fernere Kulturen“ verstehen wolle, müsse sich vor allem davor hüten, mit vorgefaßten Meinungen zu hausieren: „Er darf das für ihn Selbstverständliche nicht ungeprüft für immer und überall Selbstverständliche halten. Dem zu widerstehen erfordert eine beachtliche Anstrengung, der sich das Humboldt-Forum nur sehr gedämpft befleißigt.“ Gerade da Konfessionslosigkeit immer mehr um sich greife, gerate das christliche Gottesverständnis in Vergessenheit. Das Vakuum werden dann „mit Karikaturen aufgefüllt, die der Pflege des eigenen Feindbildes entgegenkommen.“ Es soll hier nicht mit Ernst Troeltsch darüber gestritten werden, ob es dieses eine christliche Gottesverständnis überhaupt geben kann. Es soll hier nur an eine Geschichte aus den „Europäischen Revolutionen“ von Eugen Rosenstock-Huessy erinnert werden, die gerade den angesprochenen Zusammenhang beleuchtet, zumal er seine Entdeckung sehr persönlich gedeutet hat:

„In der Denkmünze von Wittenberg 1702 finden Sie den Schlüssel zu meinem Lebensweg. Diese Münze ist für mich das Memento: ich war in Gefahr, die deutsche Universität auch dann noch als Gefäß des Heiligen Geistes zu vergöttern, als die es nicht mehr war. 1918 bekehrte mich. Bekehrungen sind ja nie Privatsache! Jede Bekehrung ist vielmehr ein Ereignis im offenen Himmel, wo Christus zur Rechten des Vaters steht. Wem also ähnliches droht, dem wird meine Bekehrung helfen können. Und weil die „Revolutionen“ mein Römerbrief VIII sind, deshalb haben Sie recht, sie als meine beste Leistung an die Deutschen zu betrachten.“[^25]

In seinem Göttinger Vortrag 1950 „Das Geheimnis der Universität“ hat er die Bedeutung der Gedenkmünze für die Entstehung der Universitäten zur Ausbildung für Fürstenräte in Deutschland eindringlich geschildert:

„Zum zweihundertjährigen Jubiläum der Universität Wittenberg schlug man eine Schaumünze. Auf der einen Seite der Münze sieht man den Kopf des Landesherren, auf der anderen Seite aber stehen Universitätsgebäude vor uns, über denen ein Band im Winde flattert, und auf dem Band steht hebräisch „Jehova“. [...]. Der Landesfürst von Gottes Gnaden schreibt also auf dieser Münze die göttliche Begnadung mit Geist nicht sich selber, sondern seinem Augapfel, der Landesuniversität, zu. Aber an den Feiertagen sagen sich die Völker die Wahrheit, die sie über sich selber ausgerufen hören. Feiertage verewigen.[^26] […]. Die Schaumünze von 1702 gesteht, daß die Universität das Wissen und Gewissen des Landesherren organisiert und dieses Wissen und Gewissen wird von der Universität ins Land gebracht. Wissen und Gewissen stehen im Gegensatz zu den Fürsten; denn die Fürsten sind angestammt, aber Gott inspiriert die Hochschule. Sie ist also von Gottes Gnaden wie der Fürst. Man kann es aber noch genauer sagen: sie verkörpert das universale Konzil der Kirche an dieser bestimmten Stelle, damit das Land und der angestammte Fürst in der ökumenischen Wahrheit bleiben können.“[^27] In den Europäischen Revolutionen konfrontierte er die Wittenberger Münze mit einer Münze Heinrichs VIII.: „Auf der begeisternden Wolke über Wittenberg steht: „Jehova“. Die Nazis haben daher ganz recht gehabt, bei der Präsidentenwahl von 1932 den Namen Hindenburg haßerfüllt in hebräischen Lettern zu setzen. Hindenburg war der letzte Regent des lutherischen Fürstenstaats. Hitler hingegen nahm die Rechte des ungeheuerlichen Heinrich VIII. Tudor, in Anspruch, Heinrichs Schaubild wurde 1535 geprägt.“[^28]

„Im Jahre 1913 schloß das Buch eines Berliner juristischen Professors: „Der Sinn des Staats ist der siegreiche Krieg.“ Das steht schon 1896 bei Treitschke: „Der Staat ist das Volk zu Schutz und Trutz.“ Und jauchzend gingen daraufhin zwei Millionen Kriegsfreiwillige in den Tod. Siegen wollen haben die Soldaten auch im zweiten Weltkrieg. Leider hatte dieser Lehrsatz nur einen Fehler. Er gab die Lehrgrundlage der deutschen Katheder preis, nämlich ihren Ursprung aus dem Konzil. Denn die Wahrheit der deutschen Universität stammte aus der universalen Menschheitsbestimmung aller ihrer Lehren. Also konnte der Staat, in den unsere Katheder aus der Kirche hineinragten, nur als gerechter Teil einer Friedenswelt reformiert werden. Sonst verlor die Stimme Gottes auf der Münze von 1702 ihren Sinn. Deshalb war der Reichskanzler Bethmann-Hollweg ein echter Rat. Denn er nannte Unrecht Unrecht, aber die Lehren Erich Kaufmanns oder die Lehre der kleindeutschen Historiker oder Ernst Haeckels haben die Grundlage der deutschen Wissenschaft preisgegeben. Diese Grundlage war das Gelöbnis, die Filiale der Konzilien der ewigen Wahrheit zu sein und so die Staaten zu verhindern, Räuberhöhlen zu werden. Nicht der Sieg, sondern das Recht prägt den Staat.“[^29] Ähnlich schilderte er ein Gespräch mit seinem Förderer an der Universität Leipzig, Rudolph Sohm: „One of the leading Protestants of my own life was so anti-Catholic that I thought he would not hold with the adoration of the saints. He wasn't my teacher, but I revered him very much, and I began my career under him. He allowed me to begin to teach in his faculty. And I once talked to him about this problem of the Protestants and their relation to the saints. And he burst forth and said, "Well, if we did not look up to these sacred and saint souls in our sky, we wouldn't deserve to have been ever led out of Egyptian darkness”.[^30]

Eine ähnliche Staatsvergottung hat er dem Rechtshistoriker Ulrich Stutz zum Vorwurf gemacht: „Der Führer des Unglaubens im Kirchenrecht, der Schweizer Stutz, war ein Renegat des Republikanismus. Bei seinem Gang in die Kaiser-Wilhelm-Gedächtniskirche in Berlin zum Gottesdienst pflegte er den Psalmvers: „Unter Deinen Flügeln mögest Du uns beschützen“, ausdrücklich auf den preußischen Adler zu beziehen. Den rief er an: protege nos. Des Daseins dieses Adlers war er, den die Preußen zum Geheimrat erhoben hatten, sehr viel sicherer als des Eingreifens Gottes. Ulrich Stutz ist nur der durchschnittliche, millionste Bürger. Die himmlische Heerschar preußischer Staat trug einen Namen für ihn, den er anrief. Der Gott dahinter war eine äußerst zweifelhafte Angelegenheit, er war eine Idee, und es ist die Schwäche aller Ideen, daß sie in Frage gestellt werden können!“.[^31]

Hermann Parzinger, seit 2008 Präsident der Stiftung Preußischer Kulturbesitz, verneigt sein Haupt nicht vor einem Gott sondern einer Göttin: Claudia Roth. Seine Eloge ist gewidmet „Claudia Roth zu ihren ersten hundert Tagen im Amt als Kulturstaatsministerin zugeeignet“. [^32] Es geht ja um Reformen, um Posten und um Gelder. Wer möchte sich da schon vor Gott verantworten?

[^24]: Richard Schröder, Mein Wort ist heilig, in: FAZ Nr.175 v. 31.7.2023.
[^25]: Eugen Rosenstock-Huessy an Georg Müller, Dezember 1951, in: Mitteilungen der Eugen Rosenstock-Huessy-Gesellschaft, 18. Folge, Mai (1973), S.3.
[^26]: Eugen Rosenstock-Huessy, Das Geheimnis der Universität, in: ders., Das Geheimnis der Universität. Wider den Verfall von Zeitsinn und Sprachkraft. Aufsätze und Reden aus den Jahren 1950 bis 1957, hrsg.v. Georg Müller, Stuttgart: W.Kohlhammer Verlag 1958, S.18.
[^27]: Ebda. S.19ff.
[^28]: Rosenstock-Huessy, Eugen, Die europäischen Revolutionen und der Charakter der Nationen, 3. veränd. Aufl., Stuttgart: W.Kohlhammer Verlag 1961, S.571.
[^29]: Eugen Rosenstock-Huessy, Das Geheimnis der Universität, in: ders., Das Geheimnis der Universität. Wider den Verfall von Zeitsinn und Sprachkraft. Aufsätze und Reden aus den Jahren 1950 bis 1957, hrsg.v. Georg Müller, Stuttgart: W.Kohlhammer Verlag 1958, S.25/26.
[^30]: Eugen Rosenstock-Huessy, Universal History 1967 (unveröffentlichtes Transkript), S.450.
[^31]: Eugen Rosenstock-Huessy, Die Sprache des Menschengeschlechts. Eine leibhaftige Grammatik in vier Teilen, Bd.1, Heidelberg: Verlag Lambert Schneider 1963, S.157ff.
[^32]: Hermann Parzinger, Ton, Steine, Scherben, in: Zeitschrift für Ideengeschichte „Der ligurische Komplex“, H.XVI/2 (2022), S.95-104.
>*Sven Bergmann*

### 4. Eugen Rosenstocks „Kreuz der Wirklichkeit“

#### Entdeckung, Konzeption, Verständnis, Perspektive

Erik Voegelin hat den Zustand der Wissenschaft an der Schwelle vom 19. zum 20. Jahrhundert plastisch beschrieben:
Die enormen Ausdehnungen unseres historischen Horizontes durch archäologische Entdeckungen, kritische Textausgaben und eine Flut monographischer Interpretationen ist so bekannt, daß sich weitere Ausführungen dazu erübrigen. Die Quellen liegen bereit, und die konvergierenden Interpretationen durch Orientalisten und Semitisten, durch klassische Philologen und Historiker der Antike, durch Theologen und Mediävisten laden geradezu zu dem Versuch ein, eine philosophische Studie der Ordnung auf der Grundlage der Primärquellen selbst durchzuführen. Die Lage der Wissenschaft in den verschiedenen Disziplinen, wie auch meine eigene Position bezüglich fundamentaler Fragen, wird im Verlauf der Studie selbst zur Sprache kommen.[^33]

Das durch alle Fakultäten zur Verfügung gestellte Wissen erreichte Ausmaße, die eine Aufnahme absolut unmöglich machte. Aber was waren die Konsequenzen? Achselzuckende Beliebigkeit? Beschränkung auf die eigene Schublade und weiter voranzutreibende Spezialisierung unter Preisgabe möglicher Relevanz? Einige Denker boten Alternativen zur Lage von zu viel Wissen und zu wenig wissen. So entwickelte Max Weber seinen „Idealtypus“ als regulatives Prinzip ja gerade vor diesem Hintergrund, um die Masse des zur Verfügung stehenden Materials zu bändigen und das charakteristische herauszuarbeiten, um es „deutend zu verstehen“: „Endlos wälzt sich der Strom des unermeßlichen Geschehens der Ewigkeit entgegen. Immer neu und anders gefärbt bilden sich die Kulturprobleme, welche die Menschen bewegen, flüssig bleibt, damit der Umkreis dessen, was aus jenem stets gleich unendlichen Strome des Individuellen Sinn und Bedeutung für uns erhält, „historisches Individuum“ wird. Es wechseln die Gedankenzusammenhänge, unter denen es betrachtet und wissenschaftlich erfaßt wird. Die Ausgangspunkte der Kulturwissenschaften bleiben damit wandelbar in die grenzenlose Zukunft hinein, solange nicht chinesische Erstarrung des Geisteslebens die Menschheit entwöhnt, neue Fragen an das immer gleich unerschöpfliche Leben zu stellen. Ein System der Kulturwissenschaften auch nur in dem Sinne einer definitiven, objektiv gültigen, systematisierenden Fixierung der Fragen und Gebiete, von denen sie zu handeln berufen sein sollen, wäre ein Unsinn in sich“[^34]. Der von ihm skizzierte Idealtypus war nur ein Werkzeug, die Wirklichkeit zu befragen, nicht die Wirklichkeit selbst!

Eine wirklich kritische Vergleichung der Entwicklungsstadien der antiken Polis und der mittelalterlichen Stadt (vgl. z.B. die Bemerkungen darüber in Eberhard Gotheins Wirtschaftsgeschichte des Schwarzwalds S.61ff.) wäre ebenso dankenswert wie fruchtbar, - natürlich nur, wenn sie als Ziel nicht, nach Art der heute modischen Konstruktionen von generellen Entwicklungsschemata, nach „Analogien“ und „Parallelen“ jagt, sondern gerade umgekehrt nur dann, wenn ihr Zweck die Herausarbeitung der Eigenart jeder von beiden, im Endresultat so verschiedenen, Entwicklungen und so die Leitung der kausalen Zurechnung jenes verschiedenen Verlaufs ist.[^35]

Auch die Konferenzen 1910 in Baden-Baden, bei denen es zum Bruch zweier Lager von Philosophen und Historikern kam, rührte gerade von dieser Frage her. Franz Rosenzweig unternahm seinen Versuch einer dialektischen Deutung der Geschichte des 19 Jahrhunderts als einer Synthese aus den Haupttendenzen des 17. und 18. Jahrhunderts mit der kardinalen Französische Revolution als Durchbruch, um der Vielfalt der historischen Vorkommnisse eine Richtung zu geben. Gerade gegen diese „Geschichtsmetaphysik“ erhob sich der Protest der angehenden Historiker aus der Schule Friedrich Meineckes. Jede Art von Geschichtsphilosophie war für sie des Teufels. Dabei hatte schon Max Weber betont, daß Historiker oft den gleichen Schemata unterlägen, die sie bei anderen geißelten, namentlich auch Meinecke selbst.

Fast zeitgleich faßte Eugen Rosenstock-Huessy seinen Plan einer Soziologie im Sinne einer „Universalrechtsgeschichte“, den er seit seinem Studium fast 50 Jahre lange verfolgte. Ein erster Band erschien schon 1925, blieb aber Torso. Auf Anregung des Verlegers de Gruyter war wohl eine Einführung in Studium der Soziologie für einen wachsenden Markt gedacht. Aber es sollte ganz anders kommen. Nach vielen Widrigkeiten mit Verlegern und den schwierigen Zeitverhältnissen konnten Ende der fünfziger Jahre zwei voluminöse Bände vorgelegt werden. Ein Projekt das sowohl Verleger als auch Rezensenten überforderte.[^36]

Eugen Rosenstock-Huessys Weg aus der alexandrinischen Unübersichtlichkeit setzte an zwei Stellen an: Zum einen wies er auf die Bedeutung der Namen für die Geschichte hin und warnte vor einer Verwechslung von Namen, Begriffen und Dingen:\
*Der Wirklichkeit, die uns aus Namen entgegentritt, kommen wir also niemals durch abstrakte Begriffe näher. Das ist eine Erkenntnis von großer Tragweite. Viele Soziologen haben dagegen verstoßen, indem sie mit Vorliebe unbenannte Beispiele einer A-Kraft und einer B-Beziehung eines Herrn X, der den Herrn Y trifft, konstruieren (ähnlich der Sitte der Juristen und ihr wohl entlehnt). Die Wirklichkeit kennt aber keine Wenn und Aber, keine X und Y! Erst muß der Zustand, die Begebenheit, das Leben nach Nam' und Art, Ort und Datum vergegenwärtigt werden, ehe man hinterher aus ihnen irgendwelche Erkenntnisse ableiten kann. Die benennende, die Dinge beim wirklichen Namen nennende Vergegenwärtigung ist also die Voraussetzung all unserer Gedanken über die Wirklichkeit. Vorher sind wir eben in der Unwirklichkeit.*[^37]

Hatte er so schon die Priorität menschlichen Handelns hervorgehoben, faßte er die Konstellation jedes menschlichen Handelns als „Kreuz der Wirklichkeit“, ein vierpoliges Spannungsfeld zur Beschreibung von Raum und Zeit. Und er ging nicht einfach von einem gleichberechtigten Nebeneinander von Zeit und Raum aus, wie es häufig vorzufinden war. Für ihn ist die Zeit die eigentlich existentielle Kategorie. Den Raum kann man wechseln, der Zeit entgeht der Mensch erst mit dem Tod. Demnach ist die eine Achse des Kreuzes definiert durch die Zustände vorher nachher. Die andere Achse beschreibt räumlich innen und außen: Meine Wahrnehmung der Wirklichkeit außer mir und mein inneres Auge, die beide nicht immer, ja sehr häufig schwer zur Deckung zu bringen sind:

Wir können sagen, daß ein Mensch unfähig wird zu denken oder zu sprechen, es sei denn, er habe die Freiheit, alle vier Bestandteile auf sich zu berufen. Nicht die Bestandteile sind in Dichtung, Wissenschaft, Politik oder Religion verschieden, sondern ihre Anordnung. Des Menschen Geist ist immer komplex, weil er das Kreuz unserer Wirklichkeit widerspiegeln muß. Des Menschen Geist ist verwurzelt in einer Seele, die vier verschiedene Formen wie Trajekt, Objekt, Subjekt und Präjekt annehmen kann, weil sie auf allen diesen vier Fronten des Lebens zu jeder Zeit kämpfen muß. Herkunft, Zukunft, Eigenart und Schauplatz verlangen immerdar Gehör.[^38]

Demnach steht jeder Mensch in jedem Augenblick in einem Spannungsfeld, in einem Koordinatensystem, zwischen innen und außen, sowie auf dem dünnen Grat zwischen Vergangenheit und Zukunft. Objekt, Subjekt, Präjekt, Trajekt bilden die vier Modi menschlicher Existenz.
Der Mensch ist wie jedes lebendige Geschöpf den vier Richtungen von Zeit und Raum – vorwärts, einwärts, auswärts, rückwärts – ausgesetzt in jedem Denk- oder Sprachvorgang.[^39]

Inwiefern er hier das Kapitel „Kreuzung sozialer Kreise“ aus Georg Simmels Soziologie von 1908 im Hinterkopf hatte, ist bisher schwer zu bestimmen. Den beabsichtigten Titel „Das Kreuz der Wirklichkeit“ trugen aber weder seine Soziologie von 1925 noch die beiden Bände aus den 50er Jahren, sondern erst die 2009 veröffentlichte posthume dreibändige Fassung, die wiederum andere Editionsprobleme mit sich brachte.[^40] Sein Bielefelder Freund Georg Müller hat das Ringen um sein mutmaßliches Hauptwerk auf den Punkt gebracht:
Eugen Rosenstocks Lebenswerk ist nicht leicht zu überschauen, und die hierauf gerichteten Bemühungen sind nicht dadurch erleichtert worden, daß sich zu mehr als vierzig Buchtiteln in den letzten Jahren zwei Bände „Soziologie“ – Die Übermacht der Räume, Die Vollzahl der Zeiten – gesellt haben. Der Autor selbst möchte sie mit den „Europäischen Revolutionen“ als ein dreiteiliges Hauptwerk angesehen wissen und hätte sie gern unter dem unverfänglicheren Titel „Das Kreuz der Wirklichkeit“ veröffentlicht. Vielleicht wäre das Werk verständniswilliger aufgenommen worden, wenn Rosenstock in der Titelfrage dem Verlegerwunsch sich nicht gefügt hätte. Vielleicht überschätzt er auch die Fähigkeit unserer Zeit zu geduldigem Lesen und meditierendem Hinhören auf mitgeteilte Tatsachen, wenn er meint, daß sein Triptychon auch ohne die dem Wesen der Sprache nachspürenden Veröffentlichungen zugänglich sein müßte.[^41]

#### Wurzeln

In ihrem Briefwechsel kündigte Eugen Rosenstock seinem Freund Franz Rosenzweig 1916 seinen „erlösenden Schritt ins System“ an und am 19. Juli des gleichen Jahres unterbreitete er ihm erstmals das Skelett dieses „Systems“, aus dem sein Leben währendes Ringen um seine Soziologie „Kreuz der Wirklichkeit“ erwuchs.[^42] Seiner Frau Margrit hatte er bereits im Januar des Jahres seine existentielle Spannung bei diesen Überlegungen geschildert:
Für mich gibts jetzt nur: Entweder ich erreiche mit Anspannung der äußersten Kraft dies Werk auszuführen, oder es ist ein für allemal mit meiner geistigen Würdigkeit aus.[^43]

Gerade in der Kriegszeit und in seinen Aufsätzen aus dieser Zeit erkannte er die Korrespondenzen der europäischen Nationen, ihr Gespräch untereinander. Gerade in der Höchstspannung des Kriegs hätten sie ihre innigsten Glaubensüberzeugungen herbeigerufen. Der Aufsatz „Der Kreuzzug des Sternenbanners“ aus dem Sommer 1918 reflektierte den Kriegseintritt der Vereinigten Staaten und beschrieb das veränderte Verhältnis des „Alten Europa“ zur „Neuen Welt“. Er erkannte das Kriegsjahr 1917 als große Wendung vom europäischen zum Weltkrieg: „Ein ganz neuer Krieg ist im Gange, ein amerikanisch-europäischer, mit anderen Zielen, um andere Fragen als der dreijährige Krieg der europäischen Großmächte.“[^44] Und damit verwandele sich der europäische „Bruderkrieg“ in einen Glaubenskrieg, in einen „Kreuzzug“ Amerikas gegen den „deutschen Militarismus“[^45]. Die Losung Amerikas laute: „Der Mensch ist gut!“ und müsse von Tyrannei und Zwangsherrschaft befreit werden.[^46] „Ein Kreuzzug, ein uneigennütziger, selbstloser Krieg ist nun der Kampf gegen die Sultane und Kaiser der alten Welt.“[^47] Im Westen stehe der britische Vorwurf des Militarismus gegen Deutschland und in den USA die Anklage des Autoritarismus.[^48] Der Präsident verlange eine einheitliche Weltordnung freier Staaten. Und Europa habe in seinem Irrwahn nicht das Recht, Amerika den Spiegel vorzuhalten. Allein die Freiheit der Kirche könne dem amerikanischen Irrglauben an das Sternenbanner den rechten Glauben entgegenhalten, „die Lehre vom Kreuz als mit seinem ewigen Widerspruch zwischen Gesetz und Liebe.“[^49] Er bezeichnete das Kreuz als „Offenbarung der menschlichen Zerrissenheit“ und „der höchsten, der menschlichen Qual.“[^50] Deshalb sei es im Zuge des allgemeinen Formenschwundes auf allen Gebieten des Lebens verdrängt worden, ja selbst aus dem christlichen. Erst das äußere Ereignis des Krieges habe die Menschen „nach innerer Notwendigkeit schreien“ lassen[^51]. „Die Formen des Lebens wurden plötzlich wieder ernsthaft.“ Auf dieser von ihm im Weltkrieg erkannten Korrespondenz der Völker ist dann später sein Werk über die „Europäischen Revolutionen“ gereift und sein „Kreuz der Wirklichkeit“.

Eine erste Form haben seine Vorsätze erst Jahre später gefunden wie er selbst schreibt. Er datierte seine Entdeckung auf einen Urlaub in Beatenberg am Thuner See in der Schweiz von August bis September 1924, in dem es auch zu Treffen mit Karl Barth und Hans Ehrenberg gekommen war.[^52] Seinem Freund Rudolf Ehrenberg teilte er nach dem Zweiten Weltkrieg in einem umfangreichen Brief mit:
„Das Kreuz der Wirklichkeit ist kein Schema. Es ist eine Entdeckung, 1924 gemacht, und nach Beatenberg in mein bestes Buch eingeschrieben „Die Kräfte der Gemeinschaft“ (das in Deutschland neu erscheinen sollte). A. Meyer hat es dann in derselben Weise auf die Biologie angewendet wie ich im letzten Kapitel auf Kant. Lies dies mal bitte nach. Es beruht auf der folgenschweren Entdeckung, daß Zeit und Raum von uns in umgekehrter Art erfahren werden und daß die beliebte Parallele „Zeit und Raum“ unhaltbar ist.
Es wird nämlich der Raum als Universum, die Zeit als Moment empirisch angetastet, und wir müssen zum Universum Innenräume, zum Moment Zeiträume hinzutun bevor aus diesem ersten Schritt – hier in der Zeit, dort in dem Raum – Erfahrung werden kann. Jedes Innen ist kleiner, jeder Zeitraum ist größer als das Erinnern dartuen! Häuser und Zeiten sind die menschlichen Taten, die aus dem panischen Moment und der panischen Welt erfahrbare Tatsachen machen! „Der Fortschritt der Wissenschaft“ ist ein geschaffner Zeitraum, „die Erde“ eine geschaffne Unterteilung des Raums.
Daher kommt es nun zweitens, daß Raum und Zeit nur in der Doppelung von Innen und Außen, Vergangenheit und Zukunft gegeben sind. Wir entstehen zwischen ihnen. Das ist kein Schema, sondern gilt für jede historische Gruppe.
Drittens, diese Doppelpoligkeit des Kreuzes ändert die Begriffe Innen und Außen, Vergangenheit und Zukunft aus auf die Gruppe zustrahlenden in von der Gruppe ausstrahlenden Richtungen. Aus Vergangenheit wird rückwärts, aus Zukunft wird vorwärts, aus Innen wird einwärts, aus Außen wird auswärts. Das ist aber revolutionär. Sieh das letzte Kapitel der Soziologie darüber! Denn nun gibt es keine Romantik oder Utopie!
Viertens, nun wird eine Diagnostik kranker und gesunder Gruppen möglich. Ich habe eben an einen Briten in Bonn darüber geschrieben. Wenn’s Dich interessiert, schicke ich Dir die Abschrift. Jede Gruppe zeigt Kreuzverkrüpplungen! Und sie heilen einander.
Fünftens, die Sprache dient der Begründung der historischen Zeit- und Raum-Koordinaten. Namen sind eben dies. Das Verhalten von Namen und Pronomina, von Hochsprache und Dialekt, zeigt die ständige Spannung. Darüber, daß die Stämme den Namenshochraum gestalteten, die Ägypter aber den pronominalen Hausraum, habe ich eben wunderschöne Entdeckungen gemacht.
All dies kommt zu seiner Erfüllung in der ersten wissenschaftlichen Grammatik, die nun möglich wird. Endlich kann Grammatik auf empirisch Wahrnehmbares basiert werden. „Du“, „ich“, „wir“, „es“ sind die vier Richtungen vorwärts, inwärts, rückwärts, auswärts (präject, subject, traject, object). Das ist kein Schema, sondern Gesetz des Sprechens, wie mein „Wort des Menschengeschlechts“ darlegt. - Dabei gebe ich Dir übrigens freudigst zu, daß Israel und die Kirche sprachmächtig sind, Laotse und Buddha sprachohnmächtig. Aber das Kreuz ist kein Schema; es entdeckt wie jede Arbeitshypothese dieses: historische Gruppen und ihr Befinden. Deshalb regt mich doch unsre Untreue gegen Patmos auf. Ich habe eben die Korrektur meines Aufsatzes über die Kreatur gelesen (Chicago 1947). Die Conprocessio des Geistes in verschiedenen Sprachen (Goethe – Schiller, Nietzsche – Dostojewski) ist das Problem der Zeit.[^53]

#### Mächte und Kräfte

Die Vierzahl seiner Entdeckung führte er schon in der strukturellen Gliederung seiner frühen Soziologie konsequent durch: Eugen Rosenstock-Huessy, Soziologie I: Die Kräfte der Gemeinschaft, Berlin und Leipzig: Walter de Gruyter 1925. Ob dies zum Verständnis seiner Entdeckung beigetragen hat, mag bezweifelt werden. Zumal er im letzten Abschnitt von „Kants Schöpfung der soziologischen Disziplin“ schreibt. Während der Verleger und wohl auch die zu erwartenden Kunden einem übersichtlichen Abriß der Soziologiegeschichte und eine Einführung in die wesentlichen Theorien erwarteten, lieferte Eugen Rosenstock eine vollkommen eigenständige, in ihrem Zugriff auch völlig neuartige Soziologie. Zwar kunstvoll strukturiert, mußten die Stichworte doch verwirren. Da war von Todesüberwindern die Rede, von den „Handlungen vom Tode her“, von Sport und Kampf, von Lebensaltern und den Sprachen der Völker. Einerseits war von Saint-Simon als dem ersten Soziologen die Rede, dann wieder von Kants Schöpfung der Disziplin. Das war harte Kost, nicht nur für Studenten, die sich Orientierung erwarteten. Der schärfste Kritiker der frühen Soziologie, der Freiburger Historiker Georg von Below, zollte dem Werk Respekt, sah dessen Meriten aber eher in der kunstvoll poetischen Sprache als im Zugriff auf die Wirklichkeit. Von einer Rezeption der ersten Soziologie kann bis heute nicht gesprochen werden. Gerade bei einem Autor wie Eugen Rosenstock-Huessy, der äußerst sparsam Fußnoten setzte, hätte man wenigstens den Hinweisen nachgehen müssen, die er selbst in den Text aufnahm. So spricht er von einigen „genialen Forschern“, namentlich James George Frazer, Heinrich Schurz und Max Weber.[^54] Von Max Weber war schon die Rede. Der Ethnologe Frazer beschäftigte sich mit dem Grenzgebiet von Ethnologie und Religion und wurde mit seinen Schriften über Totemismus und „Der goldene Zweig“ u.a. Anreger von Sigmund Freud. Der Völkerkundler Heinrich Schurz prägte den Begriff „Männerbünde“ und beschäftigte sich mit „Urkulturen“, Speisevorschriften und legt 1893 einen „Katechismus der Völkerkunde“ vor. Aber zurück zur Soziologie. Mit der Vierzahl stellte er sich ausdrücklich in die Tradition Goethes und Augustinus und bezeichnete seinen Ansatz als „Ausweg aus der philosophischen Denkweise“.[^55]

„Eins und zwei und drei erzählt wohl auch der Märchenerzähler! Mit diesen Punkten entscheidet sichs noch nicht, ob der Erzähler phantasiert oder berichtet, dichtet oder erforscht. Wissenschaftlich hat daher das Erzählen nie sein wollen noch können. Denn zur Wissenschaft gehört Nachprüfbarkeit. Daher ja soviel Lügen über menschliches umlaufen: Erzählen kann man viel. Die Geschichte als Wissenschaft hat daher noch Punkt 4, die Erkenntnis des Ursprungs, hinzugenommen, und räumt bekanntlich tausendjährigen Schutt weg, um „die Quellen“ und „Ursprünge“ klarzulegen. Das war ein großer Fortschritt. Aber das Lügen hat nicht aufgehört. Die Historiker von heute lügen entweder mit oder sie hören auf zu vergegenwärtigen und bleiben in den Quellen stecken. Es muß also noch ein Moment hinzukommen, eine Frage, die der Historiker nicht ausdrücklich beantwortet, die aber die Frage aller Fragen ist, muß der Soziologe fragen, ehe er seine Vergegenwärtigungsaufgabe gelöst hat. Die entscheidende Frage, durch die Erzählen kontrollierbar wird, ist die nach der Stunde, in der erzählt wird. Das ist nicht zu verwechseln mit dem Standpunkt des Erzählers. Denn über den täuscht sich der Erzähler allzu gern selbst. Nein, die Stunde der Erzählung von Wirklichem gibt an, ob das Erzählte selber der Erzählung mit zuhört oder ob es als alte versunkene Märchenwelt, oder auch als fremdes Rätselland vor dem Erzähler steht. Wir werden dies Grundgesetz aller Soziologie, daß man nur mit Angabe der Stunde jede Vergegenwärtigung recht hören kann, wegen seiner Wichtigkeit durch das ganze Buch hindurch immer neu prüfen und kennen lernen.“[^56]

Die zentrale Passage zum Kreuz der Wirklichkeit auf die er sich in seinem Brief an Rudolf Ehrenberg bezog ist überschrieben: „Die Mythen um die Zahl Vier und die Metaphysik“:

„Und doch müssen wir einen solchen zahlenmäßigen Vergleich unserer Grundkategorien mit den vorher aufgeführten als unfruchtbar, ja als die eigentliche Verkehrung unseres Beginnens ins Mythologische bezeichnen. Es haben nämlich die Vierzahlen in allen diesen Fällen und dem unseren nur etwas Negatives unter sich gemeinsam! Das Auftreten der Vierzahl bedeutet an sich nichts als die Hinwendung zur Fülle der Erscheinungen, bedeutet ein Hinauswachsen der Betrachtungsweise aus dem rein logischen Bereich! Wenn die Griechen den Kosmos erfassen wollen, so sehen sie in ihm vier Elemente: Wasser, Feuer, Erde und Luft. Und die Chinesen, ein Volk der Soziologie vor allen Völkern, treiben die Bedeutung der vier Himmelsrichtungen aufs äußerste! Der soziale Raum wird hier von der Mitte her in Norden, Süden, Osten und Westen so einseitig aufgeteilt und ausgedeutet, wie bei Spengler die Jahreszeiten der geschichtlichen Zeit den Rhythmus geben. Auch für die Kosmodizee, die Schöpfungslehren der kirchlichen Theologie, ist die Vier grundlegend. Sie ist das „Sinnbild des nach vier Seiten ausgedehnten Raumes, der in vier Jahreszeiten geteilten Zeit, des in vier Altersstufen ablaufenden Lebens“. Daher die Symbolik der vier Erzengel, der vier Kardinaltugenden, der vier Flüsse, die vom Kreuze in die Welt verlaufen.“[^57]

In der menschlichen Geschichte gebe es keinen Ruhepunkt und auch keinen Sehepunkt, von dem aus man die Entwicklung „objektiv“ betrachten könne. Das gelte selbst für die Naturwissenschaften nicht mehr: „Wer die Erde aus den Angeln heben will, sagte Archimedes, der muß außerhalb der Erde Posto gefaßt haben. Das gilt genau so von den Zeiten. Wer aus vier Kalendern einen machen will, der muß aus seiner von diesen vier Kalendern besessenen Zeit erst einmal heraustreten. Das Leiden Jesu ist also der vollständige Inhalt des Lebens Jesu zu seiner „eigenen“ Zeit. Dieser Zeit hat er sich entäußert, als er weder Priester noch Prophet noch Feldherr noch Künstler wurde. Es war daher das erste Anliegen der Liberalen, dieser angeblich vorurteilslosen Erfinder der „Leben Jesu“, ihn in seine eigene Zeit zurückzustoßen und ihn für einen Revolutionär, einen Lehrer, einen Künstler, ein religiöses Genie auszugeben. Wäre er einer von diesen „Typen“, dann gehörte er in das Konversationslexikon für die Neugier des Publikums.“[^58]

„Alle Gewalten und Gestalten des geschichtlichen Lebens hingegen verändern sich eben dadurch, daß ich in die Zahl derer eintrete, die sich mit ihnen befassen. Und deshalb gehört die Erkenntnis, wie sie auf mich wirken und wie gerade ich zu ihnen stehe, als notwendiger Bestandteil in das Verstehen dieser Mächte hinein. Nicht wegen meiner Person, sondern um den Machtbereich jener Gewalt kennenzulernen, ist das interessant. Sie hat also auch mich, oder sie hat mich nicht!.“[^59] Ähnlich verhält es sich übrigens auch mit der Quantenmechanik!

Im Zentrum der graphischen Darstellung des „Kreuzes der Wirklichkeit“ die Eugen Rosenstock-Huessy in seine „Angewandte Seelenkunde“ aufgenommen hat, steht als zweite Person das Recht mit Gesetzgebung und Rechtsprechung[^60]. Der Mensch sei in einem viel höherem Maße ein politisches Lebewesen, „als es der Metaphysiker Aristoteles geglaubt hat. Denn auch der Philosoph und die Seele des Philosophen streckt sich nicht nur nach dem abstrakten Wahren, Guten, Schönen.“[^61] Deshalb findet sich auch kein Werk von Eugen Rosenstock-Huessy, weder „Das Alter der Kirche“, „Die Sprache des Menschengeschlechts“, „Die Europäischen Revolutionen“ oder die „Soziologie“ ohne einen deutlichen Bezug zum Politischen. Selbst die „Angewandte Seelenkunde“ schließt mit dem den Worten:

„Die Lappen haben an ihrem Schlitten vorn eine lange Stange, an deren Spitze eine Wurst hängt. Die Hunde laufen nun wie irrsinnig hinter der Wurst her. So die Idealisten hinter ihrer selbst aufgehängten Idealwurst. Dies sture Verhalten gilt in Deutschland als grundsatzfester Ideendienst, als Politik. Wir erfuhren es anders bei unserer Übersetzung. Hinüber zu setzen auf ein anderes Ufer – dies Wagnis ist Politik. Zu übersetzen ist dabei Gestalt in Gestalt, Satz in Satz. Alle grammatische Methodenlehre ist daher selbst nicht logische oder mathematische Theorie, sondern mutige Übersetzung, ein Vorstoß und Vormarsch ins ungeschaute Land.“[^62]

In seinem existentiellen Verständnis von Soziologie traf er sich mit dem von ihm als „genialen Forscher“ eingestuften Max Weber. Dieser hatte in seinem Vortrag von 1917 über Wissenschaft als Beruf festgehalten: „Nichts ist für den Menschen als Menschen etwas wert, was er nicht mit Leidenschaft tun kann.“[^63] Und so pflichtete der 24 Jahre jüngere Eugen Rosenstock ihm bei: „Soziologie entspringt aus der Leidenschaft, nicht aus der theoretischen Gleichgültigkeit. Damit ist sie nun allerdings in Gefahr, nicht mehr als Wissenschaft sich behaupten zu können. Und diese Gefahr ist in der Tat riesengroß vor den Soziologen aufgetaucht.“[^64]

[^33]: Eric Voegelin, Ordnung und Geschichte, Bd.1: Die kosmologischen Reiche des Alten Orients – Mesopotamien und Ägypten, hrsg.v. Jan Assmann, München: Wilhelm Fink Verlag 2002, S.30.
[^34]: Max Weber, Die „Objektivität“ sozialwissenschaftlicher und sozialpolitischer Erkenntnis, in: ders., Gesammelte Aufsätze zur Wissenschaftslehre, hrsg. von Johannes Winckelmann, Tübingen: J.C.B.Mohr(Paul Siebeck) 1985, S.184.
[^35]: Max Weber, Agrarverhältnisse im Altertum, 3.Aufl. <1908>, in: ders., Zur Sozial- und Wirtschaftsgeschichte des Altertums. Schriften und Reden 1893-1908, hrsg. von Jürgen Deininger (= MWG I/6), Tübingen: J.C.B.Mohr(Paul Siebeck) 2006, S.747.
[^36]: Etwa: Otto Heinrich von der Gablentz, Metapolitik - Zur Soziologie Eugen Rosenstocks, in: Zeitschrift für Politik, 6. Jg. (1959), S.277-284.
[^37]: Eugen Rosenstock, Soziologie I: Die Kräfte der Gemeinschaft, Berlin und Leipzig: Walter de Gruyter 1925, S.10ff.
[^38]: Eugen Rosenstock-Huessy, Die Einsinnigkeit von Logik, Linguistik und Literatur, in: ders., Die Sprache des Menschengeschlechts. Eine leibhaftige Grammatik in vier Teilen, 1. Bd., Heidelberg: Verlag Lambert Schneider 1963, S.564.
[^39]: Eugen Rosenstock-Huessy, Die Einsinnigkeit von Logik, Linguistik und Literatur, in: ders., Die Sprache des Menschengeschlechts. Eine leibhaftige Grammatik in vier Teilen, 1. Bd., Heidelberg: Verlag Lambert Schneider 1963, S.564.
[^40]: Fritz Herrenbrück, Altachtundsechziger Vatermord. Gibt es Heilkraft? Gibt es Argonautik? Zur Neuedition der »Soziologie« I (1925/1956/1968) und II (1958) von Eugen Rosenstock-Huessy; Bearbeitungsstand: 21. Dez. 2012: http://www.fritz.herrenbruck.de/rosenstockhuessy.html.
[^41]: Georg Müller, Die neue Wissenschaft vom Menschen und der neue Sprachstil. Anläßlich von Eugen Rosenstock-Huessys historischer Soziologie, in: Frankfurter Hefte, 15. Jg., H.10 (1960), S.687.
[^42]: Franz Rosenzweig, Briefe, hrsg.v. Edith Rosenzweig unter Mitwirkung v. Ernst Simon, Berlin: Schocken Verlag 1935. Franz Rosenzweig und Eugen Rosenstock: Judentum und Christentum, S.641ff.
[^43]: Eugen an Margrit, 12.1.1916, ERHFDA.
[^44]: Eugen Rosenstock, Der Kreuzzug des Sternenbanners, in: Hochland, 16.Jg. (1918, November), S.12ff.
[^45]: Eugen Rosenstock, Der Kreuzzug des Sternenbanners, in: Hochland, 16.Jg. (1918, November), S.15.
[^46]: Eugen Rosenstock, Der Kreuzzug des Sternenbanners, in: Hochland, 16.Jg. (1918, November), S.14.
[^47]: Eugen Rosenstock, Der Kreuzzug des Sternenbanners, in: Hochland, 16.Jg. (1918, November), S.15.
[^48]: Steffen Bruendel, Volksgemeinschaft oder Volksstaat. Die „Ideen von 1914“ und die Neuordnung Deutschlands im Ersten Weltkrieg, Berlin: Akademie Verlag 2003, S.240ff.
[^49]: Eugen Rosenstock, Der Kreuzzug des Sternenbanners, in: Hochland, 16.Jg. (1918, November), S.122.
[^50]: Eugen Rosenstock, Der Kreuzzug des Sternenbanners, in: Hochland, 16.Jg. (1918, November), S.114.
[^51]: Eugen Rosenstock, Der Kreuzzug des Sternenbanners, in: Hochland, 16.Jg. (1918, November), S.114.
[^52]: Gottfried Hofmann, Chronik.
[^53]: ERH an Rudolf Ehrenberg, 26.III.1947, in: Jenseits all unsres Wissens wohnt Gott. Hans Ehrenberg und Rudolf Ehrenberg zur Erinnerung, hrsg.v. Rudolf Hermeier, Moers: Brendow-Verlag 1987, S.150ff.
[^54]: Eugen Rosenstock, Soziologie I: Die Kräfte der Gemeinschaft, Berlin und Leipzig: Walter de Gruyter 1925, S.33.
[^55]: Eugen Rosenstock-Huessy, Ein Wort von Augustin und eins von Goethe, in: Rosenstock-Huessy, Eugen und Wittig, Joseph, Das Alter der Kirche, Bd.I, neu hrsg. von Fritz Herrenbrück und Michael Gormann-Thelen, Münster: agenda Verlag 1998, S.24.
[^56]: Eugen Rosenstock, Soziologie I: Die Kräfte der Gemeinschaft, Berlin und Leipzig: Walter de Gruyter 1925, S.11/12.
[^57]: Eugen Rosenstock, Soziologie I: Die Kräfte der Gemeinschaft, Berlin und Leipzig: Walter de Gruyter 1925, S.231fff.
[^58]: Eugen Rosenstock-Huessy, Soziologie, 2. Bd.: Die Vollzahl der Zeiten, Stuttgart: W. Kohlhammer Verlag 1958, S.270.
[^59]: Eugen Rosenstock, Soziologie I: Die Kräfte der Gemeinschaft, Berlin und Leipzig: Walter de Gruyter 1925, S.16/17.
[^60]: Eugen Rosenstock, Angewandte Seelenkunde. Eine programmatische Übersetzung (Bücher der deutschen Wirklichkeit), Darmstadt: Roetherverlag 1924, S.55.
[^61]: Eugen Rosenstock,, Soziologie I: Die Kräfte der Gemeinschaft, Berlin und Leipzig: Walter de Gruyter 1925, S.224.
[^62]: Eugen Rosenstock-Huessy, Angewandte Seelenkunde, in: ders., Die Sprache des Menschengeschlechts. Eine leibhaftige Grammatik in vier Teilen, Bd.1, Heidelberg: Verlag Lambert Schneider 1963, S.810.
[^63]: Max Weber, Wissenschaft als Beruf <1919>, in: ders., Wissenschaft als Beruf, 1917/1919. Politik als Beruf, 1919, hrsg.v. Wolfgang J. Mommsen, Wolfgang Schluchter (= MWG I/17), Tübingen: J.C.B.Mohr(Paul Siebeck) 1992, S.81.
[^64]: Eugen Rosenstock,, Soziologie I: Die Kräfte der Gemeinschaft, Berlin und Leipzig: Walter de Gruyter 1925, S.18.
>*Sven Bergmann*

### 5. Nachruf Gerhard Gillhoff

In einem Brief an den Vorstand der Eugen Rosenstock-Huessy Gesellschaft berichtet Gottfried Hofmann von der Beisetzung unseres Mitgliedes Gerhard Gilhoff und gedenkt seines Lebens.

>*18.7.2023*

Liebe Leute,

Gerhard Gillhoff ist am 6. Juli im Alter von 87 Jahren an einem Herzversagen verstorben. Wolfram Wehrenbrecht, ehemaliger Schüler Gillhoffs, und ich haben gestern an der Trauerfeier in der Widukindstadt Enger teilgenommen.

Die Todesanzeige stand unter dem Wort Eugen Rosenstock-Huessys “Das Vergehen des Vergänglichen ist seine Schuld nicht". Gerhard Gillhoff war aus seiner Kirche ausgetreten, die Trauerfeier wurde von einer Bestatterin und den beiden Töchtern aus erster Ehe, Nikola Gillhoff und Dr. Kornelia Gillhoff, gestaltet, die das bewegende Gedicht "Im Grase” von Annette von Droste-Hülshoff vortrugen. Ein Kollege, Dr. Dankwart Vogel, machte in einer Ansprache deutlich, dass sein Freund Gerhard in einer Aufbauschule in Niedersachsen von einem schlesischen Geschichtslehrer auf Rosenstock-Huessys Revolutionsbuch hingewiesen worden sei. Sein Leben lang sei Gerhard dann immer wieder von Rosenstock-Huessys Lehren begleitet worden, besonders als der Bielefelder Professor Jürgen Frese 2001 anfing, sich mit dem Namengeber unserer Gesellschaft zu beschäftigen. Beide zusammen waren an der Gründung der Herforder Akademie, einer privaten Einrichtung der Erwachsenenbildung, maßgeblich beteiligt.

Als Jürgen Frese  leider viel  zu früh 2007 gestorben war, traf sich in Gillhoffs Haus in Enger alle 14 Tage ein Lesekreis, der sich mit Werken unseres Autors (wie Georg Müller zu sagen pflegte) beschäftigte. Auch Wolfram und ich haben an diesem Lesekreis teilgenommen bis zum Ausbruch der Pandemie 2020. Ein anderer Teilnehmer, Dr. Georg Löwen, ein Russlanddeutscher, sprach gestern das Vaterunser in der Übersetzung, die Gillhoff für richtig hielt, nämlich "unser überstoffliches Brot gib uns täglich". Gerhard Gillhoff hatte ihn ausdrücklich darum gebeten.  Am Ende der Trauerfeier wurde das bekannte Paul-Gerhardt-Lied gesungen
"Geh aus mein Herz und suche Freud".

Gillhoff war in zweiter Ehe mit der Chinesin Hongjuan verheiratet, die beiden haben sechsmal China bereist, zweimal hat Gillhoff auch in Herford darüber berichtet.

Ihr wisst sicherlich noch, wie Gillhoff gegen Eckart gekämpft hat, weil er nicht wollte, dass Michael Gormann-Thelen und Lise van der Molen aus unserer Gesellschaft ausgeschlossen wurden. Die beiden, Eckart und Gerhard, konnten nicht zueinander finden. Gerhard erzählte im Lesekreis oft, dass er die Beschäftigung Eckarts mit der Astrologie absolut nicht verstehen könne. Auch Eckarts intensives Studium der Gedichte Paul Celans beurteilte der Germanist und studierte Philosoph Gillhoff als völlig verfehlt.

Ist es nicht erstaunlich, dass mit diesen beiden, Eckart und Gerhard, zwei Männer von uns gegangen sind, die beide seit frühesten Jahren sich mit Rosenstock-Huessy beschäftigt haben und dennoch so sehr gegensätzlicher Meinung waren?!

Ich grüße euch alle herzlich und in alter Verbundenheit!
>*Gottfried Hofmann*

### 6.  Tagungsort und Anmeldung

Die Jahrestagung findet vom 5. bis 7. Oktober 2023 statt im \
Deutsches Literaturarchiv - Schillerhöhe 8-10, 71672 Marbach am Neckar,\
Telefon +49 (0) 7144 / 848-0  \
Beginn ist am 5. Oktober 2023 um 18 Uhr mit dem Abendessen.

**Kosten**\
Die Kosten betragen €130 für Unterkunft und Frühstück. Mittagessen und Abendessen ab €15 zuzüglich Getränke gibt es in nahegelegenen Restaurants. Nach Anmeldung senden wir Ihnen die Speiseangebote.\
Interessenten, die die Tagungskosten nicht in voller Höhe aufbringen können, mögen sich bitte an Thomas Dreessen oder ein Vorstandsmitglied wenden.

**Anmeldung**\
bitte an Thomas Dreessen, Tel. 0(049) 178 719 7009
>*Thomas Dreessen*


### 7. Programm der Jahrestagung

**Natürlich, künstlich oder unbezahlbar?\
Wie wollen wir arbeiten und leben?**

| Donnerstag, 5.10 | 18:00 | Eintreffen |
|               | 18:30 | Abendessen |
|               | 19:30 | Von Bielefeld nach Marbach |
|               | 21:00 | Geselliges Beisammensein |
| Freitag, 6.10 |  8:30 | Frühstück |
|               |  9:30 | Archivführung |
|               | 12:30 | Mittagessen |
|               | 15:00 | Kaffeepause |
|               | 15:30 | Lektüre Arbeit und Leben |
|               | 18:30 | Abendessen |
|               | 20:00 | Arbeit und Leben biographisch |
|               | 21:30 | Geselliges Beisammensein |
| Samstag, 7.10 |  8:00 | Morgenandacht |
|               |  8:30 | Frühstück |
|               |  9:30 | Rückblick – Plenumsgespräch |
|               | 10:45 | Mitgliederversammlung |
|               | 12:30 | Mittagessen |
|               | 13:00 | Abreise |

>*Jürgen Müller*

### 8. Einladung zur ordentlichen Mitgliederversammlung

am 7. Oktober 2023,  10:45 Uhr\
Deutsches Literaturarchiv - Schillerhöhe 8-10, 71672 Marbach am Neckar

TOP 1: Begrüßung der Mitglieder, Feststellung der Beschlussfähigkeit
               und Festlegung der Protokollführung\
TOP 2: Genehmigung des Protokolls der Mitgliederversammlung
	vom 1. Oktober 2022 in  Nümbrecht-Überdorf\
TOP 3: Anträge zur Änderung/Erweiterung der Tagesordnung\
TOP 4: Bericht und Ausblick des 1. Vorsitzenden mit Aussprache\
TOP 5: Kassenbericht für die Geschäftsjahre 2021/2022\
TOP 6: Berichte der Kassenprüfer\
TOP 7: Entlastung des Vorstands für die Geschäftsjahre 2021/2022\
TOP 8: Berichte von Respondeo, vom Eugen Rosenstock-Huessy Fund
               und der Society und von Projekten einzelner Mitglieder\
TOP 9: Wahl eines Wahlleiters/einer Wahlleiterin für die Neuwahl des Vorstands\
TOP 10: Wahl des/der 1. Vorsitzenden\
TOP 11: Wahl des/der stellvertretenden Vorsitzenden\
TOP 12: Wahl des dritten geschäftsführenden Vorstandsmitglieds\
TOP 13: Wahl zweier weiterer Vorstandsmitglieder\
TOP 14: Verschiedenes
>*Jürgen Müller*

### 9. Adressenänderungen

Bitte senden sie eine eventuelle Adressenänderung schriftlich oder per E-mail an Thomas Dreessen (s. u.), er führt die Adressenliste. Alle Mitglieder und Korrespondenten, die diesen Brief mit gewöhnlicher Post bekommen, möchten wir bitten, uns soweit vorhanden, ihre Email-Adresse mitzuteilen.  
>*Thomas Dreessen*

### 10. Hinweis zum Postversand

Der Rundbrief der Eugen Rosenstock-Huessy Gesellschaft wird als Postsendung nur an Mitglieder verschickt. Nicht-Mitglieder erhalten den Rundbrief gegen Erstattung der Druck- und Versandkosten in Höhe von € 20 p.a. Der Versand per e-Mail bleibt unberührt.
>*Thomas Dreessen*




[zum Seitenbeginn](#top)
