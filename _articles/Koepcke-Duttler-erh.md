---
title: "Arnold Köpcke-Duttler über Rosenstock-Huessy"
category: aussage
published: 2022-01-04
name: Koepcke-Duttler
---

„Mit Bonhoeffer lässt sich die Frage nachgehen, wer der Christus der Armen für uns heute ist. Ihr nähert sich auch der das Recht neu denkende Eugen Rosenstock-Huessy – ein Toter, der unser Leben zu bilden vermag mit seinem Sehnen nach Gerechtigkeit – dem Stachel im Fleisch der Welt des positiven Rechts und der in ihr Machtvollen.“
>*Arnold Köpcke-Duttler, Eugen-Rosenstock-Huessys Suche nach einem menschlichen Recht, in: Die Suche nach einem menschlichen Recht. Helmuth James von Moltke 1907-2007 (= Eugen-Rosenstock-Huessy-Gesellschaft; Mitteilungsblätter 2007) - (= stimmstein 12), Körle: Argo Books 2007, S.26.*
