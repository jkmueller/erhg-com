---
title: Jahrestagung 2017
created: 2017
category: jahrestagung
thema: Rosenstock-Huessys Stimme zum Reformationsjubileum
---
![Imshausen]({{ 'assets/images/Imshausen.jpg' | relative_url }}){:.img-right.img-large}
### {{ page.thema }}

Textgrundlage: "Die Verklärung der Arbeit" veröffentlicht in "Das Alter der Kirche", Lambert Schneider, 1928

Datum: 23.-25.6.2017

Ort: Stiftung Adam von Trott, Bebra-Imshausen

---

| Freitag, 23.6 | 18:00 | Eintreffen |
|               | 18:30 | Abendessen |
|               | 19:30 | Die Frage, die uns das Leben stellt |
|               |       | Dritter Teil: Die Arbeit der Kreatur? (Thomas Dreessen) |
|               | 21:30 | Geselliges Beisammensein |
| Samstag, 24.6 |  8:30 | Frühstück |
|               |  9:30 | Zweiter Teil: Luthers Tafel der Werte (Andreas Schreck) |
|               | 11:00 | Kaffeepause |
|               | 11:30 | Abschluß des zweiten Teils: Friedensschluß (Jürgen Müller) |
|               | 12:15 | Pause |
|               | 12:30 | Mittagessen |
|               | 14:30 | Erster Teil: Kirchenlehre und Arbeitswissenschaft (Rudolf Kremers, angefragt) |
|               | 15:30 | Kaffeepause |
|               | 16:00 | Mitgliederversammlung |
|               | 18:30 | Abendessen |
|               | 20:00 | Konzert: Scholastik, Akademik, Argonautik in der Musik (Eckart Wilkens) |
|               | 21:30 | Geselliges Beisammensein |
| Sonntag, 25.6 |  8:00 | Morgenandacht |
|               |  8:30 | Frühstück |
|               | 10:00 | Der neue Sonntag: Sabbat und Sabbatjahr (Eckart Wilkens) |
|               | 11:30 | Abschlußgespräch |
|               | 12:30 | Mittagessen |
|               | 13:00 | Abreise |

---

Anmeldung bitte an Andreas Schreck, Tel. +49 (0)551-28047871; schreckschrauber :at: web.de
