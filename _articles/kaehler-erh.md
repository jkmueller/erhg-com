---
title: "Siegfried A. Kaehler zum Milieu"
category: aussage
published: 2022-01-04
name: Kaehler
---

„Es ist doch zum Lachen, wie an Stelle des „Systems“ nun der Herrenclub als Zauberwort in der Hitlerpropaganda aufgetaucht ist. Ob der „3. Musketier“, als welche Herr Schwarzschild die Trias Schleicher-Planck-Marcks bezeichnet hat, als Trauzeuge in München auftauchen wird, ist noch ungewiß, da Gogarten trauen wird und Rosenstock mit meinem ältesten Bruder als Trauzeuge funktionieren wird, da außerdem die Familie meiner Braut stark pro Hitler ist, der Gastgeber aber Hofkammerpräsident des ehemaligen Kaisers ist, so kann man sich ja kaum ein politisch und sozial bunteres Milieu vorstellen als dieses, in welchem am 11. unsere Hochzeit in Horn bei Waging, Bezirk Traunstein, Ober-Bayern, mit dem Breslauer Präsident von Garnier stattfinden wird.”
>*Siegfried A. Kaehler an Hans Rothfels am 3.09.1932, in: Kaehler, Siegfried A., Briefe 1900 – 1963, hrsg. von Walter Bußmann und Günther Grünthal (Deutsche Geschichtsquellen des 19. und 20. Jahrhunderts; Bd.58), Boppard am Rhein: Harald Boldt Verlag 1993, S.213.*
