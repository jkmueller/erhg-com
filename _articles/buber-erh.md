---
title: "Martin Buber about Rosenstock-Huessy"
category: aussage
published: 2022-01-04
name: Buber
---

„The historical nature of man is the aspect of reality about which we have been basically and emphatically instructed in the epoch of thought beginning with Hegel... Rosenstock-Huessy has concretized this teaching in a living way that no other thinker before him has done.”
>*Martin Buber*
