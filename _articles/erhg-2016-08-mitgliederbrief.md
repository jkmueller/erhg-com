---
title: Mitgliederbrief 2016-08
category: rundbrief
created: 2016-08
summary:
zitat: |
  >In Schellings Weltaltern beginnt es so:
  >
  >Das Vergangene wird gewußt.\
  >Das Gegenwärtige wird erkannt.\
  >Das Zukünftige wird geahnt.\
  >Dem Idealismus sind nur Wissensweisen bekannt. Dieselbe Vernunft ist 3x tätig, als Denkmaschine.
  >
  >Ich widerspreche, als Erbe der Weltkrisis.\
  >Nach Nietzsche, nach Marx, nach Freud, nach Darwin\
  >heißt es: Wir singen, streiten, verheißen und wissen.\
  >Das Besungene wird Vergangenheit.\
  >Die Umstrittenen werden Gegenwart.\
  >Der Verheißene wird die Zukunft.\
  >Das Gewußte muß in die Zeiten neu hinein.\
  >Das also, was Schelling und alle Idealisten von ihrem All wissen wollen, die „Es”-se, das hat seine Zeit abgestreift, es ist da, aber „es” sind die Leichen, Hülsen, Dinge, das Gewesene!\
  >Das Gewußte also muß neu den drei Zeiten einverleibt, es muß wieder lebendig werden.
  >*Eugen Rosenstock-Huessy an Georg Müller, 22. Januar 1955*
---

## Eugen Rosenstock-Huessy Gesellschaft e.V.

{{ page.zitat }}

**Vorstand/board/bestuur:**\
*Dr. Jürgen Müller (Vorsitzender);*\
*Thomas Dreessen; Andreas Schreck; Dr. Eckart Wilkens; Rudolf Kremers*\
*Antwortadresse: Jürgen Müller: Vermeerstraat 17, 5691 ED Son, Niederlande*\
*Tel: 0(031) 499 32 40 59*

***Brief an die Mitglieder August 2016***

**Inhalt**

{{ page.summary }}


[zum Seitenbeginn](#top)
