---
title: Neue Website des ERH Funds live
category: einblick
---

Die [Website](https://erhfund.org "ERH_FUND") des ERH Funds ist erneuert.

Ein Schwerpunkt sind die Vorlesungsaufnahmen inclusive der Transkriptionen. Daneben befinden sich die meisten Schriften und Manuskripte Rosenstock-Huessys im PDF-Scan, die Gritli-Briefe, eine Biographie und die Bibliographie.

<!--more-->

[![ERH Fund site]({{ 'assets/images/erhfund_neue_website_160227.jpg' | relative_url }}){: .img-large}](https://erhfund.org)
