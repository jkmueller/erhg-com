---
title: «stimmstein», das Jahrbuch der Eugen Rosenstock-Huessy Gesellschaft e.V.
created: 2013
category: veroeffentlichung
---

## *Stimmstein 1*

Brendow, Moers 1987\
*Bas Leenman, André Sikojev, Eckart Wilkens*

- [*André Sikojev*, Der „Stimmstein”]({{ '/as-der-stimmstein' | relative_url }})
- *Eckart Wilkens*, Eugen Rosenstock-Huessy
- *Bas Leenman*, Die Tochter
- *Eugen Rosenstock-Huessy*, Ich bin ein unreiner Denker, Reflexionen über das Thema Quatember, Eva und die Folgen der Arbeitsteilung
- „Wo ist das ferne Gesicht?” 8 niederländische Gedichte (deutsch von *Eckart Wilkens*)
  *Juliana Cornelia de Lannoy*
  *Henriette Roland Holst-van der Schalck*
  *Oda Blinder*
  *Judith Herzberg*
- *Freya von Moltke*, Der Kreisauer Kreis
- *Eckart Wilkens*, Atlantis oder: Die Braut der Hölle
- *Wolfgang Ullmann*, Perspektiven für das Verhältnis von Mann und Frau im 3. Jahrtausend
- *Hans Ehrenberg*, Die großen Russen und unsere Bildungsfrage (1929)
- *Günter Brakelmann*, Hans Ehrenberg I
- *Karl-Johann Rese*, Hans Ehrenberg II
- *Fritz Friedmann*, Franz Rosenzweigs „Neues Denken”

## *Stimmstein 2*

Brendow, Moers 1988\
*Bas Leenman, Lise van der Molen, André Sikojev, Eckart Wilkens*

- VORWORT *Eckart Wilkens*, Schwarz oder Weiß
- **Erster Teil**
- WIDERWART 1978 *Eckart Wilkens*, Scheinbeeren
- WARNUNG *Eugen Rosenstock-Huessy*, Pfingsten hat schlechte Zeiten
- REQUIEM *Anna Achmatova*, Requiem, deutsch von *André Sikojev* und *Eckart Wilkens*
- BRIEFE 1945 *Dan Goldsmith*, Eugen Rosenstock-Huessy, Briefe
- ANSPRACHE 1931 *Eugen Rosenstock-Huessy*, „Arbeitslager als Sonntag”
- INDIKATIV 1933 *Eugen Rosenstock-Huessy*, Arbeitslager
- VORTRAG *Andreas Möckel*, Eugen Rosenstock-Huessy in Breslau – zwischen Universität und Erwachsenenbildung
- **Zweiter Teil**
- VERGEGENWÄRTIGUNG *Eckart Wilkens*, Nach Tschernobyl sind wir alle Ökonomen
- GEDICHTE 1975-1976 *Joseph Brodsky*, Anteil Sprache, deutsch von *Eckart Wilkens*
- BETRACHTUNG *Lise van der Molen*, „Gar nicht langsam genug”
- ERZÄHLUNGEN AD HOC: [Im Rosenstock-Huessy Huis, Haarlem.]({{ '/bl-ins-rosenstock-huessy-huis' | relative_url }})\
 [*Piet Blankevoort*]({{ '/pb-ins-rosenstock-huessy-huis' | relative_url }})\
 [*Elly Hartmann*]({{ '/eh-ins-rosenstock-huessy-huis' | relative_url }})\
 [*Lien Leenman-De Pijper*]({{ '/ll-ins-rosenstock-huessy-huis' | relative_url }})\
 [*Elias Voet*]({{ '/ev-ins-rosenstock-huessy-huis' | relative_url }})\
 [*Wim Leenman*]({{ '/wl-wie-bestimmend-ist-rosenstock-fuer-das-rosenstock-huessy-huis' | relative_url }})
- [DARLEGUNGEN *Wolfgang Ullmann*, Die Entdeckung des neuen Denkens]({{ 'wu-die-entdeckung-des-neuen-denkens' | relative_url }})
- KAPITEL (PRÄJEKTIV) *Eugen Rosenstock-Huessy*, Saint Simon – Der erste Soziologe
- SKIZZE *Andreas Möckel*, Eugen Rosenstock-Huessy 1888-1973

## *Stimmstein 3*

**Eugen Rosenstock-Huessy** zum 100. Geburtstag\
Talheimer, Mössingen-Talheim 1990\
*Bas Leenman, Lise van der Molen, Eckart Wilkens*

- *Bas Leenman*, Vorwort
- *Andreas Möckel*, Bemerkung
- *Eugen Rosenstock-Huessy*, Die Interims des Rechts
- *Wolfgang Ullmann*, Sprache – Gesellschaft – Geschichte
- *Harold J. Berman*, Law and History after the World Wars
- *Eugen Rosenstock-Huessy*, God is free
- *Eckart Wilkens*, Eugen Rosenstock-Huessy und Franz Rosenzweig – Der Ton der zweiten Stimme
- *Gertrud Weismantel*, Begegnungen: Eugen Rosenstock-Huessy und Leo Weismantel
- *Eckart Wilkens*, Page Smith
- *Page Smith*, In Memoriam Alan Chadwick 1909-1980
- *Lise van der Molen*, Een raamvertelling waarin wij participeren
- *Raymond Huessy*, Pentecost in Haarlem 1988
- *Piet Blankevoort*, Pinksteren 1988 – Eeuwfeest Eugen Rosenstock-Huessy
- *Bas Leenman*, Die Notwendigkeit des Gedenkens
- *Lise van der Molen*, Literaturbericht
- Mitarbeiter dieses Jahrbuchs

## *Beiheft Stimmstein*

*Eugen Rosenstock-Huessy*: **Hitler** and **Israel** or On Prayer\
Talheimer, Mössingen-Talheim 1992

## *Stimmstein 4*

Eugen Rosenstock-Huessy: ***Mad Economics or Polyglot Peace***\
Talheimer, Mössingen-Talheim 1993\
*Bas Leenman, Andreas Möckel, Lise van der Molen, Eckart Wilkens*

- *Lise van der Molen*, Einleitung
- *Archibald Mac Leish*, Die jungen Soldaten, deutsch von *Erich Fried*
- *Eugen Rosenstock-Huessy*, Die unsichtbare Welt (Fragment)
- *Heinrich Huebschmann* und *Andreas Möckel*, Zwei Briefe zur Vereinigung der beiden deutschen Republiken
- *Andreas Möckel*, Vorbemerkung zu *Mad Economics or Polyglot Peace*
- *Eugen Rosenstock-Huessy*, Mad Economics or Polyglot Peace
- ders., Verrückt gewordene Ökonomie oder Vielsprachiger Friede, deutsch von *Eckart Wilkens*
- *Eckart Wilkens*, Die deutsche Universität zur Friedensfrage: Rosenstock-Huessy 1944
- *Wystan Hugh Auden*, August 1968
- *Mark Huessy*, Kreisau, Rosenstock-Huessy und Friedensdienste
- *Stephan Steinlein*, Das Demokratiekonzept des Kreisauer Kreises
- *Kreisauer Kreis*, Grundsätze für die Neuordnung. Notiz *Helmuth James von Moltkes*
- *Ger van Roon*, Der Kreisauer Kreis zwischen Widerstand und Umbruch
- *Wolfgang Ullmann*, Die Staatsrechtsanforderungen der Nachkriegsgesellschaft und die Kreisauer Reformprogramme
- *Emily Dickinson*, 4 Gedichte
- *Lise van der Molen*, Literaturbericht
- Mitarbeiter dieses Bandes



## *Mitteilungsblätter* der Eugen Rosenstock-Huessy Gesellschaft

Jahrgang 1993, 2. Halbjahr\
*Michael Gormann-Thelen*

- *W. H. Auden*, Aubade/ Morgenständchen, deutsch von *Josef Lakenbrink*
- *Boleslaw Kaluza*, Helmuth James Graf von Moltke, ein Märtyrer Christi
- *Michael Gormann-Thelen*, An die Freunde und Mitglieder der Eugen Rosenstock-Huessy Gesellschaft
- *Eugen Rosenstock-Huessy*, Selbstgespräch des Verfassers, deutsch von *Michael Gormann-Thelen*
- *Ko Vos*, Vorwort/ Nachwort zur niederländischen Ausgabe von Des Christen Zukunft
- *Harold J. Berman*, Vorwort zur Neuausgabe von Out of Revolution
- *Adam Zak* SJ, Eröffnendes Wort zu Hitler and Israel oder Vom Gebet
- *William James*, The Moral Equivalent of War, deutsch von *Elfriede Büchsel*
- *Karlheinz Jackstel*, To change with honour
- *Lise van der Molen*, Bericht zur Zukunft des Stimmsteins
- *Michael Gormann-Thelen*, in memoriam Georg Müller Bio-Bibliographie,

## *Beiheft 2 Stimmstein*

*William James*: The Moral Equivalent of War\
Talheimer, Mössingen-Talheim 1995

## *Stimmstein 5*

Eugen Rosenstock-Huessy: ***Science, Superstition, Education***\
and ***The three Storeys of a University***\
Talheimer, Mössingen-Talheim 2000\
*Michael Gormann-Thelen, Andreas Möckel, Lise van der Molen, Eckart Wilkens*

- *Lise van der Molen*, Einleitung: Das Vorrecht und die Angenehme Zeit der Universität
- *Page Smith*, The Social Nonsciences
- *Eugen Rosenstock-Huessy*, Science, Superstition, Education and The three Storeys of a University
- ders. Wissenschaft, Aberglaube, Erziehnung und Die drei Stockwerke einer Universität, deutsch von *Elfriede Büchsel, Andreas Möckel, Lise van der Molen*
- *Harold J. Berman*, Judaic-Christian versus Pagan Scholarship
- *Miklós Radnóti*, Eklogen 1938-1944, deutsch von *Eckart Wilkens*
- *Bas Leenman*, Ein Brief
- *Heinrich Rombach*, Essay zu Rosenstock-Huessy; Wissenschaft, Aberglaube, Erziehung
- *Wolfgang Ullmann*, Bildungszeit und Bildungsraum an der Schwelle des dritten Milleniums. Zu Rosenstock-Huessys Vorstellungen eines Hochschulcurriculums
- *Helmut Kohlenberger*, Von der babylonischen Gefangenschaft der Universität
- *Gustav Theodor Fechner*, Glaubenssätze von 1865
- Anhang
 Ernst Michel und Eugen Rosenstock-Huessy
 *Eugen Rosenstock-Huessy*, Renovatio. Zur Zwiesprache zwischen Kirche und Welt
 *Eugen Rosenstock-Huessy*, Vom Unscheinbaren. Nachruf auf Ernst Michel am 7. März 1964
- Mitarbeiter dieses Buches


## *Stimmstein 6*

Eugen Rosenstock-Huessy: Das ***Volk Gottes*** in Vergangenheit, Gegenwart, Zukunft\
ARGO Books, Körle 2001\
*Andreas Möckel, Karl Johann Reese*

- [Inhalt]({{ 'stimmstein-6-inhalt' | relative_url }})
- [Editorial]({{ 'stimmstein-6-editorial' | relative_url }})
- [*Eugen Rosenstock-Huessy*, Die Kirche und die Völker]({{ 'text-erh-die-kirche-und-die-voelker-1930' | relative_url }})
- [*Eugen Rosenstock-Huessy*, Das Volk Gottes in Vergangenheit, Gegenwart, Zukunft](https://www.eckartwilkens.org/text-erh-das-volk-gottes/)
- [*Harold Berman,* Recht und Revolution. Die Bildung der westlichen Rechtstradition]({{ 'hb-recht-und-revolution' | relative_url }})
- *Eugen Rosenstock-Huessy* und *Joachim Günter*, Briefwechsel 1953-1954
- [*Eugen Rosenstock-Huessy*, Die Tageszeiten des Glaubens]({{ 'text-erh-des-glaubens-tageszeiten-1951' | relative_url }})
- [*Hans Thieme*, Die Epochen des Rechts]({{ 'ht-die-epochen-des-rechts' | relative_url }})
- [*Jehuda Halevi*, Morgendlicher Dienst]({{ 'jh-morgenlicher-dienst-ost-west' | relative_url }})
- [*Jehuda Halevi*, Zwischen Ost und West, deutsch von *Franz Rosenzweig*]({{ 'jh-morgenlicher-dienst-ost-west' | relative_url }})
- *Freya von Moltke*, Eugen's Adult Years in Germany
- *Jehuda Amichai*, Von Dreien oder Vieren im Zimmer
- *Jehuda Amichai*, Wer sich verläßt auf die Zeit, deutsch von *Lydia* und *Paulus Böhmer*
- [*Ger van Roon* und *Wolfgang Ullmann*, Freya von Moltke zum 90. Geburtstag]({{ 'fvm-zum-90-geburtstag' | relative_url }})
- *Hebe Kohlbrugge*, Hanna Kohlbrugge (1911-1999)
- [*Klaus von Dohnanyi*, Im Land der Verlorenen. Zum Tod von Sabine Leibholz-Bonhoeffer]({{ 'kvd-im-land-der-verlorenen' | relative_url }})
- [*Peter C. Keller*, Glänzend, doch mit editorischen Schwächen, Sebastian Haffner: Geschichte eines Deutschen]({{ 'pk-sebastian-haffner-geschichte-eines-deutschen' | relative_url }})
- [*Lise van der Molen*, Harold J. Berman: Faith and Order]({{ 'lm-berman-faith-and-order-the-reconciliation-of-law-and-religion' | relative_url }})
- [Nachrichten]({{ 'stimmstein-6-nachrichten' | relative_url }})

## *Stimmstein 7*

Wolfgang Ullmann: Ethnizistische Regression gegen die ***Globalität der Menschheitsgeschichte***\
***Der Islam und die Krise der Weltreligionen***\
ARGO Books, Körle 2002\
*Michael Gormann-Thelen, Andreas Möckel, Lise van der Molen, Karl-Johann Rese, Adri Sevenster*

- [Editorial]({{ 'stimmstein-7-editorial' | relative_url }})
- *William Butler Yeats*, The Second Coming
- *Wolfgang Ullmann*, Ethnizistische Regression gegen die Globalität der Menschheitsgeschichte
- *Wolfgang Ullmann*, Der Islam und die Krise der Weltreligionen
- [*Thomas Dreessen*, Der Islam ist eine der Abrahamsreligionen – was unterscheidet uns?]({{ 'td-der-islam-ist-eine-abrahamsreligion' | relative_url }})
- *Wilfred Owen*, The Parable oft he Old Man and the Young
- ders. The next War
- *Adri Sevenster*, Erfahrungen im Dialog
- *Yunus Emre*, Der Herr gab mir ein Herz, deutsch von Zafer Senocak
- ders. Das Kummerrad
- *Dorothee C. von Tippelskirch*, Judaism and Islam from the perspective of Christianity
- [*Hans Ehrenberg*, Das Evangelium der "Anderen"]({{ 'he-evangelium-der-anderen' | relative_url }})
- *Mohammed al-Maghut*, Die Angst des Briefträgers
- *Bassit Ben Hassan*, Diese Nacht wird nicht zu Ende gehen
- *Eugen Rosenstock-Huessy*, The Mission of the Jewish State
- *Ute Freisinger-Hahn*, Die "Ruhe" des Volkes Israel als die "Unruhe" im Leben Eugen Rosenstock-Huessys
- *Peter C. Keller*, Preußen am 20. Juli 1932 – Auf dem Verordnungswege liquidiert
- *Dietmar Kamper*, Der Augenblick des Ketzers
- *Rudolf Hermeier*, In memoriam Dietmar Kamper
- *Michael Gormann-Thelen*, Dietmar Kamper und wir
- *Gottfried Hofmann*, Dr. Gottfried Michaelis +
- Rezensionen:
 *Orhan Pamuk, Rot ist mein Name*
 *Klaus Hemmo: Warum sie Feinde wurden*
 *Said Arnaout und Manfred Budzinski: Gespräche im Libanon*
 *Theodor Eschenburg, Also hören Sie mal zu*
- Nachrichten und Zuschriften

## *Stimmstein 8*

Eugen Rosenstock-Huessy: ***Der Ton der zweiten Stimme***\
ARGO Books, Körle 2003\
*Michael Gormann-Thelen, Andreas Möckel, Lise van der Molen, Karl-Johann Rese, Adri Sevenster*

- Editorial
- *Rolf Hochhuth*, Johann Georg Elser
- *Eugen Rosenstock-Huessy*, Europas Amerika und Amerikas Europa – Lehrjahre einer atlantischen Gemeinschaft
- *Eugen Rosenstock-Huessy*, Künstler und Gemeinde, deutsch von *Eckart Wilkens*
 Nachwort des Übersetzers
- *Eugen Rosenstock-Huessy*, Für Bernhard und Carola Blume
- *Harold J. Berman*, Recht und Logos, deutsch von Rudolf Hermeier
- [*Wolfgang Ullmann*, Die Einheit der Offenbarung und die Dreiheit der Offenbarungsreligionen]({{ 'wu-die-einheit-der-offenbarung' | relative_url }})
- *Karl Heinz Potthast*, Namentlichkeit statt Personenkult
- *Jochen Rieß*, Generationenbrüche – Generationenfolge
- *Lise van der Molen*, A Wholesome Breath
- *Eugen Rosenstock-Huessy*, Revere mysterious Tritnity
- *Ana Blandiana*, Bindungen, deutsch von Joachim Wittstock
- *Harold Stahmer*, Franz Rosenzweig's Letters to Margrit Rosenstock-Huessy, 1917-1922
- *Michael Gormann-Thelen*, Nachschrift
- *Wolfgang Ullmann*, Sociology and Ethics
- Buchbesprechungen:
 *Michael Gormann-Thelen, Die Gritli-Briefe im Netz*
 *Adri Sevenster, Sabbatsrust voor een zondagskind*
 *Hanna Kohlbrugge, De Islam aan de deur*
 *Harald Vocke, Albrecht von Kassel*
 *Wolfgang Ullmann, Wir die Bürger*
 *Karel Capek, Gespräche mit Masaryk*
 *Eugen Rosenstock-Huessy, Die Gesetze des christlichen Zeitalters*
- Nachrichten

## *Stimmstein 9*

Eugen Rosenstock-Huessy: ***Aberglaube, Religion, Häresie***\
ARGO Books, Körle 2004\
*Michael Gormann-Thelen, Andreas Möckel, Lise van der Molen, Karl-Johann Rese, Andreas Schreck, Adri Sevenster*

- Editorial
- *Eugen Rosenstock-Huessy*, Aberglaube, Religion, Häresie
- *Karl Ernst Nipkow*, Weisheit in Kindheit und Alter und die Metaphorik der Bildung – Spurensuche mit Eugen Rosenstock-Huessy
- *Eugen Rosenstock-Huessy*, Der Verrat im 20. Jahrhundert – ein Brief an Margret Boveri
- *Eugen Rosenstock-Huessy*, Briefwechsel mit Rudolf und Maria Ehrenberg
- *Eckart Wilkens*, Dialog als Verständnisform des Werkes von Paul Celan
- *Dietmar Kamper*, Die Unzulänglichkeit der gegenwärtigen akademischen Soziologie im Lichte der Argonautik Eugen Rosenstock-Huessys
- *Wolfgang Ullmann*, Soziologie als Handlungsanweisung einer politischen Physik
- *Otto Kroesen*, Imperatives, Communication, Sustainable Development
- *Georg Meinecke*, Ein Student begegnet Eugen Rosenstock-Huessy in Göttingen
- *Karl-Johann Rese*, Zum 100. Geburtstag von Harald Poelchau
- Rezensionen
 *„Neues” zu Franz Rosenzweig*
 *Frank Surall: Juden und Christen – Toleranz in neuer Perspektive*
 *Hebe Kohlbrugge, Zwei mal zwei ist fünf*
 *Ulrich Schiller, Macht außer Kontrolle, Geheime Weltpolitik von Chruschtschow bis Bush*
 *Marion Kaplan, Der Mut zum Überleben. Jüdische Frauen und ihre Familien in Nazi-Deutschland*
 *Hanna Kohlbrugge, Der einsame Gott des Islam*
Nachrichten und Zuschriften

## *Stimmstein 10*

***Bernd Ternes: „Marginal Man”***\
ARGO Books, Körle 2005\
*Michael Gormann-Thelen, Andreas Möckel, Lise van der Molen, Karl-Johann Rese, Andreas Schreck, Adri Sevenster*

- Editorial
- *Marie-Thérèse Kerschbaumer*, la dame à la licorne (musée de cluny)
- *Heinz Treziak*, Brief an Dietmar Kamper
- *Bernd Ternes*, „Marginal Man” – Dietmar Kamper als Denker jenseits von Differenz und Indifferenz
- *Rudolf Hermeier*, Zur Abwendung Dietmar Kampers von der Lehre Eugen Rosenstock-Huessys
- *Günther W. Riehl*, Körperdenken nach Dietmar Kamper und Sprechdenken nach Eugen Rosenstock-Huessy. Ein Gegensatz?
- *Helmut Kohlenberger*, Kamper – mit Levinas
- *Silvia Breitwieser*, Zwei Konferenzen
- *Eugen Rosenstock-Huessy*: Peter Abaelardus – Nominalismus – Konkordanz – Konzeptualismus – Existenzialismus, deutsch von *William Buchanan, Michael Gormann-Thelen, Andreas Möckel*
- *Freya von Moltke*, 20. Juli 1944 – 20. Juli 2004
- *Wolfgang Ullmann*, Kirchengeschichte – Weltgeschichte – Menschheitsgeschichte
- *Wolfram Bürger*, Begegnung mit Wolfgang Ullmann
- *Heinz Treziak*, Über Ehe und Beruf
- *Karl Heinz Roth*, Ein engagierter Humanist und Anreger – Hans Deichmann (1907-2004)
- *Werner Voggenreiter*, Zur Diskussion
- Rezensionen:
 *Friedrich Wilhelm Marquardt, Was dürfen wir hoffen?*
 *Hans Eckehard Bahr, Erbarmen mit Amerika*
 *Harold J. Berman, Law and Revolution*
 *Marlies Flesch-Thebesius, Zu den Außenseitern gestellt*
 *Klaus Harpprecht, Harald Poelchau*
 *Richard Koch, Zeit vor eurer Zeit*
 *Hermann Jakobs, Theodisk im Frankenreich*
- Nachrichten


## *Stimmstein 11*

***„Hört auf, euch zu fürchten” Wolfgang Ullmann***\
ARGO Books, Körle 2006\
*Michael Gormann-Thelen, Andreas Möckel, Lise van der Molen, Karl-Johann Rese, Andreas Schreck, Adri Sevenster*

- Editorial
- *Jakob Ullmann*, „Hört auf euch zu fürchten!” – Ein Andenken an meinen Vater
- *Esther-Marie Ullmann*, Siebzig Verse für meinen Vater – mehr als halb so alt
- *Wolfgang Ullmann*, Albert Vigoleis Thelen und die Sprache des Totalitarismus als linguistisches, ästhetisches und theologisches Problem
- *Christian Löhr*, Von der Lust an der Erkenntnis aus dem Glauben und der Gabe, diese Lust zu lehren
- *Tine Stein*, Recht und Revolution
- *Steffen Reiche*, Nachruf für Wolfgang Ullmann
- *Matthias Artzt*, Wohin treibt die westliche Zivilisation?
- *Michael Gormann-Thelen*, Wolfgang Ullmann, das heißt Zur Soziologie als Handlungsanweisung einer politischen Physik
- [*Rudolf Kremers*, Die Sprachlehre Eugen Rosenstock-Huessys und die Predigt]({{ 'rk-erh-sprachlehre-predigt' | relative_url }})
- *Eckart Wilkens*, Konrad von Moltke +
- *Wim Leenman*, In memoriam Bas Leenman
- *Bas Leenman*, De naam
- *Andreas Möckel*, Hans Bernd von Haeften
- *Andreas Möckel*, Barbara von Haeften +
- Rezensionen
 *Wolfgang Ullmann, Ordo rerum. Die Thomas Müntzer-Studien*
 *Hans Küng, Der Islam, Geschichte, Gegenwart, Zukunft*
 *Clinton C. Gardner, D-Day and beyond*
- Nachrichten
  [*Andreas Möckel: Peter Sloterdijk zu Eugen Rosenstock-Huessy*]({{ 'am-sloterdijk-zu-erh' | relative_url }})

## *Stimmstein 12*

***Die Suche nach einem menschlichen Recht***\
ARGO Books, Körle 2007\
*Andreas Schreck*

- *Editorial*
- *Arnold Köpcke-Duttler*, Eugen Rosenstock-Huessys Suche nach einem menschlichen Recht
- *Andreas Schreck*, Eugen Rosenstock-Huessy und Kreisauer Kreis – zur Vorgeschichte der „Erzvaterschaft”
- *Knut Martin Stünkel*, Planet und planetarischer Mensch oder Gibt es eine Alternative zur Globalisierung?
- *Christian Löhr*, „Von der Lust an der Erkenntnis aus dem Glauben und der Gabe, diese Lust zu lehren”
- Anhang:
 *Wolfgang Ullmann*, Drei Thesenreihen
 *Wolfram Liebster*, Auf dem Wege zum Ketzerchristentum
 *Rudolf Hermeier*, Peter Sloterdijk und Eugen Rosenstock-Huessy
 *Wim Leenman*, Ter nagedachtenis Piet Blankevoort
- Rezensionen
 *Christoph Richter, Im Kreuz der Wirklichkeit. Die Soziologie der Räume und Zeiten von Eugen Rosenstock-Huessy*
 *Rudolf Hermeier u. a. (Hrsg.), Globale Wirtschaft und humane Gesellschaft, Ost-, West- und Südprobleme*
- Nachrichten
- Autorenverzeichnis

## *Stimmstein 13*

- Editorial
- *Eckart Wilkens*: Gedicht
- *Eugen Rosenstock-Huessy*: Zeitgeist, Mythos, Krieg
- *Rudolf Hermeier*: Zum Gedenken an Harold J. Berman (1918-2007)
- *Eckart Wilkens*: Scholastik, Akademik, Argonautik
- *Wolfdietrich Schmied-Kowarzik*: Von der ‚Tragik’ geschichtlichen Vergessens und der Notwendigkeit, das ‚Rad immer wieder neu zu erfinden’
- *Knut Martin Stünkel*: Wider die voreilige Versöhnung. Die widerwärtige Freundschaft von Franz Rosenzweig und Eugen Rosenstock-Huessy
- [*Rudolf Kremers*: Die Begegnung von Eugen Rosenstock-Huessy und Karl Barth]({{ 'rk-begenung-erh-barth' | relative_url }})
- *Agnes Lieverse-Opdam*: Afscheid van de Carrousel, afsluiting van mijn werk in het onderwijs
- *Agnes Lieverse-Opdam*: Abschied von „De Carrousel” und Abschluß meiner Arbeit an der Schule
- *Eckart Wilkens*: Der Stimmstein als Epoche zwischen Eugen Rosenstock-Huessys und Freya von Moltkes Todesdatum (24.2.1973-1.1.2011) – ein Brief
- *Sabine Leibholz*: Eugen Rosenstock-Huessy und Dietrich Bonhoeffer
- *Fritz Herrenbrück*: „Die Versuchung, an das Wissen zu glauben”
- *Gottfried Hofmann*: Bemerkungen zum Buch von Hartmut von Hentig: „Bewährung. Von der nützlichen Erfahrung, nützlich zu sein” Hanser Verlag München Wien 2006, 108 Seiten
- *Andreas Möckel*: Rezension zum Tagungsband „Kreuz der Wirklichkeit” und „Stern der Erlösung”. Die Glaubensmetaphysik von Eugen Rosenstock-Huessy und Frans Rosenzweig
- Hinweise
- Autorenverzeichnis
