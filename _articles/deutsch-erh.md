---
title: "Karl Deutsch über Rosenstock-Huessy"
category: aussage
published: 2022-01-23
name: Deutsch
---

Eugen Rosenstock-Huessy war nicht nur ein Theoretiker der Revolution, er war auch ein Augenzeuge und ein Überlebender. Wenn er vom 1. Weltkrieg als Teil der Revolution sprach, war er selbst ein Überlebender, der nur knapp davongekommen war. Niemand, der vor Verdun lag, hatte dort eine große Lebenschance. Aber er dachte über das Erlebte nach und wurde so zu einem der großen Geister seiner Zeit.
Rosenstock-Huessy war ein Meister der „Mustererkenntnis“. Er konnte Strukturen in der Geschichte sehen. Es gibt, wie sie vielleicht wissen, einen Farbentest, den die Augenärzte benutzen. Sie zeigen ihnen eine Menge kleiner Punkte.......

Dieses Wahrnehmungsvermögen für Grundfiguren, diese Fähigkeit, ein Signal von einem Geräusch zu unterscheiden, diese Fähigkeit, Strukturen zu erkennen, zeichnet jeden großen Sozialforscher aus. Arnold Toynbee hatte sie, Max Weber hatte sie, Eugen Rosenstock-Huessy hatte sie – und ich denke, er hatte sie in ebenso großem Maße wie die beiden anderen. In bestimmten Augenblicken hatte sie Kant – neben anderen Fähigkeiten; Karl Marx hatte sie. Das bedeutend nun nicht zwangsläufig, daß die Interpretation dieser Strukturen immer Zustimmung finden kann.
>*Karl W. Deutsch, Vorrede, in: Eugen Rosenstock-Huessy, Die europäischen Revolutionen und der Charakter der Nationen, Moers: Brendow Verlag 1987, S.V.:*
