---
title: "Rosenstock-Huessy: Brief an Rudolf Ehrenberg (26-3-1947)"
category: online-text
published: 2022-10-22
org-publ: 1947
---

Lieber Rudi,

Es war schön, dank Deiner Briefe wieder in einen Innenraum des Gesprächs einzutreten. Hoffentlich kommen unsre Pakete weiter an. Es schneit heut - bis Mitte Mai ist hier Winter - und wir frieren beim Gedanken an Europa. Vielleicht ist es ja aber bei Euch schon Frühling.

Um aber auf das Zentrale das Flugzeugporto zu konzentrieren:\
Ich las natürlich gern Dein Lob des Christian Future [^1]. Doch ist es mir mehr der weiße Spitzenkragen meiner Vergangenheit, für ein amerikanisches Publikum plichtgemäß umgelegt. Es entstand nicht direkt am Punkt meines eigenen Wachstums, sondern ich mußte zurückpflöcken und erst einmal mich vorstellen.

Deshalb ist vieles mehr gesagt und erwähnt als bewiesen, und Kampferfahrungen sind als solche gebucht, statt als Einsichten der Liebe und des Glaubens. Das Buch gilt als „bedeutend” und wird von niemandem - außer meinen engsten Freunden - gelesen. Sachlich möchte ich ein Mißverständnis aufklären. Es liegt mir sehr viel daran! Das Kreuz der Wirklichkeit ist kein Schema. Es ist eine Entdeckung, 1924 gemacht, und nach Beatenberg in mein bestes Buch eingeschrieben „Die Kräfte der Gemeinschaft” (das in Deutschland neu erscheinen sollte).[^2] A. Meyer hat es dann in derselben Weise auf die Biologie angewendet wie ich im letzten Kapitel auf Kant. Lies dies mal bitte nach. Es beruht auf der folgenschweren Entdeckung, daß Zeit und Raum von uns in umgekehrter Art erfahren werden und daß die beliebte Parallele „Zeit und Raum” unhaltbar ist.

Es wird nämlich der Raum als Universum, die Zeit als Moment empirisch angetastet, und wir müssen zum Universum Innenräume, zum Moment Zeiträume hinzutun bevor aus diesem ersten Schritt - hier in die Zeit, dort in den Raum - Erfahrung werden kann. Jedes Innen ist kleiner, jeder Zeitraum ist größer als das Erinnern dartuen! Häuser und Zeiten sind die menschlichen Taten, die aus dem panischen Moment und der panischen Welt erfahrbare Tatsachen machen! „Der Fortschritt der Wissenschaft” ist ein geschaffener Zeitraum, „die Erde” eine geschaffene Unterteilung des Raumes.

Daher kommt es nun zweitens, daß Raum und Zeit nur in der Dopplung von Innen und AuBen, Vergangenheit und Zukunft gegeben sind.
Wir entstehen zwischen ihnen. Das ist kein Schema, sondern gilt für jede historische Gruppe.

Drittens, diese Doppelpoligkeit des Kreuzes, ändert die Begriffe Innen und Außen, Vergangenheit und Zukunft aus auf die Gruppe zustrahlenden in von der Gruppe ausstrahlenden Richtungen. Aus Vergangenheit wird rückwärts, aus Zukunft wird vorwärts, aus Innen wird einwärts, aus Außen wird auswärts. Das ist aber revolutionär. Sieh das letzte Kapitel der Soziologie darüber! Denn nun gibt es keine Romantik oder Utopie!

Viertens, nun wird eine Diagnostik kranker und gesunder Gruppen möglich. Ich habe eben an einen Briten in Bonn darüber geschrieben. Wenn's Dich interessiert, schicke ich Dir die Abschrift. Jede Gruppe zeigt Kreuzverkrüpplungen! Und sie heilen einander.

Fünftens, die Sprache dient der Begründung der historischen Zeit- und Raum-Koordinaten. Namen sind eben dies. Das Verhalten von Namen und Pronomina, von Hochsprache und Dialekt, zeigt die ständige Spannung. Darüber, daß die Stämme den Namenshochraum gestalteten, die Ägypter aber den pronominalen Hausraum, habe ich eben wunderschöne Entdeckungen gemacht.

All dies kommt zu seiner Erfüllung in der ersten wissenschaftlichen Grammatik, die nun möglich wird. Endlich kann Grammatik auf empirisch Wahrnehmbares basiert werden. „Du”, „ich", „wir”, „es" sind die vier Richtungen vorwärts, inwärts, rückwärts, auswärts (präject, subject, traject, object). Das ist kein Schema, sondern Gesetz des Sprechens, wie mein „Wort des Menschengeschlechts” darlegt. - Dabei gebe ich Dir übrigens freudigst zu, daß Israel und die Kirche sprachmächtig sind, Laotse und Buddha sprachohnmächtig. Aber das Kreuz ist kein Schema; es entdeckt wie jede Arbeitshypothese dieses: historische Gruppen und ihr Befinden. Deshalb regt mich doch unsre Untreue gegen Patmos auf. Ich habe eben die Korrektur meines Aufsatzes über die Kreatur gelesen (Chicago 1947)[^3]. Die Coprocessio des Geistes in verschiedenen Sprachen (Goethe - Schiller, Nietzsche - Dostojewski) ist das Problem der Zeit.
Dem dient - unvollkommen - das Kapitel Laotse etc.

Dein alter Eugen.

[^1]: Eugen Rosenstock-Huessy: The Christian Future or Modern Mind Outrun. New York 1946 (deutsche Ausgabe nunmehr: Moers 1985)
[^2]: Eugen Rosenstock: Soziologie I. Die Kräfte der Gemeinschaft. Berlin und Leipzig 1925
[^3]: S. nunmehr E. Rosenstock-Huessy: Ja und Nein. Heidelberg 1968, S 107 ff.

[zum Seitenbeginn](#top)
