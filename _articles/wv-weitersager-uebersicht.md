---
title: "Übersicht zu / Overzicht van «Weitersager Mensch»"
category: weitersager
order: 0
published: 2022-01-30
---

#### Weitersager Mensch ist eine Sammlung persönlicher Einführungen in Briefform zu Büchern Rosenstock-Huessys.

*„Wir wollen nichts sein als das kurze Kabelstück, welches den Riss zwischen gestern und morgen gläubig überwindet. Ohne diesen Durchgang durch das enge Tor der Zeit stirbt der Geist.“*
>*Eugen Rosenstock-Huessy in Hochzeit des Krieges und der  Revolution 1920*

Die chronologische Reihenfolge der Schreiber (in deutscher Sprache)

* [Gerhard Gilhoff zu  Die Europäischen Revolutionen]({{ 'gg-weitersager-europ-rev' | relative_url }}) 1933, 1951
* [Gerhard Gilhoff zu  Heilkraft  und Wahrheit]({{ 'gg-weitersager-heilkraft' | relative_url }}) 1952
* [Gerhard Gilhoff zu  Die Sprache des Menschengeschlechts]({{ 'gg-weitersager-menschengeschlecht' | relative_url }}) 1962
* [Wolf-Dieter Grengel zu  Soziologie I: Volk und Amt]({{ 'wg-weitersager-soz1' | relative_url }}) 1956
* [Fritz Herrenbrück zu  Das Alter der Kirche]({{ 'fh-weitersager-alter' | relative_url }}) 1928
* [Gottfried Hofmann zu Das Geheimnis der Universität]({{ 'gh-weitersager-geheimnis' | relative_url }}) 1958
* [Otto Kroesen zu  Judaism despite Christianity]({{ 'ok-weitersager-zu-despite' | relative_url }}) 1969
* [Gudrun Elisabeth Lemm zu Werkstattaussiedlung]({{ 'gl-weitersager-werkstattaussiedlung' | relative_url }}) 1922
* [Gudrun Elisabeth Lemm zu Ja und Nein]({{ 'gl-weitersager-ja-und-nein' | relative_url }}) 1968
* [Andreas Möckel zu  Die Europäischen Revolutionen]({{ 'am-weitersager-europ-rev' | relative_url }}) 1933,1951
* [Wim van der Schee zu Der unbezahlbare Mensch]({{ 'ws-weitersager-unbezahlbar' | relative_url }}) 1955
* [Knut Martin Stünkel zu Der unbezahlbare Mensch]({{ 'ks-weitersager-multiformity' | relative_url }}) 1955
* [Wilmy Verhage zu Die Tochter]({{ 'wv-weitersager-tochter' | relative_url }}), 1958 (aus Die Hochzeit des Krieges und der Revolution)
* [Eckart Wilkens zu Die Soziologie  I und II]({{ 'ew-weitersager-soz' | relative_url }}) 1956
* Eckart Wilkens zu  Frucht der Lippen (1964) und Lazarus und Johannes, Bas Leenman  (nog niet gepubliceerd)


#### Weitersager Mensch: De mens vertelt  verder <br/> een verzameling  persoonlijke inleidingen in briefvorm over boeken van Rosenstock-Huessy

*“We willen alleen maar het korte kabelstuk zijn, dat de breuk tussen gisteren en morgen  gelovig overwint.”*
>*Eugen Rosenstock-Huessy in Die Hochzeit des Krieges und der Revolution (het huwelijk tussen oorlog en revolutie) 1920*

Volgorde in chronologie van de schrijvers in het Nederlands:

* [Wilmy Verhage over De Dochter]({{ 'wv-weitersager-dochters' | relative_url }}),  DieTochter, das Buch Ruth, 1988 Oorspronkelijk verschenen in Die Hochzeit des Krieges und der Revolution 1920
* [Marlouk Alders over De Dochter 1e brief Die Tochter, das Buch Ruth]({{ 'ma-weitersager-dochters' | relative_url }}) 1988
* [Marlouk Alders over De Dochter 2e brief]({{ 'ma-weitersager-dochters2' | relative_url }})
* [Otto Kroesen over Judaism despite Christianity]({{ 'ok-weitersager-naar-despite' | relative_url }}) 1969
* [Otto Kroesen over Toekomst – het Christelijk levensgeheim]({{ 'ok-weitersager-toekomst' | relative_url }}) (een vertaling van Ko Vos van The Christian Future 1946, 1993
* [Wim van der Schee over de Onbetaalbare Mens]({{ 'ws-weitersager-onbetaalbaar' | relative_url }}) 1955
