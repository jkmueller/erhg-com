---
title: Externe Lebensbilder von Eugen Rosenstock-Huessy
category: lebensbild
order: 200
published: 2024-12-25
---
![Eugen Rosenstock-Huessy]({{ 'assets/images/rosenstock_4.jpg' | relative_url }}){:.img-right.img-small}

[ERH Fund: A biographical sketch](http://www.erhfund.org/biography/)

[Raymond Huessy at a meeting in Poland in 2000](http://www.erhfund.org/the-polish-vita-2001/)

[Hans-Christof Kraus auf Neue Deutsche Biographie](https://www.deutsche-biographie.de/pnd11860273X.html)

[Harold Stahmer und Michael Gormann-Thelen](https://jochenteuffel.com/tag/eugen-rosenstock-huessy/)

[Kulturstiftung der deutschen Vertriebenen](https://kulturstiftung.org/biographien/rosenstock-huessy-eugen-2)

[Peter Leithart on "The Soul of Eugen Rosenstock-Huessy"](https://theopolisinstitute.com/leithart_post/the-soul-of-rosenstock-huessy/)

[Stanford Encyclopedia of Philosophy](https://plato.stanford.edu/entries/rosenstock-huessy/)

[Wipf and Stock Publishers](https://wipfandstock.com/author/eugen-rosenstock-huessy/)


**Rosenstock-Huessy auf Wikipedia**
- [deutsch](https://de.wikipedia.org/wiki/Eugen_Rosenstock-Huessy)
- [niederländlisch](https://nl.wikipedia.org/wiki/Eugen_Rosenstock-Huessy)
- [englisch](https://en.wikipedia.org/wiki/Eugen_Rosenstock-Huessy)
- [spanisch](https://es.wikipedia.org/wiki/Eugen_Rosenstock-Huessy)
- [portugisisch](https://pt.wikipedia.org/wiki/Eugen_Rosenstock-Huessy)
- [russisch](https://ru.wikipedia.org/wiki/Розеншток-Хюсси,_Ойген)
- [arabisch](https://ar.wikipedia.org/wiki/يوجين_روزنستوك_هويسي)
