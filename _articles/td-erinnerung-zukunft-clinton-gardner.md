---
title: "Thomas Dreessen: Ohne Erinnerung keine Zukunft: Der Argonaut Clinton C. Gardner"
category: einblick
published: 2024-08-23
landingpage: front
order-lp: 45
---

Ende der 1970ger. Jahre war das Gespräch zwischen USA und der Sowjetunion fast verstummt. Raketenaufstellung stand an in Europa und besonders in Deutschland.
<!--more-->
Da machte sich unser amerikanisches Vorstandsmitglied Clinton C. Gardner (1922 – 2017) auf Leningrad und Moskau zu besuchen. Er wollte Brücken bauen, um beizutragen zur Verhinderung des Atomkrieges. Er wollte mehr: einen Frieden stiften.

Heute will ich Ihnen diesen Menschen vorstellen: Wer er war und was er erreicht hat? Ein Argonautenleben.

1922 wurde Clinton C. Gardner in New York geboren Als Jugendlicher hatte er, angeregt durch seine Eltern am Exeter College Dostojewskis Roman „Die Brüder Karamasow“ kennengelernt. Er identifizierte sich mit Aljosha Karamasow. Schon früh -1937 -gewöhnte er es sich an, ein Tagebuch zu führen. Er nannte es „morning notes“ - „Not only a diary, but ideas about religion and life.”[^2] Seine späteren Bücher erzählen auf dieser Basis die Lernwege eines Menschen auf der Suche nach Gott, nach einem Gott nicht der Fundamentalisten und auch nicht der Philosophen- sondern im Leben der Menschen und Völker.

[^2]: D-Day- War,Russia and Discovery, Seite 20

Ab 1940 besuchte Gardner das Dartmouth College, und lernte dort Eugen Rosenstock-Huessy kennen. Dieser Lehrer faszinierte ihn, denn er redete in neuer Weise von Gott und den Menschen auf unserem Planeten. Es sollte ein lebenslanges Lehrer-Schüler und Freundschaftsverhältnis werden. Seine Schwester heiratete Hans Huessy, den Sohn von Eugen und Margret Rosenstock-Huessy.
Als Eugen Rosenstock-Huessy 1941 mit Unterstützung des US Präsidenten Roosevelt und dessen Frau Eleanor das Camp William James in Vermont ins Leben rief, hat Gardner begeistert mitgemacht und auch das Sekretariat geführt. Es galt ein „Moral equivalent of War“[^3] zu leben um den Krieg zu überwinden. Vorbild dafür waren die freiwilligen Arbeitslager für Bauern, Studenten und Arbeiter die Rosenstock-Huessy mit Helmut James von Moltke und vielen anderen in Schlesien ins Leben gerufen hatte.

[^3]: Der Begriff stammt von William James.

1942 entschloss er sich als Freiwilliger in die Armee einzutreten, um Hitler zu bekämpfen. Am 6. Juni 1944, dem D-Day wurde er fast getötet.\
1945 diente er als Kommandant bei der Auflösung des KZ Buchenwald.\
Zwei Erfahrungen aus dieser Zeit bestimmten sein weiteres Leben. Im Haus des Lagerkommandanten entdeckte er eine Familienbibel in der Kommandant H.Pister sorgfältigst die Namen und wichtigen Momente seiner Familie eingetragen hatte: Geburten, Heiraten, Tode. Nach dieser Entdeckung im Lager war das alte Gottesbild (s.o.) endgültig gestorben. „Ich fühlte, als ob eine tausendjährige Ära zum Ende gekommen war. Christenheit mag eine Zukunft haben, aber nicht als vom Staat gesponsortes all-mächtiges Christentum … “[^4] Er verstand was Rosenstock Huessy erzählt hatte, dass die verschiedenen Teile der Gesellschaft nicht zueinander sprachen.

[^4]: D-Day aao S.78

Die Russen bildeten die größte Gruppe unter den 19.000 lebenden Lagerinsassen. Seine zweite Lebensfrage aus diesen Gesprächen war, dem Archipel Gulag und der russischen Seele nachzuspüren: Er wußte, er würde Russisch lernen.

Zurück in Dartmouth begann er Russisch zu lernen und belegte alle Kurse bei Eugen Rosenstock-Huessy. Außerdem gründete er den Russischen Klub. Dort stellte sich heraus, daß Eugen die russischen Philosophen des Silver Age kannte und schätzte. Gardner hat für Eugen Hans Ehrenbergs Die Russifizierung Europas ins Amerikanische übersetzt[^5]. Eugen riet ihm Solovjov und Berdjajew zu studieren. Rosenstock-Huessy und die beiden Russen nannte Gardner „meine drei Helden“.[^6]
Später riet ihm ERH bei Berdjajew in Paris zu studieren und schrieb ihm ein Empfehlungsschreiben.
Zusammen mit seiner Frau Libby reiste er nach Paris. Leider kam es nicht zur Begegnung. mit Berdjajew, der zwei Tage vor dem geplanten Besuch verstarb.\
In Paris studierte Gardner dann eine Zeit am St. Sergius Institut, das Michael Bulgakow gegründet hat, nachdem Lenin ihn und viele andere 1922 aus Rußland ausgewiesen hatte. Gardners Russisch reichte zwar für alle normalen Gespräche, nicht aber für den dortigen philosophisch - theologisch - soziologischen Jargon. Zutiefst ergriffen hat ihn dort die orthodoxe Liturgie.

[^5]: D-Day… aao Seite 110.94: Hans Ehrenberg hat 1925 in zwei Bänden unter dem Titel: Östliches Christentum – Dokumente I. Politik. II. Philosophie Dokumente der Arbeit russischer Philosophen, Theologen und Soziologen an der Begegnung Ost-West herausgegeben. Am Ende des 1.Bandes steht sein Kommentar: Die Europäisierung Rußlands. Am Ende des 2. Bandes steht sein Kommentar: Die Russifizierung Europas.
[^6]: D-Day uam.

1948 änderte er seinen Plan, in den diplomatischen Dienst der USA zu gehen und folgte am 4. Juli 1948 einem Ruf nach Berlin. Er leitete für ein Jahr die Neue Zeitung für Deutschland, die die USA herausgaben. So erlebten er und seine Frau die Blockade Berlins und die Luftbrücke. In Bezug auf Rußland erinnerte er sich an seine Übersetzung des Ehrenberg Buches und seinen eigenen Artikel: Between East and West- Rediscovering the Gifts oft the Russian Spirit.[^7]

[^7]: D-Dax.. aao 110

Zurück in den USA wurden Clinton und Libby Gardner als Unternehmer tätig. Sie eröffneten erfolgreich einen Handel mit handwerklichen Gegenständen aus aller Welt. Vielleicht eine frühe Form der Eine-Welt-Läden. So kamen sie weit herum auf dem Planeten und hatten freie Zeit für weitere Studien der drei Helden und mehr.\
In Norwich haben die Gardners in der Nähe von Rosenstock-Huessys Haus ihr Haus aufgebaut. Dort gründeten sie Gesprächskreise zum Gottesbild der drei Helden. Er nennt es pan-en-theistisch. Bischof Robinson, Harvey Cox und andere prägten in den 1960ger Jahren die Diskussionen in USA, inspiriert durch Bonhoeffer, Tillich und Rosenstock-Huessy.\
Damals, so stellte Gardner fest, sagten ihm leitende Vertreter der Kirche in Deutschland, daß diese drei für das 3. Jahrtausend wichtig seien.\
Sie bauten den ARGO Verlag mit Freya von Moltke zur Neuausgabe von Büchern und Schriften Eugen Rosenstock-Huessys auf.\
Als Vorstandsmitglied unserer Gesellschaft bat Gardner 1962 Bonhoeffers Schwester Sabine Leibholz-Bonhoeffer über das Verhältnis von Dietrich Bonhoeffer und Eugen Rosenstock-Huessy zu schreiben. Sie tat es und dieser zukunftsweisende Text ist vielen bekannt geworden.[^8]

[^8]: Eugen Rosenstock-Huessy und Dietrich Bonhoeffer-Zeugen der Wende in unserer Zeit, UNIVERSITAS, April 1966; Offensive 92/6 12/2016

Als Ende der 1970ger. Jahre der Kalte Krieg zu einem Heißen zu werden drohte[^9], gaben die Gardners ihren Laden auf. 1979 reiste C. Gardner mit der Frucht seiner Studien, seinem Manuskript Rediscovering Russian Spirituality, zum 1. Besuch nach Moskau und Leningrad.\
Er wollte Vertrauen stiften und werben für Brückenbauen zwischen Bürgern aus USA und Rußland. Citizen Diplomacy sei nötig, wenn die Regierungen nicht miteinander sprechen. Der Mann und sein Manuskript begeisterten. Hunderte Exemplare wurden als Samisdat in der Sowjetunion weitergegeben. Später wurden in 2 Auflagen 6000 Exemplare gedruckt. Begegnungen in USA und der Sowjetunion fanden auf beiden Seiten viele hundert Teilnehmende. 1984 veröffentlichte Gardner die Letters to the Third Millennium in dem er von seinen Erfahrungen berichtete und feststellte, daß Rußland nicht das Problem, sondern Teil der Lösung sei.[^10] Er selber stellt 1988 selbstbewusst fest, dass sie beigetragen haben zu dem Politikwechsel der Regierung Reagan, der ja die UdSSR als „Das Reich des Bösen“ dämonisiert hatte. Der Kalte Krieg war zu Ende. 1989 veröffentlichte Gardner sein Handbook for Citizen Diplomats und widmete es dem großen US Diplomaten George Kennan.\
Sowohl die orthodoxe Kirche als auch Gorbatschow als auch die UCC (United Church of Christ) und viele Amerikaner haben dieses Projekt sehr geschätzt. Gardner spricht von 100.000 US-Amerikanern und 80.000 Russen.

[^9]: Beyond Belief s.136 uö
[^10]: Pp 187; uö

Nach dem Zusammenbruch der UdSSR gründeten Gardner und seine russischen und amerikanischen Freunde die Solovyov Gesellschaft neu, die von Bulgakov 1905 gegründet worden war und 1922 erlag. Schon vorher gab es russische Übersetzungen von Texten Rosenstock-Huessys (Out of Revoution – Autobiography of Western Man uam)[^11].

[^11]: Von Vitalij Mahlin, Alexander Pigalev uam. Zunächst in US Verlag, dann in einem russischen Verlag. – Anm: Bis heute ist die deutsche Übersetzung von Dr. Eckart Wilkens + nicht veröffentlicht!

Nach dem 11. September 2001 gründet er mit Freunden Building Bridges MEUS (Middle East-US) und eröffnete die website Transnat.

2004, also zu einer Zeit, als sichtbar und hörbar die Kräfte des Kalten Krieges wieder überhand gewannen, veröffentlichte Gardner sein eindrückliches Buch D-Day and Beyond in der Hoffnung, dass neue George Kennans auftauchen werden.[^12]

[^12]: Gardner zitiert Kennan in der Einleitung zum Handbuch für Bürger Diplomaten, das er ihm gewidmet hat: „If we insist on demonizing these Soviet leaders – on viewing them as total and incorrigible enemies, consumed only with their fear or hatred of us, and dedicated to nothing other than our destruction – that, in the end, is the way we shall assuredly have them, if for no other reason than that our view of them allows for nothing else, either for them or for us.” (1981)- “For all their historical and ideological differences, these two peoples – the Russians and the Americans – complement each other; together, granted the requisite insight and restraint, they can do more than any other two powers to assure world peace.” (1983)

Sein letztes Buch Beyond Belief- Discovering Christianity‘s new paradigm erschien 2008. Es ist zu großen Teilen die für amerikanische und europäische Hörer bestimmte Fassung des Samisdats von 1981. Darin fasst er die Lehren seiner drei Helden und des russischen Philosophen Michail Bahtin in neue Sprache: Christianity‘s new paradigm. Bahtin nennt es Metalinguistics, Rosenstock-Huessy spricht von Metanomics.\
In seinen Thesen zur Sprache in Beyond Belief fasst Gardner die russische Spiritualität und Rosenstock-Huessy zusammen und beansprucht zurecht damit der Aufgabe Wissenschaft und Religion zu versöhnen zu dienen.[^13]\
Erinnern wir uns seiner als Hoffnung für unsere Zeit: Ein Argonaut![^14]

Audi ut vivamus!
>*Thomas Dreessen*

PS: Websites: Auf der website des [ERH-Funds](https://www.erhfund.org) in USA wird sehr bald Gardners Internetpräsenz nachlesbar sein.

[^13]: Beyond Belief 81f und 198f; cf. Letters… 3ff: Planetary Consciousness
[^14]: Argonautik nennt ERH seine neue Wissenschaft im Unterschied zur Akademik.

***Bücher Gardners:***
- Letters to the Third Millennium -An Experiment in East-West Communication, 1981 Argo Books, ISBN 0-912148-11-X/0-912148-12-8 (pbk.)
- LIFELINES – Quotations from the work of EUGEN ROSENSTOCK-HUESSY hrsg. Von Clinton Gardner, 1988 ARGO Books, ISBN 0-912148-16-0
- BUILDING BRIDGES: US – USSR.  A Handbook for Citizen Diplomats, Norwich Center Books, 1989, ISBN 0-912148-17-9
- D-Day and Beyond – A Memoir of WAR, RUSSIA, AND DISCOVERY , 2004 , Orders@Xlibris.com Xlibris 844-714-8691	ISBN. 978-1-4134-4220-5 ODER 978-1-4134-4219-9
- BEYOND BELIEF – Discovering Christianity’s New Paradigm,2008, White River Press ISBN: 978-1-935052-02-9

>*aus dem [Mitgliederbrief 2024-08]({{ '/erhg-2024-08-mitteilungen' | relative_url }})*
