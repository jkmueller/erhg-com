---
title: Gerhard Gilhoff zu Die Sprache des Menschengeschlechts
category: weitersager
order: 50
---
**Brief über Eugen Rosenstock-Huessy,**\
**Die Sprache des Menschengeschlechts**\
**Eine leibhaftige Grammatik in vier Teilen**\
**Heidelberg, 1963/64**

**Sprache und Zeit in der Schule**

Liebe Frau Plaß,

während wir uns in unserem Lesekreis zu Rosenstock-Huessys Revolutionsbuch trafen, haben wir auch immer über Ihr literaturwissenschaftliches Studium und Ihre pädagogischen Praktika gesprochen. Was heißt es, heute Lehrer zu werden, so fragten wir. Aus den Pisa-Berichten schien ein desolater Zustand unserer Schulen zu sprechen, und Lehrer lassen sich von höchster Stelle als faule Säcke beschimpfen, ohne daß Schüler und Eltern laut protestieren. Sie sind offenbar nur an dem Schein interessiert.

Das positive Abschneiden Finnlands sahen wir hauptsächlich in der Achtung begründet, in der Schule und Lehrer dort stehen.

Was es mit dem Lehren und Lernen in Wahrheit auf sich hat, begründet Rosenstock-Huessy mit einer mir sonst nirgends begegneten Deutlichkeit und Tiefe oder Höhe.

Leider habe ich selbst das Sprachbuch erst nach meiner Berufstätigkeit kennengelernt. So konnte ich die allen gängigen Sprachauffassungen widersprechende leibhaftige Grammatik Rosenstock-Huessys für meinen Unterricht und meine Schüler nicht fruchtbar machen. Ich hatte Rosenstock-Huessy als Historiker kennengelernt und konnte mir nicht vorstellen, daß er für mein wichtigstes Unterrichtsfach eine solche Fundgrube seit 1964 bereithielt. Ich mußte in meinem eigenen Lehrerdasein ohne diese Klarheit über meinen Beruf und mein Fach auskommen. Damit es Ihnen nicht ähnlich geht, schreibe ich Ihnen diesen Brief.

Das Sprachbuch versammelt Texte Rosenstock-Huessys aus fast fünfzig Jahren, und das Motto schließt mit dem Satz: …"when apt and good words are neglected, then also began ill deeds to spring." (1566, Roger Ascham) Seine Übersetzung lautet: „wenn immer die rechten und guten Worte mißachtet werden, dann beginnen jedesmal auch böse Taten aufzuspringen.”

Die leibhaftige Grammatik gliedert sich in vier Teile.

1. Wer spricht? Die Bestimmung der Sprecher\
   (im Sinne von ‚wer spricht wirklich und wirksam?’

2. Wie wird gesprochen?\
   \
   (Beobachtung wirklicher Sprache auf vielen Feldern)\
   \
   Beide Teile werden unter der Überschrift Wer und Wie? in einem frühen Text von 1916 und 1923 zusammengefaßt und methodisch begründet.\
   \
   Der dritte und vierte Teil wurden im ersten Band von 1963 noch angekündigt als

3. Wenn eine neue Sprechweise laut wird\
   \
   Zur Geschichte des Sprechens

4. Wenn eine Ewigkeit verstummt\
   \
   Erinnerungen eines Entewigten

Ent-ewigt kam Rosenstock-Huessy aus dem ersten Weltkrieg zurück, d. h. er nahm die erlebte Geschichte ernst, nicht mehr die Ewigkeit der Theologen. Weil die Deutschen das Ergebnis des ersten Weltkriegs nicht gelten ließen, sagte er ihnen 1919 einen Lügenkaiser voraus.

Im zweiten Band sind beide Teile vertauscht, und es heißt jetzt

Wann hat sich die Sprache gewandelt?

Die Schichten der Erlebnisse

Seit 1902 (also mit vierzehn Jahren) ist Rosenstock-Huessy von einem Hamann-Wort „befallen”: „Vernunft ist Sprache, Logos (λόγος bei Hamann). An diesem Markknochen nage ich und werde mich zu Tode darüber nagen. *Wer nicht in die* *Gebärmutter der Sprache eingeht, welche die Gottesgebärerin der Vernunft ist, ist nicht geschickt zur Geistestaufe einer Kirchen- und Staatsreformation*” (I, 666)^3[1]^, Brief an Herder vom 10. 8. 1784: (Der kursive Teil des Zitats steht nicht in Hamanns Brief, sondern in Zwey Scherflein, J. G. Hamann, Sämtliche Werke hg. Von Josef Nadler, Bd. 3 S. 239 Z. 23 – 25 Deipara hat R.- H. übersetzt: Gottesgebärerin)

 Diese frühe Initiation ist selbst ein Beispiel für die Sprachmacht des Angesprochenwerdens und des lebenslänglichen Antwortenmüssens undkönnens.

Rosenstock-Huessy ist daher offen für die Forschungsergebnisse des Basler Zoologen Adolf Portmann. Dessen Übersicht über die höheren Säugetiere und Vögel stellt fest, daß sich ihre Geburts- und Schlüpfzustände mit den Begriffen Nesthocker und Nestflüchter beschreiben lassen. Während Nestflüchter mit offenen Sinnen und funktionstüchtigem Bewegungsapparat auf die Welt kommen, gilt für Nesthocker das Gegenteil. Portmanns Entdeckung ist es nun, daß Menschen bei ihrer Geburt weder in das eine noch in das andere Muster passen, sondern den Zustand der Nesthocker bei ihrer Geburt schon hinter sich gelassen haben und den der Nestflüchter erst am Ende des ersten Lebensjahres erreichen. In diesem *extrauterinen Frühjahr* (das vorgeburtliche exponentielle Wachstum geht ungebrochen weiter) reifen Menschen in einem sozialen Gremium (vgl. Gebärmutter der Sprache) als mit einem Namen angesprochene im Ansatz zur Person in einer ersten Wiedergeburt heran. Diese Zuwendung im Anlächeln, Ansprechen und bei seinem Namen rufen ist für Menschen lebensnotwendig, wie das Experiment Kaiser Friedrichs II. zeigt, der die Natursprache des Menschen finden wollte, indem er Findelkinder ohne jede körper- oder lippensprachliche Zuwendung aufziehen ließ.

Alle Kinder starben. Offenbar sind Wölfe lebenspendender als Automaten, mit Wölfen haben ausgesetzte Kinder überlebt. Hören und Gehorsam im Sinne der Gesamtheit des sich Ansprechenlassens kommt daher vor dem Sprechen, und **Namen vor Worten vor Zahlen**. Namen, zu denen wir uns bekennen, sagen mehr über uns aus als Worte, und die mehr als Zahlen. Namen dringen umstandslos in alle Sprachen ein, die Namen haben uns die Errungenschaft des lautschriftlichen Alphabets beschert. Rosenstock-Huessy hebt hervor, daß erst die Griechen die Vokale überhaupt und sogar nach Länge und Kürze unterschieden schreiben und die Konsonanten als bloße Mitlaute qualifizieren. Hier wird schon deutlich, daß Rosenstock-Huessy allen Sprachdenkern von Aristoteles' άνθρωπος ζωον λόγον έχον (der Mensch, das Tier, das Sprache hat) über Thomas bis Herder, die Sprache für etwas Natürliches halten, entschieden widerspricht.

*Natur* ist für Rosenstock-Huessy die Welt abzüglich aller Sätze, die in ihr laut werden.

Schule ist nun der institutionelle Ort, für den Sprache grundlegend ist. Aber welche Sprache ist dort wirksam? Was heißt **sprechen mit voller Macht?**

Rosenstock-Huessy unterscheidet sechs Grade des Sprechens und Hörens des Satzes: „Die Afghanen greifen an”

1. *spiel*begleitendes Sprechen eines Kindes
2. *Beispiel*satz in einem Lehrbuch des Afghanischen
3. Gerücht an der Börse, das zum Kurssturz führt, *Lüge*
4. *Einbildung* eines Boten, dem nicht geglaubt wird und der in die Psychiatrie eingewiesen wird
5. Telegramm übermittelt die den Tatsachen entsprechende Nachricht, die aber unwirksam bleibt, weil ihr *kein Glauben* geschenkt wird
6. *Extrablätter, die die Mobilmachung und den Krieg auslösen.*

Der Satz in der sechsten Funktion ist der einzige vollmächtige Satz, der einzige, „aus dem heraus Sprache entspringt, um derentwillen wir als Menschen seit zehntausend Jahren sprechen.” (I, 365) Dieser einzige voll-mächtige Satz ist mit voller Macht gesprochen und gehört worden, und der Satzinhalt hat dem Satz entsprochen.

„Diese Sprache mit voller Macht ist die einzige zuverlässige Quelle jeder Forschung ersten Ranges.” Bei der Lüge wird der Hörer, bei der Illusion wird das Thema und beim in den Wind sprechen wird der Sprecher um seine Macht verkürzt. Vor solchem leeren Sprechen muß die Schule sich schützen. Was geschieht aber normalerweise in unseren Schulen?

Durch Einübung in poetische, literarische und wissenschaftliche Sprache haben Millionen nach Einführung der Schulpflicht mit alexandrinischem Grammatikunterricht jenes „Sprechen verlernt, das einst den vollen Einklang des Logos zwischen Schöpfung, Geschöpf und Schöpfer aus Thema, Hörer und Sprecher hervorrief.” (I, S. 366)

Alltägliche Beispiele für vollmächtiges Sprechen sind das Jawort an den Ehepartner, der Eid vor Gericht, militärische Befehle, der Kaufvertrag früher per Handschlag. Das Kriterium ist immer, daß der Sprecher das Gesagte für und gegen sich gelten läßt. Leeres Sprechen ist dadurch gekennzeichnet, daß der Sprecher, wenn er auf seine Zusage wieder angesprochen wird, antwortet: Mein Name ist Hase, ich weiß von nichts. „Von Sprache sollte man nur reden, wo unser Ruf, und das heißt auch Leben und Ehre, engagiert sind. (I, 731) Diese Kriterien gelten auch für Lehrer und Schüler.

Sie werden fragen, wo in dieser Sprachwelt das poetische Sprechen bleibt, Ihr Literaturstudium, aus dem Sie im Unterricht schöpfen wollen? Sind Dostojewskijs oder Thomas Manns Romane lebensbegleitendes Sprechen für Erwachsene? Was Rosenstock-Huessy von dichterischer Sprache hält, erläutert er in seinem Kapitel über die Musen und Homers Epen, die in dem neuen griechischen Alphabet überliefert sind, das die Aussprache der Tönung und Länge der Vokale erstmalig schriftlich fixiert. Nur so konnte Homer die Musikalität seiner Sprache festhalten und nach ihm alle Dichter. (II, 772 ff.)

Du, Ich, Wir, Es, dies ist die Reihenfolge der Personalpronomina in Rosenstock-Huessys leibhaftiger Grammatik, weil jedes Ich ein anerkennendes Du voraussetzt, „so ist es nicht der als Wort mißverstandene Logos, der uns fesselt, sondern jener Logos, der einst griechisch ‚Gespräch’, gegenseitige Anerkennung bedeutet hat.” Du und Ich zusammen bilden das Wir, für dessen kleinste Einheit Rosenstock-Huessy Schikaneders Libretto zu Mozarts Zauberflöte zitiert: Mann und Weib und Weib und Mann reichen an die Gottheit an. Und erst nach diesem Stiften von Gemeinschaft wenden wir uns den sprachlosen Dingen der Außenwelt, dem Es, zu.

Schließlich entdeckt Rosenstock-Huessy die besondere Rolle der Imperative im wirksamen Sprechen. Dieser Modus beschränkt sich in 200 von Rosenstock-Huessy untersuchten Sprachen auf den endungslosen Wortstamm des Verbs, meint aber immer ein Du. Er ist daher der erste Modus. Er zitiert den Leonidas-Mythos von Thermopylä und ändert Schillers Simonides-Übersetzung ‚Gesetz’ in ‚Geheiß’ um, Rhemata, ρηματα steht bei Herodot. (vgl. I, 703)^4[2]^

„Jeder Imperativ fordert zwei Personen; eine wird hingerissen, das auszurufen, was geschehen müßte; die andere, die sich im Anruf zurechtfindet, nimmt den Anruf auf als mit dem Akt richtig benennbar. So erfahre ich im Anruf des namenlosen Befehls gerade meinen Namen, weil er sich in dem Geheiß aussondert und als mein besonderer Name enthüllt. Die Wirkung des Gebots auf mich ist also eine eingreifendere gegenüber der Wirkung bloßer Sätze mit Tatsachen.” (I, 405) Rosenstock-Huessy verbindet Imperative daher mit leibhaftigen Vorgängen, Konjunktive mit seelischen Stimmungen (Lieder und Lyrik) und Indikative mit Geschichten und Analysen (Zahlen).

„Immer wenn wir sprechen, versichern wir uns des Lebendig-Seins, weil wir einen Mittelpunkt einnehmen, von welchem das Auge rückwärts, vorwärts, nach innen und nach außen blickt. Sprechen heißt, im Mittelpunkt des Kreuzes der Wirklichkeit stehen”:

|               | 3. nach innen |             |
| 1.  rückwärts |               | 2. vorwärts |
|               | 4. nach außen |             |

(I, 320)

1. Wir suchen die *richtigen* Worte zu gebrauchen und sind so mit dem Urbeginn der Menschheit verbunden.
2. Wir zielen auf die Vollendung ihrer Entwicklung hin, weil wir das Erbe der Zeiten in einer *neuen* Art antwortend zusammenfassen.
3. Wir *drücken* die Absichten und Gemütsbewegungen des inneren Menschen *aus* und werden sie damit los.

1. Wir zeigen die äußeren Vorgänge an, die unsere Sinne affizieren, und ruhen nicht, bis sie in wissenschaftlicher Sprache *geklärt* sind.

„Das Kreuz der Wirklichkeit ist um uns allezeit, so lange wir um unsere Erhaltung als Gemeinschaft menschlicher Wesen kämpfen.”. (I, 322)

Diesen Kampf führen Sie täglich in Ihrem Unterricht und mit Ihren Kollegen für eine gute Schule im Interesse Ihrer Schüler.

Für die senkrechte Raum-Kreuzung hält die alexandrinische Grammatik die Begriffe Subjekt und Objekt bereit, für die waagerechte Zeit-Kreuzung schlägt Rosenstock-Huessy die Begriffe Präjekt und Trajekt vor. Diese Begriffe haben allerdings noch nicht „das Feld mitleidlosen Wettbewerbs und rücksichtloser Auslese der Schule betreten.” (I, 555/6) Alles kommt nach Rosenstock-Huessy darauf an, diese vier möglichen Sprechweisen im Wechsel lebendig zu halten. Jede Fixierung auf eine Sprechweise ist mit Unfruchtbarkeit geschlagen.

„Die Wahrheit über den Menschen ist, daß er glücklicherweise nie auch nur davon träumen kann, reinrassiges Subjekt oder ein rein objektives Rädchen in einer Maschine zu werden.” (I, 552) Dieses wäre „Entwürdigung”, jenes „unerlaubte Vergöttlichung”, nämlich sich selbst als prima causa (erste Ursache) oder wirkliches Subjekt verstehen. „Menschen, die zusammen gelebt, eine Erfahrung geteilt haben, alle, die zu und von dem anderen ‚wir’ sagen können … sind durch eine gemeinsame Vergangenheit umgewandelt worden.” (I, 553) Rosenstock-Huessy denkt dabei an die Erfahrung der Deutschen in der Reformation und dem Dreißigjährigen Krieg, oder die der Franzosen zwischen 1789 und 1871.

Diese Erfahrung macht aber auch schon jede Schulklasse und jedes Kollegium, leider nur zu häufig, ohne daraus zu lernen und das Gelernte als Entwicklungschance zu nutzen.

Rosenstock-Huessy unterscheidet vier **Krankheiten** der Sprache in und zwischen Gemeinschaften:

Krieg, Revolution, Degeneration und Anarchie. Für alle vier gibt es fehlendes oder falsches Hören oder Sprechen.

Krieg – *Taubheit* dem Feind gegenüber, man hört nicht auf das, was er sagt.

Revolution – *Niederschreien* der alten Artikulation, alte Gesetze nicht beachten, lächerlich machen.

Degeneration – heuchlerischer *Lippendienst* mit abgewirtschafteten Hochwörtern, z. B. Plappern von Gebeten.

Anarchie – niemand gibt Befehle, *Stummheit* gegenüber dem Freund.

„Eine gesunde Sprachgruppe benutzt herkömmliche Ausdrücke für neue Tatsachen (Wiederholung), neue Ausdrücke für bisher stummgebliebene, d. h. unbestimmte Tatsachen (Artikulation), breitet sich aus zu neuen Menschen (Sprechen) und achtet jeden Sprecher (Hören).”

„*Auf vier Gesundheitswegen muß Sprache leben, wenn sie nicht sterben soll. … Frieden zu schließen, Vertrauen zu geben, die Alten zu ehren und die nächste Generation frei zu machen*. (II, 477/8) Da das häufiger mißlingt als es gelingt, muß sich das Sprachdenken auf die Hochzeiten vollmächtigen Sprechens stützen, kann aber ebenso wie die Medizin aus dem Studium der Krankheiten viel über die Gesundheit lernen.

Welche dieser Krankheiten beobachten Sie in Ihrer Schule?

Taub gegen provokative Schülerfragen oder erschließende Lehrerfragen werden Lehrer und Schüler, wenn sie sich gegenseitig als Feinde in einem Kleinkrieg bekämpfen und so jeden Unterricht zerstören. – Dem liegt häufig auf beiden Seiten bloßer Lippendienst zugrunde, d. h. rücksichtsloses Durchsetzen abstrakter Ziele auf der einen und nach dem Munde reden auf der anderen Seite.

Lehrer müssen Schülern sagen, was wichtig ist und was zu tun ist. Unterricht darf nicht im Lärmpegel der Klasse verpuffen. – Die stumme Feigheit vor dem Freund ist für den Augenblick bequem, weil sie sich das Maßnehmen an den Anforderungen und Ansprüchen des außerschulischen Lebens erspart, führt aber zu berechtigter Verachtung, wenn die Schüler später vor diesen Anforderungen versagen.

Peter Handke hat in *Die Innenwelt der Außenwelt der Innenwelt* den Mißbrauch von scheinbar zwingender Sprache in Alternativen und besonders die Wirklichkeit schaffende Kraft von Imperativen dargestellt.


Einige Alternativen in der indirekten Rede

- TATEN seien die Alternativen zu WORTEN
- so wie ICH die Alternative zu IHM sei
- oder wie wir die Alternative zur UNTERDRÜCKUNG seien
- oder wie DU die Alternative zur LEEREN WOHNUNG seist:

- WORTE wieder, sagt man, seien die Alternative zum DENKEN
- so wie VERHANDLUNGEN die Alternative zum KRIEG seien
- oder wie der WIRKLICHKEITSSINN die Alternative zum UNVERBINDLICHEN SPIEL sei
- oder wie die SCHÄDLINGSBEKÄMPFUNG die Alternative zum KARTOFFELKÄFER sei:

- DAS DENKEN wieder soll, berichtet man, die Alternative zu den TATEN sein
- So wie DIE STICKIGE LUFT eine Alternative zu denen sein soll, DIE FÜR REINE LUFT SORGEN
- oder wie DIE ANARCHIE die Alternative zum GUTEN WILLEN ALLER BETEILIGTEN sein soll
- oder wie die Alternative zum KLEINEN FINGER GAR NICHTS sein soll:


Die Alternativen, könnte man also sagen, stellten zwei Worte zur Wahl /
die Alternativen bestünden aus Worten /
die Worte behaupteten, schon als Worte, was SEIN SOLLE /
die Alternativen stellten zwei Worte zur Wahl, von denen eines SEIN SOLLE, damit das andre NICHT SEI /
die Alternativen stellten sich als Worte zur Wahl, die dadurch, daß Worte, schon als Worte, behaupteten, was SEIN SOLLE, schon zwischen zwei Worten keine Wahl mehr zuließen /

wenn WORTE die Alternative zum DENKEN wären, wie die Alternativen, die Worte seien, behaupteten, weil sie Worte seien (und Worte *behaupteten*), --- so wären die Alternativen, die, schon als Worte, behaupteten, was SEIN SOLLE, die geeignete Schädlingsbekämpfung der GEDANKEN:

PARIER oder KREPIER!

^5[3]^

Vor die letzte Alternative der Imperative wird heute kaum noch ein Schüler gestellt, aber paß dich an oder geh ab scheint so selbstverständlich zu gelten, daß es Schülern schwer auszutreiben ist, dem Lehrer nach dem Mund zu reden. Unterricht wird so jedenfalls unfruchtbar und *frei machen* gelingt nicht. Der Schüler wird nicht los, was ihn umtreibt, weil er dem Lehrer keine Fragen stellt, der Lehrer muß sich bei leerem Echo langweilen, weil sein Wissen nicht herausgefordert wird, wenn es z. B. um das Verstehen eines Textes wie den von Handke geht. Wer nicht spätestens 18-jährigen Schülern jeden deutschen Text vorzulegen wagt, bringt sie um die Chance, ihr Textverständnis maximal zu entwickeln, und schließt sie teilweise aus der deutschen Sprachgemeinschaft aus. Dafür halte ich zunächst zeitgenössische Texte für geeignet, dann aber auch die großen alten. In Gruppen muß man nur solche Texte lesen, die zu ihrem Verständnis die gemeinsame Anstrengung erfordern und alle Beteiligten bereichern.

„Nur das Steckenbleiben im Mißverstand ist abscheulich; der Durchgang durch die Mißverständnisse ist hingegen eine Bedingung der Nachfolge, das heißt des Lehrererfolges. Ja, der Grad der Wichtigkeit einer Lehre hängt mit dem Grad ihrer Mißverständlichkeit zusammen.” (II, 387)

Rosenstock-Huessy schreibt in theologos, in philosophos, in tyrannos (gegen Theologen, gegen Philosophen, gegen Tyrannen ist eine bedenkenswerte Reihung) insofern ihre Vertreter die Dogmen der heidnischen Antike für verbindlich und gültig halten, wie sie im alexandrinischen Museion gebündelt und in den sieben freien Künsten tradiert worden sind:

Der Mensch steht als autonomes Ich in theoretischer Haltung (als Zuschauer also) der natürlichen Welt gegenüber, ordnet die Rhapsodie der Sinneseindrücke mit möglichst abstrakten und streng definierten Begriffen und hat von Natur aus die Sprache als beliebig verfügbares Werkzeug für die Kommunikation von Ergebnissen. „Da die Aufklärung von Robinson Crusoe ausging, …, der im Singular auf die Natur mit seinem natürlichen Geiste starre und sie anschaue, hat sich die Wahnsinnstheorie behauptet, es erfinde der Mensch die Sprache, um seine Gedanken auszudrücken.” (I, 701)

Das Ideal der Objektivität, der Logizität der möglichst weitgehenden Mathematisierung taugt nur für das tote Es im äußeren Raum, das nur besprochen werden kann. Ihm ist das schwächste und blutleere Sprechen im Indikativ des Präsens für immergültige Wahrheiten oder des Präteritums in der ersten und dritten Person Singular oder ersatzweise im Plural majestatis eigen, was in dieser Tradition aber als der einzig wahre und erstrebenswerte Schreib- und Sprechstil gilt.

Zu des „ewigen Primaners Decartes” (I, 644) Cogito, ergo sum muß es die Vorgeschichte eines mächtigen sein Leben verwandelnden Imperativs cogita! gegeben haben, der ihn zu einem dreißigjährigen Denkerleben bestimmt hat, statt das Leben als adeliger Müßiggänger weiterzuführen.

Und tatsächlich erzählt Descartes am Anfang des Discours de la Méthode, (2.Teil, 4. Absatz) wie er sich gezwungen sah, seine Selbstführung zu übernehmen, weil seine Lehrer mit *verschiedenen* Zungen geredet hatten. Auch für ihn ist also das Hören primär. Er hätte sich von **einem** Lehrer leiten lassen, "si je n'avais jamais eu qu'un seul maître ou que n'eusse point su les différences qui ont été de tout temps entre les opinions des plus doctes (wenn ich immer nur einen Lehrer gehabt hätte oder wenn ich nichts von den Unterschieden gewußt hätte, die zu allen Zeiten zwischen den Meinungen der höchsten Gelehrten bestanden S.24) und folglich "je me trouvais comme contraint d'entreprendre moi-même de me conduire" (fand ich mich gezwungen, es selbst zu unternehmen, mich zu führen. Descartes, Discours de la Méthode Classiques Larousse o. J. S. 25, Übersetzung G. G.)

„Kant hat, ohne Widerspruch zu erfahren, behauptet: Der Mensch beginne mit Anschauungen … gehe zu Begriffen fort und ende mit Ideen. … Jedermanns Erfahrung wird hier ins Gesicht geschlagen. Kein Mensch beginnt mit Anschauung. Wir beginnen mit Gehorsam und Antwort. Das Kind schaut sogar seine Mutter nicht an, sondern es erwidert das Lächeln der Mutter mit seinem Lächeln. Der Ursprung der Sprache erfolgt aus Gegenseitigkeit. … In seinem Lächeln wurde es zum Mitglied der glaubenden Geistesgemeinschaft … Denn Lächeln ist Entwaffnung.” (I, 701)

Rosenstock-Huessy erklärt die Hochschätzung von Logik, Mathematik, Erkenntnistheorie und ewigen Wahrheiten bei Platon mit dem Verfall öffentlicher Rede im zeitgenössischen Athen, wo Sophisten sich erboten, die schwächere Sache vor Gericht rhetorisch zur stärkeren machen zu können (vgl. Anfang der Apologie des Sokrates) Dieser Abwehrkampf paßt für das Athen nach dem peloponnesischen Krieg, für den auch Thukydides den Sprachverfall eindrucksvoll bezeugt.

Rosenstock-Huessy sieht anders als Platon einen Ausweg nicht von der Sprache weg, sondern ganz in sie hinein.

Die polemische Schärfe darf nicht darüber hinwegtäuschen, daß Rosenstock-Huessy das Quadrivium und die neuzeitliche Weiterentwicklung in Mathematik und Naturwissenschaft zur Erschließung der äußeren Natur im Raum hochschätzt. Wenn diese Methoden aber auf die menschliche Welt in Geschichte, Sprache und Seele übertragen werden, weil sie für die einzig erfolgreichen gehalten werden, dann ist ein solcher Übergriff für ihn ein Arbeiten mit untauglichen Mitteln am untauglichen Objekt, das es in dieser Welt des Triviums nur als antiquarischen Überrest gibt. Für die Erschließung der trivialen Welt der Zeit hält Rosenstock-Huessy seine grammatische Methode für ebenso grundlegend wie die Mathematik zur Erschließung der äußeren Welt des Es. Geschichte, Pädagogik, Andragogik und Seelenkunde bedürfen also einer neuen „Grundwissenschaft” (II, 370).

Positiv beruft Rosenstock-Huessy sich auf Heraklits Fragmente 1, 2, 19 u. 50. „Denn aus uralten Sätzen steigt die Forderung, die wir heut erheben, bereits majestätisch empor. Herakleites erhebt den Logos als die Einheit von Hören und Sprechen über den Hörer und den Sprecher. Sprache ist für ihn ein sozialer Vorgang, der den einen zum Aussprechen, den anderen zum Miteintreten auf den Ausspruch zwingt. Es ist also der gleiche Mißbrauch für Herakleites, etwas mitdenkend auszusprechen, was nicht gesagt zu werden hat, wie etwas nicht mit in Kraft zu setzen, was man mit dem Ohr hört.” (I, 343)

Wie schon zitiert, beruft Rosenstock-Huessy sich immer wieder auf Hamann, und die Menge der Zitate aus Goethes Werk ist unübersehbar.

Der andere große Traditionsstrang liegt in der Bibel vor, hier besonders im Johannes-Evangelium und der Offenbarung des Johannes. Für sein Verständnis der Zeit beruft er sich auf Augustinus. (II, 368 ff.)

Zeit erschließt sich nach Rosenstock-Huessy durch Zeitspannen unter der Herrschaft von Namen, die alten Gehorsam aufkündigen und neuen verlangen. Epochen werden also angesagt und abgesagt. (vgl. Europ. Revol. 1987 S. XV) Menschen können sich einem solchen Geheiß stellen oder sich ihm versagen. Dieses Ja oder Nein als gesprochenes und wiederum gehörtes gibt es in der Natur nicht. (vgl. Luhmann, Ja und Nein haben keine Entsprechung in der Natur)

„Die Zeit verkümmert … unter den Händen sowohl der Theologen (die sich auf die Ewigkeit gut verstehen) wie der Philosophen (die es mit dem Raum haben).” (II, 370) Das Sprechen mit Vollmacht verwandelt Krieg in Frieden, Menschen in Mitmenschen, tote Gegenstände in lebendige Gegenwart. Die höhere Grammatik meistert die Zeiten durch Vergegenwärtigen, das heißt durch unser eigenes Eingehen in die Zeit und in den Zeitablauf. „Das kommende Organon muß uns also Sprache und Zeit neu zur Verfügung stellen, so wie uns die Zahl den Raum unterworfen hat.” (II, 371)

„Wir sind mehr Mensch, wenn wir mit unseren Mitmenschen in der höchsten Potenz sprechen, als wenn wir ‚denken’ oder ‚arbeiten’.” (II, 373)

Die „im Gespräch … in uns erregten Kräfte … sind nichts Mystisches. Sie sind die Kräfte, kraft derer wir uns Zeit nehmen! Ohne Glaube bleibt der Mensch ein Stummel seiner selbst; denn die Zukunft bleibt ihm verschlossen. Ohne Hoffnung ist der Mensch von seinen Wurzeln in der Vergangenheit abgeschnitten, denn sie erregten dann in ihm keine Wünsche mehr. Und ohne Liebe ist dem Menschen sein Gegenüber ein bloßer weltlicher Gegenstand, denn er kann sich nicht zur lebendigen Gegenwart in der Einheit mit seinem Gegenüber bekennen. Der Glaube nimmt sich Zeit nach vorn, die Hoffnung Zeit nach rückwärts, die Liebe umarmt den bloßen Gegenstand, damit er ihre Gegenwart teile.

Damit werden Glaube, Liebe, Hoffnung aus „Sittlichen Tugenden” im Einzelnen zu wissenschaftlich greifbaren Prozessen in der Gesellschaft. Sie begründen unser Verhältnis zur Zeit der Zukunft, der Vergangenheit und der Gegenwart." (II, 373)

Augustinus erörtert „die Wahrheit über die zeitschaffenden Kräfte für den denkbar kleinsten Fall, … der Gruppe von Zweien” (II, 375) und gewinnt damit ein Modell der gesamten geschichtlichen Ordnung. Er steht vor der Frage, wie er als Dreiunddreißigjähriger für seinen unehelichen sechzehnjährigen Sohn, mit dem zusammen er sich soeben hat taufen lassen, Lehrer werden kann. Die Lehre war ursprünglich in der Kirche das Amt des Paten.

Zur ersten „Bedingung … zum Dasein einer Wissenschaft gehört: um der Lehre willen einen Menschen so zu begeistern, daß er zu ihrer Übertragung an einen Nachfolger sich gezwungen sieht, und um ihres Erlernens willen einen anderen so zu begeistern, daß er ihrer Übertragung von einem Vorgänger sich unterzieht.” (II, 390)

„Zeithergabe ist die Bedingung der Lehre. Wir müssen uns Zeit füreinander nehmen, wenn wir voneinander lernen sollen. Aber die Zeiten, welche Lehrer und Schüler hergeben, sind entgegengesetzter Qualität. … Der Lehrende spricht zu einem Schüler, weil … er … ihn … zu überleben vermag. Die sonst im Lehrenden zu Grabe getragene Wahrheit wir dadurch … gegen den Tod ihres Trägers geschützt. … Die Wahrheit wird vorwärtsgetrieben über den Lebenden hinaus. Sie wird also sozusagen ‚vervorwärtigt’.

Der Lernende umgekehrt gewinnt Zutritt zu der im Lehrenden ihm entgegentretenden Vergangenheit. Er wird dank der Belehrung vor sein eigenes Leben zurückgeführt. Er wird ‚verrückwärtigt’. (II, 391/2) Damit fallen die Schranken von Geburt und Tod. „Durch den Lehrer kommt alles Leben vor der Geburt des Schülers auf den Schüler zu; jeder Lehrer verkörpert nicht etwa die eigene Zeit, sondern alles Leben von Adam und Olims Zeiten her für den Lernenden. Der Lernende lernt von dem, der lehrt, nicht, was dieser denkt, sondern was dieser weiß. Und das Wissen ist die Summe aller bis auf den heutigen Tag gesehenen und eingesehenen Tatsachen. … Umgekehrt verkörpert der Schüler für den Lehrer nicht etwa die eigene Jugend, sondern die gesamte Nachwelt bis zum jüngsten Tage. .. Der gegenwärtige Schüler ist ihm sein Brückenkopf in alle Zeiten jenseits des eigenen Todes.” (II, 392/3)

Diesen zentralen Vorgang alles Lehrens und Lernens sollten Lehrer, Eltern und Schüler vor Augen und im Ohr haben. Vielleicht können Sie, liebe Frau Plaß, an Ihrer Schule dafür sorgen, wenigstens Ihren Unterricht so geben und auf Stunden des Gelingens hoffen. Wissen kann kein Lehrer, ob er einen solchen Erfolg hatte, weil er sich viel später einstellen kann. Er kann ihn dem Schüler nur anvertrauen und zutrauen. „Beim Lehren und Lernen unterziehen sich die Partner einem Prozeß gegenseitiger Einwirkung. *Wir werden von unseren zwischenzeitlichen Beschränkungen befreit, indem der Lehrer der Zukunft opfert und der Schüler der Vergangenheit.”* (II, 425) Die Schranken sind die Geburt des Schülers und der Tod des Lehrers.

Mit diesem Verständnis von Augustinus' Dialog De magistro widerspricht Rosenstock-Huessy der gängigen Deutung als schlechter Nachahmung Platos, so schon Erasmus 1527 (vgl. Anm. 1 II, 396) In diesem Werk ist Augustinus nicht mehr Philosoph und noch nicht Theologe.

Augustinus dringt „um der Liebe zu seinem Sohne willen in die Urzelle zurück, in der wir Menschen sprachfähig werden. Wir werden sprachfähig, du und ich, wenn wir uns einem gemeinsamen Geist unterstellen, … wenn wir der eigenen Zeit rückhaltlos vergessen und uns verhalten, als hätten wir *unendlich lange Zeit.* … Dem einzelnen Menschen fehlt es an Zeit. Er lebt ja nicht ewig. Alle Gruppenbildung wird nur durch das Paradox möglich, daß wir zeitdarbenden Menschen gerade unseres Zeitmangels vergessen und so handeln, als hätten wir endlos Zeit füreinander. … Marcion^6[4]^, Harnack^7[5]^ Bultmann^8[6]^, Herbert Braun^9[7]^ machen sich einen Menschen zurecht, der nur Zeitgenossen, also Raummitmenschen habe. So begreifen sie nie, weshalb Jesus in der Wüste dieser billigen Mitmenschlichkeit entsagt hat.” (II, 397)

Rosenstock-Huessy versteht sich als Laie, der 1958 die theologische Ehrendoktorwürde der evangelischen Fakultät der Universität Münster angenommen hat.

Mit Albert Schweitzers Leben-Jesu-Forschung kann er ebensowenig anfangen wie mit Karl Barths Logos-Christologie, weil er Leben, Lehre und Nachleben des Herren der christlichen Epoche nicht trennen will.

Rosenstock-Huessy gibt eine überraschende, alles Mystische vermeidende Deutung der Auferstehung in einer Laienpredigt zum Ostersonntag. Dabei verwendet er das Wort *Jungfrauengeburt* ebenso metaphorisch wie die Worte *Wahrheit, Kraft, Licht, Leben, lebendiges Wasser* bei Johannes stehen:

„Sehen Sie auf das Verhältnis von Mutter und Sohn noch einmal. Jesus ist die Jungfrau, die die Apostel aus sich geboren hat. Die Jungfrauengeburt der Maria, ihres eigenen Sohnes, ist eine Projektion dieser Leistung des Herren als Mutter seiner Apostel. Er hat verzichtet, genau wie der Samariter … sich selbst zu ernennen. Er hat sich auf die Apostel verlassen. Sie haben ihn ernannt. Sie sind sein Leib, als er selbst leiblich von ihnen ging. In ihnen ist er auferstanden, und nichts würden wir je von der Jungfrauengeburt zwischen ihm und seiner Mutter hören oder wissen, wenn die Apostel nicht sich von ihm hätten in die Welt hineingebären lassen als Träger seines Geistes und die Träger seines Amtes und die Träger seiner Berufung.” (I, 443) Unsere Zeitrechnung datiert also nicht vom Stall in Bethlehem, sondern vom ersten Ostertag, erst da nennen die Jünger und Apostel Jesus von Nazareth Christus, d. h. mit seinem geschichtsmächtigen Namen.

„In der Ostkirche erschallt am Ostersonntag der Ruf aller Gläubigen, Priester und Laien: Christus ist auferstanden, er ist wahrhaftig auferstanden. In der dritten Person wird hier von Gott gesprochen. Die Gläubigen versichern ihm, daß er in ihren Herzen auferstanden ist.” (I, 440) So konnte er zu dem Namen über allen Namen werden.

Damit zeigt Rosenstock-Huessy die geschichtliche Dimension des Modells, das Augustinus im kleinen entwickelt.

Auferstehung des Fleisches heißt also nicht Wiederaufführung und Weiterführung von Lebensgeschichten und biologische Rekonstruktion individueller Leiber.

Die vergehen zu der objektiven Unsterblichkeit Whiteheads und stellen Bausteine für neues Werden biologischen Lebens bereit, am unmittelbarsten in Tibet, wo die Leichen rituell den Geiern geopfert werden. Auferstehung findet im Erinnern der Weiterlebenden und im Weitersagen an die junge Generation statt, und zwar in der übernatürlichen Sprachwelt; so hat Rosenstock-Huessy sich von der gesamten europäischen Tradition aus altgriechischer, altisraelicher und christlicher Wurzel ansprechen lassen und ermöglicht uns, darauf zu antworten.

Was heißt nun, nach Christi Geburt leben? Metanomik (von νόμος Gesetz) als Gegenbegriff zu Metaphysik macht uns frei von den Gesetzen der römischen Vogelschau, des jüdischen Ritus' wie von allen älteren rituellen Tabus. Wer also sein Sprechen und Handeln mit astrologischen, magischen, zahlenmystischen Praktiken oder Kartenmischungen gemein macht, d. h. sich affektiv (anmachend) oder praktisch davon beeinflussen läßt, fällt hinter dieses Datum zurück. Ebensowenig ist auch Wissenschaftsgläubigkeit mit der Freiheit eines Christenmenschen vereinbar (wenn z. B. eine Versicherungsgesellschaft wegen eines Genombefunds einen Vertrag ablehnt). Als wiedergeborene Personen mit Namen aus dem Sprachenschoß, können wir der Göttesgebärerin der Vernunft (von vernehmen) vertrauen nach Goethes Wort aus dem Divan:

*Buch des Parsen*

*"Schwerer Dienste tägliche Bewahrung,*

*Sonst bedarf es keiner Offenbarung."*

Hamb. Ausg. Bd. 2 1958 S.105

Dies entspricht Rosenstock-Huessys Auffassung vom Be-**ruf** der Menschen in der Welt, nach Kräften zu ihrer Weiterschöpfung beizutragen. Aus der Art also, wie wir die Forderung des Tages meistern, ohne Verzagtheit aber auch ohne selbstverständliche Erfolgserwartung, zeigt sich, wer wir sind.

Das dritte Jahrtausend nennt Rosenstock-Huessy das samaritanische Zeitalter, in dem die Durchdringung des Alltags durch das alt- und neutestamentliche Liebesgebot ansteht. Bei Matthäus 22, 37 zitiert Jesus, als er nach dem wichtigsten Gebot gefragt wird, 5. Moses 6, 5. und 3. Moses 19, 18. Goethes Wort für Gott ist ‚Höheres und Höchstes’, es ist das, dem ich mein Leben widme, dem ich Lebenszeit opfere, und das soll ich nach 5. Mose 6, 5 liebend von ganzem Herzen, von ganzer Seele, von allem Vermögen tun. (Wo Luther ‚von’ sagt, höre ich ‚mit’)

Diese Kraft wünsche ich Ihnen, liebe Frau Plaß, für Ihren Beruf in der Schule. Die beste Unterstützung dabei ist nach unserer Erfahrung ein Lesekreis. Vielleicht haben Sie in Ihren frühen Berufsjahren das Glück, aus dem Kollegium und der Oberstufe Ihrer Schule je zwei bis vier Mitstreiter zu finden, mit denen Sie sich die 1700 Seiten in zwei Jahren erschließen. In dem Lesekreis der Herforder Akademie ist uns dieser lange Weg bei anhaltender Entdeckerfreude bis zu der umfassenden Geschichte des Sprechens und Schreibens bei den Stämmen, im Ägypten der Pharaonen, mit den Musen Homers, bei den Propheten Israels und schließlich in unserem eigenen Zeitalter kurz erschienen.

Dieses reiche Erbe gilt es anzutreten und die junge Generation in ihm aus der Erfahrung des eigenen Freiwerdens frei zu machen.

Ihr G. Gillhoff
