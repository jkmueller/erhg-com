---
title: "Wolf-Dieter Grengel zu: Soziologie I: Volk und Amt"
category: weitersager
order: 20
---


***Lieber Deutsch-Leistungskurs 12!***

Unsere Diskussionen der letzten Wochen sind immer wieder zu einem Punkt zurückgekehrt. Dieser Punkt ist Eure wachsende Ablehnung der Schule, genauer unseres Gymnasiums; man könnte von einer regelrechten Schulphobie sprechen. Das gilt nicht für alle, auch sind sicher eine gute Portion Koketterie und viel jugendliche Lust an der Provokation mit im Spiel. Aber die Tatsache ist nicht zu bestreiten, dass Ihr mit Vorwürfen und Anklagen nicht gespart habt. Ihr habt auch angedeutet, dass die schlechten Ergebnisse der Pisa-Studien mit diesem von Euch geäußerten Unbehagen zusammen hängen könnten.

Ihr stoßt Euch an den Vorgaben der Lehrpläne. So bemängelt Ihr, dass im Fach Deutsch, und das betrifft mich unmittelbar, auch Beispiele mittelalterlicher Literatur gelesen werden sollen. Ihr beklagt, dass den Klassikern ein zu großer Raum gegeben werde, dass selbst Sprachphilosophie auf dem Programm stehe. Die eigentlichen Interessen und Bedürfnisse von Jugendlichen im 21. Jahrhundert kämen zu kurz.

Dann nehmt Ihr Euch immer wieder die Schulorganisation vor und erklärt sie zu einem Hauptärgernis. Der streng fest gelegte und sehr frühe Unterrichtsbeginn, die Häppchen der 45-Minuten-Stunden, die Anwesenheitspflicht werden abgelehnt. Ihr verweist auf das Problem, dass von den Schülerinnen und Schülern erwartet wird, dass sie sich auf Kommando z.B. in der noch schläfrigen 1. Stunde und der schon wieder schläfrigen 6. Stunde auf ein lyrisches Gedicht, ein mathematisches Problem, eine philosophische Welterklärung einlassen.

Ein weiterer entscheidender Konfliktpunkt sind die Noten, ist der Leistungsdruck ganz allgemein. Nicht Sachinteresse sei gefragt, nicht Begeisterung für eine wichtige Fragestellung werde geweckt. Nein, vorwiegend taktische und strategische Opportunitätsüberlegungen und kaltes kaufmännisches Kalkül nach dem Motto „Was bringt mir das?” bestimmten notgedrungen Denken und Handeln der Schüler. Immer seltener komme es zu der doch so notwendigen Erkenntnis: Das geht dich an – „Tua res agitur.”

Vor allem prangert Ihr die Langeweile an, die sich in der Schule wie Mehltau auf alle kleinen und großen Keime, Triebe, Pflanzen lege. Wiederholung, Routine, Lustlosigkeit der frustrierten Lehrer und der vielfach abgelenkten Schüler erstickten jeden verheißungsvollen Ansatz.

Alle Vorwürfe gipfeln in dem Bild einer riesigen Maschine, deren Räderwerk abläuft, ohne Rücksicht zu nehmen auf die jeweiligen Umstände, auf Eigenständigkeit, Spontaneität, persönliches Engagement.

Ich habe bei dieser Kritik nicht den Eindruck gewonnen, dass Ihr die Schule abschaffen wollt, obwohl auch solche Stimmen laut wurden. Einige sind ohne Zweifel, wohl vor allem über ihre Eltern, mit der Schulkritik der 68er Generation in Berührung gekommen. Der Name Ivan Illich ist gefallen. Seine vehementen Angriffe auf die etablierten Schulsysteme, seine Forderung nach „Entschulung der Gesellschaft” sind offensichtlich nicht vergessen. Die meisten von Euch argumentieren weniger grundsätzlich, verweisen aber auch auf andere Formen des Lernens. Sandra hat von spontanen Treffen von Jugendlichen erzählt, wo vorgelesen und das Vorgelesene diskutiert wird, wo man Theater spielt, wo es zu heißen politischen Auseinandersetzungen kommt; wo auch Erwachsene aus verschiedenen Berufen zeitweilig dazustoßen und ihre jeweiligen Erfahrungen einbringen. Philipp hat die Ungezwungenheit in seiner Jugendgruppe beschrieben, wo ohne Druck an verschiedenen Projekten gearbeitet wird und das Lernen nicht zu kurz kommt. Und immer wieder habt Ihr die großen Lehrmeister Fernsehen und Computer mit Internet beschworen. Hier sei alles aktueller, auch kompetenter. Welcher Lehrer könne sich schon mit den Prominenten aus Politik, Wirtschaft und Kultur messen, deren gewichtige Aussagen allen zu jeder Zeit zugänglich seien und mit denen man sogar diskutieren könne.

Im Besonderen war noch die Rede von individuellen Lernprogrammen am Computer, die ohne fest gelegte Ort- und Zeitvorgaben auskämen. Dass auch in den normalen Schulunterricht diese Medien immer mehr Eingang fänden, zeige doch, wie sehr sich das überkommene Schulmodell verändere.

Ich habe die Schule, unsere Schule, meinen Unterricht und mich mit vielen, vielleicht zu vielen Worten verteidigt. Es lässt sich manches sagen zu Gunsten von Klassiker-Lektüren, von festen Unterrichtszeiten und vernünftigen Leistungskontrollen. Auch habe ich auf unsere Bemühungen um eine Offene Schule, um eigenverantwortliches Arbeiten und ausreichende Computer-Ausstattung, um Schülermitverwaltung, Lehrerfortbildung und die Verbesserung der Kommunikation zwischen Lehrern und Schülern hingewiesen. Ich will das jetzt hier nicht alles wiederholen, sondern mich vor allem auf einen Gedankengang beschränken, der uns vielleicht aus dem unfruchtbaren Antagonismus befreien kann.

Natürlich sind die Arbeit an einem Jugendprojekt, eine Suche im Internet und eine spontane Zusammenkunft freier, schöpferischer, begeisternder und vielleicht lehrreicher als gewöhnlicher Schulunterricht mit seinen festen Lernzielen und seinen Überprüfungen.

Aber… Ja, lacht nur! Jetzt kommt es, das große Aber. Was Ihr anpreist und bewundert, ist ja nicht nur neuartig, freizügig, partnerschaftlich, begeisternd und lehrreich, sondern auch ungewiss und unstet. Es bricht ab, löst sich auf, versandet, hat oft keine Folgen, bleibt unverbindlich. Das Projekt kann stocken, Ihr verirrt Euch und verwirrt Euch im Internet, Ihr versäumt immer öfter den Termin Eurer nächsten Zusammenkunft. Die Lust, die Begeisterung verfliegen ja oft ebenso schnell, wie sie uns anwandeln. Und am Ende besteht die Gefahr, dass wir mit leeren Händen da stehen.

Dagegen bieten wir in der Schule klare Konzepte, die aufeinander aufbauen, bestehen auf der Pflicht zum Unterrichtsbesuch, die gilt, auch wenn man keine Lust hat, sichern das Gelernte mit regelmäßigen Überprüfungen, wahren die Kontinuität, bereiten auf Abschlüsse vor, kurz, wir sorgen dafür, dass Ihr am Ende etwas Schwarz auf Weiß auf Papier oder Bildschirm – nach Hause tragen könnt.

Aber damit seid Ihr, ist Eure Kritik nicht widerlegt. Das Geheimnis besteht darin, dass beide Parteien Recht haben. Und das ist kein fauler Kompromiss!

 „Der Grundsatz der Völker durch alle Zeiten aber lautet: Jedermann muss alles zu tun aufgefordert werden; Einer muss für jede bestimmte Leistung bestimmt da sein. Wir alle sollen alles tun; aber es ist unbestimmt, ob wir es tun. Deshalb muss Einer es bestimmt tun, auch wenn er es vielleicht schlechter tut als die Freiwilligen und gelegentlichen Täter.”

Diese Sätze stehen im ersten Band der „Soziologie” des Rechtshistorikers und Soziologen Eugen Rosenstock-Huessy (Stuttgart, 1956), in einem kleinen Kapitel, das den Titel trägt „Volk und Amt”.

Der Autor macht deutlich, dass beides zusammen gehört, das Freiwillige, Spontane, Einmalige und die Kontinuität, die Absicherung. Das Primäre sind, wie er weiter ausführt, die Menschen aus dem Volk, die Laien, die Amateure, aber sie brauchen als festen Gegenpart die Professionellen, das Amt, die Institution. Amateur heißt ja „Liebhaber”, das Wort bezeichnet jemanden, der etwas aus Liebe und Begeisterung tut, aber diese Liebe kann schnell zur bloßen Liebhaberei herabsinken und auch gänzlich verschwinden. Auf der anderen Seite drohen, gerade Eure Kritik hat es deutlich gezeigt, Verknöcherung, Routine, Langeweile, Unpersönlichkeit und die Überheblichkeit der Macht.

Dieses Aufeinander-Angewiesen-Sein gilt für alle Lebensbereiche. So muss es Dichter und Schriftsteller geben, und jeder von uns sollte zu Zeiten singen und dichten und fabulieren.

Niemand kann so gut pflegen wie eine liebende Mutter ihr Kind, und doch muss es ein ausgebildetes Pflegepersonal geben, auch Mütter können ausfallen.

In der Rechtsprechung ist das Laien-Element durch die Heranziehung von Schöffen und Geschworenen ja sogar institutionalisiert.

In der Kirche liegen die Dinge nicht viel anders. Die Amtskirche braucht die „Kirche von unten”, die Laienverbände, die Kirchentage. Von hier muss sie immer von neuem befragt, ja in Frage gestellt werden. Aber andererseits, was wäre Kirche ohne Tradition, Lehre, Ämter und die Ordnung des Kirchenjahrs?

Oder nehmen wir den Sport, der Euch vielleicht näher liegt. Es ist ja gerade heute offensichtlich, wie bei aller Bewunderung von Spitzenleistungen der Berufssportler wir uns auch abgestoßen fühlen von der Blasiertheit und Arroganz mancher Profis, um von der zunehmenden Kommerzialisierung und dem Dopingproblem gar nicht erst zu reden. Dagegen können echte Amateure uns mit ihrer ungetrübten Spielfreude auch ohne Höchstleistungen neu für den Sport gewinnen und begeistern.

Ich denke, Licht- und Schattenseiten der beiden Positionen sind deutlich geworden. Während ich das so schreibe, beschleicht mich das Gefühl, dass es sich bei dieser Frage um eine Selbstverständlichkeit, eine Trivialität handelt, die jeder kennt, dass ich hier offene Türen einrenne. Dann aber will es mir wieder scheinen, wenn ich so manche Kämpfe und Auseinandersetzungen in unserer Gesellschaft bedenke, gerade auch die Streitgespräche zwischen Euch und mir in den letzten Wochen, dass wir es hier im Grunde mit einem wenig bekannten und oft verkannten Phänomen zu tun haben.

Das Entscheidende ist nämlich, dass beide Bereiche aufeinander bezogen und angewiesen sind: „Aber wirkliche Menschen müssen für ihren Gegenspieler unvoreingenommen bleiben und von ihm unausgesetzt lernen”, heißt es an einer wichtigen Stelle bei Rosenstock-Huessy. Und er scheut nicht davor zurück zu fordern, dass die Gegenspieler einander bewundern sollten. Es geht also um ERGÄNZUNG im ursprünglichen Sinn des Wortes.

In Franz Werfels Theaterstück „Jacobowsky und der Oberst”, an dessen Aufführung durch die Theater-Arbeitsgemeinschaft unserer Schule ja einige von Euch im letzten Jahr mitgewirkt haben, sagt der Oberst im Hinblick auf seinen so verschiedenen Gegenspieler und Konkurrenten Jacobowsky: „Gegensätze müssen sich vernichten.” Und Marianne, die Frau, die von beiden Männern geliebt wird, antwortet ihm: „Vielleicht sollten sie einander ergänzen. Gegensätze sind immer nur Hälften.”

Rosenstock--Huessy erläutert in aller wünschenswerten Klarheit, was Werfel hier andeutet: „Die Ergänzung tritt nicht ein, solange nicht in die Beteiligten ein Bewusstsein ihres Entgegengesetzten einzieht. Ich muss auf deine Leistung blicken und du auf meine. Historisch bin ich der Überzeugung, dass diese gegenseitigen Bewusstseine viel älter sind als das heute allein anerkannte Selbstbewusstsein. Nur das Kind, das den Standpunkt seiner Eltern ergriffen hat, kann je einen eigenen erwerben. Das sagen schließlich auch die Freudianer mit ihrem "Superego". Jedes Ganze verlangt von seinen Gliedern ein Absehen von der eigenen Art und eine Bewunderung der entgegengesetzten. In meinem auf den Gegenpart gerichteten Blick und in dem aus der Bewunderung des Gegenparts quellenden Sehnen, ihm gleich zu sein oder zu werden, erkenne ich ein Ganzes wirksam an.” Das nennt der Autor „das erste Sozialgesetz”.

Was heißt das alles für die Schule und unseren Streitpunkt?

Es heißt, dass ich Euch auffordere, unsere Bemühungen um Tradition, Kontinuität, Absicherung, Kontrolle zu verstehen und nicht gering zu achten und gleichzeitig Nachsicht zu haben mit unserer Unbeweglichkeit und unserem bürokratischen Übereifer.

Und es heißt weiter, dass wir Lehrer bei allem Unmut über Euren jugendlichen Überschwang und Eure jugendliche Ungerechtigkeit und Eure Unberechenbarkeit offen bleiben müssen für Eure Begeisterungsfähigkeit, Eure Experimentierlust, Euer Bedürfnis nach neuen Kommunikationsformen, auch mit nicht-schulischen und gelegentlichen Lehrmeistern.

Was Eure nächsten Projekte, Eure nächsten Zusammentreffen außerhalb der Schule angeht – ich warte auf eine Einladung!!

Und lest einmal den von mir behandelten Abschnitt aus der „Soziologie” von Rosenstock-Huessy nach, den Text stelle ich gerne zur Verfügung, samt allen nötigen Informationen.

*Euer Deutschlehrer W-D. Grengel – Lehrbeamter und Profi, der in*\
*seinem Herzen auch ein bisschen Amateur geblieben ist oder es zu-*\
*mindest wieder werden möchte. Helft ihm dabei!!*
