---
title: Mitgliederbrief 2018-02
category: rundbrief
created: 2018-02
summary: |
  1. Einladung zur Jahrestagung und Mitgliederversammlung - Jürgen Müller
  2. Tagungsort und Anmeldung                             - Andreas Schreck
  3. Einladung zur Mitgliederversammlung                  - Jürgen Müller
  4. Programm der Jahrestagung                            - Jürgen Müller
  5. Brief an die Mitglieder                              - Rudolf Kremers
  6. Neudruck der Europäischen Revolutionen               - Thomas Dreessen
  7. Adressenänderungen                                   - Thomas Dreessen
  8. Hinweis zum Postversand                              - Andreas Schreck
  9. Jahresbeiträge 2018                                  - Andreas Schreck
zitat: |
  >Wir aber, die in der babylonischen Sprachenverwirrung des Kriegs ehrlos und heimatlos Gewordenen, die wir den doppelten Fluch seitens der deutschen Heiden und seitens der Völkerbundsheiden freiwillig auf uns nehmen, empfangen in dieser immer stiller werdenden Stunde das Gesetz des ewigen Lebens, das von Abend gen Morgen weist, und die Verheißung des Reichs.
  >Der doppelte Fluch hindert uns, fortan etwas Lebendiges zu hassen, nur dem Faulen und Toten kann unser Haß noch gelten. Lebendiger Geist kann von uns Liebe fordern in jeglicher Gestalt.
  >
  >Denn alles Nationale ist selbst ein geistiger Dornröschenschlaf, ist eine Verzauberung, in die sich die Völker auf ihrem dunklen Gange für Jahrtausende hineinverirrt haben.
  >Kein Volk saß tiefer im Märchen und in der Sage, als die Deutschen. Keines hatte leidenschaftlicher die Siegfriedsage und den Wotanglauben dem „römisch-orientalischen” Kreuzesglauben zuwider entwickelt, bis ums Jahr 1900 sogar die Gebildeten nicht mehr wußten, daß Heldensage und Eddaglaube erst als trotziger Widerspruch gegen die Offenbarung zur Entfaltung gebracht worden sind.
  >
  >Damit war ein letzter Höhepunkt des Heidentums erreicht; als schon kein Großmütterlein im niedern Volk mehr an Zwerge oder Riesen glaubte, da glaubten die Gebildeten um so krampfhafter, daß ihre Altvordern einer reifen „eigenen Religion” angehangen hätten. Die geistigen Träger Neudeutschlands vermuteten in ihren Ahnen statt der trüben Barbaren, die in ihrem verzweifelten Dunkel das Licht des Geistes von Osten froh begrüßt hatten, Lichtgestalten und Weisheitskünder.
  >
  >Dieser Aberglaube der führenden Schicht war das Vorzeichen ihres Zusammenbruches. Es war ein letztes Aufbäumen des Stolzes, der sich schon bedroht fühlte.
  >*Eugen Rosenstock-Huessy, Die Hochzeit des Krieges und der Revolution, 1920*
---

## Eugen Rosenstock-Huessy Gesellschaft e.V.

{{ page.zitat }}

**Vorstand/board/bestuur:**\
*Dr. Jürgen Müller (Vorsitzender);*\
*Thomas Dreessen; Andreas Schreck; Dr. Eckart Wilkens*\
*Antwortadresse: Jürgen Müller: Vermeerstraat 17, 5691 ED Son, Niederlande*\
*Tel: 0(031) 499 32 40 59*

***Brief an die Mitglieder Februar 2018***

**Inhalt**

{{ page.summary }}

### 1. Einladung zur Jahrestagung und zur Mitgliederversammlung

In einer Zeit in der durch die Globalisierung viele Dinge ins Schwimmen gekommen sind, versucht man vielerorts durch Rückbesinnung aufs Nationale festen Grund unter den Füßen zu bekommen.

Eugen Rosenstock-Huessy hat 1928 in: „Unser Volksname Deutsch und die Aufhebung des Herzogtums Bayern” den rechtsgeschichtlichen Hintergrund für die erstmalige Nennung der Sprache Deutsch 786 und dann im Urteil gegen Tassilo 788 aufgezeigt. Die Heeres- und Rechtssprache des ganzen Frankenreiches war Deutsch. Der Ursprung war somit klar und deutlich.
![Fränkisches Reich]({{ 'assets/images/fraenkisches_Reich.jpg' | relative_url }}){:.img-center .img-xxlarge}

Wir wollen uns auf der Jahrestagung mit dem Thema:\
*„Deutsch – Europa – Planet Erde: Wie übersetzen wir den Volksnamen Deutsch in die Sprache des Menschengeschlechts?”* mit Rosenstock-Huessys Analyse und seiner Lösungsempfehlung zur Nationalen Frage befassen.

Gottfried Hofmann hat uns auf einen kürzeren Zeitungstext: *„WESHALB HEISSEN WIR DEUTSCHE?”* aus dem Jahre 1932 aufmerksam gemacht. Diesen Hinweis nehmen wir gerne auf und wollen den Text zusammen mit einem Gedicht von Rosenstock-Huessy für Georg Müller: *„JAKOB GRIMM SPRACHLOS”* als Textgrundlage für unsere Tagung benutzen.

Die Mitgliederversammlung haben wir wie gewohnt am Samstagnachmittag geplant.

Ich möchte Sie zur Jahrestagung und zur Mitgliederversammlung (am 24. März) herzlich einladen.
>*Jürgen Müller*

### 2. Tagungsort und Anmeldung

Die Jahrestagung findet vom 23. bis 25. März 2018 statt im Haus am Turm, am Turm 7, 45239 Essen-Werden, Tel. 0201-404067. Beginn ist am 23. März 2018 um 18 Uhr mit dem Abendessen.
Kosten
Die Kosten betragen € 120 bei Unterbringung im Doppelzimmer, € 145 bei Unterbringung im Einzelzimmer. Die Anzahl der Einzelzimmer mit Bad/WC ist begrenzt.
Interessenten, die die Tagungskosten nicht in voller Höhe aufbringen können, mögen sich bitte an Andreas Schreck oder ein Vorstandsmitglied wenden.

Anmeldung
bitte an Andreas Schreck, Tel. 0551-28047871;
>*Andreas Schreck*

### 3.

**Eugen Rosenstock-Huessy Gesellschaft e.V., Bielefeld**

     Einladung zur ordentlichen Mitgliederversammlung
     am 24. März 2018, 15:30 Uhr Haus am Turm, Am Turm 7, 45239 Essen

TOP 1: Begrüßung der Mitglieder, Feststellung der Beschlussfähigkeit und Festlegung der Protokollführung\
TOP 2: Genehmigung des Protokolls der Mitgliederversammlung vom 24. Juni 2017 in Bebra-Imshausen\
TOP 3: Anträge zur Änderung/Erweiterung der Tagesordnung\
TOP 4: Vorstellung neuer Mitglieder\
TOP 5: Bericht und Ausblick des 1. Vorsitzenden mit Aussprache\
TOP 6: Kassenbericht für das Geschäftsjahr 2017\
TOP 7: Berichte der Kassenprüfer\
TOP 8: Entlastung des Vorstands für das Geschäftsjahr 2017\
TOP 9: Buchprojekte\
TOP 10: Berichte von Respondeo, vom Eugen Rosenstock-Huessy Fund und der Society und von Projekten einzelner Mitglieder\
TOP 11: Verschiedenes

### 4. Programm der Jahrestagung

|       | ***„Deutsch – Europa – Planet Erde: Wie übersetzen wir den Volksnamen Deutsch in die Sprache des Menschengeschlechts?”*** |
| Freitag, 23.3 | 18:00 | Eintreffen |
|               | 18:30 | Abendessen |
|               | 19:30 | Einführung in die Tagung (Thomas Dreessen) |
|               | 21:00 | Geselliges Beisammensein |
| Samstag, 24.3 |  8:30 | Frühstück |
|               |  9:30 | "Textlesung: Weshalb heissen Wir Deutsche? Erläuterung (Andreas Schreck)" |
|               | 11:00 | Pause |
|               | 11:30 | Eigene Erfahrungen – Gesprächsgruppen |
|               | 12:15 | Pause |
|               | 12:30 | Mittagessen |
|               | 15:00 | Kaffeepause |
|               | 15:30 | Mitgliederversammlung |
|               | 18:30 | Abendessen |
|               | 20:00 | "Eugen Rosenstock-Huessys Gedicht: Jacob Grimm Sprachlos an Georg Müller, 22. August 1953 (Eckart Wilkens)" |
|               | 21:30 | Geselliges Beisammensein |
| Sonntag, 25.3 |  8:00 | Morgenandacht |
|               |  8:30 | Frühstück |
|               | 10:00 | Vorläufer der Zukunft – Plenumsgespräch |
|               | 11:30 | Überwindung der Sprachlosigkeit (Jürgen Müller) |
|               | 12:30 | Mittagessen |
|               | 13:00 | Abreise |

### 5. Brief an die mitglieder der rosenstock-huessy-gesellschaft

liebe freunde: es ist mir schwer gefallen, der bitte unseres vorsitzenden entsprechend, im rückblickauf meine dreijährige mitarbeit im vorstand unsere gesellschaft noch etwas für den neuen mitarbeiterbrief zu schreiben. nach meiner meinung habe ich dazu schon alles wesentliche gesagt und wollte mich nicht selbst wiederholen.

beim durchsehen meiner aufzeichnungen fiel mir allerdings auf, dass es darin einige aussagen gab, die mir unmittelbar machten, warum ich auf das „nicht akademische” denken von ERH so voll „abgefahren” bin.

diese aussagen stelle ich unten kommentarlos nebeneinander und überlasse es gänzlich dem vorstand, ob diese aufstellung teilweise oder ganz im nächsten mitgliederbrief mitgeteilt wird:
- alle menschen töten, weil sie lebendiges an sich reißen müssen, um selber zu leben. und alle menschen sterben.
- die geschichte des menschengeschlechts ist daher über ein einziges thema komponiert: wie wird die liebe stärker als der tod?
- so wird geschichte ein großer sang: in ihm wird jede zeile, vielleicht jeder ton, ein gelebtes menschenleben. so wird aus ungereimten zufällen, aus wertlosem abfall, aus nichtssagenden vorfällen der epochemachende notfall, in dem ein lange hingenommenes siechtum endgültig ins auge gefasst, eingekreuzt und überwunden wird.
- dies reimen, dies verknüpfen ist der menschen erdamt.und wie bewuahren wir es?wer spricht, den bewegen dazu drei nöte, in die wir menschen alle verstrickt sind: auf dem raumschiff erde herrscht an sich der tod … wir aber, die schiffsmannschaft der erde, fordern kühn diesen allherrscher tod heraus kraft der gesundheit unserer leiber, kraft der leidenschaft unseres geschlechts und kraft der sinngebung unseres sterbens in opfer und hingabe.
- kirche ist überall da, wo einem menschen die kraft verliehen wird, aus seiner zukunft heraus seine vergangenheit umzuschmelzen
- ein sprung in die unbekannte zukunft ist immer mitgesetzt, wenn wir zu sprechen versuchen.
- aus „der untergang des abendlandes” von j. spengler = die figur des dem wort und seiner erstgeburt trotzenden geistes, der nicht ewig leben, sondern mit seiner heimatseele zusammen sterben will … sein wort enthüllt, wie tief die krankheit des europäischen geistes bereits gefressen hat.spengler will nicht leben, sondern mit seiner heimatseele zusammen sterben.
- die zukunft, wie sie spenglers wissenschaft skizziert, ist nur das gespenst der vergangenheit, in die zukunft hineingerufen
- jeder für sich ist dem tode verfallen. die liebe ist aber stark wie der tod.

so viel für heute. ich wünsche ihnen allen in diesem sinn mutmachende weihnachtstage und grüße herzlich ihr
>*rudolf kremers*

### 6. Neudruck der Europäischen Revolutionen

Steffen Reiche aus Berlin hat uns angefragt, ob wir einen Neudruck von Rosenstock-Huessys Werk Die Europäischen Revolutionen und der Charakter der Nationen in der Ausgabe Brendow Verlag/Moers1987 unterstützen. Das vergriffene Buch wäre dann für €10 (in Worten: zehn) zzgl. Porto im Herbst 2018 erhältlich über die Gesellschaft. Wir bitten um Anzeige von Kaufinteresse /absicht von Exemplaren bis zum 20.3. an
>*Thomas Dreessen*

### 7. Adressenänderungen

Bitte schreiben sie eine eventuelle Adressenänderung schriftlich oder per E-mail an Thomas Dreessen (s. u.), er führt die Adressenliste. Alle Mitglieder und Korrespondenten, die diesen Brief mit gewöhnlicher Post bekommen, möchten wir bitten, uns soweit vorhanden ihre Email-Adresse mitzuteilen.
>*Thomas Dreessen*

### 8. Hinweis zum Postversand

Der Rundbrief der Eugen Rosenstock-Huessy Gesellschaft wird zukünftig als Postsendung nur noch an Mitglieder verschickt. Nicht-Mitglieder erhalten den Rundbrief gegen Erstattung der Druck- und Versandkosten in Höhe von € 20 p.a. Den Postversand betreut Andreas Schreck. Der Versand per e-Mail bleibt unberührt.
>*Andreas Schreck*

### 9. Jahresbeiträge 2018

Die „Selbstzahler” unter den Mitgliedern werden gebeten, den Jahresbeitrag in Höhe von 40 Euro (Einzelpersonen) auf das Konto Nr. 6430029 bei Sparkasse Bielefeld (BLZ 48050161) zu überweisen.
Nach Einführung des einheitlichen Zahlungssystem SEPA mit der Internationalen Kontonummer (IBAN): DE43 4805 0161 0006 4300 29
SWIFT-BIC: SPBIDE3BXXX ist diese „IBAN”-Nummer auch für Mitglieder in den Niederlanden und im ganzen „SEPA-Land”, in das sich sogar die Schweiz hat einbetten lassen, ohne Zusatzkosten nutzbar.
>*Andreas Schreck*
[zum Seitenbeginn](#top)
