---
title: "Rudolf Kremers: Die Begegnung von Eugen Rosenstock-Huessy und Karl Barth"
created: 2021
category: veroeff-elem
published: 2022-01-27
---
### Stimmstein 13, 2011
#### Die Begegnung von Eugen Rosenstock-Huessy und Karl Barth


Die ganze Menschheitsgeschichte ist nach Rosenstocks Auffassung Verleiblichung, Fleischwerdung des Wortes. Nicht nur in Christs und in der Kirche wird das Wort Fleisch, sondern überall, wo wirklich gesprochen wird. „Der Satz ,Das Wort ist Fleisch geworden"' wird entweder auch sonntags unwahr werden, oder wir werden ihn werktags nüchtern bewahrheiten müssen", schreibt ERH, „etwa so: nur das Wort, das Fleisch wird, ist Wort."[^1] Diese Auffassung der Fleischwerdung des Wortes in jeder wirklichen Sprache, also in dem ganzen Sprachstrom, der von Adam bis zum Jüngsten Tag fließt, dessen Höhepunkt nur das Christusgeschehen ist, ist wohl in entscheidender Unterschied zu der traditionellen Kirchenlehre. Der Satz „Das Wort ward Fleisch" wird da ja exklusiv auf Jesus Christus bezogen. Was vorausgeht, ist vorausblickende Verheißung dieser Inkarnation, und was folgt, ist rückblickendes Zeugnis von ihr. In unserer Zeit hat
Karl Barth in seiner unbedingten Konzentration auf das Christus-Ereignis diese überlieferte Glaubenslehre wieder glanzvoll zur Geltung gebracht. Die Begegnung zwischen diesen beiden großen christlichen Denkern des 20. Jahrhunderts verlief darum ebenso spannend wie dramatisch.

[^1]: „Der Atem des Geistes" (1951), S. 14.

Sie geschah kurz nach dem ersten Weltkrieg. ERH hatte zusammen mit den Vettern Ehrenberg sowie Leo Weismantel und Werner Picht den Patmos-Verlag gegründet, der zum Sprachrohr des neuen Denkens und Redens werden sollte, das ihnen während des Krieges aufgegangen war. Über diesen Verlag kam es zur Begegnung mit Karl Barth, dessen Tambacher Vortrag „Der Christ in der Gesellschaft" auf Betreiben von Hans Ehrenberg bei „Patmos" veröffentlicht wurde. Darüber kam es zu sehr freundschaftlichen Kontakten und intensivem Briefwechsel sowohl mit Hans Ehrenberg wie mit Eugen Rosenstock. Dabei zeigte sich von Anfang an sowohl die gegenseitige Anziehung we die ebenso starke gegenseitige Abstoßung. Es ist ergreifend zu verfolgen, mit welcher Leidenschaft ERH das Gespräch mit dem Schweizer Pfarrer sucht, obwohl von Anfang an die Gegensätze zwischen beiden sehr deutlich werden. Und auch Barth scheint diese Begegnung sehr positiv aufgenommen zu haben, denn schon bei der ersten persönlichen Begegnung bot er Rosenstock das Du an. Aber die Gegensätze wurden dabei nicht überspielt; denn sie ersparten sich gegenseitig nichts. Besonders die Kritik ERH's an Barth lässt an Schärfe
nichts zu wünschen übrig.

Worin besteht diese Kritik? Sie entzündete sich besonders an der Lektüre des Kommentars zum „Römerbrief*, der ja Barth berühmt gemacht hat. Rosenstock wirft Barth vor, dass er darin Paulus nicht interpretiere, sondern „dementiere". Während Paulus die Rechtfertigung des Sünders als wirkliches Geschehen in der Welt bezeuge, mache Barth daraus eine bloße göttliche „Erklärung". Aber „Ist das Heil nicht wirklich in die Welt gekommen", so fragt er, „Hat Gott sich nicht unser erbarmt? Redet Paulus von transzendentalen Kräften eines neuen Äon oder von einem Vater da oben 50 Millionen Kilometer weit weg - oder spricht er von dem Mensch gewordenen Gottessohn?"[^2] - Und im Blick auf Barths Übersetzung des griechischen Wortes „pistis" mit „True Gottes" spricht er von seinem „Objektivitätsfimmel".[^3]
Barth Antwortbriefe sind leider verlorengegangen, aber seine Gegenkritik wird nicht weniger deutlich gewesen sein. Was ist Offenbarung Gottes? Wie ist sie geschehen und wie geschieht sie? Darum geht es in dieser Auseinandersetzung. „Sie glauben an Moses und die Propheten und Christus, also an eine historische Offenbarung. Aber ich blicke um mich und she seit Christus eine historische, eine geschehende Offenbarung”[^4] schreibt ERH. Das war der Gegensatz. Rückblickend möchte man bedauern, das dies Gespräch schließlich abgebrochen wurde.

Ist der Eine nicht das notwendige Korrektiv des Anderen? So hat das ERH anfangs auch selbst gesehen; denn in einem seiner ersten
Briefe an Barth heißt es: „Vielleicht müssen wir so umgekehrt sprechen, um unsere übrigen sprachlosen Lebensbestandteile dadurch zur richtigen Fülle zu ergänzen ... Ich bedarf so sehr rücksichtsloser Kontrolle und Schutzhaft. Ich bitte Sie um Ihren Widerstand. Dann werden wir Frieden haben.”[^5] Ob Barth auch so gedacht hat, ist nicht mehr zu ergründen; aber er war wohl weniger bereit, dieses Gespräch fortzusetzen.

Denn dass es schließlich zum radikalen Abbruch der Beziehung kam, ist sicher auf Karl Barth zurückzuführen. Jedenfalls ist bezeichnend, dass er in seiner vielbändigen „Kirchlichen Dogmatik" ERH kein einziges Mal erwähnt. Das ist umso erstaunlicher, als sonst in diesem Werk alle wichtigen Theologen der Vergangenheit und Gegenwart zur Sprache kommen. ERH war für Barth da offenbar kein ernstzunehmender Gesprächspartner mehr. - Rosenstock selbst hat sich dagegen in seinen Schriften vielfältig mit Barth auseinandergesetzt - aber eben nicht mehr persönlich und direkt. Aus dem Gespräch wurde so nur gegenseitige Kritik und Distanzierung.

[^2]: Brief vom 18.11.1919.
[^3]: Brief vom 5.3.1920
[^4]: Brief vom 29.12.1919.
[^5]: Brief vom 23.12.1919.

Im Rückblick hat ERH später diese Auseinandersetzung so geschildert: „Er (Karl Barth) entdeckte sie (die katholische Lehre der Väter) erst wieder. Ich fragte: Was dann? ... Er fragte: Was ist das Wort? Wir: Wie wird das Wort wirksam? Er rief die Liberalen zur Ordnung, weil sie die gesunde Lehre zugunsten des neuesten Ohrenschmauses aufgegeben hatten. Wir gingen davon aus, dass man nicht rückwärts leben kann. Die Ohren, die einmal gegen die gesunde Lehre taub geworden sind, können niemals durch die bloße Wiederholung der Lehre erlebnisfähig werden.”[^6]

In diesen Sätzen wird die Differenz sehr deutlich. Rosenstock fragt, so scheint es, nach dem „Wie”. Wie muss das Wort gesagt werden, damit es „glaubwürdig klingt”, damit es „wirksam wird”? Und eben die besondere Betonung dieser Frage betrachtete Barth immer mit tiefem Misstrauen. Sein befreiender Aufruf war ja: Last uns zuerst und vor allen Dingen klären, was wir zu sagen haben. Das „Wie” wird sich dann schon finden. -

Und doch ist das nur scheinbar der Punkt, um den es in dieser Auseinandersetzung ging. Auch Rosenstock fragte nämlich nach dem „Was”
nach dem Inhalt der Offenbarung, nur war dies „Was” für ihn keine „Sache”. Als Barth ihm einmal vorwarf, dass er und seine Freunde „nicht resolut in der Sache lebten”, antwortete er: „Dieser Satz hat keinen Sinn für mich. Ich weiß nicht, was das heißt, in einer Sache leben? Wie komme ich dazu, in einer Sache zu leben?"[^7] Leben kann man nur in etwas Lebendigem, in diesem Fall in dem neuen Leben, das in Christs erschienen ist. „Wenn Barth aber von Christs redet, wird's immer wie tönendes Erz”, so schreibt ERH an Eduard Thurneysen, „Dieser Christs ist bloß ein Schlagwort, eine Vokabel, ist unglaubwürdig” und: „Barth übersetzt nirgends das Wort
Christus ins Lebendige heutige. Dies tote Wort ist der Nullpunkt, auf den er die Theologie herunterdemütig”[^8] -

Weil es Rosenstock darum ging, dass das neue Leben, das in Christus erschienen ist, sich in der Gegenwart neu verleiblicht, darum war ihm die „Sachlichkeit” Barths unverständlich und verdächtig. Dass dieser Ruf ”zur Sache” auch eine heilsame und notwendige Anfrage an bloße „Lebendigkeit" ist, dass weder emotionale Ergriffenheit allein noch nüchterne Sachlichkeit der zu bezeugenden Glaubenswahrheit gerecht wird, sondern nur beide zusammen wie das ERH anfänglich ja durchaus empfunden hat (siehe oben!) - ging dabei unter.

[^6]: „Ja und Nein" (1968) S.82f.
[^7]: Brief v. 18.3.1920.
[^8]: Brief v. 18.2.1920

Rosenstock litt darunter, dass das neue Leben, das durch Christus in der Welt erschienen ist, in Kirche und Theologie so wenig erfahrbar wurde. In seinem ersten Buch nach dem Krieg mit dem Titel „Die Hochzeit des Krieges und der Revolution" schrieb er dazu: „Die Wahrheit steht heute wie ein nackter, lebloser Pfahl, wie eine künstliche Steinsäule ohne Wirkung ... die Wahrheitspächter haben nur noch recht. Das aber genügt nicht zum Leben der Wahrheit ... Wir alle leiten nicht mehr, so heilen wir nicht.”[^9] Und für die Patmos-Gruppe heißt es in demselben Werk: „Wir aber wollen nichts sein als das kurze Kabelstück, welches den Riss zwischen gestern und morgen gläubig überwindet. Ohne diesen Durchgang durch das enge Tor der Zeit stirbt der Geist.”[^10]

In Krieg und Revolution, die er beide mit durchlitten hatte, vernahm ERH einen wirklichen Anruf Gottes. Beide „sind nur dazu da gewesen, damit uns Gott an sich reißt und durch uns hindurch unser Volk, und damit er so seinen Heilsplan vollstrecke”, schreibt er.[^11]

Karl Barth und seinem Mitstreiter Eduard Thurneysen erschien das dagegen als eine deutsche Nachkriegsschwärmerei. „Mir kommt alles vor wie ein geistreicher Rausch und Nebel, rausgeboren aus dem Elende der deutschen Lage”, schreibt Thurneysen an seinen Freund.[^12]

So mündet das so hoffnungsvoll begonnene Gespräch in Unverständnis und schließlich in Schweigen - jedenfalls im Blick auf den persönlichen und brieflichen Austausch. In seinen Schriften hat Rosenstock, wie erwähnt, sich vielfältig mit Barth auseinandergesetzt, besonders in „Ichthys" von 1925. Von Karl Barth kann man dasselbe nicht sagen. Weder in seiner „Kirchlichen Dogmatik” noch in seinen sonstigen Schriften und Reden wird Rosenstock noch einmal erwähnt.

[^9]: Hochzeit des Krieges und der Revolution (1920), S. 274.
[^10]: Ebd., S. 242.
[^11]: Brief v.18.11.19 an Karl Barth.
[^12]: Brief v. 12.6.1920.
