---
title: Gerhard Gillhoff zu «Die Europäischen Revolutionen»
category: weitersager
order: 25
---

***Brief über\
Eugen Rosenstock-Huessy,\
Die europäischen Revolutionen und der Charakter der Nationen***


Liebe Nikola,

Du hast mir schon mehr als einmal gesagt, Du wolltest in dem bevorstehenden Urlaub Rosenstock-Huessys Revolutionsbuch lesen, das ich Dir zu Deinem 30. Geburtstag geschenkt habe. Bisher ist nichts daraus geworden, vielleicht auch, weil das Buch so dick ist, und neben Deinem Zwölf-Stunden-Berufsalltag in Brüssel schreckt das erst recht ab.

Mit meinem Brief heute möchte ich Dir den Anfang erleichtern.

Dieses Geschichtsbuch habe ich zuerst 1954 als Schüler in der 2. Ausgabe von 1951 gelesen. In meinem Geschichtsstudium von 19551962 kam Rosenstock-Huessy zu meinem großen Erstaunen nicht vor, ja die Versuche, mit ihm zu arbeiten, wurden abgewiesen. Für mich ist diese Erzählung der Geschichte des 2. nachchristlichen Jahrtausends in Europa lebenslang die Orientierung darüber geblieben, in welchen historischen Kontext ich lebe.

In der unübersehlichen Fülle der überlieferten Einzelfakten, z. B. der Kriege und Feldzüge und der Herrscherdaten des vergangenen Jahrtausends, Muster zu erkennen, sich wiederholende Verlaufsmuster, war für mich die große wissenschaftliche Leistung Rosenstock-Huessys in diesem Buch. Sie ermöglicht als Struktur und Reduktion auf fünf Fälle auch, die Verläufe und ihre Variationen ein für allemal im Gedächtnis zu behalten. Es sind die fünf großen Umwälzungen in Italien, Deutschland, England Frankreich und Rußland, die diese fünf Nationen geprägt haben.

Der Text des Theoriekapitels wirkt beim ersten Lesen zunächst verwirrend, weil die Beispiele für die Theorie aus dem ganzen 2. Jahrtausend stammen, und Lenin so neben Papst Gregor VII. steht. Das reine Gerüst findest Du aber in den tabellarischen Übersichten, so daß Du nicht in den Einzelheiten ertrinkst. Dagegen hilft die Theorie beim Lesen der fünf Kapitel im Hauptteil. Danach wirst Du die Theorie bei der zweiten Lektüre am meisten schätzen. Auf diese Theorie will ich mich hier konzentrieren.


Seine Besonderheit zeigt dieses Geschichtsbuch schon am Anfang: Es beginnt mit einem Theorie-Kapitel. Rosenstock-Huessy hat darüber in der Einleitung zur 2. Ausgabe 1951 geschrieben: … alles, was ich von den Revolutionen weiß, hat mich das Noch-Einmal-Hinhören auf ihr Geschehen gelehrt. Ich habe den Gehorsam gegen sie noch einmal durchlebt. Wäre "Rückhören auf die Revolutionen" eine befreiende Überschrift?" S.XV2[1]

Hier wird deutlich, daß das Geschichtsbuch *nach* den grundlegenden Sprachstudien Rosenstock-Huessys geschrieben ist, die das methodische Gerüst seines Geschichts-Gehorsams darstellen. Rosenstock-Huessy versteht sich nicht als Beobachter auf der Galerie oder als Zuschauer im Geschichts-Theater, sondern er schöpft seine Einsichten aus selbst miterlebter Geschichte, vor allem des Epochenjahrs 1918, in dem für ihn der Selbstmord Europas manifest wird. Das ist seine Version von Spenglers Untergang des Abendlandes.

"Rückhören auf Revolutionen" und sich durch dieses Hören auf noch gültige Imperative selbst befreien, ist sein Angebot.

Um das Verlaufsmuster herauszuarbeiten, beginnt Rosenstock-Huessy seine Theorie mit der Bestimmung dessen, was er im Titel mit den europäischen Revolutionen meint.

In der Wissenschaftssprache taucht dieses Wort zuerst in der Astronomie auf, dem Vorbild für alle Gesetzlichkeit von alters her.

Revolution ist hier für Rosenstock-Huessy eine Totalumwälzung, "die ein für allemal ein neues Lebensprinzip in die Weltgeschichte einführen will."(5) Sie kündigt einer alten Rechtsordnung den Gehorsam auf und verlangt die Anerkennung ihrer neuen Sprache und Rechtsordnung auf der ganzen Welt. So sind 1830 und 1848 klar Tochterereignisse zu 1789 bis hin zu Sunyatsen 1900 in China und der bürgerlichen Revolution 1905 in Petersburg.

Alle echten Revolutionen sind also Weltrevolutionen, aber je weiter sie sich örtlich und zeitlich vom Ursprung entfernen, um so schwächer wird die Durchsetzungskraft dieser "Ableger und Nachahmungen". Das Muster bleibt immer das Land des Ausbruchs.

Damit scheiden die meisten in der Geschichtswissenschaft sog. Revolutionen als abgeleitete Ereignisse aus, die nur die Gewalt des ersten Anstoßes bezeugen.

Andererseits heißen in der Historiographie die echten Schwesterereignisse nicht alle "Revolution". Welche Ereignisse des 2. nachchristlichen Jahrtausends "bedeuten dieselbe ursprüngliche originelle Leistung, weil sie den gleichen Gesetzen unterliegen, die gleiche Wirkungsweise durch die Welt entfalten?”
Die russische Revolution ist die einzige Ausnahme von der Regel, daß es zwischen ihnen keine bewußte Verknüpfung gibt. lhre Revolutionäre sind schon Jahrzehnte vor 1917 aktiv. In der französischen Revolution werden sich die Revolutionäre ihres umwälzenden Tuns bewußt.

In der englischen Revolution heißt nur der letzte Akt auch so: The Glorious Revolution 1688/9. Cromwells Great Rebellion ist der Sache nach ebenso Revolution wie die französische, sie spricht aber noch die theologische Sprache des Alten Testaments.

Rosenstock-Huessy geht immer auf die Selbst- und Fremdbezeichnungen, die Namen, zurück und stößt bei Milton auf die Bezeichnung Reformation der Reformation für die englische Revolution.

Luthers Bruch mit dem kanonischen Recht und der Autorität des Papstes ist die von den Deutschen vollbrachte europäische Revolution, sie spricht die Sprache des Neuen Testaments.

Als ältesten Revolutionär in Europa erkennt Rosenstock-Huessy Papst Gregor VII, der dem Kaiser die religiös begründete Weltherrschaft im Investiturstreit entwand. Die hatte der Kaiser seit der Translatio imperii am Weihnachtstag 800 auf Karl den Großen inne.

Renovatio, Widererneuerung, ist das Stichwort, um das sich Kaiser und Papst am Anfang des 2. Jahrtausends streiten. Dabei benutzt der Papst die Sprache der Kirchenväter.

Alle diese großen Revolutionen sind Empörungen zur Wiedergewinnung eines Urrechts. In seinem Spätwerk "Die Sprache des Menschengeschlechts" mit dem Untertitel "Eine leibhaftige Grammatik" sagt Rosenstock-Huessy, was er für die Aufgabe der Historiographie hält: alle "Geschichte erzählt dasselbe: etwas, das unmöglich schien, ist geschehen. Nur das, was einmal unmöglich war, gehört in die Geschichte, die ausdrücklich in das Gehege des Selbstverständlichen einbricht."(Bd. II, S. 206)

So war es für die Kaiser Heinrich III. und Heinrich den IV. selbstverständlich, daß sie Bischöfe einsetzen und absetzen konnten, auch den Bischof von Rom. Unerhört war, daß ein Papst (Gregor VII.) den Kaiser in den Bann tut und die Fürsten von ihrem Eid auf den Kaiser entbindet.

Der Rhythmus ist die Zeitgestalt, das Muster in der historischen Zeit, das Rosenstock-Huessy in den fünf großen europäischen Revolutionen als sich wiederholende Struktur erkennt.

Die europäischen Revolutionen finden zunächst lokal in ihren Ursprungsländern statt, breiten sich jedoch später auf ganz Europa und den Rest der Welt aus. Dabei ist zu beobachten, daß sich die Geschwindigkeit, mit der eine Revolution sich ereignet, mit der Zeit zunimmt. ("Indem die Revolutionen sich selbst von Mal zu Mal deutlicher erkennen, beschleunigt sich ihr Gang." 17)

Die erste, die Papstrevolution, brauchte gar zwei Anläufe, bevor sich die Vorstellungen Gregors VII. unter Innozenz III. durchsetzen konnten. Erst unter diesem war nämlich die Grundvoraussetzung für eine erfolgreiche Revolution gegeben: Die Verwurzelung in nur einem Land.

Indem die Revolutionäre zunächst eine Umwälzung der gesamten Welt versuchten, und dabei scheiterten, konnten sie sich mit Italien als Land der Realisierung der neuen Freiheit begnügen.

Auch die Reformation fand in weiterem Sinne zweimal statt. Erst durch Luther nach religiösen, die Menschen als Christen befreiende Reformation und dann durch die Landesfürsten nach weltlich, pragmatischen Maßstäben.

Ebenso sieht Rosenstock-Huessy die Englische Revolution in "Civil War" und "Glorious Revolution zweigeteilt, ein Indiz dafür, daß hier der Rhythmus der vorangegangenen Revolutionen wiederkehrt. Allerdings ist das Zeitintervall zwischen den beiden Teilen der englischen Revolution auf Grund des beschleunigten Ablaufs schon so klein, daß man die Zeit zwischen 1649 und1689 bereits als eine Epoche betrachtet.

Schwieriger zu erkennen wird der doppelte Ansatz bei der französischen und russischen Revolution.

Während die früheren Revolutionäre in dem Bewußtsein handeln, altes Recht wieder herzustellen, werden die Franzosen sich während der Ereignisse ihres eigenen Tuns bewußt. Frankreichs Bourgeoisie erlangt 1830 die Alleinherrschaft.

Der Ausruf des Bürgerkönigs: "Enrichissez vous!" ist Rosenstock-Huessy Indiz dafür.

In Rußland dagegen, da es sich ja hier um die einzige geplante Revolution handelt, unterscheidet sich der Ablauf von den früheren Revolutionen. Die "Antwort", der zweite Schritt, erscheint in Rußland als "Vorspiel"; denn Bewußtheit verdreht die Zeitfolge. In Frankreich folgt der Krieg auf die Revolution, während in Rußland der Krieg erst die Revolution ermöglicht.


Die folgende Tabelle stellt die Doppelakte zusammen:

| 1075-1122 | Investiturstreit                                        |
| 1200-1268 | Ausrottung der Staufer                                  |
|           |                                                         |
| 1517-1555 | Lutherische Reformation                                 |
| 1756-1763 | Preußens Siebenjähriger Krieg                           |
|           |                                                         |
| 1649-1660 | Great Rebellion (Restoration of Freedom)                |
| 1688-1689 | Glorious Revolution                                     |
|           |                                                         |
| 1789-181S | Große Revolution                                        |
| 1830      | Julirevolution                                          |
| 1904/05   | Japanisch-Russischer Krieg, "Erster koreanischer Krieg" |
|           | Erste russische Revolution                              |
|           |                                                         |
| 1917      | Amerikas Eintritt in den Krieg                          |
|           | Russische Revolution                                    |
|           |                                                         |
| 1941      | Hitler überfällt Rußland                                |
|           | Japan überfällt Amerika                                 |
|           |                                                         |
| 1950/51   | "Zweiter koreanischer Krieg"                            |

Die letzten vier Jahreszahlen zeigen, wie Rosenstock-Huessy um 1960 die „Weltkriegsrevolution” sah.


Es wird Dich erstaunen, daß Kalender für Rosenstock-Huessy wichtige Geschichtsquellen sind, so auch die der Revolution.

Die Revolutionen haben alle aus ihrem nationalen Selbstbewußtsein heraus dazu tendiert, für ihr Volk als Beginn einer neuen Zeitrechnung zu gelten. Franzosen betrachten die Geschichte vor 1789 als zu unwichtig, als daß sie das in der Schule lernen sollten, strenge Protestanten betrachten alle Ereignisse vor 1517 als finsterstes Mittelalter; daher auch die für uns gängige Epocheneinteilung in Mittelalter und Neuzeit, die dem Verständnis der Reformation als Zeitursprung entspricht. Die Engländer, wenn auch nicht so offensichtlich, hatten durch ihre Insellage ein Sonderbewußtsein. So ist auch dort nach der Glorious Revolution ein Einschnitt, der aber auf Grund der england-zentrischen Einstellung zu internationaler Politik weniger auffällt. In Rußland wurde sogar zeitweilig ein neuer Kalender mit einer Fünf-Tage-Woche und einem Sechs-Wochen-Monat eingeführt, um Christen und Juden ihren Kultus unmöglich zu machen.
Revolutionen bringen also die einheitlich Zeitrechnung der europäischen Völker in Gefahr und damit auch ihre geistige Verwandtschaft und Gemeinschaft. Dieser Trend verstärkt sich von Mal zu Mal.

Wäre einer Revolution eine neue Zeitrechnung gelungen, so wäre die europäische Geschichte zerstört worden. Zeitrechnung ist für Rosenstock-Huessy mehr als ein Instrument für Handelskammern und Kalenderfabrikanten. „Ob die Völker wirklich 1929, 1930, 1931 von Christi Geburt an rechnen (da mußt Du heute 2003, 2004, 2005 einsetzen) oder ob sie von sich selbst und auf sich selbst allein rechnen, das ist und bleibt der ganze Unterschied zwischen der Natur der Tiervölker und der Geschichte des Menschengeschlechts. … In dem anderen Fall der durchlaufenden Ära erschafft die Revolution zwar ihr Volk, aber sie beruft es an seine besondere Aufgabe im Haushalt des Menschengeschlechts." (20/21 )

Da die Revolutionäre Europa in ein Zeitalter des Wandels stürzen und damit jahrzehntelange Unruhe stiften, müssen sie dem Volk ein Gefühl geben, daß sich die Durststrecke zwischen ideellem Aufbegehren und tatsächlicher Umsetzung der Veränderungen überwinden läßt. Sie müssen Zeit "gewinnen". Durch die Zählweise "Jahr 1 der Republik", "Jahr 2 der Republik", machen die Franzosen sich Mut. Die Existenz des revolutionierten Zustandes über eine Zeit von immerhin mehreren Jahren hinweg stärkt den Glauben an seine Beständigkeit.

Für den Beginn einer neuen Zeitrechnung nach den jeweiligen Revolutionen nennen die europäischen Völker daher völlig unterschiedliche Daten.

Das hilft die "Krise" überwinden, denn nichts anderes ist eine Revolution. "Ein Volk im ganzen kann in seiner Substanz nur umgebildet werden, wenn mindestens dreißig Jahrgänge, eine ganze Generation, in eine solche Umbildung verstrickt werden. Keine Revolution ist vollendet, ehe nicht die Jahrgänge, die sie angehoben haben, und die Jahrgänge, die nichts mehr von dem wissen, was vor ihr gewesen ist, sich begegnet sind und diese von jenen den Sinn des Unternehmens überliefert erhalten haben”.(22) Es handelt sich also um einen geistigen Vorgang. Das Neue muß wenigstens einmal als neue Selbstverständlichkeit an die nächste Generation überliefert worden sein. Nur diese lange Zeit ist Garant für ein Gelingen einer Revolution. Denn, um nachhaltige Wirkung zu zeigen, müssen ganze Generationen eines Volkes den gleichen Leidensweg durchschreiten, um eine Umwälzung völlig akzeptieren zu können. Daher kann auch die deutsche Revolution von 1848 nicht als gleichwertig neben die französische oder englische gestellt werden. Sie hat zu sehr die kurze Dauer eines Staatsstreichs von unten.

In Zahlen:

Welt/Rußland: Weltkrieg I 1917-1918 bis Weltkrieg II 1941-1945\
Frankreich: 1789-1792; 1792-1815\
England: 1649-1660; 1688/89\
Deutschland: 1517-1555; 1756-1763 Preußen; 1740-1745 Österreich\
Italien: 1200-1268\
Papsttum: 1075-1 122

Dies sind die Zeiten der Krise, die Zeiten der "Empörung".

Eine Revolution bricht immer mit einem präzisen Datum aus, und ebenso genau läßt sich das Ende datieren. Sie stellt einen Bruch mit einem Rechtszustand dar. Durch die Verachtung des alten Rechtszustandes wird Recht als vernünftiger Ausdruck des Gemeinschaftslebens und der Machtverhältnisse nicht mehr anerkannt und aus den Angeln gehoben. Der Weg zur Veränderung ist damit gezeigt. Im Unterschied zum Staatsstreich jedoch ist die Revolution nicht einfach eine Aufkündigung einer gültigen Ordnung, sondern sie spricht eine völlig neue Sprache. Die Weiterentwicklung von Liturgie, Rhetorik, geschriebener, wie gesprochener Sprache ist Zeichen einer jeden Revolution. Daher rührt auch das völlige Unverständnis zwischen Revolutionären und Konservativen. Sie sind sich nicht nur geistig uneins, sie sprechen auch völlig andere Sprachen. Karl V. kann Luther über die Umwertung hinweg nicht verstehen, ebensowenig wie Burke einen Robespierre oder Poincaré einen Lenin. Die Zeit (Rosenstock-Huessy zitiert einen Kernsatz Hamlets) in jenen Epochen ist "wahrhaft aus den Fugen" (24) Es sind nicht die verschiedenen Muttersprachen, sondern die Italiener sprechen diplomatisch, die Deutschen konfessionell, die Engländer juristisch, die Franzosen philosophisch und die Russen nationalökonomisch. Über den Sinnbruch einer Revolution hinweg können die Vertreter des Alten und des Neuen sich nicht verstehen, sie halten sich für wahnsinnig und sind beide empört. Es kann also ein Ende der Krise erst kommen, wenn sowohl "die Alten" als auch „die Neuen” das erste gemeinsame Wort sprechen. Wenn also die Alten ein Stück revolutionären Geistes zulassen und der neue Geist "die leibliche Fortdauer der Reste des Alten genehmigen. Beide gewähren sich also durchaus nicht dasselbe". (26) "Ein Zeitalter der Revolution schließt nie mit der vollständigen Rasur des alten Menschen, sondern mit einem „neuen Bund.” Das Remis ist also nie ein Kompromiß, das wäre für eine Revolution unmöglich, sondern es ist ein Zusammenleben vorrevolutionärer Menschen, die sich für die neuen Ideen öffnen, mit Revolutionären, die ihre Gegner nicht mehr unter die Guillotine bringen. Man macht "kein Zugeständnis in ideeller Hinsicht"(27).

Man einigt sich auf Personen als Träger der neuen Rechtsordnung. Denn es geht zunächst um das pragmatische Zusammenleben der Menschen. Es wird die Revolutions-Fortführung in die Hände einer neuen Gruppe gelegt. So wurde es 1555 den Fürsten als neuen Päpsten in ihren Ländern überlassen, weiter zu reformieren. Damit war die "Empörung" zwar nicht in ideeller Hinsicht beendet, aber in menschlicher und personeller Hinsicht vorbei.

Die abgeleiteten Nachahmungen bringen zwar neue Rechtssätze in weitere Länder, können die neuen Personengruppen, auf die es ankäme, aber nicht schaffen.

Ein zweites Gesetz ergänzt die Lehre von den Revolutionen: Jeder folgt ein Zeitalter der Verblendung, der Selbstüberschätzung der Revolutionäre, und danach ein Zeitalter der Demütigung. Z. B. haben die deutschen Religionsparteien am Ende des 16. Jahrhunderts in ihrem Übermut den Reichstag lahmgelegt mit der Folge des Dreißigjährigen Krieges, der Deutschland zum Kampfplatz für Schweden und Franzosen machte.

Die Zeit der "Demütigung" scheint die freigesetzte Energie der Revolution wieder einzuebnen. So folgt auf jede Revolution ein Krieg, der den Übermut der Veränderung verblassen läßt. Die Greuel und Leiden dieser ebenso langen Zeit lassen die Errungenschaften der "Empörung" nutzlos und wertlos erscheinen. So ist die "Demütigung" die Zeit der Prüfung, in der sich entscheidet, ob eine Revolution Bestand haben wird. Nur wenn der Impuls stark genug war, kann die erreichte Freiheit die Prüfungszeit überstehen.

Spanien und Schweden bieten Beispiele für das Scheitern in dieser Prüfungszeit. Rosenstock-Huessy bezeichnet derartige historische Fälle als Halbrevolutionen. Da tun sich lauter künftige Forschungsfelder auf; z.B. die Anwendung dieses Konzepts der Halbrevolutionen auf die spanische, niederländische oder polnische Geschichte. Darauf weist das Revolutionsbuch nur hin, ohne es auszuarbeiten.

In der folgenden Übersicht hast Du die Rhythmen der Revolutionen beisammen:

1. 1075-1099-1122\
   Anfang: Dictatus Papae.\
   Ende Wormser Konkordat.\
   Dazwischen Erster Kreuzzug\
   1125-1137 Überhebung. Kaiser Lothar III. als Lehnsherr des Papstes.\
   1152-1177-1198 Prüfungszeit: Friedrich I. und Heinrich VI.; Friede von Venedig

2. 1200-1239-1268\
   Ausbruch: Deliberatio de statu imperii.\
   Ende: Hinrichtung Konradins\
   Dazwischen: Kreuzzug gegen den Kaiser\
   1271-1302 Überhebung: Bann und Interdikt als rein weltliche Mittel\
   1305-1347-1377 Prüfungszeit: Exil in Avignon. Rienzi

3. 1517-1535-1555\
   Anfang: Luthers Thesen.\
   Ende: Augsburger Religionsfrieden.\
   Dazwischen: Thomas Morus hingerichtet\
   1564-1618 Überhebung: Die Kirche als Beute der Fürsten\
   1618-1648-1654 Prüfungszeit: Dreißigjähriger Krieg. Jüngster Reichstagsabschied

4. 1641-1649-1689\
    Anfang: Grand Remonstrance; die Gemeinen gehen vor das Volk.\
    Ende: Declaration of Rights.\
    Dazwischen: Karl I. hingerichtet.\
    1714-1763 Überhebung der Dukes\
    1775-1783-1815 Verlust des ersten Empires. Schlacht bei New Orleans

5. 1789-1792-1814/15\
   Anfang: Bastillesturm.\
   Ende: Die Charte.\
   Dazwischen: Valmy und Erfurt.\
   1830-1848 Überhebung. "Enrichissez-vous"\
   1848-1851-1870-1875 Prüfungszeit: zwei Arbeiteraufstände und Napoleon III. Die Republik dank einer Stimme Mehrheit.

6. &nbsp;
    1. 1904/05-1914-1917-1942-1950\
       Anfang: erste Potemkinmeuterei, zweite Meuterei im Weltkrieg.\
       Ende: Die "Kirche" zugelassen. Die Sprache als dritte Kraft 1942 und 1950.
    2. 1904-1917-1941-1951 Die Welt wird rund in Korea 1904/05 und 1950/51; einmal als russisch japanischer, dann als chinesisch-amerikanischer Konflikt.\
       1917-1920 Amerikas erster Weltkrieg\
       1941 Amerikas zweiter Weltkrieg

Mit dem politischen Horizont fragt Rosenstock-Huessy „nach dem Erdraum, den dieser Zeitenstrom heimsucht” (33), nach Abendland oder Europa.

Während das Wort Abendland die Einzahl betont stillschweigend aber „Kaiser und Papst, Nationen und Königreiche mit voraussetzt"(36), heben wir mit Europa die Mehrzahl der politischen Agenten, der Nationalstaaten hervor, das Konzert der Mächte, aber implizit verstehen wir Europa auch als Einheit: z. B. bei der Frage, ob die Türkei dazugehört, über die ihr in Brüssel beratet und mitentscheidet. 1856 am Ende des Krimkriegs wird sie in das europäische Konzert aufgenommen, die Heilige Allianz, die nur den christlichen Souveränen offenstand,
hatte sie noch ausgeschlossen,

Europa ist immer mehr der rein geographische Begriff als ein Erdteil unter anderen und immer gleichgültiger gegen Kirche und Christentum geworden.
"Die europäische Kultur steht selbständig als etwas Freies, Gottloses, Außerchristliches in den Jahrzehnten von 18711914 aufrecht, um dann im Weltkrieg umkämpft, verhöhnt, verteidigt, bewiesen und zugrundegerichtet zu werden". (35)

Von außen erscheint Europa weiterhin als Einheit mit einem "besonderen Europäischen Geist" (36)

Wie ist diese "Mischbedeutung von Geographie und Geist"(36) zustande gekommen? Der Glaube an ein geistiges Privileg, kraft dessen "Europa die kulturelle Vorherrschaft über alle Kontinente und Völker"(36) gebührt, entstand im Zeitalter der großen Entdeckungen im Himmel (Kopernikus, Galilei) und auf Erden (Kolumbus, Vasco da Gama)

Für dieses Europa spielt die antike Mythologie (Zeus als Stier entführt Europa, Tochter des Phönix, nach Kreta und hat Knossos und Athen als Mittelpunkt) keine Rolle, sondern das Hochgefühl selbstverständlicher Vorherrschaft Europas setzt sich mit der humanistischen Renaissance durch. Papst Pius II. verfaßt eine Schrift "Europa". Er setzt die Christenheit mit Europa gleich, und der Name ist ihm Gegenstand religiöspatriotischer Verehrung. Die englische Lehre vom europäischen Gleichgewicht setzt voraus, daß es nur auf die Mächte dieses Erdteils ankommt. Auch dieses Bild der Balance impliziert die Einheit und betont die Vielheit. Dasselbe gilt von der Metapher des Europäischen Konzerts. (con-: mit, zusammen; certare: kämpfen, streiten, wetteifern)

Das Gemeinsame der europäischen Kultur erkennt Rosenstock-Huessy in ihrer Unabhängigkeit und Freiheit.

Freiheit ist aber der Schlachtruf aller großen europäischen Revolutionen.

Rosenstock-Huessy hält Spenglers Versuch, den Namen Abendland für Europa wieder einzubürgern, für vergeblich, aber der Namenswechsel zeigt ihm die Epoche in der Mitte des zweiten Jahrtausends. Indem er hinter die Vokabel Europa zurückgeht, findet er "im Abendland zu den Quellen aller europäischen Freiheit und Kultur". (39) Europa erweitert den Umfang des Abendlandes um Rußland und den Balkan, d. h. um die slawisch-orthodoxen Völker. Das gilt zunächst nur geographisch, "aber politisch und kulturell wartet und vertraut das übrige Europa auf ihre Europäisierung".(40) Peter der Große hat das beherzt in die Wege geleitet.

Das Abendland hat ein anderes Gegenüber, der ältere Gegensatz ist das Morgenland. Die Zeit des Abendlandes ist die Zeit der Kreuzzüge. Das Abendland hat im Morgenland seine kulturelle Pflicht gegenüber der Vergangenheit zu erfüllen versucht. Es drängt damit in den verlorenen Raum der Antike, es strebt nach dem Besitz des Heiligen Grabes. Das Reich Karls des Großen ist sein ursprünglicher Umfang.

Europa verlagert seinen Schwerpunkt nach Norden über die Alpen und Karpaten mit Paris, London und Berlin als Zentren. Es sieht in Osteuropa seine kulturelle Zukunftsaufgabe.

Es gibt zwei Okzidente.

"Das Abendland der Kreuzzüge ist nicht der Okzident der antiken Welt"(41), der die Küsten des Mittelmeers umfaßt. Karl der Gr. beherrscht ein Binnenreich (ohne Flotte) mit Paris, Aachen und Frankfurt als Kern.

Schon am Hof Karls d. Gr. taucht der Name Europa als Selbstbezeichnung auf. Die afrikanischen und asiatischen Teile des Römerreichs gehören nicht mehr dazu. Das Ungeheure, ein solches riesiges Festland zu organisieren, ist ein historisches Novum gegenüber den küstennahen Städten rund ums Mittelmeer als universeller Verkehrsader.

Karl überträgt die Rechtsordnung der antiken Polis, der römischen Res Publica auf moderne Flächenstaaten. Alle politischen Begriffe der Antike bezeichnen im zweiten Jahrtausend die Lebensformen riesiger Staatsgebiete mit Hunderttausenden von Quadratkilometern, also völlig neuen Größenordnungen mit neuen Schwierigkeiten, die neue Leistungen erfordern.

Das Reich der Franken unterscheidet sich von denen anderer Germanenstämme dadurch, daß die Franken zunächst auf römischem Reichsboden kolonisieren und von dieser Basis aus unter Karl andere Stammesgebiete erobern. Karl faßt den Gedanken der translatio, der Übertragung der Herrschaft Konstantins auf sein Reich, d. h. er übernahm die Verantwortung für die Kirche, die in seiner Zeit den geistigen Extrakt der Antike überlieferte, die Quintessenz "ihre zum Überwintern bestimmte Frucht" (Fallmerayer)", nämlich Schrift, Literatur, Ackerbau, Bankwesen.

Seit Konstantins Vorsitz auf dem Konzil von Nicaea 325 eröffnet der Kaiser der Kirche einen befriedeten Raum zu ihrer Entfaltung, er wehrt Ketzer und Schismatiker ab, indem er den Streit um das Jota entscheidet (ob Christus Gott gleich oder ähnlich sei, homoios oder homoos); schließlich verleiht er Patriarchen und Konzilien Autorität. Dieses Kirchenamt wird am Weihnachtstag 800 Karl d. Gr. übertragen.


Was seit dem 10. Jahrhundert Abendland heißt, "ist der Versuch und die Aufgabe, eine … kirchlich-römische Organisation der vom Kaiser beherrschten Gebiete zu verwirklichen". (43)

Diese Einheit stiftende und sie organisierende Kirche mit dem Kaiser in der Rolle ihres Zusammenhalters unterscheidet Karls Reich von allen anderen germanischen und dem Attilas und verleiht ihm Dauer.

Europas ist mit dem Abendland in einen Wettkampf getreten.

Bis 1500 bleibt die ausdrückliche Einheit durch die Kirche unbestritten und offensichtlich. Der Wandel beginnt 1492 mit der Entdeckung Amerikas. Damit wurde Europa ein Morgenland oder die Alte Welt, ein Name, der heute auf Europa "lastet". (45) Abendland ist ein Renaissance-Begriff, die christliche Antike sollte wiedergeboren werden. Nur der christliche Teil schien würdig, wiedergeboren zu werden. Er versprach die ersehnte Einheit durch ein geistiges Prinzip: Die unmittelbar vorhergehende Epoche wird erneuert, aber bei gänzlich verschiedenen Daseinsbedingungen.

Daß Europa ein Renaissancebegriff ist, gehört zu unseren Selbstverständlichkeiten. Es ist aber auch ein "Konkurrenzbegriff gegen Abendland" (46) Die Renovatio der christlichen Kirche herrscht bis 1500 und wird seitdem geduldet, die Wiedergeburt des klassischen Altertums wird bis 1500 geduldet und herrscht bis 1917, dem Einbruch der außereuropäischen Welt (Kriegseintritt Amerikas).

Der Name Europa paßte um 1500 besser, er bezeichnete die gemeinten Gebiete besser, die Wohnsitze der Europäer, und er mahnte nicht wie Abendland an das völlige Scheitern der Kreuzzüge und die Eroberung Konstantinopels 1453.

Die beiden Namen bezeichnen den "unlöslichen und sinnvollen Zusammenhang zwischen Mittelalter und Neuzeit, der in dem Weltbild der reinen "Neuzeitler", der Europäer, Protestanten und Humanisten verpönt war".(47)

Der zweite Nachteil des neuen Namens besteht darin, daß der geographische Begriff die Teilung in immer kleinere Einheiten zuläßt, eine "Bewegung auf eine stets wachsende Vervielfältigung" (48). Die Suche nach natürlichen Grenzen, die rein geographische Sicht zersetzt den seelisch-kulturellen Inhalt Europas. Er "ist im Weltkrieg verdampft". Als Kultur-Anspruch waren Abendland und Europa aufeinander angewiesen.

Damit kann Rosenstock-Huessy das pluralistische Weltbild der verschiedenen Europäer entfalten:

So wie jede Revolution eine neue Zeitrechnung beginnen möchte, so entwickelt jede eine eigene "Erdansicht" (49). Alle diese Horizonte drücken Werturteile aus" (53) Die Römische Kirche, die Ecclesia Romana umfaßt mit den Kreuzzugsgebieten Konstantinopel und Jerusalem. Hier will der Papst urbi et orbi das Gesetz geben, "den Orbis als seine Urbs anreden."

Die Christenheit wehrt sich seit 1400 gegen die Türkengefahr als "gemeiner Christenheit Erbfeind". Frankreich und Habsburg sind ihre Vormacht, Papst und Kleriker fehlen nach Luther.

In der englischen Revolution wird Großbritannien Seemacht. "Für England tritt die Welt der Meere zu "gemeiner Christenheit" hinzu. Seine Kolonien haben den Charakter einer Vervielfältigung des Mutterlandes,

Das Europa Frankreichs vermehrt die Staaten um ihre Kolonialreiche, aber ohne das Commonwealth in Übersee. Französische Kolonien sind minderen Rechts und zur Ausbeutung bestimmt, ein klar imperialistisch-kapitalistischer Begriff von Europa mithin.

Für die russische Weltrevolution ist der politische Horizont die Welt.

Die Geschichtsbücher der europäischen Völker gestehen sich nur ungern ein, daß jede Revolution für den ganzen Kulturkreis einen neuen Ton angeschlagen hat, der für alle eine neue Dimension der Freiheit schafft. Alle müssen daher auch die Sprache der fremden Revolutionen lernen. Das erfordert eine "geistige(n) Gebietsabtretung" (55), sie erzwingt die Revision des selbstverständlichen Geschichtsbildes, sie vernichtet einen Teil der Tradition, aus d e r Geschichte wird ein Teil der Geschichte.

Zugleich werden die Sprecher der alten Sprache gezwungen, ihren Logos um so schärfer zu artikulieren und die Unterschiede zu explizieren.

Die Deutschen haben das durch die russische Revolution am stärksten erfahren: "Deutschland hat sich nicht nur entleibt, sondern auch entgeistigt."(55) Hitler und seine Wähler haben uns dieses Trümmerfeld hinterlassen.

"Geschichte ist die Fortsetzung der Revolution mit andern Mitteln" Rosenstock-Huessy variiert damit den Satz von Clausewitz: Krieg ist die Fortsetzung der Politik mit Beimischung anderer Mittel.

Die wirkliche Politik ist heute (1960) atlantisch oder planetarisch:

Die Pole Europa Abendland wirken nicht mehr, die Gespenster dieser Vergangenheit müssen aber noch gebannt werden. Rosenstock-Huessy zählt dazu Staat, Kirche, Nation, Klasse, Kapitalismus, Kommunismus, Kultur, Kunst und Wissenschaft. (XI) Er fragt bei jeder dieser Früchte europäischer Geschichtsräume aus dem 2. Jahrtausend, welchen Beitrag zu einem friedlichen globalen Weiterleben im 3. Jahrtausend sie dem Menschengeschlecht leisten können, wenn sie aus dem Nacheinander in die Gleichzeitigkeit rücken.

Der folgende Abschnitt über den zweiten Titelbegriff der Nation markiert nach meiner Auffassung Rosenstock-Huessys größte Leistung: Anders als die sog. objektiven Historiker, deren nationale Perspektive implizit in ihre Darstellung eingeht, schreibt Rosenstock-Huessy ein übernationales Geschichtsbuch, nicht indem er von den Nationen abstrahiert, sondern sie in ihrer Verschiedenheit und Lebensfülle, der politischen, rechtlichen, künstlerischen, religiösen, wirtschaftlichen und militärischen ernstnimmt.

Deshalb glaube ich, daß es Europa guttun würde, wenn Schüler, Studenten, Lehrer, Journalisten, Diplomaten und Politiker überall in Europa diesen Überblick gewinnen wollten.

Mache Du einen Anfang.

Von den noch übrigen Kapiteln der Theorie biete ich Dir nur noch das Knochengerüst, das Rosenstock-Huessy bisher schon immer in seinen Tabellen herauspräpariert hat. Überlege mal selbst, was er sich dabei gedacht hat, oder lasse Deine Kollegen mitdenken.

Du findest sie in der dritten Ausgabe bei Brendow 1987 auf den Seiten 66, 70/71, 74, 75, 92, 97 und 104.

Enger, Pfingsten 2005


Dein alter Vater.
