---
title: Jahrestagung 2024
created: 2023
category: jahrestagung
thema: "Haben wir das Friedenschließen verlernt? - Die Lektion Rosenstock-Huessys für ein friedliches Miteinander"
published: 2024-08-23
landingpage: 
order-lp: 1
---

### {{ page.thema }}

![Haus am Turm]({{ 'assets/images/Haus_am_Turm_Essen.jpg' | relative_url }}){:.img-right.img-large}

Datum: Freitag 11.10. - Sonntag 13.10.2024
<!--more-->


Eugen Rosenstock-Huessys Entdeckung des Kreuzes der Wirklichkeit ist nicht nur didaktische Erweiterung der Subjekt-Objekt-Beziehung von Ich-Es zur Vierzahl Du-Ich-Wir-Es, also ein Hinzufügen zweier fehlender Gesichtspunkte. Seine Entdeckung impliziert, von ihm selber oft aufgezeigt und bis jetzt noch verkannt, eine neue Grundlegung der sozialen Wissenschaften, ein Novum Organum für das dritte Jahrtausend.

In einer Zeit, in der sich, aus dem Gewahrwerden der Unterschiede, soziale Gruppen und Nationen auseinander bewegen und bekämpfen, stellt sich die Frage ob, und gegebenenfalls wie, ein friedliches Zusammenleben möglich ist und wie es gestaltet werden kann.

Rosenstock-Huessy deckt die sprachliche Verfasstheit sozialer Gruppen auf und zeigt wie wir im Angesprochenwerden und im Gegenwort auf eine Weise gebildet werden, die überkommene Grenzen überwinden und neue Einheiten stiften kann.

Auf unserer Jahrestagung mit dem Thema: *„Haben wir das Friedenschließen verlernt? - Die Lektion Rosenstock-Huessys für ein friedliches Miteinander”* wollen wir dem nachgehen. Als Textgrundlage sollen uns die beiden Stücke aus dem Jahre 1954: [Dich und Mich dienen]({{ 'text-erh-dich-und-mich-1954' | relative_url }}).[^107][^108]

[^107]:Rosenstock-Huessy, Eugen, Dich und Mich. Was folgt aus der empirischen Grammatik, in: Neues Abendland, 9.Jg., H.12 (1954), S.719-727.
[^108]:Rosenstock-Huessy, Eugen, Dich und Mich. Lehre oder Mode?, in: Neues Abendland, 9.Jg., H.11 (1954), S.462ff.

Die Jahrestagung findet vom Freitag 11.10. - Sonntag 13.10.2024 im Haus am Turm, Essen statt. Die Mitgliederversammlung wollen wir am Sonntagmorgen abhalten. Sie sind herzlich eingeladen!

Um eine gute Planbarkeit zu gewährleisten, wollen wir sie bitten uns Ihre Teilnahme baldmöglichst  mitzuteilen.

#### Programm der Jahrestagung:

***Haben wir das Friedenschließen verlernt?\
Die Lektion Rosenstock-Huessys für ein friedliches Miteinander***

| Freitag, 11.10 | 18:00 | Eintreffen |
|                | 18:30 | Abendessen |
|                | 19:30 | Gesprächsfähigkeit gesucht |
|                | 21:00 | Geselliges Beisammensein |
| Samstag, 12.10 |  8:30 | Frühstück |
|                |  9:30 | Textlesung: „Dich und Mich” |
|                | 10:45 | Pause |
|                | 11:00 | Stand der Wissenschaft bzgl. Rosenstock-Huessys Lehre |
|                | 12:30 | Mittagessen |
|                | 15:00 | Kaffeepause |
|                | 15:30 | Frieden zwischen Sachsen und Franken. Werdener Führung |
|                | 18:30 | Abendessen |
|                | 20:00 | Dialogisches Musizieren nach Goethe |
|                | 21:30 | Geselliges Beisammensein |
| Sonntag, 13.10 |  8:00 | Morgenandacht |
|                |  8:30 | Frühstück |
|                |  9:30 | Rückblick – Plenumsgespräch |
|                | 10:45 | Mitgliederversammlung |
|                | 12:30 | Mittagessen |
|                | 13:00 | Abreise |

>*Jürgen Müller*


Ort: Haus am Turm, Essen
