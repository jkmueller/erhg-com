---
title: "Wilmy Verhage: Dochters"
category: weitersager
order: 11
---

Dochters\
Die Tochter – Das Buch Rut


Amsterdam, april–oktober 2007

Lieve Marie Louise,

Je bent nu al de tweede vrouw die ik ken, die het boek ‘Die Tochter’ van Eugen Rosenstock-Huessy cadeau heeft gekregen, en het niet durft te lezen.

Is de titel al zo beladen?

Ben je bang, dat het boek weer van alles van je vraagt, wat je helemaal niet wilt of kunt geven? Dat het je met een nieuwe vorm van bevoogding opzadelt, terwijl je het oude juk nog nauwelijks van je af hebt geschud?

Ik kan me voorstellen dat de aanleiding waardoor jij het boek hebt gekregen ook niet bepaald als een aanbeveling heeft gewerkt. Je kreeg het als gebaar van je nieuwe ouders toen je officieel geadopteerd zou worden, wat uiteindelijk toch niet is doorgegaan.


Het boek gaat eigenlijk helemaal niet over vrouwen. Het gaat over mannen. Het gaat over het geestelijk failliet van mannen, zoals Eugen Rosenstock-Huessy dat aan de eerste wereldoorlog heeft ervaren en wat hij in dit opstel onder woorden brengt.

Hij schrijft zich uit het diepe dal van de wanhoop. „We zeggen het elkaar niet. Want we kunnen het wezenlijke niet meer hardop zeggen. Elk luid woord is onverdraaglijk geworden. De geordende taal is ons al bijna te sleets. We zoeken een taal die niet meer hoeft te worden uitgesproken, waarin we ons kunnen verbergen. Het is geen leeg zwijgen, dat we zoeken. Konden we elkaar maar bereiken. We moeten elkaar bereiken”. Zo begint hij.

Zelf kunnen ze het heil niet meer brengen, de mannen. Dat heeft de wereldoorlog voldoende aangetoond, is hem duidelijk geworden. Waar moet het dan nu vandaan komen? En op het eind, 15 pagina's verder, wijst hij opgelucht, puttend uit eigen ervaring naar de dochter, hoewel ze daarvoor de minst voor de hand liggende gestalte lijkt.

Ik kreeg het boek op de kop af tien jaar geleden van mijn tante Ko, jouw heldin Ko Vos. In die tijd aarzelde ik op de drempel om Rosenstock-Huessy's woorden mijn vertrouwen te schenken en daarmee het feminisme te verlaten.

Wat voor goeds had hij een vrouw te zeggen?

Hoe werd mijn zwaar bevochten zelfstandigheid gewaarborgd?

Met die onuitgesproken slag om de arm verwikkelde ik mijn veelgeplaagde tante in een heftige intellectuele briefwisseling, waar ze al gauw schoon genoeg van kreeg. Dit boek betekende haar ‘basta’. Haar voorlopig laatste antwoord.


Ik heb de gok gewaagd. Ik ben gaan lezen. En ik werd blij verrast dat ik mijn ervaringen weerspiegeld vond in Rosenstock-Huessy's analyse van wat er mis was met de wereld. Dat er eigenlijk geen plaats was voor mij, geen ruimte voor het vrouwelijk perspectief, de vrouwelijke manier van reageren.

Geestelijk kwam ik eindelijk, eindelijk thuis.

‘Die Tochter’ werd in 1920 voor het eerst gepubliceerd in het boek „Die Hochzeit des Kriegs und der Revolution’ – het huwelijk tussen oorlog en revolutie zoals dat zich voor zijn ogen en aan zichzelf voltrok.

In 1961 vroeg hij tijdens een treinreis aan Bas Leenman na zijn dood daaruit het hoofdstuk „die Tochter” uit te geven, samen met het boek Ruth in de bijbelvertaling van zijn vrienden Franz Rozenzweig en Martin Buber. „In een mooie witte band”, voegde hij er nog aan toe.

Dankzij een schenking van Ko Vos is dat in 1988 gebeurd, ter gelegenheid van zijn 100ste geboortejaar.

Waarom samen met Ruth? Waarom een mooi wit bandje? Die vraag heeft me lang bezig gehouden. Uiteindelijk heb ik het als volgt begrepen: In de geschiedenis van Ruth uit de bijbel vindt hij de kern van de gestalte van de dochter, de stem die, naar hij zegt, ontbreekt in het maatschappelijk concert.

Er wordt in verhaald, hoe Ruth met haar schoonmoeder Naomi mee wil en vervolgens ook gaat naar Israel, nadat zij haar man aan de dood verloor. Naomi was dan al 10 jaar weduwe. Naomi en haar man waren door een hongersnood gedwongen uit Bethlehem vertrokken naar een stam verderop, de Moabiten, om aan de kost te kunnen komen. Ze hadden twee zoons. Die zijn getrouwd met moabitische vrouwen, onder wie Ruth. Dan sterven ook beide zoons. Ik ben zelf weduwe, dus ik kan je uit ervaring vertellen wat dat met je doet. Een van de consequenties is dat de bodem onder je bestaan wegvalt in maatschappelijk en economisch opzicht. In die tijd betekende het dat als je niet door een broer van je overleden man kon worden geadopteerd als echtgenote, als je nog geen eigen zoons had, je terugging naar je familie, naar je eigen stam.

De andere schoondochter keert dan ook terug naar haar vader, hoewel zij veel liever bij Naomi was gebleven. Ook Naomi, de schoonmoeder keert terug naar haar verwanten, naar Bethlehem. Alleen Ruth gaat niet terug, zij trekt verder. Kennelijk had en nam ze op dat moment van machtsvacuüm, zo direct na de dood van haar man, de vrijheid om te kiezen. Het uitzonderlijke van haar situatie gaf haar die kans.

Dus Ruth doorbreekt het gewoonterecht: ze nam integendeel zelf het initiatief, alsjeblieft niet terug, maar verder in het onbekende, mee met de schoonmoeder van wie ze kennelijk zielsveel is gaan houdenen dan maar hopen dat het goed komt. Naomi, daar en op dat moment de geest van Israel vertegenwoordigend, stond het haar toe.

Ruth moet bijzonder aangenaam verrast zijn geweest door de geest die ze in huize Naomi trof om de gok te durven wagen: ik neem tenminste aan dat het de geest van de God van Israel was, en dat die een beduidend andere leefsfeer schiep dan de geest die waaide in haar eigen stam.

En de rest van het verhaal gaat erover hoe ze erin slaagt een eigen plek te krijgen. Door het toeval te laten regeren en de kennis, ervaring en intuïtie van Naomi te volgen. Hoe ze zich in alle bescheidenheid aanbiedt, wekt de achting en liefde op van een belangrijk man, Boas. Die schiep vervolgens nieuwe voorwaarden, samen met de mannen die het op dat moment in Bethlehem voor het zeggen hadden, om Ruth als dochter van Israel, dus niet als slavin, te kunnen opnemen, waardoor hij met haar kon huwen.

Met die ene daad van vrije keuze, die stap in het onbekende, regenereerde Ruth Israel. Want voortaan kon elke vrouw uit den vreemde zo worden opgenomen. Ze wordt als een van de stammoeders genoemd van David en vervolgens dus ook van Jezus.

Het was nogal een schok voor mij toen ik Die Tochter voor het eerst las en kennis maakte met het analysekader van Rosenstock-Huessy. Zo had nog nooit iemand in mijn omgeving het bekeken en het heeft dan ook lang geduurd, voordat ik me zijn kijk ook eigen kon maken, ik zit eigenlijk nog steeds in dat proces. En ik kan het niet voor je doen, maar misschien helpt mijn (voorlopige) begrip.

Wij zijn gewend als we het over mensen hebben van individuen uit te gaan. Mannen en vrouwen gelijk. Maar Rosenstock-Huessy zegt dat het individu in feite een tussenfase is, tussen de ene verbintenis en de volgende. Hij ordent het inter-menselijk gebeuren in vaders, moeders, dochters en zonen. Niet als biologische verschijnselen, maar als vier verschillende maatschappelijke gestalten. Het is de eerste ordeningsvorm die een mens – als babyleert kennen. En tegelijkertijd is het ook de oudste samenlevingsordening. Zoals elke nieuw ontdekte stam uit prehistorische tijden weer bewijst, zij het in Nieuw Guynea, dan wel in het Amazonegebied.

De vier gestalten van de mens zijn in Rosenstock-Huessy's optiek bovenpersoonlijke, op elkaar betrokken ambten, die door elke generatie opnieuw worden bekleed en doorlopen, ieder op haar of zijn eigen manier. Als het goed is doorloopt elk mens het proces van dochter naar moeder naar vrouw of van zoon naar man naar vader. Eerst ben je een dochter en het moederschap maakt je tot een zelfstandige vrouw, of eerst ben je een zoon en je zelfstandigheid als man maakt je geschikt tot vader. Die accenten liggen bij de fases die mannen en vrouwen doorlopen verschillend, legt hij uit in de ‘Soziologie’ . (Daarvoor hoef je overigens niet getrouwd te zijn of daadwerkelijk baby's te krijgen, alles waar je zorg naar uitgaat dient tot opwekking van moederlijk of vaderlijk gedrag.)

De vier grondhoudingen vader, moeder, zoon en dochter vormen evenzoveel verschillende kanalen waardoor de logos, de taal en de instituties die daaruit voortvloeien, zich ontplooit, in elk mens afzonderlijk, in de samenleving als geheel.

Althans zo zou het moeten zijn. Maar Rosenstock-Huessy constateert in ‘Die Tochter’ dat de stem van de dochter ontbreekt in de compositie. Dat de maatschappelijke krachten die tot wasdom zijn gekomen bestaan uit die van de vader, de moeder en de zoon. Geestelijk vertaald: natuur, cultuur en geest. En maatschappelijk gezien: Staat (de vader), Kerk (de moeder) en Wetenschap (de zoon).

De ziel, de samenleving zelf, het dochterlijke element, ontbreekt in het bewustzijn, in de taal van zijn, zeg maar rustig onze, tijd.

Nu, 90 jaar nadat Rosenstock-Huessy ‘Die Tochter’ had geschreven, na een crisis en nog een wereldoorlog en een koude oorlog, kun je rustig stellen dat ‘Vader Staat’ van zijn voetstuk is gevallen, dat ‘Moeder Kerk’ is leeggelopen, en dat ‘Zoon Wetenschap’ zich alles denkt te kunnen permitteren. Alle drie dorsten ze naar regeneratie door bezieling.

In de strijd tegen hun zintuigen, tegen de lichamelijke kant van hun ziel, hebben mannen hun onderscheidingsvermogen verloren, die God de zijnen had toevertrouwd: het zintuig voor echt en onecht, voor dood en levend, zegt Rosenstock-Huessy. En dat is de kern van het euvel.\
Hij constateert dat tweeduizend jaar geleden in Jezus Christus een nieuwe bron van leven is gaan stromen, die gebroken harten kon helen, maar dat mannen die bron in de loop der eeuwen zijn gaan beschouwen als hun persoonlijk eigendom: De katholieken hebben hem als "Heilige Geest" gevangen gezet en ingemetseld in hun kerkleer; de protestanten hebben haar werking geloochend in haar gang door de geschiedenis.

*”In hun strijd tegen hun aangeboren natuur heeft de katholiek geleerd zijn lichaam te haten en mag de protestant schoonheid niet ervaren,”* observeert hij, en vervolgt: *„Het is die afstomping van levensinstincten, die mannen heeft verleid zich boven God te verheffen. Het deed ze het feilloze gevoel verliezen te kunnen bepalen waar God staat en waar hijzelf staat.”* Het heeft ook hun verhouding tot vrouwen bepaald: de katholiek is haar ontvlucht, de protestant heeft haar aan huis gebonden, de liberaal – het genie – doet haar af als een niet serieus te nemen schepsel.

Deze analyses heb ik negentig jaar na dato herkend in mijn eigen leven. De katholiek die zijn lichaam haat, de protestant zonder smaakgevoel, ik heb me aan ze bezeerd. De vrouw als bezit, als een in haar uitspraken niet serieus te nemen object… ach, je kent die positie als geen ander.

Rosenstock-Huessy concludeert: *„De tijd dat een rigoureuze scheiding van lichaam en geest de weg wees naar een beter leven is voorbij. Alleen vanuit de eenheid van lichaam en geest kan heling komen, alleen de ziel verankerd in lichaam en geest, vind genade in Gods ogen.”*

Dochter samenleving zegt hij, hoopt op vrouwen in dochterlijke gedaante. Dat die er ook komen is niet zo vanzelfsprekend als het lijkt, voeg ik daar aan toe, want het dictaat van de gelijkheid voedt vrouwen op tot gemankeerde mannen. En de dictatuur van het schoonheidsideaal tot anorexiepatiënten.

Hij voorspelt: niet bepaald door de leer der vaders komen deze vrouwen tevoorschijn, niet als dochters van vaders, maar als dochters der schepping, die toegeven aan hun drang tot liefde en verlangen, spontaan hoe hun hart het hun ingeeft. (De cursivering is van mij).

Uit hun ontmoeting met de zonen die beseffen hoe hun eigen geschiedenis is vastgelopen, komt nieuwe geest voort, zegt hij, puttend uit eigen ervaring.

Want natuurlijk is de dochter als model voor de samenleving geen abstract ideaal. Rosenstock-Huessy stond een concreet beeld voor ogen. Hij is eerst zelf gered.

De vrouw in kwestie was zijn eigen vrouw Margrit Huessy. Die is, doordat hij direct na hun huwelijk onder de wapenen is geroepen, meteen in een zelfstandig leven geplonsd. En met haar liefdevolle acties, die soms lijnrecht tegen de heersende huwelijksconventies indruisten, heeft ze hem desalniettemin uit zijn wanhoop kunnen verlossen.
Hij schrijft ‘Die Tochter’ aan zijn vriend Franz Rosenzweig, die op dat moment al even vurig van Margrit houdt als hij zelf, en Margrit houdt van allebei. Dat is op zich al geen eenvoudig gegeven. Maar het heeft hen alle drie werelden van goed gedaan. Zonder Margrit's liefde hadden ze zich niet aan elkaar uit de situatie van het moment kunnen schrijven, was hun oeuvre niet ontstaan. En zonder de liefde van Franz en Eugen – voor haar, en voor elkaar, was Margrit niet uitgegroeid tot een volwassen vrouw.

Aan het eind van zijn opstel is Rosenstock-Huessy de eerste man geworden, die vrijwillig en openlijk van zijn voetstuk stapt en naast zijn, de, vrouw gaat staan. Hij is bereid te erkennen dat vrouwen nodig zijn, hun rechtstreekse contact met God onmisbaar, wil het goed komen met de wereld, met de mannen, met hemzelf.

*”We kunnen niet elkaars priester zijn, maar we kunnen onze handen zo in elkaar verstrengelen, ze worden zo in elkaar verstrengeld, dat we niet meer zonder elkaar kunnen leven, dat we moeten zeggen: ik laat je niet los, tenzij je ze zegent. Ook hier geen rust, geen ander verdragen, dan telkens meer liefhebben. Want alleen dat is werkelijk leven.”* Zegt hij tot slot.


In het voorwoord tot de Dochter stelt Bas Leenman dat het boek door en voor mannen geschreven is. En het lijkt alsof hij zich daarvoor een beetje geneert. Als het al door vrouwen wordt gelezen, vraagt hij, dan wellicht in de hoedanigheid van meelezeressen?

Welnee, Bas, jij daar inmiddels in de hemel, op deze indirecte wijze spreekt hij mij, en indertijd dus ook mijn tante Ko, juist aan!

Voor zover we uit ons hart leren spreken, eerlijk zijn in onze liefde(s) – worden we niet alleen gehoord, maar blijken wij een essentieel element in het scheppingsproces te zijn.

Mij heeft het zelfvertrouwen gegeven, het lezen van ‘Die Tochter‘. Ik ben benieuwd waar het jou zal brengen.

Liefs,

Wilmy
