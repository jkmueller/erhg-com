---
title: Jahrestagung 2008 in Loccum
created: 2008
category: jahrestagung
thema: SCHOLASTIK AKADEMIK ARGONAUTIK
---
![Loccum]({{ 'assets/images/ev_akad_loccum.jpg' | relative_url }}){:.img-right.img-large}

### {{ page.thema }}

Freitag, 14. März, bis Sonntag, 16. März 2008 in Loccum,\
Evangelische Akademie\
Münchehäger Str. 6, 31547 Rehburg-Loccum\
[www.loccum.de](https://www.loccum.de)

Freitag, 14. März\
  18 Uhr Abendessen\
  20 Uhr Referat: Eckart Wilkens: SCHOLASTIK, AKADEMIK, ARGONAUTIK -
    WAS IST IHR GEMEINSAMES, WAS UNTERSCHEIDET SIE, WIE KÖNNEN SIE ALLE ZUSAMMENWIRKEN?\
    Aussprache

Samstag, 15. März\
  8.30 Uhr Frühstück\
  9.30 Uhr Gemeinsame Bearbeitung des Textes Vivit Deus unter dem Gesichtspunkt:
      was ist zur Sicherung der Textwahrnehmung nötig -
      unter Verwendung der mitgeschickten zwölf Fragen,
      denen jeder Teilnehmer schon nachgehen kann\
  11 Uhr Pause\
  11.20 Uhr Fortsetzung der scholastischen Erörterung:
      Was steht im Text, wie wird er gewiß und was sagt er neuerdings,
      wenn er in anderen Zusammenhang gerückt wird\
  13 Uhr Mittagessen\
  14.30 Uhr Akademische Erörterung: Welchen Zweck hat der Text kämpferisch verfolgt
      und welche Position nimmt nun jeder dazu ein, in drei Gruppen,
      die von Jürgen Müller, Wilmy Verhage und Andreas Schreck geleitet werden\
  16 Uhr Berichte in der ganzen Runde\
  17 Uhr Gemeinsames Verlauten (ohne Erörterung) des Textes
      Vivit Deus durch fünf verschiedene Stimmen\
  18 Uhr Abendessen\
  20 Uhr Argonautische Erörterung: Was bedeutet mir dieser Text,
      wo berührt er meine eigene Lebenserfahrung, befragt und erneuert sie,
      in drei anderen Gruppen, die aber wieder von Jürgen Müller, Wilmy Verhage und
      Andreas Schreck geleitet werden

Sonntag, 16. März\
  7.30 Uhr Andacht, gehalten von Lothar Mack zu Psalm 18, 47\
  8.00 Uhr Frühstück\
  9.00 Uhr Evaluation der verschiedenen Erfahrungen scholastisch, akademisch, argonautisch,
    Sicherung des Tagungsergebnisses\
  10.15 Uhr MITGLIEDERVERSAMMLUNG
    Zur Sprache kommen: Stimmstein, Soziologie, Vorstandsarbeit, Website, Wikipedia,
    nächste Tagung, Kasse, Rosenstock-Huessy Fund, Respondeo\
  13.00 Uhr Mittagessen und Abschied

Praktische Informationen zur Jahrestagung 2008\
Freitag, 14. März, bis Sonntag, 16. März\
Tagungsstätte der Evangelischen Akademie Loccum,\
Münchehäger Str. 6, D-31547 Rehburg-Loccum,\
Tel. 05766-81182.\
www.loccum.de

Der Preis mit Vollpension beträgt pro Person im Einzelzimmer € 125, im Doppelzimmer € 120. Interessenten, die wegen der Kosten die Teilnahme scheuen, mögen sich bitte an den Vorstand wenden. Da im Kloster Einzelzimmer üblich sind, bitte Wunsch nach Doppelzimmer gesondert angeben.

Die Tagungskosten sind während der Tagung in bar bzw. durch Überweisung an die Gesellschaft zu begleichen.

Anreise am 14. März
Ab Bahnhof Wunstorf (S-Bahn Hannover) steht um 13.00 Uhr ein Bus der Akademie Loccum bereit. Der Bus kann wegen begrenzter Kapazität nur bei Anmeldung 2 Tage im Voraus bei Andreas Schreck (0551-3819460) genutzt werden. Fahrzeit ca. 30 Minuten.

Öffentliche Busverbindungen bestehen ab Wunstorf ZOB (neben dem Bahnhof)\
um 15.40 Uhr (Ankunft Akademie Loccum 16.50 Uhr)\
und um 17.10 Uhr (Ankunft Loccum 18.22 Uhr) mit Umsteigen in Rehburg-Loccum\
Ab Bahnhof Nienburg/Weser bestehen Busverbindungen (Linie 50) um 15.10 Uhr, 16.10 Uhr, 18.10 Uhr, Fahrzeit zur Akademie 40 Minuten.

Rückreise am 16. März\
Um 12.50 Uhr (Bus der Akademie; Fahrzeit nach Wunstorf ca. 30 Minuten) sowie um 13.26 Uhr (Bus bis Stadthagen, Ankunft Zug Wunstorf 14.38 Uhr, Ankunft Hannover Hbf 15.00 Uhr.

Anmeldung bitte an die Gesellschaft, Wilmy Verhage. Die Adresse finden Sie beim Anmeldungsformular.\
Andreas Schreck
