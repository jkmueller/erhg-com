---
title: "Rosenstock-Huessy: Friedrich Wilhelm Nietzsche (1962)"
category: online-text
published: 2024-01-10
org-publ: 1962
language: english
---
#### NIETZSCHE, FRIEDRICH WILHELM, 1844-1900,

German philosopher and theologian, was born in Rocken, near Lützen in Saxony, the son of a Lutheran minister. When Friedrich Wilhelm was four, his father died; the family then moved to Naumberg. The boy was an excellent pupil (his schoolmates called him "the little minister") except in mathematics and sports. He liked poetry, music, the Bible, and the classics. During 1858 - 64 he attended the famous boarding school at Pforta (Schulpforte). At the university in Bonn he was admitted to the faculties of theology and philosophy, but increasingly devoted himself to classical philology under Friedrich Ritschl (1806-76); when this eminent teacher went to Leipzig, Nietzsche followed. Largely through Ritschl's influence, Nietzsche was elected, 1869, professor of classical philology at the University of Basel; he held this post until 1879, when he resigned because of ill- health dating from his brief service, 1870, in the ambulance corps during the Franco-Prussian War. A friend of his Basel years, the historian Jakob Burckhardt (1818-97), said in 1869, "Nietzsche is as much an artist as a scholar” - three years before this statement was proved true by Nietzsche's first book, Die Geburt der Tragödie, 1872 (The Birth of Tragedy, Francis Golffing trans. 1956).

#### Last Years of Sanity.

In the 1880's Nietzsche wandered over most of Europe seeking a place where his health might improve. Despite the torments of disease, his mind remained sound until a few days before his mental collapse at Turin in January, 1889. The nature of Nietzsche's psychosis remains obscure; most authorities assume it to have been general paresis. Whatever the disease may have been, there are no indications of mental deterioration in the works he produced during his last years of sanity. Among these the following are especially significant:
- *Die Morgenröte*, 1881 (The Dawn);
- *Die Fröhliche Wissenschaft*, 1882 (The Joyful Science; book five added, 1887);
- *Also Sprach Zarathustra* (Thus Spoke Zarathustra: parts I and II, 1883; III, 1884; IV, 1885);
- *Jenseits von Gut und Böse*, 1886 (Beyond Good and Evil);
- *Zur Genealogie der Moral*, 1887 (Toward a Genealogy of Morals);
- *Der Fall Wagner*, 1888 (The Case of Wagner);
- *Die Götzendämmerung*, 1889 (The Twilight of the Idols).

Certain works, written earlier, were published after his collapse, by his sister, Elisabeth Förster- Nietzsche:
- *Nietzsche contra Wagner* (in part, 1895);
- *Der Antichrist* (1895);
- and the autobiographical *Ecce Homo* (1908).

Of various English translations, those of Walter Kaufmann (The Portable Nietzsche), Francis Golffing, and (though fragmentary) George Morgan are much more accurate than those in the Oscar Levy Complete Works (18 vols. 1909-13). The book My Sister and I, purportedly written by Nietzsche just before he went insane and then mysteriously lost, discovered by Levy in the 1920's, and widely circulated in an edition published (1952 and 1955) by Samuel Roth in the United States, is almost certainly spurious.

#### Nietzsche Against the Nineteenth Century.

It has been said that in his life and his work Nietzsche embodied every significant tendency in the intellectual, religious, and moral life of nineteenth century Europe, and in this sense he has been called "Europe's bad conscience." In another sense, Nietzsche set himself against the nineteenth century hypocritical morals, decadence, racism, nationalism, militarism, and sterile rationalism. Nietzsche is claimed as their precursor by twentieth century existentialists who see in him an ideal case study of the crucial moral dilemmas of modern man.

In Nietzsche's dynamic view, history consists of great cultures created by vigorous, powerful peoples but prone to lapse into decadence. In periods of decadence, more than in others, great men appear (Socrates, Jesus "the noblest man”, Shakespeare, Spinoza "the purest sage”, Goethe) - men who transcend the decadence (weakness, self-destructiveness) around them and prefigure the future. In them, the "will to power" (self-control, self-surpassing, self-overcoming - Überwindung: power within and over one's self, not power over others) is strong. Nietzsche considered the Europe of his time was decadent in the extreme. Like Goethe, he toyed with romanticism, then rejected it as the epitome of decadence. In so doing he rejected his former friend, the composer Richard Wagner, whose example in large measure had inspired The Birth of Tragedy [from the Spirit of Music].

#### Nietzsche Against Wagner.

For Nietzsche, Wagner's romanticism represented a willful surrender to the "Dionysian
chaos" (whereas Nietzsche admired the Greeks and others for their "Apollonian power" to "organize the chaos” - to transcend it). Moreover Nietzsche could not stomach Wagner's obsessive anti-Semitism (the "mendacious race swindle," in Nietzsche's words) or his obeisance to the dogmas of German militarism and nationalism. Although Nietzsche admired the vigorousness of the early Aryan tribes, he concluded that "the Aryan influence has corrupted all the world"; moreover, he saw that the people of Germany in his time were not Aryans at all but the result of extensive (and desirable) racial mixture, and he considered the "racial purity" ideas of Wagner and other nationalists as hypocritical. Nietzsche thought of race not in terms of zoology and genetics ("blood") but in terms of culture. He regarded the Jews as one of the most vigorous and creative of peoples, by and large one of the better components of European culture.

#### Nietzsche Against Christianity.

Personally an atheist "by instinct" (as he said), Nietzsche nevertheless had great respect for Jesus as a ''noble soul," and for true Christianity as he imagined it may have been (and might be again at any time). But he despised the hypocritical "Christianity" of his time, and asserted that Christians had never practiced the actions that Jesus prescribed for them. His often quoted (out of context) "God is dead" (in the essay "The Madman" in The Joyful Science) was intended as a descriptive statement, not unmixed with irony, of the progressive dilution of the meaning of God in philosophers from Gottfried von Leibniz to Gotthold Lessing, Immanuel Kant, Heinrich Heine, and Georg W. F. Hegel, and of the more general rejection by society of the "living God" in favor of a mere concept (the "moral God" of Voltaire). Yet Nietzsche perhaps was unique in willing that God be dead. Nietzsche wrote: "God is dead ! God will remain dead ! And we have killed him. How shall we console ourselves, we, the murderers among all murderers." And in another place: "The "greatest of recent events - the 'death' of God. . .- is already beginning to cast its first shadows over Europe. . . . We must . . . expect a ... long profusion of demolitions, destructions, ruins, and commotions: who could guess enough of them today to ... become the prophet of such immense terrors, such darkness, such an eclipse of the sun as has never yet been known. . . ." Nietzsche clearly did not entirely relish the prospect of the nihilism and madness he saw approaching, but hoped that as the rubble of decadent Europe was cleared away there would occur a new mixture of fruitful forces and counter-forces from which would arise a new European race of Übermenschen

("over-men" who have wholly mastered themselves) who would not be fettered by nationalism, socialism, militarism, racism, "Germanism," and other manifestations of a decadent society.

#### Nietzsche's Sister and the Nazis.

During his last years Nietzsche's affairs were wholly under the control of his sister. Both she and her husband, Bernhard Förster, were prominent in the German anti-Semitic movement, and falsely represented Nietzsche as being anti-Semitic and in favor of pan-Germanism, Prussianism, militarism, "Wagnerism," and the like. A large portion of Nietzsche's writings, especially after 1885, were attempts to counter such misrepresentations. It has been said by those personally acquainted with both Nietzsche and his sister that she understood virtually nothing of his thought, and (as Walter Kaufmann has put it) she "'inverted' her brother's philosophy into a crude doctrine of pagan mysteries. . . ." This is especially evident in her mishandling of the 'mass of unorganized and miscellaneous notes that she collected and published after Nietzsche's death as Der Wille zur Macht (The Will to Power), and misrepresented as his "final testament." In the notes to his definitive edition of Nietzsche's complete works, Werke in drei Bänden (1958), for which all surviving original manuscripts were consulted, Prof. Karl Schlechta proved that Frau Föster- Nietzsche resorted to numerous forgeries in order to reinforce the misrepresentation of her brother's views. Her distortions were later utilized by the Nazis in seeking to misrepresent Nietzsche as their precursor.

#### Bibliog.
- H. L. Mencken, Philosophy of Friedrich Nietzsche (ed. 1913);
- A. Wolf, Philosophy of Nietzsche (1925);
- G. B. Foster, Friedrich Nietzsche (1931);
- G. Morgan, What Nietzsche Means (ed. 1943);
- W. Hubben, Four Prophets of Our Destiny (1952);
- E. Heller, Disinherited Mind (1953);
- W. A. Kaufmann, Nietzsche: Philosopher, Psychologist, Antichrist (ed. 1956);
- O. Manthey-Zorn, Dionysius: The Tragedy of Nietzsche (1956);
- E. R. Bentley, Century of Hera-Worship (ed. 1957);
- F. A. Lea, Tragic Philosopher (1957).

Entry in the American People’s Encyclopedia (Grolier Inc., 1962)

[„Friedrich Wilhelm Nietzsche” as part of the PDF-Scan](https://www.erhfund.org/wp-content/uploads/545.pdf)

[zum Seitenbeginn](#top)
