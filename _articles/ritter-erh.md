---
title: "Gerhard Ritter über Rosenstock-Huessy"
category: aussage
published: 2022-01-04
name: Ritter
---

„Ein ruheloser, nirgends in der geschichtlichen Tradition ganz fest verwurzelter Geist waltet hier, dem es mehr um die Erkenntnis „dialektischer Prozesse“ in der Weltgeschichte geht als um diese selbst, als um die ruhige Anschauung des Gewesenen. Dem entspricht der unruhig flackernde, jedem neuen Einfall nachgebende, gedankenüberladene nirgends zu schlichter Erzählung oder Schilderung sich abklärende Stil des Ganzen. Geistreiches, ja Tiefsinniges steht unmittelbar neben spielerischer Willkür, gekünzeltem Vergleich, ungezügelter Phantastik, gelegentlich auch bloßer pathetischer Rhetorik. Indessen: läßt man das üppig wuchernde Rankenwerk ab, so bleibt als Kern eine so lebendige und eindrucksvolle Schilderung der verschiedenen europäischen Nationalgeister, ihres geschichtlichen Werdens und Aufeinanderwirkens, wie man sie bisher noch nicht besaß, meist auf überraschend quellennaher Kenntnis der geschichtlichen Tatsachen und Probleme beruhend.

Merkwürdig: in allen Epochen und Räumen europäischer Geschichte vermag sich dieser anpassungsfähige Geist zurechtzufinden, einzufühlen, einzuleben – nur nicht in dem Lebensraum der preußisch-deutschen Monarchie, des Bismarckschen Reiches, aus dem er herstammt.”

>*Ritter, Gerhard, Ein „neues“ Geschichtsbild – und eine politische Predigt von vorgestern, in: Der Tag v. 3.1.1932.*
