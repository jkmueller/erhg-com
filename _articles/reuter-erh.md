---
title: "Edzard Reuter über Rosenstock-Huessy"
category: aussage
published: 2022-11-21
name: Reuter
---

„Was die eigentliche Aktualität der Intuitionen Rosenstock-Huessys ausmacht, ist die Erkenntnis, dass das „soziale Problem“ auch eine geistig-kulturelle Dimension hat, die von dem nur szientistischen und materialistischen Geist nicht gesehen wird. Im Begleittext zur Neuauflage seines Buches „Der Unbezahlbare Mensch“ hat er 1962 die Fruchtbarkeit des „lebendigen Sprechens“ mit über 100 Ingenieuren unseres Hauses über die Gesetze des technischen Fortschritts erwähnt.“
>*Edzard Reuter*
