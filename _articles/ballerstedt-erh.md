---
title: "Kurt Ballerstedt über Rosenstock-Huessy"
category: aussage
published: 2022-01-04
name: Ballerstedt
---

„Man kann ein guter Jurist nur sein, wenn man sich – ich will nicht sagen: einer bestimmten Rechtsordnung, wohl aber – dem Rechtsdenken mit Haut und Haar, mit seiner ganzen Seele und seinem ganzen Gemüte verschreibt. Rosenstock hat mich daran gehindert, dieses sacrificium animae et cordis zu bringen. Er hat mich gelehrt, dem von den Rechtshistorikern und Rechtsdogmatikern herkömmlich geglaubten continuum des Rechts und der Rechtsinstitute zu mißtrauen. Wir alle haben bei ERH gelernt, daß man die Revolutionen ernster nehmen muß, als die nachrevolutionären Zeitalter jeweils getan haben, indem sie den Bruch der (Rechts-)Überlieferung leugneten und um der Festigung der neuen „Ordnung“ willen einen triplex usus legis oder ein Naturrecht oder ein aus dem Volksgeist i.S. der historischen Rechtsschule Savignys geborenes Recht postulierten.”
>*Kurt Ballerstedt an Georg Müller, Herbst 1976, in: Mitteilungen der Eugen Rosenstock-Huessy-Gesellschaft, 24. Folge, Oktober 1976, S.16.*
