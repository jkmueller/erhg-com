---
title: Vorwort von Wilmy Verhage zu «Weitersager Mensch»
category: weitersager
order: 1
---
*Willkommen, Leser, Leserin,*

fünf Jahre habe ich gezögert, eine Einleitung zu diesemvon uns Weitersager-Mensch genannten Projekt zu schreiben. So lange hat es gedauert, bis mir der Überblick gereift war, daß ich etwas mit Vollmacht dazu zu sagen wage. Und ehe mir klar war, an wen ich mich wende: zu Ihnen, zu Dir, liebe(r) Unbekannte(r).

Im Oktober 2004 kamen neun Menschen der Eugen Rosenstock-Huessy Gesellschaft nach Loccum in den Niederlanden auf ein Wochenende zusammen, um zu besprechen, was der nächste Schritt in die Zukunft sein könnte, wenn wir den Konfikt überleben wollten, der von der Herausgabe eines der Hauptwerke Rosenstock-Huessys herrührte. Es waren zwei Frauen, sieben Männer; zwei Niederländer, sieben Deutschen; vier Vorstandsmitglieder, eine Neulinge, ein Rückkehrer und drei aktive Mitglieder, die sich da trafen.

Aus allen geäußerten Möglichkeiten, aus der Sackgasse herauszukommen, tauchte der konkrete Plan Professor Dr. Jürgen Freses auf: Einführungen zu den vielen Büchern zu schreiben, die Rosenstock- Huessy seit dem ersten Weltkrieg geschrieben hat. Um so wieder auf sie aufmerksam zu machen. Jeder der Anwesenden könnte sich doch eines der Bücher dazu vornehmen. Und andere Mitglieder könnten wir auch dazu einladen. Zusammen könnten wir so ja weit kommen. Ja. Aber: es sollten persönliche Einführungen werden. Denn wenn man von einer Lehre, die die Menschen angeht, schreiben will, darf die tatsächliche Erfahrung nicht fehlenund die ist nun immer persönlich. Und so entwickelte sichdank Dr. Eckart Wilkens – der Plan, den Einführungen die Form von Briefen zu geben, Briefen an solche, die man kennt und die jünger sind, also der nächsten Generation der auf Rosenstock-Huessy Hörenden angehören, nach Alter oder Kenntnisstand und meist beidem. Darauf einigten wir uns, und so geschah es.

Für mich persönlich war das Wochenende die erste Bekanntschaft mit Menschen, von denen ich hoffen konnte, sie seien geistesverwandt. Wohl wußte ich von Rosenstock-Huessy seit meiner Jugend, habe ihn aber erst viel später wirklich gelesenjetzt war die Zeit für mich reif, Kontakt mit Menschen aufzunehmen, die auch sein Gedankengut hegen. Diese Erkenntnis war wohl gegenseitig, denn Fritz Herrenbrück – damals Vorsitzender der ERH-Gesellschaft – fragte mich, ob ich das Projekt koordinieren könne. Ich habe ja gesagt, denn von mir aus schien es eine ausgelesene Gelegenheit, das Bekanntwerden fortzusetzen und zu vertiefen. Otto Kroesen und Eckart Wilkens haben mir voran geholfen, inzwischen bin ich ganz eingebürgert und ich liebe uns.
Das Projekt enthält in sich einen Widerspruch: es war das erste Projekt für unsere Website, und die erlaubt ja ein offenes Ende: jeder der Rosenstock-Huessy gelesen hat und eine aus der Umgebung kommende Frage zu einem Buch, einem Gesetz oder einem Satz aus dem Gesamtwerk Rosenstock-Huessys beantwortet, kann diese Antwort über mich auf der Website öffentlich machen. Denn noch lange nicht alle Werke haben ihre einführenden Briefe. Insofern ist das Projekt nicht fertig. Aber es braucht auch nicht fertig zu werden. Fertig wird es nie. Und zugleich – und das ist der Widerspruch – ist das Manuskript, so wie
es jetzt ist, als Buch ein abgerundetes Ganzes. Ganz anders sind doch die Gesetze des Geistes als die der Materie, das zeigt sich mal wieder…

Ich hoffe, lieber Unbekannter, dass Sie den Briefen Zeit gönnen. Sie stehen in der Reihenfolge, in der Rosenstock-Huessy die betreffenden Bücher geschrieben hat. Stück für Stück sind es kleine Juwelen (und ein einziges großes) geworden, die mit Aufmerksamkeit gelesen zu werden verdienen. Und dann, am Ende… wie ein Prisma der Lichtfgur farbige Ränder gibt, so gestalten die Beiträge zusammen – in allen persönlichen Unterschieden – einen guten Eindruck von dem, was Rosenstock-Huessy bewegt.

Es ist Nagelneu was er sagt. Über die Postmoderne hinaus. Und von praktischem Nutzen für heute. Lassen Sie sich erschüttern.

Wilmy Verhage, Amsterdam, März 2010

1[1] Siehe „Unser Volksname Deutsch” in Mitt. Der Schles. Ges. f. Volkskunde XXIX.; „Die Sprache des Westens” in „Geschichte” 1955 und eine 1957 erscheinende Schrift „Frank-reich – Deutsch-land” (Käte Vogt Verlag).
2[1] Seitenzahlen auch arabische Zahlen in Klammern beziehen sich auf E. Rosenstock-Huessy, Die europäischen Revolutionen und der Charakter der Nationen Moers, Brendow 1987 ISBN 3-87067-301-X
3[1] Römische Zahlen für den Band und arabische Seitenzahlen beziehen sich auf\
E. Rosenstock-Huessy, Die Sprache des Menschengeschlechts, Heidelberg 1963 Bd. I, 1964 Bd. II
Literatur: Wilfrid Rohrbach, Das Sprachdenken Eugen Rosenstock-Huessys, Kohlhammer, Stuttgart, Berlin, Mainz Köln 1973


4[2] Wanderer, kommst du nach Sparta, verkündige dort, du habest uns hier liegen gesehn, wie das Gesetz es befahl.
5[3] Aus: Die Innenwelt der Außenwelt der Innenwelt edition suhrkamp 307 5. Auflage 1972 S. 96/97

6[4] Gnostischer Kirchengründer, wurde 144 von der christlichen Gemeinde in Rom als Ketzer ausgeschlossen.
7[5] 1851 -1930 der bedeutendste Vertreter der historischen Schule innerhalb der liberalen Theologie.
8[6] 1884 – 1976 will die Botschaft des NTs aus dem mythologischen Weltbild herauslösen (Entmythologisierung).
9[7] Theologe, 20. Jh.
