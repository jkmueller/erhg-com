---
title: Mitgliederbrief 2007-05
category: rundbrief
created: 2007-05
summary: |
  1. Vorstellung des neuen Vorstandes
  2. Die Tagung auf der Frankenwarte
  3. Mitgliederversammlung 2007
  4. Vorstellung des Themas der nächsten Jahrestagung
  5. Zum Tod Jürgen Freses
  6. Jahrbuch Stimmstein
  7. Das Projekt Weitersager Mensch
  8. Zur Soziologie-Edition
  9. Helmuth James v. Moltke zum 100. Geburtstag
  10. Nachrichten von Respondeo und vom Rosenstock-Huessy Fund
  11. Der Artikel Eugen Rosenstock-Huessy bei Wikipedia
  12. Hinweis zur Beitragszahlung
zitat: |
  >Sobald wir die akademische Illusion vom zeitlosen Denken abtun, können wir aller Fehlerquellen Herr werden, die sich aus dem Zeitablauf ergeben. Denn nun können der Lehrer und der Schüler sich ausdrücklich Zeit zum gegenseitigen Mißverstehen einräumen. Ja, der Zeitraum des Studiums wird dann geradezu der Spielraum für das unentbehrliche Noch-nicht-in-einer-und-derselben-Zeit-Leben. Während die pädagogische Methode sagt: Mißverständnisse zwischen Lehrer und Schüler sind vermeidbar, sagen wir mit Augustins „De magistro”: Je wichtiger eine Frage, desto mehr Mißverständnis muß in Kauf genommen werden.
  >*Eugen Rosenstock-Huessy, Die Sprache des Menschengeschlechts II, S. 388*
---

## Eugen Rosenstock-Huessy Gesellschaft e.V.

{{ page.zitat }}

**Vorstand/board/bestuur:**\
*Dr. Eckart Wilkens (Vorsitzender);*\
*Wilmy Verhage; Andreas Schreck; Dr. Jürgen Müller; Lothar Mack*\
*Antwortadresse: Dr. Eckart Wilkens: Everhardstraße 77, 50823 Köln, Deutschland*\
*Tel: 0(049) 221 511265*

***Brief an die Mitglieder Mai 2007***

Liebe Mitglieder und Freunde,
der Pfingstbrief ist so umfangreich geworden, daß es sich empfiehlt, eine Übersicht vorzuschicken:

{{ page.summary }}

### 1. Vorstellung des auf der Frankenwarte 2007 gewählten Vorstands

Nach der Tagung und Mitgliederversammlung vom 23.-25.3.2007 auf der Frankenwarte habe ich für alle Teilnehmer eine Strophe geschrieben. Sie gibt – für mich – den genauen Ort an, den wir zu der Zeit hatten. Deshalb möchte ich uns mit diesen Strophen vorstellen.\
Jedem Teilnehmer habe ich sie ja zukommen lassen. Sie alle zusammen bilden das Gedicht JUDICA, und entsprechend ist auch der ganze Psalm 43, der die Andacht am Sonntagmorgen bestimmt hat, darin zum Erklingen gebracht. Ich bringe die Strophen, die uns vorstellen, in der Reihenfolge, in der sie in dem Gedicht stehen, mit einigen Erläuterungen.

***2 Andreas Schreck***\
Merkelstr. 27j, Göttingen

Sich zu verpflanzen wissen.\
And plead my cause against an\
ungodly nation – sprich für meine\
Sache gegen das gottlose Volk, das\
so rasch die Märtyrer einebnet zu ge-\
wöhnlichen Gräbern oder einem vorüber-\
ziehenden Grauschleier. Der Taumel\
unsäglicher Anstrengung entzieht\
die gewohnte Ordnung, nichts\
wiederzufinden, was doch noch eben\
zur Hand gewesen! Im Kern\
wird geschmiedet das Nein zu der\
entwickelten Epoche, in die man ver-\
wickelt ist, und kann nicht aus-\
wickeln Beschwernis und Atemnot.\
Solches Dilemma tanzt in Momente\
der Sprachlosigkeit. Wunderbar aber,\
nicht wahr, ist die Treue, mit der uns\
Schöpfung, Offenbarung und Erlösung\
begegnen, eben gestreift und da:\
der Glanz kommt zugute. Die Kirchen-\
gemeinde gerettet in die Musik, die\
offenen Ohren für das schaffende Wort\
rufen ins Jetzt, das Erste Sozialgesetz\
gibt uns Zutritt zu Buddha und Laotse.

Am Anfang jeder Strophe steht ein Titel von Balthasar Gracían aus seinem Handorakel und Kunst der Weltklugheit von 1647, deutsch von Arthur Schopenhauer, hier Nr. 198. Darin heißt es – und das möchte ich für Andreas Schreck anführen -: Das Vaterland ist allemal stiefmütterlich gegen ausgezeichnete Talente, denn in ihm, als dem Boden, dem sie entsprossen, herrscht der Neid, und man erinnert sich mehr der Unvollkommenheit, mit der jemand anfing, als der Größe, zu der er gelangt ist.

***6 Jürgen Müller***\
Hermansweg 56, Eindhoven

*Was Gunst erwirbt, selbst verrichten,\
was Ungunst durch andre. Warum\
gehe ich in Trauer wegen des Widerdrucks\
des Feindes? Die Stationen* – es sind\
die Stationen, angefangen mit dem\
Schabbath, den es in der Natur nirgends\
gibt, alle Berechnungen beruhen auf der\
Cohärenz hienieden, ist sie erst mit\
Noachs Verheißung: Solange die Erde steht,\
soll nicht aufhören Saat und Ernte,\
Frost und Hitze, Sommer und Winter,\
Tag und Nacht zu uns gekommen,\
Übersetzung in das Einmal der Bibel\
der Tat Pharaos von Ägypten alljährlich?\
Die Stationen, das Innehalten zwischen\
Altem und Neuem – und wer rief Dich\
denn in das Neue, als wärest Du un-\
entbehrlich dafür? *Der Himmel er-\
ledigten Zwists?* Die Feiertage sind\
die Stationen des Menschengeschlechts\
im Bilde des Schabbath, Vorwegnahme\
des Endes, wie jeder gesprochene Satz,\
jede angestimmte Melodie sie voraus-\
setzen – David Bohm hat sie gekannt,\
aber nicht mit t1 und t2 erfassen können.

Eckart Wilkens, Helmuth James von Moltke

Jürgen Müller tritt hier besonders als der aufmerksame Hörer in Erscheinung. So ist auch die Zeile: Der Himmel erledigten Zwists aus dem Gedicht zum Gedenken Helmuth James von Moltkes, das ich verlas, in die Strophe gelangt. Der Zwist mit der Soziologie-Edition ist zu Ende – dafür steht die Wahl Jürgen Müllers, das Wieder-Hinzukommen des pfingstlichen Erschlossen-Werdens.

***12 Wilmy Verhage***\
Olympiaplein 32 A 1, Amsterdam

*Von sich und seinen Sachen vernünftige\
Begriffe haben. Dann will ich gehen\
zu Gott, meiner Überschwang-Freude.\
Schmerz erst lehrt uns wie zu lieben* –\
Akut, Gravis, Circumflex, alle drei\
Zeiten, Aktiv, Passiv, Medium büßen\
für solchen Schmerz, Klugheit daraus\
zu schöpfen, aber doch nicht sie vorlaut\
zu wissen, ehe solcher Schmerz durchlebt\
ist, der nicht einmal beschworen werden\
kann mit dem Zauberwort *Scheidung* –\
nach welchem sich der Abgrund einst auf-\
tat, in den man vor Scham zu versinken\
bedroht ist. Die Schallwellen solchen\
Schwurs mischen sich in die peinliche\
Frage nach Öl in den Lampen: ist noch\
Öl da, haben wir rechtzeitig Öl ein-\
gekauft, Mitternacht abwarten zu\
können, können zu tun, was *Atem des\
Geistes* jetzt von uns fordert, akut?\
Ihr wißt es, wie ich, Ihr wißt es ebenso-\
wenig und ebensoviel wie ich, laßt uns\
die Scham überstehen, indem wir die Liebe\
durch alles dringen lassen, auch das\
Scheitern aller Bemühungen um Verständnis.

Matth. 25, 1-13

Die Trennung von Gudrun Lemm als Erster Vorsitzender und als Mitglied des Vorstands hat uns – das werden Sie alle gespürt haben – Mühe gekostet. Es ging immer um den Anteil der Frauen an der Arbeit, den nun Wilmy Verhage im neuen Vorstand allein einzubringen hat. Mit welcher Vehemenz sie das tut, ist in der Strophe deutlich zu lesen.

***14 Eckart Wilkens***\
Everhardstr. 77, Köln

*Einsicht haben oder den anhören,
der sie hat. O Gott!* Das Herz
hat so viele Fenster wie es Mitmenschen\
gibt, man fragt sich, in welch kühnen\
Konstruktionen erbaut ist, was wir\
da unter der Bleibe verstehen, die das\
Herz ist. Bas Leenman und Ko Vos,\
wie vielen nicht sind sie der Freund,\
dessen Anwesenheit und Wir-Geschenk\
das gebracht hat, was nach der Lektion\
des Schmerzes kommt, Konrad von Moltke,\
Wolfgang Ullmann: dies – *wird's\
als Wahrheit uns geschenkt*, flügel-\
artig zu beiden Seiten hin, flügelartig\
wie in dem alten Segensspruch: *Abends,\
wenn ich schlafen geh, vierzehn Engel
bei mir stehn, zwei zu meiner Rechten,
zwei zu meiner Linken, zwei zu meinen\
Häupten, zwei zu meinen Füßen,\
zwei, die mich decken, zwei, die mich\
wecken, zwei die mich weisen in das\
himmlische Paradeisen*. So strömt die\
Wahrheit durch alle Glieder, erlöst\
den Atem erquickt die Heilkraft,\
ruft nicht: Hört, ihr Herrn! –\
ruft zum Dienst auf dem Planeten.

Anonym, Abends, wenn ich schlafen geh\
Nachtwächterlied: Hört ihr Herrn, und laßt euch sagen

Und seid dankbar (Kol. 3,15)– das ist ein verborgenes Motiv meines Lebens.

***21 Lothar Mack***\
Schnepfwinkelstr. 14, Safenwil

Sich den fremden Mangel zunutze\
machen. Als aber der Messias gekommen,\
als Hoherpriester der künftigen Güter,\
ist er durch das größere und vollkomm-\
nere Zelt – das kein Gemächt von Menschen-\
hand ist, das heißt: nicht von dieser\
Schöpfung, nicht kraft Blutes von Böcken\
und Kälbern, sondern kraft des eigenen\
Blutes – ein für allemal in das Heiligtum\
eingegangen. Und so hat er unendliche\
Erlösung gewonnen. Hat er das denn\
etwa mit Buddha, Laotse, ja Abraham\
gemeinsam? Sind diese nicht vielmehr,\
als von dieser Schöpfung, mit Christus ab-\
getan und der Weitersager Mensch ist ge-\
bunden an den Missionsbefehl: Geht in\
alle Welt und kündet die Heilsbotschaft\
allem Geschöpf: Wer glaubend ist und\
sich taufen läßt, wird gerettet; wer aber\
ungläubig bleibt, wird verurteilt? Sagt es\
mir, wie Ihr herauskommen wollt aus\
dieser Klarheit! Vor- oder nachchristlich –\
begegnen sie sich doch in dem Einssein\
mit dem Messias, Jesus Christus? Erlöst\
er sie wie die Gläubigen, die in der Vorhölle waren?

Projekt der Rosenstock-Huessy Gesellschaft: Weitersager Mensch, angeregt von Jürgen Frese und Eckart Wilkens in Barchem 2004\
Markus 16,15-16

Lothar Mack vertritt in dem Vorstand also nun die Stimme der Pfarrer, er sucht eine Anstellung in der Schweiz und ist mit den neuen Medien im Sinne der Verkündigung bemüht. Wir haben ihn bereits in Rastede 1997, dann in Bethel 2003 und 2006 in Neudietendorf erlebt.

Bei der Vorbereitung der ersten Vorstandssitzung vom 20.-22. April habe ich gemerkt, wie die Namen der bisherigen Vorsitzenden der Gesellschaft mit einwirken: *Georg Müller, Dietmar Kamper, Andreas Möckel, Michael Gormann-Thelen, Fritz Herrenbrück, Gudrun Lemm*. Jetzt
ist im Vorstand niemand mehr, der Eugen Rosenstock-Huessy von Angesicht zu Angesicht begegnet ist. Deshalb sind wir zum Übersetzen gerufen, um wieder eine kleine Wegstrecke zu bahnen, damit der Ruf: Respondeo etsi mutabor, Ich antworte, auch wenn es über die Hutschnur geht zur Ermutigung wird.
>*Eckart Wilkens*

### 2. Die Tagung auf der Frankenwarte 2007: Des Christen Zukunft
Als wir ankamen in der Frankenwarte, war es noch Winter. Es war kalt, es lag Schnee, die Wolken lagen tief, und es war neblig.\
Wir erwarteten 27 Mitglieder, und unter diesen diesmal ein Gutteil Frauen.\
Es war spannend, denn der Vorstand war entzweigefallen, einerseits Eckart, Andreas und ich, die wir das Programm vorbereitet hatten, andrerseits Gudrun Elisabeth, die verantwortlich war für den Sonntag mit Andacht und Mitgliederversammlung. Aber glücklicherweise zeigte sich gleich: obwohl der Kontakt verschwunden war, taten wir alle vier unser Bestes, um die Tagung gelingen zu lassen. So haben wir trotzdem alle vier die Tagung getragen.\
So zwischen fünf und sechs trafen die meisten ein, wir konnten einander begrüßen und ausruhen von der Reise und während der ersten Mahlzeit einander ein bißchen auf dem Laufenden halten, wie es mit jedem und jeder so stand, ehe das Programm anfing.\
Bei der Gestaltung des Programms haben wir weiter auf die Erfahrungen in Neudietendorf gebaut. Eine kurze Einführung zu den drei Themen: Freitagabend Gemeinde und Gemeinschaften, von Andreas eingeleitet; Samstagmorgen die christlich geprägte Zeit, von Eckart eingeführt; Samstagmittag das Kreuz der Wirklichkeit in den Gestalten Christus und Abraham, und Lao-tse und Buddha, von mir.\
Da wir uns dann in drei verschiedene Gruppe geteilt haben, kann ich nur erzählen, was in meinen Gruppen entstand. Die Unterschiede bei der Einführung wie die verschiedenen Themen und die Tatsache, daß die Teilnehmer sich jedesmal anders in die kleinen Gruppen gliederten, sorgten dafür, daß alle dreimal (jedenfalls in meiner Gruppe) eine ganz andre Gesprächsdynamik entstand. Bei den Gemeinden nahm jeder sich lange das Wort, wie es so mit den eigenen Erfahrungen mit Gemeinde und deren Schwierigkeiten oder auch dem Mangel daran stand. Als wir Samstagmorgen über die Zeit sprachen, kam heraus, wie wichtig Pausen eigentlich sind und wie anders in Europa, in Afrika oder in Mittelamerika Zeit erlebt wird. Und in der letzten Gruppe wurde vornehmlich die Vater-Sohn-Dimension bei Abraham und Jesus vertieft.\
Hätte es wissenschaftlicher gestaltet werden können? Was ich in den drei Gruppen, die ich begleitet habe, immer wieder erfahren habe: daß die Antworten auf die Texte, die doch wohl bei jedem verschieden waren, mein eigenes Verständnis ergänzten. Als ob die anderen Lichter,
die darauf geworfen werden, die Menge der in mir bereits befindlichen Prismaseiten verdeutlichten und vermehrten – was eine bereichernde Erfahrung war. Jeder muß schließlich selber und auf eigene Art das Wissen um und von Rosenstock-Huessy, das Wissen überhaupt, meistern lernen, um es verkörpern zu können. In meiner Gruppe habe ich darauf geachtet, daß jeder für sich den Raum bekam, um sein und ihr eigenes Verständnis zu prüfen und Halb- und Mißverständnisse zu beiseitigen. Aber auch da bleibt doch jeder sein eigener Endredakteur. Aktiv lernen nennt Rosenstock-Huessy das auch (davon ist in einem seiner Darthmouth-Vorlesungen die Rede).\
Abends haben wir Feicos überzeugende Erzählung vom Schicksal Helmuth James von Moltke gekostet, und wie heute in Kreisau sein Leben Frucht wird.\
Sonntagmorgen war Gudrun Elisabeths Thema bei der Andacht die Rechtfertigung (Psalm 43).
Und nach dem Frühstück hat sie der Mitgliederversammlung ihren Rechenschaftsbericht über die letzten zwei Jahre gegeben und ist der neue Vorstand gewählt worden, bei dem sie nun nicht mehr mitmacht.\
Alle vier, jeder und jede auf seine Art und Weise, haben wir uns bemüht, den Abschiedsschmerz so wenig verletzlich wie möglich zu gestalten.\
Als wir uns nach einer letzten Mahlzeit zusammen voneinander verabschiedeten, schien die Sonne, und es war warm. Überall zeigten sich Hyazinthen, Tulpen, Narzissen, die Vögel ließen sich hören. Auf Zehenspitzen war der Frühling gekommen und überraschte am Schluß.
>*Wilmy Verhage*

### 3. Mitgliederversammlung 2007
An der Mitgliederversammlung nahmen 20 Mitglieder und einige Gäste teil. Der bisherige Vorstand war an ein Ende gelangt, vermochte keinen Personalvorschlag zu unterbreiten und legte die Verantwortung für die Zukunft der Gesellschaft ganz in die Hand der Mitgliederversammlung. Das war nervenaufreibend und auch beruhigend. Denn wie so oft kam der rettende Mut. Steffen Buser übernahm die Protokollführung, Herluff Löck das Wahlverfahren, Feico Houweling kurzzeitig die Sitzungsleitung, Rudolf Hermeier und Willie Stoppelenburg erklärten ihre Bereitschaft zur Kassenprüfung für 2008, und für die weiteren Vorstandsämter meldeten sich mehrere Kandidaten.
Es zeigte sich, wie wenig erfahren wir sind in der Handhabung demokratischer Verfahren, die Person und Sache auseinanderhalten und durch die Beobachtung der Formen die mit Wahlen verbundenen Urteile über Menschen einbetten.\
Belastend wirkte der Zeitdruck, Entscheidungen und Diskussionen bis zum Mittagessen und Abreisezeitpunkt abschließen zu müssen. Hier werden wir nach einer anderen Zeitgestalt suchen.
Neben den in diesem Rundbrief gesondert erläuterten Beschlüssen wurde unter anderem die Sicherung und Zukunft unseres Archivs nach Ablauf der jetzigen Vereinbarung mit dem Archiv Bethel im Jahr 2012 angesprochen.\
Das vollständige Protokoll kann beim Vorstand angefordert werden.
>*Andreas Schreck*

### 4. Vorstellung des Themas der nächsten Jahrestagung 2008
Auf der ersten Vorstandssitzung haben wir auch schon über das Thema der nächsten Jahrestagung gesprochen.\
Gerhard Gillhoff von der Herforder Akademie hatte uns vor der Tagung an die Satzung erinnert, die ausdrücklich dazu auffordert, der wissenschaftlichen Forschung zu dienen und so (oder: damit) allen übrigen gesellschaftlichen Feldern einen Dienst zu erweisen. Nun ist Jürgen Frese gestorben, der Gründer der Herforder Akademie, so daß wir nicht mehr auch mit ihm darüber streiten können, was denn die Form sein kann, in der wir diesen Dienst tun. Aber gerade zu seinem Gedenken möchten wir auf der nächsten Tagung die drei Formen der abendländischen Hochschule zum Thema machen, das also lautet:

SCHOLASTIK, AKADEMIK, ARGONAUTIK –\
ROSENSTOCK-HUESSYS RUF ZU EINER NEUEN FORM DER HOCHSCHULE

Die Fragen, die schon jetzt deutlich sind:\
Hebt die neue Form die alten auf oder erneuert sie diese auch, wenn sie erst einmal geübt wird?
Was ist der Kern der drei Formen? Sie gehören zu den drei Singularen: EIN Gott (Scholastik), deshalb kann alles bisherige Wissen gesammelt und neu geordnet werden; EINE Welt (Akademik), deshalb müssen sich alle Forscher zu dem einen einzigen Forscher verbinden lassen; EIN Menschengeschlecht (Argonautik), deshalb wird jeder entsendet, die Verwandlungserfahrung durch die Sprache zu machen und sie einzubringen.
Wie sieht also solch ein Tun aus?
Braucht es die Form einer Institution?
Und wie sieht diese aus?
Welche Verwandtschaft gibt es zwischen den internationalen Friedensdiensten und dem Forschungshandeln der Argonautik?
Wir wollen die Tagung wieder mit ausgewählten Texten, diesmal aus der Soziologie, vorbereiten.
>*Eckart Wilkens*

### 5. Zum Tod Jürgen Freses
Unser Mitglied Prof. Dr. Jürgen Frese ist am 24. März 2007 verstorben und am 30. März 2007 auf dem Friedhof Bielefeld-Jöllenbeck beerdigt worden. Er ist 68 Jahre alt geworden. Wir werden seine markante Persönlichkeit sehr vermissen.

In der Festschrift zu seinem 60. Geburtstag wird Frese mit folgenden ihn kennzeichnenden Worten zitiert: „Worauf es beim Philosophieren ankommt, ist, die Bedingungen erkennen zu lernen, unter denen man ein anderer werden und anders denken kann. Das ist sicherlich nicht ohne nomadische Grundverfasstheit möglich … Menschen sind geneigt, willens und getrieben, immer wieder neu nach Chancen zu suchen (gerade auch Nicht-Philosophen), andere werden zu können.”
Jürgen Frese stammte aus Dortmund, hat in Münster Philosophie, Psychologie und Soziologie studiert, hat 1966 in Soziologie bei Schelsky promoviert. Er war wissenschaftlicher Assistent in Bochum und Bielefeld, hat sich 1975 für Philosophie habilitiert. Von 1970 bis 2004 hat er an der Universität Bielefeld Sozialphilosophie und Phänomenologie gelehrt.
Mit Gerhard Gillhoff hat er die Herforder Akademie gegründet. Der Ruf der Herforder Akademie nach textnaher und texttreuer Arbeit an den Werken Eugen Rosenstock-Huessys ist mit Freses Tod nicht verhallt. Ich hoffe, daß wir unsauch dazuin einem richtigen Gespräch finden und erkennen können.
>*Wilmy Verhage*

### 6. Jahrbuch Stimmstein
Die Mitgliederversammlung hat die Fortsetzung des Jahrbuchs Stimmstein beschlossen und Andreas Schreck zum verantwortlichen Redakteur bestellt. Für die langjährige Betreuung des Stimmsteins ist die Eugen Rosenstock-Huessy Gesellschaft Andreas Möckel und Karl-Johann Rese sehr dankbar.
Anregungen, Manuskripte und Angebote zur Mitarbeit nimmt Andreas Schreck (Adresse siehe unten) gern entgegen. Erste Zusagen für Stimmstein 12 (2007) liegen bereits vor. Alle Mitglieder sollen den Stimmstein weiterhin kostenlos erhalten. Wer ist bereit, ein erneuertes Vertriebskonzept zu entwickeln?
>*Andreas Schreck*

### 7. Das Projekt Weitersager Mensch
Nach zwei Jahren und sechzehn Beiträgen, deutsch und niederländisch, die noch immer in der Reihenfolge des Eintreffens auf unsrer Website stehen, wird es wohl Zeit, das Projekt Weitersager Mensch zu beenden (und doch das Weitersagen weiterzuentwickeln!) und aus diesem Versuch ein Buch zu machen. Ich gebe also jedem eine Frist, auch jedem, der oder die sich noch an einen Brief an einen Jüngeren über ein Buch, ein Gesetz, einen Satz oder auch mehrere Bücher Rosenstock-Huessys wagen will. Diese Frist setze ich auch mir selber, denn da ist noch ein Brief zu „Die Tochter”, an dem ich gerade wieder schreibe. Die erste Version fand ich zu unreif. Und vielleicht wecke ich einige von euch noch auf, die sich und mir versprochen haben, zu einem ERH-Buch zu schreiben.

Also: die Frist setze ich auf den kommenden 1. September, Ferienende. Danach nehme ich die Koordination gerne auf mich, aus den Beiträgen ein Buch zu gestalten (zunächst eine Redaktionsgruppe zu bilden). Meine Adresse finden Sie unten, also ich bin zu finden.
>*Wilmy Verhage*

### 8. Zur Soziologie-Edition
Wie bekannt, wurde auf der Mitgliederversammlung der Antrag gestellt, die Rosenstock-Huessy Gesellschaft möge juristisch prüfen lassen, wie ihre Position in dem Streit ist. Andreas Schreck hat sich erboten und nun den Auftrag erhalten, dies zu tun, ohne daß wir vorerst einen Anwalt bestellen müssen. Jürgen Kühn und Rudolf Hermeier haben aber in einem Schreiben an den Vorstand noch eine andere oder weitere Vorgehensweise vorgeschlagen: in einem Schreiben an die Erben der Rechte an den Werken Rosenstock-Huessys zu erklären, daß wir noch einmal eine Frist bis zum Jahresende setzen, dann möge uns entweder ein Text vorliegen oder aber dem Talheimer Verlag gekündigt sein – wenn nicht sollte die nächste Mitgliederversammlung darüber beraten, was mit den Spendengeldern weiter zu tun sei (der Vorschlag war zurückgeben). Wir sind dankbar auf dieses Ansinnen eingegangen, ich habe mit Datum vom 24. April an die Erbengruppe geschrieben: daß wir diese Frist setzen, welche Bedenken wir gegenüber der Herausgebergruppe haben (der Mißbrauch des Talheimer Verlags, der sich mit den Namen Eugen Rosenstock-Huessys und Freya von Moltkes in seinem öffentlichen Auftreten schmückt, Michael Gormann-Thelens endlose Versprechungen, die nicht eingehalten werden, Lise van der Molens Schonungsbedürftigkeit nach der Herzoperation, Ruth Mautners Weigerung, mit der Gesellschaft weiter zu tun zu haben), endlich klagen wir darüber, wie das Vertrauen verfällt, wenn nicht wirklich bald das Versprechen eingelöst wird, daß die Soziologie wieder erscheint, und äußern die Meinung – ohne da weiteres unternommen zu haben -, daß der von Fritz Herrenbrück erstellte und von Otto Kroesen geprüfte Text der Soziologie doch publiziert werden könnte, wenn die Erbengruppe sich von Talheimer lösen kann. Der Brief ging per e-mail an alle drei, Paula Stahmer, Hans und Mark Huessy. Paula Stahmer hat inzwischen geantwortet: Michael Gormann-Thelen habe versichert, die Soziologie werde bei der Buchmesse im Herbst erscheinen – wenn aber nicht, dann sehen auch die Erben die Frist für abgelaufen an, für die man Geduld haben soll. Abenteuerlich!
>*Eckart Wilkens*

### 9. Helmuth James v. Moltke zum 100. Geburtstag
1927 begrüßte Helmuth James v. Moltke in Kreisau Eugen Rosenstock-Huessy, um vorzubereiten, was bald in der Löwenberger Arbeitsgemeinschaft Gestalt gewann. 80 Jahre später studiert das Junge Klangforum Mitte Europa in Kreisau Gustav Mahlers „Auferstehungssinfonie” ein und führt sie im Konzerthaus am Berliner Gendarmenmarkt auf. Wenn im planetarischen Zeitalter Kontinente noch „Sternstunden” haben dürfen, der Sonntag, 11. März 2007, schenkte Europa einen solchen Moment.

Die Sinfonie dieses Tages begann mit einem Gedenkgottesdienst im Französischen Dom mit Predigtworten von Erzbischof Alfons Nossel (Oppeln/Opole) und Landesbischöfin Margot Käßmann (Hannover). Im Konzerthaus sprachen der einstige polnische Außenminister Bronislaw Geremek und die Präsidentin des europäischen Rats, Bundeskanzlerin Angela Merkel, Grußworte, aus denen hervorging: Helmuth James von Moltke und seine Freunde haben an ein Morgen gedacht, das erst zum Teil erfüllt und verstanden ist. Ein schöner Auftrag an uns. Freya von Moltke begrüßte die jungen Musiker aus Tschechien, Polen und Deutschland und sprach von einem Geschenkfür sie wie für Helmuth James – das nicht zu ermessen sei.\
Viele Mitglieder unserer Gesellschaft erlebten diesen strahlenden Tag, ich sah Franz v. Hammerstein, Fritz Herrenbrück, Erika und Karl-Heinz Kämper, Lien und Wim Leenman, Karl-Johann Rese, Jochen Rieß, Eckart Wilkens.\
Zeitgleich fand ich Würzburg eine Gedenkveranstaltung statt, die Franz Fisch und Andreas Möckel wesentlich vorbereitet hatten. Und wenige Tage später trug Feico Houweling in Rotterdam zu Helmuth James v. Moltke vor, und er wiederholte dieses Lebensbild eindrucksvoll auf der Jahrestagung in Würzburg.\
Der 100. Geburtstag war ein Höhepunkt in der Lebenserzählung von Helmuth James v. Moltke. Und ein sehr glücklicher Tag für mich.
>*Andreas Schreck*

### 10. Nachrichten von Respondeo und dem Rosenstock-Huessy Fund
Am Weißen Sonntag (Quasimodogeniti) fand in der Tagungsstätte „Unsre liebe Frau” in Amersfoort die halbjährliche Versammlung von Respondeo statt. Ich war als Gast geladen. Wir sprachen über Dietrich Bonhoeffer und Eugen Rosenstock-Huessy, wie sie von der Zwillingsschwester Bonhoeffers gesehen wurde: wie sie sich getroffen haben, wie Bonhoeffer von Rosenstock-Huessy als Märtyrer geehrt wurde, in welcher Weise ihr Erbe nach dem Zweiten Weltkrieg anzunehmen und fortzuführen ist. An der Aussage Rudolf Kooimans, beide hätten kein „System” hinterlassen, entzündete sich der Widerspruch: sehr wohl läge doch der Soziologie Rosenstock-Huessys ebenso wie seinem Sprachbuch ein lange durchdachtes System zugrunde, das aber dem Leser nicht übergestülpt wird, sondern sich – als Vorsatz – öffnet, damit der Leser mit seinen eigenen Erfahrungen den Nachsatz dazu darstelle. Wie dieser andragogische Stil im Gemeindeleben stattfinden könne, kirchlich und säkular, ist eine wichtige Frage.\
Vom Rosenstock-Huessy Fund hörten wir von Mark Huessy, daß dieser an dem Punkt angekommen zu sein glaubt, daß der ursprüngliche Zweck, das Werk von Eugen Rosenstock- Huessy (in Gedrucktem, Geschriebenem, Gesprochenem) zu sichern, an sein Ende gekommen ist und nun die Diskussion entbrannt ist, die wir – jedenfalls andeutungsweise – auch führen: ist die Gesellschaft ein Arm der wissenschaftlichen Forschung, so wie sie – kurz gesagt – scholastisch und akademisch betrieben worden ist, oder ist sie auch offen für das Vorläufige, das andragogisches, argonautisches Arbeiten als Pirat des Planetendienstes notwendig haben muß. Wir sind gespannt, davon mehr zu hören.
>*Eckart Wilkens*

### 11. Der Artikel Eugen Rosenstock-Huessy bei Wikipedia
Wir wurden aufmerksam, daß im Internet Wikipedia in dem Artikel Eugen Rosenstock-Huessy ein Absatz stand, der offenbar aus dem Studium der von Michael Gormann-Thelen und Dietmar Kamper herausgegebenen Tumult-Nummer 20 mit den beiden 1931 und 1932 im Hochland unter dem Pseudonym Ludwig Stahl veröffentlichten Artikeln Rosenstock-Huessys hervorgegangen ist und den Anschein erweckt, als könne man sich für die Blindheit gegenüber den Nazis damit entschuldigen, daß selbst ein so heller Kopf wie Rosenstock-Huessy nicht deutlich erkannt hätte, was uns da blühte. Bei der Vorstandssitzung fand Lothar Mack dann heraus, daß der Absatz von einer Adresse stammt, die sich Eisbär44 nennt – wir versuchen, diese ausfindig zu machen und Eisbär44 und Andreas Möckel miteinander ins Gespräch zu bringen. Daneben aber habe ich einen vierteiligen Artikel für Wikipedia verfaßt (Einleitung, Leben, Lehre, Wirken), über dessen endgültige Form ich noch mit Lothar Mack handle, den wir dann ins Internet stellen wollen. Wie unangetastet und wie lange er da stehen bleibt, ist abzuwarten.
>*Eckart Wilkens*

### 12. Hinweis zur Beitragszahlung
Die Mitgliederversammlung hat 2005 eine Beitragserhöhung auf € 40.— für Einzelpersonen, € 50.—für Ehepaare, € 20.—für Schüler/Studenten/Geringverdienende beschlossen., die ab 2006 gilt.
Die Mitglieder werden gebeten, den Beitrag für 2007 sowie rückständige Beträge auf das deutsche bzw. niederländische Konto (siehe unten) zu überweisen. Der Kassier erteilt gern Auskunft über den Stand der Beitragszahlung.\
Besonderer Hinweis für Mitglieder, die eine Einzugsermächtigung erteilt haben: Im Dezember 2006 sind irrtümlich nur die alten Beitragssätze (€ 30.—für Einzelpersonen) abgebucht worden. Für 2007 wird in diesen Fällen € 40.-- + € 10.— als „Nachzahlung 2006” abgebucht, soweit bis zum 30.06.2007 kein ausdrücklicher Widerspruch beim Kassier eingeht. Bescheinigungen über gezahlte Beiträge werden auf Wunsch gern ausgestellt.
>*Andreas Schreck*

[zum Seitenbeginn](#top)
