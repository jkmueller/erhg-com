---
title: "Rudolph Sohm über Rosenstock-Huessy"
category: aussage
published: 2022-01-04
name: Sohm
---

„Seit etwa 1200 tritt der Gedanke des Hauses als für den deutschen Staat grundlegend in den Vordergrund. Die Hausverfassung spiegelt sich in der Staatsverfassung. Die deutschen Fürsten sind als Vasallen die persönlichen Diener (Hofleute) des Königs. An ihrer Spitze stehen die Träger der obersten Hofämter, die Kurfürsten. Das Reich wird von den Dienern des Königshauses, das Land von den persönlichen Dienern des Landesherrn regiert. Das sind die Gedanken, die E. Rosenstock in seinem bedeutenden Buch: Königshaus und Stämme in Deutschland zwischen 911 und 1250 (Leipzig 1914) geistvoll entwickelt hat.”
>*Rudolph Sohm, Das altkatholische Kirchenrecht und das Dekret Gratians <1918>, im Anh.: Rezension von Ulrich Stutz, reprogr. Nachdr., Darmstadt: Wissenschaftliche Buchgesellschaft 1967, S.587.*
