---
title: Mitgliederbrief 2019-09
category: rundbrief
created: 2019-09
summary: |
  1. Einleitung                                       - Jürgen Müller
  2. Bericht von der Jahrestagung 2019                - Sven Bergmann
  3. Bericht von der Mitgliederversammlung 2019       - Andreas Schreck
  4. Sven Bergmann stellt sich vor                    - Sven Bergmann
  5. Glückauf                                         - Thomas Dreessen
  6. Zu den Briefen aus Wim und Lien Leenmans Nachlaß - Eckart Wilkens
  7. Übersicht Lecture-Bearbeitungen                  - Eckart Wilkens
  8. Angebot Rosenstock-Huessy Bücher                 - Thomas Dreessen
  9. Amerika                                          - Eckart Wilkens
  10. Termin der Jahrestagung 2020                    - Jürgen Müller
  11. Adressenänderungen                              - Thomas Dreessen
  12. Hinweis zum Postversand                         - Andreas Schreck
  13. Jahresbeiträge 2019                             - Andreas Schreck
zitat: |
  >Das Problem war also, zwei dem Wesen nach identische Gewalten: herzogliche und königliche, über ein und dasselbe Gebiet zur Aussöhnung zu bringen. Der Herzog von Bayern erläßt um das Jahr 1000 zu Ranshofen Gesetze für seinen Stamm. Die Ottonen ihrerseits richten die zerstörten bayrischen Kirchen wieder auf und sprengen später mit ihrer Hilfe die einheitliche Gliederung Bayerns wie der übrigen deutschen Lande in Grafschaften.
  >
  >Der König geht gegen die Herzogsgewalt vor. Und er ist dazu gerüstet durch seine Fähigkeit, neue Gerichts- und Friedensbezirke abzugrenzen.So kann er innerhalb der einzelnen Stammesgebiete neue Plätze, neue Dingstätten, Forsten usw. aussondern, „bannen”. Denn das Banngebot kann der König nach Ermessen handhaben.
  >*ERH, Königshaus und Stämme, Dritter Abschnitt: Das Haus des Königs, p.19*
---

## Eugen Rosenstock-Huessy Gesellschaft e.V.

{{ page.zitat }}

**Vorstand/board/bestuur:**\
*Dr. Jürgen Müller (Vorsitzender);*\
*Thomas Dreessen; Andreas Schreck; Dr. Eckart Wilkens; Sven Bergmann*\
*Antwortadresse: Jürgen Müller: Vermeerstraat 17, 5691 ED Son, Niederlande*\
*Tel: 0(031) 499 32 40 59*

***Brief an die Mitglieder September 2019***

**Inhalt**

{{ page.summary }}

### 1. Einleitung
Unsere diesjährige Jahrestagung stand unter dem Thema: „Was ist unsere Heimat nach dem Weltkrieg? Rosenstock-Huessys Zeitansage 1919” war eine Fortführung des Themas des letzten Jahres: „Deutsch – Europa – Planet Erde: Wie übersetzen wir den Volksnamen Deutsch in die Sprache des Menschengeschlechts?” Rosenstock-Huessy machte im zugrundeliegenden Text von 1919 „Der Heimfall der Heimat” die Ankündigung eines Lügenkaisers und gestand sowohl Nationalisten wie Kosmopoliten (wörtlich „Demokraten”) zu die richtigen Fragen zu stellen. Er selber betonte allerdings die personalen Verbindung von Herkommen und Zukünftigem.
Mit dem Ende des ersten Weltkrieges (und seiner Wiederholung) war der Siegeszug des Nationalen über die Reiche abgeschloßen. Zwei Drittel der europäischen Bevölkerung bekam ihren Nationalstaat, ein Drittel wurde zur Minderheit. Ebenfalls in Die Hochzeit des Krieges und der Revolution (1919) kommt Rosenstock-Huessy auf den Begriff der Souveränität zu sprechen, der im 15.ten Jahrhundert entstand:
*”Seitdem graben die europäischen Staaten ihre Grenzen immer tiefer, immer nachdrücklicher in die Erde hinein. Immer deutlicher scheiden sich die Zustandseinheiten und ihre Geister von einander ab. Der Begriff der Souveränität ist es, der diesen Ablösungsprozeß, dieses Verabsolutierungsstreben der einzelnen örtlich befangenen Einheit staatlichen Zustandes auf Erden beschreibt. Der Begriff hat eine doppelte Seite, nach oben und nach unten. Der Begriff der Souveränität zerstört nämlich ebensosehr die höheren Begriffe oberhalb des Staats: des Universalstaats, des Reichs, der Ökumene, wie er nach unten die niederen Begriffe: der Stände, des Widerstandsrechts, die Urwüchsigkeit der Korporationen und des Gewohnheitsrechts auslöscht. Er hat also zwei Fronten, nach außen gegen den Oberstaat, und nach innen gegen den von ihm erfaßten Menschheitsbruchteil, sein sogenanntes Volk, und gegen den von ihm besetzten Erdbruchteil, sein sogenanntes Gebiet. Sein Verhalten nach innen und außen sind nur zwei Seiten ein und desselben Prozesses: des Strebens nach Souveränität.”*
Der oben erwähnte Abschluß mag dann für die gewachsenen Reiche gelten, die Frage des richtigen Verhältnisses und einer stimmigen Gestalt von Nationalem und Supranationalem bleibt hochaktuell.
Unser neues Vorstandsmitglied Sven Bergmann wird über die diesjährige Jahrestagung berichten, Andreas Schreck über die Mitgliederversammlung. Weitere Beiträge beschäftigen sich mit Nachgelaßenem, einer neuen Zugänglichmachung und aktuellen Erlebnissen.
Ich wünsche Ihnen neue Ermutigung beim Hören auf Rosenstock-Huessy.
>*Jürgen Müller*

### 2. Bericht von der Jahrestagung 2019
***Warum nicht alle Hochzeiten Hochzeiten sind***\
Die Eugen-Rosenstock Gesellschaft hört auf das Jahr 1917, die Weltrevolution, den Weltkrieg und die Liebe zwischen Eugen, Margrit und Franz.

Auf der Höhe der Zeit zeigte sich die Eugen Rosenstock-Huessy Gesellschaft mit ihrer diesjährigen Tagung: „Was ist unsere Heimat nach dem Weltkrieg?Rosenstock-Huessys Zeitansage 1919”. Kaum ein Wort wird im Zeichen globaler Migrationsbewegungen und dem schwindenden Vertrauen in übernationale Ordnungen mehr gedreht und gewendet als Heimat. Der Begriff ist zum Staatsauftrag mutiert, Edzard Reuter und Can Dündar sprechen über „Geraubte Heimat – Exil in der Türkei” (ZDF 2019) und die Bücher von Heimathäppchen bis Heimatkrimi sind Legion. Richard Faber wittert alte und neue Populismen. Kann da ein Text, „geschrieben nach dem Friedensschluß von Versailles”, eine Perspektive bieten?

Die Teilnehmer stellten sich dieser Herausforderung in Konzentration auf den letzten, umfangreichsten Absatz „Heimfall der Heimat” aus dem Aufsatz „Ehrlos – Heimatlos”, der 1920 in dem Buch „Die Hochzeit des Kriegs und der Revolution” im Patmos-Verlag-Würzburg gedruckt worden ist. Die international besetzte Tagung versammelte, neben dem starken niederländischen Freundeskreis, Gäste aus Spanien und Rußland. Als Höhepunkte der Tagung dürften sicher der Vortrag von Eugenio Muniero zur Stammes-, Reichs- und Staats- geschichte Spaniens und die Rezitation eines Weihnachtsgedichts Eugen Rosenstock-Huessys an seine Frau Margrit in Erinnerung bleiben. Eckart Wilkens trug das Gedicht nach eigener Komposition als Sprachgesang im Zeitstil Alban Bergs vor.

Schon der Begriff Heimfall entpuppte sich als alles andere als leichte Kost. Schließlich schöpft Rosenstock-Huessy hier aus seinen rechtshistorischen Kenntnissen und knüpft an Arbeiten von Rudolph Sohm „über das Hantgemal” und Otto von Gierke „Erbrecht und Vicinenrecht, im Edikt Chilperichs” an. In seinem Buch über „Königshaus und Stämme” steht Heimfall als „wichtigster Unterschied zum Feudalsystem Frankreichs”. Was sich theoretisch anhört, erweist im Spiegel der Zeitläufe seine Brisanz: 1917, Kriegseintritt Amerikas und Revolution in Rußland und 1918, Revolution im Deutschen Reich und Flucht des Kaisers in die Niederlande. Die Hohenzollern haben vor der Geschichte versagt und das Erbe verspielt, das deshalb an die deutschen Stämme heimfällt. Der Soldat und Jurist Eugen Rosenstock rechnet ab mit der Monarchie, mit ihren Säulen Kirche und Staat in Preußen, an die er bis 1917 noch geglaubt hatte.

In verschiedenen Diskussionsbeiträgen schälte sich mehr und mehr die grundlegende Bedeutung des Textes heraus. Das gilt für die welthistorische Perspektive, das Auslaufen von „Staat” und „Kirche” sowie der Anbruch der Weltgesellschaft, angestoßen durch Weltkrieg und Weltrevolution.
Und Eugen Rosenstock prophezeit „in den kommenden Jahrzehnten” Judenhaß und ein Lügenkaisertum durch alle die, die sich den Zeichen der Zeit widersetzen und durch die „Deutschland in eine Hölle verwandelt wird”. Die Zukunft sollte seine Mahnungen schrecklich bestätigen. Parallel zu Max Webers „Polarnacht” von „eisiger Finsternis und Härte” greift Eugen Rosenstock für den 9. November zum Bild der Mitternacht, der Geisterstunde und beschwört sicher nicht zufällig den Zeitpunkt, an dem Doktor Faustus endet und die erste Szene in Shakespeares Hamlet beginnt.

*Es ist Mitternacht. Nichts ist übrig von dem „Tag der Ehre”, der über dem deutschen Reiche seit Sedan geleuchtet hat. Vom Reichsdasein ist schlechthin nicht mehr übrig, das würdig oder wert wäre.*
(Eugen Rosenstock, Ehrlos – Heimatlos (Geschrieben nach dem Friedensschluß von Versailles.), in: ders., Die Hochzeit des Kriegs und der Revolution (Der Bücher vom Kreuzweg erste Folge), Würzburg: Patmos-Verlag 1920, S.223.)

Und als sei der Panoramablick noch nicht Herausforderung genug, so spiegelt die Schrift auch eine sehr intime Beziehung. Bereits im Krieg hatten Eugen Rosenstock und Margrit Huessy 1914 geheiratet. 1917 lernten sich Margrit und Franz Rosenzweig kennen. Der Beginn einer verwickelten Dreiecksbeziehung, die alle Beteiligten lebenslang prägen sollte und bis heute Anlaß für Aufregungen, Vermutungen und Unterstellungen gibt.

***Weihnachten 1917***\
Das verlautbarte Wort ist lebendig, nicht das Geschriebene, leitete Eckart Wilkens seinen Vortrag von Eugen Rosenstocks Gedicht "Die unsichtbare Welt" ein. Als Weihnachtsgabe an seine Frau Margrit ging das Gedicht ab aus der Stellung vor Verdun „im Dunkel einer unsichtbaren Nacht”. Das Gedicht schloß mit der Hoffnung auf das Licht der Weihnacht, um die bösen Geister zu verscheuchen. Eckart Wilkens erläuterte nicht nur die Zeitumstände und trug das dreizehnstrophige Gedicht vor. Im Stile Alban Bergs hatte er den Sprachgesang von Anfang bis Ende auskomponiert. Dabei erinnerte er auch an Auftritte des collegium musicum iudaicum in der Kölner Volkshochschule, unter anderem mit Musik zum Stern der Erlösung von Franz Rosenzweig.

***Spaniens „traumatische Geschichte”***\
Einen weiteren Höhepunkt der Werdener Tagung bildete sicher der Vortrag von Eugenio Muinelo aus Madrid.

Vor dem Hintergrund des „Europäischen Kollaps” und in Annäherung an die Begriffe Eugen Rosenstock-Huessys unternahm Eugenio Muinelo einen Abriss der spanischen Geschichte, von der Reichsbildung, über die Staatsbildung bis zu Spanischem Bürgerkrieg und Franco-Regime. Entscheidende Bedeutung kam dabei der Erstarrung im Zuge der Vollendung der staatlichen Souveränität und der Judenverfolgung 1492 zu. Als übergreifende Thesen erfuhren die Hörer von der bisher nicht aufgearbeiteten „traumatischen Geschichte Spaniens” sowie daß Spanien mit der Diskriminierung der Juden und dem Umgang mit den Maranen, getauften Juden, Aspekte der Judenverfolgung in späteren Jahrhunderten vorweggenommen habe. Dabei bezog sich Eugenio Muinelo auch auf weniger bekannte Schriften José Ortega y Gassets, etwa die Rede über alte und neue Politik von 1914. Ortega y Gasset hatte von 1905 bis 1911 u.a. in Deutschland bei Hermann Cohen in Marburg studiert, der auch für Franz Rosenzweig und Eugen Rosenstock-Huessy eine wichtige Bezugsperson war. Ortega y Gasset war Gegner von Militarismus und Nationalismus und ein Verfechter europäischer Zusammenarbeit, auch wenn er Volk anders einordnete als Eugen Rosenstock-Huessy.

Im Zuge der Staatsbildung sei Spanien in heidnische Vorurteile zurückgefallen, wobei mit Alfonso de Santa María de Cartagena (1384 – 1456) ausgerechnet ein konvertierter Jude eine unrühmliche Rolle gespielt habe. Der Kleriker und Diplomat vertrat Kastilien auf dem Konzil von Basel. Heinrich Graetz schreibt ihm den Einfluß auf den am 3. März 1431 in Rom zum neuen Papst gewählte Eugen IV. zu, der zuvor gegebene Privilegien an Juden widerrief. Erst nach dem 2. Weltkrieg verzichtete Spanien auf die Idee des Reichs, ökumenisch nach oben und nach unten gegenüber Ständen und anderen Rechtsträgern und konnte sich so Europa öffnen. Abschließend bemerkte Eugenio Muinelo, daß in weiten Phasen der spanischen Geschichte die Nation zu einem bloßen Leichnam gemacht worden sei. Die libido dominandi und die Ausgrenzung des Anderen habe jede Form von civitas oder Politik zerstört. Diese traumatische Geschichte Spaniens sei bisher nicht aufgearbeitet. Dies seit um so tragischer als die Spanische Geschichte europäische Phänomene oft um Jahrhunderte vorwegnimmt. Dabei sei der Name Franz Rosenzweig heute in Spanien bekannt, während Eugen Rosenstock-Huessy ein unbeschriebenes Blatt sei.

In der Diskussion wurde das verschollene Kapitel über Spanien aus dem Buch über die Europäischen Revolutionen angesprochen, das Eugen Rosenstock-Huessy im Briefwechsel mit Carl Schmitt angibt. Der Verleger, Eugen Diederichs, habe auf Kürzungen gedrängt, deshalb sei das Kapitel nicht aufgenommen werden. Ein Indiz für seine Existenz könnte die Freundschaft von Eugen Rosenstock-Huessy mit dem Historiker Peter Rassow sein, einem der weniger Kenner Spanischer Geschichte in Deutschland. Theodor Maunz führt in seiner „Kriegsschrift”, Das Reich der spanischen Grossmachtzeit, Hamburg: Hanseatische Verlagsanstalt 1944, die einschlägigen Schriften Carl Schmitts auf, verzeichnet aber keinen Beitrag Ortega y Gassets.

***Heiden, Juden, Christen***\
Blicket hin auf die Geschichte des deutschen Volkes. Es ist nicht ein Volk, ein Staat, und ist noch nie ein Staat gewesen. Immer waren die Stämme der Deutschen vielfältig. Immer behaupteten sie ihre Zerrissenheit. Immer fehlte die sichere Einheit und Ordnung. Nur zum Kampf, um der Heerfahrt, um des Römerzuges, um der Kreuzfahrt, um der Türkengefahr oder der Wacht am Rhein willen nahmen die Männer einen Heerkönig, einen Kaiser, einen obersten Kriegsherrn über sich. (Ehrlos – Heimatlos, S.240)

Nach der gemeinsamen Lektüre von „Heimfall der Heimat” und vor dem Hintergrund der fragilen und weiter zerfasernden Weltordnung, wo jeder sich zur Nummer Eins proklamiert, um die der Planet kreist, entwickelte sich eine sehr persönliche Diskussion zu Eugen Rosenstock-Huessys Verständnis von Stamm, Reich, Nationalstaat und Gesellschaft. Unter dem Mantel der Reiche, Staaten und Gesellschaften lugen die Stämme nach wie vor heraus, ob die Schwaben, Sachsen oder Bayern in Deutschland, die Friesländer, Limburger oder die Menschen aus Drenthe in den Niederlanden oder Basken, Katalanen oder Asturier in Spanien, wie in Afrika wie in Asien. Dabei gibt es sicher keine Gebrauchsanweisung zur historischen Idealentwicklung. Für Eugen Rosenstock-Huessy stand 1918 aber felsenfest, daß das Zeitalter der Weltgesellschaft angebrochen ist. Deshalb ist die „Hochzeit” im Buchtitel für seine Sicht so treffend. Die Welt ist voll geworden. Das bedeutet aber nicht, daß man Stamm oder Nation einfach durch Europa oder Rußland oder China ersetzen kann. Eine solche ultramoderne Sicht ist sicher naiv. Die Sowjetunion steht ja exemplarisch für eine gescheiterte Kopfgeburt. Aber ein Rückfall auf die Stammesgruppierung ist genauso sicher töricht.

In dem Pferch Deutsches Reich kann nicht immer weiter so nebeneinander her vom irdischen Vaterland der Deutschen, von dem Zion der Juden und vom geistigen Zion der Christen gesungen werden, als sei eines ohne das andere zu denken. Sie sind ja nicht nur auseinander, Juden, Christen und Heiden, sondern sie sind außerdem zu verschiedenen Zeiten aus ein und dem selben Geschöpf abgezweigt. Heide, Jude, Christ folgen aufeinander am Stammbaum des Menschengeschlechts. Sie stehen aufeinander, auch wenn sie selbständig nebeneinander stehen; und so müssen sie sich zwar abstoßen und begrenzen, aber auch gegenseitig tragen und halten. (Ehrlos – Heimatlos, S.247)

Auf dem dünnen Eis der Zivilisation ist jeder vor die Wahl gestellt, vor die Frage gestellt ja oder nein zu sagen. Die Geschichte wird nicht von Historikern geschrieben. Das Reden über die „Erfindung” von Geschichte und Tradition ist ja nicht mehr als Geschichtskitsch. Daß 1917, daß der 11. September 2001, daß 1989 (4. Juni und 9. November), daß 1945, 1933, 1789, 1492, 1077 oder 70 Zäsuren und damit historische Daten sind, ist auch ohne jede „Wissenschaft” augenscheinlich. Und daß diese Daten Punkte eines bestimmten Zeitstroms bilden, war für Eugen Rosenstock-Huessy evident.

Im Krieg und im Zusammenbruch hat auch den „geistigen” Menschen das leibliche, augenblickliche Schicksal unsanft geweckt. Die Erde bebte so stark, bis er merkte, daß die Politik auch sein Schicksal, daß auch sein geistiges Leben Tageswerk bleibt.
(Eugen Rosenstock-Huessy, Werkstattaussiedlung. Untersuchungen über den Lebensraum des Industriearbeiters, in Verbindung mit Eugen May und Martin Grünberg (= Sozialpsychologische Forschungen; Bd.2), Berlin: Verlag von Julius Springer 1922, S.75.)

Eugen Rosenstock-Huessy vermag Zusammenhänge zu erschließen, um seinen Zuhörern zu helfen, die richtigen Fragen zu stellen. Für die Antwort steht jeder in seiner, jede in ihrer Verantwortung. Und wie wir in „Hochzeiten” antworten, bestimmt über unseren Namen, als Wohltäter, Inspirator, Mitläufer, Retter, Verräter oder Lügenkaiser. In seiner Zeit hat Eugen Rosenstock-Huessy oft nicht den leichten Weg gewählt und dafür auch schwere Lasten geschultert; er hat nie einfach mitgemacht, sich nie bloß treiben lassen und deshalb Spuren hinterlassen und Maßstäbe gesetzt, bis heute und für morgen.
>*Sven Bergmann*

### 3. Bericht von der Mitgliederversammlung 2019
Herausragendes Merkmal der Mitgliederversammlung war die Kandidatur von Sven Bergmann. Es ist schon lange nicht mehr geschehen, dass ein erst kürzlich beigetretenes Mitglied das Vertrauen zu verantwortlicher Mitarbeit aufbringt. Nach der Wiederwahl der bisherigen drei geschäftsführenden Vorstandsmitglieder Jürgen Müller, Thomas Dreessen und Andreas Schreck wurden Eckart Wilkens und – neu – Sven Bergmann ohne Gegenstimmen in den Vorstand gewählt. Bedauerlicherweise bleibt ein gravierendes Defizit weiterhin zu konstatieren: keine weibliche Stimme erhellt den Vorstandschor.
>*Andreas Schreck*

### 4. Zur Person Sven Bergmann
Auch wenn ich als Bergmann in Essen geboren wurde, habe ich in Bochum studiert. Am Ende stand ich mit einem Magister Artium in Geschichte vor verschlossenen Türen. Deshalb habe ich eine Ausbildung zum Journalisten absolviert und zwanzig Jahren die interne und externe Kommunikation für Unternehmen, Verbände und Vereine betreut. Ich lese ungesund viel, fahre gerne Rad und koche am Wochenende mit frischen Zutaten. Alte Musik auf zeitgenössischem Instrumentarium interessiert mich ebenso wie die Werke von Eigenbrötlern wie Allan Pettersson oder Tristan Keuris.
>*Sven Bergmann*

### 5. Glückauf
Fridays for future ist ein Weckruf . Die Resonanz zeigt, dass viele ihn erhören – auf dem ganzen Planeten. Für den nötigen Wandel habe ich in meinem Kontext Ruhrgebiet die Hymne der Bergleute umgedichtet.

**Mit Glückauf in unsere gemeinsame Zukunft**

  1. Glück auf, Glück auf, der Steiger kommt\
    Und er hat sein helles Lichtbei der Nacht,\
    Und er hat sein helles Lichtbei der Nacht,\
    schon angezündt, schon angezündt.

  2. Schon angezündt! Das gab 'nen Schein\
    Und damit so fuhren wirbei der Nacht,\
    und damit so fuhren wirbei der Nacht,\
    ins Bergwerk ein, ins Bergwerk ein.

  3. Ins Bergwerk ein, wo die Bergleut sein,\
    die da gruben das Silber und das Goldbei der Nacht,\
    die da gruben das Silber und das Goldbei der Nacht,\
    aus Felsgestein, aus Felsgestein.

  4. Glück auf, Glückauf! auch wir sind Bergleut‘\
    Die da suchen neues Lichtin der Nacht\
    Die da suchen neues Lichtin der Nacht\
    In dieser Zeit, in dieser Zeit.

   5. Wir komm‘ zusamm'! Bei gutem Schmaus,\
     Zünden an ein neues Lichtin der Nacht\
     Zünden an ein neues Lichtin der Nacht\
     Im Planetenhaus, im PlanetenHaus.

   6. Im Hause drin, viel Völker sind\
     Zusamm‘ wir beraten was tunin der Nacht\
     Zusamm‘ wir beraten was tunin der Nacht\
     für Erd` und Kind', für Erd` und Kind.

   7. Dann tanzen wir, und singen froh\
      Und der Morgen leuchtet neunach der Nacht\
      Und der Morgen leuchtet neunach der Nacht\
      Dann ziehn wir los, dann ziehn wir los.

  8. Glückauf, Glückauf! Der Frieden kommt\
     Alle werden gebrauchtfür den neuen Tag\
     Alle werden gebrauchtfür den neuen Tag\
     Im Bergwerk Planet, im Bergwerk Planet.

©Neue Fassung\
T.Dreessen 2018\
[Hör Ma!](https://m.youtube.com/watch?v=ThfziLXA4dE)
>*Thomas Dreessen*

### 6. Zu den Briefen aus Wim und Lien Leenmans Nachlaß
(ein vergessener Beitrag für den ERHG Mitgliederbrief Februar 2019)

In dem Briefwechsel zwischen Ko Vos und Freya von Moltke, den Marlouk von Lien Leenman fürs Archiv übergeben hat, steht zu lesen, daß Ko Vos während der Entstehung der erneuten Herausgabe der Soziologie von 1956/58 die Einsicht gekommen ist, daß die Zeit der Bücher vorbei ist. Oder vielmehr: daß das Lesen von der ersten Stelle an die zweite (oder dritte?) gerückt ist, das heißt vom Präjektivum, was es für die zu Rat und Tat Ge- und Berufenen seit der Reformation gewesen ist, zum Subjektivum, zum Feld, auf dem die Schätze erst ins Innere wandern müssen, ehe sie trajektiv, das heißt fruchtbar werden. Und die Editionsgeschichte mit dem Desaster, das die Eugen Rosenstock-Huessy Gesellschaft mit dem Talheimer Verlag erlebt hat, ist vielleicht Beleg für diese Erkenntnis.
So kann denn die Verwunderung, daß Eugen Rosenstock-Huessy bei so vielen Vorlesungen, die dank der Initiative von Russ Keep auf Tonband aufgenommen wurden, es dabei beließ, daß sie ihren Weg auch weiter zum Ohr, wie Ricarda Huch gesagt hat: dem Tor zum Herzen finden würden. Die Transkriptionen von Francis Huessy sind ja vordringlich dazu verfaßt worden, daß die Hörer beim Hören Hilfe hätten.
Ich bin nun dazu gelangt, etliche dieser Vorlesungen ähnlich wie die Soziologie und andere Werke Rosenstock-Huessys nach dem Gehör zu gliedern und dabei auch Editionsarbeit zu leisten, indem Floskeln der mündlichen Rede weggelassen werden und dem Leser das flüssige Verfolgen ermöglicht wird, das ein Hörer, mögen auch Störungen welcher Art auch immer, wie von selber aufbringt, weil das Gedächtnis doch streng wählt zwischen dem vom Sinn des Ganzen her Notwendigen und dem Beiwerk, das diesen Sinn, damit wir Zeit gewinnen, umgibt.
Und da kommt nun eine andere Art des Lesens heraus, die sich dem alten Gebrauch von Schrift annähert, nämlich daß sie verlautet wird. Ich habe immer so gelesen, daß ich innerlich die Worte verlautet habe – aber das ist doch längst nicht mehr Usus.
Meine Übersetzung des Gehörseindrucks mitsamt dem Fassen des Sinns fordert den Leser nun auf, die Geschwindigkeit, die das Sehen erlaubt, zu verlangsamen und also das Tor zum Herzen zu öffnen, die Hörkraft, die nämlich zwischen dem entscheidenden Augenblick und der Ewigkeit eine Brücke schlägt, von der es in einem seiner Gedichte heißt: Augenblicke, Ewigkeiten zueinander zu bekennen ist des Wortewesens Sinn.

 Gerade bin ich mit der Bearbeitung der 25 Vorlesungen Universal History 1954 beschäftigt und möchte alle, die mögen, darauf anspitzen, die Zeit aufzubringen, das in Ansprache, Ausruf, Erzählung und Feststellung ausgebreitete Wissen der Soziologie von 1956/58 in aktueller Form nachzulesen.
>*Eckart Wilkens*

### 7. Übersicht Lecture-Bearbeitungen
 - Eckart Wilkens hat im Laufe der letzten Jahre viele der aufgenommenen und transkribierten Vorlesungen Rosenstock-Huessys bearbeitet.
In der Übersicht sind es die fett gedruckten Elemente.

***Rosenstock-Huessy 's Lecture Courses (1949-1968)***

| 1949 | VOLUME 1:  | CIRCULATION OF THOUGHT (1949)         | **(6 LECTURES)**  |
|      | VOLUME 2:  | UNIVERSAL HISTORY (1949)              | (6 LECTURES)      |
| 1951 | VOLUME 3:  | UNIVERSAL HISTORY (1951)              | (1 LECTURE)       |
| 1952 | VOLUME 4:  | POTENTIAL TEACHERS (1952)             | (2 LECTURES)      |
| 1953 | VOLUME 5:  | CROSS OF REALITY (1953)               | (24 LECTURES)     |
|      | VOLUME 6:  | HINGE OF GENERATIONS (1953)           | **(14 LECTURES)** |
|      | VOLUME 7:  | MAKE BOLD TO BE ASHAMED (1953)        | (6 LECTURES)      |
| 1954 | VOLUME 8:  | COMPARATIVE RELIGION (1954)           | **(28 LECTURES)** |
|      | VOLUME 9:  | CIRCULATION OF THOUGHT (1954)         | **(26 LECTURES)** |
|      | VOLUME 10: | FOUR DYSANGELISTS (1954)              | **(2 LECTURES)**  |
|      | VOLUME 11: | HISTORY MUST BE TOLD (DRAFT, 1954)    | **(1 LECTURE)**   |
|      | VOLUME 12: | UNIVERSAL HISTORY (1954)              | **(25 LECTURES)** |
| 1955 | VOLUME 13: | HISTORY MUST BE TOLD (1955)           | (1 LECTURE)       |
|      | VOLUME 14: | UNIVERSAL HISTORY (1955)              | (4 LECTURES)      |
| 1956 | VOLUME 15: | CIRCULATION OF THOUGHT (1956)         | (1 LECTURE)       |
|      | VOLUME 16: | GREEK PHILOSOPHY (1956)               | **(26 LECTURES)** |
|      | VOLUME 17: | UNIVERSAL HISTORY (1956)              | (9 LECTURES)      |
| 1957 | VOLUME 18: | UNIVERSAL HISTORY (1957)              | (29 LECTURES)     |
| 1959 | VOLUME 19: | AMERICAN SOCIAL HISTORY (1959)        | **(42 LECTURES)** |
|      | VOLUME 20: | HISTORIOGRAPHY (1959)                 | **(13 LECTURES)** |
|      | VOLUME 21: | MAN MUST TEACH (1959)                 | **(1 LECTURE)**   |
| 1960 | VOLUME 22: | LIBERAL ARTS COLLEGE (1960)           | **(1 LECTURE)**   |
|      | VOLUME 23: | WHAT FUTURE THE PROFESSIONS (1960)    | (4 LECTURES)      |
| 1962 | VOLUME 24: | GRAMMATICAL METHOD (1962)             | (3 LECTURES)      |
|      | VOLUME 25: | ST. AUGUSTINE BY THE SEA (1962)       | **(6 LECTURES)**  |
| 1965 | VOLUME 26: | ECONOMY OF TIMES (1965)               | **(5 LECTURES)**  |
|      | VOLUME 27: | TALK WITH FRANCISCANS (1965)          | **(2 LECTURES)**  |
|      | VOLUME 28: | CROSS OF REALITY (1965)               | **(1 LECTURE)**   |
| 1966 | VOLUME 29: | LINGO OF LINGUISTICS (1966)           | **(3 LECTURES)**  |
|      | VOLUME 30: | PEACE CORPS (1966)                    | **(3 LECTURES)**  |
| 1967 | VOLUME 31: | CRUCIFORM CHARACTER OF HISTORY (1967) | **(5 LECTURES)**  |
|      | VOLUME 32: | UNIVERSAL HISTORY (1967)              | **(20 LECTURES)** |
| 1968 | VOLUME 33: | FASHIONS OF ATHEISM (1968)            | **(1 LECTURE)**   |
|      | VOLUME 34: | THE UNIVERSITY (1968)                 | **(1 LECTURE)**   |

*available at www.erhfund.org*\
**bold face: edited version by Eckart Wilkens**\
Die Bearbeitungen machen die Texte Rosenstock-Huessys leichter zugänglich indem sie Wiederholungen sichtbar machen und den vielen externen Beispielen einen Platz geben. Die Systematik des Textes wird so auch in der räumlicher Gestalt deutlich. Wir bedanken uns dafür bei Eckart recht herzlich.
>*Jürgen Müller*

### 8. Angebot: Rosenstock-Huessy Bücher
Wir haben eine Reihe von Büchern Eugen Rosenstock-Huessys erhalten aus der Auflösung der Bestände im Rosenstock-Huessy-Huis in Harlem und von Herrn Rabus aus den Beständen von Daimler Benz.
Interessierte können diese bei mir erhalten:
- Magna Charta Latina 1x
- Die Tochter 5x
- Werkstattaussiedlung 20x
- Daimler Werkszeitung 4x
- Des Christen Zukunft 1955 dt. 3x
- Soziologie I 1958 3x
- Die Europäischen Revolutionen 4x
- Stimmstein 1 1x
- Ja und Nein 1x
- Der unbezahlbare Mensch 2x
- Dienst auf dem Planeten 1x
- Die Sprache des Menschengeschlechts 2 Bände 20x
- Dabei auch: G.Müller: Last und Trost der deutschen Geschichte 1x
Kosten: Porto und wenn möglich eine Spende
>*Thomas Dreessen*

### 9. Amerika
Von dem Wiedersehen des Hauses Eugen und Margrit Rosenstock-Huessys in Norwich/ Vermont. „Four Wells” und dem Eindruck von dem Besuch ihrer Gräber auf dem Friedhof, wo auch Freya und Konrad von Moltke sowie Hans Rosenstock Huessy begraben sind – zu- letzt war ich dort im Jahre 1988, als die große Jahrhundertfeier stattfand, wo ich in dem Klassenzimmer, wo Eugen unterrichtet hat, davon gesprochen habe, was die vier Auferstehungsberichte der vier Evangelisten unterscheidet – möchte ich gern die beiden Gedichte mitteilen, die den Eindruck dort gleich aussprachen:

Das Grab Margrit und Eugen Rosenstock-\
Huessys – die Hügelzüge lassen\
die Blicke nach wie vor schweifen –\
sagt: die Auferstehung in wandelnd\
bleibender Erscheinung ist in\
*Palästina, Schlesien und anderswo*\
geschehen, die Gewißheit Jakobus\
1, Vers 25 steht wohl beschattet im Licht.

*auf dem Friedhof in Norwich/Vermont*\
*Joseph Wittig, Leben Jesu in Palästina, Schlesien und anderswo, 1926*\
*Spruch auf Margrits Grab*

Das heißt: es war mir, als spräche eine Stimme: „Nicht hier – sondern er ist auferstanden”, wir können uns nicht darauf verlassen, daß es da nun ist und als Kultort bleibt, sondern sollen das Wort des Paulus beherzigen (Röm. 10, 6-8): **6** Aber die Gerechtigkeit aus dem Glauben spricht so (5.Mose 30,11-14): »Sprich nicht in deinem Herzen: Wer will hinauf gen Himmel fahren?« – nämlich um Christus herabzuholen –, **7** oder: »Wer will hinab in die Tiefe fahren?« – nämlich um Christus von den Toten heraufzuholen –, **8** sondern was sagt sie? »Das Wort ist dir nahe, in deinem Munde und in deinem Herzen.« Dies ist das Wort vom Glauben, das wir predigen.

Dann das zweite Gedicht:

Wo alles geschehen ist, da ist nun\
Stille, die Regungen, von denen\
die Nachbarn keine Ahnung gehabt,\
die Kollegen nicht, fragen ganz\
ohne Aufwand: Hast Du darauf\
geachtet, wie sich Trotz und Liebe\
vermählt haben, lebendig und\
über die Maßen im Wort „herberget gern”?

*bei dem Haus, das Eugen und Margrit Rosenstock-Huessy gebaut haben,
Four Wells, Norwich, Vermont*\
*Römer 12, 13*

Da schimmert also der Dank an Freya von Moltke, deren Gastlichkeit wir, Sigrid und ich mit den Kindern Julia, Caroline, David, Niklas, Hanna zusammen genossen haben, später dann auch Simon (aber da war ich nicht mit dabei). Ich war wirklich ganz frei und die förderlichen Absichten, die Freya gewiß hegte, drängten mich nicht – und auch nicht mehr, als die Besuche aussetzten, weil die Freundschaft mit Konrad und Ulrike von Moltke aussetzte.
Inzwischen weiß ich: Four Wells war zwar das Amerika der Zukunft, aber das Amerika von damals hatte so gut wie keine Ahnung davon. Das wußte ich damals nicht, als ich die Sonne in Vermont als Ausdruck planetarischer Herzlichkeit erlebte.
Auch da also der Verweis auf die eigene Existenz, wo auch immer sie ist.
>*Eckart Wilkens*


### 10. Termin der Jahrestagung und Mitgliederversammlung 2020
Die Jahrestagung und die Mitgliederversammlung 2020 möchten wir wieder, wie die beiden vorherigen, an Palmarum (3.5.4. 2020) im Haus am Turm, am Turm in Essen-Werden abhalten. Wir bitten Sie sich diesen Termin vorzumerken. Thema und Programm werden wir Ihnen noch mitteilen.
>*Jürgen Müller*

### 11. Adressenänderungen
Bitte schreiben sie eine eventuelle Adressenänderung schriftlich oder per E-mail an Thomas Dreessen (s. u.), er führt die Adressenliste. Alle Mitglieder und Korrespondenten, die diesen Brief mit gewöhnlicher Post bekommen, möchten wir bitten, uns soweit vorhanden ihre Email-Adresse mitzuteilen.
>*Thomas Dreessen*

### 12. Hinweis zum Postversand

Der Rundbrief der Eugen Rosenstock-Huessy Gesellschaft wird als Postsendung nur an Mitglieder verschickt. Nicht-Mitglieder erhalten den Rundbrief gegen Erstattung der Druck- und Versandkosten in Höhe von € 20 p.a. Den Postversand betreut Andreas Schreck. Der Versand per e-Mail bleibt unberührt.
>*Andreas Schreck*

### 13. Jahresbeiträge 2019
Die „Selbstzahler” unter den Mitgliedern werden gebeten, den Jahresbeitrag in Höhe von 40 Euro (Einzelpersonen) auf das Konto Nr. 6430029 bei Sparkasse Bielefeld (BLZ 48050161) zu überweisen.
Nach Einführung des einheitlichen Zahlungssystem SEPA mit der Internationalen Kontonummer (IBAN): DE43 4805 0161 0006 4300 29
SWIFT-BIC: SPBIDE3BXXX ist diese „IBAN”-Nummer auch für Mitglieder in den Niederlanden und im ganzen „SEPA-Land”, in das sich sogar die Schweiz hat einbetten lassen,
ohne Zusatzkosten nutzbar.
>*Andreas Schreck*


[zum Seitenbeginn](#top)
