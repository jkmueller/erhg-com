---
title: Mitgliederbrief 2009-01
category: rundbrief
created: 2009-01
summary:
zitat: |
  >„Der einzige Weg der Menschwerdung ist die liebende Vorwegnahme. Gelingt es, den ersten Augenblick dieser Vorwegnahme durch ein liebendes Auge zu verewigen, dann ist dieser Menschwerdung der Weg gebahnt. Die menschliche Gesellschaft wird anhaltend von unaufhaltsamen Menschwerdungen durchzogen, unter der Bedingung, daß jeder erste Liebende Blick als Hoher Augenblick Epoche machen darf.”
  >*Eugen Rosenstock-Huessy, Soziologie II, Die Vollzahl der Zeiten, S. 728*
---

## Eugen Rosenstock-Huessy Gesellschaft e.V.

{{ page.zitat }}

**Vorstand/board/bestuur:**\
*Dr. Eckart Wilkens (Vorsitzender);*\
*Wilmy Verhage; Andreas Schreck; Dr. Jürgen Müller; Lothar Mack*\
*Antwortadresse: Dr. Eckart Wilkens: Everhardstraße 77, 50823 Köln, Deutschland*\
*Tel: 0(049) 221 511265*

***Brief an die Mitglieder Januar 2009***

**Inhalt**

{{ page.summary }}


[zum Seitenbeginn](#top)
