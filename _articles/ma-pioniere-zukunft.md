---
title: "Marlouk Alders: „Pioniere unserer gemeinsamen Zukunft – hier kannst DU finden!”"
category: bericht
created: 2019
---
![Marlouk Alders]({{ 'assets/images/Marlouk-Alders.jpg' | relative_url }}){:.img-right.img-small}


Da in den Niederlanden wenig über die deutsche Widerstandsgruppe Kreisauer Kreis bekannt ist und es mir ein Anliegen war, einer breiteren Öffentlichkeit davon zu berichten, habe ich in den Jahren 2015/2016 ein Buch über die Geschichte Kreisaus/Krzyżowas und des Kreisauer Kreises in niederländischer Sprache geschrieben. Diesem Buch geht eine persönliche Geschichte voraus, die ich nicht unerzählt lassen will, weil sie in hohem Maße etwas über meine Verbundenheit mit Kreisau und die Bedeutung des Kreisauer Kreises aussagt.

**Helmuth James von Moltke & der Kreisauer Kreis**\
*Eine persönliche Geschichte, die mich mit Kreisau seit 30 Jahren verbindet*\
von Marlouk Alders

[zum Buch]( {{ 'assets/downloads/Alders_Marlouk_Pioniere_unserer_gemeinsamen_Zukunft_.pdf' | relative_url }})
