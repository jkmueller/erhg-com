---
title: Mitgliederbrief 2021-09
category: rundbrief
created: 2021-09
published: 2021-10-03
summary: |
  1. Einleitung                                            - Jürgen Müller
  2. Tagungsort und Anmeldung                              - Andreas Schreck
  3. Programm der Jahrestagung                             - Jürgen Müller
  4. Einladung zur ordentlichen Mitgliederversammlung      - Jürgen Müller
  5. Adressenänderungen                                    - Thomas Dreessen
  6. Hinweis zum Postversand                               - Andreas Schreck 
  7. Mitgliederbeitrag 2021                                - Andreas Schreck 
zitat: |
  > Die niedrige Grammatik sah sich die Sprache als Wortschatz an. Und so ist es begreiflich, daß sie beim Nominativ anhob als dem ersten Fall und den Vokativ als fünften Fall ausgab oder ansah.\
  Wir hingegen sehen die Geisteskraft sich über keimendes Leben ergießen und aus verfallendem Leben herausreißen. Vom Ganzen ergehen Anrufe, namentliche Anrufe an die ins Leben gerufenen Geschöpfe. Sie ergehen aber nur im Notfall. Darwin hat ganz recht, die Geschichte der Neuschöpfungen in Kämpfen ums Dasein zu periodisieren. Jedes neue Geschöpf ist der Not entsprossen. Jede neue Spezies ringt gerade deshalb uns einen besonderen Namen für sich ab. Darwins Kampf ums Dasein vollendet sich eben in der Zwangsvorstellung die ein Ereignis in uns aufregt, so daß wir nicht ruhen, bis es einen Namen empfangen hat. Die Aufregung über eine Not legt sich, sobald das Kind dieser Not seinen eigenen Namen empfängt. Denn damit ist die Not anerkannt. Und erst im Akt der Anerkennung vollendet sich jegliches Ereignis. Die. namentliche Anerkennung ist der letzte Eindruck, den ein Ereignis hervorbringen muß, um sich als echtes Ereignis zu legitimieren.
  > *Eugen Rosenstock-Huessy,  „Im Notfall” oder „Die Zeitlichkeit des Geistes”, 1963*
---

## Eugen Rosenstock-Huessy Gesellschaft e.V.

{{ page.zitat }}

**Vorstand/board/bestuur:** *Dr. Jürgen Müller (Vorsitzender);\
Thomas Dreessen; Andreas Schreck; Sven Bergmann\
Antwortadresse: Jürgen Müller, Vermeerstraat 17, 5691 ED Son, Niederlande,\
Tel: 0(031) 499 32 40 59*

## Brief an die Mitglieder Juli 2021
Inhalt

{{ page.summary }}

### 1. Einladung zur Jahrestagung und zur Mitgliederversammlung

Liebe Mitglieder, liebe Freunde,\
wir wollen uns dieses Jahr mit der Herausforderung befassen, daß angesichts der aktuellen Ereignisse die akademische Sprache nicht mehr ausreicht und wir scheinen sprachlos zu werden. Rosenstock-Huessy geht im Gegensatz dazu davon aus, daß neue Sprache aus der Not heraus entstehen muß. Wie soll das zusammen passen? Darüber wollen wir uns unter dem Thema: „Sprachnot in den Zeiten der Ereignisse - Rosenstock-Huessys Entdeckung der sozialen Grammatik” austauschen und legen der Jahrestagung den folgenden Text: „Im Notfall” oder „Die Zeitlichkeit des Geistes” zugrunde.

Ich lade Sie zur Jahrestagung und zur Mitgliederversammlung (am 20. November 2021) herzlich ein.
> *Jürgen Müller*

### 2.  Tagungsort und Anmeldung

Die Jahrestagung findet vom 19. bis 21. November 2021 statt im Haus am Turm, am Turm 7, 45239 Essen-Werden, Tel. 0201-404067.   Beginn ist am 19. November 2021 um 18 Uhr mit dem Abendessen.
Kosten
Die Kosten betragen € 120 bei Unterbringung im Doppelzimmer, € 145 bei Unterbringung im Einzelzimmer. Die Anzahl der Einzelzimmer mit Bad/WC ist begrenzt.
Interessenten, die die Tagungskosten nicht in voller Höhe aufbringen können, mögen sich bitte an Andreas Schreck oder ein Vorstandsmitglied wenden.

Anmeldung
bitte an Andreas Schreck, Tel. 0551-28047871;
>*Andreas Schreck*

### 3. Programm der Jahrestagung

***Sprachnot in den Zeiten der Ereignisse -\
Rosenstock-Huessys Entdeckung der sozialen Grammatik***

| Freitag, 12.4 | 18:00 | Eintreffen |
|               | 18:30 | Abendessen |
|               | 19:30 | "Unser Zeitpunkt: Krise – planetarische Notfälle: Unsere Erfahrungen" |
|               | 21:00 | Geselliges Beisammensein |
| Samstag, 13.4 |  8:30 | Frühstück |
|               |  9:30 | Textlesung: „Im Notfall” oder „Die Zeitlichkeit des Geistes” |
|               | 10:15 | Pause |
|               | 10:30 | "Die grammatische Methode: Wann ist was dran?" (Thomas Dreessen) |
|               | 12:15 | Pause |
|               | 12:30 | Mittagessen |
|               | 15:00 | Kaffeepause |
|               | 15:30 | Mitgliederversammlung |
|               | 18:30 | Abendessen |
|               | 20:00 | "Argonautentheater" (Sven Bergmann) |
|               | 21:30 | Geselliges Beisammensein |
| Sonntag, 14.4 |  8:00 | Morgenandacht |
|               |  8:30 | Frühstück |
|               | 10:00 | "Aussprache zur grammatischen Methode" – Plenumsgespräch |
|               | 11:00 | "Welche Aufgaben hat die ERH-Gesellschaft heute" (Jürgen Müller) |
|               | 12:30 | Mittagessen |
|               | 13:00 | Abreise |

### 4. Einladung zur ordentlichen Mitgliederversammlung
am 20. November 2021,  15:30 Uhr\
Haus am Turm - Am Turm 7, 45239 Essen

TOP 1: Begrüßung der Mitglieder, Feststellung der Beschlussfähigkeit
               und Festlegung der Protokollführung\
TOP 2: Genehmigung des Protokolls der Mitgliederversammlung
	vom 13. April 2019 in  Essen\
TOP 3: Anträge zur Änderung/Erweiterung der Tagesordnung\
TOP 4: Bericht und Ausblick des 1. Vorsitzenden mit Aussprache\
TOP 5: Kassenbericht für die Geschäftsjahre 2019/2020\
TOP 6: Berichte der Kassenprüfer\
TOP 7: Entlastung des Vorstands für die Geschäftsjahre 2019/2020\
TOP 8: Berichte von Respondeo, vom Eugen Rosenstock-Huessy Fund
               und der Society und von Projekten einzelner Mitglieder\
TOP 9: Wahl eines Wahlleiters/einer Wahlleiterin für die Neuwahl des Vorstands\
TOP 10: Wahl des/der 1. Vorsitzenden\
TOP 11: Wahl des/der stellvertretenden Vorsitzenden\
TOP 12: Wahl des dritten geschäftsführenden Vorstandsmitglieds\
TOP 13: Wahl zweier weiterer Vorstandsmitglieder\
TOP 14: Verschiedenes

### 5. Adressenänderungen

Bitte senden sie eine eventuelle Adressenänderung schriftlich oder per E-mail an Thomas Dreessen (s. u.), er führt die Adressenliste. Alle Mitglieder und Korrespondenten, die diesen Brief mit gewöhnlicher Post bekommen, möchten wir bitten, uns soweit vorhanden, ihre Email-Adresse mitzuteilen.
> *Thomas Dreessen*

### 6. Hinweis zum Postversand

Der Rundbrief der Eugen Rosenstock-Huessy Gesellschaft wird als Postsendung nur an Mitglieder verschickt. Nicht-Mitglieder erhalten den Rundbrief gegen Erstattung der Druck- und Versandkosten in Höhe von € 20 p.a. Den Postversand betreut Andreas Schreck. Der Versand per e-Mail bleibt unberührt.
> *Andreas Schreck*

### 7. Mitgliederbeitrag 2021

Die „Selbstzahler” unter den Mitgliedern werden gebeten, den Jahresbeitrag in Höhe von 40 Euro (Einzelpersonen) auf das Konto Nr. 6430029 bei Sparkasse Bielefeld (BLZ 48050161) zu überweisen. IBAN-Kontonummer: DE43 4805 0161 0006 4300 29; SWIFT-BIC: SPBIDE3BXXX

Die Lastschrift-Abbuchungen des Beiträge 2020 und 2021 erfolgen in Kürze. Soweit Sie kein Mandat erteilt haben, nehmen Sie bitte die Überweisung vor, soweit noch nicht geschehen.
> *Andreas Schreck*

[zum Seitenbeginn](#top)
