---
title: Jahrestagung 2014
created: 2014
category: jahrestagung
thema: Die Auferstehungspredigten Eugen Rosenstock-Huessy's von 1957 und 1960
---
![Imshausen]({{ 'assets/images/Imshausen.jpg' | relative_url }}){:.img-right.img-large}
### {{ page.thema }}

Datum: 31.10.-2.11.2014
Ort: Stiftung Adam von Trott, Bebra-Imshausen

---

Programm Jahrestagung 2014

| Freitag, 31. Oktober 2014 | 18 Uhr    | Empfang und Abendessen |
|                           | 19.30 Uhr | Begrüßung und Wort der "Paten" zu den ersten 4 Kapiteln der (ersten) Predigt von Eugen Rosenstock-Huessy
|                           | 21 Uhr    | gemeinsame Lektüre der ersten Predigt
| Samstag, 1. November      |  8.30 Uhr | Frühstück |
|                           |  9.30 Uhr | Wort der Paten zu den Kapiteln 5 bis 8 |
|                           | 11 Uhr    | Pause |
|                           | 11.15 Uhr | Wort der Paten zu den Kapiteln 9 bis 12 |
|                           | 12.30 Uhr | Mittagessen und Pause |
|                           | 15 Uhr    | gemeinsame Lektüre der zweiten Predigt |
|                           | 16 Uhr    | Mitgliederversammlung |
|                           | 18 Uhr    | Abendessen |
|                           | 19.30 Uhr | Wort der Paten zu den Kapiteln 13 bis 16 |
|                           | 21 Uhr    | Austausch über das "Revolutionäre" dieser Auffassung der Auferstehung |
| Sonntag, 2. November      |  7.30 Uhr | Andacht mit der Allerseelenrede aus dem Sprachbuch |
|                           |  8.30 Uhr | Frühstück |
|                           |  9.30 Uhr | Was sagen die Predigten für das Leben politischer Institutionen (z.B. der ERH-Gesellschaft) ? |
|                           | 11 Uhr    | Pause |
|                           | 11.15 Uhr | Was muten uns die Predigten zu? |
|                           | 12.30 Uhr | Mittagessen und Ende der Tagung |

Anmeldung bitte an Andreas Schreck, Tel. 0551-28047871; schreckschrauber :at: web.de
