---
title: "Wilmy Verhage: Tijdbesef – Een inleiding tot het project «Weitersager Mensch»"
category: weitersager
order: 6
---
Een inleiding tot het project Weitersager Mensch

Amsterdam, 26 juli – 3 augustus 2006

Lieve Evelien,

Je vroeg waar ik de kracht en de inspiratie vandaan haal, als het niet bij het boeddhisme, niet bij drank, noch bij werk of mannen vandaan komt…

Nou laat ik eerst maar je gerust stellen: ik kan niet zonder mensen, hoor. Van het boeddhisme heb ik geleerd me op zijn tijd passief doch open op te stellen; bij tijd en wijle drink ik graag een glaasje of steek een pijpje op. Werken en geld verdienen zijn bij mij niet onmiddellijk synoniemen en ik kan nog steeds verliefd worden.\
Maar mijn geheime wapen heet: Eugen Rosenstock-Huessy.

1

Waar hij mij opmerkzaam op heeft gemaakt, is dat de westerse religie, het christendom, door de theologen (katholiek/protestant) in een ruimtelijk begrippenapparaat is opgesloten. Ontdaan van elk tijdsaspect.

Sinds Thomas van Aquino (1224-1274) dus bijna achthonderd (!) jaar geleden, is de kerk hoe langer hoe ruimtelijker gaan denken. Dat zie je bijvoorbeeld aan de theologisch gemunte tegenstelling natuur/bovennatuur.

De natuur staat dan voor het zichtbare, tastbare en de bovennatuur voor onzichtbare verschijnselen. Let op de negatieve waarde van ‘onzichtbaar’, ‘ontastbaar’.
Rosenstock-Huessy heeft me laten zien dat het in feite gaat het over tijdsver-schijnselen, processen, concreet.

Aldoende is in de religieuze perceptie zoiets als een ‘hierbovenmaals’ gecreëerd, terwijl we toch ‘hiernamaals’ zeggen. Want het gaat over de periode, het tijdperk, dat aanbreekt na een dood.

Of dat nu de dood van een mens betreft, of een deel van jezelf, een politiek bestel, een vriendschap, een huwelijk, een bedrijf, de industrie, een gedachte of wat er nog verder allemaal dood kan gaan, omdat het leeft, omdat het belichaamd wordt.

Het grote voorbeeld van ‘ervoor’ en ‘erna’ is de dood van Jezus Christus. Voor Jezus leefde, kon Christus nooit de maatstaf zijn, want hij was er nog niet, hij werd alleen nog maar verwacht. (Nou ja, alleen nog maar: de Joden hebben generatie na generatie op hem gehoopt). Maar na zijn dood kon hij de maatstaf worden. En inderdaad is de wereld sindsdien, en voorzover, generatie na generatie, getransformeerd, een heilsgeschiedenis in wording.
We staan nu op het punt dat we met ons allen op een planeet leven, en dat we één samenleving moeten zien te worden, waarin iedereen kan functioneren zoals God hem of haar heeft bedoeld. Maar zoals iedereen dagelijks kan meemaken, is het nog niet zover.

De tijd is sinds Thomas van Aquino langzamerhand uit het denken over God verwijderd. En daarmee werden de woorden, waarmee tijd en tijdverschijnselen worden benoemd, gedegradeerd tot onbelangrijk.

De zo onbewust gemaakte en dus verzwakte tijd, (gaan taal en bewustzijn niet altijd samen?) is vervolgens sinds Copernicus (1473- 1543), dus al vijfhonderd (!) jaar, in astronomische vaarwateren terechtgekomen en wordt door denkende medemensen, zoals onze universiteiten en academies die nog continu afeveren, eigenlijk alleen serieus genomen in haar hoedanigheid van 4e dimensie van de ruimte: het aspect van de ruimte dat beweging uitdrukt.

2

De werkelijke dimensies van de tijd worden gevormd door het levende heden; wat tot ons is gekomen uit het verleden, ten goede en ten kwade; en dat wat op ons afkomt vanuit de toekomst: Een dreigende onweersbui, hoe om te gaan met de Jihad-opvattingen van sommige Islamitische medemensen, een nieuwe liefdeom enkele voorbeelden te geven van wat ik bedoel.
In deze volgorde. Deze dimensies, die van werkelijke ervaringen van mensen uitgaan, zijn hopeloos geknecht door de horlogetijd.

De astronomische klok is een ingenieus gebruiksartikel om mechanisch te plannen, gedacht vanuit een soort lineaire lijn: verleden voert automatisch tot het heden tot de toekomst.
Helaas heeft dit concept de oppermacht gekregen over de tijd, zoals die door iedereen beleefd wordt in zijn hele rijke verscheidenheid vanuit een eeuwig nu.

De tijd werd hand over hand, onmerkbaar langzaam, tenslotte alleen nog maar, beschouwd als een universeel toepasbaar en meetbaar verschijnsel dat kon worden onderworpen aan onze wil.
Als het over dode materie en materiaal gaat, dat wordt gebruikt ten dienste van het leven, zijn kookwekker en horloge natuurlijk een zegen, een enorme vooruitgang, de kern van het vooruitgangsdenken, die vrucht van De Verlichting. We hebben het nog nooit zo goed gehad qua comfort: elke arbeider zijn eigen kasteel. En Adam (Eva ook trouwens) verlost van het ‘zweet zijns aanschijns’, waarin hij (en zij ook) moeten werken.

Dus dat is het probleem niet. Dat is de triomf van vijf eeuwen natuurwetenschappen, sinds Leonardo da Vinci (1452-1519) en Copernicus. Die hebben met hun astronomische kijk van onze planeet één wereld gemaakt. Hun laatste cadeau aan ons mensen is toch wel Internet.
Maar het vigerende denken dat eruit is voortgekomen, doet ons geloven dat wij leven als nietige stofjes in een onmetelijk heelal tussen onheugelijke tijden, terwijl wij tegelijkertijd als subject de maat aller dingen zijn, die al deze objectieve nietigheid heeft leren denken.
Het probleem zit hem dus in het feit dat ook het leven werd onderworpen aan het technoritme van het polshorloge.

Zelfs vrouwen in hun huishouding kregen een ijzeren formule, met maandag wasdag en woensdag gehaktballen. En een uniforme huishoudschool, die alle ervaringsleren tussen moeders en dochters overbodig moest maken, om ons ook in de vaart der volkeren op te stoten. Als baby's werden wij al gedisciplineerd door voedingstijden op de klok, en niet op de honger.

3

De werkelijke tegenstelling ligt niet tussen natuur en bovennatuur, maar tussen spraak, taal en natuur, zegt Rosenstock-Huessy. Hij zet de spraak voorop. En hij baseert zich op het evangelie van Johannes 1, vers 1: „In den beginne was het Woord”. (”Sinds het begin is er het spreken”, staat in de Naardense Bijbel). Hij was een tijd- en spraakdenker.

Als het wetenschappelijke onderwerp ons mensen, de samenleving betreft, baseert hij zich niet meer op het getal, de natuurwetenschappelijke grond, maar op de grammatica, dat hij aldoende op een hoger plan tilt.

Onze grammatica is door een Griek uit Alexandrië, ik ben zijn naam kwijt, 250 jaar v Chr. uit het spraakgebruik van zijn tijd gedistilleerd. Van hem leert nog steeds elke scholier het rijtje: ik, jij, hij, zij, etc. om werkwoorden te vervoegen en goed te leren spellen, op een volstrekt onverschillige, gelijkschakelende manier.

Maar in werkelijkheid bestaan er in de beleving van ik, van jou en van hij/zij/het oceanen van verschil. En daar zet Rosenstock-Huessy aan. Daarop baseert hij zijn meta-grammatica.
Vanuit de paradox: elk mens is gelijk, en toch is iedereen verschillend.

Hij zegt ook buitengewoon boeiende dingen over de ruimte. Hij toont aan dat ons ‘ruimtebegrip’: hoogte maal breedte maal lengte plus beweging, eigenlijk maar over één kant van de ruimte gaat, namelijk de buitenkant, en dan ook nog maar één aspect van de buitenkant.

Als je vanuit de beleving van mensen redeneert, sociologisch, gaat ruimte over binnen en buiten.
Gevoelens, gedachten, gesprekken, kringen waar je bij hoort zoals je familie, je vrienden, je land, je professie, daar gaan ‘binnens’ meervoud, over. Daar waar je niet bij hoort, of wat niet van jou is, daar gaan ‘buitens’ over. Buitens en binnens vormen je ‘hier’. Verleden en toekomst vormen je ‘nu’.

Samen vormen ze je heden, je hier en nu.

Ieders hier en nu heeft twee ruimtelijke en twee tijdsaspecten die door spraak tot stand komen en in stand worden gehouden (en door spraak vergaan). Elk mens, elk levend wezen, staat in een kruis der werkelijkheid, zoals Rosenstock-Huessy dat noemt. Alleen mensen vormen, doordat en voor zover zij spreken, aggregaten die telkens op andere wijze binnen en buiten, verleden en toekomst verbinden.

4

Zoals jij dat nu doet met je nieuwe huis. Je hebt een nieuw huis en je kan het naar je eigen hand zetten, waardoor je een nieuwe toekomst in gaat, die in ieder geval belooft, dat jij en je zoon er beter in kunnen leven. En dat alles, doordat jij indertijd een punt achter jullie huwelijk hebt gezet, waardoor je ex-man zich eindelijk eens achter zijn kop heeft moeten krabben wat er nou eigenlijk mis ging, en nu met een ander, beter antwoord komt. Zo simpel.
Zo werkt het, van het hart uit gezien, vanuit ieders leven gezien. Vanuit ervaring gedacht.

5

Met termen als bovennatuur hebben de theologen God onbereikbaar boven ons geplaatst, en de deïst Voltaire heeft God ook nog eens gedefnieerd als het beginpunt van ons bestaan, dus in het verre, verre verleden: the Big Bang als het ware. Met ons leven had het verder niks meer van doen: God bestond lang geleden of bevindt zich onbereikbaar ver boven ons, of wordt door de theologen tot onderwerp gedegradeerd of Hij/Zij bevindt zich in een onbereikbare toekomst. Geen wonder dat God enkele generaties geleden werd doodverklaard.

Daardoor ben ik zelf heel lang agnost geworden, want mijn religieus gevoel werd niet gevoed, kreeg geen taal.

Inmiddels weet ik en merk ik (en dat laatste is belangrijker) dat God door ons heen werkt, want Hij/Zij is de God van dood en leven. God vind je niet door Hem/Haar te defniëren, tot onderwerp te maken. God vind je in de liefde die je ervaart: ontvangend of gevend, in wat je in beweging zet, in de daden, in jezelf, in de interacties, in de krachten die je dragen. In de geloofde taal.

En de duivel? In leugens, in namaak, in onechtheid.

6

Het grote geschenk van God aan ons mensen is de taal. Het vermogen elkaar te benoemen, te verstaan, te geloven, te kritiseren, gaat via spraak. Taal.

Eerst komt de taal, dan pas het denken. Er moet eerst iets gebeurd zijn, wil je er überhaupt over na kunnen denken. Als baby krijg je een naam, ben je een jij, wordt je aangesproken, jaren voordat je ‘ik’ kan zeggen.

Taal is geen ruimtelijk verschijnsel. Het is niet vast te pakken, maar het werkt. In de tijd. In de tijden.

Daar vind je God. God, het Subject der subjecten, de Ik die alle ikken omsluit, bron van vertrouwen. In de tijd vind je de mens, de wederzijdsheid van elk jij, tussen wie de liefde kan regeren. En daar vind je de wereld, het het, elk ‘het’ een kans op objectieve waarheid.
Jij, ik, hetwe zijn het allemaal, zoals iedereen ook een wij, een jullie of een zij vormen, al naar gelang van het perspectief van het moment. Wij zijn elkanders jij in elk contact. En elk contact maakt ons meer tot het specifeke ‘wij’ dat jij en ik vormen, al naar gelang onze avonturen samen. Maar wij zijn samen een ‘het’ zodra iets over ons allebei wordt gezegd: dan zijn we het onderwerp.

Zoals jij dat deed in jouw brief bijvoorbeeld, toen je onze vriendschap onder de loep nam.
Liefde, vertrouwen, waarheid, trouw: jij, ik, het, wij. De hogere grammatica van Rosenstock-Huessy. Als je het ‘jij’, de ‘ik’, het ‘het’, het ‘wij’ in jezelf en in anderen leert kennen, leert articuleren, leert herkennen, in al zijn concreetheid, dan weet je te allen tijde waar jij staat, waar God staat en waar de wereld zich bevindt.
En dan klopt je perspectief en kun je onbevangen leven.

7

De kijk van Rosenstock-Huessy is dus een totaal andere dan ik in ieder geval tot nu toe ben tegengekomen. Bij mij valt alles op zijn plaats door wat hij schreef.\
Nog steeds.\
Hij is al heel lang in mijn leven.

Eerst door mijn tante Ko, een vriendin van zowel mijn moeder als mijn vader, hun huisvriendin, die door zijn zienswijze gegrepen werd, zodra ze ermee in contact kwam, en al doende in ons gezin heeft geïntroduceerd. Ergens begin jaren 60. Zij was in mijn leven de enige volwassene die grote woorden gebruikte, waarvan ik voelde dat ze wist waarover ze het had. In mijn leven als jong volwassene, toen ik de grootste moeite had mijn balans te vinden in de grote boze wereld der volwassenheid, had ze een priesterlijke functie in mijn leven: De paar keer dat ik echt niet meer wist hoe ik verder moest, kon ik altijd bij haar terecht en haar adviezen sneden hout. Hebben me gevormd.

Tussen mijn dertigste en vijftigste begon ik hem zelf te lezen: Twee boeken; Speech and Reality, zijn meest wetenschappelijke werk en Fruit of Lips dat over de vier evangelies gaat. Over deze twee boeken heb ik 20 jaar gedaan. Lezen, herlezen, langzaam tot mij door laten dringen wat hij eigenlijk zegt.

Spraak en werkelijkheid werd mijn sparring partner toen ik Andragologie studeerde en daarna toen ik in de volwasseneneducatie werkte. Ik heb heel veel aan het boek gehad, het sloeg veel beter op de sociale realiteiten waarmee ik in mijn werk te maken kreeg dan de sociologische en psychologische theorieën van de universiteit.

Vrucht der lippen was al die jaren het finterdunne lijntje dat mij aan het christendom bond.
Ik was helemaal niet van zins zijn denken over te nemen, tenzij het me door zijn werking overtuigde. Dociele gehoorzaamheid is nooit mijn sterkste punt geweest. En naar de protestante versie van het christendom met zijn fatsoensnormen voor vrouwen verlangde ik al helemaal niet terug. Vanuit dat denken gezien, verliep mijn leven sowieso hopeloos zondig.

Maar toen ik in mijn burn-out geraakte moest ik toegeven dat Rosenstock- Huessy had gewonnen van het vigerende academische denken over mensen en dat ik om hem verder te kunnen volgen dus ook weer christen moest worden.

(En dat ik mijn duits moest bijspijkeren, want hij schreef in het duits en in het engels, hoewel enkele boeken van hem in het nederlands zijn vertaald.)

Dus van toen af aan, nu 10 jaar geleden, ben ik hem gaan lezen en lezen, duizenden pagina’s, telkens opnieuw, zijn visie langzaam in mij opnemend, nog steeds.

Alles wat ik maar te pakken kon krijgen: de Engelse en de Duitse versie van zijn Boek over revoluties: Out of Revolution en Die Europäischen Revolutionen: hoe de Westerse wereld uit revoluties is ontstaan. Zijn Soziologie „Im Kreuz der Wirklichkeit”, twee banden. Het grote werk Die Sprache, ook twee banden. The Origin of Speech, I am an impure thinker. De tweeling Atem des Geistes en Heilkraft und Wahrheit. Die Tochter, waarin ook het Boek Ruth, in de bijbelvertaling van Martin Buber en Franz Rosenzweig. Das Alter der Kirche, dat hij samen met Joseph Wittig heeft geschreven. Des Christen Zukunft, Die Gesetze der christlichen Zeitrechnung, Werkstattaussiedlung, Der Unbezahlbare Mensch. Dienst auf dem Planeten.

8

Toen mijn man Peter stierf werd voor mij de tijd rijp contact te zoeken met anderen, die zich met hem bezighielden. Het heeft nog drie jaar geduurd voordat ik daarvoor de kracht en moed kon opbrengen.

En nu zit ik dus in het bestuur van een van de gezelschappen die rond hem zijn ontstaan.
Maar terug naar de offciële kerken kan ik niet meer. Hoewel ik de protestanten dankbaar ben voor wat ze tot stand hebben gebracht, kan ik er niet naar terug. Ook niet naar de katholieke kerk.

Alleen de Joodse injectie die Rosenstock-Huessy het christendom toedient maakt het voor mij de helende weg, die het belooft te zijn.

9

Hij leefde van 1888-1973. Hij was de buitengewoon getalenteerde zoon van geassimileerde duitse joden uit de bourgeoisie in Berlijn. Als jonge man overgegaan tot het christendom. Toen de Eerste Wereldoorlog uitbrak was hij, 26 jaar oud, al professor in de rechtsgeschiedenis. Hij moest onder de wapenen en toen is hem aan het front in Noord-Frankrijk horen en zien vergaan. Voor zijn ogen ging de wereld tot dan toe onder. Wat de wereld (en hem) toen is overkomen heeft zijn denken bepaald.

In 1933 na Hitlers machtsovername, is hij vertrokken naar Amerika. Daar heeft hij eerst aan Harvard gedoceerd en later aan de universiteit van Norwich.
Dus. Ik hoop dat je hier een touw aan kunt vastknopen. Anders hoor ik het wel, toch? Ik praat niet zo makkelijk over hem en zijn werk, ik reageer meer vanuit.

Enfn.

Benieuwd naar je reactie,

Liefs,

Wilmy
