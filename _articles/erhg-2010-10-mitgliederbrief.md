---
title: Mitgliederbrief 2010-10
category: rundbrief
created: 2010-10
summary:
zitat: |
  >Das volle Wort, bei dem wir einander ansehen, macht aus kauen begreifen, aus atmen inspiriert sein, aus hören vernehmen und Vernunft, aus riechen spüren, aus schmecken Sinn und sinnen, aus fühlen empfinden.
  >*Eugen Rosenstock-Huessy, Soziologie II, Die Umkehr des Worts, s.563*
---

## Eugen Rosenstock-Huessy Gesellschaft e.V.

{{ page.zitat }}

**Vorstand/board/bestuur:**\
*Dr. Eckart Wilkens (Vorsitzender);*\
*Wilmy Verhage; Andreas Schreck; Dr. Jürgen Müller; Thomas Dreessen*\
*Antwortadresse: Dr. Eckart Wilkens: Everhardstraße 77, 50823 Köln, Deutschland*\
*Tel: 0(049) 221 511265*

***Brief an die Mitglieder Oktober 2010***

**Inhalt**

{{ page.summary }}


[zum Seitenbeginn](#top)
