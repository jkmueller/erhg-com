---
title: "Bas Leenman: Im Rosenstock-Huessy-Huis in Haarlem"
created: 2021
category: veroeff-elem
published: 2021-12-18
---
### Stimmstein 2, 1988
#### Ins Rosenstock-Huessy-Huis in Haarlem

Die folgenden Beiträge haben wir mit Absicht in ihrer holländischen Sprache stehen lassen. Das Rosenstock-Huessy-Huis in Haarlem, Holland, ist vielen Lesern bekannt. Auf unser Bitte erzählen hier einige der Pioniere, wie sie damals zu diesem Schritt gekommen sind und wie sie diese Wendung in ihrem Leben empfunden haben. Piet Blankevoort, Elly Harttman, Lien Leenman, Elias Voet und Wim Leenman schreiben hier von dem Sprung, den sie taten. Wim Leenmans Beitrag ist im Deutschen geschrieben. Umso besser konnten die anderen Erzähler die Ursprünglichkeit ihrer Landessprache beibehalten.
>*Bas Leenman*
