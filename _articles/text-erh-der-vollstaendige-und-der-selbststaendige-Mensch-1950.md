---
title: "Rosenstock-Huessy: Der vollständige und der selbständige Mensch (1950)"
category: online-text
published: 2023-09-18
org-publ: 1950
---
Die Erschaffung der Handlungen vom Tode her haben uns alle vervollständigt. Denn sie haben mit dem Kapital ihres Wissens um den Tod in uns gewuchert. In jedes Leben hinein, in dem der Tod noch bevorsteht wirkt das Sterbenmüssen bereits auf den Wegen, die Abraham, Lao-tzu, Buddha, Jesus dafür gefunden haben, als Autorität, als Incognito, als Solidarität und als Bestimmung.

Der Mensch ist seit der Kreuzigung Christi im vollständigen Besitze seines Selbst. Er ist damit aus einem selbständigen Wesen zu seinem vollständigen Wesen umgewandelt worden. Alle Menschen verhalten sich seit dem als Mittelwerte irgendwo auf dem Wege zwischen selbständigem und vollständigem Menschen.

Um das zu seigen, gehe ich zunächst zurück zu den Stiftern. Jesus kam als der letzte, als der Sohn nach allen
Söhnen, aber auch nach Abraham, Laotzu, Buddha. Er war daher nicht nur der, den das letzte Kapitel gezeigt hat, er war ausserdem der Letzte von vier Stiftern. / Dies aber ist eine Eigenschaft, die den -2- drei anderen abging. Sie waren sie selber. Aber indem Jesus sie alle vollständig machte, war er nicht der Vierte, nicht nur der Letzte, sondern
war auch aller Zurechtrücker und Deuter. Das Christentum hat daher aus der Sympathie Buddhas, der Vaterschaft Abrahams, dem geräuschlosen Laotze dadurch neues geschaffen, dass sie nun in ihm zusammenstanden. Aus Buddhas
Mitleid wurde die Solidarität, und aus Laotzus Auslöschen das Christliche Incognito. Denn wenn der Sohn seiner Bestimmung folgt, dann bleibt das Erlöschen nicht ewig und das Mitleid bleibt nicht fruchtlos. Dem Vater Abraham bleibt sein Glaubensplatz im Christentum erhalten, aber er breitet sich aus in den der Autorität, weil jedes Amt
nicht nur das Väterliche, sondern auch Lehre, Recht, Kunst, Medizin, in die Spannung zwischen Vater und Sohn gerissen wird. Wo der Sohn nicht mehr rebelliert, sondern um den Preis seines eigenen Lebens die Bestimmung des
Menschengeschlechts dem Vater abringt, da werden alle Könige, Heerführer, Zünfte, Künste in den Fortschritt zwischen Autorität und Bestimmung hineingerissen. / Wo also -3- zuerst die Namen stehen, Abraham, Laotzu, Buddha, Jesus, da treten nun, aber von ihnen abgeleitet.

![Kreuz]({{ 'assets/images/Autoritaet-Incognito-Solidaritaet-Bestimmung.jpg' | relative_url }}){:.img-left.img-xxlarge}

- Autorität rückwärts
- Incognito innen
- Solidarität aussen
- Bestimmung vorwärts

unter ein Kreuz.

Dass der Alte Bund im Neuen Bund bewahrt worden ist, davon zeugt ja die Bibel, die altes und neues Testament enthält. Aber das gesamte asketische Leben der Christenheit ist ohne die nackten Weisen Indiens, ist ohne den Buddhismus nicht denkbar. Und ebenso ist der Christ im Tanz, diese uralte christliche Vision, aus Chinas Einwirkung erklärlich. Ebenso sind bereits 500 n.Chr. wichtige christliche Elemente des Christentums in Buddhismus und Taoismus eingdrungen. Unsere Art diese Mächte bloß auseinander zu setzen, muss revidiert werden. Christliche Askese und die
Erlösung des wiederkehrenden Buddha stammen aus einer Einheit, trotz der Religionsgeschichte, die geschrieben wird.

Christus hat also seine Sohnschaft / auf die vorher -4- geschaffenen Todeshandlungen Autorität, Inkognito und Solidarität aufgestockt und deshalb heisst er der homo perfectus, der vollständige Mensch.

Stellen wir nun den vollständigen Menschen gegen den selbständigen Menschen. Der selbständige Mensch wäre entweder Weib oder Mann, jung oder alt. In Geschlecht und Lebensalter gespalten, weiss jeder von uns an seinem Teil, dass er unselbständig ist. Kinder brauchen Eltern. Männer bedürfen der Weiber. Die sogenannte Selbständigkeit ist also die Folge einer Bewusstseinsverengung: das Selbstbewusstsein verneint das, was wir alle wissen, unsere Unselbständigkeit.
"Das" Selbst, die seltsame Bezeichnung des trotzigen Menschen will zuerst von sich selber wissen. Dies ist eine sekundäre Haltung. Zuerst weiss ich von denen, die mich anblicken und ansprechen. Erst hinterher weiss ich von mir selber. Die falsche Reihenfolge aber: erst ich selber, dann die da draussen, enthüllt das Selbstbewusstsein als einen Akt der Vertrotzung, der Verselbständigung. Um aus unserer Abhängigkeit herauszutreten, um selbständig / zu werden, -5- bedürfen wir des Selbstbewusstseins. Die vollständigen Menschen ihrerseits überwinden das falsche, das zu weit getriebene Selbstbewusstsein in uns. Sie verleihen uns den Platz im vollständigen Menschengeschlecht, den wir als Kind unserer Eltern, in den kleinen Zellen der Familie oder der Schule oder der Heimat vorübergehend zu Gunsten unseres Selbstbewusstseins aufgeben mussten. Das Selbstbewusstsein entwurzelt. Der Intellektuelle ist heimatlos. Selbständige Existenzen, selbständige Völker, selbständige Berufe - das alles sind entfesselte Energieen. Vollständig ist keine selbständige Existenz, keine deutsche Nation, kein auserwähltes Volk, kein deutscher Geist, kein Mekka der
Zivilisation.

Wie wirkt, denn der vollständige Mensch? Nun, der vollständige Mensch hat die selbständigen Eigenarten der Geschlechterwesen und der Lebensalter gegeneinander geöffnet. Abraham hat den Neinsagenden, herrscherlichen, zum
Sohnesopfer ermächtigten Hausherrn in den Vater umge/wandelt. Dem Despoten, der über -6- Leib und Leben seiner Hausgenossen verfügt, macht der Vater die Qualität zugänglich, die den im Raum der Gegenwart tatigen Männern die Zeitachse von Mutter und Kind überimpft. Vater und Sohn waren bloss Gegner wie Herr Knecht, sähe nicht jeder Vater
in seinem Sohn einen heraufkommenden Vater. Dieser Blick der Zukunftserwartung wandelt den Naturherrn zum Vater, und er macht aus dem Gott Baal dem der Erstgeborene zum Opfer gebracht werde, den Gott Abrahams, Isaaks und Jakobs, wo dann jede Generation unmittelbar zu Gott wird. Es mag nützlich für den Leser sein, wenn ich hier eine Anekdote
einflechte. Als ich den Rankeschen Satz: Jede Generation ist gleich unmittelbar zu Gott, in USA anführte, da
schrieb ein Professor der Literatur: das sei unvermischter Blödsinn. Nicht nur erkannte er nicht das Ranke-zitat, sondern so sehr ist dem modernen Menschen anscheinend die Möglichkeit abhanden gekommen, sich in die Zeiten vor
Abraham zurück zu versetzen, dass dieser Amerikaner nur lachte, und dass es der Sippenhaft in Deutschland bedurft hat, um die vorjüdische Natur des Despoten neu be/kannt zu machen. -7-

So ist bei Abraham die Zeitenblindheit des Geschlechtswesens "Mann" entsiegelt. Denn der Mann versieht sich in den Raum und vermisst der Zeitenfolge Zusammenhanges. Bei Jesus ist umgekehrt die bräutliche Seele, die dem Geliebten zujauchzt, nimm mich hin, statt in die Brautkammer des Geliebten in die kalte Finsternis der Schädelstätte, des öden Raums der Welt, eingegangen. Jesus hat die Heimat- und Nestabhängigkeit der Weiber entsiegelt, indem er sich statt in sein Haus, Heim, Volk, in die Welt, den wirklichen gottverlassenen Kosmos hinein wagte und in ihn hinein seine Seele aushauchte.

Die Geräuschlosigkeit Laotzus östlichen kerbte den / -8- Eingenerationsgläubigen, den einaltrigen Mann dadurch in die Zeit ein, indem sie von seinem töchterlichen Schweigen die Heilung des vorher und des nachher erwarten, als Pause in der Melodie, zu der Laotzus Schweigen wird, stellt sich der Taoisten in die Zeitenfolge erst recht hinein. Und in Buddha wird Mutter India für den einen Augenblick des grossen Auges sogar da wahr, wo die Leiber erbarmungslos gegeneinander stehen, auch im Töten und Gefressenwerden erkennt der Asket die Geburtswehen des mütterlich umfassenden Schosses.

Das ungeheuer befremdende an dem Einfluss dieser vier Todesstifter ist ihre Wirkung auf Jahrtausende. Ohne  Herrschaft, ohne Armee. ohne Land, haben sie sich durchgesetzt. Dies ist ein Wunder für die Kausaldenker. Denn die / Ursachen, die vier -9- bescheiden, ärmlich verlassen gelebten Leben, Jesus, Abraham, Buddha, Laotzu können die Wirkungen niemals erklären. Der Hass der Kausalgelehrten ist denn auch gross. Die Griechen, die Akademiker, die Naturwissenschaften wollen diese Rückwirkung nie anerkennen. Antisemiten, Christushasser, Marcioniten, Welträtselhäckels, Kirchenfeinde und Huxleyaffen werden uns immer die Millionenwirkung der vier Todesmänner lächerlich machen. Sie müssen das. Sie sind ja die Wächter des Selbstbewusstseins. Und der selbständige Mensch will
nicht hören, weshalb vollständige Menschen die Waffen der selbständigen Menschen, der Griechen und graeculi, der Professoren und Athleten nicht führen. Woher also rührt ihre Vollmacht?

Offenbar wird unsere Art garnicht am leichtesten durch die Macht geprägt, die 100.000 Polizisten oder 100 Düsenflugzeuge sendet. / Es gibt heute noch Juden, Christen, Taoisten, -10- Buddhisten aber keines der Reiche
ihrer Zeit oder späterer Zeiten, in denen sie verfolgt wurden. Was soll das heissen?

In dem Spielteil dieses Buches liegt wohl die Erklärung bereit. Obwohl es ernst ist mit unserem Geschlecht und Lebensalter, mit unserer Muttersprache und unserem Selbstbewusstsein ist es doch gegen den blutigen Ernst ein Kraut gewachsen. Wir spielen nämlich mit grosser Feierlichkeit. Feierlichkeit nun scheint dem Ernst überlegen zu sein! Feiertage sind Werktage überlegen. Der wirkliche Mensch wird nicht an seiner Arbeit, seinen Kriegen, seinen
Krankheiten, seinen Schulen geformt. Er feiert. Und wie die Olympiade im Augenblick für den Weltsport den Massstab hergibt, weil sie den / höchsten (?) Sport-Feiertag -11- verkörpert, so scheint ein Feiertag die Züge des schön menschlichen Antlitzes endgültiger zu bestimmen, als die Zwangsarbeit.

Die Sehnsucht des Geschöpfs nach seiner Bestimmung muss also wohl die Einbildungskraft unendlich befeuern, wenn Feiern den Ernst der Spezialisierung überbieten können. 'Religion makes faces'. In Mesopotamien haben die
"rassisch" ungeschiedenen Mitgliedern der verschiedenen Religionen verschiedene Körper, verschiedene Physiognomik.
Das ist nur ein Beispiel für das Rätsel der menschlichen Gesichts und Körperbildung aus ihrer Bestimmung. Die Bestimmung ist in uns Menschen wirksamer als die Ursachen. Dies entspricht unserer Plastizität: Das ist keine frohe Botschaft für die Anbeter der Ursäftewirkungslehren, diese Klempner der Wirklichkeit. Jeder Naturforscher aber wird /
-12- durch die Aufdeckung dieses Gesetzes als ein freies Wunder bestätigt. Ihn setzt ja nicht der Kausalgrund, sondern seine Sehnsucht in stand, die nächste Entdeckung zu machen. Und alle Freiheit des Menschen hängt also an der Tatsache, dass er unter den verursachten Dingen das Wunder verkörpert, wie wir um unserer Bestimmung willen den blossen tierischen Ernst in strenger Feier überwinden können.

Abhängig, selbständig, vollständig haben wir den Menschen gefunden, das spielerische Element seiner Abhängigkeit reisst ihn über den Ernst seiner Selbst empor in die Freiheit seiner Feiertage. Und deshalb haben die Stifter seiner Feiertage sich nie unter dem Ernst blosser Herrscher, Meister, Fürsten, Bankiers, Kaiser, Fabrikanten,
Diktatoren, Kommissare, Generäle zu beugen gehabt. /

Der Grieche in uns und der Heide, der selbständige Mensch, ist eine vorübergehende Erscheinung: Diesen vorübergehenden selbstbewussten Geistern verdanken wir die Macht des Augenblicks. Aber die Vollmacht der
Vollständigen Menschen wohnt ihnen nie inne. Die fremden Eroberer kommen und gehen. Wir gehorchen aber wir bleiben stehen, singen die abhängigen Menschen des Chores in der Braut von Messina. So aber spricht auch die am vollständigen
Menschen teilhabende einzelne Seele.

Nun haben wir an ihm alle teil dank der plastischen Einbildungskraft des seiner Bestimmung harrenden Geschöpfes
Mensch. Nur unser Ernst, unser Trotz, unsre Gier, unsere Angst, kurz unser Selbstbewusstsein entzieht sich den Wirkungen der feierlichen Einreihung in unsere Bestimmung. Diese Entziehung gelingt dem Selbst nur teilweise. Jeder von uns bezahlt den Tribut an sein / Selbstbewusstsein in verschiedenem Grade. Seit dem Kommen Christi in unsere Rasse und Arten sind sie alle aufbildbar und kein selbstbewusstes Wort ist mehr eines Menschen letztes Wort. Sogar Hitler entleibte sich mit Eva Braun. Nicht viel menschlichen Zuge waren Hitler geblieben. Der wirkliche Antichrist aber hätte dieses Liebeserweises des von ihm abhängigen Weibes nicht bedurft.

Wir leben zwischen abhängigem und vollständigem Menschentum und wir spielen uns in unsere Feiern über unsern selbständigen Ernst hinaus.

----

Das Manuskript hat 14 S. wovon eine keine Seitenangabe erhalten hat. die S. vor S. 13. Dem Inhalt kann entnommen werden, dass es sich hier um einen Abschnitt eines Kapitels eines Buches handelt.

Da bietet sich an erster Stelle die Soziologie I an. Der Inhalt ist aufs Innigste verknüpft mit dem Dritten Teil 4.
Abschnitt: Die Todesüberwinder: Seele. Wir finden dort 4 Paragraphen: Unsere Selbständigkeit - Scham und Selbstüberwindung - Die Handlungen von Tode her - Die Bahnbrecher. In der Neuauflage von 1956, wie in den Druckfahnen von 1950 ist diese Abteilung mit einigen Zusätze und Abstriche der ersten Ausgabe gleich geblieben. Dass Manuskript muss nach 1945 geschrieben worden sein, weil Hitler und seinen Freitod reflektiert wird. Es trägt aber die
Nummer VI. - Meine Vermutung ist, dass es zusammen mit einem V. (nicht gefundenen) Teil einen neuen Abschluss bilden sollte, der dann doch wieder aufgegeben wurde. Weiter vermute ich, dass es um 1949/50 geschrieben ist.

Das Manuskript wurde Mai 1993 in Four Wells gefunden. Es wurde am 5. Juli 1993 transkribiert von \
*Lise van der Molen, Winsum, Niederlande.*

Der Seitenwechsel ist mit dem Zeichen: / angegeben. Die Zahl der neuen Seite ist am Ende der Zeile gesetzt, z.B. -10-
Schwer leserliche Worte sind mit (?) bezeichnet worden.

[„Der vollständige und der selbständige Mensch” als PDF-Scan](http://www.erhfund.org/wp-content/uploads/435.pdf)

[zum Seitenbeginn](#top)
