---
title: "Wolfgang Ullmann: Kulturelle Identität als politisches Problem in Osteuropa"
category: einblick
published: 2022-12-31
---
![Wolfgang Ullmann]({{ 'assets/images/Wolfgang-Ullmann.jpg' | relative_url }}){:.img-right.img-small}

Motto: *„Wer nur eine Sprache spricht, ist kein Mensch."*\
Ein südungarischer Bauer an den Verfasser

### These 1

***Kultur ist die Fähigkeit, Identität und Differenz durch Kommunikation zu überschreiten, Tradition auf Zukunft zu öffnen.***

Ein mir bekannter orthodoxer Theologe entstammt einer baltisch-deutschen Familie. Seine Kindheit und Jugend verlebte er im zaristischen Rußland, also im russischen Sprachmilieu. Die bolschewistische Revolution zwang ihn zur Emigration nach Frankreich. Hier begann er eine wissenschaftliche Karriere. Er schrieb und veröffentlichte seine wissenschaftlichen Werke in französischer Sprache. Der Einmarsch der Hitlerwehrmacht in Frankreich 1940 zwang ihn abermals zur Emigration, diesmal in die USA. Hier wirkt er jetzt als US-Bürger an einem englischsprachigen Seminar für orthodoxe Theologie, d. h. er lehrt und schreibt englisch.

Diese Polykulturalität eines individuellen Lebenslaufes ist für Mittelosteuropäer beinahe die Regel. Aber sie ist auch in einem bestimmten Sinn verallgemeinerungsfähig. Je intensiver wir uns mit einer kulturellen Identität befassen, um so mehr Differenz und Differenzen entdecken wir in ihr und an ihr selbst.

Man erinnere sich nur, wie Johnsons „Jahrestage" an den mecklenburgischen Familien Crespahl und Paepcke Landes- und Völkeridentitäten dichterisch identifiziert haben, die von Güstrow und Jerichow bis ins New York des Vietnamkriegs reichen. Nichts anderes tut Grass in seinem letzten großen Roman, wenn er Fontanes hugenottisch-deutsche Vita an der des Kulturbundfunktionärs Wuttke aus der EX-DDR spiegelt.

Offenkundig legen schon diese beiden Beispiele aus der jüngeren deutschen Literatur den Schluß nahe, daß Unterscheidungs- und Differenzierungsfähigkeit ein wesentlicher Bestandteil von Kultur sind. Aber die Feststellung einer Zusammengehörigkeit von Identität und Differenz bleibt solange trivial, wie wir nicht danach fragen, wie sie sich denn beide zueinander verhalten.

Die beiden Begriffe Identität und Differenz sagen darüber nichts. Wir müssen uns klarmachen, daß sie beide gleichsam gegeneinander stumm sind. Die Stufe des Nichttrivialen erreichen wir erst dort, wo wir erkennen, daß wir andere Kategorien brauchen, wenn wir das triviale Niveau des starren Nebeneinanders von Identität und Differenz hinter uns lassen wollen.

Es sind die Kategorien von Relation und Kommunikation, die wir aufgreifen müssen, wenn wir so etwas wie eine Definition von Kultur festlegen wollen, die beides, Identität und Differenz, miteinander verbindet. Sie könnte dann so lauten: *Unter Kultur sei die Fähigkeit verstanden, im Spannungsfeld von Identität und Differenz die Fähigkeit zu besitzen, Relation und Kommunikation dort herzustellen, wo kulturelle Differenzen zur Konfrontation von inkompatiblen und darum exklusiven Identitäten führen.*

### These 2

***Mit dem Versuch, Kultur auf Identität zu begründen, zerstört Kultur sich selbst. Das antike Wort „Barbar" und das von ihm abgeleitete „Barbarei" bezeichnen nichts anderes, als eine Identität, die, zur Kommunikation unfähig, durch Abgrenzung, Abwehr und Abschreckung sich selbst zu behaupten sucht.***

„Barbar" (griechisch „barbaros") als Bezeichnung des Kulturlosen oder gar Kulturunfähigen ist Lautmalerei, Nachahmung einer unverständlichen Sprache. Barbar ist demnach, wer nur seine eigne Sprache spricht, sich nur im Raum dieser einen sprachlichen Identität bewegen kann.

Aber muß diese Definition nicht auch auf die Griechen selbst angewandt werden, die sie aufgestellt und damit sich von nicht-hellenischer Kultur abgegrenzt haben? Homer und Herodot geben die Antwort auf diese Frage. Beide verdanken ihre immense Wirkung nicht zuletzt ihrer Fähigkeit, andere kulturelle Identitäten in der eigenen Sprache zu erfassen und zu beschreiben.

Solche Beschreibungen können entweder exklusiv oder inklusiv sein. Die exklusive läuft immer darauf hinaus, Kultur als eine von Nichtkultur sich abhebende und gegen sie abgrenzende Identität zu bestimmen. Klassisch für diese Exklusivität sind alle Religionen. Jede andere Religion wird in dieser Sicht zu einem inakzeptablen oder irrelevanten Außen, demgegenüber Abgrenzung die einzig mögliche Haltung sein kann.

Für die geschichtliche Umorientierung im postkommunistischen Europa dürfte es eine entscheidende Frage sein, ob solche aus religiöser oder kultureller Abgrenzung hervorgegangenen Trennlinien unser Bewußtsein nicht auch dort noch prägen und bestimmen, wo die religiösen bzw. kirchengeschichtlichen Ursachen längst weggefallen sind.
In welchem Ausmaß das gilt, wird überall dort offenbar, wo man über die Grenzen Europas debattiert, so wie es z. B. der Bergedorfer Gesprächskreis der Körber-Stiftung im Sommer 1995 getan hat (Europa - aber wo liegen seine Grenzen? hgg. von der Körber-Stiftung, Hamburg 1995).

Allein der polnische Historiker und Politiker Geremek äußert in diesem Gespräch ein Bewußtsein dafür, daß die Trennlinie zwischen Kirchen lateinischer und denen nichtlateinischer Tradition „zu den dauerhaftesten Zivilisationsgrenzen der Welt gehört" (aaD S. 9).

Diese Behauptung Geremeks ist nichts weniger als eine Übertreibung. Es ist einer der Eckpfeiler, die die wohl heute noch einflußreichste Universalgeschichte, Toynbees 12-bändiges „Study of History", tragen, daß die griechisch-slawischen, später größeren Teils türkisch beherrschten Völker, einer Europa fremden Kultur angehören. Toynbee, der lange Zeit außenpolitischer Berater der britischen Regierung gewesen ist, hat diese stets in dem Sinne zu beeinflussen gesucht, die kommunistisch beherrschten Völker nicht als einen Teil Europas zu verstehen, was z. B. in der britischen Griechenlandpolitik bis heute nachwirkende verheerende Folgen gezeitigt hat.

Die vielzitierte These Huntingtons vom unvermeidlichen Kampf der Kulturen ist insofern gar nichts Neues. Nicht nur, daß sie auf der oben erwähnten Trennlinie zwischen lateinischer und nichtlateinischer Christenheit aufbaut. Sie wandelt die alte Toynbee-Perspektive höchstens dergestalt ab, daß sie Islam - der freilich auch bei Toynbee zusammen mit der östlichen Orthodoxie als Inbegriff des Nichteuropäischen galt - und Konfuzianismus als Inbegriffe einer Europa bedrohenden feindlichen Gegenkultur definiert. Niemandem wird verborgen bleiben, daß in dieser Sichtweise das alte „Yalta-Szenarium" des Kalten Krieges wieder auf die politische Bühne tritt, lediglich dadurch modifiziert, daß das ehemals antikommunistische Vokabular ersetzt ist durch die Terminologie des Kulturenkampfes. Mittlerweile freilich kann alle Welt an der Katastrophe Bosniens sehen, wohin eine Politik der weltpolitischen Kulturabgenzung führt: In den Sieg der ethnizistischen Barbarei. Umgekehrt mußte für die Annahme der Protokolle von Dayton genau diese Konzeption der sich ausschließenden kulturellen Identitäten verlassen werden.

### These 3

***Europa bezeichnet weder einen geographisch klar abgrenzbaren Kontinent noch die Einheit einer bestimmten Kultur. Europa - das ist vielmehr ein Kontinent der Begegnung und ein Ort der Transformation von Kulturen***

Die Frage, wo die Grenzen Europas liegen, die der Bergedorfer Gesprächskreis im Warschauer Königsschloß debattiert hat, war falsch gestellt. Denn weder ist Europa eine geographisch abgrenzbare politische Einheit noch die einer etwa durch das Attribut „christlich" definierbaren einheitlichen Kultur.

Ägypten und der Sinai, Jerusalem und Konstantinopel sind ebensogut Voraussetzungen und Bestandteile europäischer Kultur wie Rom, Paris oder London. Zu ihr gehören die Ägypter Origenes und Athanasius, die Kappadozier Basilius, Gregor von Nazianz und Gregor von Nyssa wie der Nordafrikaner Augustinus oder der Muslim Averroes und die Juden Maimonides, Mendelssohn und Rosenzweig.

Die Frage, die sich faktisch und darum beantwortbar stellt, ist nicht die nach bestehenden oder neu festzulegenden Grenzen Europas, sondern die, ob Europa sich verstehen soll im Sinne einer durch Abgrenzung politisch, kulturell und religiös definierten Identität oder als völkerübergreifender Kontinent einer Kultur der Kommunikation.

Und wie schon die obigen Aufzählungen sagen: Europa ist sicherlich ein Plural von Kulturen, seit der Hellenismus Indien, Persien, Judäa und Ägypten im Kontext einer griechischsprachigen Kulturenkommunikation zusammenfaßte, die die religiösen Überlieferungen der Juden (das „Alte Testament" nach christlicher Terminologie) ebenso umfaßte wie griechische Dichtung und Wissenschaft und iranische Monarchietraditionen.

Und so wie die Zentren wechselten, blieb Europa ein riesiger Komplex verschiedener sich ineinander übersetzender Kulturen. Vom Athen der Tragiker und des Sokrates zum Alexandria der Bibliothek und der wissenschaftlichen Traditionssicherung; zum Rom der Rechts- und Verwaltungskultur; zum Konstantinopel der Konzilien und Klöster.
Alles änderte sich, als die Karolingerherrschaft von Frankfurt und Aachen aus sich mit Europa gleichsetzte und das oströmische Reich als häretisch und unrömisch aus diesem lateinischen Europa ausschloß. Auf diese Weise wurden die Voraussetzungen für jene Zivilisationsgrenze geschaffen, von der Geremek mit Recht behauptet, daß sie noch heute nachwirke.

Seither ist Europa ganz gewiß ein Plural von Kulturen, die zum Teil die Kommunikation untereinander abgebrochen und sich feindlich voneinander abgegrenzt haben.

So gibt es das Europa der latinisierten romanischen und nichtromanischen Völker; das Europa der latinisierten Germanen, Slawen, Finnen, Balten; und schließlich die nichtlatinisierten Germanen und Slawen.

Durch das 2. Jahrtausend, also seit dem Hochmittelalter des 12. Jahrhunderts hat sich dieser Teilung noch eine zweite hinzugesellt: die zu Nationen revolutionierten Völker mit ihren Konfessions- bzw. Revolutionsverwandten: die Deutschen und Skandinavier; die Briten, Schotten, Walliser und Iren; die Franzosen mit ihren romanischen Nachbarn und schließlich die Russen mit allen postkommunistischen Staaten innerhalb und außerhalb der GUS, d. h. die mehrsprachigen und polykulturellen Völker in der Mitte und im Osten Europas.

Wer diese historische Geographie Europas auf sich wirken läßt, wird auch eine Antwort auf die Frage nach dem Zentrum Europas geben können, die sich von allen Verengungen und kulturellen Befangenheiten zu befreien vermag. Das Zentrum Europas hat immer wieder gewechselt. Aber heute liegt es ganz gewiß nicht mehr in Westeuropa, sondern dort, wo alle hier vorgestellten Europas aufeinandertreffen: östlich von Deutschland, in den mittelosteuropäischen Ländern.

### These 4

***Europa, das ist der Geschichtsraum, in dem Hellenen, Lateiner, Kelten, Germanen und Slawen sich mit Christentum, Judentum und Islam auseinandergesetzt, sich von ihnen abgegrenzt, ihre Traditionen rezipiert oder bekämpft haben.***

Verstehen wir Europa als Einheit und Identität eines bestimmten Geschichtsraumes, dann muß man sich vergegenwärtigen, inwiefern mehrere solche Identitäten im Laufe von Antike, Mittelalter und Neuzeit sich ablösten, wobei freilich klar wird, daß dieses Dreierschema von Antike-Mittelalter-Neuzeit mit der Abfolge dieser Identitäten keineswegs deckungsgleich ist.

Am Anfang dieser Identitäten steht das die heutige Türkei einschließende christianisierte Imperium Romanum mit seinem Zentrum Neu-Rom, Konstantinopel, durch welches die Geschichte des vorchristlichen Rom bis in die Mitte des 15. Jahrhunderts verlängert worden ist.

Das Vordringen des Islam bis auf die iberische Halbinsel ebenso wie die Südwanderungen der germanischen Stämme läßt eine neue Konstellation entstehen: das Kalifat von Cordoba im Westen, in dem Christentum und Judentum und Islam koexistieren; quer durch den Kontinent eine Kette von christianisierten germanischen Stammeskönigtümern, an deren Spitze schließlich um 500 das der Franken tritt, nachdem es sich der römischlateinischen Tradition angeschlossen hat.

In seiner Folge kommt es um 800 zur folgenreichsten West-Ost-Teilung des Kontinentes, als Karl der Große mit seinen Theologen Aachen und Westrom zum Zentrum eines orthodoxen Europa gegen die in ihren Augen häretische Christenheit der Griechen des Orients erheben.

Geremek (aaO 9) erinnert zurecht daran, wie Otto der Große und seine Nachfolger der sofort nach dem Tode Karls einsetzenden Destabilisierung dieses fränkisch-lateinischen Europa entgegenzuwirken versuchen, indem sie eine Art Europa der Regionen anstreben, gegliedert in Italia, Germania, Gallia, Slawonia, wie es auf einem Bamberger Evangeliar aus der Zeit Ottos III. dargestellt wird.

Aber derartige Konzepte verblaßten augenblicklich, als um 1100 das Jahrtausend der Europäischen Revolutionen (Rosenstock-Huessy) beginnt, indem das römische Papsttum sich an die Spitze der lateinischen Mächte stellt und die Leitung der anti-islamischen Kreuzzugsbewegung, an sich reißt. Aufs Ganze gesehen, scheitern diese Unternehmungen, aber sie tragen doch wesentlich dazu bei, daß der Islam seither als eine außereuropäische Größe gilt und die bis heute wirksame Verweisung Südost- und Osteuropas in den außereuropäischen Orient fester Bestandteil des europäischen Selbstbewußtseins wird.

Als Ergebnis der weiteren Revolutionen in Deutschland, England und Frankreich entsteht das System der „Großen Mächte" (Ranke), in dem die Gleichsetzung von Europa mit Westeuropa sich fortsetzt in einem immer weniger stabilen Gleichgewicht, das schließlich in dem zweigeteilten Weltkrieg unseres Jahrhunderts endgültig zusammenbricht. Bemerkenswert für dieses System war, daß es das Christentum durch eine sogenannte natürliche Religion ersetzen wollte, deren Elemente auch im Judentum und Islam anzutreffen sein sollten. Eine Illusion, die sich nur daraus erklären läßt, daß die Bedeutung des Messianismus für das Judentum und die der Theokratie im Islam gänzlich übersehen wurden. Diese Ausblendungen wurden freilich nur dadurch möglich, daß die politische Philosophie des 17. und 18. Jahrhunderts auf der Verdrängung eines für die Religionsgeschichte der Neuzeit zentralen Ereignisses beruhte, nämlich der jüdischen messianischen Bewegung um Sabbatai Zwi, die von Palästina und Kleinasien aus nach 1660 auch in Westeuropa ihre Kreise zog. Erst durch die unermüdlichen Forschungen des jüdischen Historikers und Theologen Gerschom Scholem konnte diese Verdrängung aufgeklärt und damit den Illusionen des sogenannten „Natürlichen Systems" der Boden entzogen werden.

### These 5

***Die mittlerweile zutagegetretenen Grenzen des Geschichtsbildes der rationalistischen Aufklärung sind ein Appell, das ganze Europa einschließlich seines hellenisch-slawischen Teiles und seiner Verbundenheit mit Judentum und Islam neu ins Auge zu fassen. Das schließt aber die Aufgabe ein, im Interesse einer Überwindung des kulturellen West-Ost-Schismas durch philosophische wie theologische Bemühungen eine laizistisch-dualistische Auffassung von Aufklärung durch eine universale und polykulturelle zu ersetzen, so wie das von Hamann und Herder schon einmal versucht und von Goethe weitergeführt worden war.***

Wir haben die Aktualität fränkisch-lateinischer und römisch-lateinischer Abgrenzungen unterstrichen. Geradezu verblüffend ist, wie noch heute ein Teil - und nicht gerade ein unrepräsentativer! - des intellektuellen Frankreich auf den islamistischen Fundamentalismus im modern-laizistischen Gewande genau so argumentiert wie die Theologen Karls des Großen. Statt an die schon im Hoch- und Spätmittelalter (Nikolaus von Cusa!) begonnen Grundlegungen einer Verständigung über die Grenzen der drei nachbiblischen Religionen hinweg anzuknüpfen, wird einer Politik der Abgrenzung das Wort geredet, die den aus dem Kampf der europäischen Konfessionen in der nachreformatorischen Christenheit entstandenen Laizismus zur alleinigen Norm erheben möchte.

Etwas zugespitzt könnte man sagen: Wir laufen durch Huntington und die französischen Laizisten Gefahr - mit denen sich dann merkwürdiger Weise die Ideologen des „christlichen Abendlandes“ verbünden! - eine zeitgenössische und rein kultur-politisch ausgerichtete Variante zur „Heiligen Allianz" von 1815 aufbauen zu wollen. Aber wie vor 180 Jahren müßte sie auch diesmal an den historischen Realitäten scheitern.

Weder ist das Christentum noch jene herrschende Religion, die die großen europäischen Kathedralen symbolisieren, noch erlauben es die unauslöschlichen Brand- und Blutspuren, die die Naziverbrechen hinterlassen haben, das Judentum als einen nicht zu Europa gehörigen Fremdkörper zu behandeln.

Wenn dem aber so ist, dann muß es auch eine gesamteuropäische Antwort auf die Frage nach dem gemeinsamen Fundament der europäischen Kultur nach dem mehr als 1000-jährigen Schisma geben, eine Antwort, die christlichen Lateinern, Hellenen, Slawen genauso gemeinsam ist wie Juden und Muslim.

M. E. muß diese Antwort aufs neue der manichäischen Weltreligion der Spätantike entgegentreten. Diese hat die Einheit der Geschichte nur festhalten können auf der Basis des indo-arischen Geschichtsbildes, wie es Dumezil und andere in diesem Jahrhundert erneuert und gegen den Menschheitsuniversalismus des Alten Testamentes gestellt haben. Einheit der Geschichte auf Kosten einer Zweiteilung der Wirklichkeit in Gut und Schlecht - so lautet die Formel dieses Geschichtsbildes -.

Dieser Weltreligion einer Zweiteilung der Wirklichkeit, die natürlich auch eine solche der Menschheit zur Folge haben mußte, stellte die Spätpatristik eines Augustinus, Dionysius Areopagita und Maximus Confessor nicht etwa eine andere Weltreligion, sondern eine Zeitenökonomie entgegen, aus der keine Zeit und kein Mensch mehr herausfallen mußte, weil sie sich nicht an einem einzelnen Ereignis, sondern an einer Strukturierung aller Ereignisse im Kreuz der Wirklichkeit orientierte, so wie es die eben damals entstehende christliche Zeitrechnung tat.

Nur im slawischen Osteuropa ist man sich noch der Hintergründe dieser weltkulturell bedeutsamen Auseinandersetzung bewußt. Als die beiden griechischen Theologen und Sprachgelehrten Konstantin - Kyrill und Method 863 in das Reich der Mährer zogen, um die linguistischen Grundlagen für einen Gottesdienst und Bibeltexte in der dortigen Volkssprache zu schaffen, mußten sie das gegen heftige Widerstände des lateinischen Erzbistums Salzburg durchsetzen. Dort opponierte man einer slawischen Liturgie und slawischen Bibeltexten mit der Begründung, Liturgie und Bibel müßten immer in einer der drei heiligen Sprachen gefaßt sein, Hebräisch, Griechisch oder Lateinisch. Wie man aus dem Schrifttum im Umkreis der Slawenlehrer entnehmen kann, war ihnen sehr wohl bewußt, daß es sich bei diesem Streit um mehr als die kulturelle Gleichberechtigung der Sprachen handelte. Verbarg sich doch hinter dem Sprachenstreit die Frage: Ist eine Veränderung der Geschichte denkbar und vollziehbar, die alle Völker zu Zeitgenossen macht oder nicht?

Weil Kyrill und Method diese Frage bejahten, konnten sie ihre Grundlegung slawischer Schriftsprache gegen die These von der Dominanz dreier heiliger Sprachen in dem Bewußtsein vollbringen, einen Schritt in Richtung auf die Liturgiefähigkeit aller Volkssprachen und damit für die Demokratisierung aller Kulturen getan zu haben. Es ist dieser geschichtliche Hintergrund, dessentwegen die unabhängig gewordene Slowakei Kyrill und Method in die Präambel ihrer neuen Verfassung aufnahm: Sie sah mit Recht in den geschichtlichen Anfängen ihrer Schriftsprache die allerersten Grundlegungen für die demokratische Legitimierung ihrer Unabhängigkeit.

### These 6

***Auf der hier entwickelten Basis kann Einheit der Geschichte allgemein und Einheit der Geschichte Europas als Inbegriff kultureller Codes verstanden werden. Zukunftsfähigkeit der Kultur hängt demzufolge ab vom Vorhandensein einer Sprachkompetenz, die eine Kommunikation zwischen diesen Codes ermöglicht.***

Offenkundig muß es als vorrangige politische Aufgabe moderner Demokratie verstanden werden, Wege für eine Demokratisierung von Kulturen zu ebnen. Es muß genau der dem Huntington-Programm des Kulturprivileges entgegengesetzte Weg einer Kommunikation auf gleicher Ebene beschritten werden, so daß Kulturen nicht einen wechselseitigen Kampf darum führen müssen, welcher es gelingt, die andere zu unterdrücken und zu zerstören, sondern sich gegeneinander öffnen und in Kommunikation treten können.

Aber man kann jetzt schon sehen, daß allem vielfältig vorhandenen guten Willen eine große Schwierigkeit im Wege steht. Das, was wir unter Kulturenkommunikation verstehen, muß etwas ganz anderes und viel mehr sein, als was derzeit unter kulturellem Austausch verstanden wird. So wichtig solcher Austausch zum wechselseitigen Kennenlernen ist - er allein kann noch nicht als Vollzug von Kommunikation gelten. Dadurch, daß er eine Kultur zur bloßen Zuschauerin der anderen macht, bleibt die eine Seite immer passiv, statt aktiver Kommunikationspartner zu werden. Niemand wird bestreiten, daß unter solchen Umständen nicht sichergestellt werden kann, daß Kulturen nicht mehr als herrschende oder unterworfene einander konfrontiert wären.

Das muß schon deswegen sehr ernstgenommen werden, weil Überfremdung und Unterwerfung auch dort stattfinden, wo nicht Besatzungsmächte oder Besatzungszustände beides veranlassen oder eine Mehrheit die Rechte einer Minderheit unterdrückt. So hat z. B. der Lenin nicht folgende Flügel der russischen Sozialdemokratie um Bulgakov den Marxismus deswegen abgelehnt, weil er auf eine Überfremdung des bäuerlichen Rußland durch einen ihm fremden Industriekapitalismus hinauslaufen müsse, eine Einschätzung, mit der sie gar nicht so weit von dem Marx entfernt waren, der seinen berühmten Brief an Vera Sassulitsch schrieb.

Nachdem die leninistische Diktatur sich durchgesetzt hatte, war es Pawel Florenskij, der in seinen Vorlesungen über „Denken und Sprache" an der Praxis der bolschewistischen Umbenennungen und Namengebungen nachwies, daß die Partei der Bolschewiki die russische Kultur unter das Joch einer ihr fremden westeuropäischen Technokratie lenkte. Florenskij blieb nicht bei dieser Kritik stehen. Er unternahm vielmehr den Versuch, den kulturzerstörenden Folgen eines technokratischen Rationalismus dadurch entgegenzuwirken, daß er eine klar erfaßbare und pragmatisch handhabbare Grundlage ausfindig machte, auf der Kulturen gewissermaßen miteinander sprechen können, statt sich so zu befehden, wie es Slawophilen und Westler sei mehr als 150 Jahren nicht nur in Rußland taten und teilweise noch immer tun.

Florenskij unternahm es, den gemeinsamen Code aller Kulturen dadurch zu entschlüsseln, daß er in seinem „Symbolarium" genannten Werk versuchte, die Hauptelemente der in allen Kulturen anzutreffenden Symbole zu sammeln und zu analysieren.

Das ist das genaue Gegenteil zu der von Umberto Eco jüngst monografisch behandelten Suche nach der vollkommenen Sprache (U. Eco, Die Suche nach der vollkommenen Sprache, München 1994). Eco vermag die logischen und semantischen Irrtümer, die diese Suche in immer neue Sackgassen geführt hat, sehr anschaulich zu schildern. Aber aus dem Dilemma von Esperanto und Babel, der Kunstsprache, die den Reichtum historischer Sprachen unter Utilitätskriterien verschwinden läßt und dem Chaos der Unübersetzbarkeiten, weiß auch Eco keinen Ausweg. Letztenendes hängt alles ab von der Frage: Ist die Geschichte selbst die gesuchte universale Sprache oder muß sie erst als eine Art Metasprache zu allen anderen gefunden bzw. erfunden werden?

Eco meint, es sei besser, im Chaos von Babel weiterzudolmetschen, als dies in irgendeinem Gewaltstreich beenden zu wollen. Aber ist mit dieser lobenswerten Zurückhaltung schon die Gefahr gebannt, daß der Verlust der gemeinsamen Kommunikation uns immer tiefer in den Verlust der Wirklichkeit hineintreibt?

### These 7

***Der Verlust der Kommunikation durch eine Politik der kulturellen Identitätsbehauptungen mittels Abgrenzung oder ethnischer Separation führt zur kulturellen Umweltzerstörung, zum Verlust von Geschichte, zur Entstellung und Zerstörung von Identität! So gesehen ergeht an unsere Gesellschaft der Imperativ einer Ökologie der Geschichte in der Bewahrung ihrer kulturellen Vielsprachigkeit.***

Dieser Imperativ wird sicherlich nicht nur, aber gewiß doch und zuverlässig durch eine Weiterarbeit an Florenskijs universalem Symbolarium der Kulturen befolgt. Was Florenskij vor einem reichlichen Menschenalter mit den primitiven Möglichkeiten eines Ein-Mann-Betriebes unternahm, können wir heute mit unseren Erfahrungen und Möglichkeiten in ganz anderer Intensität und Breite fortsetzen. Mit Hilfe der inzwischen entwickelten Theorien der Graphen und Fraktale könnten Florenskijs Grundlagen drastisch erweitert werden. Die Fortschritte in der strukturellen Ethnologie bei Levi-Strauss und anderen sowie die inzwischen gelungenen Entzifferungen von Sprachen könnten das Gleiche inbezug auf materielle und inhaltliche Vollständigkeit bewirken.

Eine solche Verbreiterung könnte auch förderlich sein für die kritische Durchmusterung der kulturellen Codes auf die in ihnen enthaltene Semantik der Abgrenzung und Exklusivität, sozusagen eine „Entzerrung" der Perspektive auf andere Kulturen.

Positiv erschlösse sich so eine Komparatistik kultureller „Stile" von Welterschließung, der nicht auf der problematischen Kategorie der „Denkformen", sondern auf konkret aufweisbaren weil voraussetzbaren Sprachformen und -strukturen aufbaute.

Die Frage legt sich nahe, ob nicht die Europäische Union hier eine Organisation initiieren könnte, durch die das, was zur Zeit in der heute alltäglichen Chaotik der Spezialisierung in unzähligen Europäischen Akademien geschieht, auf eine organisierende Mitte, eine Art Kristallisationskern hingeordnet und integriert werden könnte. War es nicht dies, was einem Comenius in seinem Programm der Consultatio catholica, Leibniz in seinem Akademieplan vorschwebte und was Lotman und seine Mitarbeiter am Tartuer Programm der Zeichensysteme schon einmal paradigmatisch verwirklicht hatten?

Bosnien und Sarajevo werden für immer das Menetekel bleiben für das, was eintritt, wo die kulturelle Kommunikation abbricht. Wer die Brisanz sich befehdender Identitäten so verkennt wie Handke, darf sich nicht wundern, daß er mit seiner eigenen in Konflikt kommt und auf einmal ein so schlechtes Deutsch schreibt, als sei er ein Germanistikstudent im ersten Semester.

Auch ein so bedeutender Historiker wie Braudel verkennt die Situation gänzlich, wenn er meint, alles war wir bräuchten sei „Markt, Demokratie, ein bißchen Brüderlichkeit". Aber Markt und Demokratie wird es niemals ohne Frieden geben. - Ohne Demokratie, die auf Kulturen und deren Identitäten ausgeweitet ist, - werden wir wiederum den Frieden nicht finden.

Ergo - an dem „bißchen Brüderlichkeit" hängt alles. Aber es wird eben ganz gewiß nicht nur ein Bißchen sein dürfen und auch nicht nur Brüderlichkeit. Denn soviel steht fest: Die Kultur des nächsten Jahrtausends kann nicht mehr bloß männlich sein. Auch in diesem Fall muß zur Identität die Differenz kommen!

Brüssel, 7. März 1996

Aus: Mitteilungsblätter der Eugen Rosenstock-Huessy Gesellschaft, Jahrgang 1995, S.65 - 72

T.Dr.
