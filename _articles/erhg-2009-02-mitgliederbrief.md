---
title: Mitgliederbrief 2009-02
category: rundbrief
created: 2009-02
summary:
zitat: |
  >„Denn sie (die Apostel) glaubten alle miteinander Sünder und Gerechte zu sein,
  >und eben nur miteinander der heilenden Geisteskraft würdig zu werden.”
  >*Eugen Rosenstock-Huessy, Die Sprache des Menschengeschlechts II, S. 891*
---

## Eugen Rosenstock-Huessy Gesellschaft e.V.

{{ page.zitat }}

**Vorstand/board/bestuur:**\
*Dr. Eckart Wilkens (Vorsitzender);*\
*Wilmy Verhage; Andreas Schreck; Dr. Jürgen Müller; Lothar Mack*\
*Antwortadresse: Dr. Eckart Wilkens: Everhardstraße 77, 50823 Köln, Deutschland*\
*Tel: 0(049) 221 511265*

***Brief an die Mitglieder Februar 2009***

**Inhalt**

{{ page.summary }}


[zum Seitenbeginn](#top)
