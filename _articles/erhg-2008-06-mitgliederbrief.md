---
title: Mitgliederbrief 2008-06
category: rundbrief
created: 2008-06
summary:
zitat: |
  >„Es ist daher im wörtlichen Sinne eine Notwehr, wenn ich die Nichtigkeit der beiden Dogmen der Wissenschaft stillschweigend zugrunde lege: Denn sie besagen:
  >
  >1. Daß eines Menschen Leben mit seinem Tode ende und
  >2. dass die Worte des Menschen ein bloßes Mittel zum Ausdruck seiner Gedanken seien.
  >
  >Diese beiden Dogmen berauben unsere Worte jeden Sinnes, und die letzten 50 Katastrophenjahre sind die logische Antwort auf sie. Diese Dogmen erweisen die Unsinnigkeit einer Wissenschaft,
  >die den Menschen als ein Stück Natur behandelt.”
  >*Eugen Rosenstock-Huessy, Die Frucht der Lippen, in: Die Sprache des Menschengeschlechts II, S. 804f.*
---

## Eugen Rosenstock-Huessy Gesellschaft e.V.

{{ page.zitat }}

**Vorstand/board/bestuur:**\
*Dr. Eckart Wilkens (Vorsitzender);*\
*Wilmy Verhage; Andreas Schreck; Dr. Jürgen Müller; Lothar Mack*\
*Antwortadresse: Dr. Eckart Wilkens: Everhardstraße 77, 50823 Köln, Deutschland*\
*Tel: 0(049) 221 511265*

***Brief an die Mitglieder Juni 2008***

**Inhalt**

{{ page.summary }}


[zum Seitenbeginn](#top)
