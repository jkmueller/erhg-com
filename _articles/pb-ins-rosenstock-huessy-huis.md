---
title: "Piet Blankevoort: Im Rosenstock-Huessy-Huis in Haarlem"
created: 2021
category: veroeff-elem
published: 2021-12-18
---
### Stimmstein 2, 1988
#### Piet Blankevoort


De stap van een »gewoon gezinsleven« naar een leven in een gemeenschap was een stap in het duister. De betekenis ervan werd pas van lieverlee helderder.

Daarom wil ik bij het nu beginnen, en wel met mijn nu, dat sterk verbonden is met het nu van wereld en maatschappij.

Wat mij onophoudelijk bezighoudt en benauwt is de machtsstrijd zowel op militair als op economisch gebied. Onze wereld dreigt tenonder te gaan door het denken en handelen in kaders die passé zijn.

Ik word me er steeds meer van bewust, dat de twee blikseminslagen in de 20e eeuw onze tijd aan het licht hebben gebracht: de Holocaust en Hirosjima. Ik probeer dan ook steeds dieper in de betekenis van deze twee inslagen door te dringen. De mensheid kan niet terug achter deze twee ingrepen in ons bestel. Zouden we, zoals Elie Wiesel ons leert, de Holocaust als verleden tijd beschouwen, dan is Hirosjima onze toekomst. Maar het is ons, mijn, heden, mijn geschiedenis.

Wat heeft dit te maken met de stichting van het Rosenstock Huessy Huis? Dat wij als gezin zijn toegetreden was gegrond op de overtuiging, dat Rosenstock ons geslacht nieuwe wegen wees waar steeds duidelijker bleek, dat de oude wegen òf onbegaanbaar waren geworden òf ten verderve voerden. Door het kennis nemen van Rosenstocks boeken, die we in een leeskring bespraken en trachtten te begrijpen, ging me zo nu en dan een lichtje op, dat mijn leven en geloof verlichtte en veranderde. Deze twee werden bepaald door mijn afkomst, mijn geschiedenis, inclusief de tweede Wereldoorlog, en mijn opleiding op »Kerk en Wereld« die direct na de oorlog begon. Zonder Rosenstocks woorden (die naar het hebreeuws daden zijn) zou onze leef- en werkgemeenschap niet zijn gesticht. Daar komt bij dat we hierbij gesterkt werden door de revolte van de jaren '60, die onze hoop op verandering deed herleven.

De zekerheid, het geloof dat we een goede beslissing hadden genomen, is uitgangspunkt van ons bestaan. De spanningen en ergernissen waaraan w e als club en persoonlijk onderhevig waren en zijn, hebben dit fundament niet ondergraven. Overigens beleef je ook wat goed is voor hart en ziel. Door het met elkaar leven, ervaar je heel sterk dat leven leren is.

Dat na 16 jaren zovelen ons Huis als een baken zien; dat we zoeken naar het overschrijden van grenzen, zowel in tijd als in ruimte; dat we steeds meer oor en oog krijgen voor een komende tijd (hoewel we 20 jaar geleden ook al spraken over het 3e millennium, zij het meer in abstracto dan als werkelijkheid); dat we steeds meer contacten krijgen met al of niet door Rosenstock Huessy aangesproken mensen (we ontdekten dat vele anderen zich ook op dezelfde weg en in dezelfde tijd bevinden); dit alles is een teken, dat ons verrek uit de gangbare kaders, als antwoord op de vraag »Adam, waar zijt gij?« goed was.

Ik ben ervan overtuigd, dat ons bestaan bedreigd wordt.

Rosenstock Huessy geeft geen oplossingen, maar wel richtingaanwijzers, die ons de tekenen der tijden wijzen en leren verstaan. De juistheid van dit richtsnoer, dat we, een halve generatie geleden, tot het onze maakten, wordt bevestigd in het NU.
>*Piet Blankevoort*
