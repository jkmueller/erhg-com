---
title: "Jacob Taubes über Rosenstock-Huessy"
category: aussage
published: 2022-01-04
name: Taubes
---

„Keiner, den ich kenne, weiss so um den Zusammenhang von Recht und Sprache, keiner um die geheimen Vehikeln der Macht, die bis in die sublimsten Erscheinungen hineinwirkt, keiner könnte daher so eindringlich von jenen Orten berichten, wo Macht, Recht, Sprache und Liebe sich vermischen.”
>*Jacob Taubes an Eugen Rosenstock-Huessy, 14.12.1953, Transkription der Briefe durch Walter Husemann 1999, LKA EKvW 5.16, 300-302.*
