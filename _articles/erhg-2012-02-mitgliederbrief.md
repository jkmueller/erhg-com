---
title: Mitgliederbrief 2012-02
category: rundbrief
created: 2012-02
summary:
zitat: |
  >Wo aber Gott gegenwärtig ist, da hören die abstrakten zeitlosen Wahrheiten auf. Und wo das Abstrakte aufhört, da fängt eben die Schuldvergebung an.
  >Im Angesichte Gottes wird die Schuld glückhaft. Denn er kommt.
  >*Eugen Rosenstock-Huessy, Die Sprache des Menschengeschlechts Band 1, Glückhafte Schuld, Seite 255*
---

## Eugen Rosenstock-Huessy Gesellschaft e.V.

{{ page.zitat }}

**Vorstand/board/bestuur:**\
*Dr. Eckart Wilkens (Vorsitzender);*\
*Wilmy Verhage; Thomas Dreessen; Andreas Schreck; Dr. Jürgen Müller*\
*Antwortadresse: Dr. Eckart Wilkens: Everhardstraße 77, 50823 Köln, Deutschland*\
*Tel: 0(049) 221 511265*

***Brief an die Mitglieder Februar 2012***

**Inhalt**

{{ page.summary }}


[zum Seitenbeginn](#top)
