---
title: "Hans Ehrenberg: Das Evangelium der „Anderen“ "
created: 2002
category: veroeff-elem
published: 2024-04-28
---
### Stimmstein 7, 2002

#### Mitteilungsblätter  2002

### Hans Philipp Ehrenberg

### Das Evangelium der „Anderen“
#### Glaube - Unglaube, kirchensoziologisch und theologisch interpretiert (Auszug)[^1]

Pirenne in seinem „Mahomet und Karl der Große" stellt die Erwägung an, warum die plötzliche Eroberung der ganzen Küsten des Mittelmeers durch den Islam im 7. Jahrhundert Leib und Seele der dortigen Völker erfaßt hätte, aber die vorangegangene christliche Eroberung derselben Völker nur ihre Seele.  Ist der Islam umfassender als das Christentum?  Es scheint so!  Für den Moment des Ansturms ist er es in der Tat.  Im ersten Angriff ist der politische Monotheismus (der Islam) allen anderen Religionen, auch der christlichen, überlegen, so wie der Nationalsozialismus auf den ersten Anhieb alle anderen Mächte überrennt. Aber im ersten Vorstoß lügt alle Wirklichkeit; erst muß sie sich bewähren; sie muß sich ausschwingen, um die Bewertung nach dem Erfolg auszutilgen; Wirklichkeit und Wahrheit vereinen sich erst in ihrer letzten Gestalt, am Ende aller Tage. Also beruht der Blitzsieg des Islams auf einer Sinnestäuschung, auf einer weltgeschichtlichen Verzeichnung.  Christus hat den Mißerfolg ebensosehr wie den Erfolg in das Werden seines Reiches auf Erden einkalkuliert.  Das Christentum hat fast während der ganzen Zeit seiner Existenz an einer schwerwiegenden Überschätzung des Triumphs gelitten: nur im Zeichen des Kreuzes wirst du siegen!  Erfolg und Mißerfolg sind nicht berechenbare Größen. Das echte Bild des Christentums darf nicht einer Irrenzeichnung gleichen, wo Dimension und Perspektive anormal verzeichnet sind.  Weil das aber so war, haben sich die christliche Kirche und die Wissenschaft aneinander wundreiben müssen. Heute haben beide ihre Erfolgsideologie abgeschrieben; fortan gleichen sie sich an durch die Verbindung von Erfolgs- und Mißerfolgsethik; sie hören auf, sich aneinander zu reiben. Alle rationalen Schlüsse, die man aus dem Blitzkrieg ziehen kann, sind krank und inkorrekt; weder das Dogma noch die Naturwissenschaft sind wirklich „exakt“ gewesen. Die letzten Konsequenzen vertritt nur der Fürst dieser Welt, der Satan. Konsequente Kirchenchristen und konsequente Wissenschaftsschulen dienen allesamt dem Teufel.

[^1]: Aus: Das Evangelium der „Anderen“. In: Junge Kirche – Protestantische Monatshefte,  Hrsg. von Walter Herrenbrück, Hans J. Iwand u. a. 18 (1957), S. 675-684, Auszug S. 683-684.
 
