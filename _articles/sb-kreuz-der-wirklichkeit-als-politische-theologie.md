---
title: "Sven Bergmann: „Kreuz der Wirklichkeit“ als „politische Theologie“"
category: einblick
published: 2024-07-13
landingpage: 
order-lp: 5
---
#### Eugen Rosenstock-Huessy zum 6. Juli 1888
<!--more-->

Im Ringen um die geistigen Verwandtschaften im bestialischen 20. Jahrhundert hat die Kontroverse um die Genese der plebiszitären Führerdemokratie internationale Beachtung gefunden. Wolfgang J. Mommsens Klassiker über „Max Weber und die deutsche Politik“ und die Frage, ob Carl Schmitt in legitimer Ahnenfolge Webers zu sehen sei, trennte Schüler und politische Denker dies- und jenseits des Atlantiks. Dabei ist ein paralleler Rechtsgelehrter in dem Schlagabtausch kaum beachtet worden. Und während sich die Weberausgaben türmen und die Schmittstudien blühen, fällt nur wenig Licht auf einen ihrer gewichtigsten „Kritiker“. Wie kann das sein, bei einem Werk, das umfangreicher und weitreichender ist, als bei der „Konkurrenz“?

Eugen Moritz Friedrich Rosenstock kam am 6. Juli 1888 genau fünf Tage vor Carl Schmitt zur
Welt. Sehr früh hat er sein Leben als schicksalhaft empfunden: Beide Eltern trugen den
Geburtsnamen Rosenstock und er wuchs im großbürgerlichen Haus als einziger Sohn umrahmt von
drei älteren und drei jüngeren Schwestern auf und die dreifache Acht entfaltete ihr Mysterium. Von seinem Vater übernahm er die Leidenschaft zur Geschichtsforschung und von der Mutter die Liebe zur Sprache. In dem preußisch-jüdischen Elternhaus wurde der christliche Festkalender gefeiert. Zu Weihnachten oder an Geburtstagen beschenkte Eugen seine Familie mit Übersetzungen. Der 15jährige entzifferte altägyptische Inschriften, verdeutschte das sechste Buch der Ilias und widmete sich fachmännisch Luthers Bibelübersetzung oder Jacob Grimms deutschen Rechtsaltertümern. Bei Grimm erlernt er die fundamentale Bedeutung der Sprache und ihren semantischen Tiefgang (Roland Reuß). Diese Mitgift beflügelte sein Studium in Zürich, Heidelberg und Berlin. Der 19jährige läßt sich 1909 evangelisch taufen, sicher nicht unbeeinflusst vom „innerweltlichen“
protestantischen Erfolg Preußens in Wirtschaft, Verwaltung und Wissenschaft. Taufpate ist Karl von Noorden, Enkel eines bekannten Rankeschülers. 1912 gilt seine Habilitation „Ostfalens Rechtsliteratur unter Friedrich II“. Mit 24 Jahren ist Rosenstock der jüngste „Kronprinz“ einer deutschen Universität. 1914 erscheint sein „Professorenbuch“: „Königshaus und Stämme in Deutschland zwischen 911 und 1250“ , das „Klassikern“ der Geschichtsforschung von Ernst H. Kantorowicz (Die Körper des Königs) oder Otto Brunner (Land und Herrschaft) in vielem vorauseilt: Rudolph Sohm, Anreger für Webers Charisma ebenso wie für Schmitts katholische „Politische Theologie“, lobt die „geistvolle“ Arbeit. Der Kriegsausbruch verhindert die Rezeption.

Am 6. Juli 1913 feiert Eugen seinen 25. Geburtstag und am Tag darauf kommt es mit seinen
Freunden Rudolf Ehrenberg und Franz Rosenzweig in Leipzig zu dem berühmten Nachtgespräch,
dass Franz Rosenzweig nicht zur Konversion zum Christentum, sondern zum bewusst gelebten
Judentum führt. Die Fragen des „Werturteilens“ werden mit Haut und Haaren besprochen in
gemeinsamer Lektüre von Selma Lagerlöfs Roman „Die Wunder des Antichrist“. In diesem
Gespräch bricht sich endgültig Franz' Lösung vom Historismus und die Hinwendung zu einem
„Relativismus“ in gegenseitiger Anerkennung Bahn. Es kann nicht die eine Wahrheit für alle geben. Jeder muss seinen eigenen Dämon suchen und finden. Bei Max Weber steht Objektivität als „Sachzusammenhang der Probleme“ in Anführungsstrichen! Die drei ringen hart, aber voller
Respekt um Religion, Weltgeschichte und ihre Verantwortung zum praktischen Handeln.
Rosenzweig selbst hat den Anstoß zu seinem „Stern der Erlösung“ auf die konsequente „Haltung“ seines zwei Jahre „jüngeren Lehrers“ Eugen zurück geführt. Für Rosenstock bleibt das „Kreuz der Wirklichkeit“ zentrale Orientierungsachse: Jeder Mensch lebt gleichzeitig zwischen Innen und Außen wie zwischen Vergangenheit und Zukunft. Die Sprache vermittelt diese polaren Wirklichkeiten. In einem der letzten und intimsten Briefe an seine Mutter Helene schreibt Max Weber am 12. April 1914, wie er sich in einem Stich der Madonna Sixtina „ als das Jesuskind auf Deinen Armen und die anderen Geschwister als die Engel“ sah. Und 1909 an Ferdinand Tönnies: „Aber ich bin, nach genauer Prüfung, weder antireligiös noch irreligiös.“

Auch im Krieg reist die Korrespondenz zwischen Rosenzweig und Rosenstock nicht ab. Während
Eugen an der Westfront der Hölle von Verdun ins Auge sieht, ist Franz in Galizien stationiert. Eugen erkennt die existentielle Bedeutung von Befehl und Gehorsam in der Ausnahmesituation des Krieges. Das ändert seinen Blick auf das Zusammenleben der Menschen über Generationen. Der Appell, der Imperativ, der Anruf der Namen geht den Sachen voraus und hat eine andere Qualität. Der Mensch erfährt seine Verbindung ins Leben durch seinen Namen. Jedes Kind wird zuerst mit Du und mit Namen angeredet. Das Du geht dem Ich voraus. Glaubwürdiges und überzeugendes Sprechen gelingt nur mit vollem Körpereinsatz, eine Einsicht denen sich die neuere Hirnforschung auf anderen Wegen nähert. Eugen nimmt diese Entdeckung ernst und entwickelt eine leibhaftige Grammatik: Der Mensch und seine Geschichte stehen in weitreichenden Korrespondenzen. Dieses
Einsicht wird er in seinem Werk über „Die europäischen Revolutionen und der Charakter der
Nationen“ und in seiner Universalrechtsgeschichte „Im Kreuz der Wirklichkeit“ über 40 Jahre
weiter ausarbeiten und er wird danach leben. Er versteht es als eine „Neue Politik“. Politik lernt
man nicht aus Büchern. Es gibt auch keinen „Idealstaat“. Ein „Weltstaat“ wäre im Gegenteil die
Hölle auf Erden, da ist er sich mit Max Weber und Carl Schmitt einig. Aber jeder Mensch handelt
im namentlichen Gespräch Tag für Tag „politisch“. Und den „großen Politiker“ kennzeichnet die
Zahl der Menschen mit denen er sich zu vereinbaren vermag. So wird Zeitgenosse der Zukunft, wer
sich einen Namen macht. Ghandi, Mandela, Aun San Suu Kyi oder Martin Luther King sind solche
Namen. Vielleicht Edward Snowden. „Das Vaterland liegt nicht als Mumie in den Gräbern der
Ahnen, sondern es soll leben als das Land unserer Nachfahren“ (Max Weber). Helmut Schmidt
wäre so ein Mensch.

Auch familiär reicht Eugen Rosenstock-Huessy's Nabelschnur in die große Politik. Er ist Schwager
des Juristen Hermann U. Kantorowicz, des Landeshuter Unternehmers Max Hamburger sowie von
Hermann Michel, der seit 1912 als Chefredakteur die 15. Auflage des Brockhaus vorantreibt. Über
Kantorowicz, den der Student auf Archivreisen in Florenz begleitet, erfährt er Neuigkeiten aus dem
Hause Weber in Heidelberg. Dort lernt er 1912 auch seine Frau Margrit Huessy kennen und lieben.
Das Paar wird 1914 heiraten und Eugen wird sich ab 1925 auch offiziell Rosenstock-Huessy
benennen. Das ist bis heute „allen bekannt und ein Geheimnis“ (nach Jacob Grimm)! Seine älteste
Schwester Thea ist mit dem deutsch-russischen Revolutionär Eugen Leviné befreundet, bei dem
Meinecke-Schüler Siegfried A. Kaehler ist er Trauzeuge.

Stationen „politischen Handelns“ sind 1919 die Chefredaktion der Daimler Werkzeitung, 1920 die
Leitung der Akademie der Arbeit in Frankfurt, das Gespräch von Protestanten, Katholiken, Juden
und „Griechen“ in den Neuwerk Verlagen: Patmos, Moria und Eleusis und später in der Zeitschrift
„Die Kreatur“. Hier veröffentlichen Walter Benjamin und Dolf Sternberger frühe Aufsätze. Freunde
sind Karl Barth, Rudolf Breitscheid, Walter Dirks, Hans Ehrenberg, Willy Hellpach, Ernst Michel,
Karl Muth, Gustav Radbruch, Paul Tillich, Leo Weismantel, Victor von Weizsäcker und Joseph
Wittig. Mit Robert von Erdberg und Werner Picht treibt Rosenstock-Huessy die
Erwachsenenbildung voran und korrespondiert mit dem „Collegen“ Carl Schmitt über Politische
Theologie oder politisch geladene Begriffe. Dem jungen Georg Picht, der in einer Jahrhunderte
übergreifenden deutsch-französischen Familienkonstellation steht, widmet er eine
Zusammenfassung dreier Aufsätze: Bei Georg Picht werden Hellmut Becker und Alexander Kluge
Anknüpfung finden!

Eugen Rosenstock-Huessy hat sich nie der Illusion hingegeben, den Führer zu führen, der viele
Mitglieder der „geistigen Elite“ 1933 erlegen sind. Im Februar 1933 stellt er seine
Universitätsveranstaltungen in Breslau ein und übersiedelt am 9. November auf der MS
Deutschland, mit seiner Frau Margrit und seinem Sohn Hans in die Vereinigten Staaten, um in der
Einsamkeit Vermonts ein neues Leben zu beginnen. Noch zweimal wird er mit Sohn Hans seine
Mutter Paula in Deutschland besuchen. Als diese 1938 ihre Wohnung räumen soll, wählt sie den
Freitod und schreibt einen Abschiedsbrief an die Familie. Später hat Eugen Rosenstock vielen
seiner Zeitgenossen vorgehalten, sie hätten 1918 wieder an die Zeit vor 1914 anknüpfen wollen, mit
all den fatalen Konsequenzen eines zweiten Krieges. Er schreibt 1919 vom „Selbstmord Europas“
und prophezeit ein Lügenkaisertum. Auch Max Weber hatte 1920 vor der eiskalten Nacht der
Reaktion binnen eines Jahrzehnts gewarnt.

Schon 1910 war es in einem Kreis fortgeschrittener Studenten in Heidelberg zu einem großen
Disput über die „Lösung“ der sozialen Frage gekommen. Zwischen den Extremen einer
marxistischen Revolution (Eugen Leviné) und dem gemeinsamen „Teetrinken“ von Arbeiterschaft
und Vermögenden in Großbritannien, reifte bei Werner Picht und Eugen Rosenstock-Huessy der
Gedanke einer „deutschen Lösung“ im Sinne einer „Arbeitsgemeinschaft“ von Kapital und Arbeit.
Diesen Gedanken hat Rosenstock-Huessy weitergetragen und in Wirklichkeit übersetzt. Unter
anderem auch im Waldenburgischen Industrierevier in Schlesien auf dem hohe Arbeitslosigkeit
lastete. Hier ermunterte er jüngere Männer aus „besten Kreisen“ und die z.T. bei ihm in Breslau
studierten, zur sozialen Arbeit: Helmuth James von Moltke, Horst von Einsiedel und Carl Dietrich
von Trotha griffen den Gedanken auf. Aus ihren Kontakten und ihrer Hingabe an die Sache
entwickelte sich später die Keimzelle des Kreisauer Kreises gegen den „Wilderer aus Braunau“. Zu
seinem 70. Geburtstag ehrte Walter Hammer Eugen Rosenstock-Huessy mit dem Titel „Erzvater des
Kreisauer Kreises“. Und die Idee setzte ihren Siegeszug in den Camps im Gedenken an William
James und den Peace Camps John F. Kennedys fort. Auch der Deutsche Entwicklungsdienst war bis
zu seiner „Kommerzialisierung“ dieser Idee verbunden.

Mit Max Weber und Felix Somary hat Eugen Rosenstock-Huessy mehr von der globalen Wirtschaft
verstanden, als kurzsichtig gewordene Ökonomen: „Jeder Fortschritt der Technik erweitert den
Raum, verkürzt die Zeit, zerschlägt die Gruppe“. Ganz durchgeführt, erschüttert die „reine Lehre“
die Grundlagen jeder bürgerlichen Gesellschaft. Der „Kalkulationskalender der Industrie“ enthält
„einen 24-Stunden-Tag, ein 365-Tage-Jahr und gelegentlich Perioden von 5 oder 10 oder 30 Jahren,
um das Budget wieder auszubalancieren oder um Amortisationen von Darlehen zu planen“ und
versucht, sich „auf alle Teile unseres Lebens auszudehnen“. 1951 warnt Eugen Rosenstock-Huessy
vor einer „Nationalökonomie der Raubtierstaaten“. „Die Erschütterung des Rechtsgefühls äußert
sich auf allen Seiten; bei den Juristen darin, dass sie nur noch als Handlanger der
Wirtschaftsgruppen sich fühlen und von Gerechtigkeit nicht zu wissen vorgeben, bei jungen
Unternehmern, die sich einbilden, nur in ihre Hände sei der Menschheit Würde gelegt, bei jungen
Gewerkschaftlern, die beinahe einen politischen Generalstreik mitgemacht hätten“. Heute ist
Deutschland wieder das Raubtier mit den schärfsten Zähnen in Europa und „Top-Ökonomen“
jubeln. Schon Max Weber forderte, die „Anmaßung“, mit der „gewisse angebliche Schöpfer von
angeblich neuen Methoden der Nationalökonomie sich breit machen, als das zu brandmarken, was
sie ist: Geschäftsreklame und weiter nichts.“ Und während in Europa die alten Konstellationen
zurück sind, schreiben Berliner Hofhistoriker von goldenen Zeiten.

Nach 1945 wird Eugen Rosenstock-Huessy zu Gastvorlesungen nach Deutschland eingeladen, unter
anderem auch nach Köln. Dort lernt Hans-Ulrich Wehler den „Deutsch-Amerikaner“ kennen. Es
ehrt den Nestor der „kleinstdeutschen Schule“ der Geschichtswissenschaft, dessen Konzept mehr
von Sachen als von Namen ausgeht, dass er dennoch festhält: „Eugen Rosenstock-Huessy ist der
einzige geniale Mann, den ich bisher kennen gelernt habe.“ Mit den Jahren werden auch seine
philologisch akribischen Studien zum Namen „deutsch“ aufgegriffen, in denen er eine deutsch-
französische Korrespondenz aufzeigt, ausgehend vom karolingischen Gerichts- und Heeresverband.
Carl Zuckmayer schreibt ihm: „Du hast das Wort Deutsch, von üblich-völkischer Deutung gereinigt,
als Wesen des Dialogs, den „Dialog des deutschen Volkes“, neu erkannt und begründet.“ Ein Jahr
nach dem Tod seiner Frau Margrit zieht 1960 Freya von Moltke in das Haus Four Wells in Vermont.
Wie kaum ein zweiter hat der Preuße aus Steglitz bei Berlin die Verständigung zwischen
Religionen, Nationen, Kontinenten und Generationen befördert. Sein Leben ist mit dem 20.
Jahrhundert verwoben, ohne sich zu verstricken. War er etwas besonderes? Im Drei-Kaiser-Jahr
1888 geboren, stirbt Eugen Rosenstock-Huessy am 24. Februar im Drei-Krisen-Jahr 1973. Seine
sechs Enkelsöhne trugen den Lattensarg, der Jahre zuvor von Unbekannten für einen Studentenulk
vor das Haus gestellt worden war, zur Beerdigung. Dieses „Symbol“ war dem Geehrten ganz recht.

Eugen Rosenstock-Huessy ist ein Riese des 20. Jahrhunderts auf den Schulten eines Riesen des 19.
Jahrhunderts. Wo schifft sich Eugen Rosenstock-Huessy denn zu seiner Argonautenfahrt von der
alten Welt in die Neue Welt ein? Die Argo ist ja bereits auf dem Wasser! Das unendliche Meer sind
die „Agrarverhältnisse des Altertums“ von Max Weber. Diese „Weltreise“ setzt Eugen Rosenstock-
Huessy fort! Auch Hanna Vollrath hat nicht ganz hingeschaut, sonst hätte sie die Bauern, Mönche
und Ritter entdeckt. Wenn heute Mediävisten den Investiturstreit „auf die Couch legen“ und wenn
Ägyptologen auf der Zauberflöte blasen, ohne Eugen Rosenstock-Huessy zur Kenntnis zu nehmen,
kann in Deutschland von einer „Rezeption“ noch keine Rede sein.
