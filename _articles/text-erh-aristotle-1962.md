---
title: "Rosenstock-Huessy: Aristotle (1962)"
category: online-text
published: 2022-08-21
org-publ: 1962
language: english
---

**ARISTOTLE,** 384 - 322 B.C., outstanding Greek philosopher and one of the greatest thinkers of all time. He was a disciple of PLATO and a tutor of ALEXANDER THE GREAT. Interested mainly in understanding the essential nature and relations of life, Aristotle thereby differed from Plato, whose chief concern was the improvement of life. Aristotle wrote and lectured on logic, physics or the general study of inorganic nature, astronomy, biology, psychology, metaphysics or pure philosophy, ethics or moral philosophy, politics or political philosophy, economics, and aesthetics as embodied in rhetoric and poetics. Thus his encyclopedic writings laid the foundation of all the sciences and all the branches of philosophy known today, and many of his ideas are still held to be valid.

Aristotle was called the Stagirite because he was born at Stagira, a Greek colonial town on the coast of Macedonia. His interest in physiological and zoological phenomena may have resulted partly from his father's being court physician to the Macedonian king, Amyntas II, grandfather of Alexander the Great. Aristotle's Ionian descent was no doubt partly responsible for the thoroughness of his studies, Ionian philosophers having had a passion for exactitude. He became at 17 a pupil of Plato at the Academy of Athens, and studied or taught there for 20 years, until Plato's death in 347 B.C. Disappointed with the appointment of Plato's successor, he spent the years
347-335 B.C. in wandering and teaching. His tutorship of Alexander the Great occurred during this period. When Alexander ascended the throne of Macedon, Aristotle returned to Athens and established a school, the Lyceum, which he headed until 323 B.C., and where he produced most of his numerous writings. This school of philosophy became known as the *Peripatetic* because of the discussions carried on between teacher and students on a covered walk *peripatos*, in its garden. Anti-Macedonian agitation in Athens in 323 B.C. caused Aristotle to leave the school and flee to Chalcis, where he spent the remainder of his days.

**Major Works.** *Organon*, consisting of six treatises on logic, is regarded as Aristotle's chief work, the other most famous ones being the *Metaphysics*, *Physics*, *On the Heavens*, *History of Animals*, *On the Parts of Animals*, *De Anima*, *Politics*, *Nicomachean Ethics*, *Rhetoric*, and *Poetics*. In addition there is the renowned *Constitution of Athens*, Aristotle's description of Athenian government, a work which was found late in the nineteenth century. He produced descriptions of 158 "politics" or constitutions, but all the others are lost.

It has been said that Aristotle's philosophy was a reaction against the idealism of Plato, the theory that the ultimate reality as regards anything is an idea. Aristotle maintained that the world consists of substances, each existing in itself. Universals, according
to him, exist only in individual substances. Fundamental in Aristotle's philosophy is the distinction between matter and form and that between the actual and the potential. He believed there is a purpose in the world, also that God is both its first cause and final objective.

Aristotle's biological information is remarkably accurate. The same cannot be said of his astronomical deductions, nor of his theory that the basic components of nature are earth, air, fire, and water, an error common among the ancient Greeks. His psychological concepts were not successfully challenged until late in the nineteenth century. Aristotle's political and ethical theories emphasize his acceptance of the world as it is. Though he believed in benevolent monarchy, he did not build up a case for any specific form of
government, as Plato did. An exponent of the golden mean, nothing in excess, Aristotle considered happiness the chief end in life and believed that to attain happiness a man must have a measure of good luck as well as adequate worldly goods.

**Influence.** In ancient times and the early Middle Ages, Aristotle's works, though extensively used played only a secondary role compared to the philosophy of Plato. In later medieval times, after the twelfth century, however, Aristotle was regarded as the final authority in every field, and he has probably had greater influence on the intellectual life of Western civilization than any other individual.

The Arabs took up Aristotle in the ninth century, and it was Jewish and Arabian scholars that brought Aristotle's ideas to Christendom in the Middle Ages. The Mohammedan AVERROES, the Jew MAIMONIDES, and the Christian St. Thomas Aquinas all sought to harmonize their theology with Aristotelian thought. Eventually, however, it hardened into a dogmatic system quite different in spirit from the original and has been widely considered an obstacle to further intellectual development. This gave rise to a successful revolt against its dominance, a revolt led by FRANCIs BACON and RENÉ DESCARTEs. In recent years efforts have been made to revive the dominance of Aristotelian scholasticism in a modified form, known as Thomism. (See NEO-SCHOLASTICISM.) Aside from that his influence remains vital in philosophy, science, literary criticism, and social thought.

**BIBLIOG.** R. P. McKeon, ed., *Basic Works* (1941), *Introduction to Aristotle* (1946); W. Durant, *The Story of Philosophy* (ed.
1933); E.W.F. Tomlin, *Great Philosophers* (1950); R.C. Lodge, *Great Thinkers* (1951); L. H. Hough, *Great Humanists* (1952)

Entry in the American People’s Encyclopedia (Grolier Inc., 1962)

“ARISTOTLE,”8: 201-204. {Unsigned.  Francis Squibb, co-author.} Reel 11, Item 545, Frame 6733.

[„Aristotle” as part of the PDF-Scan](http://www.erhfund.org/wp-content/uploads/545.pdf)

[zum Seitenbeginn](#top)
