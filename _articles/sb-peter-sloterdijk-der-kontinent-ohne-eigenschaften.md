---
title: "Sven Bergmann: Peter Sloterdijk: Der Kontinent ohne Eigenschaften"
category: einblick
published: 2024-12-24
landingpage: front
order-lp: 10
---
Nachdem Peter Sloterdijk Eugen Rosenstock-Huessy schon verschiedentlich als den *„bedeutendsten Sprachphilosophen des 20. Jahrhunderts“* gelobt hatte
<!--more-->
(den Jürgen Habermas in einer Fußnote seiner *„Theorie des kommunikativen Handelns“* versteckte) oder als *„geistreichen, christlichen Revolutionstheoretiker“*, hat er ihm in seinem neuesten Werk ein ganzes Kapitel eingeräumt.[^36] Hatte der Meisterarrangeur aus dem Hause Suhrkamp bereits mehrere Interventionen zu Europas Vergangenheit, Gegenwart und Zukunft vorgelegt, so eröffnet er *„Der Kontinent ohne Eigenschaften. Lesezeichen im Buch Europa“* mit seiner Antrittsvorlesung am Collège de France vom 4. April 2024: *„Ausreden, Nekrologe, Aprèsludes“*. Im Zentrum der Lektion Zwei steht Eugen Rosenstock-Huessy und sein Werk über die europäischen Revolutionen, vor allem in der Fassung, die 1938 auf ein angelsächsisches Publikum umformuliert worden war. Peter Sloterdijk geht zwar auf die Erstausgabe von 1931 ein und erwähnt den ersten „Wurf“ von 1920 als *„Die Hochzeit des Kriegs und der Revolution“*, aber im Mittelpunkt steht *„Out of Revolution“*. Damit dürfte er der erste Denker von Rang sein, der sich in dieser Ausführlichkeit und vertiefter Kenntnis mit dieser Version beschäftigt. Eugen Rosenstock-Huessy selbst hatte immer wieder beklagt, daß diese Fassung seines Werkes nicht einmal in den Universitätsbibliotheken vorhanden sei.[^37]

[^36]: [Sloterdijk über Rosenstock-Huessy](https://www.rosenstock-huessy.com/sloterdijk-erh/)
[^37]: Immerhin dieses Manko ist nun schon seit vielen Jahren behoben.

Lektion drei widmet sich der legendären Untergangsprognose Oswald Spenglers, bei der Sloterdijk die Pointe entgeht, daß sich Spengler und Rosenstock auf Vermittlung des Hochland Herausgebers Carl Muth im April 1919 zum Gespräch in München getroffen hatten, in jenem Schwabing, in dem auch Lenin über seine Revolutionspläne brütete.[^38] Auf das ausdrücklich Spengler umkreisende Kapitel vom *„Selbstmord Europas“* aus der *„Hochzeit des Kriegs und der Revolution“* geht er nicht ein, auch wenn er ausführt:

*„In der Konfrontation Rosenstocks mit Spengler treffen ein Verfechter der negentropischen Prozeßlogik und ein Fürsprecher der zivilisatorischen Entropie hart aufeinander. In anderer Terminologie könnte man von der Kollision von Utopismus und Pragmatismus sprechen – oder, um an die klassisch gewordene Musilsche Unterscheidung zu erinnern von dem unauslotbaren Gegensatz zwischen den Möglichkeitssinn und dem Wirklichkeitssinn.“*[^39]

[^38]:  Man wird kaum fehl gehen, wenn man die Verbreitung von Spenglers Zeitansage in Parallele mit der Einschätzung vom „Ende der Geschichte“ nach 1989 sieht.
[^39]:  Peter Sloterdijk, Der Kontinent ohne Eigenschaften. Leseteichen im Buch Europa, Berlin: Suhrkamp 2024, S.151.

Im Text bemüht Sloterdijk für „Our of Revolution“ die Parallele zum Afrikaner Aurelius Augustinus, der sein Werk über den Gottesstaat *„Civitas Dei“* in dem Jahr 410 aufschrieb, als der Barbar Alarich die ewige Stadt Rom eroberte und brandschatzte. Ähnlich einschneidend sei für Eugen Rosenstock-Huessy und einige seiner wacheren Zeitgenossen das Weltkriegserlebnis von 1914-1918 gewesen, in der unumkehrbar eine alte Welt zusammenbrach. In diesem Zusammenhang erwähnt der Autor Karl Barth, Ernst Bloch, Martin Heidegger, Paul Valery, Hermann Broch und dann vor allem auch Rosenstocks vielleicht engsten Freund Franz Rosenzweig:

*Er teilt es besonders mit Franz Rosenzweig, der in seinem Werk *„Der Stern der Erlösung“*, 1922, eine Abkehr von aller Überlieferung theoretischen Philosophierens vollzog, um nur noch ein „neues“, ein vom wirklichen Dasein „verunreinigtes“ Denken gelten zu lassen: eine durchaus ent-ewigte, ganz in dir Zeitlichkeit ausgesetzte Bestimmung auf die Prämissen des unerlösten Lebens.*[^40]

[^40]: ebd. S.106.

Er habe sich als Kämpfer begriffen, der „nackt vor dem Tod gestanden hatte“ und „gewandelt“ aus der Prüfung hervorgegangen sei.[^41] Mit leicht spöttischem Unterton formuliert Sloterdijk:

*Von Verdun zurückkehrend wie Jesus nach vierzig Tagen in der Wüste, wandte sich der Autor im Jahr 1931 an seine Zeitgenossen innerhalb und außerhalb der deutschen Sprachgrenzen, dann erneut im Jahre 1938, als Europa vorübergehend verloren war, an die anglophone Öffentlichkeit, um sie in seine verbindliche Vision von der Kohärenz der revolutionären Aufbrüche in weitere, freiere, reichere Weltentwürfe einzubeziehen.*[^42]

[^41]: ebd. S.118.
[^42]: ebd. S.119.

Mithin müsse *„Rosenstock-Huessys Autobiographie des westlichen Menschen als ein Dokument der verzögerten Rückkehr aus dem Weltkrieg“* begriffen werden, als eine Art Feldpostbrief, der erst auf amerikanischem Boden zugestellt worden sei. Seitdem sei kein authentisches Leben mehr möglich, das nicht im Zeichen der Revolution stehe.

In seiner sprudelnd geläufigen Sprachakrobatik gibt Sloterdijk zu erkennen, daß er den tieflotenden Ansatz von Eugen Rosenstock-Huesy durchaus zu schätzen weiß. Mit *„Out of Revolution“* und weiteren Hinweisen habe *„der Autor das Recht erworben, sich nicht allein an die amerikanischen Leser von 1938, sondern auch an die Bürger des heutigen Europa zu wenden.”*[^43] Neben glänzenden Einsichten stellt er ihm aber fatale historische Fehlurteile in Rechnung. Zum Teil habe er die brutale Entschlossenheit Adolf Hitlers zur Revolte gegen die Revolution unterschätzt und vor allem habe er die Russische Revolution zu lange aus Sicht seiner Revolutionstheorie wohlwollend begleitet, obwohl die Revolutionäre von Anfang an entschlossen gewesen seien, im *„selbstlosen Kampf gegen das Bestehende“* *„über Leichen“* zu gehen: *„Der Verzicht auf privates Glück übersetzte sich bei Ihnen in beispiellose Begabungen zu politischer Grausamkeit.“*[^44] Von den Sklavenlagern Trotzkis und dem nach 1930 gegründeten Gulag habe Rosenstock nichts gehört oder nichts hören wollen, obwohl er kein Apologet gewesen sei.

[^43]: ebd. S.139.
[^44]: ebd. S.125.

Rußland wird als die radikalste Revolution geschildert, die sich einerseits ihres Vorgängers in der Französischen Revolution bewußt war, die andererseits aber auch auf extrem disparate Voraussetzungen in einem Land traf mit *„subtil demoralisierten Eliten“* und *„grob demoralisierenden Lebenswirklichkeiten der Dörfer und Kleinstädte“*.[^45] Dabei unterstellt Sloterdijk dem Autor, den Idealismus der russischen Revolutionäre überschätzt zu haben, ohne zu bedenken, daß er einige der Protagonisten persönlich aus Heidelberg oder Zürich kannte, wie etwa den in München hingerichteten Eugen Leviné.

[^45]: ebd. S.123.

Seinen Zuhörern deutete Peter Sloterdijk in Paris unverhohlen weitere Etappen der zugrundeliegenden historischen Konstellationen an: *„Die herkömmliche Pflege der franco-russischen Freundschaft, ohne welche die französische Linke sich vormals kaum denken ließ, ist – seit Putins Willen zur Zerstörung der Ukraine sich offen gezeigt hat – zu einer Peinlichkeit geworden, für die sich nur noch wenige Akteure hergeben.“* Dabei erwähnt er Pierre de Gaulle, den Enkel des Generals sowie Emmanuel Todd, den Soziologen, der schon seit vielen Jahren den *„Untergang der Neuen Welt“* gekommen sieht.[^46]

[^46]: Emmanuel Todd, *Der Westen im Niedergang. Ökonomie, Kultur und Religion im freien Fall*, Neu-Isenburg: Westend Verlag 2024.

Steht Lenin am Ende der Protagonisten der europäischen Revolutionen, so steht Gregor der VII. an ihrer Wiege. Gegen die Korruption der Stämme, Geschlechter und Sippschaften erhob er sein Forderung an den Kaiser „herabzusteigen“ und eine höhere Autorität als Herkunft und Blutsbande anzuerkennen. Für den von Eugen Rosenstock-Huessy geprägten Begriff der *„Papstrevolution“* verweist Sloterdijk zurecht auf den Juristen Harold Joseph Berman und seine Forschungen zu *„Recht und Revolution“*, ohne zu erwähnen, daß dieser, bis zu seinem Tod 2007 in Harvard lehrende Professor in den 30er Jahren am Dartmouth College ein Schüler Rosenstocks war, der ihm auch bei seinen Studien des russischen Rechts in der UdSSR lebhaft unterstützte. Er galt als einer der *„Polymaths of American Education“*.[^47]

[^47]: Zitat nach Wikipedia.

Nach dem Startpunkt der Papstrevolution, die sich gegen die Korruption der Clans und Familienverbände auflehnte, bildeten den Mittelblock die drei Ereignisse, *„durch welche sich Deutschland im 16. Jahrhundert, England im 17. Jahrhundert und Frankreich im 18. Jahrhundert in das revolutionäre Weltkulturerbe einbrachten.“*[^48] Dabei seien es immer kleine avantgardistische Gruppen gewesen, die das Risiko auf sich nahmen, „den Umbruch“ zu wagen.[^49] Die kurze Liste der Revolutionäre führt von Gregor VII. zu Luther und geht weiter über Cromwell und Robespierre bis zu Lenin.

[^48]: Peter Sloterdijk, Der Kontinent ohne Eigenschaften. Leseteichen im Buch Europa, Berlin: Suhrkamp 2024, S.131.
[^49]: Hier verweist Sloterdijk auf die aktuellen Forschungen von Bruno Karsenti über „pivot elites“ aus Gesprächen mit Bruno Latour, denen Rosenstock-Huessy schon vor Jahrzehnten vorausgegangen war.

Die *„radikale Promotion des Menschenwesens“*, die mit den neuzeitlichen Revolutionen in die Welt gekommen sei, sei *„bis heute weder in Asien noch in der arabo-muslimischen Welt und in den Nationen des Globalen Südens nachvollzogen, weswegen man man dort gern – um altehrwürdige Repressionen auf der Linie patriarchalischer Strukturen zu legitimieren – gern“ betone, „es gebe doch eine Mehrzahl von eigenständigen „Zivilisationen“. Daher sei das Pathos allgemeiner Menschenwürde und weiblicher Gleichberechtigung nicht mehr als ein eurozentrischer Spleen, dem Kredit zu geben man sich hüten werde.“*[^50]

[^50]: Peter Sloterdijk, Der Kontinent ohne Eigenschaften. Leseteichen im Buch Europa, Berlin: Suhrkamp 2024, S.137.

Dabei wird das Christentum als Fundament der europäischen Revolutionen deutlich hervorgehoben, auch wenn Sloterdijk Einspruch gegen allzu geradlinige Kausalitäten erhebt. Das Christentum habe, angefangen bei der Übersetzung des Namen Jesu, über das Pfingstwunder, bis hin zum griechisch überlieferten Neuen Testament oder aramäischen Texten, besonders von ungenauen „Übersetzungen“ profitiert.

Es sei das große Verdienst von Eugen Rosenstock und seinen Freunden nach den Erschütterungen des Weltkriegs mit der jenseitigen Ausrichtung des Christentums, sei es in theologischer, metaphysischer oder ideologischer Maske gebrochen zu haben. In diesem Zusammenhang habe sich der Sieg des Imperiums über die Ecclesia als ein „religionsgeschichtliches Desaster“ erwiesen und zwar für alle die, denen es mit ihrem Glauben ernst war.[^51] Und fast überall hätten sich die Kirchen als Conciergerien der Machtstaaten erwiesen.[^52]

Bei Rosenstock-Huessy klärte sich während der Nachkriegsjahre die Überzeugung, daß die Wahrheit, auf die es ankommt, nicht auf eine „andere Welt“ angewiesen ist. Sie speist sich aus Zukunft in unserer Welt. Die Vornehmen der kommenden Zeiten sollen sich nicht auf ihre Herkunft berufen; sie legitimieren sich durch ihre Zukunftsbereitschaft, getragen von der Hingabe an das Neue, das nottut.[^53]

[^51]: ebd. S.111.
[^52]: ebd. S.112.
[^53]: ebd. S.118f.

*Mit dem Hinweis auf die Idee der notwendig gewordenen Pseudonymität christlicher Impulse rühren wir an das logische Zentrum von Rosenstock-Huessys damaligem und späterem Denken, bei dem „die Sprache des Menschengeschlechts“ und das Zusammenleben von Menschen unter dem antwortfordernden Druck einer mehrdimensionalen Wirklichkeit ins Zentrum rückte. Seine bedeutendste Intuition drückte sich in der Behauptung aus, wonach es künftig darauf ankomme, die Wahrheit unter den Bedingungen der Mehrsprachigkeit zu denken – einer Herausforderung, die mehr bedeutete als das friedlich-unfriedliche Nebeneinander nationaler und ethnischer Idiome.*[^54]

[^54]: ebd. S.114.

Der Begriff der „Vollzahl der Zeiten“, der auch eine Wiederkunft der Stämme, wenn auch in verwandelter Form bedeutet, hat Sloterdijk nicht rezipiert. Daher unterstellt er Rosenstock-Huessy „schweigende Skepsis“ zu dem „Kult um indigene Kulturen“, obwohl er bereit sei, ihnen zuzuhören.[^55]

[^55]: ebd. S.138.

Obwohl Sloterdijk in seinem Fazit den eingetretenen Perspektivwechsel hervorhebt, daß Europa sich nicht mehr selbstsüchtig oder beschränkt selbst beschreibt, sondern zunehmend von außen beurteilt wird und daß dabei gerade die Schattenseiten der globalen Eroberungen des Planeten in Rechnung gestellt werden, entgeht ihm, dass schon Eugen Rosenstock-Huessy genau diesen Perspektivwechsel beschrieben hat, zuerst in der kleinen Broschüre *Europa und die Christenheit*, dann in seiner *Hochzeit des Kriegs und der Revolution* und schließlich in *Out of Revolution*. In jeder dieser Schriften wurde die Chronologie der Ereignisse um 180 Grad gedreht. Die jüngsten Ereignisse gehen voraus und das ursprünglichere Geschehen rückt nach hinten. Jeder „mittelalte“ Leser wird diesen Perspektivwechsel in seiner eigenen Biographie nachvollziehen können. Noch in den 80er Jahren waren die Lebensläufe traditionell bestimmt und begannen mit dem Elternhaus. Seit das „Business“ in alle Lebensbereiche eingesickert ist, bewirbt man sich vor allem und zuvorderst mit seinen letzten Erfolgen und neuesten Kompetenzen. Dabei hat sich Europa von Amerika überholen lassen, genau wie Eugen Rosenstock dies schon 1919 gesehen hat: *Amerika holt über, mit dem „Kreuzzug des Sternenbanners“*. Europa hat seinen Monopolanspruch schon längst beerdigen müssen. In seinem Fazit empfiehlt Peter Sloterdijk *„Out of Revolution“* gerade auch in der Gegenwart steckengebliebenen Europäern als Medizin gegen *„die besseren Engel“* in Brüssel, an die sich Zukunftsverantwortung so herrlich bequem delegieren läßt.[^56]

[^56]: ebd. S.139.

Peter Sloterdijk, *Der Kontinent ohne Eigenschaften. Leseteichen im Buch Europa*, Berlin: Suhrkamp 2024.
>*Sven Bergmann*

>*aus dem [Mitgliederbrief 2024-08]({{ '/erhg-2024-12-mitteilungen' | relative_url }})*
