---
title: Voorwoord van Wilmy Verhage bij «Weitersager Mensch»
category: weitersager
order: 2
---
*Welkom Lezer, Lezeres,*

Vijf jaar heb ik geaarzeld om een inleiding te schrijven bij dit project, dat wij Weitersager Mensch hebben genoemd. Zo lang heeft het geduurd voor ik tot enig overzicht was gerijpt er iets met volmacht over te durven zeggen. En voor ik helder had tot wie ik mij richt: Tot u, tot jou, beminde onbekende.

Oktober 2004 kwamen negen mensen van de Rosenstock-Huessy Gesellschaft een weekeinde bij elkaar in Loccum, Nederland om te bespreken wat de volgende stap in de toekomst zou moeten worden, wilden we een confict overleven dat was ontstaan over de heruitgave van een van Rosenstock-Huessy's hoofdwerken. Twee vrouwen, zeven mannen; twee Nederlanders, zeven Duitsers; vier bestuursleden, een nieuwkomer, een herintreder, en drie actieve leden kwamen daartoe bijeen.
Uit alle opties die op dat weekeinde werden geopperd om uit de impasse te geraken, kwam het concrete plan van Professor Dr. Jürgen Frese bovendrijven inleidingen te schrijven bij de vele werken, die Rosenstock- Huessy vanaf de eerste wereldoorlog heeft geschreven. Ze zodoende weer onder de aandacht te brengen. Elke aanwezige zou een boek voor zijn rekening kunnen nemen. Andere leden konden we daartoe ook uitnodigen. Zo met elkaar zouden we een eind kunnen komen. Ja. Maar: Dat zouden persoonlijke inleidingen moeten worden. Want als je over een leer over mensen wilt schrijven, dan mag de ervaring niet ontbreken, en die is nu eenmaal persoonlijk. En zo evolueerde het plan van de inleidingen tot een plan – met dank aan Dr. Eckart Wilkensdie inleidingen in de vorm van brieven te gieten aan mensen die je kent, die jonger zijn in leeftijd of in kennis, meestal beide. Dus de volgende generatie die hoort bij de mensen die naar Rosenstock-Huessy luisteren. We werden het eens, en zo geschiedde.

Voor mij persoonlijk was dat weekeinde de eerste kennismaking met mensen van wie ik hoopte dat ze geestverwant zouden zijn. Ik wist van Rosenstock-Huessy vanaf mijn jeugd, maar ik ben hem op latere leeftijd pas echt gaan lezen en voor mij was de tijd rijp contact te leggen met mensen die zich ook met zijn gedachtegoed bezighielden. Kennelijk was de herkenning wederzijds, want Fritz Herrenbrücktoen voorzitter van de ERH-Gesellschaftvroeg mij het project te coördineren. Ik heb daar ja op gezegd, want op mijn beurt leek het me een uitgelezen gelegenheid de kennismaking voort te zetten en te verdiepen. Otto Kroesen en Eckart Wilkens hebben me daarbij vooruit geholpen, en inmiddels ben ik geheel ingeburgerd en hou ik van ons.
Het project is enigermate met zichzelf in tegenspraak: het was het eerste project voor onze website, en die staat een open einde toe: Iedereen die Rosenstock-Huessy heeft gelezen en die een vraag vanuit zijn of haar omgeving beantwoordt over een werk of een wet of een zinsnede of voor mijn part het hele oeuvre van Rosenstock-Huessy, kan dat antwoord nog steeds via mij op de site publiceren. Want nog lang niet alle werken
hebben hun inleidende persoonlijke brieven. Het is niet klaar. Het hoeft ook niet klaar. Het komt nooit klaar.

Maar tegelijkertijd wordt het, zoals het er nu in manuscriptvorm ligt, als boek een afgerond geheel. De wetten van de geest zijn toch geheel andere dan die van de materie, zo blijkt maar weer…

Ik hoop, beminde onbekende, dat u de brieven uw tijd gunt. Ze staat in de volgorde waarop Rosenstock-Huessy zijn boeken waarop ze betrekking hebben heeft geschreven. Het zijn stuk voor stuk juweeltjes (en een enkele juweel) geworden, die het verdienen met aandacht gelezen te worden. En dan, aan het eind… Zoals een prisma het licht verdeelt in kleuren, zo vormen de bijdragen samenin al hun persoonlijke verschilleneen goede indruk van waar Rosenstock- Huessy over gaat.

Het is fonkelend nieuw wat hij zegt. Nog voorbij het postmoderne. Én van praktisch nut voor nu. Laat u schokken.

Wilmy Verhage, Amsterdam, maart 2010
