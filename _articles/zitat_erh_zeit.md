---
title: "Rosenstock-Huessy: Zeiten im Kreuz"
category: online-text
landingpage: front
order-lp: 50
org-publ: 1958
---

>Die Vergangenheit werde erzählt.\
Die Zukunft werde verheißen.\
Die Gegenwart werde erkämpft.\
Das Tote mag man wissen.
<!--more-->

Eugen Rosenstock-Huessy, Soziologie II, Seite 21
