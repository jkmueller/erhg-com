---
title: Jahrestagung 2009
created: 2009
category: jahrestagung
thema: Die Frucht der Lippen
---
![Haus Salem]({{ 'assets/images/haus_salem.jpg' | relative_url }}){:.img-right.img-large}

Die Jahrestagung fand vom 3.-5. April 2009 im Haus Salem in Bielefeld statt zum Thema:

### {{ page.thema }}
das Schlußkapitel aus der «Sprache des Menschengeschlechts».

Für Eugen Rosenstock-Huessy war die «Frucht der Lippen» der Höhe- und Schlußpunkt seines jahrzehntelangen Wirkens. Er geht darin der inneren Beziehung der vier Evangelien zueinander nach und entdeckt, wie sie sich gemeinsamen Korpus anwachsen, einer vierstimmigen Harmonie.

Er vergleicht auch den Anfang und das Ende der einzelnen Evangelien: Jeder Schreiber endet bei ganz anderen Aussagen über Jesus als ihm zu Beginn seines Werkes wichtig waren. Vor allem um diesen Aspekt der «Frucht der Lippen» ging es in den Gruppen. Deren Leiter geben persönlich gefärbte Rückblicke auf diese Begegnungen.


Wie charakterisiert Eugen Rosenstock-Huessy die vier Evangelisten? Einige zentrale Aussagen regen das Gespräch an.

## Gruppen zu den vier Evangelien

Auszüge aus dem Rundbrief vom Juni 2009

### Gruppenarbeit zum Johannesevangelium

Mir ist noch nicht sagbar, was sich entfaltet. Ich fühle mich wie ein Anfänger. Immerhin: Mit Rosenstock-Huessy im Rücken auf ein Evangelium zu schauen (und die anderen drei mehr oder weniger ungestaltet liegenzulassen), es verlangt Mut und Zutrauen. Unsere Gruppe fand keinen Weg zum Erkennen „des Prozesses”, erkannte die Wegmarken, gar den Wendepunkt, nur schemenhaft. Ein ganzer Tag ist sehr kurz, um gemeinsame Geduld, gemeinsame Bescheidenheit anzuerkennen. Ohne solche Kühnheit, dieses Zutrauen reift in mir, ist die Frucht der Lippen nicht zu ernten.

> *Andreas Schreck*

### Markus in Salem

Das war ein schönes Abenteuer, mit sechs Menschen (drei Männern, drei Frauen, drei Deutschen, drei Niederländern, zwei Theologen, vier Laien also eigentlich sehr ausgewogen) zu versuchen, in Rosenstock-Huessys Fußspuren Neuland im Markusevangelium zu entdecken. Wir brauchten dazu wirklich die drei Sessionen. Die ersten zwei, um uns einigen zu können und uns zu richten, die letzte, um gemeinsam die Mitte zu finden: Den Umschlagpunkt vom Ich des Sohns Gottes zu dem Wir der Diener des Worts.

Erstmal haben wir in einer ersten Sprechrunde konstatiert, wie verschieden alle Eingangspositionen waren, und von da aus haben wir die ungeheure Anmaßung, dass Jesus der wirkliche Sohn Gottes war, im Gegensatz zu Caesar/Pharao, dem als Gott verehrten Reichshaupt, ins Auge gefasst. Und wie gefährlich das war und unter gegebenen politischen Bedingungen noch ist, zeigt das Schicksal Helmuth James von Moltkes. Das war die erste Stunde.

Die zweite Stunde, als wir das Ende, die Diener des Worts, nachvollziehen wollten, kam für mich völlig unerwartet die Mitteilung eines unserer Theologen, dass er damals unter den kritisch-theologischen Bedingungen als Student gelernt hat, dass das Ende des Markusevangeliums aus logischen Gründen gestrichen war. Was nun? Die Lehrer von damals sagten dies und Rosenstock-Huessy das andere. Da brauchten wir erst mal einen Umschlagpunkt aus der Lukasgeschichte. Das Pauluswort „Ich weiß, wem ich geglaubt habe” brachte uns die Möglichkeit zu entscheiden, Rosenstock-Huessy zu folgen, der selber den Alten gefolgt ist, und mal sehen, was daraus kommen würde. Dafür haben wir auch eine Stunde gebraucht. Hinter geschlossenen Türen haben wir sogar die von Rosenstock-Huessy empfohlene spezielle Markus-Bet-Haltung geübt. Sitzen, stehen und liegen können wir ja alle.

Das Finden der qualitativen Mitte, des Umschlagpunktes, ging eigentlich sehr geschmeidig in den letzten anderthalb Stunden, wo wir aus einem Herzen (jedenfalls habe ich das so erlebt) zusammengearbeitet haben und zu meiner Überraschung den Moment fanden, wo die Jünger als Diener ‚Ich’ sagen lernten, da wurden wir uns einig:

Markus 10 42-45:\
„Jesus roept hen bij zich, en zegt tot hen: ge weet dat zij die geacht worden te regeren over de volkeren hen overheersen en dat hun groten hun macht op hen botvieren; zo is het niet bij u; nee, wie bij u groot wil worden, zal uw bediende zijn, en al wie bij u een eerste wil zijn, zal aller dienstknecht zijn, want ook de mensenzoon is niet gekomen om bediend te worden maar om te dienen en zijn lijf-en-ziel te geven als losprijs voor velen!” (Vertaling van Pieter Oussoren, De Naardense Bijbel).

”Und Jesus ruft sie heran und sagt zu ihnen: Ihr wißt, die als Anführer der Völker gelten, herrschen auf sie herunter, und ihre Großen lassen sie ihre Vollmacht spürten. Bei euch aber ist es nicht so! Sondern: Wer ein Großer bei euch werden will, sei euer Diener. Und wer bei euch Erster sein will, sei aller Knecht. Denn auch der Menschensohn ist nicht gekommen, um sich dienen zu lassen, sondern um zu dienen und sein Leben zu geben als Lösepreis für viele.” (verdeutscht von Fridolin Stier.)

Das ist ein Aha-Erlebnis, das jede und jeden in ihrer und seiner politischen Seiten des Lebens – in Vorständen, Regierungen und Betrieben zu Herzen nehmen muss.

> *Wilmy Verhage*


### Matthäus

Matthäus begann sich freizuschwimmen, oder eben: freizuschreiben von seiner jüdischen Herkunft. Das war uns in der Gruppe offenkundig. Aber dieses Freischreiben war ihm mehr Widerfahrnis denn Absicht. Sein Evangelium beginnt gegenteilig: mit der großen Selbstvergewisserung durch den Stammbaum Jesu. Er endet beim Bekenntnis zum Herrn für den ganzen Erdkreis. Auffällig ist, daß er in der zweiten Hälfte seines Evangeliums markant weniger Schriftbeweise bringt als in der ersten: Er – bzw. Jesus – hat das je länger, desto weniger nötig, weil er, Jesus, sich durch sein Tun als der Sohn Gottes erweist.

Das Evangelium des Matthäus führt das Stammesdenken weiter. Bezeichnend für dieses Denken ist der Versuch, die je eigene Genealogie „oben”, bei einer Gottheit, festzumachen oder doch die ersten Ahnen in einen numinosen Nebel zu stellen. Matthäus ist gegen Ende seines Schreibens darüber hinausgewachsen. Paulus folgt ihm, wie ein Teilnehmer aus der Gruppe bemerkte, wenn er im Philipperbrief schreibt, er betrachtet seine edle Herkunft „für Kot”, wenn es darum geht, Christus zu gewinnen.

Der Schritt zur Aktualisierung war entsprechend klein: der Islam als der Versuch, einem Stamm des 7. Jahrhunderts weltweite Geltung zu verschaffen. Ungebrochen, an Matthäus vorbei, will er alles in seine eigene kleine Geschichte pressen und für sich vereinnahmen. Gebrochen, mit Matthäus, müßte er seine Anfangszeit für beendet erklären und … – An dieser Stelle blieb uns Raum zum Weiterdenken.

> *Lothar Mack*
