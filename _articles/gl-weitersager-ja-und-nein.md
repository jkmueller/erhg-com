---
title: Gudrun Elisabeth Lemm zu Ja und Nein
category: weitersager
order: 55
---

*Göthewitz, Neujahr 2004*

Ja, Thomas Eipper,\
an Dich will ich mich wenden und Dir zumuten, was mich hat aufhorchen lassen, auflachen und weinen, aufatmen und antworten.

Dies kleine Büchlein bekam ich 1972 von einem Studenten aus Westberlin in die Hand, der es über die Grenze geschmuggelt hatte, weil ich die einzige in der spontanen Runde von Theologie Studierenden im Sprachenkonvikt war, die dem Namen Eugen Rosenstock- Huessy schon begegnet war, als Zitat in einem Buch, das ich für eine Seminararbeit verwendet hatte.

„In Verwaltung von Gudrun Eipper” schrieb ich hinein, als ich begriff, welche befreiende Botschaft mich in diesem Büchlein erreicht hatte. In „ewiger” fügte Bernd Dietrich ein, den ich damals zu lieben suchte. So wehrte er sich und so schickte er zurück in unendliche Ferne, worauf er nicht antworten wollte.

Verwaltung kann nicht ewig sein. Der Schatz sucht Erben. Du bist mir eingefallen als Adressat in der nächsten Generation.

Das wäre mir vor einem Jahr nicht in den Sinn gekommen, aber 2004 haben wir einander erlebt bei unseren Festen und beim Abschied von Deinem Opa Josef. Du hast in diesem Jahr Umstellungen erfahren, Neues begonnen, anderes geprobt. Du liebst ein Mädchen.

Ich war in den letzten Semestern meines zweiten Studiums, als ich das Buch in die Hand bekam, und Ja und Nein sagen hatte ich geübt in meiner Zeit als „sozialistischer” Lehrerin. Es hilft nichts, die Schüler zum rechten Denken zu erziehen, das Denken wird verbogen durch die Angst, hatte ich erkannt, und so begann ich, Ja oder Nein zu sagen, egal, was es kostet. Es kostete mich nach drei Jahren den mit allem Ernst erwählten Beruf. Aber da zeigte sich auch umgehend, wie ich weitergehen kann, und es war mir ein wunderbarer Trost, daß ich am gleichen Tag in Naumburg zum Theologiestudium immatrikuliert wurde, an dem die fristlose Entlassung vom letzten dafür zuständigen Gremium, dem Rat des Kreises Merseburg, bestätigt worden war.

In meinem zweiten Studium genoß ich es, nicht mehr der Ideologie entgegen denken und antworten zu müssen. Endlich kann ich einfach sagen und schreiben, was ich erkenne, was mir einleuchtet… Und doch war ich wie durch Wände beeinträchtigt, befremdet, fand gute Achtung, aber nicht wirklich Gehör bei denen, die da lehrten und merkte, daß ich nicht wirklich wohnen kann in den Lehrgebäuden, die mir in Vorlesungen und Büchern vorgestellt wurden.

„Das arme Kind und der Denker” da fand ich den Schlüssel, der mein Gefängnis öffnete,

da las ich auf Seite 38: „Es gibt nämlich drei Sprachstile:Hochsprache, Kindersprache, Denken. Und der Denker, der die Sprache verachtet, kann das nur, weil er meist nicht unmittelbar aus der Hochspraceh ins Denken übergeht, sondern seinen Denkstil meist aus der Kinderstube weiterentwickelt.

Die Hochsprache kennt jedes Ereignis und ernennt alle Mitglieder mit vollem Namen…

Die Kinderstube läßt die Namen weg und setzt für die Nomina die Pronomina, die Fürworte ein…

Die Kinder sagen „du", „ich”, „dort”, „hier”, „da”, „er”, „die”, „das”. Der Philosoph liebt aber das eine Pronomen vor allen andren: das Wörtchen „Sein”. Das Sein, da vergehen ihm Hören und Sehen. Er will das Sein ergründen."

So war ich erlöst davon, auf diese abstrakten Lehren zu starren als hülfen sie mir leben.

Und da las ich erwartungsvoll weiter, alles, was ich von Eugen Rosenstock Huessy bekommen konnte.

Zwischen dem, was mich begeisterte, war vieles, was ich nicht nachvollziehen konnte, aber ich merkte, daß sich im Weiterlesen immer wieder Zusammenhänge erschlossen und dort deutlich ausgeführt war, was hier wie flüchtig hingeworfen schien. Es ist wie ein Netz, das sich durch alle Sphären unseres Lebens zieht und Zusammenhänge tun sich auf von Dingen, die mir bis dahin nur in getrennten Bereichen begegnet sind:

Die Europäischen Revolutionen, die Sprache des Menschengeschlechtes, Dienst auf dem Planeten, Heilkraft und Wahrheit, Des Christen Zukunft, Der unbezahlbare Mensch Das Alter der Kirche, Werkstattaussiedlung ich habe diese Bücher alle hier neben mir stehen. Jetzt kann ich sie wieder neu lesen, weil ich ein Jahr nach meinem Vorruhestand endlich in der Eugen Rosenstock-Huessy Gesellschaft Menschen treffe, die davon berührt sind und versuchen, das Erfahrene zu erproben, weiterzusagen, hineinzuleben in die blind und taub gewordene Gesellschaft. Bis dahin hatte ich mich immer wieder als die einzige erlebt, die das ernst nimmt und die seltsam unverstanden bleibt, wenn sie davon sprechen oder von daher etwas ändern will.

Das sind Widerstände. Das hat mich oft ganz ratlos, müde, schweigsam gemacht. Doch gewiß bin ich auch, dies ist das Beste, was ich bisher gefunden habe, und so werden mir auch die Widerstände zum Zeichen, daß Eugen Rosenstock Huessy wirksam spricht und lehrtich schreibe nicht, gelehrt hat, obwohl er schon gestorben ist, etwa zur gleichen Zeit, als ich ihn als meinen Lehrer wahrzunehmen begann und so stelle ich ihn Dir vor mit dem Büchlein, das mir zuerst in die Hand kam, ob nicht auch in Dir etwas darauf harrt, das Dich bereit macht, aufhorchend einzutreten in das Gespräch des Menschengeschlechtes.

Deine Tante

*Gudrun Elisabeth Lemm,*\
*geb. Gudrun Eipper*

P.S. dem Studierenden technischer Fächer

**Das dritte Gesetz der Technik:**\
(nach Eugen Rosenstock-Huessy)

Die Anwendung einer neuen Technologie
1. erweitert den Raum,
2. verkürzt die Zeit,
3. und zerschlägt eine Gruppe.
