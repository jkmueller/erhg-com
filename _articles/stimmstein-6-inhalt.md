---
title: "Stimmstein 6: Inhalt"
created: 2001
category: veroeff-elem
published: 2024-04-14
---
### Stimmstein 6, 2001

#### Mitteilungsblätter  2001
Eugen Rosenstock-Huessy Gesellschaft

Eugen Rosenstock-Huessy:\
- **Die Kirche und die Völker**\
- **Das Volk Gottes in Vergangenheit, Gegenwart, Zukunft**

Mit Beiträgen von
- *Harold Berman*
- *Hans Thieme*
- *Joachim Günther*
- *Jehuda Halevi*
- *Freya von Moltke*
- *Jehuda Amichai*
- *Ger van Roon*
- *Wolfgang Ullmann*
- *Hebe Kohlbrugge*
- *Klaus von Dohnanyi*
- *Peter Keller*
- *Lise van der Molen*

Im Auftrag der Eugen Rosenstock-Huessy Gesellschaft\
herausgegeben von Andreas Möckel und Karl Johann Rese

Stimmstein\
6\
ARGO BOOKS

- [Inhalt]({{ 'stimmstein-6-inhalt' | relative_url }})
- [Editorial]({{ 'stimmstein-6-editorial' | relative_url }}) ..7
- [*Eugen Rosenstock-Huessy*, Die Kirche und die Völker]({{ 'text-erh-die-kirche-und-die-voelker-1930' | relative_url }}) ..11
- [*Eugen Rosenstock-Huessy*, Das Volk Gottes in Vergangenheit, Gegenwart, Zukunft](https://www.eckartwilkens.org/text-erh-das-volk-gottes/) ..20
- [*Harold Berman,* Recht und Revolution. Die Bildung der westlichen Rechtstradition]({{ 'hb-recht-und-revolution' | relative_url }}) ..40
- *Eugen Rosenstock-Huessy* und *Joachim Günter*, Briefwechsel 1953-1954 ..50
- [*Eugen Rosenstock-Huessy*, Die Tageszeiten des Glaubens]({{ 'text-erh-des-glaubens-tageszeiten-1951' | relative_url }}) ..55
- [*Hans Thieme*, Die Epochen des Rechts]({{ 'ht-die-epochen-des-rechts' | relative_url }}) ..56
- [*Jehuda Halevi*, Morgendlicher Dienst]({{ 'jh-morgenlicher-dienst-ost-west' | relative_url }}) ..60
- [*Jehuda Halevi*, Zwischen Ost und West, deutsch von *Franz Rosenzweig*]({{ 'jh-morgenlicher-dienst-ost-west' | relative_url }}) ..60
- *Freya von Moltke*, Eugen's Adult Years in Germany ..61
- *Jehuda Amichai*, Von Dreien oder Vieren im Zimmer ..76
- *Jehuda Amichai*, Wer sich verläßt auf die Zeit, deutsch von *Lydia* und *Paulus Böhmer* ..76
- [*Ger van Roon* und *Wolfgang Ullmann*, Freya von Moltke zum 90. Geburtstag]({{ 'fvm-zum-90-geburtstag' | relative_url }}) ..77
- *Hebe Kohlbrugge*, Hanna Kohlbrugge (1911-1999) ..81
- [*Klaus von Dohnanyi*, Im Land der Verlorenen. Zum Tod von Sabine Leibholz-Bonhoeffer]({{ 'kvd-im-land-der-verlorenen' | relative_url }}) ..85
- [*Peter C. Keller*, Glänzend, doch mit editorischen Schwächen, Sebastian Haffner: Geschichte eines Deutschen]({{ 'pk-sebastian-haffner-geschichte-eines-deutschen' | relative_url }}) ..87
- [*Lise van der Molen*, Harold J. Berman: Faith and Order]({{ 'lm-berman-faith-and-order-the-reconciliation-of-law-and-religion' | relative_url }}) ..99
- [Nachrichten]({{ 'stimmstein-6-nachrichten' | relative_url }}) ..102

*Die Staaten und die Staats-Nationen sind, gemessen an der Kirche, die ihnen als erste vorangeht, die zweite Gestalt unserer Zeitrechnung. Die Gesellschaft ist die dritte Gestalt. Sie bringt Einheit wie die Kirche, widerstandsloser als die Kirche, nämlich wirklich über das Erdenrund. Sie überrundet und kreist ein kleine und große Völker, sie macht die Völker zu bloßen Filialen des Welthandelshauses Ökumene. Sie uniformiert die Völker durch die Mode, das Radio, den Kino, das Flugzeug usw. Und sie verkettet sie durch Handel und Wirtschaft und Verkehr und Arbeitsteilung zu einer einzigen Ökonomie.*\
[*Eugen Rosenstock-Huessy* 1930]({{ 'text-erh-die-kirche-und-die-voelker-1930' | relative_url }})

Die Deutsche Bibliothek − CIP Einheitsaufnahme

Eugen Rosenstock-Huessy: Das Volk Gottes in Vergangenheit, Gegenwart, Zukunft. Im Auftrag der Rosenstock-Huessy Gesellschaft hrsg. von Andreas Möckel und Karl-Johann Rese...mit Beiträgen von Harold Berman ...1. Aufl. Körle ARGO BOOKS Verlag 2001 (Mitteilungsblätter - stimmstein 6)

ISBN 3-936094-00-4

Redaktionsgruppe:\
*Michael Gormann-Thelen, Andreas Möckel,  Lise van der Molen, Karl-Johann Rese, Adri Sevenster*

Manuskripte und Leserbriefe an:\
*Andreas Möckel, Von-Luxburg-Str. 9, D-97074 Würzburg*\
*Karl-Johann-Rese, Emdener Straße 41, D-10551 Berlin*

Für Druckerlaubnis danken wir Klaus von Dohnanyi, Barbara Günther-Hendler, Freya von Moltke, ferner der Stiftung Kreisau für europäische Verständigung, dem Eugen Rosenstock-Huessy Fund USA, dem Deutschen Literatur Archiv Marbach a. N. und dem Suhrkamp Verlag, Frankfurt a. M.

ARGO BOOKS Verlag und Verlagsbuchhandlung\
Melsungerstraße 24,  D-34327 Koerle\
Druck: Englert, Würzburg\
Umschlaggestaltung: Handelsdruckerei Rosenbaum & Sohn, Würzburg\
Satz und Gestaltung ARGO BOOKS Verlag\
ISBN 3-936094-00-4
