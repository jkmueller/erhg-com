---
title: Simon-Dubnow-Institut Jahreskonferenz 2013
category: rezeption
---
![Simon-Dubnow-Institut]({{ 'assets/images/simon-dubnow-leipzig.jpg' | relative_url }}){:.img-right.img-large}
***Biographie und Erkenntnis***\
*Eugen Rosenstock-Huessy (18881973)*\
*oder die Konversionen des Wissens*

Die Jahreskonferenz des Simon-Dubnow-Instituts widmet sich dem Wirken und Werk des Universalhistorikers, Soziologen und Sprach-philosophen Eugen Rosenstock-Huessy (1888-1973), der am 7. Juli 1913 neben Franz Rosenzweig und Rudolf Ehrenberg einer der Protagonisten des geistesgeschichtlich bedeutsamen „Leipziger Nachtgesprächs” war. Dessen hundertste Wiederkehr wird zum Anlass genommen, um über eine erneute Lektüre von Selbstzeugnis-sen der Hauptbeteiligten die Folgen und Wirkungen der Begegnung in Leipzig zu beleuchten. Einen weiteren Schwerpunkt der Konferenz bilden die beruflichen und denkerischen Konsequenzen, die Rosen-stock-Huessy aus dem von ihm als Schlüsselerlebnis wahrgenomme-nen Ersten Weltkrieg zog. Schließlich wird sich der Entwicklung von Rosenstock-Huessys historisch-soziologischem Werk angenähert. Der Bogen spannt sich dabei von den frühen mediävistischen Schriften über seine Arbeiten zu den Revolutionen bis hin zu seiner universalhistorischen Metahistorie.


[Programm der Tagung](https://www.dubnow.de/fileadmin/user_upload/PDF/Programm_Jahreskonferenz_2013.pdf)
