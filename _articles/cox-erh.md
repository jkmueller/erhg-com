---
title: "Harvey Cox about Rosenstock-Huessy"
category: aussage
published: 2022-01-04
name: Cox
language: english
---


„It is unfortunate that Rosenstock-Huessy's thought has been so overlooked. For years he has been concerned with many of the same things theologians are grappling with today, that is, the meaning of speech, the question of hermeneutics, the problem of secularization, and the disappearance of a sense of the transcendent in modern life.”
>*Harvey G. Cox*
