---
title: "Agnes Lieverse: Drei Sommer-Höhepunkte auf DVD"
category: bericht
created: 2010
---
Im Sommer 1987 trafen sich die Bewohner des Rosenstock-Huessy-Huis mit den Mitgliedern der Eugen Rosenstock-Huessy Gesellschaft in einer Ferienkolonie in Landsrade bei Gulpen. Thema dieser Woche war „Die Rolle der Frau im Denken von Eugen Rosenstock-Huessy”. Im Sommer darauf (1988) feierten wir zusammen den 100. Geburtstag von ERH. In Haarlem fand ein Symposium und ein großes Fest statt.

Aufgrund der gelungenen gemeinsamen Woche 1987 wurde im Sommer 1989 eine zweite gemeinsame Ferienwoche in Gulpen organisiert. Diesmal mit dem Thema „Dienen auf dem Planeten”. Ein wichtiger Gast war dabei Mark Feedman aus der Dominikanischen Republik. Diese DVD enthält die Filme zu diesen Ereignissen.

Sie ist Teil einer Filmserie zur Geschichte des Rosenstock-Huessy Huis in Haarlem, die Elly Hartman in der Zeit von 1971-1991 mit einer Super-8-Kamera aufgenommen hat. Um diese Filme für die Zukunft zu bewahren und besser zugänglich zu machen, hat eine Gruppe von ehemaligen Bewohnern des RHH zusammen mit Elly das Material gesichtet, untertitelt und auf DVD übertragen.

Dank vieler begeisterter Reaktionen und finanzieller Unterstützung konnte dieser Plan ausgeführt werden. Inzwischen ist die Digitalisierung fast abgeschlossen und liegen zahlreiche DVDs vor. Wir hoffen, dass Sie Freude an dieser DVD haben werden.

Kosten DVD: €12,50 (einschließlich Versandkosten).\
Kontonummer kommt nach dem Auftrag\
E-Mail: aglieveopdam@xs4all.nl\
Brief:\
Agnes Lieverse Opdam,\
Beringlaan 4,\
2803 GA Gouda\
Niederlande

Agnes Lieverse\
Januar 2010
