---
title: Mitgliederbrief 2015-08
category: rundbrief
created: 2015-08
summary:
zitat: |
  >„Unser Geist wacht nur wenn er weiß: Das kommt nie wieder. Unermüdetheit ist also die Vorbedingung für die Ausdauer einer Gesellschaftsordnung.\
  >Unsere Ära zieht Herzen aus der Welt, um unermüdet zu bleiben, trotz der Wiederkehr des Gleichen im Alltag der Fabriken. Wenn sie es nicht tut, verfällt sie.”
  >*Eugen Rosenstock-Huessy: Soziologie II,683*

---

## Eugen Rosenstock-Huessy Gesellschaft e.V.

{{ page.zitat }}

**Vorstand/board/bestuur:**\
*Dr. Eckart Wilkens (Vorsitzender);*\
*Thomas Dreessen; Andreas Schreck; Dr. Jürgen Müller*\
*Antwortadresse: Dr. Eckart Wilkens: Everhardstraße 77, 50823 Köln, Deutschland*\
*Tel: 0(049) 221 511265*

***Brief an die Mitglieder September 2015***

**Inhalt**

{{ page.summary }}


[zum Seitenbeginn](#top)
