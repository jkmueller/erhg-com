---
title: Mitgliederbrief 2013-12
category: rundbrief
created: 2013-12
summary:
zitat: |
  > Wir Menschen sprechen also zu gar keinem anderen Behufe, als daß wir einander du sagen können.\
  >*Eugen Rosenstock-Huessy, Die Umkehr des Worts, in: Soziologie II, S. 561*
---

## Eugen Rosenstock-Huessy Gesellschaft e.V.

{{ page.zitat }}

**Vorstand/board/bestuur:**\
*Dr. Eckart Wilkens (Vorsitzender);*\
*Thomas Dreessen; Andreas Schreck; Dr. Jürgen Müller*\
*Antwortadresse: Dr. Eckart Wilkens: Everhardstraße 77, 50823 Köln, Deutschland*\
*Tel: 0(049) 221 511265*

***Brief an die Mitglieder Dezember 2013***

**Inhalt**

{{ page.summary }}


[zum Seitenbeginn](#top)
