---
title: Satzung der Eugen Rosenstock-Huessy Gesellschaft e.V. Bielefeld
menu: Satzung
column: 2
row: 6
used-categories: satzung
---
## §1
1. Die Eugen Rosenstock-Huessy Gesellschaft ist ein Verein im Sinne des BGB und soll die Rechtsfähigkeit durch Eintragung ins Vereinsregister erlangen. Der Vorstand veranlaßt die Eintragung in das Vereinsregister.
2. Der Verein hat seinen Sitz in Bielefeld.
3. Die Geschäftsstelle befindet sich am Sitz des Vereins. Sie kann durch Beschluß der Mitgliederversammlung an einen anderen Ort verlegt werden.
4. Geschäftsjahr ist das Kalenderjahr.

## §2
1. Der Verein stellt sich die Aufgabe, im Geiste Eugen Rosenstock-Huessys der Pflege und Förderung der Wissenschaft zu dienen und damit die vielfachen Anregungen seines Werkes in allen Lebensbereichen fruchtbar werden zu lassen.
2. Die Aufgaben umfassen neben Tagungen und Vorträgen die Herausgabe von Schriften und Mitteilungen, den Aufbau und die Verwaltung des Eugen Rosenstock-Huessy Archivs und die Übernahme von Privatarchiven, um sie der Öffentlichkeit und der wissenschaftlichen Forschung zugänglich zu machen.
3. Die Gesellschaft verfolgt ausschließlich und unmittelbar gemeinnützige Zwecke im Sinne des Abschnittes "Steuerbegünstigte Zwecke" der Abgabenordnung.
4. Der Verein ist selbstlos tätig; er verfolgt nicht in erster Linie eigenwirtschaftliche Zwecke.

## §3
1. Mitglieder können Einzelpersonen, Personenvereinigungen und juristische Personen werden.
2. Die Mitgliedschaft wird mit der Aufnahme durch den Vorstand erworben. Ein abgelehnter Bewerber um die Mitgliedschaft hat innerhalb eines Monats nach Bekanntgabe des Ablehnungsbeschlusses das Recht, die nächste Mitgliederversammlung anzurufen; diese entscheidet endgültig. Ein Aufnahmeanspruch besteht nicht.
3. Die Mitgliedschaft endigt durch Kündigung seitens des Mitglieds oder des Vorstandes oder durch Tod. Ein ausgeschlossenes Mitglied hat innerhalb eines Monats nach Bekanntgabe des Ausschlusses (unzustellbare Postsendungen gelten als bekannt gegeben, wenn der Beschluß des Ausschlusses an die zuletzt bekannte Adresse versandt worden ist) die Möglichkeit, die nächste Mitgliederversammlung anzurufen; diese entscheidet endgültig über die Mitgliedschaft.
4. Die Kündigung muß schriftlich mindestens drei Monate vor Beendigung des Geschäftsjahres ausgesprochen werden.
5. Die Gesellschaft kennt Förderer (Donatore), die nicht Mitglieder der Gesellschaft sind, diese aber durch regelmäßige Zuwendungen in der Höhe von jährlich mindestens 12.- € unterstützen.

## §4
1. Von den Mitgliedern werden Jahresbeiträge erhoben. Höhe und Fälligkeit der Jahresbeiträge werden von der Mitgliederversammlung festgesetzt, wobei nach Mitgliedsgruppen differenziert werden kann.
2. Jedes Mitglied hat in der Mitgliederversammlung eine 1. Stimme.
3. Die Mitglieder erhalten keine Anteile aus einem rechnungsmäßigen Überschuß und als Mitglieder auch keine sonstigen Zuwendungen aus Mitteln des Vereins.
4. Kein Mitglied darf durch zweckfremde Verwaltungsaufgaben oder durch unangemessene Vergütungen begünstigt werden. Die mit einem Ehrenamt betrauten Mitglieder haben nur Anspruch auf Ersatz tatsächlich erfolgter Auslagen.

## §5
1. Organe des Vereins sind:
    1. der Vorstand,
    2. der Beirat,
    3. die Mitgliederversammlung.
2. Der Vorstand besteht aus fünf Personen. Der Präsident ist Vorsitzender, der Vizepräsident ist stellvertretender Vorsitzender. Der Vorstand gibt sich eine Geschäftsordnung.
3. Der Beirat ist ein Gremium, das den Vorstand in wichtigen Fragen der Tätigkeit des Vereins berat. Die Zahl der Mitglieder beträgt mindestens neun 9.. Er soll sich aus Angehörigen verschiedener Lebensbereiche und Sprachgemeinschaften zusammensetzen.

## §6
1. Die Mitgliederversammlung tritt mindestens einmal jährlich zusammen. Sie wird vom geschäftsführenden Vorstand mindestens vier Wochen vorher schriftlich unter Beifügung der Tagesordnung einberufen.
2. Die Mitgliederversammlung beschließt in den Angelegenheiten des Vereins. Insbesondere wählt sie den
Vorstand und bestimmt den Präsidenten, den Vizepräsidenten und das dritte geschäftsführende
Vorstandsmitglied im Sinne von § 7 der Satzung.
Sie wählt den Beirat.
Sie beschließt den Jahresarbeitsplan.
Sie beschließt Satzungsänderungen.
Sie beruft die Kassenprüfer und beschließt über die Anerkennung der Jahresrechnung und die Entlastung des Vorstandes.
Sie beschließt die Jahresbeitrage.
Sie beschließt die Auflösung.
3. Die Mitgliederversammlung entscheidet mit einfacher Mehrheit. Satzungsänderungen und die Auflösung müssen mit einer Mehrheit von drei Vierteln der erschienenen Mitglieder beschlossen werden. Jede Satzungsänderung ist dem zuständigen Finanzamt durch Übersendung der geänderten Satzung anzuzeigen.
4. Über die Beschlüsse der Mitgliederversammlung sind Protokolle anzufertigen, die vom geschäftsführenden Vorstand zu unterzeichnen sind.

## §7
1. Der Präsident, der Vizepräsident und ein weiteres Vorstandsmitglied (vgl. § 6 2.) bilden den geschäftsführenden Vorstand.
2. Der geschäftsführende Vorstand vertritt den Verein gerichtlich und außergerichtlich im Sinne von § 26 2. BGB. Zur Vertretung sind die Unterschriften von zwei Mitgliedern des geschäftsführenden Vorstandes erforderlich und genügend.
3. Der Vorstand wird für jeweils zwei Jahre gewählt. Der erste Vorstand wird jedoch nur für das erste Geschäftsjahr gewählt.
4. Der Vorstand ist beschlußfähig, wenn mindestens 4 Vorstandsmitglieder anwesend sind. Der Vorstand entscheidet mit Stimmenmehrheit seiner Mitglieder.
5. Der geschäftsführende Vorstand ist nur beschlußfahig, wenn alle geschäftsführenden Vorstandsmitglieder anwesend sind. Ist ein Mitglied des geschäftsführenden Vorstandes verhindert, so können Beschlüsse gefaßt werden. Solche Beschlüsse werden aber nur mit der schriftlichen Zustimmung des verhinderten Vorstandsmitgliedes wirksam. Der geschäftsführende Vorstand entscheidet einstimmig.

## §8
1. Bei Auflösung des Vereins oder bei Wegfall seines bisherigen Zweckes fällt das verbleibende Barvermögen des Vereins an die Deutsche Forschungsgemeinschaft. Die bedachte Stelle hat es ausschließlich und unmittelbar im Sinne § 2 dieser Satzung für gemeinnützige Zwecke zu verwenden. Beschlüsse über die künftige Verwendung des Vermögens dürfen erst nach Einwilligung des zuständigen Finanzamtes ausgeführt werden.
2. Das Sachvermögen – Archiv, Schriftwechsel, Inventar, Urheberrechte u.a.fällt an eine öffentlich-rechtliche Körperschaft, die die Gewähr bietet, dass das Archiv im Rahmen einer Bibliothek der Öffentlichkeit und der wissenschaftlichen Forschung zugänglich bleibt. Die bedachte Bibliothek wird von der Mitgliederversammlung bestimmt, spätestens zusammen mit einem Auflösungsbeschluß.

Bielefeld, den 6. Juli 1963

mit den Satzungsänderungen vom 2. und 3. August 1987 in Gulpen (Niederlande),\
vom 7. Oktober 1990 in Loccum,\
vom 28. Oktober 2001 in Berlin und\
vom 27. 10. 2002 in Iserlohn.

[zum Seitenbeginn](#top)
