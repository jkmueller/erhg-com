---
title: Links
column: 2
row: 7
used-categories: link
---

## Befreundete Werke und Initiativen

* [***Respondeo***](https://www.respondeo.nl)\
  Niederländische Gesellschaft, die sich mit dem Gedankengut
  Rosenstock-Huessys auseinandersetzt.

* [***Eugen Rosenstock-Huessy Fund***](http://erhfund.org)\
  Der Fund setzt sich in den USA für die Sicherung und
  Verbreitung des Werkes von ERH ein.

* [***Eugen Rosenstock-Huessy Society of North America***](http://erhsociety.org)

* [***Martin Buber-Gesellschaft***](https://buber-gesellschaft.eu)

* [***Hans Ehrenberg Gesellschaft***](https://hansehrenberg.info/)

* [***Freya von Moltke-Stiftung***](http://www.fvms.de)

* [***Internationale Rosenzweig-Gesellschaft***](https://www.rosenzweig-gesellschaft.org)

* [***Joseph Wittig Gesellschaft***](http://www.joseph-wittig.de)

* [***Stiftung Kreisau für Europäische Verständigung***](https://www.krzyzowa.org.pl/de)

* [***Fundacja Krzyżowa dla Porozumienie Europejskiego***](https://www.krzyzowa.org.pl/pl)

* [***Kreisau-Initiative Würzburg***](http://www.kreisau-initiative.de)\
  Partnerorganisation der Stiftung Kreisau in Deutschland

## Persönliche Webseiten

* [***Feico Houweling's Homepage***](https://www.feico-houweling.nl)\
  Erzählte Geschichte auf der Grundlage
  von Eugen Rosenstock-Huessys Einsichten.

* [***Otto Kroesen's Homepage***](https://temporavitae.nl)\
  Inspiriert von Rosenstock-Huessy stellt
  er seine Arbeiten als Soziologe, Technikphilosph
  und Coach vorstellt.

* [***Fritz Herrenbrück's Homepage***](http://www.fritz.herrenbruck.de)\
  Unter anderem mit einer Rezension der
  Talheimer Ausgabe der Soziologie.

* [***Rosenstock-Huessy und seine Bedeutung***](https://www.erhg.net)\
  Einführungstexte zu Rosenstock-Huessy in mehreren Sprachen zusammengestellt von Jürgen Müller.

* [***Eckart Wilkens' Texte zu Eugen Rosenstock-Huessy***](https://www.eckartwilkens.org)\
  
